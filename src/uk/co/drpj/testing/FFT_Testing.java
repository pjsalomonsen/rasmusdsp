/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.testing;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.controls.ControlViewer;
import rasmus.interpreter.metadata.FunctionCallEditor;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.util.FFT;
import rasmus.interpreter.sampled.util.FFTConstantQ;
import rasmus.interpreter.sampled.util.LogDFT;
import rasmus.interpreter.ui.AboutDialog;
import rasmus.interpreter.ui.GroupMenuListener;
import rasmus.interpreter.ui.GroupMenus;
import rasmus.interpreter.ui.RSyntaxDocument;
import rasmus.interpreter.ui.RegisterFunction;
import rasmus.interpreter.ui.RegisterUnit.Record;
import rasmus.testing.SoundFontTester;
import rasmus.util.RasmusUtil;

public class FFT_Testing extends JFrame {
	
	private static final long serialVersionUID = 1L;
	BufferedImage offImg;
	JPanel panel;
	JPanel cpanel;
	JTextPane textarea = new JTextPane();
	
	boolean process_running = false;
	boolean b_phase_included;
	public void processStereoImage(boolean phase_included)
	{		
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		b_phase_included = phase_included;
		Runnable runnable = new Runnable() { boolean phase_included = b_phase_included; public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}
			
			AudioSession audiosession = new AudioSession(samplerate, 2);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			
			
			int fftsize = 1024*8;
			
			int fftsizeh = fftsize / 2;
			
			double fftsizeh1 = fftsize / 4; // 256
			double fftsizeh2 = fftsize / 8; // 128
			
			int readsize = 1024*2;
			int fftoffset = fftsize*2 - readsize;
			int seclen = (int)(samplerate / (readsize/2));
			int secounter = 0;
			
			FFT fft = new FFT(fftsize);
			double[] window = fft.wHanning();
			
			double[] buffer = new double[fftsize*2];
			
			
			//for (int yy = 0; yy <  ww-10; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}				
				
				for (int i = 0; i < fftoffset; i++) {
					buffer[i] = buffer[i + readsize]; 
				}
				int len = audiostream.replace(buffer, fftoffset, fftsize*2);						
				if(len == -1) break;
				
				//len += fftsize;
				
				//len /= 2;
				
				double[] noiseL = new double[fftsize];									
				double[] noiseR = new double[fftsize];									
				// Fill FFT Buffer
				
				for (int i = 0; i < fftsize; i++) {		
					
					noiseL[i] = buffer[i*2] * window[i];
					//noiseL[2*i + 1] = 0;                  
					
					noiseR[i] = buffer[i*2 + 1] * window[i];
					//noiseR[2*i + 1] = 0;                   
				}
				
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				fft.calcReal(noiseL, -1);
				fft.calcReal(noiseR, -1);
				
				//double fftstart = 220;
				//double fftscale = -10;
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				double[] fmagn1 = new double[513];
				double[] fmagn2 = new double[513];
				double[] fmagn3 = new double[513];
				
				for (int i = 0; i < fftsizeh; i++) {
					
					//noiseL[2*i] = avgL[2*i]*0.5 + noiseL[2*i]*0.5;
					//noiseL[2*i + 1] = avgL[2*i + 1]*0.5 + noiseL[2*i +1]*0.5;
					//noiseR[2*i] = avgR[2*i]*0.5 + noiseR[2*i]*0.5;
					//noiseR[2*i + 1] = avgR[2*i + 1]*0.5 + noiseR[2*i + 1]*0.5;
					
					//avgL[2*i] = noiseL[2*i];
					//avgL[2*i + 1] = noiseL[2*i + 1];
					//avgR[2*i] = noiseR[2*i];
					//avgR[2*i + 1] = noiseR[2*i + 1];
					
					double L_real = noiseL[2*i];
					double L_imag = noiseL[2*i+1];
					double L_magn = 2.*Math.sqrt(L_real*L_real + L_imag*L_imag);
					double L_phase = Math.atan2(L_imag,L_real);							
					
					double R_real = noiseR[2*i];
					double R_imag = noiseR[2*i+1];
					double R_magn = 2.*Math.sqrt(R_real*R_real + R_imag*R_imag);
					double R_phase = Math.atan2(R_imag,R_real);							
					
					double magn = (L_magn + R_magn)/2;
					
					double T_magn = L_magn + R_magn;
					double pan = R_magn / T_magn;
					
					double phase_diff = Math.abs(L_phase - R_phase) % (2*Math.PI);
					if(phase_diff > Math.PI) phase_diff = (2*Math.PI) - phase_diff;
					phase_diff /= Math.PI;
					
					int pos = (int)(pan*512);
					/*
					 int phase_size;
					 if(pos > 256)
					 {
					 phase_size = 512 - pos;
					 }
					 else
					 {
					 }*/
					
					int phase_size1 = pos;
					int phase_size2 = 512 - pos;
					
					int i_phase_diff1 = (int)(phase_diff * phase_size1);
					int i_phase_diff2 = (int)(phase_diff * phase_size2);
					if(!phase_included)
					{
						i_phase_diff1 = 0;
						i_phase_diff2 = 0;
					}
					
					int posa = pos - i_phase_diff1;
					int posb = pos + i_phase_diff2;
					//int posa = pos;
					//int posb = pos;
					
					if(posa > 512) posa = 512;
					if(posa < 0) posa = 0;
					if(posb > 512) posb = 512;
					if(posb < 0) posb = 0;
					double ii = i;
					if(i < fftsizeh2) // 0 .. 6 khz
					{
						double magna = magn *  Math.pow(10, -5*(1 - (fftsizeh2 - (ii)) / fftsizeh2)); 
						fmagn1[posa] += magna;
						fmagn1[posb] += magna;
						magna = magn * Math.pow(10, -5*(1 - (ii) / fftsizeh2)); 
						fmagn2[posa] += magna;
						fmagn2[posb] += magna;								
						
					}
					else
						if(i < fftsizeh1)  // 6 ..12 khz
						{
							
							double magna = magn * Math.pow(10, -5*(1 - (fftsizeh2 - (ii - fftsizeh2)) / fftsizeh2)); 
							fmagn2[posa] += magna;
							fmagn2[posb] += magna;
							
							magna = magn * Math.pow(10, -5*(1 - (ii - fftsizeh2) / fftsizeh2)); 
							fmagn3[posa] += magna;
							fmagn3[posb] += magna;									
						}
						else								
							//if(i < 384) // 12 .. 24 khz
						{
							fmagn3[posa] += magn;
							fmagn3[posb] += magn;								
						}
					
					
					
					
					
					
				}
				
				for (int i = 0; i < 513; i++) {
					
					
					double logman1 = Math.log10(fmagn1[i]/fftsizeh)*2;							
					float colormag1 = (float)((logman1+9) / 7.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					double logman2 = Math.log10(fmagn2[i]/fftsizeh)*2;							
					float colormag2 = (float)((logman2+9) / 7.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					double logman3 = Math.log10(fmagn3[i]/fftsizeh)*2;							
					float colormag3 = (float)((logman3+9) / 7.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					
					/*
					 double logman = Math.log10(fmagn[i]/512.0)*2;							
					 
					 float colormag3 = (float)((logman+7) / 7.0);
					 if(colormag3 > 1.0) colormag3 = 1.0f;
					 else
					 if(colormag3 < 0.0) colormag3 = 0.0f;
					 
					 float colormag2 = (float)((logman+9) / 7.0);
					 if(colormag2 > 1.0) colormag2 = 1.0f;
					 else
					 if(colormag2 < 0.0) colormag2 = 0.0f;
					 
					 float colormag1 = (float)((logman+8) / 7.0);
					 if(colormag1 > 1.0) colormag1 = 1.0f;
					 else
					 if(colormag1 < 0.0) colormag1 = 0.0f;
					 */
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,i, yy, i);
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				panel.repaint();
				//if(!process_running) return;
				secounter--;
				
				
				
			}
			
			/*
			 g2.setColor(Color.GREEN.darker());
			 for (int i = 0; i < 24; i++) {
			 int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
			 
			 g2.drawLine(0, y, panel.getWidth(), y);
			 }*/
			
			g2.setColor(Color.GREEN);
			g2.drawLine(panel.getWidth()-10, 0, panel.getWidth(), 0);
			g2.drawLine(panel.getWidth()-10, 256, panel.getWidth(), 256);
			g2.drawLine(panel.getWidth()-10, 512, panel.getWidth(), 512);
			
			panel.repaint();
			audiostream.close();		
			interpreter.close();
					
			
			process_running = false;
			hideFFTPanel();
			
			//System.out.println("LO = " + lo);
			//System.out.println("HI = " + hi);
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();
	}
	
	
	public void processStereoPhaseImage()
	{
		//double hi = -10000;
		//double lo = 10000;
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			
			Graphics2D g2 = offImg. createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double 
			samplerate = 44100;
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 2);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			
			
			int fftsize = 1024*8;
			
			int fftsizeh = fftsize / 2;
			
			int fftsizeh1 = fftsize / 4; // 256
			int fftsizeh2 = fftsize / 8; // 128
			
			int readsize = 1024*2;
			int fftoffset = fftsize*2 - readsize;
			int seclen = (int)(samplerate / (readsize/2));
			int secounter = 0;
			
			FFT fft = new FFT(fftsize);
			double[] window = fft.wHanning();
			
			double[] buffer = new double[fftsize*2];
			
			
			//for (int yy = 0; yy <  ww-10; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}				
				
				for (int i = 0; i < fftoffset; i++) {
					buffer[i] = buffer[i + readsize]; 
				}
				int len = audiostream.replace(buffer, fftoffset, fftsize*2);						
				if(len == -1) break;
				
				//len += fftsize;
				
				//len /= 2;
				
				double[] noiseL = new double[fftsize];									
				double[] noiseR = new double[fftsize];									
				// Fill FFT Buffer
				for (int i = 0; i < fftsize; i++) {		
					
					noiseL[i] = buffer[i*2] * window[i];
					//noiseL[2*i + 1] = 0;                  
					
					noiseR[i] = buffer[i*2 + 1] * window[i];
					//noiseR[2*i + 1] = 0;                   
				}
				
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				fft.calcReal(noiseL, -1);
				fft.calcReal(noiseR, -1);
				
				//double fftstart = 220;
				//double fftscale = -10;
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				double[] fmagn1 = new double[513];
				double[] fmagn2 = new double[513];
				double[] fmagn3 = new double[513];
				
				for (int i = 0; i < fftsizeh; i++) {
					
					//noiseL[2*i] = avgL[2*i]*0.5 + noiseL[2*i]*0.5;
					//noiseL[2*i + 1] = avgL[2*i + 1]*0.5 + noiseL[2*i +1]*0.5;
					//noiseR[2*i] = avgR[2*i]*0.5 + noiseR[2*i]*0.5;
					//noiseR[2*i + 1] = avgR[2*i + 1]*0.5 + noiseR[2*i + 1]*0.5;
					
					//avgL[2*i] = noiseL[2*i];
					//avgL[2*i + 1] = noiseL[2*i + 1];
					//avgR[2*i] = noiseR[2*i];
					//avgR[2*i + 1] = noiseR[2*i + 1];
					
					double L_real = noiseL[2*i];
					double L_imag = noiseL[2*i+1];
					double L_magn = 2.*Math.sqrt(L_real*L_real + L_imag*L_imag);
					double L_phase = Math.atan2(L_imag,L_real);							
					
					double R_real = noiseR[2*i];
					double R_imag = noiseR[2*i+1];
					double R_magn = 2.*Math.sqrt(R_real*R_real + R_imag*R_imag);
					double R_phase = Math.atan2(R_imag,R_real);							
					
					double magn = (L_magn + R_magn)/2;
					
					double T_magn = L_magn + R_magn;
					double pan = R_magn / T_magn;
					
					double phase_diff = Math.abs(L_phase - R_phase) % (2*Math.PI);
					if(phase_diff > Math.PI) phase_diff = (2*Math.PI) - phase_diff;
					phase_diff /= Math.PI;
					
					int pos = (int)(pan*512);
					
					int phase_size = fftsizeh1;
					double i_phase_diff = (phase_diff * phase_size);
					//if(!phase_included)
					//i_phase_diff = 0;
					
					int posa = pos;
					//int posa = pos;
					//int posb = pos;
					
					//if(posa > 512) posa = 512;
					//if(posa < 0) posa = 0;
					//if(posb > 512) posb = 512;
					//if(posb < 0) posb = 0;
					
					/*
					 if(i < fftsizeh2) // 0 .. 6 khz
					 {
					 double magna = magn *  Math.pow(10, -5*(1 - (fftsizeh2 - (ii)) / fftsizeh2)); 
					 magna = magn * Math.pow(10, -5*(1 - (ii) / fftsizeh2)); 			
					 }
					 else
					 if(i < fftsizeh1)  // 6 ..12 khz
					 {
					 
					 double magna = magn * Math.pow(10, -5*(1 - (fftsizeh2 - (ii - fftsizeh2)) / fftsizeh2)); 
					 magna = magn * Math.pow(10, -5*(1 - (ii - fftsizeh2) / fftsizeh2)); 
					 }*/
					
					
					
					if(i_phase_diff < fftsizeh2) // 0 .. 6 khz
					{
						double magna = magn * Math.pow(10, -5*(1 - (fftsizeh2 - (i_phase_diff)) / fftsizeh2)); 
						fmagn1[posa] += magna*2;
						
						magna = magn * Math.pow(10, -5*(1 - (i_phase_diff) / fftsizeh2)); 
						fmagn2[posa] += magna*2;
						
					}
					else
						if(i_phase_diff <= fftsizeh1)  // 6 ..12 khz
						{
							
							double magna = magn * Math.pow(10, -5*(1 - (fftsizeh2 - (i_phase_diff - fftsizeh2)) / fftsizeh2)); 
							fmagn2[posa] += magna*2;
							
							magna = magn * Math.pow(10, -5*(1 - (i_phase_diff - fftsizeh2) / fftsizeh2)); 
							fmagn3[posa] += magna*2;
						}
					
					
					
					
					
					
					
				}
				
				for (int i = 0; i < 513; i++) {
					
					
					double logman1 = Math.log10(fmagn1[i]/fftsizeh)*2;							
					float colormag1 = (float)((logman1+9) / 7.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					double logman2 = Math.log10(fmagn2[i]/fftsizeh)*2;							
					float colormag2 = (float)((logman2+9) / 7.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					double logman3 = Math.log10(fmagn3[i]/fftsizeh)*2;							
					float colormag3 = (float)((logman3+9) / 7.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					
					/*
					 double logman = Math.log10(fmagn[i]/512.0)*2;							
					 
					 float colormag3 = (float)((logman+7) / 7.0);
					 if(colormag3 > 1.0) colormag3 = 1.0f;
					 else
					 if(colormag3 < 0.0) colormag3 = 0.0f;
					 
					 float colormag2 = (float)((logman+9) / 7.0);
					 if(colormag2 > 1.0) colormag2 = 1.0f;
					 else
					 if(colormag2 < 0.0) colormag2 = 0.0f;
					 
					 float colormag1 = (float)((logman+8) / 7.0);
					 if(colormag1 > 1.0) colormag1 = 1.0f;
					 else
					 if(colormag1 < 0.0) colormag1 = 0.0f;
					 */
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,i, yy, i);
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				panel.repaint();		
				//if(!process_running) return;
				secounter--;
				
			}
			
			/*
			 g2.setColor(Color.GREEN.darker());
			 for (int i = 0; i < 24; i++) {
			 int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
			 
			 g2.drawLine(0, y, panel.getWidth(), y);
			 }*/
			
			g2.setColor(Color.GREEN);
			g2.drawLine(panel.getWidth()-10, 0, panel.getWidth(), 0);
			g2.drawLine(panel.getWidth()-10, 256, panel.getWidth(), 256);
			g2.drawLine(panel.getWidth()-10, 512, panel.getWidth(), 512);
			
			panel.repaint();
			audiostream.close();						
			interpreter.close();
					
			
			process_running = false;
			hideFFTPanel();
			
			//System.out.println("LO = " + lo);
			//System.out.println("HI = " + hi);
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();		
	}
	
	
	
	public void processPhaseImage()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 2);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			
			
			int fftsize = 1024*8;
			
			int fftsizeh = fftsize / 2;
			
			int fftsizeh1 = fftsize / 4; // 256
			int fftsizeh2 = fftsize / 8; // 128
			
			int readsize = 1024*2;
			int fftoffset = fftsize*2 - readsize;
			int seclen = (int)(samplerate / (readsize/2));
			int secounter = 0;
			
			FFT fft = new FFT(fftsize);
			double[] window = fft.wHanning();
			
			double[] buffer = new double[fftsize*2];
			
			
			//for (int yy = 0; yy <  ww-10; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}					
				
				for (int i = 0; i < fftoffset; i++) {
					buffer[i] = buffer[i + readsize]; 
				}
				int len = audiostream.replace(buffer, fftoffset, fftsize*2);						
				if(len == -1) break;
				
				//len += fftsize;
				
				//len /= 2;
				
				double[] noiseL = new double[fftsize];									
				double[] noiseR = new double[fftsize];									
				// Fill FFT Buffer
				for (int i = 0; i < fftsize; i++) {		
					
					noiseL[i] = buffer[i*2] * window[i];
					//noiseL[2*i + 1] = 0;                  
					
					noiseR[i] = buffer[i*2 + 1] * window[i];
					//noiseR[2*i + 1] = 0;                   
				}
				
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				fft.calcReal(noiseL, -1);
				fft.calcReal(noiseR, -1);
				
				//double fftstart = 220;
				//double fftscale = -10;
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				double[] fmagn1 = new double[513];
				double[] fmagn2 = new double[513];
				double[] fmagn3 = new double[513];
				
				for (int i = 0; i < fftsizeh; i++) {
					
					//noiseL[2*i] = avgL[2*i]*0.5 + noiseL[2*i]*0.5;
					//noiseL[2*i + 1] = avgL[2*i + 1]*0.5 + noiseL[2*i +1]*0.5;
					//noiseR[2*i] = avgR[2*i]*0.5 + noiseR[2*i]*0.5;
					//noiseR[2*i + 1] = avgR[2*i + 1]*0.5 + noiseR[2*i + 1]*0.5;
					
					//avgL[2*i] = noiseL[2*i];
					//avgL[2*i + 1] = noiseL[2*i + 1];
					//avgR[2*i] = noiseR[2*i];
					//avgR[2*i + 1] = noiseR[2*i + 1];
					
					double L_real = noiseL[2*i];
					double L_imag = noiseL[2*i+1];
					double L_magn = 2.*Math.sqrt(L_real*L_real + L_imag*L_imag);
					double L_phase = Math.atan2(L_imag,L_real);							
					
					double R_real = noiseR[2*i];
					double R_imag = noiseR[2*i+1];
					double R_magn = 2.*Math.sqrt(R_real*R_real + R_imag*R_imag);
					double R_phase = Math.atan2(R_imag,R_real);							
					
					double magn = (L_magn + R_magn)/2;
					
					//double T_magn = L_magn + R_magn;
					//double pan = R_magn / T_magn;
					
					double phase_diff = Math.abs(L_phase - R_phase) % (2*Math.PI);
					if(phase_diff > Math.PI) phase_diff = (2*Math.PI) - phase_diff;
					phase_diff /= Math.PI;
					
					//int pos = (int)(pan*512);
					
					int phase_size = 512;
					int i_phase_diff = (int)(phase_diff * phase_size);
					int posa = i_phase_diff;
					
					//int posa = pos;
					//int posb = pos;
					
					if(posa > 512) posa = 512;
					if(posa < 0) posa = 0;
					/*if(posb > 512) posb = 512;
					 if(posb < 0) posb = 0;*/
					
					double ii = i;
					
					if(i < fftsizeh2) // 0 .. 6 khz
					{
						double magna = magn * Math.pow(10, -5*(1 - (fftsizeh2 - (ii)) / fftsizeh2)); 
						fmagn1[posa] += magna*2;
						
						magna = magn * Math.pow(10, -5*(1 - (ii) / fftsizeh2)); 
						fmagn2[posa] += magna*2;
						
					}
					else
						if(i < fftsizeh1)  // 6 ..12 khz
						{
							
							double magna = magn * Math.pow(10, -5*(1 - (fftsizeh2 - (ii - fftsizeh2)) / fftsizeh2)); 
							fmagn2[posa] += magna*2;
							
							magna = magn * Math.pow(10, -5*(1 - (ii - fftsizeh2) / fftsizeh2)); 
							fmagn3[posa] += magna*2;
						}
						else								
							//if(i < 384) // 12 .. 24 khz
						{
							fmagn3[posa] += magn*2;
						}
					
					
					
					
					
					
				}
				
				for (int i = 0; i < 513; i++) {
					
					
					double logman1 = Math.log10(fmagn1[i]/fftsizeh)*2;							
					float colormag1 = (float)((logman1+9) / 7.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					double logman2 = Math.log10(fmagn2[i]/fftsizeh)*2;							
					float colormag2 = (float)((logman2+9) / 7.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					double logman3 = Math.log10(fmagn3[i]/fftsizeh)*2;							
					float colormag3 = (float)((logman3+9) / 7.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					
					/*
					 double logman = Math.log10(fmagn[i]/512.0)*2;							
					 
					 float colormag3 = (float)((logman+7) / 7.0);
					 if(colormag3 > 1.0) colormag3 = 1.0f;
					 else
					 if(colormag3 < 0.0) colormag3 = 0.0f;
					 
					 float colormag2 = (float)((logman+9) / 7.0);
					 if(colormag2 > 1.0) colormag2 = 1.0f;
					 else
					 if(colormag2 < 0.0) colormag2 = 0.0f;
					 
					 float colormag1 = (float)((logman+8) / 7.0);
					 if(colormag1 > 1.0) colormag1 = 1.0f;
					 else
					 if(colormag1 < 0.0) colormag1 = 0.0f;
					 */
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,i, yy, i);
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				panel.repaint();
				//if(!process_running) return;
				
			}
			
			/*
			 g2.setColor(Color.GREEN.darker());
			 for (int i = 0; i < 24; i++) {
			 int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
			 
			 g2.drawLine(0, y, panel.getWidth(), y);
			 }*/
			
			g2.setColor(Color.GREEN);
			g2.drawLine(panel.getWidth()-10, 0, panel.getWidth(), 0);
			g2.drawLine(panel.getWidth()-10, 256, panel.getWidth(), 256);
			g2.drawLine(panel.getWidth()-10, 512, panel.getWidth(), 512);
			
			panel.repaint();
			audiostream.close();						
			interpreter.close();
					
			
			process_running = false;
			hideFFTPanel();
			
			//System.out.println("LO = " + lo);
			//System.out.println("HI = " + hi);
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();			
		
	}
	
	
	public void processIntensityImage()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			
			
			int fftsize = 1024*8;
			
			int fftsizeh = fftsize / 2;
			
			int fftsizeh1 = fftsize / 4; // 256
			int fftsizeh2 = fftsize / 8; // 128
			
			int readsize = 1024;
			int fftoffset = fftsize - readsize;
			int seclen = (int)(samplerate / (readsize));
			int secounter = 0;
			
			FFT fft = new FFT(fftsize);
			double[] window = fft.wHanning();
			
			double[] buffer = new double[fftsize];
			
			
			//for (int yy = 0; yy <  ww-50; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}				
				
				for (int i = 0; i < fftoffset; i++) {
					buffer[i] = buffer[i + readsize]; 
				}
				int len = audiostream.replace(buffer, fftoffset, fftsize);						
				if(len == -1) break;
				
				//len += fftsize;
				
				//len /= 2;
				
				double[] noiseL = new double[fftsize];									
				// Fill FFT Buffer
				for (int i = 0; i < fftsize; i++) {		
					
					noiseL[i] = buffer[i] * window[i];
					//noiseL[2*i + 1] = 0;                  
					
				}
				
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				fft.calcReal(noiseL, -1);
				
				//double fftstart = 220;
				//double fftscale = -10;
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				double[] fmagn1 = new double[513];
				double[] fmagn2 = new double[513];
				double[] fmagn3 = new double[513];
				
				for (int i = 0; i < fftsizeh; i++) {
					
					//noiseL[2*i] = avgL[2*i]*0.5 + noiseL[2*i]*0.5;
					//noiseL[2*i + 1] = avgL[2*i + 1]*0.5 + noiseL[2*i +1]*0.5;
					//noiseR[2*i] = avgR[2*i]*0.5 + noiseR[2*i]*0.5;
					//noiseR[2*i + 1] = avgR[2*i + 1]*0.5 + noiseR[2*i + 1]*0.5;
					
					//avgL[2*i] = noiseL[2*i];
					//avgL[2*i + 1] = noiseL[2*i + 1];
					//avgR[2*i] = noiseR[2*i];
					//avgR[2*i + 1] = noiseR[2*i + 1];
					
					double L_real = noiseL[2*i];
					double L_imag = noiseL[2*i+1];
					double L_magn = 2.*Math.sqrt(L_real*L_real + L_imag*L_imag);
					//double L_phase = Math.atan2(L_imag,L_real);							
					/*
					 double R_real = noiseR[2*i];
					 double R_imag = noiseR[2*i+1];
					 double R_magn = 2.*Math.sqrt(R_real*R_real + R_imag*R_imag);
					 double R_phase = Math.atan2(R_imag,R_real);*/							
					
					double magn = (L_magn); // + R_magn)/2;
					
					//double T_magn = L_magn + R_magn;
					//double pan = R_magn / T_magn;
					
					//double phase_diff = Math.abs(L_phase - R_phase) % (2*Math.PI);
					//if(phase_diff > Math.PI) phase_diff = (2*Math.PI) - phase_diff;
					//phase_diff /= Math.PI;
					
					//int pos = (int)(pan*512);
					
					//int phase_size;
					//if(pos > 256)
					//{
//					phase_size = 512 - pos;
					//}
//					else
//					{
//					phase_size = pos;
					//}
					//int i_phase_diff = (int)(phase_diff * phase_size);
					//if(!phase_included)
					//i_phase_diff = 0;
					
					double logman1 = Math.log10(magn/fftsizeh)*2;							
					//int posa = (int)(50 - ((logman1) / 20.0)*512.0);
					int posa = (int)(50 - (logman1) * 20);
					
					//double logman = Math.log10(magn/2048.0)*2;							
					//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					//int posa =  //pos - i_phase_diff;
					//int posb = pos + i_phase_diff;
					//int posa = pos;
					//int posb = pos;
					
					if(posa > 512) posa = 512;
					if(posa < 0) posa = 0;
					
					magn = 0.1; //fftsizeh/500;
					
					double ii = i;
					
					if(i < fftsizeh2) // 0 .. 6 khz
					{
						double magna = magn * (fftsizeh2 - (ii)) / fftsizeh2; 
						fmagn1[posa] += magna*2;
						
						magna = magn * (ii) / fftsizeh2; 
						fmagn2[posa] += magna*2;
						
					}
					else
						if(i < fftsizeh1)  // 6 ..12 khz
						{
							
							double magna = magn * (fftsizeh2 - (ii - fftsizeh2)) / fftsizeh2; 
							fmagn2[posa] += magna*2;
							
							magna = magn * (ii - fftsizeh2) / fftsizeh2; 
							fmagn3[posa] += magna*2;
						}
						else								
							//if(i < 384) // 12 .. 24 khz
						{
							fmagn3[posa] += magn*2;
						}
					
					
					
					
					
					
				}
				
				for (int i = 0; i < 513; i++) {
					
					
					double logman1 = fmagn1[i]; //Math.log10(fmagn1[i]/fftsizeh)*2;							
					float colormag1 = (float)logman1;//((logman1+9) / 7.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					double logman2 = fmagn2[i]; //Math.log10(fmagn2[i]/fftsizeh)*2;							
					float colormag2 = (float)logman2;//((logman2+9) / 7.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					double logman3 = fmagn3[i]; //Math.log10(fmagn3[i]/fftsizeh)*2;							
					float colormag3 = (float)logman3;//((logman3+9) / 7.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					
					/*
					 double logman = Math.log10(fmagn[i]/512.0)*2;							
					 
					 float colormag3 = (float)((logman+7) / 7.0);
					 if(colormag3 > 1.0) colormag3 = 1.0f;
					 else
					 if(colormag3 < 0.0) colormag3 = 0.0f;
					 
					 float colormag2 = (float)((logman+9) / 7.0);
					 if(colormag2 > 1.0) colormag2 = 1.0f;
					 else
					 if(colormag2 < 0.0) colormag2 = 0.0f;
					 
					 float colormag1 = (float)((logman+8) / 7.0);
					 if(colormag1 > 1.0) colormag1 = 1.0f;
					 else
					 if(colormag1 < 0.0) colormag1 = 0.0f;
					 */
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,i, yy, i);
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				panel.repaint();
				//if(!process_running) return;
				
			}
			
			/*
			 g2.setColor(Color.GREEN.darker());
			 for (int i = 0; i < 24; i++) {
			 int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
			 
			 g2.drawLine(0, y, panel.getWidth(), y);
			 }*/
			
			g2.setColor(Color.GREEN);
			//g2.drawLine(panel.getWidth()-10, 50, panel.getWidth(), 50);
			//g2.drawLine(panel.getWidth()-10, 50 + 40, panel.getWidth(), 50 + 40);
			
			for (int i = 0; i < 22; i+=1) {
				
				g2.drawString((-i*10) + "dB", panel.getWidth()-40, 50 + 20*i - 4);
				g2.drawLine(panel.getWidth()-50, 50 + 20*i, panel.getWidth(), 50 + 20*i);			
				//g2.drawLine(0, s, w - 40, s);
			}							
			
			//g2.drawLine(panel.getWidth()-10, 256, panel.getWidth(), 256);
			//g2.drawLine(panel.getWidth()-10, 512, panel.getWidth(), 512);
			
			panel.repaint();
			audiostream.close();			
			interpreter.close();
					
			
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();			
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
	}
	
	
	
	public void processCepstrumGraph()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			int fftlen = 1024*4;
			double[] buffer = new double[fftlen];
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			int fftlenh = fftlen / 2;
			
			int seclen = (int)(samplerate / 1024);
			int secounter = 0;
			/*
			 double[] freqarray = new double[512];
			 double[] freqmagn = new double[512];
			 for (int i = 0; i < 512; i++) {
			 double note = ((double)i) / 16.0;
			 freqarray[i] = Math.pow(2.0,  (note - 36.0) / 12.0 ) * 440;
			 freqmagn[i]  = 0; 
			 }
			 */
			
			FFT fft = new FFT(fftlen);
			double[] window = fft.wHanning();
			
			int readsize = 1024;
			int fftoffset = fftlen - readsize;
			
			//for (int yy = 0; yy <  ww; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}
				
				for (int i = 0; i < fftoffset; i++) {
					buffer[i] = buffer[i + readsize]; 
				}
				int len = audiostream.replace(buffer, fftoffset, fftlen);						
				
				//int len = audiostream.replace(buffer, 0, fftlen);
				if(len == -1) break;
				
				
				double[] noise = new double[fftlen*2];
				double[] noise2 = new double[fftlen*2];
				// Fill FFT Buffer
				for (int i = 0; i < fftlen; i++) {							
					noise[2*i] = buffer[i];
					noise[2*i + 1] = 0;                   
				}
				/*
				 for (int i = len; i < fftlen; i++) {							
				 noise[2*i] = 0;
				 noise[2*i + 1] = 0;                   
				 }
				 */
				
				
				
				// Perform windowing
				for (int i = 0; i < fftlen; i++) {
					noise[i*2] *= window[i];
				}						
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				fft.calc(noise, -1);
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;	
				
				/*	
				 for (int i = 0; i < 512; i++) {
				 }*/
				
				
				for (int i = 0; i < fftlen; i++) {
					//int j = (i + (fftlenh)) % fftlen;
					double real = noise[2*i] / fftlen; 
					double imag = noise[2*i+1] / fftlen;
					
					noise2[i*2]     = Math.log( Math.sqrt(real*real + imag*imag) );
					
					noise2[i*2 + 1] = 0; // Do not perform log on imagenary part !!!			
					//noise2[i*2 + 1] = Math.atan2(imag, real);			
				}
				
				// Unwrap Phase 
				/*
				 double lastimag = noise[1];
				 double adds = 0;
				 for (int i = 0; i < fftlen; i++) {
				 double imag = noise[2*i+1] + adds;
				 double diff = imag - lastimag;
				 if(diff > Math.PI)
				 {
				 imag -= Math.PI * 2;
				 adds -= Math.PI * 2;
				 }
				 else
				 if(diff < -Math.PI)
				 {
				 imag += Math.PI * 2;
				 adds += Math.PI * 2;
				 }
				 noise2[i*2 + 1] = 0; //imag;
				 lastimag = imag;
				 }		
				 */
				
				// Perform hi-pass filtering on the real part !!!
				// single pole hi pass filter
				
				/*
				 double theta = 0.24 * 2*Math.PI; // 0 < theta < Pi/2
				 double p = (1+2*Math.cos(theta)) - Math.sqrt((1+2*Math.cos(theta))*(1+2*Math.cos(theta)) - 1);
				 
				 double lastv = 0;
				 for (int i = 0; i < fftlen; i++) {
				 double curv = noise2[i*2];			
				 noise2[i*2] = 1/(1+p) * curv - p/(1+p) * lastv;
				 lastv = curv;			
				 }		*/
				
				// Perform diff hi pass
				/*
				 double lastv = 0;
				 for (int i = 0; i < fftlen; i++) {
				 double curv = noise2[i*2];			
				 noise2[i*2] = curv - lastv;
				 lastv = curv;			
				 }					
				 */
				
				// Perform windowing		
				for (int i = 0; i < fftlen; i++) {
					int j = (i + (fftlenh)) % fftlen;
					noise2[i*2] *= window[j];
				}						
				
				
				fft.calc(noise2, -1);
				/*	
				 for (int i = 0; i < fftlen; i++) {
				 double real = noise2[2*i];
				 double imag = noise2[2*i+1];
				 double magn = 2.*Math.sqrt(real*real + imag*imag);
				 noise[i*2]     = magn / fftlen;
				 noise[i*2 + 1] = 0;
				 }		
				 
				 fft.calc(noise, -1);
				 */
				
				
				for (int i = 0; i < 512; i++) {
					
					//int bucket = i*4;     // fftlenh
					double real = noise2[2*(512 - i)];   // H?fum bara ?huga ? j?kv??um real t?lum !!!
					double imag = noise2[2*(512 - i)+1]; // Engar uppl?singar ? imag partinum
					
					/* compute magnitude and phase */
					double magn = 2.*Math.sqrt(real*real + imag*imag);
					//double magn = 2.0*real;
					//2.*real; //
					//if(magn < 0) magn = 0;
					//magn = Math.log(magn);
					//double phase = Math.atan2(imag,real);							
					
					
					//int x = (int)(i * w / 512.0);							
					double logman = 1 + (Math.log10(magn/512.0)*2) / 2;			
					//logman = (logman) * 2;
					
					//double logmanR = -2.*real / 16;
					//if(logmanR < 0) logmanR = 0;
					
					//double logman = imag * 1000000000000000.0; //magn / 16;
					//double logman = magn / 16;
					
					
					//if(i == 0) { lx = x; ly = y; }
					//g2.drawLine(lx,ly,x,y);
					//lx = x;
					//ly = y;
					
					// Green - Yellow - Red
					
					//if(logman > hi) hi = logman;
					//if(logman < lo) lo = logman;
					
					
					/*
					 float colormag3 = (float)(logmanR / 4.0);
					 if(colormag3 > 1.0) colormag3 = 1.0f;
					 else
					 if(colormag3 < 0.0) colormag3 = 0.0f;
					 
					 if(logmanR == 0)
					 {
					 colormag3 = (float)(logman / 32.0);
					 if(colormag3 > 1.0) colormag3 = 1.0f;
					 else
					 if(colormag3 < 0.0) colormag3 = 0.0f;				
					 }*/
					
					
					float colormag3 = (float)((logman));
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)((logman / 2.0));
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					float colormag1 = (float)(logman / 4.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,512 - i, yy, 512 - i);
					
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			
			g2.setColor(Color.GREEN.darker());
			
			for (int i = 12; i <= 60; i++) {
				
				double f = Math.pow(2.0, (i - 45.0) / 12.0) * 440.0; 
				int y = (int)(44100.0 / f);
				g2.drawLine(panel.getWidth() - 30, y, panel.getWidth(), y);
				if(i % 12 == 0)
					g2.drawLine(0, y, panel.getWidth(), y);
				
				if(i % 12 == 0)
				{
					g2.setColor(Color.WHITE);
					g2.drawString(RasmusUtil.encodeMusicNote(i) + " = " + Math.round(f) +" Hz", panel.getWidth() - 130, y - 2);
					g2.setColor(Color.GREEN.darker());
				}
				
			}
			
			
			g2.setColor(Color.WHITE);
			
			for (int i = 12; i <= 48; i++) {
				
				double f = Math.pow(2.0, (i - 45.0) / 12.0) * 440.0; 
				int y = (int)(44100.0 / f);
				
				if(i % 2 == 0)
					g2.drawString(RasmusUtil.encodeMusicNote(i), panel.getWidth() - 20, y - 2);
				if(i % 2 == 1)
					g2.drawString(RasmusUtil.encodeMusicNote(i), panel.getWidth() - 40, y - 2);
				
			}
			
			
			//for (int i = 0; i < 24; i++) {
			//	int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
			//	
			//	g2.drawLine(panel.getWidth() - 10, y, panel.getWidth(), y);
			//}
			
			/*
			 g2.setColor(Color.GREEN.darker());
			 for (int i = 0; i < 24; i++) {
			 int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
			 
			 g2.drawLine(0, y, panel.getWidth(), y);
			 } */
			
			panel.repaint();
			audiostream.close();	
			interpreter.close();
			
			
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	
	public void processHarmonicProductSpectrumGraph()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			int fftlen = 1024;
			int fftlen2 = fftlen / 2;
			
			double[] buffer = new double[fftlen];
			
			
			FFT fft = new FFT(fftlen);
			double[] window = fft.wHanning();
			
			int readsize = 1024;
			int fftoffset = fftlen - readsize;
			
			int seclen = (int)(samplerate / readsize);
			int secounter = 0;
			
			//for (int yy = 0; yy <  ww; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}		
				
				for (int i = 0; i < fftoffset; i++) {
					buffer[i] = buffer[i + readsize]; 
				}
				int len = audiostream.replace(buffer, fftoffset, fftlen);						
				if(len == -1) break;
				
				
				double[] noise = new double[fftlen*2];									
				// Fill FFT Buffer
				for (int i = 0; i < fftlen; i++) {							
					noise[i] = buffer[i];
				}
				/*
				 for (int i = len; i < fftlen; i++) {							
				 noise[2*i] = 0;
				 noise[2*i + 1] = 0;                   
				 }*/
				
				// Perform windowing
				for (int i = 0; i < fftlen; i++) {
					noise[i] *= window[i];
				}						
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				fft.calcReal(noise, -1);
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				// Calculate Magnutade part!!!
				for (int i = 0; i < fftlen2; i++) {
					double real = noise[2*i];
					double imag = noise[2*i+1];			
					noise[2*i] =  2.*Math.sqrt(real*real + imag*imag);
					noise[2*i + 1] = 1;
				}
				for (int i = fftlen2; i < fftlen; i++) {
					noise[2*i] = 0;
					noise[2*i + 1] = 1;
				}
				
				// Perform HPS
				for (int h = 1; h < 4; h++) {
					
					for (int i = 0; i < fftlen2 / h; i++) {
						
						
						int ii = i * h;
						double magn = 0;
						for (int j = 0; j < h; j++) {
							magn += noise[2*(ii + j)];
						}
						
						noise[2*i + 1] *= magn ;
					}			
					
				}
				
				for (int i = 0; i < 512; i++) {
					
					//double real = noise[2*i];
					//double imag = noise[2*i+1];
					
					/* compute magnitude and phase */
					//double magn = 2.*Math.sqrt(real*real + imag*imag);
					//double phase = Math.atan2(imag,real);
					
					double magn = 1 + Math.log(noise[2*i + 1]);
					
					//int x = (int)(i * w / 512.0);							
					double logman = magn;							
					//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					//if(i == 0) { lx = x; ly = y; }
					//g2.drawLine(lx,ly,x,y);
					//lx = x;
					//ly = y;
					
					// Green - Yellow - Red
					
					//if(logman > hi) hi = logman;
					//if(logman < lo) lo = logman;
					
					
					float colormag3 = (float)(logman / 4.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(logman / 8.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					float colormag1 = (float)(logman / 16.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,512 - i, yy, 512 - i);
					
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			
			g2.setColor(Color.GREEN.darker());
			for (int i = 0; i < 24; i++) {
				int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
				
				g2.drawLine(0, y, panel.getWidth(), y);
			}
			
			panel.repaint();
			audiostream.close();	
			interpreter.close();
					
			
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	FFTConstantQ fftconstanq  = null;
	FFTConstantQ fftconstanq2  = null;
	
	public FFTConstantQ getFFTConstantQ()
	{
		double samplerate = 44100;
		// Fyrsta bylgjulengd er n?ta 24 ?.s. C1 er ? 30 hZ		
		if(fftconstanq == null) fftconstanq = new FFTConstantQ(samplerate, 440*Math.pow(2,((24-58)/12.0)), samplerate / 2, 24);
		return fftconstanq;
	}
	

	
	public FFTConstantQ getFFTConstantQ2()
	{
		double samplerate = 44100;
		// Fyrsta bylgjulengd er n?ta 24 ?.s. C1 er ? 30 hZ		
		if(fftconstanq2 == null) fftconstanq2 = new FFTConstantQ(samplerate, 440*Math.pow(2,((24-58)/12.0)), samplerate / 2, 48);
		return fftconstanq2;
	}	
	public void processConstantQSpectrumGraphHPS()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			FFTConstantQ fftconstanq = getFFTConstantQ2();
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
					
			
			
			
			int fftlen = fftconstanq.getFFTSize();
			
			double[] buffer = new double[fftlen];
			
			int seclen = (int)(samplerate / 1024);
			int secounter = 0;
			
			int outk = fftconstanq.getNumberOfOutputBands();
			
						
			
			//for (int yy = 0; yy <  ww; yy++) {												
			int yy = 0;
			double[] noise = new double[fftlen];
			double[] outnoise = new double[fftconstanq.getNumberOfOutputBands()*2];
			double[] outnoise2 = new double[fftconstanq.getNumberOfOutputBands()*2];
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}
				
				for (int i = 0; i < buffer.length - 1024; i++) {
					buffer[i] = buffer[i + 1024];
				}
				int len = audiostream.replace(buffer, buffer.length - 1024, buffer.length);
				for (int i = len; i < 1024; i++) {
					buffer[i + buffer.length - 1024] = 0;
				}
				//int len = audiostream.replace(buffer, 0, fftlen);
				if(len == -1) break;
				
				
					
				// Fill FFT Buffer
				for (int i = 0; i < fftlen; i++) {							
					noise[i] = buffer[i];
					//noise[2*i + 1] = 0;                   
				}
				/*
				for (int i = len; i < fftlen; i++) {							
					noise[2*i] = 0;
					noise[2*i + 1] = 0;                   
				}*/
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				//fft.calcReal(noise, -1);
				//fft.calcReal(noise, 1);
				
				fftconstanq.calc(noise, outnoise);
				
				
				double max = 0;
				double min = 0;
				for (int i = 0; i < fftconstanq.getNumberOfOutputBands(); i++) {
					
					double real = outnoise[2*(i)];
					double imag = outnoise[2*(i)+1];
					
					/* compute magnitude and phase */
					outnoise[i] = 2.*Math.sqrt(real*real + imag*imag);
					
					if(i == 0)
					{
						min = max = outnoise[i];
					}
					{
						if(outnoise[i] > max) max = outnoise[i];
						if(outnoise[i] < min) min = outnoise[i];
					}
					
					//if(outnoise[i] < 0.01) outnoise[i] = 0;
					
				}						
				//System.out.println("range : " + min + " .. " + max);
				// Perform HPS in Spectrum Q, 2,3,4,5
				double startfreq = 440*Math.pow(2,((24-58)/12.0));
				double endfreq = samplerate / 2;
				//int outk = fftconstanq.getNumberOfOutputBands();
				
				for (int i = 0; i < fftconstanq.getNumberOfOutputBands(); i++) {
					outnoise2[i] = outnoise[i];
				}
				for (int h = 2; h < 5; h++) {
					double offset = outk * (Math.log(h)) / (Math.log(endfreq) - Math.log(startfreq));
					int offset_i = (int)offset;

					for (int i = 0; i < outk - offset_i; i++) {											
						outnoise2[i] *= outnoise2[i + offset_i]*100*h*h*h;						
					}								
				}							
								
				
				//int lx = 0;
				//int ly = 100;
				
				

				
				g2.setColor(Color.GREEN);
				

				
				for (int i = 0; i < fftconstanq.getNumberOfOutputBands(); i++) {
					
					//double real = outnoise[2*(i/2)];
					//double imag = outnoise[2*(i/2)+1];
					
					/* compute magnitude and phase */
					//double magn = 2.*Math.sqrt(real*real + imag*imag);
					double magn = outnoise[i];
					double magn2 = outnoise2[i];
					
					//int x = (int)(i * w / 512.0);							
					double logman = Math.log10(magn )*1.5; // /512.0
					double logman2 = Math.log10(magn2 )*1.5; // /512.0
					
					//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					//if(i == 0) { lx = x; ly = y; }
					//g2.drawLine(lx,ly,x,y);
					//lx = x;
					//ly = y;
					
					// Green - Yellow - Red
					
					//if(logman > hi) hi = logman;
					//if(logman < lo) lo = logman;
					
					
					float colormag3 = (float)(1 + logman / 8.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(1 + logman2 / 12.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					colormag3 *= colormag2;
					if(colormag3 > 1.0) colormag3 = 1.0f;
					colormag2 = colormag3;
					//float colormag1 = colormag2;
					
					float colormag1 = (float)(1 + logman / 8.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					
					g2.setColor(new Color(colormag1, colormag1, colormag1));
					g2.drawLine(yy,512 - i, yy, 512 - i);
					
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			
			g2.setColor(Color.GREEN.darker());
			for (int i = 0; i < 24; i++) {
				int y = 512 - i*24*2; //((int)((i * 2 * 1000.0 / samplerate) * 512.0));
				
				g2.drawLine(0, y, panel.getWidth(), y);
				double freq = 440.0*Math.pow(2.0,(((i)*12)-58+24)/12.0);
								
				g2.drawString("c" + (1 + i) + " = " + (int)freq + " hz", panel.getWidth()-100, y-5);
			}
			
			panel.repaint();		
			audiostream.close();
			interpreter.close();
					
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	public void processConstantQSpectrumGraph()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			FFTConstantQ fftconstanq;
			if(!q_hires)
				fftconstanq = getFFTConstantQ();
			else
				fftconstanq = getFFTConstantQ2();
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
					
			
			
			
			int fftlen = fftconstanq.getFFTSize();
			
			double[] buffer = new double[fftlen];
			
			int seclen = (int)(samplerate / 1024);
			int secounter = 0;
			

			

			//for (int yy = 0; yy <  ww; yy++) {												
			int yy = 0;
			double[] noise = new double[fftlen];
			double[] outnoise = new double[fftconstanq.getNumberOfOutputBands()*2];
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}
				
				for (int i = 0; i < buffer.length - 1024; i++) {
					buffer[i] = buffer[i + 1024];
				}
				int len = audiostream.replace(buffer, buffer.length - 1024, buffer.length);
				for (int i = len; i < 1024; i++) {
					buffer[i + buffer.length - 1024] = 0;
				}
				//int len = audiostream.replace(buffer, 0, fftlen);
				if(len == -1) break;
				
				
					
				// Fill FFT Buffer
				for (int i = 0; i < fftlen; i++) {							
					noise[i] = buffer[i];
					//noise[2*i + 1] = 0;                   
				}
				/*
				for (int i = len; i < fftlen; i++) {							
					noise[2*i] = 0;
					noise[2*i + 1] = 0;                   
				}*/
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				//fft.calcReal(noise, -1);
				//fft.calcReal(noise, 1);
				
				fftconstanq.calc(noise, outnoise);
				
				
				g2.setColor(Color.GREEN);
				
			
				//int lx = 0;
				//int ly = 100;
				
				if(q_hires)
				{
					
					for (int i = 0; i < fftconstanq.getNumberOfOutputBands(); i++) {
						
						double real = outnoise[2*(i)];
						double imag = outnoise[2*(i)+1];
						
						/* compute magnitude and phase */
						double magn = 2.*Math.sqrt(real*real + imag*imag);
			
						
						//int x = (int)(i * w / 512.0);							
						double logman = Math.log10(magn )*1.5; // /512.0
						
						//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
						
						
						//if(i == 0) { lx = x; ly = y; }
						//g2.drawLine(lx,ly,x,y);
						//lx = x;
						//ly = y;
						
						// Green - Yellow - Red
						
						//if(logman > hi) hi = logman;
						//if(logman < lo) lo = logman;
						
						
						float colormag3 = (float)(1 + logman / 16.0);
						if(colormag3 > 1.0) colormag3 = 1.0f;
						else
							if(colormag3 < 0.0) colormag3 = 0.0f;
						
						float colormag2 = (float)(1 + logman / 8.0);
						if(colormag2 > 1.0) colormag2 = 1.0f;
						else
							if(colormag2 < 0.0) colormag2 = 0.0f;
						
						float colormag1 = (float)(1 + logman / 4.0);
						if(colormag1 > 1.0) colormag1 = 1.0f;
						else
							if(colormag1 < 0.0) colormag1 = 0.0f;
						
						
						g2.setColor(new Color(colormag1, colormag2, colormag3));
						g2.drawLine(yy,512 - i, yy, 512 - i);
						
					}
					
				}
				else
				{
				
				for (int i = 0; i < fftconstanq.getNumberOfOutputBands()*2; i++) {
					
					double real = outnoise[2*(i/2)];
					double imag = outnoise[2*(i/2)+1];
					
					/* compute magnitude and phase */
					double magn = 2.*Math.sqrt(real*real + imag*imag);
		
					
					//int x = (int)(i * w / 512.0);							
					double logman = Math.log10(magn )*1.5; // /512.0
					
					//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					//if(i == 0) { lx = x; ly = y; }
					//g2.drawLine(lx,ly,x,y);
					//lx = x;
					//ly = y;
					
					// Green - Yellow - Red
					
					//if(logman > hi) hi = logman;
					//if(logman < lo) lo = logman;
					
					
					float colormag3 = (float)(1 + logman / 16.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(1 + logman / 8.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					float colormag1 = (float)(1 + logman / 4.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,512 - i, yy, 512 - i);
					
				}
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			
			g2.setColor(Color.GREEN.darker());
			for (int i = 0; i < 24; i++) {
				int y = 512 - i*24*2; //((int)((i * 2 * 1000.0 / samplerate) * 512.0));
				
				g2.drawLine(0, y, panel.getWidth(), y);
				double freq = 440.0*Math.pow(2.0,(((i)*12)-58+24)/12.0);
								
				g2.drawString("c" + (1 + i) + " = " + (int)freq + " hz", panel.getWidth()-100, y-5);
			}
			
			panel.repaint();		
			audiostream.close();
			interpreter.close();
					
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	public void processConstantQSpectrumHarmonicGraph()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			
			FFTConstantQ fftconstanq = getFFTConstantQ();
			Interpreter interpreter = new Interpreter();
			

			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
					
			
			
			
			int fftlen = fftconstanq.getFFTSize();
			
			int outk = fftconstanq.getNumberOfOutputBands();
			int harmonic_fftlen = (int)Math.pow(2, Math.ceil(Math.log(outk*2)/Math.log(2)));
			
			FFT harmonic_fft = new FFT(harmonic_fftlen);
			double[] harmonic_buffer = new double[harmonic_fftlen];
			double[] last_value = new double[harmonic_fftlen];
			
			double[] buffer = new double[fftlen];
			
			int seclen = (int)(samplerate / 1024);
			int secounter = 0;
			
			//for (int yy = 0; yy <  ww; yy++) {												
			int yy = 0;
			double[] noise = new double[fftlen];
			double[] outnoise = new double[fftconstanq.getNumberOfOutputBands()*2];
			
			// 1. Create Harmonic Pattern
			//double[] harmonic_pattern1 = new double[harmonic_fftlen];
			//{
				/*
			
				// Let's create idel -20dB Harmonic pattern
				RInterpreter interpreter2 = new RInterpreter();
				interpreter2.eval("f <- 200;"); // Fundamental frequency
				interpreter2.eval("n <- -20;");  // Harmonic damping
				for (int i = 0; i < 1; i++) {  // Harmonics
					interpreter2.eval("output <- gain(db(n*" + i +")) <- sin() <- clock(f*2*pi*" + (i + 1) + ");");
				}				
				R
				AudioStream haudiostream = AudioEvents.openStream(interpreter2.get("output"), audiosession);
				// Let's preread from stream
				for (int i = 0; i < fftlen*10; i+= 1024) {
					haudiostream.replace(noise, 0, 1024);								
				}
				
				// Read in the buffer
				haudiostream.replace(noise, 0, noise.length);
				haudiostream.close();
												
				// Calculate Constant Q Transform
				fftconstanq.calc(noise, outnoise); */
				/*
				for (int i = 0; i <  fftconstanq.getNumberOfOutputBands(); i++) { // fftconstanq.getNumberOfOutputBands()*2
					double real = outnoise[2*(i)];
					double imag = outnoise[2*(i)+1];
					harmonic_pattern1[i] = -2.*Math.sqrt(real*real + imag*imag);
				}*/	
				
				//harmonic_pattern1[0] = 1;
				//harmonic_pattern1[23] = 0.1 * 0.3; //Math.pow(10, 0*10.0/10.0);  // bins per octave
				//harmonic_pattern1[24] = 0.5 * 0.3; //Math.pow(10, 0*10.0/10.0);  // bins per octave
				//harmonic_pattern1[25] = 0.1 * 0.3; //Math.pow(10, 0*10.0/10.0);  // bins per octave

				//harmonic_pattern1[(int)(Math.log(3)/Math.log(2)*24.0)-1] = 0.1 * 0.1;
				//harmonic_pattern1[(int)(Math.log(3)/Math.log(2)*24.0)]   = 0.5 * 0.1;
				//harmonic_pattern1[(int)(Math.log(3)/Math.log(2)*24.0)+2] = 0.1 * 0.1;
				
				//harmonic_pattern1[(int)(Math.log(4)/Math.log(2)*24.0)] = Math.pow(10, -2*10.0/10.0);  // bins per octave
				//harmonic_pattern1[(int)(Math.log(5)/Math.log(2)*24.0)] = Math.pow(10, -3*10.0/10.0);  // bins per octave
				//harmonic_pattern1[(int)(Math.log(6)/Math.log(2)*24.0)] = Math.pow(10, -4*10.0/10.0);  // bins per octave
								
				

				//harmonic_pattern1[10] = 1.0;
				
				//harmonic_pattern1[30] = 1.0;
				
				// Perform FFT on harmonic pattern
				
				//harmonic_fft.calcReal(harmonic_pattern1, -1);
				/*
				interpreter2.close();
				R */
				
			//}
			
			Arrays.fill(noise , 0);
			Arrays.fill(outnoise, 0);
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}
				
				for (int i = 0; i < buffer.length - 1024; i++) {
					buffer[i] = buffer[i + 1024];
				}
				int len = audiostream.replace(buffer, buffer.length - 1024, buffer.length);
				for (int i = len; i < 1024; i++) {
					buffer[i + buffer.length - 1024] = 0;
				}
				//int len = audiostream.replace(buffer, 0, fftlen);
				if(len == -1) break;
				
				
					
				// Fill FFT Buffer
				for (int i = 0; i < fftlen; i++) {							
					noise[i] = buffer[i];
					//noise[2*i + 1] = 0;                   
				}
				/*
				for (int i = len; i < fftlen; i++) {							
					noise[2*i] = 0;
					noise[2*i + 1] = 0;                   
				}*/
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				//fft.calcReal(noise, -1);
				//fft.calcReal(noise, 1);
				
				fftconstanq.calc(noise, outnoise);
				
				Arrays.fill(harmonic_buffer, 0);
				int k = 0; //harmonic_fftlen / 2;
				for (int i = 0; i <  fftconstanq.getNumberOfOutputBands(); i++) { // fftconstanq.getNumberOfOutputBands()*2
					
					double real = outnoise[2*(i)];
					double imag = outnoise[2*(i)+1];
					
					/* compute magnitude and phase */
					harmonic_buffer[k] = 2.*Math.sqrt(real*real + imag*imag);
					k++;
				}
				
				harmonic_fft.calcReal(harmonic_buffer, -1);
									
				for (int i = 0; i <  harmonic_fftlen; i+=2) {
					double b_r = harmonic_buffer[i];
					double b_i = harmonic_buffer[i + 1];
					//double p_r = harmonic_pattern1[i];
					//double p_i = harmonic_pattern1[i + 1];
					
					//double t_r =  (b_r * p_r + b_i * p_i) / ( p_r * p_r + p_i * p_i );
					//double t_i =  (b_i * p_r - b_r * p_i) / ( p_r * p_r + p_i * p_i );

					//double t_r = b_r * p_r - b_i * p_i;
					//double t_i = b_r * p_i + b_i * p_r;
					
					double t_r = b_r ;
					double t_i = b_i ;					
					
					
					
					t_r *= 0.1 + 0.9 * ( (double)(i) / (double)harmonic_fftlen );
					t_i *= 0.1 + 0.9 * ( (double)(i) / (double)harmonic_fftlen );

					harmonic_buffer[i] = t_r;
					harmonic_buffer[i + 1] = t_i;
				}
				
				harmonic_fft.calcReal(harmonic_buffer, 1);
				
				
				g2.setColor(Color.GREEN);
				
				


				
				//int lx = 0;
				//int ly = 100;
				
				//int offset = harmonic_fftlen / 2;
				
				for (int i = 0; i < harmonic_buffer.length; i++) {
					harmonic_buffer[i] = last_value[i] * 0.6 + harmonic_buffer[i]*0.4; 
					last_value[i] = harmonic_buffer[i]; 
				}
				
				
				for (int i = 0; i < harmonic_buffer.length; i++) { // fftconstanq.getNumberOfOutputBands()*2
				//for (int i = 0; i < fftconstanq.getNumberOfOutputBands(); i++) { // fftconstanq.getNumberOfOutputBands()*2
				//for (int i = 0; i <  fftconstanq.getNumberOfOutputBands()*2; i++) { // fftconstanq.getNumberOfOutputBands()*2
				//for (int i = 0; i <  fftconstanq.getNumberOfOutputBands(); i++) { // fftconstanq.getNumberOfOutputBands()*2
					
					//double real = outnoise[2*(i)];
					//double imag = outnoise[2*(i)+1];
					//double magn =  2.*Math.sqrt(real*real + imag*imag);;					
					
					/* compute magnitude and phase */
					//harmonic_buffer[i] = 2.*Math.sqrt(real*real + imag*imag);
					
					
					
					//double magn = harmonic_buffer[i/2 + offset]/harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					double magn1 = harmonic_buffer[i/2] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					double magn2 = 0.4 * harmonic_buffer[(i/2 + 12*2)] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					double magn3 = 0.4 * harmonic_buffer[(i/2 + 19*2)] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					
					magn2 += 0.2 * harmonic_buffer[(i/2 + 12*2) -1] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					magn3 += 0.2 * harmonic_buffer[(i/2 + 19*2) -1] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					magn2 += 0.2 * harmonic_buffer[(i/2 + 12*2) +1] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					magn3 += 0.2 * harmonic_buffer[(i/2 + 19*2) +1] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
/*
					magn2 += 0.1 * harmonic_buffer[(i/2 + 12*2) -2] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					magn3 += 0.1 * harmonic_buffer[(i/2 + 19*2) -2] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					magn2 += 0.1 * harmonic_buffer[(i/2 + 12*2) +2] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					magn3 += 0.1 * harmonic_buffer[(i/2 + 19*2) +2] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
*/
					//int x = (int)(i * w / 512.0);							
					double logman1 = Math.log10(magn1 )*1.5; // /512.0
					double logman2 = Math.log10(magn2 )*1.5; // /512.0
					double logman3 = Math.log10(magn3 )*1.5; // /512.0
					
					//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					//if(i == 0) { lx = x; ly = y; }
					//g2.drawLine(lx,ly,x,y);
					//lx = x;
					//ly = y;
					
					// Green - Yellow - Red
					
					//if(logman > hi) hi = logman;
					//if(logman < lo) lo = logman;

					float colormag1 = (float)(1 + logman1 / 10.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;

					
					float colormag3 = (float)(1 + logman3 / 10.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(1 + logman2 / 10.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					
					colormag3 *= colormag2 * 2;
					colormag2 *= colormag2 * 2;
					
					colormag2 *= colormag1 * 2;
					colormag3 *= colormag1 * 2;
					colormag1 *= colormag1 * 2;
					
					if(colormag1 > 1.0) colormag1 = 1.0f;
					if(colormag2 > 1.0) colormag2 = 1.0f;
					if(colormag3 > 1.0) colormag3 = 1.0f;
										
					
					/*
					if((colormag2 > 0.2) && (colormag3 > 0.2))
					{
						colormag2 = colormag1;
						colormag3 = colormag1;
					}
					else
					{
						colormag2 = 0;
						colormag3 = 0;
					}*/
					/*
					colormag2 = colormag2 * colormag3 * 10f;
					
					
					colormag2 *= colormag1 * 2;				
					colormag3 = (float)(colormag3 * 0.1f + last_value[i/2] * 0.9f);
					last_value[i/2] = colormag3;
					if(colormag2 > 1.0) colormag2 = 1.0f;					
					
					colormag3 = colormag2;*/
					/*
					colormag1 = (colormag1 + colormag2 + colormag3) / 3.0f;
					if(colormag1 > 1.0) colormag1 = 1.0f;
					colormag2 = colormag1;
					colormag3 = colormag1; 
*/					
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,512 - i, yy, 512 - i);
					
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			
			g2.setColor(Color.GREEN.darker());
			for (int i = 0; i < 24; i++) {
				int y = 512 - i*24*2; //((int)((i * 2 * 1000.0 / samplerate) * 512.0));
				
				g2.drawLine(0, y, panel.getWidth(), y);
				double freq = 440.0*Math.pow(2.0,(((i)*12)-58+24)/12.0);
								
				g2.drawString("c" + (1 + i) + " = " + (int)freq + " hz", panel.getWidth()-100, y-5);
			}
			
			panel.repaint();		
			audiostream.close();
			interpreter.close();
					
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	
	
	public void processSpectrumGraph()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			int fftlen = 1024;
			
			double[] buffer = new double[fftlen];
			
			int seclen = (int)(samplerate / fftlen);
			int secounter = 0;
			
			FFT fft = new FFT(fftlen);
			double[] window = fft.wHanning();
			
			//for (int yy = 0; yy <  ww; yy++) {												
			int yy = 0;
			double[] noise = new double[fftlen*2];
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}
				int len = audiostream.replace(buffer, 0, fftlen);
				if(len == -1) break;
				
				
													
				// Fill FFT Buffer
				for (int i = 0; i < len; i++) {							
					noise[i] = buffer[i];
					//noise[i + 1] = 0;                   
				}
				for (int i = len; i < fftlen; i++) {							
					noise[i] = 0;
					//noise[i + 1] = 0;                   
				}
				
				// Perform windowing
				
				for (int i = 0; i < fftlen; i++) {
					noise[i] *= window[i];
				}						
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				//fft.calcReal(noise, -1);
				//fft.calcReal(noise, 1);
				fft.calcReal(noise, -1);
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				for (int i = 0; i < 512; i++) {
					
					double real = noise[2*i];
					double imag = noise[2*i+1];
					
					/* compute magnitude and phase */
					double magn = 2.*Math.sqrt(real*real + imag*imag);
					
					//int x = (int)(i * w / 512.0);							
					double logman = Math.log10(magn/512.0)*2;
					
					//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					//if(i == 0) { lx = x; ly = y; }
					//g2.drawLine(lx,ly,x,y);
					//lx = x;
					//ly = y;
					
					// Green - Yellow - Red
					
					//if(logman > hi) hi = logman;
					//if(logman < lo) lo = logman;
					
					
					float colormag3 = (float)(1 + logman / 16.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(1 + logman / 8.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					float colormag1 = (float)(1 + logman / 4.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,512 - i, yy, 512 - i);
					
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			
			g2.setColor(Color.GREEN.darker());
			for (int i = 0; i < 24; i++) {
				int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
				
				g2.drawLine(0, y, panel.getWidth(), y);
			}
			
			panel.repaint();		
			audiostream.close();
			interpreter.close();
					
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	
	public void processSpectrumGraphDFT()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			double[] buffer = new double[4096];
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			int seclen = (int)(samplerate / 1024);
			int secounter = 0;
			
			//FFT fft = new FFT(1024);
			LogDFT fft = new LogDFT(1024);
			
			//for (int yy = 0; yy <  ww; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}		
				
				int len = audiostream.replace(buffer, 0, 1024);
				if(len == -1) break;
				
				
				double[] noise = new double[1024*2];									
				// Fill FFT Buffer
				for (int i = 0; i < len; i++) {							
					noise[2*i] = buffer[i];
					noise[2*i + 1] = 0;                   
				}
				for (int i = len; i < 1024; i++) {							
					noise[2*i] = 0;
					noise[2*i + 1] = 0;                   
				}
				
				// Perform windowing
//				for (int i = 0; i < 1024; i++) {
//				noise[i*2] *= window[i];
//				}						
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				//fft.calc(noise, -1);
				
				fft.calcLOGDFT(noise);
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				for (int i = 0; i < 512; i++) {
					
					double real = noise[2*i];
					double imag = noise[2*i+1];
					
					/* compute magnitude and phase */
					double magn = 2.*Math.sqrt(real*real + imag*imag);
					//double magn = amp[i];
					//double phase = Math.atan2(imag,real);							
					
					//int x = (int)(i * w / 512.0);							
					double logman = Math.log10(magn/512.0)*2;							
					//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					//if(i == 0) { lx = x; ly = y; }
					//g2.drawLine(lx,ly,x,y);
					//lx = x;
					//ly = y;
					
					// Green - Yellow - Red
					
					//if(logman > hi) hi = logman;
					//if(logman < lo) lo = logman;
					
					
					float colormag3 = (float)(1 + logman / 16.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(1 + logman / 8.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					float colormag1 = (float)(1 + logman / 4.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,512 - i, yy, 512 - i);
					
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			
			g2.setColor(Color.GREEN.darker());
			for (int i = 0; i < 24; i++) {
				int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
				
				g2.drawLine(0, y, panel.getWidth(), y);
			}
			
			panel.repaint();
			audiostream.close();	
			interpreter.close();
							
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	
	public void processSpectrumGraphP()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			double[] buffer = new double[4096];
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			int seclen = (int)(samplerate / 1024);
			int secounter = 0;
			
			FFT fft = new FFT(1024);
			double[] window = fft.wHanning();
			
			//for (int yy = 0; yy <  ww; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}		
				
				int len = audiostream.replace(buffer, 0, 1024);
				if(len == -1) break;
				
				
				double[] noise = new double[1024];									
				// Fill FFT Buffer
				for (int i = 0; i < len; i++) {							
					noise[i] = buffer[i];
					//noise[2*i + 1] = 0;                   
				}
				for (int i = len; i < 1024; i++) {							
					noise[i] = 0;
					//noise[2*i + 1] = 0;                   
				}
				
				// Perform windowing
				for (int i = 0; i < 1024; i++) {
					noise[i] *= window[i];
				}						
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				fft.calcReal(noise, -1);
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				for (int i = 0; i < 512; i++) {
					double real = noise[2*i];
					double imag = noise[2*i+1];
					
					/* compute magnitude and phase */
					double magn = 2.*Math.sqrt(real*real + imag*imag);
					double phase = Math.atan2(imag,real);							
					
					//int x = (int)(i * w / 512.0);							
					double logman = Math.log10(magn/512.0)*2;							
					//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					//if(i == 0) { lx = x; ly = y; }
					//g2.drawLine(lx,ly,x,y);
					//lx = x;
					//ly = y;
					
					// Green - Yellow - Red
					
					//if(logman > hi) hi = logman;
					//if(logman < lo) lo = logman;
					
					
					
					float colormag1 = (float)(1 + logman / 16.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					double f_phase = (((phase / (Math.PI*2.)) % 1) + 1) % 1;
					float red = (float)(colormag1*f_phase);
					float green = (float)(colormag1*(1-f_phase));
					if(red < 0.0f) 
						red = 0.0f;
					if(red > 1.0f) 
						red = 1.0f;
					if(green < 0.0f) 
						green = 0.0f;
					if(green > 1.0f) 
						green = 1.0f;			
					g2.setColor(new Color(red, green, colormag1));
					g2.drawLine(yy,512 - i, yy, 512 - i);
					
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			
			g2.setColor(Color.GREEN.darker());
			for (int i = 0; i < 24; i++) {
				int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
				
				g2.drawLine(0, y, panel.getWidth(), y);
			}
			
			panel.repaint();
			audiostream.close();	
			interpreter.close();
							
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	
	public void processSpectrumGraph2()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			double[] buffer = new double[4096];
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession( samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			int seclen = (int)(samplerate / 1024);
			int secounter = 0;
			
			FFT fft = new FFT(1024);
			double[] window = fft.wHanning();
			
			//for (int yy = 0; yy <  ww; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}		
				
				int len = audiostream.replace(buffer, 0, 1024);
				if(len == -1) break;
				
				
				double[] noise = new double[1024*2];									
				// Fill FFT Buffer
				for (int i = 0; i < len; i++) {							
					noise[2*i] = buffer[i];
					noise[2*i + 1] = 0;                   
				}
				for (int i = len; i < 1024; i++) {							
					noise[2*i] = 0;
					noise[2*i + 1] = 0;                   
				}
				
				// Perform windowing
				for (int i = 0; i < 1024; i++) {
					noise[i*2] *= window[i];
				}						
				
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				fft.calc(noise, -1);
				fft.calc(noise, -1);
				fft.calc(noise, -1);
				
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				for (int i = 0; i < 512; i++) {
					
					double real = noise[2*i];
					double imag = noise[2*i+1];
					
					/* compute magnitude and phase */
					double magn = 2.*Math.sqrt(real*real + imag*imag);
					
					//int x = (int)(i * w / 512.0);							
					double logman = Math.log10(magn/512.0)*2;							
					//int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					//if(i == 0) { lx = x; ly = y; }
					//g2.drawLine(lx,ly,x,y);
					//lx = x;
					//ly = y;
					
					// Green - Yellow - Red
					
					//if(logman > hi) hi = logman;
					//if(logman < lo) lo = logman;
					
					
					float colormag3 = (float)(1 + logman / 16.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(1 + logman / 8.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					float colormag1 = (float)(1 + logman / 4.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;
					
					
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					g2.drawLine(yy,512 - i, yy, 512 - i);
					
				}
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			
			g2.setColor(Color.GREEN.darker());
			for (int i = 0; i < 24; i++) {
				int y = 512 - ((int)((i * 2 * 1000.0 / samplerate) * 512.0));
				
				g2.drawLine(0, y, panel.getWidth(), y);
			}
			
			panel.repaint();
			audiostream.close();	
			interpreter.close();
					
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	
	public void processSpectrumGraphLog()
	{
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession( samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
			
			int seclen = (int)(samplerate / 1024);
			int secounter = 0;
			
			FFT fft = new FFT(1024*4);
			FFT fft2 = new FFT(1024*8);
			FFT fft3 = new FFT(1024*16);
			double[] window = fft.wHanning();
			double[] window2 = fft2.wHanning();
			double[] window3 = fft3.wHanning();
			
			
			double[] noise  = new double[1024*4];	
			double[] noise2 = new double[1024*8];
			double[] noise3 = new double[1024*16];
			//double[] noise4 = new double[1024*2];
			
			
			double[] buffer = new double[1024*16];
			
			audiostream.replace(buffer, 1024, buffer.length);
			
			//for (int yy = 0; yy <  ww; yy++) {
			int yy = 0;
			while(process_running)
			{
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}		
				
				for (int i = 0; i < buffer.length - 1024; i++) {
					buffer[i] = buffer[i + 1024];
				}
				int len = audiostream.replace(buffer, buffer.length - 1024, buffer.length);
				if(len == -1) break;
				
				
				// Fill FFT Buffer #1
				for (int i = 0; i < len*4; i++) {							
					noise[i] = buffer[i+7000]*window[i];
					//noise[2*i + 1] = 0;                   
				}
				for (int i = len*4; i < 1024; i++) {							
					noise[i] = 0;
					//noise[2*i + 1] = 0;                   
				}		
				
				// Fill FFT Buffer #2
				for (int i = 0; i < len*8; i++) {			
					noise2[i] = buffer[i+7000]*window2[i];			
					//noise2[2*i + 1] = 0;                   
				}
				for (int i = len*8; i < 1024*4; i++) {							
					noise2[i] = 0;
					//noise2[2*i + 1] = 0;                   
				}			
				
				// Fill FFT Buffer #3
				for (int i = 0; i < len*16; i++) {			
					noise3[i] = buffer[i]*window3[i];			
					//noise3[2*i + 1] = 0;                   
				}
				for (int i = len*16; i < 1024*16; i++) {							
					noise3[i] = 0;
					//noise3[2*i + 1] = 0;                   
				}					
				
				fft.calcReal(noise, -1);
				fft2.calcReal(noise2, -1);		
				fft3.calcReal(noise3, -1);		
				
				g2.setColor(Color.GREEN);
				
				
				//int lx = 0;
				//int ly = 100;
				
				for (int i = 32*4; i < 512*4; i++) {		
					double real = noise[2*i];
					double imag = noise[2*i+1];
					double magn = 2.*Math.sqrt(real*real + imag*imag);
					double logman = Math.log10(magn/512.0)*2;							
					
					float colormag3 = (float)(1 + logman / 16.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(1 + logman / 8.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					float colormag1 = (float)(1 + logman / 4.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;													
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					
					if(i > 1)
					{
						double ii = i / 4.0;
						double ii2 = (i + 1) / 4.0;				
						int xx = (int)(- 60*(Math.log(ii) - Math.log(512))/Math.log(2)); 
						int xx2 = (int)(- 60*(Math.log(ii2) - Math.log(512))/Math.log(2)); 
						g2.drawLine(yy,xx, yy, xx2);
					}			
				}
				
				
				
				for (int i = 0; i < 256; i++) {
					double real = noise2[2*i];
					double imag = noise2[2*i+1];
					double magn = 2.*Math.sqrt(real*real + imag*imag);
					double logman = Math.log10(magn/512.0)*2;							
					
					float colormag3 = (float)(1 + logman / 16.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(1 + logman / 8.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					float colormag1 = (float)(1 + logman / 4.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;													
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					
					if(i > 1)
					{
						double ii = i / 8.0;
						double ii2 = (i + 1) / 8.0;
						int xx = (int)(- 60*(Math.log(ii) - Math.log(512))/Math.log(2)); 
						int xx2 = (int)(- 60*(Math.log(ii2) - Math.log(512))/Math.log(2)); 
						g2.drawLine(yy,xx, yy, xx2);
					}			
				}	
				
				
				for (int i = 0; i < 128; i++) {
					double real = noise3[2*i];
					double imag = noise3[2*i+1];
					double magn = 2.*Math.sqrt(real*real + imag*imag);
					double logman = Math.log10(magn/512.0)*2;							
					
					float colormag3 = (float)(1 + logman / 16.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					float colormag2 = (float)(1 + logman / 8.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;
					
					float colormag1 = (float)(1 + logman / 4.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;													
					g2.setColor(new Color(colormag1, colormag2, colormag3));
					
					if(i > 1)
					{
						double ii = i / 16.0;
						double ii2 = (i + 1) / 16.0;
						int xx = (int)(- 60*(Math.log(ii) - Math.log(512))/Math.log(2)); 
						int xx2 = (int)(- 60*(Math.log(ii2) - Math.log(512))/Math.log(2)); 
						g2.drawLine(yy,xx, yy, xx2);
					}			
				}			
				
				
				
				if(secounter == 0)
				{
					secounter = seclen;
					g2.setColor(Color.GREEN);
					g2.drawLine(yy, 512, yy, panel.getHeight());
				}
				secounter--;
				
				panel.repaint();
				//if(!process_running) return;
				
				
			}
			/*
			 g2.setColor(Color.GREEN.darker());
			 for (int i = -36; i < 100; i++) {
			 double y = (((440 * Math.pow(2.0, (i + 3)/12.0) / samplerate) * 512.0));
			 int yy = (int)(- 60*(Math.log(y) - Math.log(512))/Math.log(2)); 			
			 g2.drawLine(0, yy, panel.getWidth(), yy);
			 }*/
			
			panel.repaint();
			audiostream.close();	
			interpreter.close();
					
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		//System.out.println("LO = " + lo);
		//System.out.println("HI = " + hi);
		
		
	}
	
	
	public void processFrequencyResponse()
	{
		
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			
			Graphics2D g2 = offImg.createGraphics();
			//g2.drawLine(0, 0, 100, 100);
			// g2.drawString("FFT Testing 1 2 3", 50, 50);
			double samplerate = 44100;
			
			
			double[] buffer = new double[4096];
			
			Interpreter interpreter = new Interpreter();
			//String mp3file = "E:\\Multimedia\\My EMusic\\Various Artists - Artemis Records\\Hollywood Motel\\Various Artists - Artemis Records_Hollywood Motel _06_Russ And The Shrink.mp3";
			//String script = "output <- PitchShift(2) <- File(\"" + mp3file + "\");";
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			/*
			 for (int i = 0; i < (counter); i++) {							
			 audiostream.skip(4800);
			 }
			 counter++; */
			
			FFT fft = new FFT(4096);
			// Make 4096 samples of noise					
			double[] noise = buffer; //new double[4096*2];
			double[] window = fft.wHanning();
			
			while(process_running)
			{
				
				
				
				int len = audiostream.replace(buffer, 0, 4096);
				if(len == -1)
				{
					break;
				}
				
				g2.setColor(Color.BLACK);
				g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
				g2.setColor(Color.RED);
				
				
				
				

				
				// Add Noise
				/*
				 Random random = new  Random();
				 for (int i = 0; i < 4096; i++) {
				 noise[2*i]     = 0; //0.0000001 * (random.nextDouble()-0.5); // Real Part
				 noise[2*i + 1] = 0;                   // Img Part
				 } */
				
				// Add Frequency
				//for (int i = 0; i < 4096; i++) {							
					//noise[2*i]     += 1 * Math.sin(i* 200.0 * 2* Math.PI / samplerate);
					//noise[2*i]     += 1 * Math.sin(i* 400.0 * 2* Math.PI / samplerate);
					//noise[2*i]     += 1 * Math.sin(i* 600.0 * 2* Math.PI / samplerate);
					//noise[2*i]     += 1 * Math.sin(i* 1000.0 * 2* Math.PI / samplerate);
					//noise[2*i]     += 1 * Math.sin(i* 7000.0 * 2* Math.PI / samplerate);
					//noise[2*i]     += 1 * Math.sin(i* 11000.0 * 2* Math.PI / samplerate);
					//noise[i] = buffer[i];
					
					//noise[2*i + 1] = 0;                   
				//}
				
				
				
				
				int w = panel.getWidth();
				
				g2.setColor(Color.RED.darker());
				int sx = 0;
				int sy = 25;						
				for (int i = 0; i < 4096; i++) {
					int x = (int)(i * w / 4096.0);
					int y = (int)((noise[i]*7) + 20);
					g2.drawLine(sx,sy,x,y);
					sx = x;
					sy = y;							
				}
				g2.drawLine(0, 20 -7, w, 20 -7);
				g2.drawLine(0, 20 +7, w, 20 +7);
				
				
				sx = 0;
				sy = 25;
				g2.setColor(Color.YELLOW.darker());
				
				// Perform windowing
				for (int i = 0; i < 4096; i++) {
					noise[i] *= window[i];
					
					int x = (int)(i * w / 4096.0);
					int y = (int)((window[i]*7) + 20);
					g2.drawLine(sx,sy,x,y);
					g2.drawLine(sx,20-(sy-20),x,20-(y-20));
					
					sx = x;
					sy = y;				
				}
				
				g2.setColor(Color.YELLOW);
				sx = 0;
				sy = 25;						
				for (int i = 0; i < 4096; i++) {
					int x = (int)(i * w / 4096.0);
					int y = (int)((noise[i]*7) + 20);
					g2.drawLine(sx,sy,x,y);
					
					sx = x;
					sy = y;							
				}						
				
				{
					g2.setColor(Color.BLUE);
					g2.drawLine(0, 100, w, 100);
					int s = (int)(Math.PI * 1 * 20);
					g2.drawLine(0, 100 + s, w, 100 + s);
					g2.drawLine(0, 100 - s, w, 100 - s);
				}
				
				
				// Perform FFT
				//fft.calcReal(noise, -1);
				//fft.calcReal(noise, 1);
				fft.calcReal(noise, -1);
				
				double fftstart = 220;
				double fftscale = -10;
				{
					
					g2.setColor(Color.GREEN.darker().darker().darker().darker());
					
					double logman = Math.log10(2048/2048.0)*2;
					int s =(int)((fftstart + logman*fftscale));
					g2.drawLine(0, s, w - 40, s);
					
					for (int i = 0; i < 32; i++) {
						s =(int)((fftstart + -(1*i)*fftscale));
						g2.drawLine(0, s, w - 40, s);
					}
					
					g2.setColor(Color.GREEN.darker().darker());
					for (int i = 0; i < 23; i++) {
						double freq = i * 1000;
						int f = (int)( (freq*2 / samplerate)*w );
						g2.drawLine(f, (int)((fftstart + -(1*0)*fftscale)), f, (int)((fftstart + -(1*31)*fftscale)));
						
						g2.drawString(i+"kHz", f, (int)(fftstart - 2));
					}
					
					
					for (int i = 0; i < 32; i+=2) {
						s =(int)((fftstart + -(1*i)*fftscale));
						g2.drawString((-i*10) + "dB", w - 40, s+1);
						g2.drawLine(0, s, w - 40, s);
					}					
				}
				
				
				g2.setColor(Color.GREEN);
				
				
				int lx = 0;
				int ly = 100;
				int px = 0;
				int py = 50;
				
				//double last_real = 0;
				//double last_imag = 0;
				
				for (int i = 0; i < 2048; i++) {
					
					double real = noise[2*i];
					double imag = noise[2*i+1];
					
					//real = last_real * 0.99 + real * 0.01;
					//imag = last_imag * 0.99 + imag * 0.01;
					
					/* compute magnitude and phase */
					double magn = 2.*Math.sqrt(real*real + imag*imag);
					double phase = Math.atan2(imag,real);							
					
					g2.setColor(Color.GREEN);
					int x = (int)(i * w / 2048.0);							
					double logman = Math.log10(magn/2048.0)*2;							
					int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;
					
					
					if(i == 0) { lx = x; ly = y; }
					g2.drawLine(lx,ly,x,y);
					lx = x;
					ly = y;
					
					g2.setColor(Color.RED);
					x = (int)(i * w / 2048.0);
					y =(int)(phase*20) + 100;
					if(i == 0) { px = x; py = y; }
					g2.drawLine(px,py,x,y);
					px = x;
					py = y;
					
				}
				/*
				 g2.setColor(Color.CYAN);
				 
				 /// AVARAGE MEHTOD
				  {
				  
				  // POOR MANS LPC
				   
				   // PEAK ANALIS
				    double[] magnmap = new double[2048];
				    for (int i = 0; i < 2048; i++) {
				    
				    double real = noise[2*i];
				    double imag = noise[2*i+1];			
				    magnmap[i] = 2.*Math.sqrt(real*real + imag*imag);
				    }
				    
				    // FIND FIRST PEAK
				     double min = magnmap[0];		
				     double max = magnmap[0];		
				     int first100HZ = (int)(100.0 / 24000.0 * 2048.0);
				     int first2KHZ = (int)(2000.0 / 24000.0 * 2048.0);
				     for (int i = 0; i < first2KHZ; i++) {
				     if(magnmap[i] < min) min = magnmap[i];
				     if(magnmap[i] > max) max = magnmap[i];
				     }		
				     double avg = (min + max) / 2;
				     //double findmarg = (max * 9.0 + min) / 10.0;
				      int binsperfreq = (int)(250.0 / 24000.0 * 2048.0);
				      
				      
				      int bcounter = 0;	
				      int lastmaxpos = 0;
				      double lastmaxmagn = min;		
				      int maxpos = -1;
				      double maxmagn = 0;
				      
				      double avgmagn = 0;
				      int avgcount = 0;
				      for (int i = 0; i < 2048; i++) {
				      
				      
				      bcounter++;
				      if(magnmap[i] > maxmagn)
				      {
				      maxmagn = magnmap[i];
				      maxpos = i;					
				      }
				      avgmagn = magnmap[i];
				      avgcount++;				
				      
				      if(bcounter > binsperfreq)
				      {
				      maxpos = i;			
				      maxmagn = avgmagn / avgcount;
				      int x = (int)(maxpos * w / 2048.0);							
				      int x2 = (int)(lastmaxpos * w / 2048.0);							
				      double m = lastmaxmagn;
				      double m_step = (maxmagn - lastmaxmagn) / (x - x2);
				      for (int ii = x2; ii < x; ii++) {
				      double magn = m;
				      double logman = Math.log10(magn/2048.0)*2;							
				      int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;					
				      m += m_step;
				      g2.drawLine(ii, y, ii, y);
				      }
				      
				      lastmaxpos = maxpos;
				      lastmaxmagn = maxmagn;
				      
				      avgmagn = magnmap[i];
				      avgcount = 0;
				      
				      
				      maxmagn = 0;
				      maxpos = -1;
				      bcounter = 0;
				      i = lastmaxpos + (binsperfreq / 3);
				      }
				      
				      }			
				      }
				      
				      g2.setColor(Color.YELLOW);
				      // PEAK METHOD
				       
				       // POOR MANS LPC
				        
				        // PEAK ANALIS
				         double[] magnmap = new double[2048];
				         for (int i = 0; i < 2048; i++) {
				         
				         double real = noise[2*i];
				         double imag = noise[2*i+1];			
				         magnmap[i] = 2.*Math.sqrt(real*real + imag*imag);
				         }
				         
				         // FIND FIRST PEAK
				          double min = magnmap[0];		
				          double max = magnmap[0];		
				          int first100HZ = (int)(100.0 / 24000.0 * 2048.0);
				          int first2KHZ = (int)(2000.0 / 24000.0 * 2048.0);
				          for (int i = 0; i < first2KHZ; i++) {
				          if(magnmap[i] < min) min = magnmap[i];
				          if(magnmap[i] > max) max = magnmap[i];
				          }		
				          double avg = (min + max) / 2;
				          //double findmarg = (max * 9.0 + min) / 10.0;
				           int binsperfreq = (int)(750.0 / 24000.0 * 2048.0);
				           for (int i = first100HZ; i < first2KHZ; i++) {
				           if(magnmap[i] > avg)
				           {
				           int x = (int)(i * w / 2048.0);							
				           g2.drawLine(x, 0, x, 1024);
				           binsperfreq = (int)(i*1.5);
				           break;
				           }			
				           }				
				           
				           
				           int bcounter = 0;	
				           int lastmaxpos = 0;
				           double lastmaxmagn = avg;		
				           int maxpos = -1;
				           double maxmagn = 0;
				           
				           for (int i = 0; i < 2048; i++) {
				           
				           
				           bcounter++;
				           if(magnmap[i] > maxmagn)
				           {
				           maxmagn = magnmap[i];
				           maxpos = i;
				           
				           }
				           
				           if(bcounter > binsperfreq)
				           {
				           
				           int x = (int)(maxpos * w / 2048.0);							
				           int x2 = (int)(lastmaxpos * w / 2048.0);							
				           double m = lastmaxmagn;
				           double m_step = (maxmagn - lastmaxmagn) / (x - x2);
				           for (int ii = x2; ii < x; ii++) {
				           double magn = m;
				           double logman = Math.log10(magn/2048.0)*2;							
				           int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;					
				           m += m_step;
				           g2.drawLine(ii, y, ii, y);
				           }
				           
				           lastmaxpos = maxpos;
				           lastmaxmagn = maxmagn;
				           
				           maxmagn = 0;
				           maxpos = -1;
				           bcounter = 0;
				           i = lastmaxpos + (binsperfreq / 3);
				           }
				           
				           }
				           */
				
				try
				{
					SwingUtilities.invokeAndWait(new
							Runnable()
							{
						public void run() {
							panel.repaint();
						}
							});
				}
				catch(Exception e ) {}
				
			}
			audiostream.close()		;		
			interpreter.close();
			
			
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();				
		
		
		
	}
	
	boolean q_hires = false;
	public void processLogFrequencyResponse()
	{
		
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			
			//g2.drawLine(0, 0, 100, 100);
			// g2.drawString("FFT Testing 1 2 3", 50, 50);
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			FFTConstantQ fftconstanq ;
			if(!q_hires)
				fftconstanq = getFFTConstantQ();
			else
				fftconstanq = getFFTConstantQ2();
			
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
					
			
			
			int fftlen = fftconstanq.getFFTSize();
			
			int outk = fftconstanq.getNumberOfOutputBands();
			int harmonic_fftlen = (int)Math.pow(2, Math.ceil(Math.log(outk*2)/Math.log(2)));
			
			
			double startfreq = 440*Math.pow(2,((24-58)/12.0));
			double endfreq = samplerate / 2;
				
			//double disp50hz = (Math.log(50) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq)); 
			double disp75hz = (Math.log(75) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq)); 
			double disp100hz = (Math.log(100) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq)); 
			double disp250hz = (Math.log(250) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq)); 
			double disp500hz = (Math.log(500) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq)); 
			//double disp750hz = (Math.log(750) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq)); 
			double disp1000hz = (Math.log(1000) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq));
			double disp2500hz = (Math.log(2500) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq));
			double disp5000hz = (Math.log(5000) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq));
			//double disp7500hz = (Math.log(7500) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq));
			double disp10000hz = (Math.log(10000) - Math.log(startfreq)) / (Math.log(endfreq) - Math.log(startfreq));
						

			//double fund2 = (Math.log(2)) / (Math.log(endfreq) - Math.log(startfreq));
			//double fund3 = (Math.log(3)) / (Math.log(endfreq) - Math.log(startfreq));
			//double fund4 = (Math.log(4)) / (Math.log(endfreq) - Math.log(startfreq));
			//double fund5 = (Math.log(5)) / (Math.log(endfreq) - Math.log(startfreq));
			//double fund6 = (Math.log(6)) / (Math.log(endfreq) - Math.log(startfreq));
			
	        AffineTransform xform = AffineTransform.getTranslateInstance(50, 50);
	        xform.scale(20, 20);		        
	        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			
			
			double[] harmonic_buffer = new double[harmonic_fftlen];
			//double[] last_value = new double[harmonic_fftlen];
			
			//double[] harmonic_buffer_cursum = new double[harmonic_fftlen];			
			int runsum_len = 3;
			int runsum_cur = 0;
			//double[][] harmonic_buffer_runsum = new double[runsum_len][harmonic_fftlen];
			
			double[] buffer = new double[fftlen];
			
			
			//for (int yy = 0; yy <  ww; yy++) {												
			int yy = 0;
			double[] noise = new double[fftlen];
			double[] outnoise = new double[fftconstanq.getNumberOfOutputBands()*2];			
			
			Arrays.fill(noise , 0);
			Arrays.fill(outnoise, 0);
			

			
			while(process_running)
			{
				
				
				
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}
				
				for (int i = 0; i < buffer.length - 4096; i++) {
					buffer[i] = buffer[i + 4096];
				}
				int len = audiostream.replace(buffer, buffer.length - 4096, buffer.length);
				for (int i = len; i < 4096; i++) {
					buffer[i + buffer.length - 4096] = 0;
				}
				//int len = audiostream.replace(buffer, 0, fftlen);
				if(len == -1) break;
				
				
					
				// Fill FFT Buffer
				for (int i = 0; i < fftlen; i++) {							
					noise[i] = buffer[i];
					//noise[2*i + 1] = 0;                   
				}
				/*
				for (int i = len; i < fftlen; i++) {							
					noise[2*i] = 0;
					noise[2*i + 1] = 0;                   
				}*/
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				//fft.calcReal(noise, -1);
				//fft.calcReal(noise, 1);
				
				fftconstanq.calc(noise, outnoise);
				
				Arrays.fill(harmonic_buffer, 0);
				int k = 0; //harmonic_fftlen / 2;
				for (int i = 0; i <  fftconstanq.getNumberOfOutputBands(); i++) { // fftconstanq.getNumberOfOutputBands()*2
					
					double real = outnoise[2*(i)];
					double imag = outnoise[2*(i)+1];
					
					/* compute magnitude and phase */
					harmonic_buffer[k] = 2.*Math.sqrt(real*real + imag*imag);
					k++;
				}
				/*
				harmonic_fft.calcReal(harmonic_buffer, -1);
				
				for (int i = 0; i <  harmonic_fftlen; i+=2) {
					double b_r = harmonic_buffer[i];
					double b_i = harmonic_buffer[i + 1];
					//double p_r = harmonic_pattern1[i];
					//double p_i = harmonic_pattern1[i + 1];
					
					//double t_r =  (b_r * p_r + b_i * p_i) / ( p_r * p_r + p_i * p_i );
					//double t_i =  (b_i * p_r - b_r * p_i) / ( p_r * p_r + p_i * p_i );

					//double t_r = b_r * p_r - b_i * p_i;
					//double t_i = b_r * p_i + b_i * p_r;
					
					double t_r = b_r ;
					double t_i = b_i ;					
					
					
					
					t_r *= 0.1 + 0.9 * ( (double)(i) / (double)harmonic_fftlen );
					t_i *= 0.1 + 0.9 * ( (double)(i) / (double)harmonic_fftlen );

					harmonic_buffer[i] = t_r;
					harmonic_buffer[i + 1] = t_i;
				}
			
				harmonic_fft.calcReal(harmonic_buffer, 1);
				*/
				
				g2.setColor(Color.GREEN);
				
				

				
				
				//int lx = 0;
				//int ly = 100;
				
				/*
				for (int i = 0; i < harmonic_buffer.length; i++) {
					harmonic_buffer[i] = last_value[i] * 0.6 + harmonic_buffer[i]*0.4; 
					last_value[i] = harmonic_buffer[i]; 
				}*/
				
				
				
				g2.setColor(Color.BLACK);
				g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
				g2.setColor(Color.RED);
				
				
				int lx = 0;
				int ly = 100;
				double fftstart = 100;
				double fftscale = -10;				
				int w = panel.getWidth();
					
					
				{
					
					g2.setColor(Color.GREEN.darker().darker());
					
					g2.drawString("FFT Size = " + fftconstanq.getFFTSize(), 20, 20);
					g2.drawString("Q bands  = " + fftconstanq.getNumberOfOutputBands(), 20, 40);
					
					
					g2.setColor(Color.GREEN.darker().darker().darker().darker());
					
					double logman = 0;
					int s =(int)((fftstart + logman*fftscale));
					g2.drawLine(0, s, w - 40, s);
					
					for (int i = 0; i < 32; i++) {
						s =(int)((fftstart + -(1*i)*fftscale));
						g2.drawLine(0, s, w - 40, s);
					}
					
					g2.setColor(Color.GREEN.darker().darker());
					
					for (int i = 0; i < 10; i++) {
						
						double freq = 0;
						if(i == 0) freq = disp75hz;
						if(i == 1) freq = disp100hz;
						if(i == 2) freq = disp250hz;
						if(i == 3) freq = disp500hz;
						if(i == 4) freq = disp1000hz;
						if(i == 5) freq = disp2500hz;
						if(i == 6) freq = disp5000hz;
						if(i == 7) freq = disp10000hz;
						
						int f = (int)( freq*w );
						g2.drawLine(f, (int)((fftstart + -(1*0)*fftscale)), f, (int)((fftstart + -(1*31)*fftscale)));
						
						if(i == 0) g2.drawString("75 hz", f, (int)(fftstart - 2));
						if(i == 1) g2.drawString("100 hz", f, (int)(fftstart - 2));
						if(i == 2) g2.drawString("250 hz", f, (int)(fftstart - 2));
						if(i == 3) g2.drawString("500 hz", f, (int)(fftstart - 2));
						if(i == 4) g2.drawString("1 kHz", f, (int)(fftstart - 2));
						if(i == 5) g2.drawString("2.5 kHz", f, (int)(fftstart - 2));
						if(i == 6) g2.drawString("5 kHz", f, (int)(fftstart - 2));
						if(i == 7) g2.drawString("10 kHz", f, (int)(fftstart - 2));
						
						
					}
					
					
					
					for (int i = 0; i < 32; i+=2) {
						s =(int)((fftstart + -(1*i)*fftscale));
						g2.drawString((-i*10) + "dB", w - 40, s+1);
						g2.drawLine(0, s, w - 40, s);
					}					
				}
				
				

				//int m = 0;
				int hlen = outnoise.length / 2; // harmonic_buffer.length / 2;
				int max = fftconstanq.getNumberOfOutputBands();
				g2.setColor(Color.GREEN);
					
				for (int i = 0; i < max - 2; i++) { // fftconstanq.getNumberOfOutputBands()*2				
						double magn = 2.*harmonic_buffer[i] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];					
						int x = (int)(i * (double)w / (double)hlen);							
						double logman = Math.log10(magn*500)*2;							
						int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;												
						if(i == 0) { lx = x; ly = y; }
						g2.drawLine(lx,ly,x,y);
						lx = x;
						ly = y;																	
					
				}
				
				for (int i = 0; i < max - 2; i++) {					
					double magn = 2.*harmonic_buffer[i] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];					
					double logman = Math.log10(magn*500)*2;							
					harmonic_buffer[i] = logman;					
				}
				
				g2.setColor(Color.PINK);
				
				double lastval = harmonic_buffer[0];
				double lowerby = 0.1;
				for (int i = 0; i < max - 2; i++)
				{
					double val = harmonic_buffer[i];
					lastval -= lowerby;
					if(val > lastval) lastval = val;
					harmonic_buffer[i] = lastval;
				}
				for (int i = max - 3; i >= 0; i--)
				{
					double val = harmonic_buffer[i];
					lastval -= lowerby;
					if(val > lastval) lastval = val;
					harmonic_buffer[i] = lastval;
				}
				
				//int lastpick = -100;
				
				lastval = harmonic_buffer[3];
				double lastval2 = 0;
				int ly2 = 0;
				
				//double[] runsum_buffer = harmonic_buffer_runsum[runsum_cur];
				for (int i = 3; i < max - 2 - 3; i++) 				
				{
					double logman = harmonic_buffer[i];
						
					int x = (int)(i * (double)w / (double)hlen);
					int y =(int)((fftstart + logman*fftscale)); //(magn/20) + 100;												
					if(i == 3) { lx = x; ly = y; }
					g2.drawLine(lx,ly,x,y);
					
					double diff = logman - lastval;
					
					double diffdiff = diff - lastval2;
					lastval2 = diff;
					
					if(diffdiff > 0) diffdiff = 0;
					
					diffdiff = -diffdiff;
					/*
					double sum = harmonic_buffer_cursum[i];					
					sum -= runsum_buffer[i];
					runsum_buffer[i] = diffdiff;
					sum += diffdiff;
					harmonic_buffer_cursum[i] = sum;
					
					diffdiff = sum / runsum_len;
					*/
					//if(diffdiff < 0.3) diffdiff = 0;
					
					int y2 = (int)((fftstart + 200 + diffdiff*fftscale*10)); //(magn/20) + 100;
					
					if(i == 3) { ly2 = y2; }
						
					g2.drawLine(lx,ly2,x,y2);
					
					ly2 = y2;
					/*
					if(logman > harmonic_buffer[i-2])
					if(logman > harmonic_buffer[i+2])
						{
							if(Math.abs(lastpick - i) > 5)
								g2.drawLine(x,y,x,1024);
							lastpick = i;
						}*/
					
					lastval = logman;
							
					lx = x;
					ly = y;							
				}
				
							
				runsum_cur = (runsum_cur + 1) % runsum_len;
				
				
				try
				{
					SwingUtilities.invokeAndWait(new
							Runnable()
							{
						public void run() {
							panel.repaint();
						}
							});
				}
				catch(Exception e ) {}
				
			}
			audiostream.close()		;		
			interpreter.close();
			
			
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();							
		
	}
	
	
	
	public void processOctaveNoteMatrix()
	{
		
		if(process_running) return;
		process_running = true;
		showFFTPanel();
		
		Runnable runnable = new Runnable() { public void run() {
			
			//g2.drawLine(0, 0, 100, 100);
			// g2.drawString("FFT Testing 1 2 3", 50, 50);
			//double hi = -10000;
			//double lo = 10000;
			
			Graphics2D g2 = offImg.createGraphics();
			g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
			g2.setColor(Color.RED);
			
			double samplerate = 44100;
			
			FFTConstantQ fftconstanq = getFFTConstantQ();
			
			


			
			
			Interpreter interpreter = new Interpreter();
			
			String script = textarea.getText();
			try {
				interpreter.eval(script);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}

			
			AudioSession audiosession = new AudioSession(samplerate, 1);												
			AudioStream audiostream = AudioEvents.openStream(interpreter.get("output"), audiosession);
			
			int ww = panel.getWidth();
					
			
			
			int fftlen = fftconstanq.getFFTSize();
			
			int outk = fftconstanq.getNumberOfOutputBands();
			int harmonic_fftlen = (int)Math.pow(2, Math.ceil(Math.log(outk*2)/Math.log(2)));
			
			//FFT harmonic_fft = new FFT(harmonic_fftlen);
			
			int[] matriximagedata = new int[(harmonic_fftlen / 2) ];
			
			MemoryImageSource mis = new MemoryImageSource(24, 10, matriximagedata, 0, 24);
			
			
			
	        AffineTransform xform = AffineTransform.getTranslateInstance(50, 50);
	        xform.scale(20, 20);		        
	        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			
			
			double[] harmonic_buffer = new double[harmonic_fftlen];
			//double[] last_value = new double[harmonic_fftlen];
			
			double[] buffer = new double[fftlen];
			
			
			//for (int yy = 0; yy <  ww; yy++) {												
			int yy = 0;
			double[] noise = new double[fftlen];
			double[] outnoise = new double[fftconstanq.getNumberOfOutputBands()*2];			
			
			Arrays.fill(noise , 0);
			Arrays.fill(outnoise, 0);
			

			
			while(process_running)
			{
				
				
				
				yy++;
				if(yy == ww)
				{
					yy = 0;
					g2.setColor(Color.BLACK);
					g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
					
				}
				
				for (int i = 0; i < buffer.length - 4096; i++) {
					buffer[i] = buffer[i + 4096];
				}
				int len = audiostream.replace(buffer, buffer.length - 4096, buffer.length);
				for (int i = len; i < 4096; i++) {
					buffer[i + buffer.length - 4096] = 0;
				}
				//int len = audiostream.replace(buffer, 0, fftlen);
				if(len == -1) break;
				
				
					
				// Fill FFT Buffer
				for (int i = 0; i < fftlen; i++) {							
					noise[i] = buffer[i];
					//noise[2*i + 1] = 0;                   
				}
				/*
				for (int i = len; i < fftlen; i++) {							
					noise[2*i] = 0;
					noise[2*i + 1] = 0;                   
				}*/
				
				//int w = panel.getWidth();
				//int h = panel.getHeight();
				
				// Perform FFT
				//fft.calcReal(noise, -1);
				//fft.calcReal(noise, 1);
				
				fftconstanq.calc(noise, outnoise);
				
				Arrays.fill(harmonic_buffer, 0);
				int k = 0; //harmonic_fftlen / 2;
				for (int i = 0; i <  fftconstanq.getNumberOfOutputBands(); i++) { // fftconstanq.getNumberOfOutputBands()*2
					
					double real = outnoise[2*(i)];
					double imag = outnoise[2*(i)+1];
					
					/* compute magnitude and phase */
					harmonic_buffer[k] = 2.*Math.sqrt(real*real + imag*imag);
					k++;
				}
				/*
				harmonic_fft.calcReal(harmonic_buffer, -1);
				
				for (int i = 0; i <  harmonic_fftlen; i+=2) {
					double b_r = harmonic_buffer[i];
					double b_i = harmonic_buffer[i + 1];
					//double p_r = harmonic_pattern1[i];
					//double p_i = harmonic_pattern1[i + 1];
					
					//double t_r =  (b_r * p_r + b_i * p_i) / ( p_r * p_r + p_i * p_i );
					//double t_i =  (b_i * p_r - b_r * p_i) / ( p_r * p_r + p_i * p_i );

					//double t_r = b_r * p_r - b_i * p_i;
					//double t_i = b_r * p_i + b_i * p_r;
					
					double t_r = b_r ;
					double t_i = b_i ;					
					
					
					
					t_r *= 0.1 + 0.9 * ( (double)(i) / (double)harmonic_fftlen );
					t_i *= 0.1 + 0.9 * ( (double)(i) / (double)harmonic_fftlen );

					harmonic_buffer[i] = t_r;
					harmonic_buffer[i + 1] = t_i;
				}
			
				harmonic_fft.calcReal(harmonic_buffer, 1);
				*/
				
				g2.setColor(Color.GREEN);
				
				

				
				
				//int lx = 0;
				//int ly = 100;
				
				/*
				for (int i = 0; i < harmonic_buffer.length; i++) {
					harmonic_buffer[i] = last_value[i] * 0.6 + harmonic_buffer[i]*0.4; 
					last_value[i] = harmonic_buffer[i]; 
				}*/
				
				
				
				g2.setColor(Color.BLACK);
				g2.fillRect(0, 0, panel.getWidth(), panel.getHeight());
				g2.setColor(Color.RED);
					
				//int w = panel.getWidth();
				int m = 0;
				for (int i = 0; i < harmonic_buffer.length/2; i++) { // fftconstanq.getNumberOfOutputBands()*2
				
					double magn1 = harmonic_buffer[i] / harmonic_fftlen; //harmonic_buffer[2*(i/2)];
					double logman1 = Math.log10(magn1 ); // /512.0
					
					float colormag1 = (float)(1 + logman1 / 8.0);
					if(colormag1 > 1.0) colormag1 = 1.0f;
					else
						if(colormag1 < 0.0) colormag1 = 0.0f;

					float colormag2 = (float)(1 + logman1 / 12.0);
					if(colormag2 > 1.0) colormag2 = 1.0f;
					else
						if(colormag2 < 0.0) colormag2 = 0.0f;

					float colormag3 = (float)(1 + logman1 / 16.0);
					if(colormag3 > 1.0) colormag3 = 1.0f;
					else
						if(colormag3 < 0.0) colormag3 = 0.0f;
					
					// 0..1..2..3
					// B  G  Y  R  
					
					
					//int x = note*40;
					//int y = octave*40;
					
					int value1 =  (int)(colormag1*colormag1*0xFF);
					int value2 =  (int)(colormag2*colormag2*0xFF);
					int value3 =  (int)(colormag3*colormag3*0xFF);
					matriximagedata[m++]   = value3 | value2<<8 | value1<<16 | 0xFF << 24;
					
					                
					/*
					g2.setColor(new Color(colormag1, colormag1, colormag1));
					g2.fillRect(x+ 50 ,y + 50, 40, 40);*/

					
				}
				
				Image matriximage = createImage (mis);
		        g2.drawImage(matriximage, xform, null);				
				
				g2.setColor(Color.GREEN);
				g2.drawString("C" , 0*40 + 55, 35);
				g2.drawString("C#", 1*40 + 55, 35);
				g2.drawString("D" , 2*40 + 55, 35);
				g2.drawString("D#", 3*40 + 55, 35);
				g2.drawString("E" , 4*40 + 55, 35);
				g2.drawString("F" , 5*40 + 55, 35);
				g2.drawString("F#", 6*40 + 55, 35);
				g2.drawString("G" , 7*40 + 55, 35);
				g2.drawString("G#", 8*40 + 55, 35);
				g2.drawString("A" , 9*40 + 55, 35);
				g2.drawString("A#", 10*40 + 55, 35);
				g2.drawString("B" , 11*40 + 55, 35);

				g2.drawString("1" , 35, 55 + 0*20);
				g2.drawString("2" , 35, 55 + 1*20);
				g2.drawString("3" , 35, 55 + 2*20);
				g2.drawString("4" , 35, 55 + 3*20);
				g2.drawString("5" , 35, 55 + 4*20);
				g2.drawString("6" , 35, 55 + 5*20);
				g2.drawString("7" , 35, 55 + 6*20);
				g2.drawString("8" , 35, 55 + 7*20);
				g2.drawString("9" , 35, 55 + 8*20);
				g2.drawString("10" , 35, 55 + 9*20);
				
				try
				{
					SwingUtilities.invokeAndWait(new
							Runnable()
							{
						public void run() {
							panel.repaint();
						}
							});
				}
				catch(Exception e ) {}
				
			}
			audiostream.close()		;		
			interpreter.close();
			
			
			process_running = false;
			hideFFTPanel();
			
		}};
		
		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();							
		
	}
	
	GroupMenuListener groupmenulistener = new GroupMenuListener()
	{

		public void actionPerformed(Record record) {
			
			if(record.unit instanceof RegisterFunction)
			{				
				if(FunctionCallEditor.isEditableFunction(record.name, globalinterpreter))
				{
					String statement = FunctionCallEditor.editFunctionCall(FFT_Testing.this, record.name+"()", globalinterpreter);
					if(statement != null)
					{
						textarea.replaceSelection(statement);
					}
					return;					
				}				
				textarea.replaceSelection(record.name+"()");
				return;
			}
			textarea.replaceSelection(record.name);
		}
		
	};
	


	UndoManager undo = new UndoManager();
	Interpreter globalinterpreter = new Interpreter();
	//int counter = 0;
	public FFT_Testing()
	{		
		textarea.setDocument(new RSyntaxDocument(globalinterpreter));
		textarea.setFont(new Font("Courier", 0, 12));
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = getClass().getResourceAsStream("/uk/co/drpj/testing/Synth.r");
		try
		{
			try
			{
				byte[] buffer = new byte[1024];
				int len = 0;
				while((len = is.read(buffer)) != -1)
				{
					baos.write(buffer,0, len);
				}
			}
			finally
			{
				is.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try {
			textarea.replaceSelection(new String(baos.toByteArray(), "LATIN1"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
	    // Add Undo Support to TextArea
	    
	    Document doc = textarea.getDocument();
	    
	    doc.addUndoableEditListener(new UndoableEditListener() {
	        public void undoableEditHappened(UndoableEditEvent evt) {
	            undo.addEdit(evt.getEdit());
	        }
	    });
	    
	    ActionMap textarea_actionmap = textarea.getActionMap();
	    InputMap textarea_inputmap = textarea.getInputMap();

	    textarea_inputmap.put(KeyStroke.getKeyStroke("control Z"), "Undo");
	    textarea_inputmap.put(KeyStroke.getKeyStroke("control Y"), "Redo");
	    
	    textarea_actionmap.put("Undo",
	        new AbstractAction("Undo") {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent evt) {
	                try {
	                    if (undo.canUndo()) {
	                        undo.undo();
	                    }
	                } catch (CannotUndoException e) {
	                }
	            }
	       });
	    
	    textarea_actionmap.put("Redo",
		        new AbstractAction("Redo") {
	
	    			private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
		                try {
		                    if (undo.canRedo()) {
		                        undo.redo();
		                    }
		                } catch (CannotRedoException e) {
		                }
		            }
		        });
	    

	    
		JMenuBar menubar = new JMenuBar();
		setJMenuBar(menubar);
		
		
		JMenu file_menu = menubar.add(new JMenu("File"));
		
		JMenuItem exit_menuitem = file_menu.add(new JMenuItem("Exit"));
		exit_menuitem.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0)
			{
				System.exit(0);
			}});		
		
		
		JMenu edit_menu = menubar.add(new JMenu("Edit"));
		
		JMenuItem menu_undo = new JMenuItem("Undo");
		menu_undo.setMnemonic('U');
		menu_undo.setAccelerator(KeyStroke.getKeyStroke("control Z"));
		menu_undo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
                try {
                    if (undo.canUndo()) {
                        undo.undo();
                    }
                } catch (CannotUndoException e) {
                }
			}
		});		
		JMenuItem menu_cut = new JMenuItem("Cut");
		menu_cut.setMnemonic('t');
		menu_cut.setAccelerator(KeyStroke.getKeyStroke("control X"));
		menu_cut.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
              	textarea.cut();
			}
		});
		JMenuItem menu_copy = new JMenuItem("Copy");
		menu_copy.setMnemonic('C');
		menu_copy.setAccelerator(KeyStroke.getKeyStroke("control C"));
		menu_copy.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				textarea.copy();
			}
		});
		JMenuItem menu_paste = new JMenuItem("Paste");
		menu_paste.setMnemonic('P');
		menu_paste.setAccelerator(KeyStroke.getKeyStroke("control V"));		
		menu_paste.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				textarea.paste();
			}
		});		
		JMenuItem menu_delete = new JMenuItem("Delete");
		menu_delete.setMnemonic('D');
		menu_delete.setAccelerator(KeyStroke.getKeyStroke("DELETE"));		
		menu_delete.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				textarea.replaceSelection("");
			}
		});					
		JMenuItem menu_selectAll = new JMenuItem("Select All");
		menu_selectAll.setMnemonic('A');
		menu_selectAll.setAccelerator(KeyStroke.getKeyStroke("control A"));		
		menu_selectAll.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				textarea.selectAll();
			}
		});					
		JMenuItem menu_arguments = new JMenuItem("Arguments...");
		menu_arguments.setAccelerator(KeyStroke.getKeyStroke("F4"));
		menu_arguments.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {						
						try
						{
							int start = textarea.getSelectionStart();
							int[] pair = ((RSyntaxDocument)textarea.getDocument()).extractFunctionCall(start);
							if(pair == null) return;
																					
							textarea.select(pair[0], pair[0]+pair[1]);
		
							String token = textarea.getDocument().getText(pair[0], pair[1]);
														
							String expr = FunctionCallEditor.editFunctionCall(FFT_Testing.this, token, globalinterpreter);
							if(expr != null)
							{
								textarea.replaceSelection(expr);
							}
							
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
				});			
		
		edit_menu.add(menu_undo);
		edit_menu.addSeparator();
		edit_menu.add(menu_cut);
		edit_menu.add(menu_copy);
		edit_menu.add(menu_paste);
		edit_menu.add(menu_delete);
		edit_menu.addSeparator();
		edit_menu.add(menu_selectAll);
		edit_menu.addSeparator();
		edit_menu.add(menu_arguments);
		
		
		JMenu run_menu = menubar.add(new JMenu("Run"));
		
		JMenuItem start_menuitem = run_menu.add(new JMenuItem("Run"));
		start_menuitem.setAccelerator(KeyStroke.getKeyStroke("F5"));
		start_menuitem.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0)
			{
				runScript();
			}});
				
		
		JMenuItem stop_menuitem = run_menu.add(new JMenuItem("Terminate"));
		stop_menuitem.setAccelerator(KeyStroke.getKeyStroke("ESCAPE"));
		stop_menuitem.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0)
			{
				stopScript();
			}});
		
		
		JMenu calc_menu = menubar.add(new JMenu("View"));
		
		JMenu groupmenu = new GroupMenus(new Interpreter(), groupmenulistener);
		menubar.add(groupmenu);
		
		
		
		calc_menu.add(new JMenuItem("Intensity graph...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				processIntensityImage();
			}});
		calc_menu.add(new JMenuItem("Stereo image...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				processStereoImage(false);
			}});
		calc_menu.add(new JMenuItem("Stereo + phase image...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				processStereoImage(true);
			}});
		calc_menu.add(new JMenuItem("Stereo image with phase color...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				processStereoPhaseImage();
			}});		
		calc_menu.add(new JMenuItem("Stereo phase image...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				processPhaseImage();
			}});				
		calc_menu.add(new JMenuItem("Spectrum graph...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {						
				processSpectrumGraph();
			}});

		calc_menu.add(new JMenuItem("Spectrum graph (Constant Q, res = 24/oct )...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				q_hires = false; 
				processConstantQSpectrumGraph();
			}});
		calc_menu.add(new JMenuItem("Spectrum graph (Constant Q, res = 48/oct )...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				q_hires = true;
				processConstantQSpectrumGraph();
			}});
		
		calc_menu.add(new JMenuItem("Spectrum graph (Constant Q, HPS)...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {						
				processConstantQSpectrumGraphHPS();
			}});

		
		calc_menu.add(new JMenuItem("Spectrum graph (Constant Q, Harmonics in color)...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {						
				processConstantQSpectrumHarmonicGraph();
			}});		
		
		calc_menu.add(new JMenuItem("Spectrum graph (DFT)...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {						
				processSpectrumGraphDFT();
			}});		
		/*
		 calc_menu.add(new JMenuItem("View spectrum graph (phase)...")).addActionListener(new ActionListener()
		 {
		 public void actionPerformed(ActionEvent arg0) {						
		 processSpectrumGraphP();
		 }});		*/
		calc_menu.add(new JMenuItem("Spectrum graph (log)...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {						
				processSpectrumGraphLog();
			}});	
		
		calc_menu.add(new JMenuItem("Harmonic product spectrum graph ...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {						
				processHarmonicProductSpectrumGraph();
			}});	
		
		calc_menu.add(new JMenuItem("Cepstrum graph ...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {						
				processCepstrumGraph();
			}});	
		
		
		
		
		
		/*
		 calc_menu.add(new JMenuItem("View spectrum graph (^2)...")).addActionListener(new ActionListener()
		 {
		 public void actionPerformed(ActionEvent arg0) {						
		 processSpectrumGraph2();
		 }});*/
		calc_menu.add(new JMenuItem("Frequency response...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				processFrequencyResponse();
			}});
		
		calc_menu.add(new JMenuItem("Frequency response (Constant Q, res = 24/oct )...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				q_hires = false;
				processLogFrequencyResponse();
			}});		

		calc_menu.add(new JMenuItem("Frequency response (Constant Q, res = 48/oct )...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				q_hires = true;
				processLogFrequencyResponse();
			}});		
		
		calc_menu.add(new JMenuItem("Note matrix...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				processOctaveNoteMatrix();
			}});
				
				

		JMenu tools_menu = menubar.add(new JMenu("Tools"));
		tools_menu.add(new JMenuItem("SoundFont Tester...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				SoundFontTester sft = new SoundFontTester();
				sft.setExitOnClose(false);
				sft.setVisible(true);
			}});		
		
		JMenu help_menu = menubar.add(new JMenu("Help"));
		
		help_menu.add(new JMenuItem("About RasmusDSP...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				new AboutDialog(FFT_Testing.this).setVisible(true);
			}});		
		
		
		setSize(1280, 800);
		//setResizable(false);
		//setSize(800, 600);	
		setLocationByPlatform(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		
		setTitle("RasmusDSP - Script Tester");		
		setIconImage(new javax.swing.ImageIcon(getClass().getResource("/rasmus/rasmusdsp.PNG")).getImage());
		
		GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();	
		GraphicsConfiguration graphicsConfiguration = graphicsEnvironment.getDefaultScreenDevice().getDefaultConfiguration();		
		offImg = graphicsConfiguration.createCompatibleImage(1280, 800, Transparency.BITMASK);				
		Graphics2D g2 = offImg.createGraphics();				
		g2.setBackground(getBackground());
		
		panel = new JPanel()
		{
			private static final long serialVersionUID = 1L;

			public void paint(Graphics graphics) {
				super.paint(graphics);
				
				graphics.drawImage(offImg,0,0 ,this);
			}
			
		};
		panel.setBackground(Color.BLACK);
		panel.setMinimumSize(new Dimension(10, 1000));
		panel.setPreferredSize(new Dimension(10, 1000));
		
		
		JPanel textpanel = new JPanel();
		textpanel.setLayout(new BorderLayout());
		textpanel.add(textarea);
		JScrollPane scrollpane = new JScrollPane(textpanel);
		
		
		//scrollpane.setMinimumSize(new Dimension(10, 200));
		//scrollpane.setPreferredSize(new Dimension(10, 200));
		cpanel = new JPanel();
		uipanel = new JPanel();
		uipanel.setLayout(new BorderLayout());
		
		cardlayout = new CardLayout(); 
		cpanel.setLayout(cardlayout);
		cpanel.add(scrollpane, "text");
		cpanel.add(panel, "fft");
		cpanel.add(uipanel, "uipanel");
		setContentPane(cpanel);
		
		setVisible(true);
		
	}
	
	JPanel uipanel;
	CardLayout cardlayout;
	public void showFFTPanel()
	{
		cardlayout.show(cpanel, "fft");
	}
	public void hideFFTPanel()
	{
		//cardlayout.show(cpanel, "text");
	}	
	
	Interpreter ui_interpreter = null;
	ControlViewer panelviewer = null;
	public void runScript()
	{
		if(process_running) return;
		process_running = true;
		cardlayout.show(cpanel, "uipanel");
		ui_interpreter = new Interpreter();
		String script = textarea.getText();
		try {
			ui_interpreter.eval(script);
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}		
		panelviewer = new ControlViewer(ui_interpreter, ui_interpreter.get("output"));
		uipanel.add(panelviewer);
		uipanel.invalidate();
		uipanel.validate();
		ui_interpreter.commit();
	}
	public void stopScript()
	{
		if(panelviewer != null)
		{
			uipanel.remove(panelviewer);
			panelviewer.close();	
			panelviewer = null;
			uipanel.invalidate();
			uipanel.validate();
			cardlayout.show(cpanel, "text");
		}
		if(ui_interpreter != null)
		{
			ui_interpreter.close();
			ui_interpreter.commit();
			ui_interpreter = null;
		}
		if(!process_running)
		cardlayout.show(cpanel, "text");
		process_running = false;
		
		
	}
	
	
	public static void main(String[] args) {
		new FFT_Testing();
	}
	
}
