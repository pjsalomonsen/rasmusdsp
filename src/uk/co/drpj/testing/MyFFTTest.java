/*
 * Created on Jan 10, 2007
 *
 * Copyright (c) 2006 P.J.Leonard
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.testing;

import java.util.Random;

import rasmus.interpreter.sampled.util.FFT;

public class MyFFTTest {

	int N = 3;
	FFT fft = new FFT(N);
	double fact = 2 * Math.PI / N;

	void reco(double X[], double x[], int N) {
		for (int k = 0; k < N; k++) {
			for (int j = 0; j < N; j++) {
				x[2 * j] = x[2 * j]
						+ (X[k * 2] * Math.cos(j * fact * k) - X[k * 2 + 1]
								* Math.sin(j * fact * k)) / N;
				x[2 * j + 1] = x[2 * j + 1]
						+ (X[k * 2] * Math.sin(j * fact * k) + X[k * 2 + 1]
								* Math.cos(j * fact * k)) / N;
			}
		}
	}

	public void print(double x[], int N) {

		for (int i = 0; i < N; i++) {
			System.out.print(String.format("%+4.2f%+5.2fj ", x[2 * i],
					x[2 * i + 1]));
			if (i % ((N+1)/2) ==0) System.out.print("|");
		}
		System.out.println();
	}

	public void printReal(double x[], int N) {
		for (int i = 0; i < N; i++) {
			System.out.print(String.format("(%4.2f %+5.2fj)", x[i], 0.0) + " ");
		}
		System.out.println();
	}

	public void randFill(double x[],int N) {
		Random rand = new Random();
		for (int i = 0; i < N; i++) {
			x[i] = 2.0 * (rand.nextDouble() - 0.5);
		}
	}

	void copy(double a[] ,double b[],int N) {
		for(int i=0;i<N;i++) b[i]=a[i];
	}
	void scale(double a[] ,double mult) {
		for(int i=0;i<a.length;i++) a[i] *=mult;
	}


	void toComplex(double xr[],double x[],int N) {
		for(int i=0;i<N;i++) {
			x[2*i]=xr[i];
			x[2*i+1]=0.0;
		}
	}
	
	void doitAll() {

		double xreal[] = new double[N];
		double xcomp[] = new double[2*N];

		double temp[]=new double[2*N];
		
		randFill(xreal,N);	
		copy(xreal,temp,N);
		toComplex(xreal,xcomp,N);
		print(xcomp,N);

		fft.calc(xcomp, -1);
		print(xcomp,N);
		
		fft.calcReal(temp, -1);
		print(temp,N);
		double x2[]=new double[2];
		x2[0]=temp[0]+temp[1];
		x2[1]=temp[0]-temp[1];
		print(xcomp,1);
		
		
	}
	
	public static void main(String args[]) {
	
		MyFFTTest t=new MyFFTTest();
		t.doitAll();
		

	}

}
