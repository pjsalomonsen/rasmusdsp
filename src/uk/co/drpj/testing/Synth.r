
//midiinputvar <- MidiInput("In USB Keystation");
midiinputvar <- MidiInput("Keystation [hw:1,0]");


/**************************************
 *  User Interface
 **************************************/


//<- CQFilter(width=400,height=200,row=0,minHz=100,mode=1,ninsperocatve=36) <- mon;
<- cqspectrumgraph(0.1,width=400,height=200,row=1,binsperoctave=12,minHz=100,mode=0,mindB=-90) <- mon;
//<- cqmag          (0.1,height=200,width=400,row=2,binsperoctave=12,minHz=100,mode=1,key=0) <- mon;
<- cqmag          (0.1,height=200,width=400,row=2,binsperoctave=12,minHz=100,mode=0,key=4,mindB=-90) <- mon;
<- MidiKeyboard(midiinputvar,fill=1, row=4, margin=0);


Sin2 <- function(note, velocity, active, pitch)
{

    freq <- 440*pi*pow(2, (note - 57)/12) * pitch;
    
	osc  <- sin() <- clock(freq);
	out  <- gain(velocity*0.2) <- osc;
	output <- adsr(active, 0.02, 0.1, 0.5, 0.2) * out;  

};

impulse2 <- function(note, velocity, active, pitch, modamount)
{
	 //xadsr(active, 0.02, 0.1, 0.5, 0.02) * 
	output <- impulse(44100);
};

Sf2voice <- Sf2Preset("/home/pjl/8MBGMSFX.SF2",0,0);

AudioOutput(channels=1) <- AudioMonitor(mon,chennels=1) <- Synthesizer(Sf2Voice) <- midiinputvar;



//AudioOutput(channels=1) <- AudioMonitor(mon) <- Synthesizer(Sin2) <- midiinputvar;


