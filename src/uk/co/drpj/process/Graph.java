/*
 * Created on 23-Sep-2006
 *
 * Copyright (c) 2006 P.J.Leonard
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.process;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Graph extends JPanel {

	Color colors[]={Color.GREEN,Color.RED,Color.BLUE};
	
	double [][] a;
	double [] max;
	double [] min;
	
	public Graph() {
		setBackground(Color.BLACK);
	}
	
	public void plot(double [][] a) {

		this.a=a;
		this.max=new double[a.length];

		for(int i=0;i<a.length;i++) {
			max[i]=Double.MIN_VALUE;
		}
			
		for(int i=0;i<a.length;i++) {
			for (int j=0;j<a[i].length;j++) {
				max[i]=Math.max(Math.abs(max[i]), a[i][j]);
			}
		}
		repaint();
	}
		
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		if ( a== null) return;
		int w=getWidth();
		int h=getHeight();
		int start=0;
		for (int i=0;i<a.length;i++ ) {
			g.setColor(colors[i]);
			int ptr=start+1;
			int n=Math.min(w,a[i].length-start);
			for (int j=1;j<n;j++,ptr++) {
				int x1=h/2 + (int) (h*0.5*a[i][ptr-1]/max[i]);		
				int x2=h/2 + (int) (h*0.5*a[i][ptr]/max[i]);		
				g.drawLine( j-1, x1, j, x2);
			}				
		}
	}
	
	
}
