/*
 * Created on 16-Sep-2006
 *
 * Copyright (c) 2006 P.J.Leonard
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.process;

import javax.swing.JFrame;

import rasmus.interpreter.sampled.util.FFTConstantQ;

public class ConstantQ {

	FFTConstantQ fftconstanq = null;

	boolean process_running = false;

	static double samplerate = 44100.0;
	double freqs[];
	int frameInc;
	
	ConstantQ(int frameInc) {


		this.frameInc = frameInc ; // advance each frame by this number of samples

		double minFreq = 440 * Math.pow(2, ((24 - 58) / 12.0));
			double maxFreq = samplerate / 2;
			int binsPerOctave = 24;

			this.fftconstanq = new FFTConstantQ(samplerate, minFreq, maxFreq,
					binsPerOctave);
			this.freqs=new double[this.fftconstanq.getNumberOfOutputBands()];
			for (int i = 0; i < this.fftconstanq.getNumberOfOutputBands(); i++) {
				// Calculate the frequency of current bin
				this.freqs[i] = minFreq * Math.pow(2, (double)i / binsPerOctave);

			}
	}

	public void process(double in[], double out[]) {


		int fftlen = fftconstanq.getFFTSize();

		double[] noise = new double[fftlen];
		double[] outnoise = new double[fftconstanq.getNumberOfOutputBands() * 2];

		double magn[] = new double[fftconstanq.getNumberOfOutputBands()];
		double phase[] = new double[fftconstanq.getNumberOfOutputBands()];

		for (int i = 0; i < fftconstanq.getNumberOfOutputBands(); i++) {
			magn[i] = phase[i] = 0.0;
		}
		
		for (int frameCount = 0; (frameCount - 1) < (in.length) / frameInc; frameCount++) {
	//	int frameCount = 0;{
			System.out.print(".");
			// Fill FFT Buffer
			for (int i = 0; i < fftlen; i++) {
				if (i + frameCount * frameInc < in.length)
					noise[i] = in[i + frameCount * frameInc];
				else
					noise[i] = 0.0;
			}

			fftconstanq.calc(noise, outnoise);


			// time of first sample in buffer
			int sampleCount = frameCount * frameInc;

			double t1 = sampleCount / samplerate;

			for (int bin = 0; bin < fftconstanq.getNumberOfOutputBands(); bin++) {
				double freq = this.freqs[bin];
				double period = 1.0 / freq;

				// phase of kernel relative to t=0
				// we need to shift frame relative signal by this
				double phaseShift = Math.PI * 2.0 * (t1 % period) / period;

				double real = outnoise[2 * bin];
				double imag = outnoise[2 * bin + 1];

				/* compute magnitude and phase */
				double magni = 2. * Math.sqrt(real * real + imag * imag);
			//	System.out.println(freq + "   " + magni);

				double phasei = Math.atan2(imag, real) - phaseShift; // TODO
				// think
				// +/-
				// ????

				// wrap the phase.
				while (phasei < phase[bin] - Math.PI)
					phasei += Math.PI * 2;
				while (phasei > phase[bin] + Math.PI)
					phasei -= Math.PI * 2;

				double scale = 1.0 / (frameInc-1);
				
				
				for (int i = 0; i < frameInc; i++) {
				
					if (sampleCount + i >= in.length)
						break;
					double fact2 = i * scale;
					double fact1 = 1.0 - fact2;
					double mag = (magn[bin] * fact1 + magni * fact2);
					double pha = (phase[bin] * fact1 + phasei * fact2);
					
					double tt = (sampleCount + i) * freq * Math.PI * 2.0
							/ samplerate;
				
					out[sampleCount+i] += mag * Math.cos(tt+pha);
			//		out[sampleCount + i] += mag * Math.cos(tt);
				}

				magn[bin] = magni;
				phase[bin] = phasei;

			}
		}
	}

	public static void main(String args[]) {
		JFrame frame = new JFrame();
		Graph graph = new Graph();
		frame.setContentPane(graph);
		frame.pack();
		frame.setVisible(true);
		frame.setSize(1000, 500);

		int frameInc=128;
		ConstantQ q = new ConstantQ(frameInc);

		double freq =q.freqs[25];  //+q.freqs[25])/2.0;
		double in[] = new double[10000];
		double out[] = new double[10000];

		for (int i = 0; i < in.length; i++) {
			in[i] = Math.sin(i * 2 * Math.PI * freq / samplerate);
			out[i] = 0;
		}

		for (int i = 0; i < frameInc; i++) in[i]*= (double)i/frameInc;
		
		q.process(in, out);

		for (int i = 0; i < frameInc; i++) out[i]=0.0;
		double a[][] = new double[2][];
		a[0] = in;
		a[1] = out;
		graph.plot(a);

	}

}
