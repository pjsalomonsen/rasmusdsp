/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * modified by pjl
 * 
 */
package uk.co.drpj.interpreter.controls.sampled;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.controls.ControlUnitFactory;
import rasmus.interpreter.controls.ControlInstance;
import rasmus.interpreter.controls.sampled.AudioAbstractGraphAnalyzer;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import uk.co.drpj.interpreter.sampled.util.FFTConstantQ;



class AudioCQSpectrumGraphInstance extends AudioAbstractGraphAnalyzer 
{

	Variable input;
	Variable timebase;
	Variable samplerate;
	Variable channels;
	Variable binsPerOctave;
	Variable threshold;
	Variable key;

	Variable mindB;
	Variable maxdB;
	Variable minHz;
	Variable maxHz;
	Variable spread;
	
	public AudioCQSpectrumGraphInstance(Map parameters) {
		super(parameters);
		
		timebase = (Variable)parameters.get("timebase");
		input = (Variable)parameters.get("input");
		samplerate = (Variable)parameters.get("samplerate");
		channels = (Variable)parameters.get("channels");
		binsPerOctave = (Variable)parameters.get("binsperoctave");
		threshold = (Variable)parameters.get("threshold");
		mindB = (Variable)parameters.get("mindb");
		maxdB = (Variable)parameters.get("maxdb");
		minHz = (Variable)parameters.get("minhz");
		maxHz = (Variable)parameters.get("maxhz");
		spread=(Variable)parameters.get("spread");
		key=(Variable)parameters.get("key");
		Dimension size = new Dimension(300, 300);
		getJComponent().setPreferredSize(size);
		
		init();
	}
	
	public void run() {
		
		if(input == null) return;

		super.run();

		double samplerate = 44100;
		int channels = 1;
		double timebase = 0.1; // 100 msec / 4410 bytes
		int binsPerOctave = 12;

		double mindB = -180;
		double maxdB = 0;
		double minHz = 100;
		double maxHz = samplerate / 2;
		double threshold = 0.001;
		double spread = 1.0;
		int key=0;
		
		if(this.samplerate != null) samplerate = DoublePart.asDouble(this.samplerate);
		if(this.channels != null) channels = (int)DoublePart.asDouble(this.channels);
		if(this.timebase != null) timebase = DoublePart.asDouble(this.timebase);
		if(this.binsPerOctave != null) binsPerOctave = (int)DoublePart.asDouble(this.binsPerOctave);
		if(this.threshold != null) threshold = DoublePart.asDouble(this.threshold);
		if(this.mindB != null) mindB = DoublePart.asDouble(this.mindB);
		if(this.maxdB != null) maxdB = DoublePart.asDouble(this.maxdB);
		if(this.minHz != null) minHz = DoublePart.asDouble(this.minHz);
		if(this.maxHz != null) maxHz = DoublePart.asDouble(this.maxHz);
		if(this.spread != null) spread = DoublePart.asDouble(this.spread);
		if(this.key != null) key = (int)DoublePart.asDouble(this.key);
				
		double rangedB = maxdB - mindB;
		
		{
			Graphics2D g = getGraphics();
			g.setColor(Color.BLACK);
			Dimension size = getSize();
			int w = size.width;
			int h = size.height;				
			g.fillRect(0,0,w,h);					
			g.setColor(Color.CYAN);
			g.drawString("Calculating Constant Q Kernel", 20, 20);
			repaint();
		}
		
		FFTConstantQ fftcq = new FFTConstantQ(samplerate, minHz, maxHz, binsPerOctave, threshold,spread,key);
		
		{
			Graphics2D g = getGraphics();
			Dimension size = getSize();
			int w = size.width;
			int h = size.height;							
			g.setColor(Color.BLACK);
			g.fillRect(0,0,w,h);					
			repaint();
		}
		
		
		int fftframesize = fftcq.getFFTSize();
		int outputbins = fftcq.getNumberOfOutputBands();
		int outputbins2 = outputbins*2;
		int fftframesizec = fftframesize * channels;

		int monitorsamples = (int)(samplerate * timebase);
		int monitorsamplesc = monitorsamples * channels;
		
		
		System.out.println("monitorsample=" + monitorsamples + "fftframesize " + fftframesize );
		
		AudioSession audiosession = new AudioSession(samplerate, channels);												
		AudioStream audiostream = AudioEvents.openStream(input, audiosession);	
		
		double[][] fftbuffer = new double[channels][];
		double[][] fftoutbuffer = new double[channels][];
		double[][] graphbuffer = new double[channels][];
		
		for (int c = 0; c < channels; c++) {
			fftbuffer[c] = new double[fftframesize];
			fftoutbuffer[c] = new double[outputbins2];
			graphbuffer[c] = new double[outputbins]; // fftframesize2
		}		
		double[] window = fftcq.getFFT().wHanning();		
		int bufferlen = monitorsamplesc;
		if(bufferlen < fftframesizec) bufferlen = fftframesizec;
		double[] buffer = new double[bufferlen];

		while(isActive())
		{
			// Read audio buffer
			for (int i = 0; i < bufferlen - monitorsamplesc; i++) 
				buffer[i] = buffer[i + monitorsamplesc];					
			int len = audiostream.replace(buffer, bufferlen - monitorsamplesc, bufferlen);			
			if(len == -1) break;
			Arrays.fill(buffer, bufferlen - monitorsamplesc + len, bufferlen, 0);
			
			// Do FFT transform
			for (int c = 0; c < channels; c++) {
				double[] fftbuff = fftbuffer[c];
				double[] fftbuffout = fftoutbuffer[c];
				int ci = c;
				for (int i = 0; i < fftframesize; i++) {
					// why window ?
					// we are using <x,y*>   = <X,Y*>
					fftbuff[i] = buffer[ci] ;// * window[i];
					ci += channels;
				}
				fftcq.calc(fftbuff, fftbuffout);
			}			

			for (int c = 0; c < channels; c++) {
				double[] fftbuffout = fftoutbuffer[c];
				double[] gbuffer = graphbuffer[c];				
				int x = 0;
				for (int i = 0; i < outputbins2; x++ ) {
					double real = fftbuffout[i++];
					double imag = fftbuffout[i++];
					double magn = 4.0 * Math.sqrt(real*real + imag*imag);					
					double logman = ((Math.log10(magn)*20.0) - mindB) / (rangedB);
					gbuffer[x] = logman;
				}		

				
			}			
			
			paint(graphbuffer);
			
		}
		
		
		audiostream.close();
		audiosession.close();		
	}
}

public class AudioCQSpectrumGraph extends ControlUnitFactory {

	public AudioCQSpectrumGraph()
	{
		registerParameter(1, "timebase");
		registerParameter(2, "control");
		registerParameter(3, "samplerate");
		registerParameter(4, "channels");
	}
	
	public MetaData getMetaData()
	{
		MetaData metadata = super.getMetaData();
		metadata.setDescription("Constant Q Spectrum Graph");
		metadata.add(1, "timebase",		 "Timebase",		"0.1", "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "control",		 "Control",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(3, "samplerate",	 "Sample rate",	"44100", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "channels",		 "Channels",		"1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "mindb",	     "Minimum energy", "-180", "dB", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "maxdb",	     "Maximum energy", "0", "dB", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "minhz",	     "Minimum frequency", "100", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "maxhz",	     "Maximum frequency", null, "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "spread",	     "Q *= 1/spread", "1",null , MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "threshold",	 "Threshold",         "0.001", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "binsperoctave","Bins Per Octave", "12", "bin/oct", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "refreshrate",	 "Refresh rate",	"15", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "autostart",	 "Auto Start",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		List options = new ArrayList();
		options.add("0");
		options.add("1");
		options.add("2");
		options.add("3");
		
		metadata.add(-1, "key",	 	 "Key",	"0", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN).put("options", options);

		
		options = new ArrayList();
		options.add("0");
		options.add("1");
		options.add("2");
		metadata.add(-1, "mode",	 	 "Mode",	"0", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN).put("options", options);
		
		
		return metadata;
	}	
	
	public ControlInstance newControlInstance(NameSpace namespace, Map parameters)
	{
		return new AudioCQSpectrumGraphInstance(parameters);
	}	
	
}
