/* 
 * Copyright (c) 2006, Karl Helgason
 *                     Modified by Paul Leonard
 *                     
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.interpreter.controls.sampled;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.controls.ControlInstance;
import rasmus.interpreter.controls.ControlUnitFactory;
import rasmus.interpreter.controls.sampled.AudioAbstractGraphAnalyzer;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import uk.co.drpj.feature.CQFilter;


class CQFilterBankGraphInstance extends AudioAbstractGraphAnalyzer 
{

	Variable input;
	Variable timebase;
	Variable samplerate;
	Variable channels;
	Variable chunksize;
	Variable binsperoctave;
	Variable nOctave;
	Variable Q;

	Variable mindB;
	Variable maxdB;
	Variable minHz;
	
	public CQFilterBankGraphInstance(Map parameters) {
		super(parameters);
		
		timebase = (Variable)parameters.get("timebase");
		input = (Variable)parameters.get("input");
		samplerate = (Variable)parameters.get("samplerate");
		channels = (Variable)parameters.get("channels");
		chunksize = (Variable)parameters.get("chunksize");
		binsperoctave = (Variable)parameters.get("binsperoctave");
		
		Dimension size = new Dimension(300, 300);
		getJComponent().setPreferredSize(size);
		
		init();
	}
	
	public void run() {
	
		if(input == null) return;
		
		super.run();
		
		double samplerate = 44100;
		int channels = 1;
	//	double timebase = 0.1; // 100 msec / 4410 bytes
		int chunksize = 1024;
		int binsPerOctave=12;
		int nOctave=7;
		double minHz=50.0;
		double Q=60;
		double mindB=-180;
		double maxdB=0;
		if(this.samplerate != null) samplerate = DoublePart.asDouble(this.samplerate);
		if(this.channels != null) channels = (int)DoublePart.asDouble(this.channels);
	//	if(this.timebase != null) timebase = DoublePart.asDouble(this.timebase);
		if(this.chunksize != null) chunksize = (int)DoublePart.asDouble(this.chunksize);
		if(this.binsperoctave != null) chunksize = (int)DoublePart.asDouble(this.binsperoctave);
		if(this.minHz != null) chunksize = (int)DoublePart.asDouble(this.minHz);
		if(this.Q != null) Q = (int)DoublePart.asDouble(this.Q);
		if(this.nOctave != null ) nOctave=(int)DoublePart.asDouble(this.nOctave);		
		if(this.mindB != null) mindB = DoublePart.asDouble(this.mindB);	
		if(this.maxdB != null) maxdB = DoublePart.asDouble(this.maxdB);	
	
		
		double rangedB = maxdB - mindB;
	//	int monitorsamples = (int)(samplerate * timebase);
	//	int monitorsamplesc = monitorsamples * channels;
		
		AudioSession audiosession = new AudioSession(samplerate, channels);												
		AudioStream audiostream = AudioEvents.openStream(input, audiosession);
	
		
		int nBins=7*binsPerOctave;
		
		CQFilter filt[] = new CQFilter[channels];
		

		double [][] graphbuffer = new double[channels][];
		
		for (int c = 0; c < channels; c++) {
			
			filt[c]=new CQFilter(minHz,
				binsPerOctave,
				nBins,
				Q,
				samplerate);

			graphbuffer[c] = new double[nBins]; // fftframesize2
		}		
		int bufferlen = chunksize*channels;
		double[] bufferc = new double[bufferlen];
		double[] buffer = new double[chunksize];

		while(isActive())
		{
			// Read audio buffer
			int len = audiostream.replace(bufferc, 0 , bufferlen);			
			if(len == -1) break;
			assert(len == bufferlen);
			
			// Feed CQFilter
			
			for (int c = 0; c < channels; c++) {
								
				int ic=c;
				for (int i=0;i<chunksize;i++,ic +=channels) {
					buffer[i]
					       =bufferc[ic];
					//System.out.println(buffer[i]);
				}
				filt[c].feed(buffer, chunksize);
			//	System.out.println(filt[c].stateIm[0]);
			}			

			for (int c = 0; c < channels; c++) {
				double[] gbuffer = graphbuffer[c];				
				filt[c].getMagnitude(gbuffer);
				for (int i=0;i<nBins;i++) {
				double logman = ((Math.log10(gbuffer[i])*20.0) - mindB) / (rangedB);
				gbuffer[i]=logman;
				}
			}			
			
			paint(graphbuffer);
			
		}
		
		
		audiostream.close();
		audiosession.close();		
	}
}

public class CQFilterBankGraph extends ControlUnitFactory {

	public CQFilterBankGraph()
	{
		registerParameter(1, "timebase");
		registerParameter(2, "control");
		registerParameter(3, "samplerate");
		registerParameter(4, "channels");
	}
	
	public MetaData getMetaData()
	{
		MetaData metadata = super.getMetaData();
		metadata.setDescription("CQ Filter Bank");
		metadata.add(1, "timebase",		 "Timebase",		"0.1", "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "control",		 "Control",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(3, "samplerate",	 "Sample rate",	"44100", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "channels",		 "Channels",		"1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "chunksize",    "filter chunk Size", "1024", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "Q",           "Q" , null,null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "minHz",           "min freq" , null,"Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "mindB",           "min dB" , null,"dB", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "maxdB",           "max dB" , null,"dB", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "binsperoctave", "bins per octave", "12", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "refreshrate",	 "Refresh rate",	"15", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "autostart",	 "Auto Start",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		
		List options = new ArrayList();
		options.add("0");
		options.add("1");
		options.add("2");
		metadata.add(-1, "mode",	 	 "Mode",	"0", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN).put("options", options);
		
		
		return metadata;
	}	
	
	public ControlInstance newControlInstance(NameSpace namespace, Map parameters)
	{
		return new CQFilterBankGraphInstance(parameters);
	}	
	
}
