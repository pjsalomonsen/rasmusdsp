/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.interpreter.sampled.generators;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioImpulseInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable period;
	Variable answer = new Variable();
	private int periodVal=44100;
	
	public void calc()
	{
	}	
	public AudioImpulseInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		period = parameters.getParameterWithDefault(1,"period");
		answer = AudioEvents.asVariable(new AudioEvent(0, this));	
		if(this.period != null) periodVal = (int)DoublePart.asDouble(this.period);
		output.add(answer);			
	}
	public void close() {
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{		
		long countval = 0;
		int channels;
		public FilterStreamInstance(AudioSession session)
		{			
			channels = session.getChannels();
		}
		public int mix(double[] buffer, int start, int end) {			
			int chs = channels;			
			for (int c = 0; c < chs; c++) {
				long ccountval = countval;
				for (int i = start + c; i < end; i+= chs) {
					if (ccountval % periodVal == 0) buffer[i] += 1.0;
					ccountval++;
				}
			}
			countval += end - start;
			return end - start;
		}
		public int replace(double[] buffer, int start, int end) {
			int chs = channels;			
			for (int c = 0; c < chs; c++) {
				long ccountval = countval;
				for (int i = start + c; i < end; i+= chs) {
					if (ccountval % periodVal == 0) buffer[i] = ccountval;
					else buffer[i]=0;
					ccountval++;
				}
			}
			countval += end - start;
			return end - start;
		}
		public int skip(int len)
		{
			return len;
		}
		public int isStatic(double[] buffer, int len) {
			return -1;
		}				
		public void close() {
		}		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioImpulse implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Impulse train generator");
		metadata.add(-1, "output",		"Output",		    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(-1, "period",		"Period",		   null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioImpulseInstance(parameters);
	}
}