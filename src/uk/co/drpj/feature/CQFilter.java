/*
 * Created on Dec 18, 2006
 *
 * Copyright (c) 2006 P.J.Leonard
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.feature;

public class CQFilter {

	double freq[];

	public double stateRe[];

	public double stateIm[];

	double dampRe[];

	double dampIm[];

	int nBins;

	double xLast = 0.0;


	public CQFilter(double freqMin, double binsPerOctave, int nBins, double Q,
			double Fs) {

		// Q is freq/bandwidth which is approx number of cycles in the window.
		// in terms of samples this is nsamp = Q*Fs/freq

		// The signal decays at a rate of damp^nsamp call window length when
		// this is smallTol

		double smallVal = 0.5;

		// 

		this.nBins = nBins;
		freq = new double[nBins];
		stateRe = new double[nBins];
		stateIm = new double[nBins];
		dampRe = new double[nBins];
		dampIm = new double[nBins];

		for (int i = 0; i < nBins; i++) {
			freq[i] = 2 * Math.PI * freqMin
					* Math.pow(2.0D, (double) i / binsPerOctave);
			stateRe[i] = stateIm[i] = 0.0;
			double nsamp = Q * Fs / freq[i];
			double damp = Math.pow(smallVal, 1.0D / nsamp);
			Complex dampC = Complex.expMinusJayArg(freq[i] / Fs);
			dampRe[i] = damp * dampC.re;
			dampIm[i] = damp * dampC.im;
			System.out.println(freq[i] + " " + damp + "  " + dampC.re + " "
					+ dampC.im);
		}
	}

	/**
	 * 
	 * @param x
	 *            input data
	 * @param n
	 *            number of samples
	 */
	public void feed(double x[], int n) {

		for (int i = 0; i < n; i++) {
			double dx = x[i] - xLast;
			xLast=x[i];
			for (int bin = 0; bin < nBins; bin++) {
				
				stateRe[bin] = stateRe[bin] * dampRe[bin] - stateIm[bin]
						* dampIm[bin] + dx;
				stateIm[bin] = stateIm[bin] * dampRe[bin] + stateRe[bin]
						* dampIm[bin];
			}
		}
	}

	public void getMagnitude(double mag[]) {
		for (int i = 0; i < nBins; i++) {
			mag[i] = Math.sqrt(stateRe[i] * stateRe[i] + stateIm[i]
					* stateIm[i]);
			

		}
	}

}
