/*
 * Created on Jan 2, 2007
 *
 * Copyright (c) 2006 P.J.Leonard
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.feature;

public class ComplexSpeedTest {
	static int N=1000;
	static int M=10000;
	double xr[]=new double[N];
	double xi[]=new double[N];
	
	double xd[]=new double[2*N];
	
	Complex x[]=new Complex[N];
	
	
	ComplexSpeedTest(){
		Complex xx=Complex.expMinusJayArg(0.0001);
		
		
		for (int i=0;i<N;i++) {
			x[i]=new Complex(xx);
			xr[i]=xx.re;
			xi[i]=xx.im;
			xd[2*i]=xx.re;
			xd[2*i+1]=xx.im;
			
		}	
	}
	
	
	void doC() {
		Complex tt=new Complex(1.0);
		for (int i=0;i<N;i++) {
			for (int j=0;j<M;j++) {
				tt.re = tt.re *x[i].re - tt.im*x[i].im; 				
				tt.im = tt.im *x[i].re + tt.re*x[i].im; 				
			}
		}
	}
	
	void doR() {
		double tr=1.0;
		double ti=0.0;
		for (int i=0;i<N;i++) {
			for (int j=0;j<M;j++) {
				tr = tr *xr[i] - ti*xi[i]; 				
				ti = ti *xr[i] + tr*xi[i]; 				
			}
		}
		
		
		
	}

	
	
	void doD() {
		double tr=1.0;
		double ti=0.0;
		for (int i=0;i<N;i++) {
			for (int j=0;j<M;j++) {
				tr = tr *xd[2*i] - ti*xd[2*i+1]; 				
				ti = ti *xd[2*i] + tr*xd[2*i+1]; 				
			}
		}
		
		
		
	}

	static public void main(String args[]) {
		ComplexSpeedTest t=new ComplexSpeedTest();
		
		long t1=System.nanoTime();
		t.doC();
		long t2=System.nanoTime();
		System.out.println(t2-t1);
		
		
		t1=System.nanoTime();
		t.doR();
		t2=System.nanoTime();
		System.out.println(t2-t1);

		
		t1=System.nanoTime();
		t.doD();
		t2=System.nanoTime();
		System.out.println(t2-t1);

		
	}
	
	

}
