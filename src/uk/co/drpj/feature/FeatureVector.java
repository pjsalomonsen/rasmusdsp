/*
 * Created on Sep 7, 2006
 *
 * Copyright (c) 2006 P.J.Leonard
 * 
 * http://www.frinika.com
 * 
 * This file is part of Frinika.
 * 
 * Frinika is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * Frinika is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Frinika; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package uk.co.drpj.feature;

import java.util.ArrayList;
import java.util.Iterator;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;

public class FeatureVector extends ListPart implements FeatureVectorListener {

    public static FeatureVector getInstance(Variable variable)
    {
            return (FeatureVector)variable.get(FeatureVector.class);
    }      
    
    public ArrayList featureVectorListeners = new ArrayList();

    
    public void addListener(ListPartListener listener)
    {
            if(listener instanceof FeatureVectorListener) 
                    addFeatureVectorListener((FeatureVectorListener)listener);
            super.addListener(listener);
    }
    public void removeListener(ListPartListener listener)
    {
            if(listener instanceof FeatureVectorListener) 
                    removeFeatureVectorListener((FeatureVectorListener)listener);
            super.removeListener(listener);
    }       

    public void addFeatureVectorListener(FeatureVectorListener listener)
    {
            synchronized(featureVectorListeners)
            {
                    featureVectorListeners.add(listener);
            }
    }
    public void removeFeatureVectorListener(FeatureVectorListener listener)
    {
            synchronized(featureVectorListeners)
            {
                    featureVectorListeners.remove(listener);
            }
    }                                        
	
	public void send(FeatureVector vector) {
        synchronized(featureVectorListeners)
        {
                Iterator iterator = featureVectorListeners.iterator() ;
                while (iterator.hasNext()) {                            
                        ((FeatureVectorListener) iterator.next()).send(vector);
                
                }
        }
		
	}

}

