/*
 * Created on Jan 2, 2007
 *
 * Copyright (c) 2006 P.J.Leonard
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.feature;

public class Complex {

	double re;
	double im;
	
	
    public Complex()
    {
            this.re = 0.0;
            this.im = 0.0;
    }

   
    public Complex(double re, double im)
    {
            this.re = re;
            this.im = im;
    }

 
    public Complex(double re)
    {
            this.re = re;
            this.im = 0.0;
    }

 
    public Complex(Complex c)
    {
            this.re = c.re;
            this.im = c.im;
    }



    public double abs(){
            double rmod = Math.abs(this.re);
            double imod = Math.abs(this.im);
            double ratio = 0.0D;
            double ret = 0.0D;

            if(rmod==0.0D){
                    ret=imod;
            }
            else{
                    if(imod==0.0D){
                            ret=rmod;
                    }
                    if(rmod>=imod){
                            ratio=this.im/this.re;
                            ret=rmod*Math.sqrt(1.0D + ratio*ratio);
                    }
                    else
                    {
                            ratio=this.re/this.im;
                            ret=imod*Math.sqrt(1.0D + ratio*ratio);
                    }
            }
            return ret;
    }
    

    public static Complex exp(Complex aa){
            Complex z = new Complex();

            double a = aa.re;
            double b = aa.im;

            if(b==0.0D){
                    z.re=Math.exp(a);
                    z.im=0.0D;
            }
            else{
                    if(a==0D){
                            z.re=Math.cos(b);
                            z.im=Math.sin(b);
                    }
                    else{
                            double c=Math.exp(a);
                            z.re=c*Math.cos(b);
                            z.im=c*Math.sin(b);
                    }
            }
            return z;
    }
    
 
    public static Complex exp(double aa){
            Complex bb = new Complex(aa, 0.0D);
            return Complex.exp(bb);
    }

  
    public static Complex expPlusJayArg(double arg){
            Complex argc = new Complex(0.0D, arg);
            return Complex.exp(argc);
    }

   
    public static Complex expMinusJayArg(double arg){
            Complex argc = new Complex(0.0D, -arg);
            return Complex.exp(argc);
    }

  
    public static Complex log(Complex aa ){

            double a=aa.re;
            double b=aa.im;
            Complex c = new Complex();

            c.re=Math.log(Complex.abs(aa));
            c.im=Math.atan2(b,a);

            return c;
    }
  
    public static double abs(Complex a){
            double rmod = Math.abs(a.re);
            double imod = Math.abs(a.im);
            double ratio = 0.0D;
            double res = 0.0D;

            if(rmod==0.0D){
            res=imod;
            }
            else{
            if(imod==0.0D){
                    res=rmod;
            }
                    if(rmod>=imod){
                            ratio=a.im/a.re;
                            res=rmod*Math.sqrt(1.0D + ratio*ratio);
                    }
                    else{
                            ratio=a.re/a.im;
                            res=imod*Math.sqrt(1.0D + ratio*ratio);
                    }
            }
            return res;
    }
    
}
