/*
 * Created on Sep 8, 2006
 *
 * Copyright (c) 2006 P.J.Leonard
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.feature;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.Variable;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class FeatureDetectInstance implements UnitInstancePart, Runnable
{

        Variable output;
        Variable input;

        Thread thread;
        boolean active = true;


        public void run()
        {

        	
        //		double peak;
        //		double decay;
        		int sampleRate=44100;
        		
                FeatureVectorListener listener = FeatureVector.getInstance(output);

                // Let's create 44.1 khz Mono session
                AudioSession audiosession = new AudioSession(sampleRate, 1);
                AudioStream audiostream = audiosession.openStream(input);

                double[] buffer = new double[500];
                while(active)
                {
                		audiosession.commit(); // Always call commit before reading from streams
                							   // as we want all changes to the session the commited
                							   // before we read more from it
                							   // - This is part of multi-threading synchronization
                		
                        int len = audiostream.replace(buffer, 0, 500);  
                        assert(len == 500 );
                        
                        
                        System.out.println(" PLEASE DO SOME THNG HERE ");

                        FeatureVector vect=new FeatureVector();
                        listener.send(vect);

                }

                audiostream.close();
                audiosession.close();
        }

        public FeatureDetectInstance(Parameters parameters)
        {
                output = parameters.getParameterWithDefault("output");
                input = parameters.getParameterWithDefault("input");

                // Let's use Interpreter Init Stack
                // because we don't want the this code run
                // until all code has been interpreted

                parameters.getNameSpace().addToCommitStack(new Commitable()
                {

						public int getRunLevel() {
							return RUNLEVEL_INIT;
						}

                        public void commit()
                        {
                                thread = new Thread(FeatureDetectInstance.this);
                                thread.start();
                        }
                });
        }

        public void close()
        {
                if(thread != null) thread.interrupt();
                active = false;
        }       

}


public class FeatureDetect implements UnitFactory {
    public UnitInstancePart newInstance(Parameters parameters) {
            return new FeatureDetectInstance(parameters);
    }
}
