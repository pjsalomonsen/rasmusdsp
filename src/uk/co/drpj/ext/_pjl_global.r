impulse                <- ExternalJava("uk.co.drpj.interpreter.sampled.generators.AudioImpulse");

CQFilter               <- ExternalJava("uk.co.drpj.interpreter.controls.sampled.CQFilterBankGraph");

group_controls_audio   <- RegisterFunction("CQFilter", "CQ Filter");

CQMag              <- ExternalJava("uk.co.drpj.interpreter.controls.sampled.AudioCQSpectrumGraph");
CQPhase                <- ExternalJava("uk.co.drpj.interpreter.controls.sampled.AudioCQPhaseGraph");
group_controls_audio   <- RegisterFunction("CQMag",     "CQ Spect Graph(pjl)");
group_controls_audio   <- RegisterFunction("CQPhase",   "CQ Phase");

/* FeatureDetect <- ExternalJava("uk.co.drpj.feature.FeatureDetect");
*/
 
 
