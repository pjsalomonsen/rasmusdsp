/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.drpj.ext;

import java.io.IOException;
import java.io.InputStream;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.spi.InterpreterExtension;

public class EXTInterpreterExtension implements InterpreterExtension {
	
	Interpreter interpreter;
	public void processExtension(String filename)
	{
		try
		{
			InputStream inputstream = getClass().getResourceAsStream("/uk/co/drpj/ext/" + filename);
			if(inputstream == null)
			{
				System.out.println("Can't find extension " + filename);
				return;
			}
			try {
				interpreter.source(inputstream);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}
			inputstream.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	public void execute(Interpreter interpreter) {
		this.interpreter = interpreter;
		processExtension("_pjl_global.r");
	}
	
}
