/*
 * Created on 06-Sep-2006
 *
 * Copyright (c) 2006 P.J.Leonard
 * 
 * Based on Sun code see notice below.
 * 
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * @(#)MidiSynth.java	1.15	99/12/03
 *
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Sun grants you ("Licensee") a non-exclusive, royalty free, license to use,
 * modify and redistribute this software in source and binary code form,
 * provided that i) this copyright notice and license appear on all copies of
 * the software; and ii) Licensee does not utilize the software in a manner
 * which is disparaging to Sun.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS
 * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
 * OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 *
 * This software is not designed or intended for use in on-line control of
 * aircraft, air traffic, aircraft navigation or aircraft communications; or in
 * the design, construction, operation or maintenance of any nuclear
 * facility. Licensee represents and warrants that it will not use or
 * redistribute the Software for such purposes.
 */

package uk.co.drpj.midi;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Transmitter;
import javax.swing.JPanel;

public class VirtualPianoPanel extends JPanel implements MouseListener,
		Transmitter, KeyListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	final int ON = 0, OFF = 1;

	final Color jfcBlue = new Color(204, 204, 255);

	final Color pink = new Color(255, 175, 175);

	Vector<Key> blackKeys = new Vector<Key>();

	Key keys[];

	Vector<Key> whiteKeys = new Vector<Key>();

	Key prevKey;

	final int nOctave;

	final int nNote;

	final int nWhiteNote ;

	private Receiver recv;

	private int currentvelocity = 100;

	private int currentoctave = 3;

	int width;

	int height = 30;

	int noteItemHeight;

	private int noteOffset;

	public VirtualPianoPanel(){
		this(3,6);
	}
	public VirtualPianoPanel(int botOctave,int nOctave) {
		super(false);
		this.nOctave=nOctave;
		noteOffset = botOctave*12;
		nNote=nOctave*12;
		nWhiteNote=7 * this.nOctave;
		this.width = this.nWhiteNote * 10;
		setPreferredSize(new Dimension(this.width, this.height));
		resizeKeys();
		setFocusable(true);
		addMouseListener(this);
		addKeyListener(this);
	}

	private void resizeKeys() {

		int whiteKeyWidth = this.width / this.nWhiteNote;
		int blackKeyDepth = (int) (this.height * .7);

		final int keyDepth = this.height;

		int blackWhiteGap;

		keys = new Key[nNote];

		this.blackKeys.clear();
		this.whiteKeys.clear();
		

		this.noteItemHeight = 8;

		blackWhiteGap = (int) (whiteKeyWidth / 3);

		int whiteIDs[] = { 0, 2, 4, 5, 7, 9, 11 };

		for (int i = 0, y = 0; i < this.nOctave; i++) {
			for (int j = 0; j < 7; j++, y += whiteKeyWidth) {
				int keyNum = i * 12 + whiteIDs[j];
				if (keyNum >= this.nNote)
					break;
				this.whiteKeys.add(new Key(0, y, keyDepth, whiteKeyWidth,
						keyNum));
			}
		}

		int halfGap = blackWhiteGap / 2;
		int blackKeyWidth = 2 * whiteKeyWidth / 3;

		for (int i = 0, y = 0; i < this.nOctave; i++, y += whiteKeyWidth) {
			int keyNum = i * 12 + 1;

			if (keyNum >= this.nNote)
				break;
			this.blackKeys.add(new Key(0, (y += whiteKeyWidth) - 2 * halfGap,
					blackKeyDepth, blackKeyWidth, keyNum));
			keyNum += 2;

			if (keyNum >= this.nNote)
				break;
			this.blackKeys.add(new Key(0, (y += whiteKeyWidth) - 2 * halfGap,
					blackKeyDepth, blackKeyWidth, keyNum));
			keyNum += 3;
			if (keyNum >= this.nNote)
				break;
			y += whiteKeyWidth;
			this.blackKeys.add(new Key(0, (y += whiteKeyWidth) - 2 * halfGap
					+ 1, blackKeyDepth, blackKeyWidth, keyNum));
			keyNum += 2;
			if (keyNum >= this.nNote)
				break;
			this.blackKeys.add(new Key(0, (y += whiteKeyWidth) - 2 * halfGap,
					blackKeyDepth, blackKeyWidth, keyNum));
			keyNum += 2;
			if (keyNum >= this.nNote)
				break;
			this.blackKeys.add(new Key(0, (y += whiteKeyWidth) - 2 * halfGap
					- 1, blackKeyDepth, blackKeyWidth, keyNum));

		}
		
		for (Key key:whiteKeys) {
			keys[key.kNum]=key;	
		}
		for (Key key:blackKeys) {
			keys[key.kNum]=key;	
		}

	}

	public void mousePressed(MouseEvent e) {
		this.prevKey = getKey(e.getPoint());
		if (this.prevKey != null) {
			this.prevKey.on();
			repaint();
		}
	}

	public void mouseReleased(MouseEvent e) {
		if (this.prevKey != null) {
			this.prevKey.off();
			repaint();
		}
	}

	public void mouseExited(MouseEvent e) {
		if (this.prevKey != null) {
			this.prevKey.off();
			repaint();
			this.prevKey = null;
		}
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		requestFocusInWindow();
	}

	public Key getKey(Point point) {

		for (Key key:keys) {
			if (key.contains(point)) {
				return key;
			}
		}
		return null;
	}

	@Override
	public void paintComponent(Graphics g) {
		Thread.yield();
		super.paintComponent(g);
		Thread.yield();

		// if (noteItemHeight!=Layout.getNoteItemHeight()) resizeKeys();

		Graphics2D g2 = (Graphics2D) g;

		if (this.width != getWidth() || this.height != getHeight()) {
			this.width = getWidth();
			this.height = getHeight();
			resizeKeys();

		}

		g2.setColor(Color.PINK);
		g2.fillRect(0, 0, this.width, this.height);

		for (int i = 0; i < this.whiteKeys.size(); i++) {
			Key key = (Key) this.whiteKeys.get(i);
			if (key.isNoteOn()) {
				g2.setColor(this.jfcBlue);
				g2.fill(key);

			}
			g2.setColor(Color.black);
			g2.draw(key);
			if (key.kNum % 12 == 0) {
				g2.setColor(Color.BLACK);
				g2.drawString(String.valueOf((key.kNum + noteOffset) / 12), key.x+1,
						this.height - 1);
				// g2.setPaintMode();
			}
		}
		for (int i = 0; i < this.blackKeys.size(); i++) {
			Key key = (Key) this.blackKeys.get(i);
			if (key.isNoteOn()) {
				g2.setColor(this.jfcBlue);
				g2.fill(key);
				g2.setColor(Color.black);
				g2.draw(key);
			} else {
				g2.setColor(Color.black);
				g2.fill(key);
			}
		}

		Thread.yield();

	}

	class Key extends Rectangle {

		private static final long serialVersionUID = 1L;

		int chan = 0; // TODO prgram this.

		int noteState = VirtualPianoPanel.this.OFF;

		int kNum;

		public Key(int y, int x, int height, int width, int num) {
			super(x, y, width, height);
			this.kNum = num;
		}

		public boolean isNoteOn() {
			return this.noteState == VirtualPianoPanel.this.ON;
		}

		public void on() {
			setNoteState(VirtualPianoPanel.this.ON);

			if (VirtualPianoPanel.this.recv == null)
				return;
			ShortMessage shm = new ShortMessage();
			try {
				shm.setMessage(ShortMessage.NOTE_ON, this.chan, this.kNum + noteOffset,
						getVelocity());
			} catch (InvalidMidiDataException e) {
				e.printStackTrace();
			}
			VirtualPianoPanel.this.recv.send(shm, -1);
		}

		public void off() {

			if (this.noteState == VirtualPianoPanel.this.OFF)
				return;
			setNoteState(VirtualPianoPanel.this.OFF);
			if (VirtualPianoPanel.this.recv == null)
				return;

			ShortMessage shm = new ShortMessage();

			try {
				shm.setMessage(ShortMessage.NOTE_ON, this.chan, this.kNum + noteOffset, 0);
			} catch (InvalidMidiDataException e) {
				e.printStackTrace();
			}
			VirtualPianoPanel.this.recv.send(shm, -1);
		}

		public void setNoteState(int state) {
			this.noteState = state;
			repaint();
		}
	}

	public void setReceiver(Receiver receiver) {
		this.recv = receiver;
	}

	public Receiver getReceiver() {
		return this.recv;
	}

	public void close() {
		this.recv = null;
	}

	public int getVelocity() {
		return this.currentvelocity;
	}

	public int getOctave() {
		return this.currentoctave;
	}

	public void setVelocity(int value) {
		this.currentvelocity = value;
	}

	public void setOctave(int value) {
		this.currentoctave = value;
	}

	public boolean isConsumed(KeyEvent e) {
		return !(keyCodeToNote(e) == 0);
	}

	private int keyCodeToNote(KeyEvent e) {

		return keyCodeToNote(this.currentoctave, e);
	}

	private int keyCodeToNote(int currentoctave, KeyEvent e) {
		int keycode = e.getKeyCode();
		int note;
		if (keycode == KeyEvent.VK_UNDEFINED) {
			char keychar = e.getKeyChar();
			switch (keychar) {
			case (char) 230: // Ae
				note = 15;
				break;
			case (char) 198: // Ae (CAP)
				note = 15;
				break;
			case (char) 254: // Thorn
				note = 16;
				break;
			case (char) 222: // Thron (CAP)
				note = 16;
				break;
			case (char) 240: // Eth
				note = 29;
				break;
			case (char) 208: // Eth (CAP)
				note = 29;
				break;
			default:
				return 0;
			}
		} else
			switch (keycode) {
			/*
			 * 49 = 81 (-1) 52 = 69 (+1) 56 = 85 (+1) 65 = 90 (-1)
			 * 
			 * 70 = 67 (+1) 75 = 77 (+1)
			 */

			case 49: // EXT
				note = 11;
				break;

			case 81:
				note = 12;
				break;
			case 50:
				note = 13;
				break;
			case 87:
				note = 14;
				break;
			case 51:
				note = 15;
				break;

			case 52: // EXT
				note = 17;
				break;

			case 69:
				note = 16;
				break;
			case 82:
				note = 17;
				break;
			case 53:
				note = 18;
				break;
			case 84:
				note = 19;
				break;
			case 54:
				note = 20;
				break;
			case 89:
				note = 21;
				break;
			case 55:
				note = 22;
				break;

			case 56: // EXT
				note = 24;
				break;

			case 85:
				note = 23;
				break;
			case 73:
				note = 24;
				break;
			case 57:
				note = 25;
				break;
			case 79:
				note = 26;
				break;
			case 48:
				note = 27;
				break;
			case 80:
				note = 28;
				break;

			case 45:
				note = 30;
				break;
			case 222:
				note = 31;
				break;

			case 70: // EXT
				note = -1;
				break;

			case 153:
				note = -1;
				break;
			case 90:
				note = 0;
				break;
			case 83:
				note = 1;
				break;
			case 88:
				note = 2;
				break;
			case 68:
				note = 3;
				break;

			case 65: // EXT
				note = 5;
				break;

			case 67:
				note = 4;
				break;
			case 86:
				note = 5;
				break;
			case 71:
				note = 6;
				break;
			case 66:
				note = 7;
				break;
			case 72:
				note = 8;
				break;
			case 78:
				note = 9;
				break;
			case 74:
				note = 10;
				break;

			case 75: // EXT
				note = 12;
				break;

			case 77:
				note = 11;
				break;
			case 44:
				note = 12;
				break;
			case 76:
				note = 13;
				break;
			case 46:
				note = 14;
				break;

			default:
				return 0;
			}

		note += currentoctave * 12;
		
		while(note > nNote ) note -=12;
		while(note < 0 ) note +=12;
		
		return note;

	}

	public void allNotesOff() {
		for (Key key : this.keys) {
			key.off();
		}
	}

	public void keyPressed(KeyEvent e) {
		// System.out.println("Hello");

		ShortMessage ssmg = new ShortMessage();

		switch (e.getKeyCode()) {
		case KeyEvent.VK_F10: // Garbage Collect
			System.gc();
			System.runFinalization();
			e.consume();
			return;

		case KeyEvent.VK_F11: // Reset All Controllers

			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x79, 0);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}
			if (this.recv != null)
				this.recv.send(ssmg, 0);
			e.consume();
			return;

		case KeyEvent.VK_F12: // All Sound Off

			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x78, 0);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}
			if (this.recv != null)
				this.recv.send(ssmg, 0);
			e.consume();
			return;

		case KeyEvent.VK_ESCAPE: // All Notes Off
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x7B, 0);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}
			if (this.recv != null)
				this.recv.send(ssmg, 0);
			e.consume();
			return;

		case KeyEvent.VK_PAGE_UP: // Octave offset up
			currentoctave++;
			if (currentoctave >8) currentoctave=8;

			e.consume();
			return;

		case KeyEvent.VK_PAGE_DOWN: // down
			currentoctave--;
			if (currentoctave <0) currentoctave=0;
			e.consume();
			return;

		}

		if (e.getKeyChar() == '+') {
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x40, 64);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}
			if (this.recv != null)
				this.recv.send(ssmg, 0);
		} else if (e.getKeyChar() == '-') {
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x40, 63);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}
			if (this.recv != null)
				this.recv.send(ssmg, 0);
		} else if (e.getKeyChar() == '*') {
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x42, 64);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}
			if (this.recv != null)
				this.recv.send(ssmg, 0);
		} else if (e.getKeyChar() == '/') {
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x42, 63);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}
			if (this.recv != null)
				this.recv.send(ssmg, 0);
		} else {

			int note = keyCodeToNote(e);
			if (note == 0)
				return;
			e.consume();

			this.keys[note].on();
		}
	}

	public void keyReleased(KeyEvent e) {

		int note = keyCodeToNote(e);
	//	System.out.println(note);
		if (note == 0)
			return;
		e.consume();

		this.keys[note].off();

	}

	public void keyTyped(KeyEvent e) {
		int note = keyCodeToNote(e);
		if (note == 0)
			return;
		e.consume();

	}

}
