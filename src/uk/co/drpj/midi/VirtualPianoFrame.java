/*
 * Created on 06-Sep-2006
 *
 * Copyright (c) 2006 P.J.Leonard
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package uk.co.drpj.midi;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Transmitter;

import javax.swing.JFrame;

public class VirtualPianoFrame extends JFrame implements MidiDevice {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	VirtualPianoPanel piano;

	static class VirtualPianoInfo extends Info {
		VirtualPianoInfo() {
			super("Virtual Piano", "drpjl.co.uk", "Swing virtual piano",
					"0.0.1");
		}
	}

	static Info deviceInfo = new VirtualPianoInfo();

	public VirtualPianoFrame() {
		setContentPane(piano = new VirtualPianoPanel(3,5));
		pack();
	}

	
	
	static public void main(String[] args) {
		VirtualPianoFrame frame = new VirtualPianoFrame();
		try {
			frame.open();
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Info getDeviceInfo() {
		// TODO Auto-generated method stub
		return deviceInfo;
	}

	public void open() throws MidiUnavailableException {
		setVisible(true);
		// TODO Auto-generated method stub

	}

	public void close() {
		setVisible(false);
		// TODO Auto-generated method stub

	}

	public boolean isOpen() {
		// TODO Auto-generated method stub
		return true;
	}

	public long getMicrosecondPosition() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getMaxReceivers() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getMaxTransmitters() {
		// TODO Auto-generated method stub
		return 1;
	}

	public Receiver getReceiver() throws MidiUnavailableException {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Receiver> getReceivers() {
		// TODO Auto-generated method stub
		return null;
	}

	public Transmitter getTransmitter() throws MidiUnavailableException {
		// TODO Auto-generated method stub
		return piano;
	}

	public List<Transmitter> getTransmitters() {
		// TODO Auto-generated method stub
		List<Transmitter> list=new ArrayList<Transmitter>();
		list.add(piano);
		return list;
		
	}
}
