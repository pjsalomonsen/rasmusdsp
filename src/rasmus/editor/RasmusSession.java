/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.io.ResourceManager;
import rasmus.interpreter.namespace.GlobalNameSpace;
import rasmus.interpreter.namespace.InheritNameSpace;
import rasmus.interpreter.parser.ScriptParserException;

public class RasmusSession {
	
	private Interpreter startupinterpreter;
	private Interpreter defaultinterpreter;
	private InheritNameSpace namespace;
	private ScriptDocument doc = new ScriptDocument();

	
	private Interpreter wrkdirinterpreter = null;
	private File wrkdir = null;
	
	public NameSpace getNameSpace()
	{
		return namespace;
	}
	
	public ScriptDocument getScriptDocument()
	{
		return doc;
	}
	
	public File getWorkDirectory()
	{
		return wrkdir;
	}
	public void setWorkDirectory(File file)
	{
		if(file != null && file.equals(wrkdir)) return;
		if(wrkdirinterpreter != null)
		{
			wrkdirinterpreter.close();
			wrkdirinterpreter = null;
			wrkdir = null;
		}
		if(file == null) return;
		String path;
		try {
			path = file.getCanonicalPath();
		} catch (IOException e) {
			path = file.getPath();
			e.printStackTrace();
		}
		
		if(!path.endsWith(File.separator)) path += File.separator;
		
		wrkdirinterpreter = new Interpreter(namespace);
		wrkdirinterpreter.setAutoCommit(false);
		wrkdirinterpreter.add("wrkdir", path);
		wrkdir = file;
		wrkdirinterpreter.commit();
	}
	
	InheritNameSpace overridenamespace;
	Interpreter overrideinterpreter = null;
	
	public RasmusSession()
	{
		// Search for startup.rasmusdsp file
		
		String user_home = System.getProperty("user.home");
				
		File file = new File(user_home, "startup.rasmusdsp");
		if(!file.exists())
		{
			file = new File("startup.rasmusdsp");
			if(!file.exists()) file = null;
		}
		 
		NameSpace globalnamespace = GlobalNameSpace.getNameSpace();
		
		InheritNameSpace defaultnamespace = new InheritNameSpace(globalnamespace);
		defaultnamespace.registerAsPrivate("_resource_manager");
		ResourceManager.getInstance(defaultnamespace);
		
		//defaultnamespace.put("instruments", globalnamespace.get("instruments"));
		//defaultnamespace.put("defaulttempo", globalnamespace.get("defaulttempo"));
		defaultinterpreter = new Interpreter(defaultnamespace);
		defaultinterpreter.setAutoCommit(false);
		InheritNameSpace startupnamespace = new InheritNameSpace(defaultnamespace);
		
		overridenamespace = new InheritNameSpace(startupnamespace);
		
		InheritNameSpace namespace = new InheritNameSpace(overridenamespace);
		Variable editorns_struct = namespace.get("this");
		startupnamespace.put("editor", editorns_struct);
		defaultnamespace.put("editor", editorns_struct);
		

		try {
			InputStream inputstream = getClass().getResourceAsStream("/rasmus/editor/default.rasmusdsp");
			try
			{					
				defaultinterpreter.source(inputstream);
			}
			finally
			{
				inputstream.close();				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}		
		
		//startupnamespace.put("instruments", defaultnamespace.get("instruments"));
		//startupnamespace.put("defaulttempo", defaultnamespace.get("defaulttempo"));
		startupinterpreter = new Interpreter(startupnamespace);
		if(file != null)
			try {
				startupinterpreter.source(file);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}
		
		
		//namespace.put("instruments", startupnamespace.get("instruments"));
		//namespace.put("defaulttempo", startupnamespace.get("defaulttempo"));
		this.namespace = namespace;
		doc.setNameSpace(namespace);
		
	}

	public void close()
	{
		doc.stopAll();
		if(wrkdirinterpreter != null)
			wrkdirinterpreter.close();
		namespace.close();
		defaultinterpreter.close();
		startupinterpreter.close();	
	
				
	}

	public void routeAudioOutput(Variable realtimeout) { 
		if(overrideinterpreter != null) overrideinterpreter.close();
		if(realtimeout == null) return;
		
		overrideinterpreter = new Interpreter(overridenamespace);
		overrideinterpreter.setAutoCommit(false);
		try {
			overrideinterpreter.eval("defaultoutputs <- function(input) {  parent.realtimeoutput <- input; } ");
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}
		
		realtimeout.add( overrideinterpreter.get("realtimeoutput") );
	}
}
