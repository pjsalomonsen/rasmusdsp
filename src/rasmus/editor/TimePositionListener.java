package rasmus.editor;

public interface TimePositionListener {
	
	public void start();
	public void stop();
	public void positionChange(double time, double beat);
}
