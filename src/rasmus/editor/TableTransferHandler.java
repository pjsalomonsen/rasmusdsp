/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.TransferHandler;
import javax.swing.event.TableModelEvent;

public class TableTransferHandler extends TransferHandler {

	private static final long serialVersionUID = 1L;
	
	public void clear(JComponent comp)
	{
		if(!(comp instanceof JTable)) return;
		JTable table = (JTable)comp;		
		int[] rows = table.getSelectedRows();
		int[] cols = table.getSelectedColumns();
		
		int ri = table.getSelectedRow();
		int ci = table.getSelectedColumn();
		
		for (int r = 0; r < rows.length; r++) {
			for (int c = 0; c < cols.length; c++) {
				table.setValueAt("",rows[r],cols[c]);
			}
		}
		
		table.tableChanged(new TableModelEvent(table.getModel()));
		
		if(ri != -1 && ci != -1)
			table.changeSelection(ri, ci, false, false);		
	}
	
	public int getSourceActions(JComponent c)
	{
		if(!(c instanceof JTable)) return NONE;
		return COPY_OR_MOVE;
	}
	
	protected void exportDone(JComponent source, Transferable data, int action) 
	{
		if(action == MOVE) clear(source);
	}
	
	public boolean importData(JComponent comp, Transferable t) 
	{
		if(!(comp instanceof JTable)) return false;
		
		JTable table = (JTable)comp;
		
		String s;
		try {
			s = (String)(t.getTransferData(DataFlavor.stringFlavor));
		} catch (UnsupportedFlavorException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		int r = 0;
		int c = 0;
		int ri = table.getSelectedRow();
		int ci = table.getSelectedColumn();
		if(ri == -1) ri = 0;
		if(ci == -1) ci = 0;
		
		int maxr = table.getRowCount();
		int maxc = table.getColumnCount();
		
		boolean lastfilled = false;
		StringTokenizer st = new StringTokenizer(s, "\n\t", true);
		while(st.hasMoreTokens())
		{
			String token = st.nextToken();
			if(token.equals("\n"))
			{
				if(!lastfilled)
				{
					if(((r+ri)<maxr)  && ((c+ci<maxc)))
						table.setValueAt("",r+ri,c+ci);
				}
				c = 0;
				r++;
				lastfilled = false;
			}
			else
			if(token.equals("\t"))
			{
				c++;
				lastfilled = false;
			}
			else
			{
				if(((r+ri)<maxr)  && ((c+ci<maxc)))
					table.setValueAt(token,r+ri,c+ci);
				lastfilled = true;
			}
		}
		
		table.tableChanged(new TableModelEvent(table.getModel()));
		
		table.changeSelection(ri, ci, false, false);

		return true;
	}
	
	public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) 
	{
		if(!(comp instanceof JTable)) return false;
		for (int i = 0; i < transferFlavors.length; i++) {
			if(transferFlavors[i].isFlavorTextType())
			{
				return true;
			}
		}
		return false;
	}
	
	protected Transferable createTransferable(JComponent comp) 
	{
		if(!(comp instanceof JTable)) return null;		
		JTable table = (JTable)comp;
		StringBuffer sb = new StringBuffer();
		int[] rows = table.getSelectedRows();
		int[] cols = table.getSelectedColumns();
		int lastfr = 0;
		for (int r = 0; r < rows.length; r++) {
			int lastfc = sb.length();
			boolean row_hascontent = false; 
			for (int c = 0; c < cols.length; c++) {
				String v = (String)table.getValueAt(rows[r],cols[c]);
				sb.append(v);
				if(v.length() != 0)
				{
					lastfc = sb.length();
					row_hascontent = true;
				}
				sb.append("\t");
			}
			sb.setLength(lastfc);
			
			if(row_hascontent)
				lastfr = sb.length();
			
			sb.append("\n");
		}
		sb.setLength(lastfr);
		
		return new StringSelection(sb.toString());
	}
}
