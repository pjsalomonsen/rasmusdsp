/* 
 * Copyright (c) 2006-2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.filechooser.FileFilter;

import rasmus.editor.ScriptDocument.ScriptObject;
import rasmus.editor.spi.FileConverter;
import rasmus.editor.spi.FileExporter;
import rasmus.editor.spi.ScriptImporter;
import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.BeatToTimeMapper;
import rasmus.interpreter.ui.AboutDialog;
import rasmus.util.RasmusUtil;

class MixedNumberComparator implements Comparator
		{
			public int compare(Object arg0, Object arg1) {
								
				String a  = arg0.toString();
				String b = arg1.toString();
				if(a.equals(b)) return 0;
				
                if(a.length() != 0) 
                if(b.length() != 0) 						
				while(a.charAt(0) == b.charAt(0))
				{
					a = a.substring(1);
                    b = b.substring(1);
                    if(a.length() == 0) break;
                    if(b.length() == 0) break;
				}             

                boolean ok = false;
                if(a.length() != 0)                         
                if(b.length() != 0)
                {
				char[] aa = a.toCharArray();
				char[] bb = b.toCharArray();
				ok = true;
				for (int i = 0; i < aa.length; i++) {
					if(!Character.isDigit(aa[i])) ok = false;
					if(!ok) break;
				}
				if(!ok)
				for (int i = 0; i < bb.length; i++) {
					if(!Character.isDigit(bb[i])) ok = false;
					if(!ok) break;
				}		
                }
                
				if(ok)
				{
				  try
				  {
				  return Integer.parseInt(a) -Integer.parseInt(b);
				  }
				  catch(Throwable e)
				  {
				  	return a.compareTo(b);						  	
				  }
				}
				else
				  return a.compareTo(b);
			}
		}


public class RasmusEditor extends JFrame {

	class RasmusListModel implements ComboBoxModel
	{
		ArrayList<ListDataListener> listeners = new ArrayList<ListDataListener>();
		String[] objects;
		String selitem;
		String[] items;
		
		public void commit()
		{
			processItems();
			
			Iterator<ListDataListener> iter = listeners.iterator();
			ListDataEvent event = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 0 );
			while (iter.hasNext()) 
				 iter.next().contentsChanged(event);
			
		}
		
		public void processItems()
		{
			doc.getObject(""); // Ensure that "" object always exists in object list
			List<ScriptObject> objects = doc.getObjects();
			items = new String[objects.size()];
			for (int i = 0; i < items.length; i++) {
				String name = objects.get(i).getName();
				if(name == null) name = "";
				items[i] = name;
			}
			Arrays.sort(items, new MixedNumberComparator());			
		}
		
		public void setSelectedItem(Object arg0) {
			selitem = ((String)arg0).toLowerCase();
		}
		public Object getSelectedItem() {
			return selitem;
		}
		public int getSize() {
			editorpanel.commit(true);
			if(items == null) processItems();
			return items.length;
		}
		public Object getElementAt(int arg0) {
			if(items == null) processItems();
			return items[arg0];
		}
		public void addListDataListener(ListDataListener arg0) {
			listeners.add(arg0);
		}
		public void removeListDataListener(ListDataListener arg0) {
			listeners.remove(arg0);
		}
		
	}	
	
	private static final long serialVersionUID = 1L;
	
	ScriptDocument doc;
	NameSpace namespace;
	
	JFileChooser fileopenchooser = null;
	JFileChooser filesavechooser = null;
	
	class FileConverterAdapter extends FileFilter
	{
		FileConverter fc;
		public FileConverterAdapter(FileConverter fc)
		{
			this.fc = fc;			
		}
		public FileConverter getConverter()
		{
			return fc;
		}
		public boolean accept(File file) {
			return fc.accept(file);
		}
		public String getDescription() {
			return fc.getDescription();
		}
	}
	
	FileFilter[] openfilters = null;
	public FileFilter[] getOpenFilters()
	{
		if(openfilters != null) return openfilters;
		List list = new ArrayList<FileFilter>();
		list.add(rasmusdsp_filefilter);
		FileConverter[] converts = getConverters();
		for (int i = 0; i < converts.length; i++) {
			FileConverter convert = converts[i];
			if(convert instanceof ScriptImporter)
				list.add(new FileConverterAdapter(convert));
		}
		openfilters = new FileFilter[list.size()];
		list.toArray(openfilters);
		return openfilters;
	}

	FileFilter[] savefilters = null;
	public FileFilter[] getSaveFilters()
	{
		if(savefilters != null) return savefilters;
		List list = new ArrayList<FileFilter>();
		list.add(rasmusdsp_filefilter);
		FileConverter[] converts = getConverters();
		for (int i = 0; i < converts.length; i++) {
			FileConverter convert = converts[i];
			if(convert instanceof FileExporter)
				list.add(new FileConverterAdapter(convert));
		}
		savefilters = new FileFilter[list.size()];
		list.toArray(savefilters);
		return savefilters;
	}
	
	FileFilter allformats_filefilter = new FileFilter()
	{
		public boolean accept(File file) {
			
			FileFilter[] filters = getOpenFilters();
			for (int i = 0; i < filters.length; i++) 
				if(filters[i].accept(file)) return true;			
			return false;
		}

		public String getDescription() {
			return "All formats";
		}		
	};
	
	FileFilter rasmusdsp_filefilter = new FileFilter()
	{
		public boolean accept(File arg0) {
			if(!arg0.isFile()) return true;
			return arg0.getName().toLowerCase().endsWith(".rasmusdsp");
		}
		public String getDescription() {
			return "RasmusDSP Scripts (*.rasmusdsp)";
		}
	};
	
	
	public JFileChooser getFileOpenChooser()
	{
		if(fileopenchooser == null)
		{
			fileopenchooser = new JFileChooser();	
			fileopenchooser.addChoosableFileFilter(allformats_filefilter);			
			FileFilter[] filters = getOpenFilters();
			for (int i = 0; i < filters.length; i++)
				fileopenchooser.addChoosableFileFilter(filters[i]);			
			fileopenchooser.setAcceptAllFileFilterUsed(true);			
			fileopenchooser.setFileFilter(allformats_filefilter);						
		}
		return fileopenchooser;
	}
	
	public JFileChooser getFileSaveChooser()
	{
		if(filesavechooser == null)
		{
			filesavechooser = new JFileChooser();
			FileFilter[] filters = getSaveFilters();
			for (int i = 0; i < filters.length; i++)
				filesavechooser.addChoosableFileFilter(filters[i]);					
			filesavechooser.setAcceptAllFileFilterUsed(false);
			filesavechooser.setFileFilter(rasmusdsp_filefilter);
		}
		return filesavechooser;
	}	
	
	File selectedfile = null;
	File currentdir = null;
	
	FileConverter[] converters = null;
	FileConverter currentconverter = null;
			
	public FileConverter[] getConverters()
	{
		if(converters != null) return converters;
		List list = RasmusUtil.getProviders(FileConverter.class);
		converters = new FileConverter[list.size()];
		list.toArray(converters);
		return converters;
	}
	
	public FileConverter getSelectedFileOpenConveter()
	{
		JFileChooser fc = getFileOpenChooser();
		File file = fc.getSelectedFile();
		
		FileFilter[] filters = getOpenFilters();
		for (int i = 0; i < filters.length; i++)
		{
			FileFilter filter = filters[i];
			if(filter.accept(file))
			{
				if(filter instanceof FileConverterAdapter)
				{
					return ((FileConverterAdapter)filter).getConverter();
				}
			}
		}
		return null;		
	}	
	
	public File selectFileForOpen()
	{
		JFileChooser fc = getFileOpenChooser();
		if(selectedfile != null)
		{
			fc.setSelectedFile(selectedfile);
			fc.setCurrentDirectory(currentdir);			
		}
		int ret = fc.showOpenDialog(this);
		selectedfile = fc.getSelectedFile();
		currentdir = fc.getCurrentDirectory();		
		if(ret == JFileChooser.APPROVE_OPTION)
		{
			return fc.getSelectedFile();
		}
		return null;
	}
	public File selectFileForSave()
	{
		JFileChooser fc = getFileSaveChooser();
		if(selectedfile != null)
		{
			fc.setSelectedFile(selectedfile);
			fc.setCurrentDirectory(currentdir);			
		}		
		int ret = fc.showSaveDialog(this);
		selectedfile = fc.getSelectedFile();
		currentdir = fc.getCurrentDirectory();		
		if(ret == JFileChooser.APPROVE_OPTION)
		{

			File file = fc.getSelectedFile();
			if(fc.getFileFilter() != null)
			{
				if(fc.getFileFilter() == rasmusdsp_filefilter)
				{							
					if(!file.getPath().toLowerCase().endsWith(".rasmusdsp"))
					{
						file = new File(file.getPath()+".rasmusdsp");
						fc.setSelectedFile(file);
					}
				}			
				if(fc.getFileFilter() instanceof FileConverterAdapter)
				{
					file = ((FileExporter)((FileConverterAdapter)fc.getFileFilter()).getConverter()).setFileExtension(file); 
					fc.setSelectedFile(file);
				}
			}
			
			return fc.getSelectedFile();
		}
		return null;
	}			
	
	public void fileNew()
	{	
		doc.stopAll();
		editorpanel.commit();
		session.setWorkDirectory(null);
		
		filename = null;
		updateTitle();
		resetPlayRenderCache();
		doc.setString("");
		currentobject = null;
		showObject("");
		combobox.setSelectedItem("");
		model.commit();
	}
	public void fileOpen()
	{	
			
			File file = selectFileForOpen();
			if(file == null) return;
			
			doc.stopAll();
			editorpanel.commit();
			filename = null;
			currentconverter = getSelectedFileOpenConveter();
			setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			if(currentconverter != null)			
			{
				String script = ((ScriptImporter)currentconverter).importToScript(this, file);
				
				
				if(script == null)
				{
					setCursor(Cursor.getDefaultCursor());
					return;
				}
				load(script);
			}
			else
				load(file);
			
			setCursor(Cursor.getDefaultCursor());
			session.setWorkDirectory(file.getParentFile());
			currentobject = null;
			showObject("");
			combobox.setSelectedItem("");
			model.commit();		
						
			filename = file;
			updateTitle();
		
	}
	
	public void update()
	{
		showObject("instruments");
		showObject("");
	}
	
	public void fileSave()
	{	
		if(filename == null) 
		{
			fileSaveAs();
			return;
		}
		
		
		FileFilter filefilter = null;
		boolean ok = false;
		
		
		FileFilter[] filters = getSaveFilters();
		for (int i = 0; i < filters.length; i++)
		{
			filefilter = filters[i];
			if(filefilter.accept(filename))
			{			
				ok = true;
				break;
			}
		}
		if(!ok)
		{
			fileSaveAs();
			return;			
		}
		
		save(filename);
		
	}
	public void fileSaveAs()
	{

		getFileSaveChooser().setFileFilter(rasmusdsp_filefilter);		
		if(filename != null)
		{
			
			FileFilter[] filters = getSaveFilters();
			for (int i = 0; i < filters.length; i++)
			{
				FileFilter filefilter = filters[i];
				if(filefilter.accept(filename))
				{			
					getFileSaveChooser().setFileFilter(filefilter);
					break;
				}
			}		
		}
		
		File file = selectFileForSave();
		if(file == null) return;
		
		save(file);
		
	}
	
	public void save(File file)
	{
		editorpanel.commit();
		
		filename = null;
		
		try {
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(doc.toString().getBytes("LATIN1"));
			fos.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}			
		
		session.setWorkDirectory(file.getParentFile());
		
		filename = file;
		updateTitle();		
	}
		
	
	public JMenuBar processMenuBar(JMenuBar p_menubar)
	{
		
		JMenuBar menubar = new JMenuBar();
		
		JMenu file_menu = menubar.add(new JMenu("File"));
		
		JMenuItem file_new = file_menu.add(new JMenuItem("New"));
		file_new.setMnemonic('N');
		file_new.setAccelerator(KeyStroke.getKeyStroke("control N"));
		file_new.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						fileNew();
					}			
				});
		JMenuItem file_open = file_menu.add(new JMenuItem("Open"));
		file_open.setMnemonic('O');
		file_open.setAccelerator(KeyStroke.getKeyStroke("control O"));
		file_open.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						fileOpen();
					}			
				});
		JMenuItem file_save = file_menu.add(new JMenuItem("Save"));
		file_save.setMnemonic('S');
		file_save.setAccelerator(KeyStroke.getKeyStroke("control S"));		
		file_save.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						fileSave();
					}			
				});
		JMenuItem file_saveas = file_menu.add(new JMenuItem("Save As"));
		file_saveas.setMnemonic('A');
		file_saveas.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						fileSaveAs();
					}			
				});
		
		file_menu.addSeparator();
		
		JMenuItem file_commit = file_menu.add(new JMenuItem("Commit"));
		file_commit.setMnemonic('C');
		file_commit.setAccelerator(KeyStroke.getKeyStroke("F12"));
		file_commit.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						editorpanel.commit();
					}			
				});		
		
		JMenuItem file_restart = file_menu.add(new JMenuItem("Restart"));
		file_restart.setMnemonic('R');
		file_restart.setAccelerator(KeyStroke.getKeyStroke("control F12"));
		file_restart.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						doc.stopAll();
						resetPlayRenderCache();
						editorpanel.commit();
					}			
				});				
		
		JMenuItem file_suspend = file_menu.add(new JMenuItem("Suspend"));
		file_suspend.setMnemonic('R');
		file_suspend.setAccelerator(KeyStroke.getKeyStroke("shift F12"));
		file_suspend.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						doc.stopAll();
						resetPlayRenderCache();
						namespace.commit();
					}			
				});						
		
		file_menu.addSeparator();
		
		JMenuItem exit_menuitem = file_menu.add(new JMenuItem("Exit"));
		exit_menuitem.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0)
			{
				closing();
				if(close_session) 
					System.exit(0);
				else
					setVisible(false);
			}});	
		
		JMenu[] menus = new JMenu[p_menubar.getMenuCount()];
		for (int i = 0; i < p_menubar.getMenuCount(); i++) 
			menus[i] = p_menubar.getMenu(i);		
		p_menubar.removeAll();
		
		for (int i = 0; i < menus.length; i++) 
			menubar.add(menus[i]);

		JMenu play_menu = menubar.add(new JMenu("Play"));
		
		JMenuItem play_realtime = new JMenuItem("RealTime");
		play_realtime.setAccelerator(KeyStroke.getKeyStroke("F9"));
		play_realtime.setMnemonic('R');
		
		JMenuItem play_play = new JMenuItem("Play");
		play_play.setAccelerator(KeyStroke.getKeyStroke("F5"));
		play_play.setMnemonic('P');
		
		JMenuItem play_stop = new JMenuItem("Stop");
		play_stop.setAccelerator(KeyStroke.getKeyStroke("F7"));
		play_stop.setMnemonic('S');
		
		play_realtime.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				playRealTime();
			}});		
		play_play.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				playPlay();
			}});		
		play_stop.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				playStop();
			}});
		
		play_menu.add(play_realtime);
		play_menu.add(play_play);
		play_menu.add(play_stop);
		
		JMenu help_menu = menubar.add(new JMenu("Help"));
		
		help_menu.add(new JMenuItem("About RasmusDSP...")).addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0) {
				new AboutDialog(RasmusEditor.this).setVisible(true);
			}});		
				
		
		return menubar;
	}
	
	
	public void setCursor(Cursor cursor)
	{
		super.setCursor(cursor);
		if(editorpanel != null)
		if(editorpanel.editors != null)
		{
			if(cursor == Cursor.getDefaultCursor())
				cursor = Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR);
			((ScriptEditor)(editorpanel.editors.get(0))).textarea.setCursor(cursor);
		}
		
	}
	
	public void commit()
	{	
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		
		model.commit();							
		doc.startAll();
		namespace.commit();
			
		setCursor(Cursor.getDefaultCursor());
	}
	
	public static String readScript(File file) throws UnsupportedEncodingException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is;
		try {
			is = new FileInputStream(file);
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
			return null;
		} 

		try
		{
			try
			{
				byte[] buffer = new byte[1024];
				int len = 0;
				while((len = is.read(buffer)) != -1)
				{
					baos.write(buffer,0, len);
				}
			}
			finally
			{
				is.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
		
		return new String(baos.toByteArray(), "LATIN1");
	}
	
	public void load(File file)
	{
				
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is;
		try {
			is = new FileInputStream(file);
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
			return;
		} 

		try
		{
			try
			{
				byte[] buffer = new byte[1024];
				int len = 0;
				while((len = is.read(buffer)) != -1)
				{
					baos.write(buffer,0, len);
				}
			}
			finally
			{
				is.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		resetPlayRenderCache();
		
		try {
			doc.setString(new String(baos.toByteArray(), "LATIN1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}		

	}
	
	public void load(String script)
	{		
		resetPlayRenderCache();
		doc.setString(script);			
	}
	
	ObjectEditor editorpanel;
	RasmusListModel model = new RasmusListModel();
	JComboBox combobox;
	
	String currentobject = null;	
	public void showObject(String objectname)
	{
		editorpanel.commit();
		
		if(objectname == null) objectname = "";
		if(currentobject != null)
			if(currentobject.equals(objectname)) return;
		currentobject = objectname;
		
		if(objectname.length() == 0) objectname = null;
		editorpanel.setObjectScope(objectname);
	}
	
	String programtitle = "RasmusDSP";
	String synthtitle = null;
	File filename = null;
	
	public void updateTitle()
	{
		if(synthtitle != null)
		{
			setTitle(synthtitle);
			return;
		}
		if(filename == null) 
			setTitle("untitled.rasmusdsp - " + programtitle);
		else
			setTitle(filename.getName() + " - " + programtitle);
	}
	
	
	Map<String, VariableRenderedInstance> renderedvariables = new HashMap<String, VariableRenderedInstance>();
	class VariableRenderedInstance
	{
		Interpreter vinterpreter;
		Variable var;
		public VariableRenderedInstance(String name)
		{
			vinterpreter = new Interpreter(namespace);
			vinterpreter.setAutoCommit(false);
			try {
				var = vinterpreter.eval("defaultrender(" + name + ")");
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}
		}
		public void close()
		{
			vinterpreter.close();
		}
		public Variable getRenderedInstance()
		{
			return var;
		}
	}	
	
	public void resetPlayRenderCache()
	{
		Iterator<VariableRenderedInstance> iter = renderedvariables.values().iterator();
		while (iter.hasNext()) 
			iter.next().close();
		renderedvariables.clear();				
	}
	
	public Variable getRenderedInstance(String name)
	{
		name = name.toLowerCase();
		VariableRenderedInstance vari = renderedvariables.get(name);
		if(vari == null)
		{
			vari = new VariableRenderedInstance(name);
			renderedvariables.put(name, vari);
		}
		return vari.getRenderedInstance();
	}
	
	volatile boolean s1_active = false;
	volatile boolean s2_active = false;
	volatile long s1_count = -1;
	volatile long s2_count = -1;
	
	Receiver sync1 = new Receiver()
	{
		public void send(MidiMessage arg0, long arg1) {
			byte[] msg = arg0.getMessage();
			if(msg.length == 1)
			{
				byte status = msg[0];
				
				if(status == ((byte)0xF9))
					s1_count += 1;
				else if(status == ((byte)ShortMessage.STOP))
					s1_active = false;
				else if(status == ((byte)ShortMessage.START))
					s1_active = true;

				
			}
		}
		public void close() {
		}
	};
	Receiver sync2 = new Receiver()
	{
		public void send(MidiMessage arg0, long arg1) {
			byte[] msg = arg0.getMessage();
			if(msg.length == 1)
			{
				byte status = msg[0];

				if(status == ((byte)0xF9))
					s2_count += 1;
				else if(status == ((byte)ShortMessage.STOP))
					s2_active = false;
				else if(status == ((byte)ShortMessage.START))
					s2_active = true;
				
			}
		}
		public void close() {
		}
	};	
	Interpreter playernamespace = new Interpreter();
	{
		MidiSequence.getInstance(playernamespace.get("syncout1")).addReceiver(sync1);
		MidiSequence.getInstance(playernamespace.get("syncout2")).addReceiver(sync2);
	}
	
	BeatToTimeMapper b2t = null;
	
	ClockWatcher cw = null;
	
	ArrayList<TimePositionListener> timeposlisteners = new ArrayList<TimePositionListener>();
	TimePositionListener[] timeposlistenersA = null;
	double lasttime = 0;
	public void addTimePositionListener(TimePositionListener listener)
	{
		timeposlisteners.add(listener);
		timeposlistenersA = new TimePositionListener[timeposlisteners.size()];
		timeposlisteners.toArray(timeposlistenersA);
	}
	public void removeTimePosListener(TimePositionListener listener)
	{
		timeposlisteners.add(listener);
		timeposlistenersA = new TimePositionListener[timeposlisteners.size()];
		timeposlisteners.toArray(timeposlistenersA);
	}
	
	class StatusUpdater implements Runnable
	{
		String statustext;
		double time;
		double beat;		
		public StatusUpdater(String statustext, double time, double beat)
		{
			this.statustext = statustext;
			this.time = time;
			this.beat = beat;
		}
		public void run()
		{
			if(playing)
			{
				statuslabel.setText(statustext);
				if(time != lasttime)
				{
				for (int i = 0; i < timeposlistenersA.length; i++) {
						timeposlistenersA[i].positionChange(time, beat);
				}
				lasttime = time;
				}
			}
		}		
	}
		
	boolean play_hasaudio = false;
	
	class ClockWatcher extends Thread
	{		
		transient boolean active = true;
				
		public void run() 
		{
			while(active)
			{
				long s1_count = RasmusEditor.this.s1_count;
				long s2_count = RasmusEditor.this.s2_count;
				boolean s1_active = RasmusEditor.this.s1_active;
				boolean s2_active = RasmusEditor.this.s2_active;
				if(!s1_active) s1_count = -1;
				if(!s2_active) s2_count = -1;
				
				String mode = "Stop";
				if(s1_count != -1 && s2_count == -1)
					mode = "Audio";
				else if(s1_count != -1 && s2_count != -1)
					mode = "Audio";
				else if(s1_count == -1 && s2_count != -1)
					mode = "MIDI";
					
				
				long mcount = s1_count;
				if(!play_hasaudio) mcount = s2_count;
				//if(mcount == -1) mcount = s2_count;
				

				String statustext;
				double time = -1;
				double beat = -1;					

				
				if(mcount == -1)
					statustext = "";
				else
				{
					
					time = mcount / 100.0;
					beat = b2t.getBeat(time);					
					
					int csec = (int)((mcount / 10) % 10);
					mcount = mcount / 100;
					
					long sec = mcount % 60;
					long min = mcount / 60;
					StringBuffer sb = new StringBuffer();
					
					sb.append(mode);
					sb.append("  ");
					sb.append(Long.toString(min));		
					sb.append(":");						
					if(sec < 10)
						sb.append("0");
					sb.append(Long.toString(sec));
					sb.append(".");
					sb.append(Integer.toString(csec));
					sb.append(" - ");
					sb.append((int)(beat+1));
										
					statustext = sb.toString();
				}
											
				try {
					SwingUtilities.invokeLater(new StatusUpdater(statustext, time, beat));
					Thread.sleep(50);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}
			}
		}
		public void close()
		{
			active = false;
			try {
				join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createPlayInterpreter(String name)
	{
		s1_active = false;
		s2_active = false;
		s1_count = -1;
		s2_count = -1;
		
		
		
		//RVariable input = globalinterpreter.get(name);
		
		if(playinterpreter != null) playinterpreter.close();
		playinterpreter = new Interpreter(playernamespace);
		playinterpreter.setAutoCommit(false);
		Variable ri = getRenderedInstance(name);
		b2t = AudioEvents.getBeatToTimeMap(ri);
		play_hasaudio = AudioEvents.getInstance(ri).getObjects().size() != 0;
		playinterpreter.add("renderinput", ri);
		//playinterpreter.add("input", input);
		//playinterpreter.getNameSpace().registerAsPrivate("defaultrender");
		playinterpreter.registerAsPrivate("defaultoutputs");
		//playinterpreter.add("defaultrender", globalinterpreter.get("defaultrender"));
		playinterpreter.add("defaultoutputs", namespace.get("defaultoutputs"));		
		try {
			playinterpreter.eval("defaultoutputs() <- renderoutput;");
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}
		try {
			playinterpreter.eval("renderoutput <- AudioPlayer(controlinput, syncout=syncout1) <- renderinput;");
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}
		try {
			playinterpreter.eval("renderoutput <- MidiPlayer(controlinput, syncout=syncout2) <- renderinput;");
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}
		//playinterpreter.eval("renderinput <- DefaultRender() <- input;");		
		controlinput = MidiSequence.getInstance(playinterpreter.get("controlinput"));
		
		cw = new ClockWatcher();
		cw.setPriority(Thread.MIN_PRIORITY);
		cw.start();
		
	}
	
	public void createRealTimeInterpreter()
	{
		if(realtimeinterpreter != null) realtimeinterpreter.close();
		realtimeinterpreter = new Interpreter();
		realtimeinterpreter.setAutoCommit(false);
		realtimeinterpreter.add("input", realtimeinput);
		realtimeinterpreter.registerAsPrivate("defaultrender");
		realtimeinterpreter.registerAsPrivate("defaultoutputs");
		realtimeinterpreter.add("defaultrender", namespace.get("defaultrender"));
		realtimeinterpreter.add("defaultoutputs", namespace.get("defaultoutputs"));
		
		try {
			realtimeinterpreter.eval("defaultoutputs() <- renderinput;");
		} catch (ScriptParserException e1) {
			e1.printStackTrace();
		}
		try {
			realtimeinterpreter.eval("renderinput <- DefaultRender() <- input;");
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}		
	}
	
	Interpreter realtimeinterpreter = null;	
	Interpreter playinterpreter = null;	
	Receiver controlinput;
	Variable realtimeinput = new Variable();
	JLabel statuslabel;
	
	public Variable getRealTimeInput()
	{
		return realtimeinput;
	}
		
	public void playRealTime()
	{
		playStop();
		editorpanel.commit();
		
		createRealTimeInterpreter();
		namespace.commit();
		
		statuslabel.setText("RealTime");
		
	}	
	
	boolean playing = false;
	public void playPlay()
	{	

		playStop();
				
		for (int i = 0; i < timeposlistenersA.length; i++) {
			timeposlistenersA[i].start();
		}		
		
		playing = true;
		String sel = (String)combobox.getSelectedItem();
		if(sel == null || sel.trim().length() == 0) sel = "output";
		
		editorpanel.commit();
				
		createPlayInterpreter(sel);
		namespace.commit();
		
		try {
			ShortMessage msg = new ShortMessage();
			msg.setMessage(ShortMessage.START);
			controlinput.send(msg, -1);
			/*
			ShortMessage msg2 = new ShortMessage();
			msg2.setMessage(ShortMessage.TIMING_CLOCK);
			controlinput.send(msg2, -1);*/

		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}		
		
		//statuslabel.setText("Play");
	}
	public void playStop()
	{	
		
		playing = false;
		if(cw != null)
		{
			cw.close();
			cw = null;
		}
		
		if(controlinput != null)
		{
		try {
			ShortMessage msg = new ShortMessage();
			msg.setMessage(ShortMessage.STOP);
			controlinput.send(msg, -1);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
		/*
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
		}
		
		if(playinterpreter != null)
		{
			playinterpreter.close();
			controlinput = null;
			playinterpreter = null;
		}
		if(realtimeinterpreter != null)
		{
			realtimeinterpreter.close();
			realtimeinterpreter = null;
		}
		namespace.commit();
		
		statuslabel.setText("Stop");
		
		for (int i = 0; i < timeposlistenersA.length; i++) {
			timeposlistenersA[i].positionChange(0, 0);
			timeposlistenersA[i].stop();
		}		
	}
	
	public void closing()
	{
		if(close_session) playStop();
		if(close_session) editorpanel.closing();

		if(close_session) session.close();
	}
	
	
	RasmusSession session;
	boolean close_session = true;
	
	public RasmusEditor() 
	{	
		session = new RasmusSession();
		init();		
	}
	public RasmusEditor(RasmusSession session) 
	{	
		this.session = session;
		close_session = false;
		init();		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	public void init()
	{
		
		this.doc = session.getScriptDocument();
		this.namespace = session.getNameSpace();

		
		resetPlayRenderCache();
		
		setSize(800, 600);	
		setLocationByPlatform(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		
		setTitle("untitled.rasmusdsp - " + programtitle);		
		setIconImage(new javax.swing.ImageIcon(getClass().getResource("/rasmus/rasmusdsp.PNG")).getImage());
		
		addWindowListener(new WindowListener()
				{
					public void windowOpened(WindowEvent arg0) {
					}
					public void windowClosing(WindowEvent arg0) {
						closing();
					}
					public void windowClosed(WindowEvent arg0) {
					}
					public void windowIconified(WindowEvent arg0) {
					}
					public void windowDeiconified(WindowEvent arg0) {
					}
					public void windowActivated(WindowEvent arg0) {
					}
					public void windowDeactivated(WindowEvent arg0) {
					}
				});

		
		//getClass().getResourceAsStream("/rasmus/testing/example.r");		
		///load(new File("S:\\Users\\kalli\\Desktop\\cango.rasmusdsp"));
		
		setLayout(new BorderLayout());
				
		editorpanel = new ObjectEditor(this);

		combobox = new JComboBox(model);
		combobox.setEditable(true);		
		combobox.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						String sel = (String)combobox.getSelectedItem();
						showObject(sel);	
					}			
				});
		Dimension size = combobox.getPreferredSize();		
		size.width = 200;
		combobox.setPreferredSize(size);
		
		JToolBar toppanel = new JToolBar();
		toppanel.setRollover(true);
		toppanel.setFloatable(false);
		toppanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		toppanel.add(new JLabel("Object: "));
		toppanel.add(combobox);
	
		//ImageIcon play_icon = new javax.swing.ImageIcon(getClass().getResource("/rasmus/editor/play.gif"));
		//ImageIcon stop_icon = new javax.swing.ImageIcon(getClass().getResource("/rasmus/editor/stop.gif"));
		
		toppanel.addSeparator();
		
		JButton realtime_button = new JButton("Realtime");
		realtime_button.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						playRealTime();
					}
				});
		JButton play_button = new JButton("Play");
		play_button.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						playPlay();
					}
				});		
		JButton stop_button = new JButton("Stop");
		stop_button.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						playStop();
					}
				});
		
		toppanel.add(realtime_button);
		toppanel.add(play_button);
		toppanel.add(stop_button);
		
		statuslabel = new JLabel("Stop");
		Dimension prefdim = statuslabel.getPreferredSize();
		prefdim.width = 150;
		statuslabel.setPreferredSize(prefdim);
		
		JPanel timepanel = new JPanel();
		timepanel.setBackground(new JEditorPane().getBackground());
		timepanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		timepanel.add(statuslabel);
		toppanel.add(timepanel);
		
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(toppanel, BorderLayout.SOUTH);
		panel.add(editorpanel, BorderLayout.CENTER);		
		
		setContentPane(panel);

		showObject("");
		editorpanel.init();
		
		//setVisible(true);
	}
	
		
	public static void main(String[] args) {
		/*
		try {
			javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
			//javax.swing.UIManager.setLookAndFeel(new MetalLookAndFeel());
			//MetalLookAndFeel.setCurrentTheme(new OceanTheme());			
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		new RasmusEditor().setVisible(true);
	}

	public void setSynthName(String name) {
		synthtitle = name;
		updateTitle();
	}

	
}
