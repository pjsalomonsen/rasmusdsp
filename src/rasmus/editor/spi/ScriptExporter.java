package rasmus.editor.spi;

import java.awt.Component;
import java.io.File;

public interface ScriptExporter extends FileExporter {
	public boolean exportFromScript(Component parent, File file, String script);
}
