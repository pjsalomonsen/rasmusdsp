package rasmus.editor.spi;

import java.awt.Component;
import java.io.File;

public interface ScriptImporter extends FileConverter {
	public String importToScript(Component parent, File file);
}
