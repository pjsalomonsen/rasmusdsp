package rasmus.editor.spi;

import java.awt.Component;
import java.io.File;

import rasmus.interpreter.Variable;

public interface ObjectExporter extends FileExporter {
	public boolean exportFromObject(Component parent, File file, Variable variable);
}
