package rasmus.editor.spi;

import java.io.File;

public interface FileExporter extends FileConverter {
	
	public File setFileExtension(File file);
	
}
