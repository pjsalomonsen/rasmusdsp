package rasmus.editor.spi;

import java.io.File;

public interface FileConverter {	
	public String getDescription();
	public boolean accept(File file);
}