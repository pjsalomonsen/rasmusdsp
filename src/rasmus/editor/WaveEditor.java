/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import rasmus.interpreter.Variable;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;


class WavePanel extends JPanel
{

	private static final long serialVersionUID = 1L;
	
	protected void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);
		
		if(isreading) return;
						
		Rectangle clip = graphics.getClipBounds();
		
		int height = getHeight();
		
		int from = clip.x;
		int to = from + clip.width;

		try {

			randomfile.seek(from);
		} catch (IOException e) {
			e.printStackTrace();
		}

		int bufflen = to - from;
		ByteBuffer bytebuffer = ByteBuffer.allocate(bufflen*8);
		int ret;
		try {
			ret = randomfile.read(bytebuffer.array(), 0, (to - from)*8);
		} catch (IOException e) {
			e.printStackTrace();
			ret = 0;
		}
		double[] dbuffer = new double[bufflen];
		bytebuffer.asDoubleBuffer().get(dbuffer);
		
		bufflen = ret / 8;		
		
		graphics.setColor(Color.GREEN);
		for (int i = 0; i < bufflen; i++) {
			int x = i + from;
			int y = (int)(height*(-dbuffer[i]*0.5+0.5)); 
			graphics.drawLine(x, y, x, y);
		}		
		
		
		if(!eof)
		{
			if(totalsize < to)
			{
				read(to - totalsize);
			}		
		}
	}

	public WavePanel()
	{
		setBackground(Color.BLACK);
		
		try {
			tempfile = File.createTempFile("rasmusdsp_", "_audiobuffer.float");			
			tempfile2 = File.createTempFile("rasmusdsp_", "_audiobuffer.float2");			
			randomfile = new RandomAccessFile(tempfile, "rw");
			randomfile2 = new RandomAccessFile(tempfile2, "rw");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setVariable(Variable variable)
	{
		
		//System.out.println("WavePanel.setVariable");
		
		reset();
		AudioSession session = new AudioSession(44100, 1);
		stream = session.openStream(variable);
		
		buffersize = 4096;
		buffersize -= (buffersize % session.getChannels()); 
		doublebuffer = new double[buffersize];
		bytebuffer = ByteBuffer.allocate(buffersize*8);
		doubleview = bytebuffer.asDoubleBuffer();
		
	
	}
	
	boolean eof = false;
	long writepos = 0;
	int totalsize = 0;
	int buffersize;
	double[] doublebuffer;
	ByteBuffer bytebuffer;
	DoubleBuffer doubleview;
	File tempfile;
	File tempfile2;
	
	RandomAccessFile randomfile;
	RandomAccessFile randomfile2;
	AudioSession session = null;
	AudioStream stream = null;
	
	boolean isreading = false;
	
	class WaveReader implements Runnable
	{
		int step = 1;
		long len;
		public void run()
		{
			if(step == 1) run1();
			if(step == 2) run2();
		}			
		public void run1()
		{
			readStream(len);
			step = 2;
			SwingUtilities.invokeLater(this);
		}
		public void run2()
		{
			isreading = false;
			repaint();
		}
	}
	
	public void read(long len)
	{
		if(isreseting) return;
		
		isreading = true;
		WaveReader runnable = new WaveReader();
		runnable.len = len;		
		new Thread(runnable).start();		
	}
	
	public void readStream(long len)
	{
		try {
			while(len > 0)
			{
							
			len -= buffersize;
			int ret = stream.replace(doublebuffer, 0, buffersize);
			if(ret == -1) 
			{
				stream.close();
				stream = null;
				eof = true;
				return;
			}			
			randomfile.seek(writepos);
			doubleview.position(0);
			doubleview.put(doublebuffer);			 
			randomfile.write(bytebuffer.array(), 0, ret*8);
			totalsize += ret;
			
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			eof = true;
		}
		
		repaint();
			
	}
	
	public void reset()
	{

		isreseting = true;
		
		while(isreading)
		{
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
		}		
		
		if(stream != null)
		{
			stream.close();
			stream = null;
		}
		if(session != null)
		{		
			session.close() ;
			session = null;
		}
		try {
			randomfile.setLength(0);
			randomfile2.setLength(0);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		writepos = 0;
		eof = false;
		totalsize = 0;
		isreseting = false;
				
	}
	
	boolean isreseting = false;
	
	public void close()
	{
		
		reset();			
		
		try {			
			randomfile.close();
			tempfile.delete();
			randomfile2.close();
			tempfile2.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}

public class WaveEditor extends JPanel implements EditorView {

	private static final long serialVersionUID = 1L;

	RasmusEditor editor;
	JMenuBar menubar;
	WavePanel wavepanel;
	
	public WaveEditor(RasmusEditor editor) 
	{
		this.editor = editor;
		
		setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		setOpaque(false);
		setLayout(new BorderLayout());		
		
		menubar = new JMenuBar();		
		menubar = editor.processMenuBar(menubar);	
		
		wavepanel = new WavePanel();
		add(wavepanel);
		
	}	
	
	public JComponent getJComponent() {
		return this;
	}

	public String getEditorName() {
		return "Waveform";
	}

	public boolean commit() {
		return false;
	}

	public void init() {
		editor.setJMenuBar(menubar);
		editor.commit();
	}

	String objectscope = null;
	public void setObjectScope(String objectscope) {
		this.objectscope = objectscope;
		
		if(objectscope == null || objectscope.length() == 0) objectscope = "output";
		
		wavepanel.setVariable(editor.namespace.get(objectscope));
		
	}

	public void closing() {
		wavepanel.close();
	}

}
