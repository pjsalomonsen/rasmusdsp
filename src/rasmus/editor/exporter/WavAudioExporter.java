package rasmus.editor.exporter;

import java.awt.Component;
import java.io.File;

import javax.swing.JOptionPane;

import rasmus.editor.spi.ObjectExporter;
import rasmus.interpreter.Variable;

public class WavAudioExporter implements ObjectExporter {

	public boolean exportFromObject(Component parent, File file, Variable variable) {
		JOptionPane.showMessageDialog(parent, "Wave Audio Export not implemented!!!");
		return false;
	}

	public boolean accept(File file)
	{
		return file.getName().toLowerCase().endsWith(".wav");
	}
	
	public String getDescription()
	{
		return "Wave Sound (*.wav)";
	}
	
	public File setFileExtension(File file)
	{
		if(file.getPath().toLowerCase().endsWith(".wav")) return file;
		return new File(file.getPath()+".wav");
	}
	

}
