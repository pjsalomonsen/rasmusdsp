/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import rasmus.interpreter.Closeable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.parser.ScriptCompiler;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.parser.ScriptTokenParser;

public class ScriptDocument {

	private NameSpace namespace;

	private ArrayList<ScriptObject> objectlist = new ArrayList<ScriptObject>();

	private Map<String, ScriptObject> objects = new HashMap<String, ScriptObject>();

	private Statement root = new Statement();
	private Statement last = root;
	
	public Statement getRoot()
	{
		return root;
	}	
	public Statement getFirst()
	{
		return root.getNext();
	}	
	public Statement getLast()
	{
		return last;
	}

	public class Statement 
	{
		private String code = null;
		private String exec_code = null;
		private ScriptObject object = null;
		private Statement previous = null;
		private Statement next = null;
		private Closeable instance = null;
		
		public ScriptObject getObject() 
		{
			return object;
		}
		
		public Statement getPrevious() 
		{
			return previous;
		}

		public Statement getNext() 
		{
			return next;
		}
		
		public String toString()
		{
			return code;
		}
		
		public void remove()
		{
			if(instance != null)
			{
				addToGarbage(exec_code, instance);
				instance = null;
			}
			if(object != null)
			{
				object.statements.remove(this);
				if(object.statements.size() == 0)
					object.remove();
			}			
			
			if(next != null) next.previous = previous;
			if(previous != null) previous.next = next;
			
			if(this == last) last = previous;
		}
		
		public Statement insertAfter(String code)
		{			
			Statement nextbak = next;
			next = new Statement();
			next.previous = this;
			next.next = nextbak;
			next.code = code;
			next.object = ScriptDocument.this.getObject(ScriptTokenParser.extractStatementResultVariable(code));

			if(next.object != null)
			{
				String ecode = ScriptTokenParser.removeCommentsAndWhiteSpace(next.code).trim();
				if(ecode.length() != 0)
					next.exec_code = ecode;
			}

			
			next.object.statements.add(next);
			if(nextbak != null)
				nextbak.previous = next;
			
			if(this == last) 
				last = next;
			
			return next;
		}
	}
	
	public class ScriptObject {
		private String name;

		protected ArrayList<Statement> statements = new ArrayList<Statement>();
		
		public ScriptObject(String name) {
			this.name = name;
		}
		
		public void add(String code)
		{
			if(statements == null || statements.size() == 0)
			{
				setString(code);
				return;
			}

			Statement current = statements.get(statements.size() - 1);
			
			String[] insertlines = ScriptTokenParser.seperateStatements(code);
			
			for (int i = 0; i < insertlines.length; i++) 
				current = current.insertAfter(insertlines[i]);
			
		}
		
		public List<Statement> getStatements()
		{
			return statements;
			                           
		}
		
		public String toNormalizedString()
		{
			String str = toString();
			int firsti = 0;
			for (int i = 0; i < str.length(); i++) {
				char c = str.charAt(i);
				if(!Character.isWhitespace(c))
				{
					firsti = i;
					break;
				}
			}
			int lasti = str.length();
			for (int i = str.length(); i > 0; i--) {
				char c = str.charAt(i-1);
				if(!Character.isWhitespace(c))
				{
					lasti = i;
					break;
				}
			}			
			return str.substring(firsti, lasti);			
		}
		
		public void setNormalizedString(String value)
		{
			if(statements.size() == 0)
			{
				setString("\n" + value);
				return;
			}
			
			String str = toString();
			int firsti = 0;
			for (int i = 0; i < str.length(); i++) {
				char c = str.charAt(i);
				if(!Character.isWhitespace(c))
				{
					firsti = i;
					break;
				}
			}
			int lasti = str.length();
			for (int i = str.length(); i > 0; i--) {
				char c = str.charAt(i-1);
				if(!Character.isWhitespace(c))
				{
					lasti = i;
					break;
				}
			}
			
			setString(str.substring(0, firsti) + value + str.substring(lasti, str.length()));
		}		

		public String toString() {

			StringBuffer buffer = new StringBuffer();
			Iterator<Statement> iterator = statements.iterator();
			while (iterator.hasNext()) {
				buffer.append(iterator.next().code);

			}
			return buffer.toString();
		}

		public void setString(String code) {
			
			String[] insertlines = ScriptTokenParser.seperateStatements(code);
			
			if (statements.size() == 0) {
				Statement current = last;
				for (int i = 0; i < insertlines.length; i++) 
					current = current.insertAfter(insertlines[i]);
				return;
			}

			Statement current = statements.get(0);
			while(current.object == this) current = current.previous;

			Statement[] list = statements.toArray(new Statement[statements.size()]);
			statements.clear();
			for (int i = 0; i < list.length; i++) 
				list[i].remove();
			
			for (int i = 0; i < insertlines.length; i++) 
				current = current.insertAfter(insertlines[i]);
						
		}

		public String getName() {
			return name;
		}

		public void remove() {
			Statement[] list = statements.toArray(new Statement[statements.size()]);
			statements.clear();
			for (int i = 0; i < list.length; i++) 
				list[i].remove();
			
			String lname = name;
			if (lname == null)
				lname = "";
			objects.remove(lname);
			objectlist.remove(this);
		}
	}

	public ScriptDocument() {
	}
	
	public void sanityCheck()
	{		
		ScriptDocument doc = this;
		Iterator<ScriptObject> oiter = doc.getObjects().iterator();
		while (oiter.hasNext()) {
			 Iterator<Statement> siter = oiter.next().statements.iterator();
			 while(siter.hasNext())
			 {
				 Statement pre = siter.next();

		Statement p_test = pre;
		Statement p_root = doc.getRoot();
		while(p_test != null)
		{
			if(p_test == p_root) break;
			p_test = p_test.getPrevious();
			if(p_test == null)
			{
				System.out.println("PRE IS INVALID, MAJOR ERROR INDEED!!!!");
				return;
			}				
		}
		if(pre != null)
		{
			p_test = p_root;
			while(p_test != null)
			{
				if(p_test == pre) break;
				p_test = p_test.getNext();					
			}
			if(p_test != pre)
			{
				System.out.println("PRE CHAIN IS INVALID, MAJOR ERROR INDEED!!!!");
				return;
				
			}
		}

			 }
		}
		
	}	

	public void setNameSpace(NameSpace namespace) {
		this.namespace = namespace;
	}

	public NameSpace getNameSpace() {
		return namespace;
	}

	public void clear() {
		stopAll();
		this.objectlist.clear();
		this.objects.clear();
		root.next = null;
	}
	
	private void clearAndKeepGarbage() {
		stopAllAndKeepGarbage();
		this.objectlist.clear();
		this.objects.clear();
		root.next = null;
	}
		
	
	private Map<String, Object> code_garbage = new HashMap<String, Object>();
	
	private void addToGarbage(String code, Closeable instance)
	{
		Object item = code_garbage.get(code);
		if(item == null)
		{
			code_garbage.put(code, instance);
		}
		else
		{
			if(item instanceof Closeable)
			{
				List list = new ArrayList();
				list.add(item);
				item = list;
			}			
			((List)item).add(instance);				
		}	
	}
	private Closeable getGarbage(String code)
	{
		Object item = code_garbage.get(code);
		if(item == null) return null;
		if(item instanceof Closeable)
		{
			code_garbage.remove(code);
			return (Closeable)item;
		}
		List list = (List)item;
		Closeable ret = (Closeable)list.remove(list.size() - 1);
		if(list.size() == 0) code_garbage.remove(code);
		return ret;		
	}
	
	private void clearGarbage()
	{
		Iterator iter = code_garbage.values().iterator();
		while (iter.hasNext()) 
		{
			Object garbage_instance = iter.next();
			if(garbage_instance instanceof Closeable)
			{
				((Closeable)garbage_instance).close();
			}
			else
			{
			if(garbage_instance instanceof List)
			{
				Iterator iter2 = ((List)garbage_instance).iterator();
				while (iter2.hasNext()) {
					((Closeable) iter2.next()).close();					
				}
			}
			}
		}			
		code_garbage.clear();		
	}

	public void startAll() {	
		Statement current = root;
		while((current = current.next) != null)
		{
			if(current.exec_code != null)
			if(current.instance == null)
			{
				try
				{
				try {					
					Closeable garbage_instance = getGarbage(current.exec_code);
					if(garbage_instance != null)
						current.instance = garbage_instance;
					else
						current.instance = ScriptCompiler.compile(current.code).execute(namespace);
				} catch (ScriptParserException e) {
					System.err.println(e.getMessage());
				}
				}
				catch(Throwable t)
				{
					t.printStackTrace();
				}
			}
		}
		
		clearGarbage();
	}
	
	private void stopAllAndKeepGarbage() {
		Statement current = root;
		while((current = current.next) != null)
		{
			if(current.instance != null)
			{
				addToGarbage(current.exec_code, current.instance);
				current.instance = null;
			}
		}	
	}

	public void stopAll() {
		Statement current = root;
		while((current = current.next) != null)
		{
			if(current.instance != null)
			{
				current.instance.close();
				current.instance = null;
			}
		}
		
		clearGarbage();	
	}

	public List<ScriptObject> getObjects() {
		return objectlist;
	}
	
	public ScriptObject findObject(String name) {
		String lname = name;
		if (lname == null)
			lname = "";		
		ScriptObject object = objects.get(lname);
		return object;
	}

	public ScriptObject getObject(String name) {
		String lname = name;
		if (lname == null)
			lname = "";
		ScriptObject object = objects.get(lname);
		if (object == null) {
			object = new ScriptObject(name);
			objects.put(lname, object);
			objectlist.add(object);
		}
		return object;
	}

	public void setString(String code) {
		clearAndKeepGarbage();
		
		Statement current = root;
		
		String[] statements = ScriptTokenParser.seperateStatements(code);
		
		for (int i = 0; i < statements.length; i++) 
			current = current.insertAfter(statements[i]);
		
		last = current;
						
	}

	public String toString() {
		
		StringBuffer buffer = new StringBuffer();				
		
		Statement current = root;
		while((current = current.next) != null)		
			buffer.append(current.code);
		
		
		return buffer.toString();
	}

}
