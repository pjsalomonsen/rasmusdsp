/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;

public class TextPopupMenu {

	JTextComponent textcomp;

	public static JPopupMenu createPopupMenu(JTextComponent comp) {

		TextPopupMenu textpopupmenu = new TextPopupMenu();
		textpopupmenu.textcomp = comp;
		textpopupmenu.init();
		return textpopupmenu.popupmenu;
	}

	JPopupMenu popupmenu;
	
	private static class PopupMenuListener implements MouseListener
	{
		JTextComponent comp;
		JPopupMenu textpopupmenu;
		
		public PopupMenuListener(JTextComponent comp)
		{
			this.comp = comp;
			textpopupmenu = TextPopupMenu.createPopupMenu(comp);
		}
		
		public void mouseClicked(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
			if(e.isPopupTrigger())
			{
				comp.grabFocus();
				textpopupmenu.show((JComponent) e.getSource(), e.getX(), e.getY());							
			}
		}		
	};
	
	public static JPopupMenu addPopupMenu(JTextComponent comp)
	{
		PopupMenuListener listener = new PopupMenuListener(comp);
		comp.addMouseListener(listener);	
		return listener.textpopupmenu;
	}

	private void init() {

		popupmenu = new JPopupMenu();

		Action undo_action = textcomp.getActionMap().get("Undo");

		if (undo_action != null) {
			JMenuItem menu_undo = new JMenuItem("Undo");
			menu_undo.setMnemonic('U');
			menu_undo.setAccelerator(KeyStroke.getKeyStroke("control Z"));
			menu_undo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					Action undo_action = textcomp.getActionMap().get("Undo");
					if (undo_action != null)
						undo_action.actionPerformed(arg0);
				}
			});
			popupmenu.add(menu_undo);
			popupmenu.addSeparator();
		}

		JMenuItem menu_cut = new JMenuItem("Cut");
		menu_cut.setMnemonic('t');
		menu_cut.setAccelerator(KeyStroke.getKeyStroke("control X"));
		menu_cut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textcomp.cut();
			}
		});
		JMenuItem menu_copy = new JMenuItem("Copy");
		menu_copy.setMnemonic('c');
		menu_copy.setAccelerator(KeyStroke.getKeyStroke("control C"));
		menu_copy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textcomp.copy();
			}
		});
		JMenuItem menu_paste = new JMenuItem("Paste");
		menu_paste.setMnemonic('p');
		menu_paste.setAccelerator(KeyStroke.getKeyStroke("control V"));
		menu_paste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textcomp.paste();
			}
		});

		JMenuItem menu_delete = new JMenuItem("Delete");
		menu_delete.setMnemonic('d');
		menu_delete.setAccelerator(KeyStroke.getKeyStroke("DELETE"));
		menu_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textcomp.replaceSelection("");
			}
		});

		JMenuItem menu_selectAll = new JMenuItem("Select All");
		menu_selectAll.setMnemonic('A');
		menu_selectAll.setAccelerator(KeyStroke.getKeyStroke("control A"));
		menu_selectAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textcomp.selectAll();
			}
		});

		popupmenu.add(menu_cut);
		popupmenu.add(menu_copy);
		popupmenu.add(menu_paste);
		popupmenu.add(menu_delete);
		popupmenu.addSeparator();
		popupmenu.add(menu_selectAll);

	}

}
