/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import rasmus.editor.ScriptDocument.ScriptObject;
import rasmus.editor.ScriptDocument.Statement;
import rasmus.editor.TableEditor.ParsedTableModel.TableCol;
import rasmus.interpreter.Interpreter;
import rasmus.interpreter.midi.MidiKeyListener;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.parser.ScriptElement;
import rasmus.interpreter.parser.ScriptOptimizer;
import rasmus.interpreter.parser.ScriptParser;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.parser.ScriptTokenizer;
import rasmus.util.ABCTokenizer;

public class TableEditor extends JPanel implements EditorView {

	private static final long serialVersionUID = 1L;

	JScrollPane scrollpane;
	JTable table;
	String objectscope = null;
	RasmusEditor editor;
	
	static String[] scale_items = 
		{"1/48",
        "1/36", 
        "1/32",
        "1/24", 
        "1/16", 
        "1/12", 
        "1/9", 
        "1/8", 
        "1/4", 
        "1",
        "4",
        "8",
        "12",
        "16"};

	static double[] scales = 
		        {1.0/48.0,
		        1.0/36.0, 
		        1.0/32.0,
		        1.0/24.0, 
		        1.0/16.0, 
                1.0/12.0, 
                1.0/9.0, 
                1.0/8.0, 
                1.0/4.0, 
                1.0,
                4.0,
                8.0,
                12.0,
                16.0};

	static int[] abcscales = 
        {4*48,
        4*36, 
        4*32,
        4*24, 
        4*16, 
        4*12, 
        4*9, 
        4*8, 
        4*4, 
        4*1,
        1,
        1,
        1,
        1}; 
	
	JCheckBoxMenuItem[] zoom_items = new JCheckBoxMenuItem[scale_items.length];
		
	public boolean commit()
	{
		if(model != null)
		if(model.changed)
		{
		
			Statement pre = null;
			ScriptDocument doc = editor.doc;
			ScriptObject object = doc.getObject(objectscope);
									
			if(object.getStatements().size() != 0)
			{
				pre = object.getStatements().get(0);
				while(pre.getObject() == object)
					pre = pre.getPrevious();
			}
					
			// 1. Remove parsed statements
			Iterator<Statement> iterator = model.parsedstatements.iterator();
			while (iterator.hasNext()) {
				Statement st = iterator.next();				
				st.remove();				
			}
			
			if(object.getStatements().size() != 0)
			{
				pre = object.getStatements().get(0);
				Statement next = pre;
				do
				{
					pre = next;
					next = pre.getNext();
					if(next == null) break;
				}
				while(next.getObject() == object);
			}
			
			if(pre == null) pre = doc.getLast();
			
			
			StringBuffer abcbuffer = new StringBuffer();
			String abc_legalSymbols = "zxabcdefgABCDEFG";
			Map<Double, Integer> delaytimetable = new HashMap<Double, Integer>();
			int delaycolid = 0;
			
			// Remove empty columns
			for (int i = model.cols.size() - 1; i >= 0; i--) {
				TableCol col = model.cols.get(i);
				if(col.rows.size() != 0) break;
				model.cols.remove(i);
			}
			
			Iterator<TableCol> citer = model.cols.iterator();
			while (citer.hasNext()) {
				TableCol col = citer.next();
				if(col.changed || (col.delaystatements != null) || (col.delaystatements == null && col.abcstatement == null))
				{

					// 1. Check for ABC note data

					boolean abc_used = false;
					boolean delay_used = false;
					{
						
						int abc_scale = model.abcscale; 						
						double pos = 0;
						
						abcbuffer.setLength(0);
						
						abcbuffer.append("[L:1/"+abc_scale+"]");
						
						boolean first_item = true;
						Iterator<Entry<Double, String>> criter = col.rows.entrySet().iterator();
						StringBuffer cmdpart = new StringBuffer();
						StringBuffer notepart = new StringBuffer();
						while (criter.hasNext()) {
							Entry<Double, String> element = criter.next();
							String val = element.getValue().trim();		
							double abcpos = element.getKey() * abc_scale / 4.0;
							if(val.startsWith("<-"))
							{
								delay_used = true;
							}
							else
							if(val.length() != 0)
							{
								cmdpart.setLength(0);
								notepart.setLength(0);
								for (int i = 0; i < val.length(); i++) {
									char c = val.charAt(i);
									if(c == '[') 
									{
										for (; i < val.length(); i++) {
											c = val.charAt(i);
											cmdpart.append(c);
											if(c == ']') break;
										}										
									}
									else
									if(!Character.isWhitespace(c))
										notepart.append(c);
								}
								
								double delta = abcpos - pos;
								String delta_str = ABCTokenizer.formatNoteLen(delta);
								pos = abcpos;
								
								if(first_item)
								{
									if(delta != 0)
									{
										abcbuffer.append('z');
										if(!delta_str.equals("1")) abcbuffer.append(delta_str);
									}
									first_item = false;
								}
								else
									if(!delta_str.equals("1")) abcbuffer.append(delta_str);
													
								String nt = notepart.toString();
								if(nt.length() == 0) nt = "z";
								
								if(cmdpart.length() != 0)
								{									
									abcbuffer.append(cmdpart);									
								}	
								
								if(!(nt.equalsIgnoreCase("z") && !criter.hasNext()))
								{
									    if(nt.length() > 0 && Character.isDigit(nt.charAt(0)))
									    {
									    	abcbuffer.append("[d:" + nt + "]z");
									    }
									    else
									    {

										int ncount = 0;
										for (int i = 0; i < nt.length(); i++) {
											char l = nt.charAt(i);
											if(abc_legalSymbols.indexOf(l) != -1)
												ncount++;
										}
										if(ncount > 1)
											abcbuffer.append("[" + nt + "]");
										else
											abcbuffer.append(nt);
										
									    }
								
								}
															
								abc_used = true;
								//System.out.println();
								//System.out.println(abcpos + " " + notepart.toString());
							}
						}
						
						if(abc_used)
						{
							String code = "\n" + objectscope + " <- abc(\"" + abcbuffer.toString() + "\");";
							pre = pre.insertAfter(code);					
						}
					}
					
					// 2. Check for Delay Data
					{
						Iterator<Entry<Double, String>> criter = col.rows.entrySet().iterator();
						while (criter.hasNext()) {
							Entry<Double, String> element = criter.next();
							String val = element.getValue().trim();						
							if(val.startsWith("<-"))
							{
								Double epos = element.getKey();
								String pos = Double.toString(epos);
								if(pos.endsWith(".0")) pos = pos.substring(0, pos.length() - 2);
								
								Integer cc = delaytimetable.get(epos);
								int c;
								if(cc == null) c = 0; else c = cc;
								for (int i = c; i < delaycolid; i++) {
									String code = "\n" + objectscope + " <- delay(" + pos + ");";
									pre = pre.insertAfter(code);
								}
								delaytimetable.put(epos, delaycolid+1);
								
								String code = "\n" + objectscope + " <- delay(" + pos + ") " + val + ";";
								pre = pre.insertAfter(code);							
							}						
						}
					}
					
					if(delay_used || !abc_used)
					{
						if(!delay_used)
						{
							Integer cc = delaytimetable.get(0.0);
							int c;
							if(cc == null) c = 0; else c = cc;
							for (int i = c; i < delaycolid+1; i++) {
								String code = "\n" + objectscope + " <- delay(0);";
								pre = pre.insertAfter(code);
							}
							delaytimetable.put(0.0, delaycolid+1);
							
						}
						delaycolid++;
					}
				
					
				}
				else
				{
					if(col.abcstatement != null)
					{
						pre = pre.insertAfter(col.abcstatement.toString());
					}
					if(col.delaystatements != null)
					{
						Iterator<Statement> diter = col.delaystatements.iterator();
						while (diter.hasNext()) {
							pre = pre.insertAfter(diter.next().toString());
						}
						delaycolid++;
					}
				}
			}
			
			// 3. Re-parse the data			
			parse();
			
			return true;
		}
		return false;
	}
	
	public JComponent getJComponent()
	{
		return this;
	}	
	
	public String getEditorName()
	{
		return "Pattern";
	}	
	
	class ParsedTableModel extends AbstractTableModel
	{
	
	private static final long serialVersionUID = 1L;
	List<TableCol> cols = new ArrayList<TableCol>();	
	List<Statement>  parsedstatements = new ArrayList<Statement>();
	
	boolean changed = false;
	class TableCol
	{
		
		boolean changed = false;
		
		int colid = 0;
		
		Statement abcstatement = null;
		List<Statement> delaystatements = null;
		
		TreeMap<Double,String> rows   = new TreeMap<Double,String>();
		
		public TableCol()
		{
			colid = cols.size();
			cols.add(this);
		}
		
		public void setValue(double row, String value)
		{
			if(!scale_set) calcRowScale(row);
			if(value != null) 
				if(value.trim().length() == 0) value = null;
					
			String lvalue = rows.get(row);
			if(lvalue == null)
			{
				if(value == null) return;
			}
			else
			{
				if(value != null)
				{
					if(lvalue.equals(value)) return;
				}
			}			
			
			if(value == null)
				rows.remove(row);
			else
				rows.put(row, value);
			
			if(active)
			{
				TableCol.this.changed = true;
				ParsedTableModel.this.changed = true;
			}
		}
		
		public String getValue(double row)
		{
			String value = rows.get(row);
			if(value == null) return "";
			return value;
		}
	}
	
	double scale = 1.0/4.0;	
	int abcscale = 16;
	

	
	int scale_index = scales.length - 1;
	double optimalscale = scales[scale_index];	
	
	
	boolean scale_set = false;
	boolean use_scale = false;
	
	public void calcRowScale(double row)
	{
		if(row > 0.000000001) use_scale = true;
		while( Math.abs(Math.rint(row / optimalscale) - (row/optimalscale)) > 0.000000001 )
		{
			if(scale_index == 0) return;
			scale_index--;
			optimalscale = scales[scale_index];
		}
	}
	
	public void calcScale()
	{
		if(!use_scale)
		{
			scale_index = 8;
		}
		
		scale = scales[scale_index];
		abcscale = abcscales[scale_index];
		
		scale_set = true;
	}
		
		
	boolean active = false;
	
	List<TableCol> delaycols = new ArrayList<TableCol>();
	Map<Double, Integer> delaytimetable = new HashMap<Double, Integer>();
	
	public TableCol getDelayCol(int row)
	{
		while(delaycols.size() <= row) delaycols.add(new TableCol());
		return delaycols.get(row);
	}
	
	public void parseDelayStatement(Statement statement, String delay, String value)
	{
		try {
			ScriptElement sp = ScriptParser.parse(delay.toString());
			sp = ScriptOptimizer.optimize(sp);
			if(sp.getElements() != null) sp = sp.getElements().get(0);
			if(sp.getType() == ScriptElement.TYPE_NUMBER)
			{
				Double time = (Double)sp.getValue();
				
				Integer d = delaytimetable.get(time);
				if(d == null) d = 0; else d += 1;				
				delaytimetable.put(time, d);			
				int col_index = d.intValue();
				
				TableCol col = getDelayCol(col_index);
				
				if(value != null)
				{
				value = value.trim();
				if(value.endsWith(";")) value = value.substring(0, value.length() - 1);
				col.setValue(time, "<- " + value);
				if(col.delaystatements == null) col.delaystatements = new ArrayList();
				col.delaystatements.add(statement);
				}
				
				parsedstatements.add(statement);
				
			}
			    
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}											
	}
	
	public void parseABCStatement(Statement statement, String value)
	{
		try {
			
			ScriptElement sp = ScriptParser.parse(value);
			sp = ScriptOptimizer.optimize(sp);
			if(sp.getElements() != null) sp = sp.getElements().get(0);
			if(sp.getType() == ScriptElement.TYPE_STRING)
			{
				TableCol col = new TableCol();
				col.abcstatement = statement;
				
				ABCTokenizer abct = new ABCTokenizer();
				List tokens = abct.parse((String)sp.getValue());
				
				double abc_notelen = 1.0 / 8.0;
				
				double lastpos = 0;
				for (int i = 0; i < tokens.size(); i++) {
					Object token = tokens.get(i);
					
					if(token instanceof ABCTokenizer.ABCToken)
					{
						ABCTokenizer.ABCToken ntoken = (ABCTokenizer.ABCToken)token;
						if(!(ntoken.pos == 0 && ntoken.toString().equalsIgnoreCase("z")))
						{
							String val = col.getValue(ntoken.pos);
							int li = val.indexOf("[");
							if(!(val.length() > 0 && Character.isDigit(val.charAt(0))))
							{
							if(li != -1)
							{
								if(!(ntoken.symbol == 'z' || ntoken.symbol == 'Z'))
								{
								if(li == 0)
									val = " " + val;
								else
								{
									if(val.charAt(li-1) == ' ') li--;
								}
									
								val = val.substring(0, li) +  ntoken.toString() + val.substring(li);
								
								}
							}
							else
								val += ntoken.toString();
														
							col.setValue(ntoken.pos, val);
							double endpos = ntoken.pos + ntoken.len;
							if(endpos > lastpos) lastpos = endpos; 
							
							}
							else
							{
								double endpos = ntoken.pos;
								if(endpos > lastpos) lastpos = endpos; 								
							}
							
							
							
						}
					}
					if(token instanceof ABCTokenizer.CommandToken)
					{						
						ABCTokenizer.CommandToken ntoken = (ABCTokenizer.CommandToken)token;
						if(!ntoken.fieldname.equals("L") && !ntoken.fieldname.equals("M"))
						{
							if(ntoken.fieldname.equals("d"))
							{
								String val = col.getValue(ntoken.pos);
								int li = val.indexOf("[");
								if(li != -1)
								{
									if(li == 0)
										val = " " + val;
									else
									{
										if(val.charAt(li-1) == ' ') li--;
									}
										
									val = val.substring(0, li) +  ntoken.fieldvalue + val.substring(li);
								}
								else
									val += ntoken.fieldvalue;
															
								col.setValue(ntoken.pos, val);								
							}
							else
							{
							
							String val = col.getValue(ntoken.pos);
							String code = ntoken.toString();	
							if(val.length() == 0) 
								val = "[" + code + "]";
							else
								val = val + " [" + code + "]";
							
							col.setValue(ntoken.pos, val);
							
							}
						}
						else
						if(ntoken.fieldname.equals("L"))
						{
							abc_notelen = ABCTokenizer.parseNoteLen(ntoken.fieldvalue.trim());
						}
					}
					
				}
				
				calcRowScale(abc_notelen*4);
				
				if(col.getValue(lastpos).length() == 0)
					col.setValue(lastpos, "z");
								
				parsedstatements.add(statement);
			}
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}											
		
		
	}
	
	public void parseStatement(Statement statement, String st)
	{
		ScriptTokenizer tokens = new ScriptTokenizer(st);
		
		if(tokens.hasMoreTokens())
		{
			String outputvar = "";
			String token = tokens.nextToken();
			if(token.equals("<-")) 
				outputvar = "output";
			else
				if(tokens.hasMoreTokens())
				{
					outputvar = token;
					token = tokens.nextToken();
				}
			if(token.equals("<-"))
			if(objectscope.equalsIgnoreCase(outputvar))
			if(tokens.hasMoreTokens())
			{
				String funcname = tokens.nextToken();
				if(tokens.hasMoreTokens())
				if(tokens.nextToken().equals("("))
				{
					if(funcname.equalsIgnoreCase("abc"))
					{
						
						boolean ok = true;
						StringBuffer parametercontent = new StringBuffer();
						while(tokens.hasMoreTokens())								
						{
							token = tokens.nextToken();
							if(token.equals(")")) break;
							if(token.equals(","))
							{
								ok = false;
								break;
							}
							parametercontent.append(token);
						}
						if(ok)
						if(tokens.hasMoreTokens())
						if(tokens.nextToken().equals(";"))
						{
							parseABCStatement(statement, parametercontent.toString());
						}
					}
					else
					if(funcname.equalsIgnoreCase("delay"))
					{
						boolean ok = true;
						
						StringBuffer parametercontent = new StringBuffer();
						int pi = 1;
						while(tokens.hasMoreTokens())								
						{
							token = tokens.nextToken();
							if(token.equals("(")) pi++;
							if(token.equals(")")) pi--;
							if(pi == 0) break;
							parametercontent.append(token);
						}
						
						String content = null;
						if(tokens.hasMoreTokens())
						{
							String ntoken = tokens.nextToken();
							if(ntoken.equals("<-"))
							{
								int index = tokens.nextIndex();
								content = st.substring(index);
							}
							else
							if(!ntoken.equals(";"))
								ok = false;
						}
						
						if(ok)
						{
							parseDelayStatement(statement, parametercontent.toString(), content);
						}
						
					}
				}
			}
		
		}
	}

	public int getRowCount() {
		return 300;
	}

	public int getColumnCount() {
		return 100;
	}
	
	public Object getValueAt(int row, int col) {
		
		if(cols.size() <= col) return "";
		if(col < 0) return "";
		TableCol tablecol = cols.get(col);;		
		return tablecol.getValue(((double)row)*scale);
	}
	public void setValueAt(Object value, int row, int col)
	{
		if(!(value instanceof String)) return;
        if(col < 0) return;
		while(cols.size() <= col) new TableCol();
		double r = row * scale; 
		if(value != null)
			if(((String)value).indexOf("[") != -1)
			{
				midi_setup_lastbeat = 0;
				midi_setup_lastcol = -1;
			}
		cols.get(col).setValue(r, (String)value);
	}
	
	public boolean isCellEditable(int rowIndex, int columnIndex) 
	{
		return true;
	}
	
	}
	
	ParsedTableModel model; 
	
	public void parse()
	{
		model = new ParsedTableModel();
		
		ScriptObject object = editor.doc.findObject(objectscope);
		if(object != null)
		{
		Iterator<Statement> iterator = object.getStatements().iterator();
		while (iterator.hasNext()) {
			Statement rst = iterator.next();
			model.parseStatement(rst, rst.toString());
		}
		}
		
		model.calcScale();
		model.active = true;
		table.setModel(model);
		
		posmodel.fireTableDataChanged();		
	}
	
	public void setObjectScope(String objectscope)
	{
		commit();
		this.objectscope = objectscope;		
		if(this.objectscope == null) this.objectscope = "output";
		

		parse();
		midi_setup_lastbeat = 0;
		midi_setup_lastcol = -1;
	}
	
	
	AbstractTableModel posmodel;
	
	MidiKeyListener keylistener = new MidiKeyListener();

	class SendMidiRunnable implements Runnable
	{
		MidiMessage msg;
		long tick;
		Receiver recv;
		public SendMidiRunnable(Receiver recv, MidiMessage msg, long tick)
		{
			this.recv = recv;
			this.msg = msg;
			this.tick = tick;
		}
		public void run() {
			recv.send(msg, tick);
		}		
	};
	Receiver recv_midiin = new Receiver()
	{
		public void send(MidiMessage msg, long tick) {
			SwingUtilities.invokeLater(new SendMidiRunnable(recv_midiin2, msg, tick));
		}

		public void close() {
		}
	};
	
	Receiver recv_midiin2 = new Receiver()
	{
		public void send(MidiMessage msg, long tick) {

			if(it.isInFileSelectionMode() || (it.tree.isVisible() && it.tree.hasFocus()))
			{
				it.recv_midiin.send(msg, tick);
				return;
			}
			
			if(!table.isVisible()) return;
			if(!table.hasFocus()) return;
			
			procSessMidiSetup();
			
			if(msg instanceof ShortMessage)
			{
				
				ShortMessage sms = (ShortMessage)msg;
				if((sms.getCommand() == ShortMessage.NOTE_ON) && (sms.getData2() > 0))
				{
					noteOn(sms.getData1(), sms.getData2());
				}
				
				if(sms.getCommand() == ShortMessage.NOTE_OFF
				  ||(sms.getCommand() == ShortMessage.NOTE_ON) && (sms.getData2() == 0))
				{
					noteOff(sms.getData1());
				}
								
			}
			
			sendMessage(msg);
		}
		public void close() {
		}
	};
	
	Receiver recv = new Receiver()
	{
		public void send(MidiMessage msg, long tick) {

			procSessMidiSetup();
			
			if(msg instanceof ShortMessage)
			{
				
				ShortMessage sms = (ShortMessage)msg;
				if((sms.getCommand() == ShortMessage.NOTE_ON) && (sms.getData2() > 0))
				{
					noteOn(sms.getData1(), 0);
					try {
						ShortMessage newmsg = new ShortMessage();
						newmsg.setMessage(sms.getCommand(), sms.getChannel(), sms.getData1(), midi_setup_v);
						msg = newmsg;					
					} catch (InvalidMidiDataException e) {
						e.printStackTrace();
					}
				}
				
				if(sms.getCommand() == ShortMessage.NOTE_OFF
				  ||(sms.getCommand() == ShortMessage.NOTE_ON) && (sms.getData2() == 0))
				{
					noteOff(sms.getData1());
				}
								
			}
			
			sendMessage(msg);
		}
		public void close() {
		}
	};
	
	
	int[] downnnotes = new int[127];
	{
		for (int i = 0; i < downnnotes.length; i++) {
			downnnotes[i] = i;
		}
	}
	public void sendMessage(MidiMessage msg)
	{
		if(msg instanceof ShortMessage)
			if(msg.getMessage().length != 1)
			{		

				try {
					ShortMessage sms = (ShortMessage)msg; 
					ShortMessage newmsg = new ShortMessage();
					int cmd = sms.getCommand();
					int data1 = sms.getData1();
					int data2 = sms.getData2();
					if(cmd == ShortMessage.NOTE_ON || cmd == ShortMessage.NOTE_OFF)
					{
						int odata1 = data1;
						if(cmd == ShortMessage.NOTE_OFF || data2 == 0)
						{
							data1 = downnnotes[odata1];
						}
						else
						{
							data1 =  data1 + ((midi_setup_o-4)*12);
							if(data1 < 0) data1 = 0;
							if(data1 > 127) data1 = 127;
							downnnotes[odata1] = data1;
						}
					}
					newmsg.setMessage(cmd, midi_setup_c-1, data1, data2);
					msg = newmsg;
				} catch (InvalidMidiDataException e) {
					e.printStackTrace();
				}
								
			}		
		midi_out.send(msg, -1);
	}
	
	{
		keylistener.setReceiver(recv);
	}
	
	
	double midi_setup_lastbeat = 0;
	int midi_setup_lastcol = -1;
	int midi_setup_c = 1;
	int midi_setup_v = 127;
	int midi_setup_o = 4;

	public String updateOnlyIfNotEmpty(String before, String after)
	{
		if(after.length() == 0) return before;
		return after;
	}
	
	public void procSessMidiSetup()
	{
		procSessMidiSetup(true);
	}
	
	public void procSessMidiSetup(boolean doins)
	{
		int row = table.getSelectedRow();
		int col = table.getSelectedColumn();
		if(col == -1) return;
		if(row == -1) return;		
		if(model == null) return;
		
		double beat = ((double)row)*model.scale;
		
		// public String extractParamValue(String paramname, String value)

		if(col != midi_setup_lastcol
		   || midi_setup_lastbeat > beat)
		{
			// Total reset
			
			midi_setup_c = 1;
			midi_setup_o = 4;
			midi_setup_v = 127;
			midi_setup_lastbeat = Integer.MIN_VALUE;
		}
		
		 
		if(model.cols.size() <= col) return;	
		TableCol tablecol = model.cols.get(col);;		
		TreeMap<Double, String> rows = tablecol.rows; 
		
		String str_c = null;
		String str_o = null;
		String str_v = null;
		String str_i = null;		
		//Map map; 
		Iterator<Entry<Double, String>> map = rows.subMap(midi_setup_lastbeat, beat).entrySet().iterator();
		while(map.hasNext())
		{
			Entry<Double, String> item = map.next();
			if(item.getKey() != midi_setup_lastbeat)
			{
				String pvalue = item.getValue();
				str_c = updateOnlyIfNotEmpty(str_c,extractParamValue("c", pvalue));
				str_o = updateOnlyIfNotEmpty(str_o,extractParamValue("o", pvalue));
				str_v = updateOnlyIfNotEmpty(str_v,extractParamValue("v", pvalue));
				str_i = updateOnlyIfNotEmpty(str_i,extractParamValue("i", pvalue));
			}
		}
		String item = rows.get(beat);
		if(item != null)
		{
			String pvalue = item;
			str_c = updateOnlyIfNotEmpty(str_c,extractParamValue("c", pvalue));
			str_o = updateOnlyIfNotEmpty(str_o,extractParamValue("o", pvalue));
			str_v = updateOnlyIfNotEmpty(str_v,extractParamValue("v", pvalue));
			str_i = updateOnlyIfNotEmpty(str_i,extractParamValue("i", pvalue));			
		}
		
		
		if(str_c != null && str_c.length() != 0)
		{
			try
			{
				midi_setup_c = Integer.parseInt(str_c);
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
		}
		
		if(str_o != null && str_o.length() != 0)
		{
			try
			{
				midi_setup_o = Integer.parseInt(str_o);
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
		}
		if(str_v != null && str_v.length() != 0)
		{
			try
			{
				midi_setup_v = Integer.parseInt(str_v);
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
		}		
		
		if(doins)
		if(str_i != null)
		{

		    String program = str_i;
		    String bank = null;
		    int li = str_i.indexOf(",");
		    if(li != -1)
		    {
		    	bank = program.substring(li+1);
		    	program = program.substring(0, li);
		    }
		    
		    if(bank != null)
		    {
		    	int i_bank = Integer.parseInt(bank);
		    	if(i_bank < 0) i_bank = 0;
		    	if(i_bank > 127*128) i_bank = 128*127;
		    	
		    	int m_bank = i_bank / 128;
		    	//if(m_bank != 0)
		    	{
					try {
						ShortMessage smsg = new ShortMessage();
						smsg.setMessage(ShortMessage.CONTROL_CHANGE, midi_setup_c-1, 0, m_bank);
						sendMessage(smsg);
					} catch (InvalidMidiDataException e) {
						e.printStackTrace();
					}
		    	}

		    	int l_bank = i_bank % 128;
		    	//if(l_bank != 0)
		    	{
					try {
		    		ShortMessage smsg = new ShortMessage();
					smsg.setMessage(ShortMessage.CONTROL_CHANGE, midi_setup_c-1, 32, l_bank);
					sendMessage(smsg);
					} catch (InvalidMidiDataException e) {
						e.printStackTrace();
					}
		    	}
		    	
		    }
			try {
		    int i_program = Integer.parseInt(program);
		    ShortMessage smsg = new ShortMessage();
			smsg.setMessage(ShortMessage.PROGRAM_CHANGE, midi_setup_c-1, i_program, 0);
			sendMessage(smsg);
			} catch (InvalidMidiDataException e) {
				e.printStackTrace();
			}
			
		}
		
		midi_setup_lastcol = col;
		midi_setup_lastbeat = beat;
					
	}
	
	StringBuffer notebuffer = new StringBuffer();
	public void noteOn(int note, int vel)
	{
		if(chord_input_mode)
		{
			notebuffer.append(convertNote(note-48));
			setNote(notebuffer.toString());
			if(vel != 0)
			{
				setParam("v", Integer.toString(vel));
			}
		}
		else
		{
			if(vel != 0)
			{
				setParam("v", Integer.toString(vel));
			}			
			enterNote(convertNote(note-48));
		}
	}
	public void noteOff(int note)
	{	
		if(!chord_input_mode) return;
		if(notebuffer.length() != 0)
		{
			enterNote(notebuffer.toString());
			notebuffer.setLength(0);
		}
	}
	
	int stepsize = 1;
	
	String[] basenotes = {"C","^C","D","^D","E","F","^F","G","^G","A","^A","B"};
	
	public String convertNote(int in)
	{
				
		int note = in % 12;
		if(note < 0) note += 12;
		int octave = (in - note) / 12;
		
		StringBuffer base = new StringBuffer(basenotes[note]);
		if(octave > 0)
		{
			octave--;
			base = new StringBuffer(base.toString().toLowerCase());
		}
		while(octave > 0) { base.append('\''); octave--; }
		while(octave < 0) { base.append(','); octave++; }
		return base.toString();	
	}
	
	public String extractParamValue(String paramname, String value)
	{
		StringBuffer parambuff = new StringBuffer();
		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			if(c == '[')
			{
				parambuff.setLength(0);
				for (; i < value.length(); i++) {
					c = value.charAt(i);
					parambuff.append(c);
					if(c == ']') break;
				}		
				String p = parambuff.toString();
				if(p.startsWith("["+paramname+":"))
				{
					int li = p.indexOf(":");
					p = p.substring(li+1);
					p = p.substring(0,p.length() - 1).trim();
					return p;
				}
			}
		}		
		return "";		
	}
	
	public String getParam(String paramname)
	{
		int row = table.getSelectedRow();
		int col = table.getSelectedColumn();
		if(row == -1) return "";
		if(col == -1) return "";
		String value = ((String)table.getValueAt(row, col)).trim();
		if(value.startsWith("<-")) return "";
		return extractParamValue(paramname, value);
	}
	
	public void setParam(String paramname, String paramvalue)
	{
		if(stop_param_update) return;
		int row = table.getSelectedRow();
		int col = table.getSelectedColumn();
		
		if(row == -1) return;
		if(col == -1) return;		
		paramvalue = paramvalue.trim();
		String value = ((String)table.getValueAt(row, col)).trim();
		String code = "["+paramname+":"+paramvalue+"]";
		if(value.length() == 0 || value.startsWith("<-"))
		{
			if(paramvalue.length() == 0) return;
			table.setValueAt(code, row, col);
		}
		else
		{
			StringBuffer notepart = new StringBuffer();
			StringBuffer cmdpart = new StringBuffer();
			StringBuffer parambuff = new StringBuffer();
			boolean paramset = false; 
			for (int i = 0; i < value.length(); i++) {
				char c = value.charAt(i);
				if(c == '[')
				{
					parambuff.setLength(0);
					for (; i < value.length(); i++) {
						c = value.charAt(i);
						parambuff.append(c);
						if(c == ']') break;
					}		
					String p = parambuff.toString();
					if(p.startsWith("["+paramname+":"))
					{
						paramset = true;
						if(paramvalue.length() != 0)
						{
							if(cmdpart.length() != 0) cmdpart.append(' ');					
							cmdpart.append(code);
						}
					}
					else
					{
						if(cmdpart.length() != 0) cmdpart.append(' ');					
						cmdpart.append(p);
					}
				}
				else
				if(!Character.isWhitespace(c))
				{
					notepart.append(c);
				}
			}
			if(!paramset)
			{				
				if(paramname.equals("c"))
				{
					if(cmdpart.length() != 0)
						cmdpart.insert(0, code + " ");
					else
						cmdpart.append(code);
				}
				else
				{				
					if(cmdpart.length() != 0) cmdpart.append(' ');
					cmdpart.append(code);
				}
			}
			if(notepart.length() == 0)
				 code = cmdpart.toString(); 
			else
				code = notepart.toString() + " " + cmdpart.toString();			
			table.setValueAt(code, row, col);
			
		}
		table.tableChanged(new TableModelEvent(table.getModel(), row, row, col));
		
	}
	
	public void setNote(String code)
	{
		int row = table.getSelectedRow();
		int col = table.getSelectedColumn();
		String value = ((String)table.getValueAt(row, col)).trim();
		if(code.length() == 0 || value.length() == 0 || value.startsWith("<-"))
		{
			table.setValueAt(code, row, col);
		}
		else
		{
			//StringBuffer notepart = new StringBuffer();
			StringBuffer cmdpart = new StringBuffer();
			for (int i = 0; i < value.length(); i++) {
				char c = value.charAt(i);
				if(c == '[')
				{
					if(cmdpart.length() != 0) cmdpart.append(' ');
					for (; i < value.length(); i++) {
						c = value.charAt(i);
						cmdpart.append(c);
						if(c == ']') break;
					}										
				}/*
				else
				if(!Character.isWhitespace(c))
				{
					notepart.append(c);
				}*/
			}
			code = code.trim() + " " + cmdpart.toString();
			table.setValueAt(code, row, col);
			
		}
		table.tableChanged(new TableModelEvent(table.getModel(), row, row, col));
	}
	
	public void enterNote(String code)
	{
		int row = table.getSelectedRow();
		int col = table.getSelectedColumn();
		setNote(code);				
		table.changeSelection(row+stepsize, col, false, false);
	}
	
	JComboBox combobox_steplens;
	JComboBox combobox_voct;
	JCheckBox chord_checkbox;	
	JComboBox controls_combobox;	
	JComboBox o_combobox;
	JComboBox v_combobox;
	JComboBox c_combobox;	
	
	boolean chord_input_mode = true;
	boolean stop_param_update = false;
	
	String[] controls =
	{
	"1  Mod",
	"7  Vol",
	"10 Pan",
	"11 Exp",
	"91 Eff.1(Rev)",
	"92 Eff.2",
	"93 Eff.3(Cho)",
	"94 Eff.4",
	"95 Eff.5",
	"p Pitch"};

	String[] controls_i = {"1","7","10","11","91","93","p"};
		
	public void updateParamFields()
	{		
		stop_param_update = true;

		String o_param = getParam("o");
		String v_param = getParam("v");
		String c_param = getParam("c");
		
		o_combobox.setSelectedItem(o_param);
		v_combobox.setSelectedItem(v_param);
		c_combobox.setSelectedItem(c_param);
		
		o_combobox.getEditor().setItem(o_param);
		v_combobox.getEditor().setItem(v_param);
		c_combobox.getEditor().setItem(c_param);
		
		it.setSelectedItem(getParam("i"));
		
		String f = getParam("f").trim();
		boolean f_set = false;
		for (int i = 0; i < controls_i.length; i++) {
			if(controls_i[i].equals(f))
			{
				controls_combobox.setSelectedIndex(i);
				f_set = true;
				break;
			}
		}
		if(!f_set)
			controls_combobox.setSelectedItem(f);
		
		stop_param_update = false;
	}
		
	TableTransferHandler transferhandler;
	JPopupMenu popup_zoom;
	
	class ZoomActionListener implements ActionListener
	{
		int index;
		public ZoomActionListener(int index)
		{
			this.index = index;
		}

		public void actionPerformed(ActionEvent arg0) {
			if(model == null) return;
			model.scale_index = index;
			model.scale = scales[index];
			model.abcscale = abcscales[index];
			model.fireTableStructureChanged();
			posmodel.fireTableStructureChanged();
			
		}
	}
	
	InstrumentsTree it;
	
	int playrow = -1;
	
	TimePositionListener timeposlistener = new TimePositionListener()
	{
		public void start()
		{
		}
		public void stop()
		{
			int lastrow = playrow;
			playrow = -1;
			repaintRow(lastrow);
		}		
		public void positionChange(double time, double beat) {			
			if(model != null)
			{
				int row;
				if(beat < 0)
					row = -1;
				else
				    row = (int)(beat / model.scale);
				if(row != playrow)
				{
					int lastrow = playrow;
					playrow = row;					
					if(isVisible())
					{
						repaintRow(lastrow);
						repaintRow(playrow);
					}
				}
			}
		}
	};
	
	public void repaintRow(int row)
	{
		if(row == -1) return;
		Rectangle rect = table.getCellRect(row, 0, true);
		rect.x = 0;
		rect.width = table.getWidth();
		table.repaint(rect);		
	}
	
	RasmusTableCellRenderer cellrenderer = new RasmusTableCellRenderer();
		
	class RasmusTableCellRenderer extends DefaultTableCellRenderer
	{
		private static final long serialVersionUID = 1L;
		
		Color orgcolor;
		Color playcolor;
		
		public RasmusTableCellRenderer()
		{
			playcolor = UIManager.getColor("Table.selectionBackground");
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
		{			
			
			setBackground(null);
			
			Component ret = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if(!(isSelected || hasFocus))
			if(row == playrow)
				setBackground(playcolor);
			
			return ret;
		}
		
	}
	
	public TableEditor(RasmusEditor editor)
	{
		test_midi_interpreter = new Interpreter(editor.namespace);
		test_midi_interpreter.setAutoCommit(false);
		setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		setOpaque(false);
		this.editor = editor;
		editor.addTimePositionListener(timeposlistener);
		setLayout(new BorderLayout());
		
		JPanel formatpanel = new JPanel();
		formatpanel.setOpaque(false);
		formatpanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		String[] steplens = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13","14","15","16"};
		combobox_steplens = new JComboBox(steplens);
		combobox_steplens.setSelectedIndex(1);
		combobox_steplens.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						stepsize = Integer.parseInt((String)(combobox_steplens.getSelectedItem()));
					}
				});
		
		String[] voct = {"C,,,,","C,,,","C,,","C,","C","c","c'","c''","c'''","C''''"};
		combobox_voct = new JComboBox(voct);
		combobox_voct.setSelectedIndex(3);
		combobox_voct.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						keylistener.setOctave(combobox_voct.getSelectedIndex());
						it.keylistener.setOctave(combobox_voct.getSelectedIndex()); 
					}
				});
		
		formatpanel.add(new JLabel("Steps:"));
		formatpanel.add(combobox_steplens);
		
		formatpanel.add(new JLabel("Key.Oct.:"));
		formatpanel.add(combobox_voct);
		
		String[] o_str = {"1","2","3","4","5","6","7","8","9"};
		String[] v_str = {"16","25","32","50","64","80","96","100","127"};
		String[] c_str = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"};
		o_combobox = new JComboBox(o_str);
		v_combobox = new JComboBox(v_str);
		c_combobox = new JComboBox(c_str);
		
		Dimension psize = o_combobox.getPreferredSize();
		psize.width = 50;
		
		o_combobox.setPreferredSize(psize);
		v_combobox.setPreferredSize(psize);
		c_combobox.setPreferredSize(psize);
		
		o_combobox.setEditable(true);
		o_combobox.setSelectedItem("");
		v_combobox.setEditable(true);
		v_combobox.setSelectedItem("");
		c_combobox.setEditable(true);
		c_combobox.setSelectedItem("");
		
		o_combobox.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						setParam("o",(String)o_combobox.getSelectedItem());
					}
				});		
		v_combobox.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						setParam("v",(String)v_combobox.getSelectedItem());
					}
				});		
		c_combobox.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						setParam("c",(String)c_combobox.getSelectedItem());
					}
				});		
		
		it = new InstrumentsTree(editor, this);
		it.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						String selitem = it.getSelectedItem();
						if(selitem == null) return;
						setParam("i",selitem);
					}
				});		
		
		
		//formatpanel.add(new JSeparator(SwingConstants.VERTICAL));
		formatpanel.add(new JLabel("Oct.:"));
		formatpanel.add(o_combobox);
		formatpanel.add(new JLabel("Vel.:"));
		formatpanel.add(v_combobox);
		formatpanel.add(new JLabel("Channel:"));
		formatpanel.add(c_combobox);
		
		controls_combobox = new JComboBox(controls);
		controls_combobox.setEditable(true);
		controls_combobox.setSelectedItem("");
		controls_combobox.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						String s = (String)controls_combobox.getSelectedItem();
						StringBuffer sb = new StringBuffer();
						for (int i = 0; i < s.length(); i++) {
							char c = s.charAt(i);
							if(Character.isWhitespace(c)) break;
							sb.append(c);
						}
						setParam("f",sb.toString());
					}
				});
		
		psize = controls_combobox.getPreferredSize();
		psize.width = 105;
		controls_combobox.setPreferredSize(psize);
				
		formatpanel.add(new JLabel("Control:"));
		formatpanel.add(controls_combobox);
		
		chord_checkbox = new JCheckBox("Chord input");
		chord_checkbox.setOpaque(false);
		chord_checkbox.setSelected(true);
		chord_checkbox.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						chord_input_mode = chord_checkbox.isSelected();
					}
				});
		formatpanel.add(chord_checkbox);
		
		add(formatpanel, BorderLayout.NORTH);
		
		table = new JTable()
		{
			private static final long serialVersionUID = 1L;
			protected boolean processKeyBinding(KeyStroke ks,
                    KeyEvent e,
                    int condition,
                    boolean pressed){				
				if(e.getKeyCode() == KeyEvent.VK_F5) return false;
				if(e.getKeyCode() == KeyEvent.VK_F6) return false;
				if(e.getKeyCode() == KeyEvent.VK_F7) return false;
				if(e.getKeyCode() == KeyEvent.VK_F8) return false;
				if(e.getKeyCode() == KeyEvent.VK_F9) return false;
				if(e.getKeyCode() == KeyEvent.VK_F10) return false;
				return super.processKeyBinding(ks, e, condition, pressed);
			}
		};
		table.getTableHeader().setReorderingAllowed(false);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setRowSelectionAllowed(true);
		table.setColumnSelectionAllowed(true);
		table.setDefaultRenderer(Object.class, cellrenderer);
		transferhandler = new TableTransferHandler();
		table.setTransferHandler(transferhandler);
		table.addFocusListener(new FocusListener()
				{
					public void focusGained(FocusEvent arg0) {
						keylistener.allNotesOff();						
					}
					public void focusLost(FocusEvent arg0) {
						keylistener.allNotesOff();						
					}
				});
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener()
				{
					public void valueChanged(ListSelectionEvent arg0) {
						updateParamFields();
					}
				});
		table.getColumnModel().addColumnModelListener(new TableColumnModelListener()
				{
					public void columnAdded(TableColumnModelEvent arg0) {
					}
					public void columnRemoved(TableColumnModelEvent arg0) {
					}
					public void columnMoved(TableColumnModelEvent arg0) {
					}
					public void columnMarginChanged(ChangeEvent arg0) {
					}
					public void columnSelectionChanged(ListSelectionEvent arg0) {
						updateParamFields();						
					}
				});
		table.addKeyListener(new KeyListener()
				{
					public void keyTyped(KeyEvent event) {
						if(event.getKeyCode() == KeyEvent.VK_DELETE)
						{
							event.consume();
							return;							
						}						if(table.getSelectedRowCount() > 1 || table.getSelectedColumnCount() > 1) return;
						if(table.isEditing()) return;
						if(event.getModifiers() != 0) return;
						if(event.getKeyChar() == '<') return;
						if(event.getKeyCode() == KeyEvent.VK_SPACE)
						{
							event.consume();
							return;
						}
						keylistener.keyTyped(event);
					}
					public void keyPressed(KeyEvent event) {
						if(event.getKeyCode() == KeyEvent.VK_DELETE)
						{
							if(table.isEditing()) return;
							if(table.getSelectedRowCount() > 1 || table.getSelectedColumnCount() > 1) 
							{
								transferhandler.clear(table);
								event.consume();
								return;
							}
							
							enterNote("");
							event.consume();
							return;							
						}
						if(table.getSelectedRowCount() > 1 || table.getSelectedColumnCount() > 1) return;
						if(table.isEditing()) return;
						if(event.getModifiers() != 0) return;
						if(event.getKeyChar() == '<') return;
						if(event.getKeyCode() == KeyEvent.VK_SPACE)
						{
							enterNote("z");
							event.consume();
							return;
						}
						keylistener.keyPressed(event);
					}
					public void keyReleased(KeyEvent event) {
						if(event.getKeyCode() == KeyEvent.VK_DELETE)
						{
							event.consume();
							return;							
						}
						if(table.getSelectedRowCount() > 1 || table.getSelectedColumnCount() > 1) return;
						if(table.isEditing()) return;
						if(event.getModifiers() != 0) return;
						if(event.getKeyChar() == '<') return;
						if(event.getKeyCode() == KeyEvent.VK_SPACE)
						{
							event.consume();
							return;
						}
						keylistener.keyReleased(event);
					}			
				});
		
		posmodel = new AbstractTableModel()
		{
			private static final long serialVersionUID = 1L;
			public int getRowCount() {
				return table.getRowCount();
			}
			public int getColumnCount() {
				return 1;
			}
			public Object getValueAt(int row, int col) {
				double scale = 1.0;
				if(model != null) scale = model.scale;
				double r = ((double)row) * scale;
				if(Math.abs(Math.rint(r) - r) > 0.00000001) return "";
				return Long.toString(  (long)r );				
			}
		};
		
		popup_zoom = new JPopupMenu();
		for (int i = 0; i < scale_items.length; i++) {
			zoom_items[i] = new JCheckBoxMenuItem(scale_items[i]);
			zoom_items[i].addActionListener(new ZoomActionListener(i));
			popup_zoom.add(zoom_items[i]);			
		}
		
		
		
		JTable rowtable = new JTable(posmodel);
		rowtable.setColumnSelectionAllowed(false);
		rowtable.setRowSelectionAllowed(false);
		rowtable.setEnabled(false);
		rowtable.setBackground(new JPanel().getBackground());
		rowtable.addMouseListener(new MouseAdapter()
				{
					public void mouseReleased(MouseEvent e) {
						if(model == null) return;
						for (int i = 0; i < zoom_items.length; i++) {
							zoom_items[i].setState(model.scale_index == i);
						}
						popup_zoom.show((JComponent) e.getSource(), e.getX(), e.getY());
					}					
				});
		
		scrollpane = new JScrollPane(table);
		scrollpane.setRowHeaderView(rowtable);
		scrollpane.getRowHeader().setPreferredSize(new Dimension(45, 45));
		
		JSplitPane splitpane = new JSplitPane();
		splitpane.setDividerLocation(200);
		splitpane.setBorder(BorderFactory.createEmptyBorder());
		splitpane.setLeftComponent(it);
		splitpane.setRightComponent(scrollpane);
		
		BasicSplitPaneUI basicsplitui = new BasicSplitPaneUI()
		{			
			public BasicSplitPaneDivider createDefaultDivider()
			{
				BasicSplitPaneDivider div = new BasicSplitPaneDivider(this)
				{
					private static final long serialVersionUID = 1L;
					public void paint(Graphics g)
					{						
						Dimension size = this.getSize() ;
						g.setColor(this.getBackground());
						g.fillRect(0,0,size.width,size.height);
					}
				};	
				div.setBackground(UIManager.getColor("TabbedPane.contentAreaColor"));
				return div;
			}
		};
		splitpane.setUI(basicsplitui);
		splitpane.setDividerSize(5);
		add(splitpane);
		
		menubar = new JMenuBar();
		
		JMenu edit_menu = menubar.add(new JMenu("Edit"));
		
		JMenuItem menu_cut = new JMenuItem("Cut");
		menu_cut.setMnemonic('t');
		menu_cut.setAccelerator(KeyStroke.getKeyStroke("control X"));
		menu_cut.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				arg0.setSource(table);
				TransferHandler.getCutAction().actionPerformed(arg0);
			}
		});
		JMenuItem menu_copy = new JMenuItem("Copy");
		menu_copy.setMnemonic('C');
		menu_copy.setAccelerator(KeyStroke.getKeyStroke("control C"));
		menu_copy.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				arg0.setSource(table);
				TransferHandler.getCopyAction().actionPerformed(arg0);
			}
		});
		JMenuItem menu_paste = new JMenuItem("Paste");
		menu_paste.setMnemonic('P');
		menu_paste.setAccelerator(KeyStroke.getKeyStroke("control V"));		
		menu_paste.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				arg0.setSource(table);
				TransferHandler.getPasteAction().actionPerformed(arg0);
			}
		});		
		JMenuItem menu_delete = new JMenuItem("Delete");
		menu_delete.setMnemonic('D');
		menu_delete.setAccelerator(KeyStroke.getKeyStroke("DELETE"));		
		menu_delete.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				transferhandler.clear(table);
			}
		});					
		JMenuItem menu_selectAll = new JMenuItem("Select All");
		menu_selectAll.setMnemonic('A');
		menu_selectAll.setAccelerator(KeyStroke.getKeyStroke("control A"));		
		menu_selectAll.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				table.selectAll();
			}
		});					
		
		JMenuItem menu_expression_editor = new JMenuItem("Expression...");
		menu_expression_editor.setAccelerator(KeyStroke.getKeyStroke("F4"));
		menu_expression_editor.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {						
						try
						{						
							
							if (table.isEditing())
								table.getCellEditor().stopCellEditing();
							
							int row = table.getSelectedRow();
							int col = table.getSelectedColumn();
							if(row == -1 || col == -1) return;
							
							String script = (String)table.getValueAt(row, col);
							script = ExpressionEditor.editScript(TableEditor.this.editor, script);
							if(script == null) return;
							
							table.setValueAt(script, row, col);
							table.tableChanged(new TableModelEvent(table.getModel(), row, row, col));
							
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
				});			
		
		
		edit_menu.add(menu_cut);
		edit_menu.add(menu_copy);
		edit_menu.add(menu_paste);
		edit_menu.add(menu_delete);
		edit_menu.addSeparator();
		edit_menu.add(menu_selectAll);
		edit_menu.addSeparator();
		edit_menu.add(menu_expression_editor);
		
		menubar = editor.processMenuBar(menubar);
		
		//test_midi_interpreter.eval("MidiOutput(\"Microsoft MIDI Mapper\") <- input;");
		try {
			test_midi_interpreter.eval("midi_in <- defaultinputs();");
		} catch (ScriptParserException e1) {
			e1.printStackTrace();
		}
		//RInterpreter.commit();
		//midi_out =  MidiSequence.getInstance(test_midi_interpreter.get("input"));
		MidiSequence.getInstance(test_midi_interpreter.get("midi_in")).addReceiver(recv_midiin);
		
		midi_out = MidiSequence.getInstance(editor.realtimeinput);
	}
	
	Receiver midi_out;
	JMenuBar menubar;
	
	Interpreter test_midi_interpreter;
	
	public void init()
	{
		editor.setJMenuBar(menubar);
	}

	public void closing() {
		test_midi_interpreter.close();
	}
}
