/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.metadata.FunctionCallEditor;
import rasmus.interpreter.ui.GroupMenuListener;
import rasmus.interpreter.ui.GroupMenus;
import rasmus.interpreter.ui.RSyntaxDocument;
import rasmus.interpreter.ui.RegisterFunction;
import rasmus.interpreter.ui.RegisterUnit.Record;

public class ExpressionEditor extends JDialog {

	private static final long serialVersionUID = 1L;

	JTextPane textarea = new JTextPane();

	UndoManager undo = new UndoManager();
	Interpreter globalinterpreter = new Interpreter();
	JPopupMenu insert_popup;
	JButton insert_function_button;
	JButton arguments_button;
	
	GroupMenuListener groupmenulistener = new GroupMenuListener()
	{

		public void actionPerformed(Record record) {
			
			if(record.unit instanceof RegisterFunction)
			{				
				if(FunctionCallEditor.isEditableFunction(record.name, globalinterpreter))
				{
					String statement = FunctionCallEditor.editFunctionCall(ExpressionEditor.this, record.name+"()", globalinterpreter);
					if(statement != null)
					{
						textarea.replaceSelection(statement);
					}
					return;					
				}				
				textarea.replaceSelection(record.name+"()");
				return;
			}
			textarea.replaceSelection(record.name);
		}
		
	};	

	private ExpressionEditor(Frame parent) {
		super(parent);

		textarea.setDocument(new RSyntaxDocument(globalinterpreter));
		textarea.setFont(new Font("Courier", 0, 12));
		
	    // Add Undo Support to TextArea
	    
	    Document doc = textarea.getDocument();
	    
	    doc.addUndoableEditListener(new UndoableEditListener() {
	        public void undoableEditHappened(UndoableEditEvent evt) {
	            undo.addEdit(evt.getEdit());
	        }
	    });
	    
	    ActionMap textarea_actionmap = textarea.getActionMap();
	    InputMap textarea_inputmap = textarea.getInputMap();

	    textarea_inputmap.put(KeyStroke.getKeyStroke("control Z"), "Undo");
	    textarea_inputmap.put(KeyStroke.getKeyStroke("control Y"), "Redo");
	    
	    textarea_actionmap.put("Undo",
	        new AbstractAction("Undo") {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent evt) {
	                try {
	                    if (undo.canUndo()) {
	                        undo.undo();
	                    }
	                } catch (CannotUndoException e) {
	                }
	            }
	       });
	    
	    textarea_actionmap.put("Redo",
		        new AbstractAction("Redo") {
	
	    			private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
		                try {
		                    if (undo.canRedo()) {
		                        undo.redo();
		                    }
		                } catch (CannotRedoException e) {
		                }
		            }
		        });
	    
	    TextPopupMenu.addPopupMenu(textarea);
	    
	    JMenu groupmenu = new GroupMenus(globalinterpreter, groupmenulistener);	    
	    insert_popup = new JPopupMenu();
	    Component[] menucomps = groupmenu.getMenuComponents();
	    groupmenu.removeAll();
	    for (int i = 0; i < menucomps.length; i++) {
	    	insert_popup.add(menucomps[i]);
		}
	    
	    insert_function_button = new JButton("Insert Function...");
	    insert_function_button.setFocusable(false);
	    insert_function_button.addActionListener(new ActionListener()
	    		{
					public void actionPerformed(ActionEvent arg0) {
						insert_popup.show(insert_function_button, 0, 0); 
					}
	    		});
	    
	    
	    
	    arguments_button = new JButton("Arguments...");
	    arguments_button.setFocusable(false);
	    
	    ActionListener arguments_action = new ActionListener()
	    		{
					public void actionPerformed(ActionEvent arg0) {
						try
						{
							int start = textarea.getSelectionStart();
							int[] pair = ((RSyntaxDocument)textarea.getDocument()).extractFunctionCall(start);
							if(pair == null) return;
																					
							textarea.select(pair[0], pair[0]+pair[1]);
		
							String token = textarea.getDocument().getText(pair[0], pair[1]);
														
							String expr = FunctionCallEditor.editFunctionCall(ExpressionEditor.this, token, globalinterpreter);
							if(expr != null)
							{
								textarea.replaceSelection(expr);
							}
							
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}						
					}
	    		};

	    arguments_button.addActionListener(arguments_action);
	    
		KeyStroke stroke_f4 = KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0);		
		textarea.registerKeyboardAction(arguments_action
				, stroke_f4, JComponent.WHEN_IN_FOCUSED_WINDOW);	    
	    
	    JButton okbutton = new JButton("OK");
	    okbutton.setDefaultCapable(true);
	    okbutton.addActionListener(new ActionListener()
	    		{
					public void actionPerformed(ActionEvent arg0) {
						ok = true;
						setVisible(false);
					}
	    		});
	    
	    JButton cancelbutton = new JButton("Cancel");
	    cancelbutton.addActionListener(new ActionListener()
	    		{
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
					}
	    		});
	    
	    JPanel buttonpanelL = new JPanel();
	    buttonpanelL.setLayout(new FlowLayout(FlowLayout.LEFT));
	    buttonpanelL.add(insert_function_button);
	    buttonpanelL.add(arguments_button);
	    
	    JPanel buttonpanelR = new JPanel();
	    buttonpanelR.setLayout(new FlowLayout(FlowLayout.RIGHT));
	    buttonpanelR.add(okbutton);
	    buttonpanelR.add(cancelbutton);
	    
	    JPanel buttonpanel = new JPanel();
	    buttonpanel.setLayout(new BorderLayout());
	    buttonpanel.add(buttonpanelL);
	    buttonpanel.add(buttonpanelR,BorderLayout.EAST);
	    
		JPanel textpanel = new JPanel();		
		textpanel.setLayout(new BorderLayout());
		textpanel.add(textarea);
		JScrollPane scrollpane = new JScrollPane(textpanel);
		
		setLayout(new BorderLayout());
		add(scrollpane);
		add(buttonpanel, BorderLayout.SOUTH);
			    
		getRootPane().setDefaultButton(okbutton);
		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		
		scrollpane.registerKeyboardAction(new ActionListener()
				{
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				}
				, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
				
		setTitle("Expression Editor");
		//setIconImage(new javax.swing.ImageIcon(getClass().getResource("/rasmus/rasmusdsp.PNG")).getImage());
	    		
		Dimension size = new Dimension(500, 300);		
		setSize(size);
		Dimension screenSize = parent.getSize();
		Point parent_loc = parent.getLocation();			
		setLocation(parent_loc.x + screenSize.width/2 - (size.width/2),
				    parent_loc.y + screenSize.height/2 - (size.height/2));
		
	}	
	
	boolean ok = false;

	public static String editScript(Frame parent, String string)
	{
		ExpressionEditor ee = new ExpressionEditor(parent);
		ee.setModal(true);
		ee.textarea.setText(string);
		ee.setVisible(true);
		if(!ee.ok) return null;
		return ee.textarea.getText();
	}
	
}
