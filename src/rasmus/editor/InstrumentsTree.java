/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.sound.midi.Instrument;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Patch;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Soundbank;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import rasmus.editor.ScriptDocument.Statement;
import rasmus.interpreter.Commitable;
import rasmus.interpreter.Interpreter;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.midi.MidiKeyListener;
import rasmus.interpreter.parser.ScriptElement;
import rasmus.interpreter.parser.ScriptParser;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.sampled.midi.InstrumentRecord;
import rasmus.midi.provider.RasmusSoundbank;
import rasmus.midi.provider.RasmusSoundbankReader;

public class InstrumentsTree extends JPanel {
	/*
	String[] instruments_gm = 
	{"Acoustic Piano",
		"Bright Piano",
		"Electric Grand Piano",
		"Honky-tonk Piano",
		"Electric Piano 1",
		"Electric Piano 2",
		"Harpsichord",
		"Clavi",
		"Celesta",
		"Glockenspiel",
		"Music Box",
		"Vibraphone",
		"Marimba",
		"Xylophone",
		"Tubular Bell",
		"Dulcimer",
		"Drawbar Organ",
		"Percussive Organ",
		"Rock Organ",
		"Church organ",
		"eed organ",
		"Accordion",
		"Harmonica",
		"Tango Accordion",
		"Acoustic Guitar (nylon)",
		"Acoustic Guitar (steel)",
		"Electric Guitar (jazz)",
		"Electric Guitar (clean)",
		"Electric Guitar (muted)",
		"Overdriven Guitar",
		"Distortion Guitar",
		"Guitar harmonics",
		"Acoustic Bass",
		"Electric Bass (finger)",
		"Electric Bass (pick)",
		"Fretless Bass",
		"Slap Bass 1",
		"Slap Bass 2",
		"Synth Bass 1",
		"Synth Bass 2",
		"Violin",
		"Viola",
		"Cello",
		"Double bass",
		"Tremolo Strings",
		"Pizzicato Strings",
		"Orchestral Harp",
		"Timpani",
		"String Ensemble 1",
		"String Ensemble 2",
		"Synth Strings 1",
		"Synth Strings 2",
		"Voice Aahs",
		"Voice Oohs",
		"Synth Voice",
		"Orchestra Hit",
		"Trumpet",
		"Trombone",
		"Tuba",
		"Muted Trumpet",
		"French horn",
		"Brass Section",
		"Synth Brass 1",
		"Synth Brass 2",
		"Soprano Sax",
		"Alto Sax",
		"Tenor Sax",
		"Baritone Sax",
		"Oboe",
		"English Horn",
		"Bassoon",
		"Clarinet",
		"Piccolo",
		"Flute",
		"Recorder",
		"Pan Flute",
		"Blown Bottle",
		"Shakuhachi",
		"Whistle",
		"Ocarina",
		"Lead 1 (square)",
		"Lead 2 (sawtooth)",
		"Lead 3 (calliope)",
		"Lead 4 (chiff)",
		"Lead 5 (charang)",
		"Lead 6 (voice)",
		"Lead 7 (fifths)",
		"Lead 8 (bass + lead)",
		"Pad 1 (new age)",
		"Pad 2 (warm)",
		"Pad 3 (polysynth)",
		"Pad 4 (choir)",
		"Pad 5 (bowed)",
		"Pad 6 (metallic)",
		"Pad 7 (halo)",
		"Pad 8 (sweep)",
		"FX 1 (rain)",
		"FX 2 (soundtrack)",
		"FX 3 (crystal)",
		"FX 4 (atmosphere)",
		"FX 5 (brightness)",
		"FX 6 (goblins)",
		"FX 7 (echoes)",
		"FX 8 (sci-fi)",
		"Sitar",
		"Banjo",
		"Shamisen",
		"Koto",
		"Kalimba",
		"Bagpipe",
		"Fiddle",
		"Shanai",
		"Tinkle Bell",
		"Agogo Bells",
		"Steel Drums",
		"Woodblock",
		"Taiko Drum",
		"Melodic Tom",
		"Synth Drum",
		"Reverse Cymbal",
		"Guitar Fret Noise",
		"Breath Noise",
		"Seashore",
		"Bird Tweet",
		"Telephone Ring",
		"Helicopter",
		"Applause",
		"Gunshot"};
	*/
	private class InstrumentItem
	{
		String name;
		String program;
		
		public InstrumentItem(String name, String program)
		{
			this.name = name;
			this.program = program;
		}
		
		public String toString()
		{
			return program + ": " + name;
		}
	}
	
	private static final long serialVersionUID = 1L;

	DefaultTreeModel model;
	JScrollPane scrollpane;
	JTree tree;
	
	
	
	public void setSelectedItem(String value)
	{
		if(isInFileSelectionMode())
		{
			return;
		}
		
		DefaultMutableTreeNode ins = insmap.get(value);
		if(ins == null)
		{
			tree.setSelectionPath(null);
		}
		else
		{
			TreePath path = new TreePath(ins.getPath());
			tree.setSelectionPath(path);
			Rectangle bounds =  tree.getPathBounds(path);
			tree.scrollRectToVisible(bounds);
		}
	}
	
	public String getSelectedItem()
	{
		if(isInFileSelectionMode())
		{
			if(sfa != null) return sfa.getSelectedItem();
		}
		
		TreePath treepath = tree.getSelectionPath();
		if(treepath == null) return null;		
		DefaultMutableTreeNode treenode = (DefaultMutableTreeNode)treepath.getLastPathComponent();
		Object uobject = treenode.getUserObject();
		if(!(uobject instanceof InstrumentItem)) return null;
		return ((InstrumentItem)uobject).program; 
	}
	
	ArrayList<ActionListener> actionlisteners = new ArrayList<ActionListener>();
	
	public void addActionListener(ActionListener listener)
	{
		actionlisteners.add(listener); 
	}
	
	public void fireChangeSelection()
	{
		ActionEvent event = new ActionEvent(this, 0, null);
		Iterator<ActionListener> iter = actionlisteners.iterator();
		while (iter.hasNext()) {
			 iter.next().actionPerformed(event);
		}
	}
	
	HashMap<String,DefaultMutableTreeNode> insmap = new HashMap<String,DefaultMutableTreeNode>();
	HashMap<String,DefaultMutableTreeNode> gm_insmap = new HashMap<String,DefaultMutableTreeNode>();
	
	Variable instruments_var;
	Commitable inst_commit = new Commitable()
	{		
		public int getRunLevel()
		{
			return RUNLEVEL_DEFAULT;
		}
		public void commit() {
			
			if(isInFileSelectionMode()) return;
						
			List list = ObjectsPart.asList(instruments_var);
			if(list.size() == 0)
			{
			    tree.setModel(gmmodel);
			    insmap = gm_insmap;
			}
			else
			{
				DefaultMutableTreeNode instruments = new DefaultMutableTreeNode("Instruments");
				
				TreeSet<InstrumentRecord> ins_sort = new TreeSet<InstrumentRecord>(new Comparator<InstrumentRecord>()
						{
							public int compare(InstrumentRecord arg0, InstrumentRecord arg1) {								
								int a = arg0.bank * 128 + arg0.program;
								int b = arg1.bank * 128 + arg1.program;
								return a - b;
							}
					
						});
				Iterator iter = list.iterator();
				while (iter.hasNext()) {
					Object object = iter.next();
					if(object instanceof InstrumentRecord)
					if(((InstrumentRecord)object).channels[0])
					{
						ins_sort.add((InstrumentRecord)object); 
					}				
				}
				
				insmap = new HashMap<String,DefaultMutableTreeNode>();
								
				DefaultMutableTreeNode bank = null;
				int curbank = -1;
												
				iter = ins_sort.iterator();
				while (iter.hasNext()) {
					InstrumentRecord element = (InstrumentRecord) iter.next();
					
					String desc = element.description;
					InstrumentItem insitem = new InstrumentItem(desc, element.program + "," + element.bank);
					DefaultMutableTreeNode ins  = new DefaultMutableTreeNode(insitem);
					
					if(curbank != element.bank)
					{
						curbank = element.bank;
						bank = new DefaultMutableTreeNode("Bank " + curbank);
						instruments.add(bank);
					}
					
					bank.add(ins);
					insmap.put(insitem.program, ins);
					
				}
				
				tree.setModel(new DefaultTreeModel(instruments));
			}
			
			
		}
	};
	
	DefaultTreeModel gmmodel;
	
	boolean ins_set = false;
	
	Receiver recv_midiin = new Receiver()
	{
		public void send(MidiMessage msg, long tick) {

			if(!ins_set)
			{
				procSessMidiSetup();
				ins_set = true;
			}
			
			teditor.sendMessage(msg);
		}
		public void close() {
		}				
	};

	Receiver recv = new Receiver()
	{
			public void send(MidiMessage msg, long tick) {
				
				if(!ins_set)
				{
					procSessMidiSetup();
					ins_set = true;
				}
				
				if(msg instanceof ShortMessage)
				{
					
					ShortMessage sms = (ShortMessage)msg;
					if((sms.getCommand() == ShortMessage.NOTE_ON) && (sms.getData2() > 0))
					{
						try {
							ShortMessage newmsg = new ShortMessage();
							newmsg.setMessage(sms.getCommand(), sms.getChannel(), sms.getData1(), teditor.midi_setup_v);
							msg = newmsg;					
						} catch (InvalidMidiDataException e) {
							e.printStackTrace();
						}
					}
									
				}				
				
				teditor.sendMessage(msg);

			}
			public void close() {
			}		
		};
		
	public void procSessMidiSetup()
	{
		
		teditor.midi_setup_lastbeat = 0;
		teditor.midi_setup_lastcol = -1;		
		teditor.procSessMidiSetup(false);
		teditor.midi_setup_lastbeat = 0;
		teditor.midi_setup_lastcol = -1;		
		
		String str_i = getSelectedItem();
		if(str_i == null) return;
		
	    String program = str_i;
	    String bank = null;
	    int li = str_i.indexOf(",");
	    if(li != -1)
	    {
	    	bank = program.substring(li+1);
	    	program = program.substring(0, li);
	    }
	    
	    	
	
	    
	    if(bank != null)
	    {
	    	int i_bank = Integer.parseInt(bank);
	    	if(i_bank < 0) i_bank = 0;
	    	if(i_bank > 127*128) i_bank = 128*127;
	    	
	    	int m_bank = i_bank / 128;
	    	//if(m_bank != 0)
	    	{
				try {
					ShortMessage smsg = new ShortMessage();
					smsg.setMessage(ShortMessage.CONTROL_CHANGE, teditor.midi_setup_c-1, 0, m_bank);
					teditor.sendMessage(smsg);
				} catch (InvalidMidiDataException e) {
					e.printStackTrace();
				}
	    	}

	    	int l_bank = i_bank % 128;
	    	//if(l_bank != 0)
	    	{
				try {
	    		ShortMessage smsg = new ShortMessage();
				smsg.setMessage(ShortMessage.CONTROL_CHANGE, teditor.midi_setup_c-1, 32, l_bank);
				teditor.sendMessage(smsg);
				} catch (InvalidMidiDataException e) {
					e.printStackTrace();
				}
	    	}
	    	
	    }
		try {
	    int i_program = Integer.parseInt(program);
	    ShortMessage smsg = new ShortMessage();
		smsg.setMessage(ShortMessage.PROGRAM_CHANGE, teditor.midi_setup_c-1, i_program, 0);
		teditor.sendMessage(smsg);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}		
	}
	
	MidiKeyListener keylistener = new MidiKeyListener();
	{
		keylistener.setReceiver(recv);
	}
		
	TableEditor teditor;
	
	JFileChooser soundbankpresetchooser = null;
	
	FileFilter soundfont_filefilter = new FileFilter()
	{
		public boolean accept(File arg0) {
			if(!arg0.isFile()) return true;
			return arg0.getName().toLowerCase().endsWith(".sf2");
		}
		public String getDescription() {
			return "SoundFont 2.x (*.sf2)";
		}
	};
	
	class SoundFontAccessory extends JPanel
	{
		private static final long serialVersionUID = 1L;
		
		JFileChooser filechooser;
		File curfile = null;
		PropertyChangeListener sfa = new PropertyChangeListener()
		{
			public void propertyChange(PropertyChangeEvent evt) {
				
				if(JFileChooser.DIRECTORY_CHANGED_PROPERTY.equals(evt
						.getPropertyName())) {					
					soundfontlist.setModel(new DefaultListModel());
					invalidate();
					validate();
					repaint();								
				}
				if(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(evt
						.getPropertyName())) {
					File newFile = (File) evt.getNewValue();				
					
					if(newFile != null)
					if(newFile.getName().toLowerCase().endsWith(".sf2"))
					{
						try {
																					
							//Soundbank soundbank = MidiSystem.getSoundbank(newFile);
							Soundbank soundbank = new RasmusSoundbankReader().getSoundbank(newFile);

							Instrument[] instruments = soundbank.getInstruments();
							
							DefaultListModel listmodel = new DefaultListModel();
							listmodel.ensureCapacity(instruments.length);
							for (int i = 0; i < instruments.length; i++) {
								Patch patch = instruments[i].getPatch();
								String ident = patch.getProgram() + "," + patch.getBank();
								listmodel.addElement(ident + ": " + instruments[i].getName());
							}
						
							ins_set = false;
							soundfontlist.setModel(listmodel);
							curfile = newFile;
							invalidate();
							validate();
							repaint();	
							
							return;
						
						} catch (InvalidMidiDataException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					
					}
					
					soundfontlist.setModel(new DefaultListModel());
					invalidate();
					validate();
					repaint();						
					
				}
			}
		};
		
		
		JList soundfontlist = new JList()
		{
			private static final long serialVersionUID = 1L;
			
			public void processKeyEvent(KeyEvent e) {
				if (e.getModifiers() > 1) {
					super.processKeyEvent(e);
					return;
				}

				if (e.getKeyCode() == 0)
					return;
				if (e.getID() == KeyEvent.KEY_PRESSED) {
					keyPressed(e);
				}
				if (e.getID() == KeyEvent.KEY_RELEASED) {
					keyReleased(e);
				}
				if (e.getID() == KeyEvent.KEY_TYPED) {
					keyTyped(e);
				}
				if (!e.isConsumed())
					super.processKeyEvent(e);
			}

			public void keyPressed(KeyEvent e) {				
				keylistener.keyPressed(e);
			}
			public void keyReleased(KeyEvent e) {
				keylistener.keyReleased(e);
			}
			public void keyTyped(KeyEvent e) {
				keylistener.keyTyped(e);
			}			
		};
	

		public String selectedPreset;
		public String selectedPresetDescription;
		
		Interpreter testinterpreter = null;
		public String getSelectedItem()
		{
			
			String epreset = getEmptyPresetIdent();
			
			unloadTestPreset();
			
			if(soundfontlist.getSelectedIndex() == -1) return null;
			
			testinterpreter = new Interpreter(editor.namespace);
			testinterpreter.setAutoCommit(false);
			
			
			String val = soundfontlist.getSelectedValue().toString();
			int li = val.indexOf(":");
			String selectedPresetDescription = val.substring(li+1); 
			val = val.substring(0, li);
			String selectedPreset = val;
			
			
			String pathcode = "\"" + curfile.getPath() + "\"";
			String code = "\ninstruments <- instrument(" + epreset + ",\"" + selectedPresetDescription.trim() + "\"" + ") <- soundbankpreset(" + pathcode + "," + selectedPreset + ");";
			try {
				testinterpreter.eval(code);
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}
			
			editor.namespace.commit();
			return epreset;
		}
		
		public void unloadTestPreset()
		{
			if(testinterpreter != null)
			{
				testinterpreter.close();
				testinterpreter = null;
				editor.namespace.commit();
			}
			
		}
		
		public void selectPreset()
		{
			if(soundfontlist.getSelectedIndex() == -1) return;							
			String val = soundfontlist.getSelectedValue().toString();
			int li = val.indexOf(":");
			selectedPresetDescription = val.substring(li+1); 
			val = val.substring(0, li);
			selectedPreset = val;
			soundbankpresetchooser.approveSelection(); 			
		}

		public SoundFontAccessory(JFileChooser filechooser)
		{
			
			soundfontlist.getSelectionModel().addListSelectionListener(new ListSelectionListener()
					{
						public void valueChanged(ListSelectionEvent arg0) {
							ins_set = false;
						}
					});
			soundfontlist.addMouseListener(new MouseAdapter()
					{
						public void mouseClicked(MouseEvent event) {
							if(event.getClickCount() == 2)
							{
								fireChangeSelection();
							}
						}
					});	
			soundfontlist.addFocusListener(new FocusListener()
					{
						public void focusGained(FocusEvent arg0) {
							keylistener.allNotesOff();
							ins_set = false;
						}
						public void focusLost(FocusEvent arg0) {
							keylistener.allNotesOff();						
							ins_set = false;
						}
					});		
			
			this.filechooser = filechooser;
			filechooser.setAccessory(this);
			filechooser.addPropertyChangeListener(sfa);
			Dimension psize = getPreferredSize();
			psize.height = 300;
			psize.width = 200;
			setPreferredSize(psize);
						
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(0,4,0,0));
			add(new JScrollPane(soundfontlist));
			
			JButton addpreset = new JButton("Open Instrument");
			
			addpreset.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent arg0) {
							selectPreset();
						}				
					});			
			
			soundfontlist.addMouseListener(new MouseAdapter()
					{
						public void mouseClicked(MouseEvent arg0) {
							if(arg0.getClickCount() == 2)
							{
								selectPreset();
							}
						}
				
					});

			
			JPanel buttonpanel = new JPanel();
			buttonpanel.add(addpreset);
			add(buttonpanel, BorderLayout.SOUTH);
			
			String[] values = {"disk", "memory"};
			streammode = new JComboBox(values);
			streammode.setSelectedIndex(1);
			
			JPanel optionpanel = new JPanel();
			optionpanel.setLayout(new FlowLayout());
			optionpanel.add(new JLabel("Stream mode:"));
			optionpanel.add(streammode);
			add(optionpanel, BorderLayout.NORTH);			
		}
		
		JComboBox streammode ;
	};
	
	SoundFontAccessory sfa;
		
	public JFileChooser getFileChooser()
	{
		if(soundbankpresetchooser != null) 
		{
			sfa.selectedPreset = null;
			return soundbankpresetchooser;
		}
		
		soundbankpresetchooser = new JFileChooser();
		soundbankpresetchooser.setDialogTitle("Add Instrument(s)");
		soundbankpresetchooser.addChoosableFileFilter(soundfont_filefilter);
		soundbankpresetchooser.setAcceptAllFileFilterUsed(true);
		soundbankpresetchooser.setFileFilter(soundfont_filefilter);
		sfa = new SoundFontAccessory(soundbankpresetchooser);
		sfa.selectedPreset = null;
		return soundbankpresetchooser;
	}
	
	
	boolean is_InFileSelectionMode = false;
	public boolean isInFileSelectionMode()
	{
		return is_InFileSelectionMode;
	}
	public void setInFileSelectionMode(boolean val)
	{
		if(sfa != null) sfa.unloadTestPreset();
		is_InFileSelectionMode = val;
	}
	
	String emptypreset = null;
	public String getEmptyPresetIdent()
	{
		if(emptypreset != null) return emptypreset;
		
		List list = ObjectsPart.asList(instruments_var);
		
		Iterator iter = list.iterator();
		TreeSet<InstrumentRecord> ins_sort = new TreeSet<InstrumentRecord>(new Comparator<InstrumentRecord>()
				{
					public int compare(InstrumentRecord arg0, InstrumentRecord arg1) {								
						int a = arg0.bank * 128 + arg0.program;
						int b = arg1.bank * 128 + arg1.program;
						return a - b;
					}
			
				});
		while (iter.hasNext()) {
			
			Object element = iter.next();
			if(element instanceof InstrumentRecord) 
			if(((InstrumentRecord)element).channels[0])
			{
				InstrumentRecord ielement = (InstrumentRecord)element;
				ins_sort.add(ielement);
			}
		}
		
		int targetid = 0;
		Iterator<InstrumentRecord> iiter = ins_sort.iterator();
		while(iiter.hasNext())
		{
			InstrumentRecord ielement = iiter.next();									
			int ins_id = ielement.bank * 128 + ielement.program;
			if(ins_id != targetid) break;
			targetid++;
		}

		emptypreset = (targetid % 128) + "," + (targetid / 128);
		
		return emptypreset;
		
	}
	
	RasmusEditor editor;
	public InstrumentsTree(RasmusEditor editor, TableEditor teditor)
	{
		this.teditor = teditor;
		this.editor = editor;
		setLayout(new BorderLayout());
		
		tree = new JTree()
		{
			private static final long serialVersionUID = 1L;
			
			public void processKeyEvent(KeyEvent e) {
				if (e.getModifiers() > 1) {
					super.processKeyEvent(e);
					return;
				}

				if (e.getKeyCode() == 0)
					return;
				if (e.getID() == KeyEvent.KEY_PRESSED) {
					keyPressed(e);
				}
				if (e.getID() == KeyEvent.KEY_RELEASED) {
					keyReleased(e);
				}
				if (e.getID() == KeyEvent.KEY_TYPED) {
					keyTyped(e);
				}
				if (!e.isConsumed())
					super.processKeyEvent(e);
			}

			public void keyPressed(KeyEvent e) {				
				keylistener.keyPressed(e);
			}
			public void keyReleased(KeyEvent e) {
				keylistener.keyReleased(e);
			}
			public void keyTyped(KeyEvent e) {
				keylistener.keyTyped(e);
			}			
		};
		tree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener()
				{
					public void valueChanged(TreeSelectionEvent arg0) {
						ins_set = false;
					}
				});
		tree.addMouseListener(new MouseAdapter()
				{
					public void mouseClicked(MouseEvent event) {
						if(event.getClickCount() == 2)
						{
							fireChangeSelection();
						}
					}
				});	
		tree.addFocusListener(new FocusListener()
				{
					public void focusGained(FocusEvent arg0) {
						keylistener.allNotesOff();
						ins_set = false;
					}
					public void focusLost(FocusEvent arg0) {
						keylistener.allNotesOff();						
						ins_set = false;
					}
				});		
		//tree.addKeyListener(keylistener);		
		
		DefaultMutableTreeNode instruments = new DefaultMutableTreeNode("Instruments");
		/*
		DefaultMutableTreeNode gm  = new DefaultMutableTreeNode("General MIDI"); 
		instruments.add(gm);
		
		for (int i = 0; i < instruments_gm.length; i++) {
			InstrumentItem insitem = new InstrumentItem(instruments_gm[i],""+i);
			DefaultMutableTreeNode ins  = new DefaultMutableTreeNode(insitem);
			gm.add(ins);
			insmap.put(insitem.program, ins);
		}*/
		
		
		gm_insmap = insmap;
		
		gmmodel = new DefaultTreeModel(instruments);		
		
		instruments_var = editor.namespace.get("instruments");
		ObjectsPart.getInstance(instruments_var).addListener(new
				ListPartListener()
				{
					public void objectAdded(ListPart source, Object object) {
						InstrumentsTree.this.editor.namespace.addToCommitStack(inst_commit);
					}
					public void objectRemoved(ListPart source, Object object) {
						InstrumentsTree.this.editor.namespace.addToCommitStack(inst_commit);
					}
					public void objectsAdded(ListPart source, List objects) {
						InstrumentsTree.this.editor.namespace.addToCommitStack(inst_commit);
					}
					public void objectsRemoved(ListPart source, List objects) {
						InstrumentsTree.this.editor.namespace.addToCommitStack(inst_commit);
					}
				});
		
		inst_commit.commit();		
		
		JPanel buttonpanel = new JPanel();
		buttonpanel.setBackground(UIManager.getColor("TabbedPane.contentAreaColor"));
		
		JButton addbutton = new JButton("Add..");
		addbutton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						
						emptypreset = null;
						JFileChooser fc = getFileChooser();
						ins_set = false;
						setInFileSelectionMode(true);
						int ret = fc.showOpenDialog(InstrumentsTree.this.editor);
						setInFileSelectionMode(false);
						ins_set = false;
						if(ret == JFileChooser.APPROVE_OPTION)
						{
							
							String path;
							try {
								path = fc.getSelectedFile().getCanonicalPath();
							} catch (IOException e) {
								e.printStackTrace();
								return;
							}
							
							String pathcode = "\"" + path + "\"";
							
							if(InstrumentsTree.this.editor.session.getWorkDirectory() != null)
							{
								try {
									String wpath = InstrumentsTree.this.editor.session.getWorkDirectory().getCanonicalPath();
									if(!wpath.endsWith(File.separator)) wpath += File.separator;
									
									if(path.startsWith(wpath))
									{
										path = path.substring(wpath.length());
										pathcode = "wrkdir & \"" + path + "\"";
									}
									
								} catch (IOException e) {
									e.printStackTrace();
								}
								
							}
							
							int streammode = sfa.streammode.getSelectedIndex();
							if(streammode < 0) streammode = 0;
							if(streammode > 1) streammode = 0;
							
							String options = "";
							if(streammode == 0)
							{
								options = ",streammode=1";
							}
							
							if(sfa.selectedPreset != null)
							{
								
//								 Adding soundbank preset...
								
								String seltarget = getEmptyPresetIdent();
								
								Object sel = JOptionPane.showInputDialog(InstrumentsTree.this.editor, "Target program, bank:", "Add soundbank preset", JOptionPane.QUESTION_MESSAGE, null, null, seltarget );
								if(sel == null) return;
								seltarget = sel.toString().trim();
								
								String code = "\ninstruments <- instrument(" + seltarget + ",\"" + sfa.selectedPresetDescription.trim() + "\"" + ") <- soundbankpreset(" + pathcode + "," + sfa.selectedPreset + options + ");";
								InstrumentsTree.this.editor.doc.getObject("instruments").add(code);
								InstrumentsTree.this.editor.commit();

								setSelectedItem(seltarget);
								
								return;								
							}
							
							// Adding soundbank...
							
							List list = ObjectsPart.asList(instruments_var);
														
							Iterator iter = list.iterator();
							TreeSet banksinuse = new TreeSet<Integer>(); 
							while (iter.hasNext()) {
								Object element = iter.next();
								if(element instanceof InstrumentRecord)
								{
									InstrumentRecord ielement = (InstrumentRecord)element;
									if(!banksinuse.contains(ielement.bank))
										banksinuse.add(ielement.bank);
								}
							}
							
							int emptybank = 0;
							while(banksinuse.contains(emptybank)) emptybank++;
							
							// Search for empty bank
							Object selectedbank = JOptionPane.showInputDialog(InstrumentsTree.this.editor, "Target bank:", "Add soundbank", JOptionPane.QUESTION_MESSAGE, null, null, Integer.toString(emptybank) );
							if(selectedbank == null) return;
							
							String s_selectedbank = selectedbank.toString().trim();
																					

							
							String code;
							if(selectedbank.equals("0"))
								code = "\ninstruments <- soundbank(" + pathcode + options + ");";
							else
								code = "\ninstruments <- soundbank(" + pathcode + "," +  s_selectedbank + options + ");";
								
							InstrumentsTree.this.editor.doc.getObject("instruments").add(code);
							InstrumentsTree.this.editor.commit();

							
							
							//return fc.getSelectedFile();
						}
						
					}
				});
		
		
		JButton removebutton = new JButton("Remove");
		removebutton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						
						TreeSet<Integer> allbanks = new TreeSet<Integer>();
						TreeSet<Integer> banks = new TreeSet<Integer>();
						TreeSet<String> programs = new TreeSet<String>();
						TreePath[] treepaths = tree.getSelectionPaths();
						for (int i = 0; i < treepaths.length; i++) {
							TreePath treepath = treepaths[i];
							DefaultMutableTreeNode treenode = (DefaultMutableTreeNode)treepath.getLastPathComponent();
							Object uobject = treenode.getUserObject();
							if(uobject instanceof InstrumentItem) 
							{
								InstrumentItem ielement = (InstrumentItem)uobject;																
								int li = ielement.program.indexOf(",");
								int bank = 0;
								if(li != -1)
									bank = Integer.parseInt(ielement.program.substring(li+1));
								
								if(!banks.contains(bank))
									banks.add(bank);								
								programs.add(ielement.program);								

																						
							}
							else
							{
								if(uobject.toString().startsWith("Bank "))
								{
									int bank = Integer.parseInt(uobject.toString().substring(5));								
									banks.add(bank);
									allbanks.add(bank);
								}
							}
						} 
						
						List<Statement> list = InstrumentsTree.this.editor.doc.getObject("instruments").getStatements();
						Statement[] statements = new Statement[list.size()];
						list.toArray(statements); 
						
						for (int i = 0; i < statements.length; i++) {
							Statement st = statements[i];
							ScriptElement element;
							try {
								element = ScriptParser.parse(st.toString());								
							} catch (ScriptParserException e) {
								continue;
							}							
							if(element.getElements() == null) continue;
							if(element.getElements().size() == 0) continue;
							
							ScriptElement assignment = element.getElements().get(0);
							if(assignment.getType() != ScriptElement.TYPE_ASSIGNMENT) continue;

							if(assignment.getElements() == null) continue;
							if(assignment.getElements().size() == 0) continue;
							
							ScriptElement call = assignment.getElements().get(0);
							
							if(call.getValue() == null) continue;
							
							Map<String, ScriptElement> params = new HashMap<String, ScriptElement>();
							if(call.getElements() != null) 
							{
								Iterator<ScriptElement> iter = call.getElements().iterator();
								while (iter.hasNext()) {
									ScriptElement pelement = iter.next();
									if(pelement.getElements() != null)
									if(pelement.getElements().size() != 0)
									params.put(pelement.getValue().toString().toLowerCase(), pelement.getElements().get(0));
								}
							}
							
							if(call.getValue().equals("instrument"))
							{
								int program = 0;
								int bank = 0;
								ScriptElement param1 = params.get("1");
								ScriptElement param2 = params.get("2");
								if(param1 != null)
								{					
									if(param1.getType() != ScriptElement.TYPE_NUMBER) continue;
									program = (int)((Double)param1.getValue()).doubleValue();
								}								
								if(param2 != null)
								{					
									if(param2.getType() != ScriptElement.TYPE_NUMBER) continue;
									bank = (int)((Double)param2.getValue()).doubleValue();
								}
								
								String programid = program + "," + bank;
								if(programs.contains(programid) || allbanks.contains(bank))
									st.remove();
							}
							
							if(call.getValue().equals("soundbank"))
							{
								int bank = 0;
								ScriptElement param2 = params.get("2");
								if(param2 != null)
								{					
									if(param2.getType() != ScriptElement.TYPE_NUMBER) continue;
									bank = (int)((Double)param2.getValue()).doubleValue();
								}
								
								if(banks.contains(bank))
									st.remove();
							}							
							
						}
						
						InstrumentsTree.this.editor.commit();
						
						
					}
				});
		
		buttonpanel.add(addbutton);
		buttonpanel.add(removebutton);
		
		scrollpane = new JScrollPane(tree);
		add(scrollpane);
		add(buttonpanel, BorderLayout.SOUTH);
	}

}
