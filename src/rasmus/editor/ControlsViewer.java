/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import rasmus.interpreter.controls.ControlViewer;

public class ControlsViewer extends JPanel implements EditorView {

	private static final long serialVersionUID = 1L;

	RasmusEditor editor;
	JMenuBar menubar;
	
	public ControlsViewer(RasmusEditor editor) 
	{
		this.editor = editor;
		
		setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		setOpaque(false);
		setLayout(new BorderLayout());
	
		menubar = new JMenuBar();		
		menubar = editor.processMenuBar(menubar);		
		
	}
	
	public JComponent getJComponent() {
		return this;
	}

	public String getEditorName() {
		return "User Interface";
	}

	public boolean commit() {
		return false;
	}

	public void init() {
		editor.setJMenuBar(menubar);
		editor.commit();
		setObjectScope(objectscope);
	}
	
	ControlViewer panelviewer = null;
    String objectscope;
	public void setObjectScope(String objectscope) {
		
		this.objectscope = objectscope;
		
		if(panelviewer != null)
		{
			remove(panelviewer);
			panelviewer.close();
			panelviewer = null;
		}
		
		if(objectscope == null || objectscope.length() == 0) objectscope = "output";
		panelviewer = new ControlViewer(editor.namespace, editor.namespace.get(objectscope));
		invalidate();
		validate();		
		editor.namespace.commit();
		add(panelviewer);
	}

	public void closing() {
	}

}
