/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.StringTokenizer;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import rasmus.editor.ScriptDocument.ScriptObject;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.metadata.FunctionCallEditor;
import rasmus.interpreter.ui.GroupMenuListener;
import rasmus.interpreter.ui.GroupMenus;
import rasmus.interpreter.ui.RSyntaxDocument;
import rasmus.interpreter.ui.RegisterFunction;
import rasmus.interpreter.ui.RegisterUnit.Record;

public class ScriptEditor extends JPanel implements EditorView {

	JMenuBar menubar;
	public JMenuBar getMenuBar()
	{
		return menubar;
	}
	
	public JComponent getJComponent()
	{
		return this;
	}
	
	public String getEditorName()
	{
		return "Source";
	}
	
	private static final long serialVersionUID = 1L;
	BufferedImage offImg;
	JPanel panel;
	JPanel cpanel;
	JTextPane textarea = new JTextPane();
	JScrollPane scrollpane;
	RasmusEditor editor;
	
	boolean process_running = false;
	
	GroupMenuListener groupmenulistener = new GroupMenuListener()
	{

		public void actionPerformed(Record record) {
			
			if(record.unit instanceof RegisterFunction)
			{				
				if(FunctionCallEditor.isEditableFunction(record.name, globalinterpreter))
				{
					String statement = FunctionCallEditor.editFunctionCall(ScriptEditor.this.editor, record.name+"()", globalinterpreter);
					if(statement != null)
					{
						textarea.replaceSelection(statement);
					}
					return;					
				}				
				textarea.replaceSelection(record.name+"()");
				return;
			}
			textarea.replaceSelection(record.name);
		}
		
	};
	


	UndoManager undo = new UndoManager();
	NameSpace globalinterpreter ;
	
	String objectscope;
	
	public String simpleifyLineSeperators(String string)
	{
		// Convert LF,CR+LF,CR   to  LF		
		StringTokenizer st = new StringTokenizer(string, "\n\r", true);
		StringBuffer sb = new StringBuffer();
		boolean last_was_r = false;
		while(st.hasMoreTokens())
		{
			String token = st.nextToken();			
			if(token.equals("\r"))
			{
				sb.append('\n');
				last_was_r = true;
			}
			else if(token.equals("\n"))
			{
				if(!last_was_r) sb.append('\n');
				last_was_r = false;
			}
			else
			{
				sb.append(token);
				last_was_r = false;
			}
		}
		return sb.toString();
	}
	public String converToSystemLineSeperator(String string)
	{
		String line_separator = System.getProperty("line.separator");
		StringTokenizer st = new StringTokenizer(string, "\n\r", true);
		StringBuffer sb = new StringBuffer();
		while(st.hasMoreTokens())
		{
			String token = st.nextToken();
			if(token.equals("\n")) sb.append(line_separator);
			else if(!token.equals("\r")) sb.append(token);
		}
		return sb.toString();
	}
	
	public void setObjectScope(String objectscope)
	{
		commit();
		this.objectscope = objectscope;
		String code = null;
		if(objectscope == null) 
			code = editor.doc.toString();
		else
		{
			ScriptObject object = editor.doc.findObject(objectscope);
			if(object == null)
				code = "";
			else
				code = object.toNormalizedString();
		}
		if(code == null) code = "";
		//textarea.selectAll();
		//textarea.replaceSelection(code);
		
		RSyntaxDocument doc = (RSyntaxDocument)textarea.getDocument();
		textarea.setDocument(new DefaultStyledDocument());
		doc.setText(simpleifyLineSeperators(code));		
		textarea.setDocument(doc);
		undo.discardAllEdits();
		SwingUtilities.invokeLater(
			new Runnable()
			{
				public void run()
				{
					scrollpane.getViewport().setViewPosition(new Point(0,0));
				}
			});
		
		is_changed = false;
	}
	public boolean commit()
	{
		if(isChanged())
		{
			String code = converToSystemLineSeperator(textarea.getText());
			if(objectscope == null) 
				editor.doc.setString(code);
			else
				editor.doc.getObject(objectscope).setNormalizedString(code);
								
			is_changed = false;
			
			setObjectScope(objectscope);
			
			return true;
		}
		return false;
	}
	public void init()
	{
		editor.setJMenuBar(menubar);
	}
	
	boolean is_changed = false;
	public void changed()
	{
		is_changed = true;
	}
	
	public boolean isChanged()
	{
		return is_changed;
	}
	
	public ScriptEditor(RasmusEditor editor) 
	{
		this.globalinterpreter = editor.namespace;
		this.editor = editor;
		
		setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		setOpaque(false);
		textarea.setDocument(new RSyntaxDocument(globalinterpreter));
		textarea.setFont(new Font("Courier", 0, 12));
		
	    // Add Undo Support to TextArea
	    
	    Document doc = textarea.getDocument();
	    doc.addDocumentListener(new DocumentListener()
	    		{
					public void insertUpdate(DocumentEvent arg0) {
						changed();
					}
					public void removeUpdate(DocumentEvent arg0) {
						changed();
					}
					public void changedUpdate(DocumentEvent arg0) {
						changed();
					}
	    		});
	    
	    doc.addUndoableEditListener(new UndoableEditListener() {
	        public void undoableEditHappened(UndoableEditEvent evt) {
	            undo.addEdit(evt.getEdit());
	        }
	    });
	    
	    ActionMap textarea_actionmap = textarea.getActionMap();
	    InputMap textarea_inputmap = textarea.getInputMap();

	    textarea_inputmap.put(KeyStroke.getKeyStroke("control Z"), "Undo");
	    textarea_inputmap.put(KeyStroke.getKeyStroke("control Y"), "Redo");
	    
	    textarea_actionmap.put("Undo",
	        new AbstractAction("Undo") {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent evt) {
	                try {
	                    if (undo.canUndo()) {
	                        undo.undo();
	                    }
	                } catch (CannotUndoException e) {
	                }
	            }
	       });
	    
	    textarea_actionmap.put("Redo",
		        new AbstractAction("Redo") {
	
	    			private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
		                try {
		                    if (undo.canRedo()) {
		                        undo.redo();
		                    }
		                } catch (CannotRedoException e) {
		                }
		            }
		        });
	    

	    
		menubar = new JMenuBar();
		
		
		JMenu edit_menu = menubar.add(new JMenu("Edit"));
		
		JMenuItem menu_undo = new JMenuItem("Undo");
		menu_undo.setMnemonic('U');
		menu_undo.setAccelerator(KeyStroke.getKeyStroke("control Z"));
		menu_undo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
                try {
                    if (undo.canUndo()) {
                        undo.undo();
                    }
                } catch (CannotUndoException e) {
                }
			}
		});		
		JMenuItem menu_cut = new JMenuItem("Cut");
		menu_cut.setMnemonic('t');
		menu_cut.setAccelerator(KeyStroke.getKeyStroke("control X"));
		menu_cut.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
              	textarea.cut();
			}
		});
		JMenuItem menu_copy = new JMenuItem("Copy");
		menu_copy.setMnemonic('C');
		menu_copy.setAccelerator(KeyStroke.getKeyStroke("control C"));
		menu_copy.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				textarea.copy();
			}
		});
		JMenuItem menu_paste = new JMenuItem("Paste");
		menu_paste.setMnemonic('P');
		menu_paste.setAccelerator(KeyStroke.getKeyStroke("control V"));		
		menu_paste.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				textarea.paste();
			}
		});		
		JMenuItem menu_delete = new JMenuItem("Delete");
		menu_delete.setMnemonic('D');
		menu_delete.setAccelerator(KeyStroke.getKeyStroke("DELETE"));		
		menu_delete.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				textarea.replaceSelection("");
			}
		});					
		JMenuItem menu_selectAll = new JMenuItem("Select All");
		menu_selectAll.setMnemonic('A');
		menu_selectAll.setAccelerator(KeyStroke.getKeyStroke("control A"));		
		menu_selectAll.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				textarea.selectAll();
			}
		});					
		JMenuItem menu_arguments = new JMenuItem("Arguments...");
		menu_arguments.setAccelerator(KeyStroke.getKeyStroke("F4"));
		menu_arguments.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {						
						try
						{
							int start = textarea.getSelectionStart();
							int[] pair = ((RSyntaxDocument)textarea.getDocument()).extractFunctionCall(start);
							if(pair == null) return;
																					
							textarea.select(pair[0], pair[0]+pair[1]);
		
							String token = textarea.getDocument().getText(pair[0], pair[1]);
														
							String expr = FunctionCallEditor.editFunctionCall(ScriptEditor.this.editor, token, globalinterpreter);
							if(expr != null)
							{
								textarea.replaceSelection(expr);
							}
							
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
				});			
		
		edit_menu.add(menu_undo);
		edit_menu.addSeparator();
		edit_menu.add(menu_cut);
		edit_menu.add(menu_copy);
		edit_menu.add(menu_paste);
		edit_menu.add(menu_delete);
		edit_menu.addSeparator();
		edit_menu.add(menu_selectAll);
		edit_menu.addSeparator();
		edit_menu.add(menu_arguments);
		
		/*
		JMenu run_menu = menubar.add(new JMenu("Run"));
		
		JMenuItem start_menuitem = run_menu.add(new JMenuItem("Run"));
		start_menuitem.setAccelerator(KeyStroke.getKeyStroke("F11"));
		start_menuitem.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0)
			{
				ScriptEditor.this.editor.run();
			}});*/
				/*
		
		JMenuItem stop_menuitem = run_menu.add(new JMenuItem("Terminate"));
		stop_menuitem.setAccelerator(KeyStroke.getKeyStroke("ESCAPE"));
		stop_menuitem.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent arg0)
			{
				stopScript();
			}});
		
		*/
		
		JMenu groupmenu = new GroupMenus(globalinterpreter, groupmenulistener);
		menubar.add(groupmenu);
		
		menubar = editor.processMenuBar(menubar);
		
		TextPopupMenu.addPopupMenu(textarea);
			
		JPanel textpanel = new JPanel();		
		textpanel.setLayout(new BorderLayout());
		textpanel.add(textarea);
		scrollpane = new JScrollPane(textpanel);
		
		setLayout(new BorderLayout());
		add(scrollpane);
		
		setVisible(true);
		
	}

	public void closing() {
		
	}
	

}
