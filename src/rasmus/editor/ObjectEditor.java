/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.editor;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ObjectEditor extends JPanel {

	private static final long serialVersionUID = 1L;

	RasmusEditor editor;
	
	JTabbedPane tabs ;
	
	ArrayList<EditorView> editors = new ArrayList();
	
	public ObjectEditor(RasmusEditor editor)
	{
		this.editor = editor;

		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		setOpaque(false);

		editors.add(new ScriptEditor(editor));
		editors.add(new TableEditor(editor));
		editors.add(new WaveEditor(editor));
		editors.add(new ControlsViewer(editor));

		tabs = new JTabbedPane();
		tabs.addChangeListener(new ChangeListener()
				{
					public void stateChanged(ChangeEvent arg0) {
						commit();
						updateView();
					}
				});
		tabs.setTabPlacement(JTabbedPane.BOTTOM);
		
		Iterator<EditorView> iterator = editors.iterator();		
		while (iterator.hasNext()) {
			EditorView editorview = iterator.next();
			tabs.addTab(editorview.getEditorName(), editorview.getJComponent());
			
		}
		
		add(tabs);
	}
	
	public void init()
	{
		editors.get(0).init();
	}
	
	public void commit()
	{
		commit(false);
	}
	
	public void commit(boolean lazy)
	{
		boolean ok = !lazy;
		Iterator<EditorView> iterator = editors.iterator();		
		while (iterator.hasNext()) 
			ok |= iterator.next().commit();	
		if(ok) editor.commit();
	}	
	
	String objectname;
	public void setObjectScope(String objectname)
	{
		this.objectname = objectname;
		updateView();
	}
	
	public void updateView()
	{
		
		EditorView editorview = editors.get(tabs.getSelectedIndex());
		editorview.setObjectScope(objectname);
		editorview.init();
						
	}

	public void closing() {

		Iterator<EditorView> iterator = editors.iterator();		
		while (iterator.hasNext()) {
			iterator.next().closing();
		}		
		editor.commit();		
	}
}
