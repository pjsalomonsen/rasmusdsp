/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

fe <- expseg(0.0001, 120, 1, 120, 0.0001, 9999, 0.0001);

f  <- resample(fe) <- expseg(20, 0.01, 1000, 10, 100, 120, 1000, 9999, 10)  + (expseg(100, 1, 10000, 120, 5000, 9999, 100) * noise());
f2 <- resample(fe) <- 100 + expseg(6000, 1, 18000, 9999, 200) * noise();

out <- sin() <- clock(f  * 2 * pi);
out <- gain(fe) <- sin() <- clock(f2 * 2 * pi);
wet <- reverb() <- out;

oe <- expseg(1, 10, 1, 30, 0.5, 120, 0.0001, 9999, 1);
out2 <- gain(oe) <- flanger() <- out;
out2 <- gain(1 - oe) <- wet;



AudioOutput() <- gain(0.5) <- agc() <- out2 * 0.2;

