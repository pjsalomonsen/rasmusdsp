/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.testing;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStreamReader;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;

public class RasmusSynthesizerTesting {

	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		File soundbankfilename = null;
		File midifilename = null;
		Frame dummyframe = new Frame();
		{
		FileDialog selectsoundbank = new FileDialog(dummyframe, "Select SoundFont", FileDialog.LOAD);
		selectsoundbank.setFilenameFilter(new FilenameFilter()
				{
					public boolean accept(File arg0, String arg1) {
						return arg0.getName().toLowerCase().endsWith(".sf2");
					}
				});			
		selectsoundbank.setModal(true);
		selectsoundbank.setVisible(true);
		soundbankfilename = new File(new File(selectsoundbank.getDirectory()), selectsoundbank.getFile());
		
		if(soundbankfilename == null) return;
		
		}
		
		{
			FileDialog selectsoundbank = new FileDialog(dummyframe, "Select MIDI File", FileDialog.LOAD);
			selectsoundbank.setFilenameFilter(new FilenameFilter()
					{
						public boolean accept(File arg0, String arg1) {
							return arg0.getName().toLowerCase().endsWith(".mid");
						}
					});			
			selectsoundbank.setModal(true);
			selectsoundbank.setVisible(true);
			midifilename = new File(new File(selectsoundbank.getDirectory()), selectsoundbank.getFile());
			}
					
		
		Soundbank soundbank = MidiSystem.getSoundbank((soundbankfilename));
		Instrument[] ins = soundbank.getInstruments();
		for (int i = 0; i < args.length; i++) {
			System.out.println(ins[i].getName()); 
		}
		
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		MidiDevice.Info info = null;
		MidiDevice.Info rtinfo = null;
		for (int i = 0; i < infos.length; i++) {
			//System.out.println(infos[i].getName());
			/*
			if(infos[i].getName().equals("Java Sound Synthesizer"))
				info = infos[i];*/
			if(infos[i].getName().equals("RasmusDSP Synthesizer"))
				info = infos[i];
			if(infos[i].getName().equals("Real Time Sequencer"))
				rtinfo = infos[i];
		}
		if(info == null) return;
		if(rtinfo == null) return;
		
		System.out.println("Create Synthesizer...");
		Synthesizer mididevice = (Synthesizer)MidiSystem.getMidiDevice(info);
		System.out.println("Load Soundbank...");
		mididevice.loadAllInstruments(soundbank);
		System.out.println("Open Device...");
		mididevice.open();
		
				
		Sequencer sequencer = (Sequencer)MidiSystem.getMidiDevice(rtinfo);
		sequencer.open();
		sequencer.getTransmitter().setReceiver(mididevice.getReceiver());
				
		Sequence sequence = MidiSystem.getSequence((midifilename));
		
		sequencer.setSequence(sequence);
				
		sequencer.start();
		System.out.println("Playing, press enter to stop");
		
		br.readLine();
		
		System.out.println("Stop...");
		
		sequencer.stop();
		mididevice.close();
		
		System.out.println("Finish...");
		System.exit(0);
	}

}
