/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.testing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.midi.MidiKeyListener;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.sf2.SF2SoundFont;
import rasmus.interpreter.sf2.SF2SoundFontManager;
import rasmus.interpreter.ui.RegisterUnit;

public class SoundFontTester extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	public static void main(String[] args) {
		SoundFontTester sft = new SoundFontTester();
		sft.setVisible(true);
	}
	
	Interpreter interpreter ;
	
	Interpreter presetinterpreter = null;
	Interpreter effectinterpreter = null;
	
	JList jlist;
	JComboBox jefectlist;
	
	int curindex = -1;
	
	public void selectPreset(int index)
	{
		
		midikeylistener.allNotesOff();
		if(index != curindex)
		{
			
			curindex = index;
			jlist.setEnabled(false);
			Runnable runnable = new Runnable()
			{
				public void run()
				{
					synchronized(SoundFontTester.this)
					{
						setTitle("Loading Preset...");
						if(presetinterpreter != null)
						{
							presetinterpreter.close();
							presetinterpreter.commit();
						}
						
						if(curindex != -1)
						{
							String preset = presets[curindex];
							presetinterpreter = new Interpreter(interpreter);
							presetinterpreter.setAutoCommit(false);
							presetinterpreter.add("filename", filename);                      //,lowpass=0
							try {
								presetinterpreter.eval("preset <- SF2Preset(filename, " + preset + ");");
							} catch (ScriptParserException e1) {
								e1.printStackTrace();
							}
							try {
								presetinterpreter.eval("preset_output <- clip(-3, 3) <- Synthesizer(preset) <- input;");
							} catch (ScriptParserException e) {
								e.printStackTrace();
							}
							presetinterpreter.commit();
						}
						setTitle(title);
					};
					jlist.setEnabled(true);
				}
			};
			Thread t = new Thread(runnable);
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
			
		}
	}
	
	String filename;
	String[] presets;
	MidiKeyListener midikeylistener;
	String title = "";
	int listmode = 0; // File Mode
	public void loadSoundFont(String filename)
	{
		midikeylistener.allNotesOff();
		Thread t2 = this.t2;
		if(t2 != null)
		{			
			try {
				t2.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.filename = filename;
		SF2SoundFont soundfont = SF2SoundFontManager.getSF2SoundFont(filename);
		NodeList presetlist = soundfont.getDocument().getElementsByTagName("preset");
		String[] listdata = new String[presetlist.getLength()];
		presets = new String[presetlist.getLength()];		
		Element[] elementlist = new Element[presetlist.getLength()];
		for (int i = 0; i < presetlist.getLength(); i++) {
			elementlist[i] = (Element)presetlist.item(i);
		}
		Arrays.sort(elementlist, new Comparator<Element>()
				{
			public int compare(Element arg0, Element arg1) {
				int preset1 = Integer.parseInt(arg0.getAttribute("preset"));
				int bank1 = Integer.parseInt(arg0.getAttribute("bank"));
				int preset2 = Integer.parseInt(arg1.getAttribute("preset"));
				int bank2 = Integer.parseInt(arg1.getAttribute("bank"));
				if(bank1 != bank2)
					return bank1 - bank2;
				return preset1 - preset2;
			}
				});
		for (int i = 0; i < elementlist.length; i++) {
			Element element = elementlist[i];
			//tabledata[i]
			//System.out.println("Preset : " + element.getAttribute("preset") + "." + element.getAttribute("bank") + " = " + element.getAttribute("name"));
			
			listdata[i] = element.getAttribute("preset") + "," + element.getAttribute("bank") + " = " + element.getAttribute("name");
			presets[i] = element.getAttribute("preset") + "," + element.getAttribute("bank");
		}
		
		listmode = 1;
		jlist.setListData(listdata);
	}
	
	File currentfile = null;
	File[] filelist; 
	
	String[] effectpresets = {"Dry", "Large Reverb", "Small Reverb", "Pure Reverb", "DelayLine 120 bpm", "DelayLine 180 bpm", "DelayLine 180 bpm Stereo", "Flanger", "Distortion", "Distortion+Flanger", "Vocoder using Mic/Line In"};	
	public void selectEffectPreset(int id)
	{
		effectinterpreter.close();
		effectinterpreter = new Interpreter(interpreter);
		effectinterpreter.setAutoCommit(false);
		try
		{
		
		switch (id) {
		case 1:
			effectinterpreter.eval("effect_output <- reverb(dry=0.7, wet=0.3) <- preset_output;");
			break;
			
		case 2:
			effectinterpreter.eval("effect_output <- reverb(dry=0.7, wet=0.3, scale=0.1, damp=0.1) <- preset_output;");
			break;
			
		case 3:
			effectinterpreter.eval("effect_output <- reverb() <- preset_output;");
			break;
			
		case 4: 
			effectinterpreter.eval("effect_output <- gain(0.5) <- delayline(60/120, 0.5) <- preset_output;");
			effectinterpreter.eval("effect_output <- preset_output;");
			break;
			
		case 5: 
			effectinterpreter.eval("effect_output <- gain(0.5) <- delayline(60/180, 0.5) <- preset_output;");
			effectinterpreter.eval("effect_output <- preset_output;");
			break;
			
		case 6: 
			effectinterpreter.eval("effect_output <- ChannelGain(0.5,0) <- delayline(60/90, 0.5) <- preset_output;");
			effectinterpreter.eval("effect_output <- ChannelGain(0,0.5) <- delayline(60/180) <- preset_output + (delayline(60/90, 0.5) <- preset_output);");
			effectinterpreter.eval("effect_output <- preset_output;");
			break;
			
			
		case 7: 
			effectinterpreter.eval("effect_output <- agc() <- flanger() <- preset_output;");
			break;
			
		case 8:
			effectinterpreter.eval("effect_output <- bw_lowpass(3000/srate()) <- gain(0.1) <- atan() <- gain(1000) <- bw_lowpass(8000/srate()) <- preset_output;");
			break;
			
		case 9:
			effectinterpreter.eval("effect_output <- flanger() <- bw_lowpass(3000/srate()) <- gain(0.1) <- atan() <- gain(1000) <- bw_lowpass(8000/srate()) <- preset_output;");
			break;
			
		case 10:
			effectinterpreter.eval("mic <- agc() <- AudioInput()*2;"); //dehiss(input=AudioInput(), 0.5);");
			effectinterpreter.eval("mono <- vocoder(mic) <- preset_output;");
			effectinterpreter.eval("effect_output <- agc() <- channelmux(mono, mono);");
			break;			
		default:
			effectinterpreter.eval("effect_output <- preset_output;");
		break;
		}
		
		}
		catch(ScriptParserException e)
		{
			e.printStackTrace();
		}
		
		effectinterpreter.commit();
	}
	
	public void selectFile(File file)
	{
		midikeylistener.allNotesOff();
		currentfile = file;
		try {
			if(file == null)
				title = "SoundFont Tester";
			else
				title = "SoundFont Tester - " + file.getCanonicalPath();
			setTitle(title);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(file != null)
			if(file.isFile())
			{
				loadSoundFont(file.getPath());
				return;
			}
		
		
		listmode = 0;
		if(file == null)
		{
			filelist = File.listRoots();
		}
		else
		{
			filelist = file.listFiles();
			
			ArrayList alist = new ArrayList();
			for (int i = 0; i < filelist.length; i++) {
				if(filelist[i].isDirectory()) alist.add(filelist[i]);
			}
			for (int i = 0; i < filelist.length; i++) {
				if(filelist[i].toString().toLowerCase().endsWith(".sf2")) alist.add(filelist[i]);
			}
			
			filelist = new File[alist.size()];
			alist.toArray(filelist);
		}
		
		String[] listdata = new String[filelist.length];
		for (int i = 0; i < filelist.length; i++) {
			if(file == null)
			{
				listdata[i] = filelist[i].toString() + " <ROOT>";
			}
			else
				if(filelist[i].isDirectory())
					listdata[i] = filelist[i].getName() + " <DIR>";
				else
					listdata[i] = filelist[i].getName();
		}
		jlist.setListData(listdata);
	}
	
	
	boolean exitonclose = true;
	public void setExitOnClose(boolean value)
	{
		exitonclose = value;
	}
	public SoundFontTester()
	{
		
		setSize(640, 400);
		setLocationByPlatform(true);
		setTitle("RasmusDSP - SoundFont Tester");
		
		addWindowListener(new WindowAdapter()
				{
			public void windowClosing(WindowEvent arg0) {
				setVisible(false);
				
				System.out.println("Close interpreter...");
				
				Thread t2 = SoundFontTester.this.t2;
				if(t2 != null)
				{			
					try {
						t2.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				interpreter.close();
				interpreter.commit();
				System.out.println("OK");
				if(exitonclose) System.exit(0);						
			}
				});
		
		// Sm��um midi keylistenter og tengjum inn� RVariable Input 
		midikeylistener = new MidiKeyListener();			
		
		
		
		
		jefectlist = new JComboBox(effectpresets);
		jefectlist.setFocusable(false);
		jefectlist.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						selectEffectPreset(jefectlist.getSelectedIndex());
					}
				});
		jefectlist.setEnabled(false);
		
		
		jlist = new JList()
		{
			private static final long serialVersionUID = 1L;
			protected boolean processKeyBinding(KeyStroke s, KeyEvent e, int arg2, boolean arg3) {
				
				if(e.getKeyCode() == KeyEvent.VK_PAGE_UP || e.getKeyCode() == KeyEvent.VK_PAGE_DOWN)
				{
					return false;
				}
				
				if(listmode == 0)
				{
					return super.processKeyBinding(s, e, arg2, arg3);
				}
				
				if(e.getID() == KeyEvent.KEY_TYPED) return false;
				if(midikeylistener.isConsumed(e)) return false;
				return super.processKeyBinding(s, e, arg2, arg3);
				
			}
			protected void processKeyEvent(KeyEvent e) {
				
				int id = e.getID();
				
				
				
				if(id == KeyEvent.KEY_PRESSED)
				{
					if(e.getKeyCode() == KeyEvent.VK_PAGE_UP)
					{
						midikeylistener.setOctave(midikeylistener.getOctave() +1);
						return;
					}
					if(e.getKeyCode() == KeyEvent.VK_PAGE_DOWN)
					{
						midikeylistener.setOctave(midikeylistener.getOctave() -1);
						return;
					}
					if(e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
					{
						if(currentfile != null)
							selectFile(currentfile.getParentFile());
						return;
					}
					
					if(e.getKeyCode() == KeyEvent.VK_ENTER)
					{
						if(listmode == 0) 
						{
							int li = jlist.getSelectedIndex();
							if(li != -1)
							{
								selectFile(filelist[li]);
							}
						}
						
					}
				};
				
				if(listmode == 0)
				{
					super.processKeyEvent(e);
					return;
				}
				
				if(id == KeyEvent.KEY_TYPED) return;
				if(id == KeyEvent.KEY_PRESSED) midikeylistener.keyPressed(e);
				if(id == KeyEvent.KEY_TYPED) midikeylistener.keyTyped(e);
				if(id == KeyEvent.KEY_RELEASED) midikeylistener.keyReleased(e);
				
				
				if(!e.isConsumed())
				{
					super.processKeyEvent(e);
				}
			}
			
		};
		
		jlist.addListSelectionListener(new ListSelectionListener()
				{
			public void valueChanged(ListSelectionEvent arg0) {
				if(listmode == 0) 
				{
					return;
				}
				else
					selectPreset(jlist.getSelectedIndex());
			}
				});
		jlist.addMouseListener(new MouseAdapter()
				{
			public void mouseClicked(MouseEvent arg0) {
				if(listmode == 0) 
					if(arg0.getClickCount() == 2)
					{
						int li = jlist.getSelectedIndex();
						if(li != -1)
						{
							selectFile(filelist[li]);
						}
					}
			}
			
				});
		//jlist.addKeyListener(midikeylistener);
		
		JPanel sp = new JPanel();
		sp.setLayout(new BorderLayout());
		sp.add(jefectlist, BorderLayout.NORTH);
		sp.add(new JScrollPane(jlist), BorderLayout.CENTER);
		add(sp);
		selectFile(null);
		
		
		t2 = new Thread()
		{
			public void run()
			{
				
				
				
				//(new AudioSession(48000, 2)).close();, not needed in small projects
				System.out.println("Initilize interpreter...");
				interpreter = new Interpreter();
				
				Object[] audioutputs = ObjectsPart.asObjects(interpreter.get("group_audio_dev_output"));
				for (int i = 0; i < audioutputs.length; i++) {
					audioutputs[i] = ((RegisterUnit.Record)audioutputs[i]).name;
				}
				Object audiooutput = showInputDialog("Select Audio Output", audioutputs);
				
				Object[] bufferlens = {"100", "90", "80", "70", "60", "50", "40", "30", "20", "10", "5", "1"};
				Object bufferlen = showInputDialog("Select Audio Buffer Length (ms)", bufferlens);
				if(bufferlen == null) bufferlen = "100";
				
				bufferlen  = Double.toString( Double.parseDouble( (String)bufferlen ) / 1000.0 );
				
				Object[] midiinputs = ObjectsPart.asObjects(interpreter.get("group_midi_dev_input"));
				for (int i = 0; i < midiinputs.length; i++) {
					midiinputs[i] = ((RegisterUnit.Record)midiinputs[i]).name;
				}
				Object midiinput = showInputDialog("Select MIDI Input", midiinputs);
				
				
				
				midikeylistener.setReceiver(MidiSequence.getInstance(interpreter.get("input")));
				
				
				// Tengjum input vi� MidiOutput
				System.out.println("Create AudioOutput(\"" + audiooutput + "\", 44100hz, 2ch, 16bits)...");
				//interpreter.eval("AudioOutput(rate=44100, channels=2, bits=16, priority=0) <- agc() <- gain(0.3) <- reverb(dry=1, wet=0.3) <- preset_output;");
				
				effectinterpreter = new Interpreter(interpreter);
				effectinterpreter.setAutoCommit(false);
				try {
					effectinterpreter.eval("effect_output <- preset_output;");
				} catch (ScriptParserException e) {
					e.printStackTrace();
				}
				
				if(audiooutput != null)
					try {
						interpreter.eval("AudioOutput(\"" + audiooutput + "\", bufferlen=" + bufferlen + ", rate=44100, channels=2, bits=16, blocking=0) <- agc() <- gain(0.7) <- effect_output;");
					} catch (ScriptParserException e1) {
						e1.printStackTrace();
					}
				else
					try {
						interpreter.eval("AudioOutput(bufferlen=" + bufferlen + ", rate=44100, channels=2, bits=16, blocking=0) <- agc() <- gain(0.7) <- effect_output;");
					} catch (ScriptParserException e1) {
						e1.printStackTrace();
					}
				//interpreter.eval("AudioOutput(rate=44100, channels=2, bits=16, priority=0) <- agc() <- gain(0.7) <- reverb(dry=1, wet=0.3) <- clip(-3,3) <- preset_output;");
				
				if(midiinput != null)
				{
					System.out.println("Create MidiInput(\"" + midiinput + "\")...");
					try {
						interpreter.eval("input <- MidiInput(\"" + midiinput + "\");");
					} catch (ScriptParserException e) {
						e.printStackTrace();
					}
				}
				System.out.println("OK");
				//interpreter.eval("AudioOutput(rate=44100, channels=2,priority=0) <- agc() <- gain(0.3) <- reverb(dry=0, wet=1.0) <- preset_output;");		
				interpreter.commit();
				jefectlist.setEnabled(true);
				
				synchronized(t2)
				{
					t2 = null;
				}
			}
		};
		t2.setPriority(Thread.MIN_PRIORITY);
		t2.start();
		
	}
	
	public Object showInputDialog(String title, Object[] midioutputs) 
	{
		SelectMidiRunner selectMidiRunner = new SelectMidiRunner();
		selectMidiRunner.midioutputs = midioutputs;
		selectMidiRunner.title = title;
		try {
			SwingUtilities.invokeAndWait(selectMidiRunner);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return selectMidiRunner.ret;
	}
	
	class SelectMidiRunner implements Runnable 
	{
		Object[] midioutputs;
		Object ret;
		String title;
		public void run()
		{
			ret = JOptionPane.showInputDialog(SoundFontTester.this,
					title, title,
					JOptionPane.INFORMATION_MESSAGE, null,
					midioutputs, midioutputs[0]); 
		}
	}	
	
	Thread t2 = null;
	
}
