/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.testing;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.Line;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;

public class RasmusSynthesizerTesting2 {

	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		File soundbankfilename = null;
		File midifilename = null;
		Frame dummyframe = new Frame();
		{
		FileDialog selectsoundbank = new FileDialog(dummyframe, "Select SoundFont", FileDialog.LOAD);
		selectsoundbank.setFilenameFilter(new FilenameFilter()
				{
					public boolean accept(File arg0, String arg1) {
						return arg0.getName().toLowerCase().endsWith(".sf2");
					}
				});			
		selectsoundbank.setModal(true);
		selectsoundbank.setVisible(true);
		soundbankfilename = new File(new File(selectsoundbank.getDirectory()), selectsoundbank.getFile());
		
		if(soundbankfilename == null) return;
		
		}
		
		{
			FileDialog selectsoundbank = new FileDialog(dummyframe, "Select MIDI File", FileDialog.LOAD);
			selectsoundbank.setFilenameFilter(new FilenameFilter()
					{
						public boolean accept(File arg0, String arg1) {
							return arg0.getName().toLowerCase().endsWith(".mid");
						}
					});			
			selectsoundbank.setModal(true);
			selectsoundbank.setVisible(true);
			midifilename = new File(new File(selectsoundbank.getDirectory()), selectsoundbank.getFile());
			}
					
		
		Soundbank soundbank = MidiSystem.getSoundbank((soundbankfilename));
		Instrument[] ins = soundbank.getInstruments();
		for (int i = 0; i < args.length; i++) {
			System.out.println(ins[i].getName()); 
		}
		
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		MidiDevice.Info info = null;
		MidiDevice.Info rtinfo = null;
		for (int i = 0; i < infos.length; i++) {
			//System.out.println(infos[i].getName());
			/*
			if(infos[i].getName().equals("Java Sound Synthesizer"))
				info = infos[i];*/
			if(infos[i].getName().equals("RasmusDSP Synthesizer"))
				info = infos[i];
			if(infos[i].getName().equals("Real Time Sequencer"))
				rtinfo = infos[i];
		}
		if(info == null) return;
		if(rtinfo == null) return;
		
		System.out.println("Create Synthesizer...");
		Synthesizer mididevice = (Synthesizer)MidiSystem.getMidiDevice(info);
		System.out.println("Load Soundbank...");
		mididevice.loadAllInstruments(soundbank);
		System.out.println("Open Device...");
		mididevice.open();
		
		TargetDataLine dataline = (TargetDataLine)((Mixer)mididevice).getLine( new Line.Info(TargetDataLine.class));
		AudioFormat format = new AudioFormat(44100, 16, 2, true, false);
		dataline.open(format);
				
		Sequencer sequencer = (Sequencer)MidiSystem.getMidiDevice(rtinfo);
		sequencer.open();
		sequencer.getTransmitter().setReceiver(mididevice.getReceiver());
				
		Sequence sequence = MidiSystem.getSequence((midifilename));
		
		sequencer.setSequence(sequence);
				
		OutputWriterThread outputwriter = new OutputWriterThread();
		outputwriter.dataline = dataline;
		outputwriter.start();
		
		sequencer.start();
		System.out.println("Playing, press enter to stop");			
		
		br.readLine();
		
		System.out.println("Stop...");
		
		sequencer.stop();
		
		outputwriter.active = false;
		outputwriter.join();
		
		dataline.close();
		
		mididevice.close();
		
		System.out.println("Finish...");
		System.exit(0);
	}
	
	
		
    static class OutputWriterThread extends Thread 
	{
		boolean active = true;	
		TargetDataLine dataline;
		public void run()
		{
			try
			{
			FileOutputStream fos = new FileOutputStream("C:\\testaudio.raw");
						
			byte[] buffer = new byte[500 * 4]; // 16 bit Stereo
			double waittime = 500.0 / 44100.0;
			waittime /= 2;
			long sleepTime = (long)(waittime * 1000 * 1000000);
			System.out.println("waittime = " + waittime);
			System.out.println("waittime = " + (sleepTime / 1000000));
			System.out.println("waittime = " + (sleepTime % 1000000));
			
			while(active)
			{
				dataline.read(buffer, 0, buffer.length);
				fos.write(buffer);
				Thread.sleep(sleepTime / 1000000, (int)(sleepTime % 1000000));
			}
			fos.close();
						
			}
			catch(Exception e)
			{				
				e.printStackTrace();
			}
		}
	};

}
