/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.testing;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.swing.JOptionPane;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.ui.RegisterUnit;

public class FmInstrumentTest {
	
	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		Interpreter interpreter = new Interpreter();
		
		try
		{
			
			Object[] midiinputs = ObjectsPart.asObjects(interpreter.get("group_midi_dev_input"));
			for (int i = 0; i < midiinputs.length; i++) {
				midiinputs[i] = ((RegisterUnit.Record)midiinputs[i]).name;
			}
			Object midiinput = JOptionPane.showInputDialog(null, "Select MIDI Device", "Select MIDI Device", JOptionPane.INFORMATION_MESSAGE, null, midiinputs, midiinputs[0]);
			if(midiinput == null)
			{
				return;
			}
			
			interpreter.eval("input <- MidiInput(\"" + midiinput.toString() + "\");");
			
			interpreter.source(SoundGenerator1.class.getResourceAsStream("/rasmus/testing/FmInstrumentTest.r"));					
			
			System.out.println("Playing, press enter to stop");
			
			br.readLine();
			
			System.out.println("Stop...");
			
		}
		finally
		{
			interpreter.close();
			interpreter.commit();
		}
		
	}
	
}
