
// For testing soundfonts go to: tools -> SoundFont Tester...

/**************************************
 *  Example Script
 **************************************/

// if you want to use external midi device,
// include following line:
//
// midiinputvar <- MidiInput("1:EDIROL PCR 1");

/**************************************
 *  User Interface
 **************************************/

<- spectrumgraph(height=40,channels=2,row=0,colspan=2,fill=1,margin=2,mode=0) <- mon;
<- cepstrumgraph(height=80,channels=2,row=1,col=0,fill=1,margin=2,fftframesize=256,mode=2) <- mon;
<- spectrumgraph(height=80,channels=2,row=1,col=1,fill=1,margin=2,mindb=-90,mode=2) <- mon;
<- oscilloscopeXY(channels=2, margin=2,row=2,col=0,refreshrate=40) <- mon;
<- oscilloscope(channels=2, margin=2,row=2,col=1,refreshrate=40) <- mon;
<- MidiKeyboard(midiinputvar, colspan=2, row=4, margin=2);
<- label("Modulation:", row=5, colspan=4, margin=2);
<- midicontrolslider(modulator_midi, 1,4,min=0,max=12,row=6, colspan=2, margin=2);

/**************************************
 *  Audio Synthesis
 **************************************/

modamount <- RenderControl(1, 4) <- modulator_midi;
FmSound2 <- function(note, velocity, active, pitch, modamount)
{
	freq <- 440*pi*pow(2, (note - 57)/12) * pitch;
	osc2 <- sin() <- clock(freq*modamount);
	e <- linseg(0.1, 1, 2.5, 5, 0.75, 9999, 0.75);
	osc <- sin() <- clock(freq) + (osc2 * e);
	out <- gain(velocity*0.2) <- osc;
	output <- adsr(active, 0.02, 0.1, 0.5, 0.2) * out;
};
AudioOutput() <- AudioMonitor(mon) <- reverb() <- flanger() <- Synthesizer(FmSound2, modamount=modamount) <- midiinputvar;


