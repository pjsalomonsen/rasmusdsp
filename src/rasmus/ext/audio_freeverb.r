/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 // Reverb

comb <- function(delay, amount, maxdelay, damp, input)
{
   <-  DelayLine(input, delay, amount, maxdelay, damp);
};

allpass <- function(delay, amount, maxdelay, damp, input)
{
   <-  DelayLine(input, delay, amount, maxdelay, damp) - input;
};

FreeVerb <- function(roomsize=0.9, damp=0, input)
{
  delayL <- DelayLine(0.0253, roomsize, 0, damp) <- input;
  delayL <- DelayLine(0.0269, roomsize, 0, damp) <- input;
  delayL <- DelayLine(0.0289, roomsize, 0, damp) <- input;
  delayL <- DelayLine(0.0307, roomsize, 0, damp) <- input;
  delayL <- DelayLine(0.0322, roomsize, 0, damp) <- input;
  delayL <- DelayLine(0.0338, roomsize, 0, damp) <- input;
  delayL <- DelayLine(0.0357, roomsize, 0, damp) <- input;
  delayL <- DelayLine(0.0366, roomsize, 0, damp) <- input;
  allpassL1 <-  DelayLine(input=delayL,   0.0126, 0.5) - delayL;
  allpassL2 <-  DelayLine(input=allpassL1, 0.0100, 0.5) - allpassL1;
  allpassL3 <-  DelayLine(input=allpassL2, 0.0070, 0.5) - allpassL2;
  allpassL4 <-  DelayLine(input=allpassL3, 0.0051, 0.5) - allpassL3;

  delayR <- DelayLine(0.0253 + 0.005, roomsize, 0, damp) <- input;
  delayR <- DelayLine(0.0269 + 0.005, roomsize, 0, damp) <- input;
  delayR <- DelayLine(0.0289 - 0.005, roomsize, 0, damp) <- input;
  delayR <- DelayLine(0.0307 - 0.005, roomsize, 0, damp) <- input;
  delayR <- DelayLine(0.0322 + 0.005, roomsize, 0, damp) <- input;
  delayR <- DelayLine(0.0338 + 0.005, roomsize, 0, damp) <- input;
  delayR <- DelayLine(0.0357 - 0.005, roomsize, 0, damp) <- input;
  delayR <- DelayLine(0.0366 - 0.005, roomsize, 0, damp) <- input;
  allpassR1 <-  DelayLine(input=delayR,   0.0126 - 0.005, 0.5) - delayR;
  allpassR2 <-  DelayLine(input=allpassR1, 0.0100 + 0.005, 0.5) - allpassR1;
  allpassR3 <-  DelayLine(input=allpassR2, 0.0070 - 0.005, 0.5) - allpassR2;
  allpassR4 <-  DelayLine(input=allpassR3, 0.0051 + 0.005, 0.5) - allpassR3;

  <- gain(0.02) <- channelmux(allpassL4, allpassR4);

};