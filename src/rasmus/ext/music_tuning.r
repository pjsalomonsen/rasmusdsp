/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 // Music Tuning

groups                    <- RegisterGroup("Music Tuning") <- group_music_tuning;

// The Golden Mean Constant, sj� http://en.wikipedia.org/wiki/Golden_ratio
phi <- (1+sqrt(5))/2;
group_math_constants          <- RegisterConstant("phi", "The Golden Mean Constant");

ChromaticScale <- function(note, n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12)
{
  <- scale(note, pow(2, n1/1200), pow(2, n2/1200), pow(2, n3/1200),
                 pow(2, n4/1200), pow(2, n5/1200), pow(2, n6/1200),
                 pow(2, n7/1200), pow(2, n8/1200), pow(2, n9/1200),
                 pow(2, n10/1200), pow(2, n11/1200), pow(2, n12/1200));
};

//----------------------------------------------------------------------
//
//   Equal Tempered Scales
//
//      "Equal temperament is a scheme of musical tuning in which the octave is divided 
//      into a series of equal steps (equal frequency ratios). 
//      The best known example of such a system is twelve-tone equal temperament, 
//      sometimes abbreviated to 12-TET, which is nowadays used in most Western music. 
//      Other equal temperaments do exist (some music has been written in 19-TET and 31-TET 
//      for example, and Arab music is based on a 24-tone equal temperament), 
//      but they are so rare that when people use the term equal temperament without qualification, 
//      it is usually understood that they are talking about the twelve tone variety." - Wikipedia
//
//   References:
//      http://en.wikipedia.org/wiki/Equal_temperament
//      http://en.wikipedia.org/wiki/Quarter_tone_scale
//      http://en.wikipedia.org/wiki/19_tone_equal_temperament
//      http://en.wikipedia.org/wiki/22_tone_equal_temperament
//      http://en.wikipedia.org/wiki/53_tone_equal_temperament
//      http://en.wikipedia.org/wiki/72_tone_equal_temperament
//      http://www.microtonal-synthesis.com/scales.html
//      http://members.aol.com/bpsite/scales.html
//
//----------------------------------------------------------------------



// (12-tET) twelve-tone equal temperament
Tuning_12tET <- function(note) { <- pow(2, note/12); };

// (19-tET) 19 tone equal temperament
// second equal temperament dealing with 5-limit music in a tolerable manner.
// Francisco de Salinas proposed in 1577.
Tuning_19tET <- function(note) { <- pow(2, note/19); };

// (22-tET) 22 tone equal temperament  
// third equal temperament dealing with 5-limit music in a tolerable manner.
// Originated with nineteenth century music theorist Robert Holford Macdowall Bosanquet (English scientist)
// Inspired by the division of the octave into 22 unequal parts in the music theory of India.
Tuning_22tET <- function(note) { <- pow(2, note/22); };

// (24-tET) quarter tone scale equal temperament, used in Arab tone system
Tuning_24tET <- function(note) { <- pow(2, note/24); };

// (31-tET) 31 tone equal temperament
Tuning_31tET <- function(note) { <- pow(2, note/31); };

// (41-tET) 41 tone equal temperament
Tuning_41tET <- function(note) { <- pow(2, note/41); };

// (53-tET) 53 tone equal temperament (Holdrian comma)
Tuning_53tET <- function(note) { <- pow(2, note/53); };

// (72-tET) 72 tone equal temperament
Tuning_72tET <- function(note) { <- pow(2, note/53); };

// (19-tET|24) 19 tone equal temperament mapped to 24 midi notes
Tuning_19tET24 <- function(note) { 
<- Scale(note, pow(2, 1/19), pow(2, 1/19),
               pow(2, 2/19), pow(2, 3/19), pow(2, 4/19),  pow(2, 5/19),
               pow(2, 6/19), pow(2, 6/19), 
               pow(2, 7/19), pow(2, 7/19),
               pow(2, 8/19), pow(2, 9/19), pow(2, 10/19), pow(2, 11/19), pow(2, 12/19), pow(2, 13/19), 
               pow(2, 14/19), pow(2, 14/19), 
               pow(2, 15/19), pow(2, 16/19), 
               pow(2, 17/19), pow(2, 17/19), 
               pow(2, 18/19), pow(2, 19/19)); };

// Bohlen-Pierce scale. 13-tone equal division of 3/1
Tuning_Bohlen_Pierce_13tET <- function(note) { <- pow(3, (note/13) - 2); };

// Wendy Carlos Alpha scale, http://www.wendycarlos.com/resources.html#tables
Tuning_Wendy_Carlos_Alpha_ET <- function(note) { <- pow(2, note*78/1200); };

// Wendy Carlos Beta scale, http://www.wendycarlos.com/resources.html#tables
Tuning_Wendy_Carlos_Beta_ET <- function(note) { <- pow(2, note*63.8/1200); };

// Wendy Carlos Gamma scale, http://www.wendycarlos.com/resources.html#tables
Tuning_Wendy_Carlos_Gamma_ET <- function(note) { <- pow(2, note*35.1/1200); };

group_music_tuning        <- RegisterGroup("Equal Tempered Scales") <- group_music_tuning_et;
group_music_tuning_et     <- RegisterFunction("Tuning_12tET", "Twelve-tone equal temperament");
group_music_tuning_et     <- RegisterFunction("Tuning_19tET", "19 tone equal temperament");
group_music_tuning_et     <- RegisterFunction("Tuning_19tET24", "19 tone equal temperament, mapped to 24 midi notes");
group_music_tuning_et     <- RegisterFunction("Tuning_22tET", "22 tone equal temperament");
group_music_tuning_et     <- RegisterFunction("Tuning_24tET", "24 tone equal temperament, used in Arab tone system");
group_music_tuning_et     <- RegisterFunction("Tuning_31tET", "31 tone equal temperament");
group_music_tuning_et     <- RegisterFunction("Tuning_41tET", "41 tone equal temperament");
group_music_tuning_et     <- RegisterFunction("Tuning_53tET", "53 tone equal temperament");
group_music_tuning_et     <- RegisterFunction("Tuning_72tET", "72 tone equal temperament");
group_music_tuning_et     <- RegisterFunction("Tuning_Bohlen_Pierce_13tET", "Bohlen-Pierce scale. 13-tone equal division of 3/1");
group_music_tuning_et     <- RegisterFunction("Tuning_Wendy_Carlos_Alpha_ET", "Wendy Carlos Alpha scale");
group_music_tuning_et     <- RegisterFunction("Tuning_Wendy_Carlos_Beta_ET", "Wendy Carlos Beta scale");
group_music_tuning_et     <- RegisterFunction("Tuning_Wendy_Carlos_Gamma_ET", "Wendy Carlos Gamma scale");

//----------------------------------------------------------------------
//
//   Just Intonation scales
//
//      "In music, Just intonation, also called rational intonation, 
//       is any musical tuning in which the frequencies of notes are related by whole number ratios; 
//       that is, by positive rational numbers. 
//
//   References:
//      http://en.wikipedia.org/wiki/Equal_temperament
//      http://www.microtonal-synthesis.com/scales.html
//      http://members.aol.com/bpsite/scales.html
//
//----------------------------------------------------------------------

// Basic Just Intonation with a 7 limit tritone
Tuning_12tJI1 <- function(note) { <- Scale(note, 16/15, 9/8, 6/5, 5/4, 4/3, 7/5, 3/2, 8/5, 5/3, 9/5, 15/8, 2/1); };

// 12-tone Just Intonationm, minor seventh=7/4
Tuning_12tJI2 <- function(note) { <- Scale(note, 16/15, 9/8, 6/5, 5/4, 4/3, 7/5, 3/2, 8/5, 5/3, 7/4, 15/8, 2/1); };

// 12-tone Just Intonation, minor seventh=16/9
Tuning_12tJI3 <- function(note) { <- Scale(note, 16/15, 9/8, 6/5, 5/4, 4/3, 7/5, 3/2, 8/5, 5/3, 16/9, 15/8, 2/1); };

// Bohlen-Pierce scale. 13-tone Just Intonation, division of 3/1
Tuning_Bohlen_Pierce_13tJI <- function(note) { <- Scale(note, 27/25, 25/21, 9/7, 7/5, 75/49, 5/3, 9/5, 49/25, 15/7, 7/3, 63/25, 25/9, 3/1); };

// Bohlen-Pierce scale. 10-tone Just Intonation, division of 3/1, basic diatonic BP scale
Tuning_Bohlen_Pierce_10tJI <- function(note) { <- Scale(note, 25/21, 9/7, 7/5, 5/3, 9/5, 15/7, 7/3, 25/9, 3/1); };

// Bohlen-Pierce scale. 10-tone Just Intonation, division of 3/1, basic diatonic BP scale mapped to 12 midi notes
//                                                             C#   D      D#   E    F    F#,  G    G#   A     A#   B     C
Tuning_Bohlen_Pierce_10tJI12 <- function(note) { <- Scale(note, 1/1, 25/21, 1/1, 9/7, 7/5, 1/1, 5/3, 9/5, 15/7, 7/3, 25/9, 3/1); };
                   
// Ben Johnston 25 note just enharmonic scale
Tuning_Ben_Johnston_25tJI <- function(note) 
{ <- Scale(note, 25/24, 135/128, 16/15, 10/9, 9/8, 75/64, 6/5, 5/4, 81/64, 32/25, 4/3, 27/20, 45/32, 
                 36/25, 3/2, 25/16, 8/5, 5/3, 27/16, 225/128, 16/9, 9/5, 15/8, 48/25, 2/1
); };

// Harry Partch 43 tone Just Intonation scale
Tuning_Harry_Partch_43tJI <- function(note) 
{ <- Scale(note, 
81/80, 33/32, 21/20, 16/15, 12/11, 11/10, 10/9, 9/8, 8/7, 7/6, 32/27, 6/5, 11/9, 5/4, 14/11, 9/7,
21/16, 4/3, 27/20, 11/8, 7/5, 10/7, 16/11, 40/27, 3/2, 32/21, 14/9, 11/7, 8/5, 18/11, 5/3, 
27/16, 12/7, 7/4, 16/9, 9/5, 20/11, 11/6, 15/8, 40/21, 64/33, 160/81, 2/1
); };

// John Chalmers 19 tone Just Intonation scale
Tuning_John_Chalmers_19tJI <- function(note) 
{ <- Scale(note, 
21/20, 16/15, 9/8, 7/6, 6/5, 5/4, 21/16, 4/3, 7/5, 35/24, 
3/2, 63/40, 8/5, 5/3, 7/4, 9/5, 28/15, 63/32, 2/1
); };

// Jon Catler 24 tone Just Intonation Scale "over and under the 13 limit"
Tuning_Jon_Catler_24tJI <- function(note) 
{ <- Scale(note, 
33/32, 16/15, 9/8, 8/7, 7/6, 6/5, 128/105, 16/13, 5/4, 21/16, 4/3, 11/8,
45/32, 16/11, 3/2, 8/5, 13/8, 5/3, 27/16, 7/4, 16/9, 24/13, 15/8, 2/1
); };

// Lou Harrison 16 tone Just Intonation scale
Tuning_Lou_Harrison_16tJI <- function(note) 
{ <- Scale(note, 
16/15, 10/9, 8/7, 7/6, 6/5, 5/4, 4/3, 17/12, 3/2, 8/5, 5/3, 12/7, 7/4, 9/5, 15/8, 2/1
); };

// Mayumi Reinhard 13 limit Just Intonation scale
Tuning_Mayumi_Reinhard_13tJI <- function(note) 
{ <- Scale(note, 
14/13, 13/12, 16/13, 13/10, 18/13, 13/9, 20/13, 13/8, 22/13, 13/7, 208/105, 2/1
); };

// Wendy Carlos Harmonic scale
Tuning_Wendy_Carlos_Harmonic_12tJI <- function(note) 
{ <- Scale(note,
17/16, 9/8, 19/16, 5/4, 21/16, 11/8, 3/2, 13/8, 27/16, 7/4, 15/8, 2/1
); };

// Wendy Carlos Super Just Intonation scale
Tuning_Wendy_Carlos_Super_12tJI <- function(note) 
{ <- Scale(note,
17/16, 9/8, 6/5, 5/4, 4/3, 11/8, 3/2, 13/8, 5/3, 7/4, 15/8, 2/1
); };

group_music_tuning        <- RegisterGroup("Just Intonation Scales") <- group_music_tuning_just;
group_music_tuning_just   <- RegisterFunction("Tuning_12tJI1", "Basic Just Intonation with a 7 limit tritone");
group_music_tuning_just   <- RegisterFunction("Tuning_12tJI2", "12-tone Just Intonationm, minor seventh=7/4");
group_music_tuning_just   <- RegisterFunction("Tuning_12tJI3", "12-tone Just Intonation, minor seventh=16/9");
group_music_tuning_just   <- RegisterFunction("Tuning_Bohlen_Pierce_13tJI", "Bohlen-Pierce scale. 13-tone Just Intonation, division of 3/1");
group_music_tuning_just   <- RegisterFunction("Tuning_Bohlen_Pierce_10tJI", "Bohlen-Pierce scale. 10-tone Just Intonation, division of 3/1, basic diatonic BP scale");
group_music_tuning_just   <- RegisterFunction("Tuning_Bohlen_Pierce_10tJI12", "Bohlen-Pierce scale. 10-tone Just Intonation, division of 3/1, mapped to 12 midi notes");
group_music_tuning_just   <- RegisterFunction("Tuning_Ben_Johnston_25tJI", "Ben Johnston 25 note just enharmonic scale");
group_music_tuning_just   <- RegisterFunction("Tuning_Harry_Partch_43tJI", "Harry Partch 43 tone Just Intonation scale");
group_music_tuning_just   <- RegisterFunction("Tuning_John_Chalmers_19tJI", "John Chalmers 19 tone Just Intonation scale");
group_music_tuning_just   <- RegisterFunction("Tuning_Jon_Catler_24tJI", "Jon Catler 24 tone Just Intonation Scale 'over and under the 13 limit'");
group_music_tuning_just   <- RegisterFunction("Tuning_Lou_Harrison_16tJI", "Lou Harrison 16 tone Just Intonation scale");
group_music_tuning_just   <- RegisterFunction("Tuning_Mayumi_Reinhard_13tJI", "Mayumi Reinhard 13 limit Just Intonation scale");
group_music_tuning_just   <- RegisterFunction("Tuning_Wendy_Carlos_Harmonic_12tJI", "Wendy Carlos Harmonic scale");
group_music_tuning_just   <- RegisterFunction("Tuning_Wendy_Carlos_Super_12tJI", "Wendy Carlos Super Just Intonation scale");

//----------------------------------------------------------------------
//
//   Well temperament
//
//      "Well temperament (or circular temperament) 
//       is a type of tuning described in 20th-century music theory. 
//       The term is modelled on the German word wohltemperiert 
//       which appears in the title of J.S. Bach's famous composition, 
//       Well-Tempered Clavier. The phrase wohl temperiert also occurs in the works of Bach's predecessor, 
//       the organ tuner and music theorist Andreas Werckmeister. 
//       The German usage is gute Temperatur "good temperament", 
//       but "well temperament" has become ingrained, 
//       despite its perceived ungrammaticality and hence avoidance by some authors." - Wikipedia
//
//   References:
//      http://en.wikipedia.org/wiki/Well_temperament
//      http://en.wikipedia.org/wiki/Pythagorean_tuning
//      http://www.microtonal-synthesis.com/scale_kirnberger.html
//      http://www.microtonal-synthesis.com/scale_werckmeisterIII.html
//      http://www.microtonal-synthesis.com/scale_vallotti_young.html
//      http://www.microtonal-synthesis.com/scale_pythagorean.html
//
//----------------------------------------------------------------------


// Andreas Werckmeister Temperament III (circa 1681)
Tuning_Andreas_Werckmeister_Temperament_III <- function(note) { 
    <- ChromaticScale(note,  90.225, 192.180, 294.135, 390.225, 498.045, 588.270,
                             696.090, 792.180, 888.270, 996.091, 1092.180, 1200);
};

// Kirnberger scale
Tuning_Kirnberger <- function(note) { 
    <- ChromaticScale(note,  90.225, 193.849, 294.135,  386.314,  498.045,  590.224, 
                            696.663, 792.180, 889.650,  996.091, 1088.269, 1200);
};


// Vallotti & Young scale (Vallotti version)
Tuning_Vallotti_Young <- function(note) { 
    <- ChromaticScale(note,  94.135, 196.090, 298.045,  392.180, 501.955,   592.180, 
                            698.045, 796.090, 894.135, 1000.000, 1090.225, 1200); 
};


group_music_tuning        <- RegisterGroup("Well temperament") <- group_music_tuning_well;
group_music_tuning_well   <- RegisterFunction("Tuning_Andreas_Werckmeister_Temperament_III", "Andreas Werckmeister Temperament III (circa 1681)");
group_music_tuning_well   <- RegisterFunction("Tuning_Kirnberger", "Kirnberger scale");
group_music_tuning_well   <- RegisterFunction("Tuning_Vallotti_Young", "Vallotti & Young scale (Vallotti version)"); 

//----------------------------------------------------------------------
//
//   Pythagorean tuning 
//
//      "Pythagorean tuning is a system of musical tuning in which the frequency relationships
//       of all intervals are based on the ratio 3:2. Its discovery is generally credited to Pythagoras. 
//       It is the oldest way of tuning the 12-note chromatic scale and, as such, 
//       it is the basis for many other methods of tuning." - Wikipedia
//
//   References:
//      http://en.wikipedia.org/wiki/Pythagorean_tuning
//
//----------------------------------------------------------------------


// Pythagorean scale (12 tone)
Tuning_Pythagorean <- function(note) { 
    <- Scale(note, 2187/2048, 9/8, 32/27, 81/64, 4/3, 729/512, 3/2, 128/81, 27/16, 16/9, 243/128, 2/1); 
};
// Pythagorean tuning, E flat
Tuning_Pythagorean_E <- function(note) { <- Scale(note + 10, 256/243, 9/8, 32/27, 81/64, 4/3, 729/512, 3/2, 128/81, 27/16, 16/9, 243/128, 2/1) / (16/9); };

// Pythagorean tuning, D sharp
Tuning_Pythagorean_D <- function(note) { <- Scale(note + 10, 2187/2048, 9/8, 32/27, 81/64, 4/3, 729/512, 3/2, 128/81, 27/16, 16/9, 243/128, 2/1) / (16/9); };

group_music_tuning        <- RegisterGroup("Pythagorean tuning") <- group_music_tuning_pyth;
group_music_tuning_pyth   <- RegisterFunction("Tuning_Pythagorean", "Pythagorean scale"); 
group_music_tuning_pyth   <- RegisterFunction("Tuning_Pythagorean_E", "Pythagorean scale, E flat"); 
group_music_tuning_pyth   <- RegisterFunction("Tuning_Pythagorean_D", "Pythagorean scale, D sharp"); 

//----------------------------------------------------------------------
//
//   Meantone temperament 
//
//      "Meantone temperament is a system of musical tuning. 
//       In general, a meantone is constructed the same way as Pythagorean tuning, 
//       as a chain of perfect fifths, but in a meantone, each fifth is narrowed by the same amount 
//       (or equivalently, each fourth widened) in order to make the other intervals 
//       like the major third closer to their ideal just ratios." - Wikipedia
// 
//   References:
//      http://en.wikipedia.org/wiki/Quarter_comma_meantone
//      http://www.microtonal-synthesis.com/scales.html
//      http://www.microtonal-synthesis.com/scale_meantone_fourth.html
//      http://www.microtonal-synthesis.com/scale_meantone_fifth.html
//
//----------------------------------------------------------------------

// Quarter-Meantone Chromatic Scale by Pietro Aaron, 1523 
Tuning_Quarter_Meantone <- function(note) { 
T  <- sqrt(5)/2;
S  <- 8/pow(5,5/4);
P  <- T*T*T*S;        // pow(5, 1/4)
St <- T/S;            // pow(5, 7/4)/16
<- Scale(note, St, T, T*S, T*T, T*T*S, T*T*T, P, P*St, P*T, P*T*S, P*T*T, P*T*T*S);
};

// Meantone, 1/4 comma with equal beating fifths
Tuning_Meantone_4 <- function(note) { 
    <- ChromaticScale(note,  85.720,  194.940, 305.672, 390.837,  502.067, 587.593, 
                             696.578, 782.559, 892.091, 1003.122, 1088.503, 1200);
};

// Meantone, 1/5 comma
Tuning_Meantone_5 <- function(note) { 
    <- ChromaticScale(note,  83.576, 195.307, 307.039, 390.615, 502.346, 585.922,
                            697.654, 781.230, 892.961, 1004.693, 1088.269, 1200);
};

group_music_tuning        <- RegisterGroup("Meantone temperament") <- group_music_tuning_mean;
group_music_tuning_mean   <- RegisterFunction("Tuning_Quarter_Meantone", "Quarter-Meantone Chromatic Scale by Pietro Aaron, 1523");
group_music_tuning_mean   <- RegisterFunction("Tuning_Meantone_4", "Meantone, 1/4 comma with equal beating fifths");
group_music_tuning_mean   <- RegisterFunction("Tuning_Meantone_5", "Meantone, 1/5 comma");

//----------------------------------------------------------------------
//
//  World Music scales
//
//   References:
//      http://www.microtonal-synthesis.com/scales.html
//
//----------------------------------------------------------------------

// Observed balafon tuning from West Africa
Tuning_Africa <- function(note) { 
    //                       C#   D    D#   E    F    F#   G    G#   A    A#    B     C
    <- ChromaticScale(note,  0,   152, 152, 287, 533, 724, 724, 890, 890, 1039, 1039, 1200); 
};

// Bagpipe tuning
Tuning_Bagpipe  <- function(note) { 
<- Scale(note, 117/115, 146/131, 196/169, 89/73, 141/106, 81/59, 150/101, 152/82, 139/84, 205/116, 11/6, 2/1); 
};

// Chinese Lu scale by Huai-nan-dsi, Han era. Kurt Reinhard: Chinesische Musik
Tuning_China_Lu  <- function(note) { <- Scale(note, 
18/17, 9/8, 6/5, 54/43, 4/3, 27/19, 3/2, 27/17, 27/16, 9/5, 36/19, 2/1
); };

// Indian shruti scale
Tuning_India_Shruti <- function(note) { <- Scale(note, 
256/243, 16/15, 10/9, 9/8, 32/27, 6/5,  5/4, 81/64,4/3, 
27/20, 45/32, 729/512, 3/2, 128/81, 8/5, 5/3, 27/16, 16/9, 9/5, 15/8, 243/128, 2/1
); };

// Thailand (observed ranat tuning, Helmholtz (#85, p. 518)
Tuning_Thailand <- function(note) { 
    //                       C#  D    D#   E    F    F#   G    G#   A    A#    B     C
    <- ChromaticScale(note,  54, 129, 277, 277, 508, 726, 726, 771, 771, 1029, 1029, 1200); 
};


group_music_tuning        <- RegisterGroup("World Music scales") <- group_music_tuning_world;
group_music_tuning_world   <- RegisterFunction("Tuning_Africa", "Observed balafon tuning from West Africa");
group_music_tuning_world   <- RegisterFunction("Tuning_China_Lu", "Chinese Lu scale by Huai-nan-dsi, Han era. Kurt Reinhard: Chinesische Musik");
group_music_tuning_world   <- RegisterFunction("Tuning_India_Shruti", "Indian shruti scale");
group_music_tuning_world   <- RegisterFunction("Tuning_Thailand", "Thailand (observed ranat tuning, Helmholtz (#85, p. 518)");
