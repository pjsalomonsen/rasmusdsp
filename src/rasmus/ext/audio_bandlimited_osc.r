/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 /* Audio Bandlimited */

osc_saw <- function(f=440)
{
   srate2 <- srate() / 2;
   period <- f / srate2 ;
   <- (integ(0.99997) <- (buzz(f, 1, floor(srate2 / f) + 1 )  - 1 )  *  period) - 0.5;
};

osc_square <- function(f=440,w=0.5)
{
   srate2 <- srate() / 2;
   period <- f / srate2 ;
   blit <-  (buzz(f, 1, floor(srate2 / f) + 1 )  )  *  period;
   <- ((integ(0.99997) <- (blit -  delayline(input=blit, w/(f),0,0.5)  ) ) - w)  - 0.5;
};

osc_triangle <- function(f=440,w=0.5)
{
   srate2 <- srate() / 2;
   period <- f / srate2;
   blit <-  (buzz(f, 1, floor(srate2 / f) + 1 )  )  * period;
   <- gain(period/w) <- integ(0.997) <-  ((integ() <- (blit -  delayline(blit, w/(f),0,5)  ) ) - w)  - ( w ) ;
};

group_audio_synth_signal  <- RegisterFunction("osc_saw", "Bandlimited Saw Wave");
group_audio_synth_signal  <- RegisterFunction("osc_square", "Bandlimited Square Wave");
group_audio_synth_signal  <- RegisterFunction("osc_triangle", "Bandlimited Triangle Wave");

osc_saw <- UIFigure(icon="icons/audio/osc_saw.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 
osc_square <- UIFigure(icon="icons/audio/osc_square.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 
osc_triangle <- UIFigure(icon="icons/audio/osc_triangle.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1);                                                    