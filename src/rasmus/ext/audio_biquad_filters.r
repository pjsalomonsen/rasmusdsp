/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 // BiQuad Filters, tweaked butterworth
// References : Posted by Patrice Tarrabia from musicdsp

// f = cutoff frequency / samplerate frequency 

bw_lowpass <- function(f, r=sqrt(2), input)
{
      vc <- 1 / tan(pi * f);
      vb0 <- 1 / ( 1 + r * vc + vc * vc);
      vb1 <- 2*vb0;
      vb2 <- vb0;
      va0 <- 1;
      va1 <- 2 * ( 1 - vc*vc ) * vb0;
      va2 <- ( 1 - r*vc + vc*vc) * vb0;
      output <- biquad(vb0, vb1, vb2, va0, va1, va2) <- input;
};

bw_hipass <- function(f, r=sqrt(2), input)
{
      vc <- tan(pi * f);
      vb0 <- 1 / ( 1 + r * vc + vc * vc);
      vb1 <- -2*vb0;
      vb2 <- vb0;
      va0 <- 1;
      va1 <- 2 * ( vc*vc - 1 ) * vb0;
      va2 <- ( 1 - r*vc + vc*vc) * vb0;
      output <- biquad(vb0, vb1, vb2, va0, va1, va2) <- input;
};

group_audio_effect_filter     <- RegisterFunction("bw_lowpass", "Tweaked Butterworth Lowpass Filter");
group_audio_effect_filter     <- RegisterFunction("bw_hipass", "Tweaked Butterworth Hipass Filter");



/* Based on http://www.smartelectronix.com/musicdsp/text/filters005.txt
 * Enjoy.
 *
 * This work is hereby placed in the public domain for all purposes, whether
 * commercial, free [as in speech] or educational, etc.  Use the code and please
 * give me credit if you wish.
 *
 * Tom St Denis -- http://tomstdenis.home.dhs.org  
*/

// f         = cutoff frequency    / samplerate frequency 
// bandwidth = octaves
// A         = amplitude

sinh <- function(x) { <- (exp(x) - exp(-x))*0.5; };
m_ln2 <- log(2);

biquad_LPF <- function(f, bandwidth, input)
{
    omega <- 2 * pi * f;
    cs <- cos(omega);
    sn <- sin(omega);    
    alpha <- sn * sinh((M_LN2 * bandwidth * omega) / (sn * 2));

    b0 <- (1 - cs) /2;
    b1 <- 1 - cs;
    b2 <- (1 - cs) /2;
    a0 <- 1 + alpha;
    a1 <- -2 * cs;
    a2 <- 1 - alpha;

    output <- biquad(b0, b1, b2, a0, a1, a2) <- input;     
};

biquad_HPF <- function(f, bandwidth, input)
{
    omega <- 2 * pi * f;
    cs <- cos(omega);
    sn <- sin(omega);        
    alpha <- sn * sinh((M_LN2 * bandwidth * omega) / (sn * 2));

    b0 <- (1 + cs) /2;
    b1 <- -(1 + cs);
    b2 <- (1 + cs) /2;
    a0 <- 1 + alpha;
    a1 <- -2 * cs;
    a2 <- 1 - alpha;

    output <- biquad(b0, b1, b2, a0, a1, a2) <- input;      
};


biquad_BPF <- function(f, bandwidth, input)
{

    omega <- 2 * pi * f;
    cs <- cos(omega);
    sn <- sin(omega);        
    alpha <- sn * sinh((M_LN2 * bandwidth * omega) / (sn * 2));

    b0 <- alpha;
    b1 <- 0;
    b2 <- -alpha;
    a0 <- 1 + alpha;
    a1 <- -2 * cs;
    a2 <- 1 - alpha;

    output <- biquad(b0, b1, b2, a0, a1, a2) <- input;       
};


biquad_NOTCH <- function(f, bandwidth, input)
{
    omega <- 2 * pi * f;
    cs <- cos(omega);
    sn <- sin(omega);    
    alpha <- sn * sinh((M_LN2 * bandwidth * omega) / (sn * 2));

    b0 <- 1;
    b1 <- -2 * cs;
    b2 <- 1;
    a0 <- 1 + alpha;
    a1 <- -2 * cs;
    a2 <- 1 - alpha;

    output <- biquad(b0, b1, b2, a0, a1, a2) <- input;      
};

biquad_PEQ <- function(f, bandwidth, A, input)
{
    omega <- 2 * pi * f;
    cs <- cos(omega);
    sn <- sin(omega);    
    alpha <- sn * sinh((M_LN2 * bandwidth * omega) / (sn * 2));

    b0 <- 1 + (alpha * A);
    b1 <- -2 * cs;
    b2 <- 1 - (alpha * A);
    a0 <- 1 + (alpha /A);
    a1 <- -2 * cs;
    a2 <- 1 - (alpha /A);

    output <- biquad(b0, b1, b2, a0, a1, a2) <- input;      
};

biquad_LSH <- function(f, A, input)
{
    omega <- 2 * pi * f;
    cs <- cos(omega);
    sn <- sin(omega);    
    beta <- sqrt(A + A);

    b0 <- A * ((A + 1) - (A - 1) * cs + beta * sn);
    b1 <- 2 * A * ((A - 1) - (A + 1) * cs);
    b2 <- A * ((A + 1) - (A - 1) * cs - beta * sn);
    a0 <- (A + 1) + (A - 1) * cs + beta * sn;
    a1 <- -2 * ((A - 1) + (A + 1) * cs);
    a2 <- (A + 1) + (A - 1) * cs - beta * sn;

    output <- biquad(b0, b1, b2, a0, a1, a2) <- input;      
};

biquad_HSH <- function(f, A, input)
{
    omega <- 2 * pi * f;
    cs <- cos(omega);
    sn <- sin(omega);    
    beta <- sqrt(A + A);

    b0 <- A * ((A + 1) + (A - 1) * cs + beta * sn);
    b1 <- -2 * A * ((A - 1) + (A + 1) * cs);
    b2 <- A * ((A + 1) + (A - 1) * cs - beta * sn);
    a0 <- (A + 1) - (A - 1) * cs + beta * sn;
    a1 <- 2 * ((A - 1) - (A + 1) * cs);
    a2 <- (A + 1) - (A - 1) * cs - beta * sn;

    output <- biquad(b0, b1, b2, a0, a1, a2) <- input;      
};

group_audio_effect_filter     <- RegisterFunction("biquad_LPF", "Biquad Lowpass Filter");
group_audio_effect_filter     <- RegisterFunction("biquad_HPF", "Biquad Hipass Filter");
group_audio_effect_filter     <- RegisterFunction("biquad_BPF", "Biquad Band Filter");
group_audio_effect_filter     <- RegisterFunction("biquad_NOTCH", "Biquad Notch Filter");
group_audio_effect_filter     <- RegisterFunction("biquad_PEQ", "Biquad Peq Filter");
group_audio_effect_filter     <- RegisterFunction("biquad_LSH", "Biquad Low Shelf Filter");
group_audio_effect_filter     <- RegisterFunction("biquad_HSH", "Biquad High Shelf Filter");

HiPassFilterFigure <- UIFigure(icon="icons/audio/hipass.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1); 
LowPassFilterFigure <- UIFigure(icon="icons/audio/lowpass.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);        
BandPassFilterFigure <- UIFigure(icon="icons/audio/bandpass.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);         
FilterFigure <- UIFigure(icon="icons/audio/filter.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);         
                        
biquad_LPF <- LowPassFilterFigure;
biquad_HPF <- HiPassFilterFigure;
biquad_BPF <- BandPassFilterFigure;
biquad_NOTCH <- FilterFigure;
biquad_PEQ <- FilterFigure;
biquad_LSH <- FilterFigure;
biquad_HSH <- FilterFigure;

bw_lowpass <- LowPassFilterFigure;
bw_hipass <- HiPassFilterFigure;
                         