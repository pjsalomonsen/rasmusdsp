/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 
struct <- ExternalJava("rasmus.interpreter.struct.StructUnit");

use <- ExternalJava("rasmus.interpreter.ext.UseModule");
eval <- ExternalJava("rasmus.interpreter.ext.EvalExpression");
source <- ExternalJava("rasmus.interpreter.ext.EvalSource");

 /*********************************************************************
 *
 * UI Units
 *
 *********************************************************************/

RegisterGroup    <- ExternalJava("rasmus.interpreter.ui.RegisterGroup");
RegisterConstant <- ExternalJava("rasmus.interpreter.ui.RegisterConstant");
RegisterFunction <- ExternalJava("rasmus.interpreter.ui.RegisterFunction");

// Add Support for .RLM files
UIFigure <- ExternalJava("rasmus.interpreter.ui.UIFigure");


groups                        <- RegisterGroup("File I/O") <- group_file_io;
group_file_io                 <- RegisterFunction("File", "Link to external File");
group_file_io                 <- RegisterFunction("TextFile", "Link to external Text File");
group_file_io                 <- RegisterConstant("wrkdir", "Working Directory");
group_file_io                 <- RegisterConstant("sysdir", sysdir);


delay <- UIFigure(icon="icons/delay.png",
                        backcolor="220,220,220",
                        textcolor="0,0,0",
                        bordercolor="64,64,64",
                        functionbold=1);  
file <- UIFigure(icon="icons/file.png",
                        backcolor="220,220,220",
                        textcolor="0,0,0",
                        bordercolor="64,64,64",
                        functionbold=1);  
stretch <- UIFigure(icon="icons/stretch.png",
                        backcolor="220,220,220",
                        textcolor="0,0,0",
                        bordercolor="64,64,64",
                        functionbold=1);  
			
/*********************************************************************
 *
 * Visual Control Units
 *
 *********************************************************************/

panel      <- ExternalJava("rasmus.interpreter.controls.ControlPanel");
label      <- ExternalJava("rasmus.interpreter.controls.ControlLabel");

groups                        <- RegisterGroup("Visual Controls") <- group_controls;

group_controls                <- RegisterGroup("Basic Controls") <- group_controls_basic;

group_controls_basic          <- RegisterFunction("panel", "Panel");
group_controls_basic          <- RegisterFunction("label", "Label");

/*********************************************************************
 *
 * Visual Audio Control Units
 *
 *********************************************************************/

oscilloscope        <- ExternalJava("rasmus.interpreter.controls.sampled.AudioOscilloscope");
oscilloscopeXY      <- ExternalJava("rasmus.interpreter.controls.sampled.AudioOscilloscopeXY");
spectrumGraph       <- ExternalJava("rasmus.interpreter.controls.sampled.AudioSpectrumGraph");
cqspectrumGraph     <- ExternalJava("rasmus.interpreter.controls.sampled.AudioCQSpectrumGraph");
cepstrumGraph       <- ExternalJava("rasmus.interpreter.controls.sampled.AudioCepstrumGraph");

group_controls                 <- RegisterGroup("Audio Controls") <- group_controls_audio;

group_controls_audio           <- RegisterFunction("oscilloscope", "Oscilloscope");
group_controls_audio           <- RegisterFunction("oscilloscopeXY", "OscilloscopeXY");
group_controls_audio           <- RegisterFunction("spectrumgraph", "FFT spectrum Graph");
group_controls_audio           <- RegisterFunction("cepstrumgraph", "FFT cepstrum Graph");
group_controls_audio           <- RegisterFunction("cqspectrumgraph", "Constant Q FFT spectrum Graph");

// SpectrumGraph(time, rate, channels)       // FFT spectrum, linear frequency
// CQSpectrumGraph(time, rate, channels)     // FFT spectrum, logarithmic frequency, constant Q

// StereoImageGraph
// PhaseImageGraph
// EnergyGraph

/*********************************************************************
 *
 * Visual Midi Control Units
 *
 *********************************************************************/
 
midikeyboard        <- ExternalJava("rasmus.interpreter.controls.midi.MidiKeyboard");
midibutton          <- ExternalJava("rasmus.interpreter.controls.midi.MidiButton");
midicontrolslider   <- ExternalJava("rasmus.interpreter.controls.midi.MidiControlSlider");

group_controls                 <- RegisterGroup("MIDI Controls") <- group_controls_midi;

group_controls_midi            <- RegisterFunction("midikeyboard", "Midi Keyboard");
group_controls_midi            <- RegisterFunction("midibutton", "Midi Button");
group_controls_midi            <- RegisterFunction("midicontrolslider", "Midi Control Slider");
// <- midibutton(caption, midioutput, midiinput) <- midicommands;
// <- midislider(orientation, control, default, min, max, midioutput, midiinput);
// <- midiplaycontroller(midioutput);


/*********************************************************************
 *
 * Math Units
 *
 *********************************************************************/


use(ExternalJava("rasmus.interpreter.math.MathModule"));

groups                        <- RegisterGroup("Math") <- group_math;

group_math                    <- RegisterGroup("Trigonometry Functions") <- group_math_trig;
group_math_trig               <- RegisterFunction("sin", "Sinus");
group_math_trig               <- RegisterFunction("cos", "Cosinus");
group_math_trig               <- RegisterFunction("tan", "Tangent");
group_math_trig               <- RegisterFunction("tanh", "Hyperbolic Tangent");
group_math_trig               <- RegisterFunction("asin", "Inverse Sinus");
group_math_trig               <- RegisterFunction("acos", "Inverse Cosinus");
group_math_trig               <- RegisterFunction("atan", "Inverse Tangent");
group_math_trig               <- RegisterFunction("atan2", "Convert rectangular coordinates (x, y) to polar (r, theta)");

group_math                    <- RegisterGroup("Exponential and Logarithmic functions") <- group_math_exp;
group_math_exp                <- RegisterFunction("exp", "Euler's number e raised to the power");
group_math_exp                <- RegisterFunction("log", "Natural logarithm");
group_math_exp                <- RegisterFunction("pow", "x raised to the power");

group_math                    <- RegisterGroup("Rounding") <- group_math_round;
group_math_round              <- RegisterFunction("round", "Round to closest integer");
group_math_round              <- RegisterFunction("floor", "Largest integer that is not greater than input");
group_math_round              <- RegisterFunction("ceil", "Smallast integer that is not smaller than input");

group_math                    <- RegisterGroup("Numerics") <- group_math_num;
group_math_num                <- RegisterFunction("max", "max of arguments");
group_math_num                <- RegisterFunction("min", "min of arguments");
group_math_num                <- RegisterFunction("abs", "absolute value");
group_math_num                <- RegisterFunction("sgn", "sign function");
group_math_num                <- RegisterFunction("random", "random value");

group_math                    <- RegisterGroup("Constants") <- group_math_constants;
group_math_constants          <- RegisterConstant("pi", "PI = " & pi);
group_math_constants          <- RegisterConstant("e",  "Euler's number e = " & e);


/*********************************************************************
 *
 * Strings Units
 *
 *********************************************************************/

use(ExternalJava("rasmus.interpreter.strings.StringsModule"));

 
/*********************************************************************
 *
 * MIDI Units
 *
 *********************************************************************/

use(ExternalJava("rasmus.interpreter.midi.MidiModule"));
MidiFigure <- UIFigure( backcolor="240,220,220",
                        textcolor="0,0,0",
                        bordercolor="128,0,0",
                        functionbold=1);  
Channel <- UIFigure(icon="icons/midi/channel.png",
                        backcolor="240,220,220",
                        textcolor="0,0,0",
                        bordercolor="128,0,0",
                        functionbold=1);  
FilterMessages <- UIFigure(icon="icons/midi/filter.png",
                        backcolor="240,220,220",
                        textcolor="0,0,0",
                        bordercolor="128,0,0",
                        functionbold=1);  
FilterMetaMessages <- UIFigure(icon="icons/midi/filter.png",
                        backcolor="240,220,220",
                        textcolor="0,0,0",
                        bordercolor="128,0,0",
                        functionbold=1);  
ModifyMessages <- MidiFigure;
Transpose <- UIFigure(icon="icons/midi/transpose.png",
                        backcolor="240,220,220",
                        textcolor="0,0,0",
                        bordercolor="128,0,0",
                        functionbold=1);  
Transpose2 <- UIFigure(icon="icons/midi/transpose.png",
                        backcolor="240,220,220",
                        textcolor="0,0,0",
                        bordercolor="128,0,0",
                        functionbold=1);  
ChordTranspose <- UIFigure(icon="icons/midi/chordtranspose.png",
                        backcolor="240,220,220",
                        textcolor="0,0,0",
                        bordercolor="128,0,0",
                        functionbold=1);  
MidiInput <- UIFigure(icon="icons/midi/midiinput.png",
                        backcolor="220,220,240",
                        textcolor="0,0,0",
                        bordercolor="0,0,128",
                        functionbold=1);  
MidiOutput <- UIFigure(icon="icons/midi/midioutput.png",
                        backcolor="220,220,240",
                        textcolor="0,0,0",
                        bordercolor="0,0,128",
                        functionbold=1); 
Note <- UIFigure(icon="icons/midi/note.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 
Tempo <- UIFigure(icon="icons/midi/message.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 
ShortMessage <- UIFigure(icon="icons/midi/message.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 
LongMessage <- UIFigure(icon="icons/midi/message.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 

groups                        <- RegisterGroup("MIDI Effects") <- group_midi_effect;

group_midi_effect             <- RegisterGroup("Delay Effects") <- group_midi_effect_delay;
group_midi_effect_delay       <- RegisterFunction("Delay", "Delay by beats"); 

group_midi_effect             <- RegisterGroup("Filters")    <- group_midi_effect_filter;
group_midi_effect_filter      <- RegisterFunction("MonoNotes", "Monophony filtering");
group_midi_effect_filter      <- RegisterFunction("FilterMessages", "Short Messages Filter");
group_midi_effect_filter      <- RegisterFunction("FilterMetaMessages", "Filter for MetaMessages");
group_midi_effect_filter      <- RegisterFunction("FilterNotes", "Filter for Note Messages");

group_midi_effect             <- RegisterGroup("Modifiers")  <- group_midi_effect_mod;
group_midi_effect_mod         <- RegisterFunction("Channel", "Assign messages to channel");
group_midi_effect_mod         <- RegisterFunction("ModifyMessages", "Short Messages Modifier");

group_midi_effect             <- RegisterGroup("Notes Mapping")  <- group_midi_effect_notes;
group_midi_effect_notes       <- RegisterFunction("ChordTranspose", "Chord Transposer");
group_midi_effect_notes       <- RegisterFunction("ChordTranspose2", "Chord Transposer (skip channel 10)");
group_midi_effect_notes       <- RegisterFunction("Transpose", "Note Tranposer");
group_midi_effect_notes       <- RegisterFunction("Transpose2", "Note Tranposer (skip channel 10)");
group_midi_effect_notes       <- RegisterFunction("RemapNotes", "Remap Notes");

group_midi_effect             <- RegisterGroup("Time/Stretch")  <- group_midi_effect_time;
group_midi_effect_time        <- RegisterFunction("Stretch", "Time Stretcher");
group_midi_effect_time        <- RegisterFunction("Extract", "Extract by Clipping");

groups                        <- RegisterGroup("MIDI Synthesis") <- group_midi_generator;
group_midi_generator          <- RegisterFunction("LongMessage", "Long Message");
group_midi_generator          <- RegisterFunction("ShortMessage", "Short Message");
group_midi_generator          <- RegisterFunction("Tempo", "Tempo Message");
group_midi_generator          <- RegisterFunction("Note", "Note Message");
group_midi_generator          <- RegisterFunction("ABC", "ABC Notation");

groups                    <- RegisterGroup("MIDI I/O") <- group_midi_io;
// group_midi_io             <- RegisterFunction("File", "MIDI File");
group_midi_io             <- RegisterFunction("MidiPlayer", "MIDI Player");
group_midi_io             <- RegisterFunction("MidiFile", "MIDI File");
group_midi_io             <- RegisterFunction("MidiOutput", "MIDI Output");
group_midi_io             <- RegisterFunction("MidiInput", "MIDI Input");
group_midi_io             <- RegisterFunction("Switch", "MIDI Controlled circuit Switch");

// groups                    <- RegisterGroup("MIDI Devices") <- group_midi_dev;


/*********************************************************************
 *
 * Audio Units
 *
 *********************************************************************/

use(ExternalJava("rasmus.interpreter.sampled.AudioModule"));
AudioFigure <- UIFigure(backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);                      
                        
Agc <- UIFigure(icon="icons/audio/agc.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);                       
Gain <- UIFigure(icon="icons/audio/gain.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);  
DelayLine <- UIFigure(icon="icons/audio/delayline.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);  
Delay1 <- UIFigure(icon="icons/audio/delay1.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);                          
Reverb <- UIFigure(icon="icons/audio/reverb.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);  
DeReverb <- UIFigure(icon="icons/audio/dereverb.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);                      
Vocoder <- UIFigure(icon="icons/audio/vocoder.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);                         
AudioInput <- UIFigure(icon="icons/audio/audioinput.png",
                        backcolor="220,220,240",
                        textcolor="0,0,0",
                        bordercolor="0,0,128",
                        functionbold=1);  
AudioOutput <- UIFigure(icon="icons/audio/audiooutput.png",
                        backcolor="220,220,240",
                        textcolor="0,0,0",
                        bordercolor="0,0,128",
                        functionbold=1);  
RenderControl <- UIFigure(icon="icons/audio/rendercontrol.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1);  
Clock <- UIFigure(icon="icons/audio/clock.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1);     
Counter <- UIFigure(icon="icons/audio/counter.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1);   
buzz <- UIFigure(icon="icons/audio/dsf.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1);                                                                       
ChannelMux <- AudioFigure;
InstrumentFigure <- UIFigure(icon="icons/audio/rendernotes.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1);         
Synthesizer <- InstrumentFigure;
RenderNotes <- InstrumentFigure;

diff <- UIFigure(icon="icons/audio/diff.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);   
integ <- UIFigure(icon="icons/audio/integ.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);                           

ReSampleFigure <- UIFigure(icon="icons/audio/resample.png",
                        backcolor="220,240,220",
                        textcolor="0,0,0",
                        bordercolor="0,128,0",
                        functionbold=1);   
                        
                        

Resample <- ReSampleFigure;
ResampleI <- ReSampleFigure;
ResampleC <- ReSampleFigure;
ResampleH <- ReSampleFigure;

groups                        <- RegisterGroup("Audio Effects") <- group_audio_effect;

group_audio_effect            <- RegisterGroup("Amplitude Effects")    <- group_audio_effect_amp;
group_audio_effect_amp        <- RegisterFunction("AGC", "Auto Gain Control");
group_audio_effect_amp        <- RegisterFunction("ChannelGain", "Multiple Channel Gain");
group_audio_effect_amp        <- RegisterFunction("ChannelMux", "Multiple Channel Mux");
group_audio_effect_amp        <- RegisterFunction("Gain", "Gain Control");
group_audio_effect_amp        <- RegisterFunction("DownMix", "Downmix multiple channel");

group_audio_effect            <- RegisterGroup("Delay Effects")    <- group_audio_effect_delay;
group_audio_effect_delay      <- RegisterFunction("Delay", "Delay by beats");
group_audio_effect_delay      <- RegisterFunction("Delay1", "Delay by one sample");
group_audio_effect_delay      <- RegisterFunction("DelayLine", "Variable Delay Line");
group_audio_effect_delay      <- RegisterFunction("Reverb", "Reverberation Effect");
group_audio_effect_delay      <- RegisterFunction("FFTConvolution", "FFT Convolution");

group_audio_effect        <- RegisterGroup("Envelopes Followers") <- group_audio_effect_env;
group_audio_effect_env     <- RegisterFunction("follow",  "Envelopes Follower");
group_audio_effect_env     <- RegisterFunction("follow2",  "Controllable Envelopes Follower");

group_audio_effect            <- RegisterGroup("Noise Reduction")    <- group_audio_effect_noisered;
group_audio_effect_noisered   <- RegisterFunction("DeReverb", "De-Reverberation");
group_audio_effect_noisered   <- RegisterFunction("DeHiss", "Hiss Reduction");

group_audio_effect            <- RegisterGroup("Filters")    <- group_audio_effect_filter;
group_audio_effect_filter     <- RegisterFunction("Vocoder", "FFT Based Channel Vocoder");
group_audio_effect_filter     <- RegisterFunction("Biquad", "Biquad IIR Filter");
group_audio_effect_filter     <- RegisterFunction("Diff", "Signal Differentation");
group_audio_effect_filter     <- RegisterFunction("Integ", "Signal Intergration");

group_audio_effect            <- RegisterGroup("Time/Pitch")  <- group_audio_effect_time;
group_audio_effect_time       <- RegisterFunction("PitchShift", "Pitch and Formant Shifter");
group_audio_effect_time       <- RegisterFunction("Stretch", "Time Stretcher");
group_audio_effect_time       <- RegisterFunction("TimeStretch", "Time Stretcher");

group_audio_effect            <- RegisterGroup("Resampling")  <- group_audio_effect_resampling;
group_audio_effect_resampling <- RegisterFunction("Resample",  "Resample with no interpolation");
group_audio_effect_resampling <- RegisterFunction("ResampleI", "Resample using linear interpolation");
group_audio_effect_resampling <- RegisterFunction("ResampleC", "Resample using cubic interpolation");
group_audio_effect_resampling <- RegisterFunction("ResampleH", "Resample using hermite interpolation");

groups                    <- RegisterGroup("Audio Synthesis") <- group_audio_synth;

group_audio_synth         <- RegisterGroup("Envelopes Generators") <- group_audio_synth_env;
group_audio_synth_env     <- RegisterFunction("Release",  "Linear Release Envelope");
group_audio_synth_env     <- RegisterFunction("XRelease",  "Exponential Release Envelope");
group_audio_synth_env     <- RegisterFunction("Adsr",  "Linear ADSR Envelope");
group_audio_synth_env     <- RegisterFunction("Ahdsr", "AHDSR Envelope");
group_audio_synth_env     <- RegisterFunction("XAdhs", "Exponential ADSR Envelope");
group_audio_synth_env     <- RegisterFunction("Linseg", "Linear Segments");
group_audio_synth_env     <- RegisterFunction("Expseg", "Exponential Segments");
group_audio_synth_env     <- RegisterFunction("One", "Constant signal with the value 1");
group_audio_synth_env     <- RegisterFunction("Counter", "Sample based count generator");
group_audio_synth_env     <- RegisterFunction("Clock", "Variable clock signal generator");
group_audio_synth_env     <- RegisterFunction("srate", "Current sample rate");

group_audio_synth         <- RegisterGroup("Noise Generators") <- group_audio_synth_noise;
group_audio_synth_noise   <- RegisterFunction("Noise", "Random values between 0 and 1");

group_audio_synth         <- RegisterGroup("Bandlimited Generators") <- group_audio_synth_signal;
group_audio_synth_signal  <- RegisterFunction("buzz", "Buzz Generator");

group_audio_synth         <- RegisterGroup("Midi Rendering") <- group_audio_synth_midi;
group_audio_synth_midi    <- RegisterFunction("Synthesizer", "MIDI Synthesizer");
group_audio_synth_midi    <- RegisterFunction("RenderNotes", "Render Notes");
group_audio_synth_midi    <- RegisterFunction("RenderControl", "Render Control");
group_audio_synth_midi    <- RegisterFunction("RenderPitchBend", "Render PitchBend");
group_audio_synth_midi    <- RegisterFunction("RenderPitch", "Render Pitch");
group_audio_synth_midi    <- RegisterFunction("RenderGain", "Render Gain");
group_audio_synth_midi    <- RegisterFunction("RegisterVoice", "Register Voice");
group_audio_synth_midi    <- RegisterFunction("AudioVoiceFactory", "AudioVoiceFactory");

groups                    <- RegisterGroup("Audio I/O") <- group_audio_io;
// group_audio_io            <- RegisterFunction("File", "Audio File");
group_audio_io            <- RegisterFunction("AudioPlayer", "Audio Player");
group_audio_io            <- RegisterFunction("AudioFile", "Audio File");
group_audio_io            <- RegisterFunction("AudioOutput", "Audio Output");
group_audio_io            <- RegisterFunction("AudioInput", "Audio Input");

// groups                    <- RegisterGroup("Audio Devices") <- group_audio_dev;

/*********************************************************************
 *
 * SoundFont 2 Units
 *
 *********************************************************************/

SF2Sample           <- ExternalJava("rasmus.interpreter.sf2.SF2Sample");
SF2Preset           <- ExternalJava("rasmus.interpreter.sf2.SF2Preset");
SoundBankPreset     <- ExternalJava("rasmus.interpreter.sf2.SF2Preset");
SoundBank           <- ExternalJava("rasmus.interpreter.sf2.SF2Soundbank");

groups        <- RegisterGroup("Soundbanks") <- group_sf2;
//group_sf2     <- RegisterFunction("SF2Sample", "SoundFont 2 Sample");
//group_sf2     <- RegisterFunction("SF2Preset", "SoundFont 2 Preset");

group_sf2     <- RegisterFunction("SoundBank", "Soundbank");
group_sf2     <- RegisterFunction("SoundBankPreset", "Soundbank Preset");


/*********************************************************************
 *
 * Audio/Midi Device Names
 *
 *********************************************************************/

//dev_midi_in         <- "1:EDIROL PCR 1";
//dev_midi_out        <- "Java Sound Synthesizer";
//dev_audio_out       <- "Primary Sound Driver";
//dev_audio_out_front <- "kX Wave SB0360 10k2 [9400] 4/5";
//dev_audio_out_back  <- "kX Wave SB0360 10k2 [9400] 6/7";
                
// Examples (in code):                
//midiinput1 <- MidiInput(dev_midi_in);
//MidiOutput(output, dev_midi_out);
//AudioOutput(output, dev_audio_out);
//audiooutput1 <- output;
//AudioOutput(audiooutput1, dev_audio_out_front);
//AudioOutput(audiooutput2, dev_audio_out_back);
