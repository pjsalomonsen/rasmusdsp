/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 // Noises

white_noise <- function()
{
  <- noise()*2 - 1;
};

brown_noise <- function()
{
  <- gain(0.01) <- integ(0.99) <- white_noise();
};

/* Paul Kellet's economy method, http://www.firstpr.com.au/dsp/pink-noise/ */
/* An 'economy' version with accuracy of +/-0.5dB. */
pink_noise <- function()
{
  white <- white_noise();
  b0 <- integ(0.99765) <- white * 0.0990460;
  b1 <- integ(0.96300) <- white * 0.2965164;
  b2 <- integ(0.57000) <- white * 1.0526913;
  <- gain(0.05) <-b0 + b1 + b2 + white * 0.1848;
  // gain(0.05) <- 
};

/* 
'Instrumentation grade version'
This is an approximation to a -10dB/decade filter using a weighted sum
of first order filters. It is accurate to within +/-0.05dB above 9.2Hz
(44100Hz sampling rate). Unity gain is at Nyquist, but can be adjusted
by scaling the numbers at the end of each line.
*/
pink_noise2 <- function()
{
   // x <- integ(y) <- z   <=>  x = (x * y) + z
   white <- white_noise();
   b0 <- integ(0.99886) <- white * 0.0555179;
   b1 <- integ(0.99332) <- white * 0.0750759;
   b2 <- integ(0.96900) <- white * 0.1538520;
   b3 <- integ(0.86650) <- white * 0.3104856;
   b4 <- integ(0.55000) <- white * 0.5329522;
   b5 <- integ(-0.7616) <- white * (-0.0168980);
   b6 <- delay1() <- white * 0.115926;
   <- gain(0.05) <- b0 + b1 + b2 + b3 + b4 + b5 + b6 + white * 0.5362;

};

purple_noise <- function()
{
  <- diff() <- noise();
};

group_audio_synth_noise   <- RegisterFunction("white_noise", "White Noise");
group_audio_synth_noise   <- RegisterFunction("brown_noise", "Brown Noise");
group_audio_synth_noise   <- RegisterFunction("pink_noise", "Pink Noise");
group_audio_synth_noise   <- RegisterFunction("purple_noise", "Purple Noise");

NoiseFigure <- UIFigure(icon="icons/audio/noise.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 
                        
white_noise  <- NoiseFigure;                        
brown_noise  <- UIFigure(icon="icons/audio/brown_noise.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 
pink_noise   <- UIFigure(icon="icons/audio/pink_noise.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 
purple_noise <- UIFigure(icon="icons/audio/purple_noise.png",
                        backcolor="240,240,220",
                        textcolor="0,0,0",
                        bordercolor="128,128,0",
                        functionbold=1); 
