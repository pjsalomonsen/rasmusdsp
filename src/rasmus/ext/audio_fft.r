/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 //Audio FFT

Complex2Phase <- function(phaseout, input)
{
   // realpart <- ExtractChannel(1, 2) <- input;
   // impart   <- ExtractChannel(2, 2) <- input;
   // Alterntaive to ExtractChannel is : downmix(2) <- channelgain(1,0) 
   realpart <- downmix(2) <- channelgain(1,0) <- input;
   imgpart  <- downmix(2) <- channelgain(0,1) <- input;

   <- 2*sqrt((realpart*realpart) + (imgpart*imgpart));
   phaseout <- atan2(imgpart, realpart);
};


Phase2Complex <- function(ampin, phasein)
{
   <- ChannelMux(amp*cos(phasein), amp*sin(phasein));
};


// FFT Windowing

RectangularWindow <- function(size)
{
   <- 1;
};

HanningWindow <- function(size)
{
   k <- counter();
   <- 0.5 - 0.5*cos(k*(2*pi/size));
};

HammingWindow <- function(size)
{
   k <- counter();
   <- 0.54 - 0.46*cos(k*(2*pi/size));
};

BlackmanWindow <- function(size)
{
   k <- counter();
   <- 0.42 -0.5  * cos(k*(2*pi/size))
           +0.08 * cos(k*(4*pi/size))
};

// Gaussian Distb.

GaussianDist <- function(m, s)
{
   x <- counter();
   <- (1/(s*sqrt(2*pi)) * exp( (-1/2) * pow( (x - m)/s ,2) ));
};

