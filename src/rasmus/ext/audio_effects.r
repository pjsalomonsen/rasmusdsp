/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 // Experimental Filters

//  Flanger(0.0015, 1, pi, 0, dry=1)
//  There is a need for default values for the parameters 

flanger <- function(depth=0.0015, rate=1, phase=pi, feedback=0, dry=1, input)
{
  hdepth <- depth * 0.5;
  <- channelgain(1,0) <- delayline(0.0001 + hdepth + hdepth*sin(clock(rate*2*pi)+phase),feedback,1) <- input;
  <- channelgain(0,1) <- delayline(0.0001 + hdepth + hdepth*sin(clock(rate*2*pi)),feedback, 1) <- input;
  <- input*dry;
};

autowah <- function(lowfreq=300, hifreq=15000, q=0.3, windowlen=0.1, input)
{
  env <- follow(windowlen) <- input;
  freq <- lowfreq + (env*(hifreq - lowfreq));
  <- biquad_LPF(freq/srate(),q) <- input;
} 

softdist <- function(pregain=100, input)
{
  <- gain(1/atan(pregain)) <- atan() <- gain(pregain) <- input;
}

harddist <- function(pregain=100, input)
{
  <- clip(-1,1) <- gain(pregain) <- input;
}

normdist <- function(pregain=100, input)
{
  <- gain(1/(1-exp(-pregain))) <- (1-exp(-abs(input*pregain)))*sgn(input);
}

group_audio_effect_env <- RegisterFunction("autowah", "Auto-wah lowpass filter");

group_audio_effect_amp <- RegisterFunction("normdist", "Normal Distortion");
group_audio_effect_amp <- RegisterFunction("softdist", "Soft Distortion");
group_audio_effect_amp <- RegisterFunction("harddist", "Hard Distortion");

flanger <- AudioFigure;

group_audio_effect_delay      <- RegisterFunction("Flanger", "Flanger Effect");