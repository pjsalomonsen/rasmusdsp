/* 
 * Copyright (c) 2006-2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.wavefloat;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.spi.AudioFileReader;

import rasmus.util.ByteConversion;
import rasmus.util.riff.RiffReader;

public class WaveFloatFileReader extends AudioFileReader {
		
	public AudioFileFormat getAudioFileFormat(InputStream stream) throws UnsupportedAudioFileException, IOException {

		stream.mark(200);
		AudioFileFormat format;
		try
		{
			format = internal_getAudioFileFormat(stream);
		}
		finally
		{
			stream.reset();			
		}
		return format;
	}

	private AudioFileFormat internal_getAudioFileFormat(InputStream stream) throws UnsupportedAudioFileException, IOException {
								
		RiffReader riffiterator = new RiffReader(stream);
		if (!riffiterator.getFormat().equals("RIFF")) throw new UnsupportedAudioFileException();
		if (!riffiterator.getType().equals("WAVE")) throw new UnsupportedAudioFileException();
		
		boolean fmt_found = false;
		boolean data_found = false;
		
		int channels = 1;
		long samplerate = 1;
		long framerate = 1;
		int framesize = 1;
		int bits = 1;
		
		while (riffiterator.hasNextChunk()) {
			RiffReader chunk = riffiterator.nextChunk();

			if(chunk.getFormat().equals("fmt "))
			{
				fmt_found = true;
						
				byte[] sdata = new byte[2];
				byte[] ldata = new byte[4];

				chunk.read(sdata);						
				int format = ByteConversion.shortToInt(sdata);
				if(format != 3)  throw new UnsupportedAudioFileException(); // WAVE_FORMAT_IEEE_FLOAT only
				chunk.read(sdata);
				channels = ByteConversion.shortToInt(sdata);					
				chunk.read(ldata);
				samplerate = ByteConversion.dwordToLong(ldata);
				chunk.read(ldata);
				framerate = ByteConversion.dwordToLong(ldata);
				chunk.read(sdata);
				framesize = ByteConversion.shortToInt(sdata);
				chunk.read(sdata);
				bits = ByteConversion.shortToInt(sdata);						
												
						
			}
			if(chunk.getFormat().equals("data"))
			{
				data_found = true;
				break;
			}
		}
				
		if(!fmt_found) throw new UnsupportedAudioFileException();
		if(!data_found) throw new UnsupportedAudioFileException();
		
		// Microsoft Float is little endian!!!, while Java Floats are big endian
		AudioFormat audioformat = new AudioFormat(FloatEncoding.PCM_FLOAT, samplerate, bits, channels, framesize, framerate, false);		
		AudioFileFormat fileformat = new AudioFileFormat(AudioFileFormat.Type.WAVE, audioformat , AudioSystem.NOT_SPECIFIED);				
		return fileformat;					
	}
	public AudioInputStream getAudioInputStream(InputStream stream) throws UnsupportedAudioFileException, IOException {
		
		AudioFileFormat format = getAudioFileFormat(stream);		
		RiffReader riffiterator = new RiffReader(stream);
		if (!riffiterator.getFormat().equals("RIFF")) throw new UnsupportedAudioFileException();
		if (!riffiterator.getType().equals("WAVE")) throw new UnsupportedAudioFileException();				
		while (riffiterator.hasNextChunk()) {
			RiffReader chunk = riffiterator.nextChunk();
			if(chunk.getFormat().equals("data"))
			{
				return new AudioInputStream(chunk, format.getFormat(), chunk.getSize());
			}
		}
		throw new UnsupportedAudioFileException();					
	}

	public AudioFileFormat getAudioFileFormat(URL url) throws UnsupportedAudioFileException, IOException {
		InputStream stream = url.openStream();
		AudioFileFormat format;
		try
		{
			format = getAudioFileFormat(new BufferedInputStream(stream));
		}
		finally
		{
			stream.close();
		}
		return format;
	}

	public AudioFileFormat getAudioFileFormat(File file) throws UnsupportedAudioFileException, IOException {
		InputStream stream = new FileInputStream(file);
		AudioFileFormat format;
		try
		{
			format = getAudioFileFormat(new BufferedInputStream(stream));
		}
		finally
		{
			stream.close();
		}
		return format;	}

	public AudioInputStream getAudioInputStream(URL url) throws UnsupportedAudioFileException, IOException {
		return getAudioInputStream(new BufferedInputStream(url.openStream()));
	}

	public AudioInputStream getAudioInputStream(File file) throws UnsupportedAudioFileException, IOException {
		return getAudioInputStream(new BufferedInputStream(new FileInputStream(file)));
	}

}
