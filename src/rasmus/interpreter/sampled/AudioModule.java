/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Line;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.ext.Module;
import rasmus.interpreter.ext.ModuleFactory;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.sampled.generators.AudioAdsr;
import rasmus.interpreter.sampled.generators.AudioAhdsr;
import rasmus.interpreter.sampled.generators.AudioClock;
import rasmus.interpreter.sampled.generators.AudioCounter;
import rasmus.interpreter.sampled.generators.AudioDSF;
import rasmus.interpreter.sampled.generators.AudioExpSeg;
import rasmus.interpreter.sampled.generators.AudioLinSeg;
import rasmus.interpreter.sampled.generators.AudioOne;
import rasmus.interpreter.sampled.generators.AudioRandom;
import rasmus.interpreter.sampled.generators.AudioRelease;
import rasmus.interpreter.sampled.generators.AudioSampleRate;
import rasmus.interpreter.sampled.generators.AudioSignal;
import rasmus.interpreter.sampled.generators.AudioTable;
import rasmus.interpreter.sampled.generators.AudioXAdsr;
import rasmus.interpreter.sampled.generators.AudioXRelease;
import rasmus.interpreter.sampled.io.AudioFile;
import rasmus.interpreter.sampled.io.AudioInput;
import rasmus.interpreter.sampled.io.AudioMonitor;
import rasmus.interpreter.sampled.io.AudioOutput;
import rasmus.interpreter.sampled.math.AudioOperator;
import rasmus.interpreter.sampled.math.AudioOperator2;
import rasmus.interpreter.sampled.midi.AudioMidiTrap;
import rasmus.interpreter.sampled.midi.AudioRegisterInstrument;
import rasmus.interpreter.sampled.midi.AudioRegisterVoice;
import rasmus.interpreter.sampled.midi.AudioRenderControl;
import rasmus.interpreter.sampled.midi.AudioRenderGain;
import rasmus.interpreter.sampled.midi.AudioRenderNotes;
import rasmus.interpreter.sampled.midi.AudioRenderPitch;
import rasmus.interpreter.sampled.midi.AudioRenderPitchBend;
import rasmus.interpreter.sampled.midi.AudioSynthesizer;
import rasmus.interpreter.sampled.midi.AudioVoiceFactory;
import rasmus.interpreter.sampled.modifiers.AudioAGC;
import rasmus.interpreter.sampled.modifiers.AudioBiquad;
import rasmus.interpreter.sampled.modifiers.AudioChannelGain;
import rasmus.interpreter.sampled.modifiers.AudioChannelMixer;
import rasmus.interpreter.sampled.modifiers.AudioChannelMux;
import rasmus.interpreter.sampled.modifiers.AudioDeHiss;
import rasmus.interpreter.sampled.modifiers.AudioDeReverb;
import rasmus.interpreter.sampled.modifiers.AudioDelay;
import rasmus.interpreter.sampled.modifiers.AudioDelay1;
import rasmus.interpreter.sampled.modifiers.AudioDelayLine;
import rasmus.interpreter.sampled.modifiers.AudioDiff;
import rasmus.interpreter.sampled.modifiers.AudioDownMix;
import rasmus.interpreter.sampled.modifiers.AudioEnvelopeFollower;
import rasmus.interpreter.sampled.modifiers.AudioEnvelopeFollower2;
import rasmus.interpreter.sampled.modifiers.AudioFFT;
import rasmus.interpreter.sampled.modifiers.AudioFFTConvolution;
import rasmus.interpreter.sampled.modifiers.AudioFormatMapping;
import rasmus.interpreter.sampled.modifiers.AudioHarmonicsBooster;
import rasmus.interpreter.sampled.modifiers.AudioIFFT;
import rasmus.interpreter.sampled.modifiers.AudioInteg;
import rasmus.interpreter.sampled.modifiers.AudioPitchShift;
import rasmus.interpreter.sampled.modifiers.AudioResample;
import rasmus.interpreter.sampled.modifiers.AudioResampleC;
import rasmus.interpreter.sampled.modifiers.AudioResampleH;
import rasmus.interpreter.sampled.modifiers.AudioResampleI;
import rasmus.interpreter.sampled.modifiers.AudioReverb;
import rasmus.interpreter.sampled.modifiers.AudioTimeStretch;
import rasmus.interpreter.sampled.modifiers.AudioVocoder;
import rasmus.interpreter.unit.UnitInstancePart;

public class AudioModule extends ModuleFactory {
	
	class ModuleInstance extends Module
	{		
		public ModuleInstance(NameSpace namespace)
		{
			setNameSpace(namespace);
			
			// DSF (buzz)
			add("buzz", new AudioDSF());			
			
			
			// Standards Functions
			add("File", new AudioFile(false));	// Uses Linear Interpolation if samplerate doesn't match	
			add("PlayAudio", new AudioPlayer());			
			add("AudioInput", new AudioInput());			
			add("AudioOutput", new AudioOutput());			
			add("AudioMonitor", new AudioMonitor());			
			add("AudioFile", new AudioFile(false));
			add("AudioPlayer", new AudioPlayer());
			
			add("AudioMidiTrap", new AudioMidiTrap());
			
			//add("|", new AudioDelay());						
			add("Delay", new AudioDelay());			
			
			
			// Vera kannski me� PSOLA Stretch (time domain) l�ka ?			
			add("Stretch", new AudioTimeStretch()); // Non-RealTime Signal FFT Based function
			add("TimeStretch", new AudioTimeStretch()); // Non-RealTime Signal FFT Based function
			
//			* extract   <- function (input, from, to, smoothing); // from and beat are in beat
//			with default 5ms smoothing, vantar skip � kerfi� fyrst			
			
			// IO units
			add("Sample", new AudioFile(true));						
//			* SampleRate <- function(input);
			
//			output <- Resample(samplerate) <- input   // Performs resampling of input			
			
			// Midi 2 Audio units
			add("Synthesizer", new AudioSynthesizer());
			add("Instrument", new AudioRegisterInstrument());
			add("Voice", new AudioRegisterVoice());
			add("RegisterVoice", new AudioRegisterVoice());
			add("AudioVoiceFactory", new AudioVoiceFactory());
			
			add("RenderNotes", new AudioRenderNotes());
			add("RenderControl", new AudioRenderControl());
			add("RenderPitchBend", new AudioRenderPitchBend());
			add("RenderPitch", new AudioRenderPitch());
			add("RenderGain", new AudioRenderGain());
			// * LoadSoundFont(bank, filename)  // �tti frekar a� vera LoadSoundBank, h�fum bara b��i :)
			// * RegisterVoice(bank, program, voice)

			
			// General Filtering
			add("Biquad", new AudioBiquad());
			// * IIR (st�rri �tg�fa af Biquad)
			
			// Envelope Follower
			add("follow", new AudioEnvelopeFollower());
			add("follow2", new AudioEnvelopeFollower2());
			
			// Time Delay Filtering
			add("Reverb", new AudioReverb());
			add("Delay1", new AudioDelay1());			
			add("DelayLine", new AudioDelayLine());			
			
			// RealTime Signal FFT based functions   // Vocoder og Format corrector �arfnast lagf�ringa
			add("FFTConvolution", new AudioFFTConvolution());
			add("PitchShift", new AudioPitchShift());
			add("Vocoder", new AudioVocoder()); 
			add("DeReverb", new AudioDeReverb()); 
			add("DeHiss", new AudioDeHiss());
			add("FormatMapper", new AudioFormatMapping());
			add("HarmonicsBooster", new AudioHarmonicsBooster());
			
			// HissReducer(noisefloor, reducehissby) // Simple Hiss Reducer
			// FFTFilter(freqresponse, magnresponse)  // t.d.  FFTFilter(linseg(0,0.5,0,0,1,0.5,1)) <-- Filterar allt undir 12 khz (100%)
			// t.d.  FFTFilter(1, linseg(0,0.5,0,0,1,0.5,1)) <-- Allt hlj�� undir 0.5 blockera�
			// PhaseShift(phaseshiting)   // t.d.  PhaseShift(linseg(0,0.5,0,0,pi.5,pi)) <-- Filterar yfir 12 khz , sett �r fasa
			// StereoResponse(panresponse)  // t.d. StereoResponse(linseg(0,0.5,0,0,1,0.5,1)) <-- Filtera allt hlj�� vinstra megin a� mi�ju
			// PhaseResponse(phaseresponse)
			
			// General FFT functions
			
			add("IFFT", new AudioIFFT()); 
			add("FFT", new AudioFFT()); 
			
			// Resampling 
			add("Resample", new AudioResample());    // Resampling using No Interpolation
			add("ResampleI", new AudioResampleI());  // Resampling using Linear Interpolation
			add("ResampleC", new AudioResampleC());  // Resampling using Cubic Interpolation
			add("ResampleH", new AudioResampleH());  // Resampling using Hermite Interpolation
			//add("ResampleS", new AudioResampleS());  // Resampling using Sinc Interpolation
			
			
//			Loop/Extracting functions			
//			(static) (variable)			  
//			* loop      <- function (input, from, to, ovarlap);   // using sec values for from an to
//			* loopS     <- function (input, from, to, ovarlap);   // using sample values for from and to
//			* extractT  <- function (input, from, to, smoothing); // default smoothing = 0
//			* extractS  <- function (input, from, to, smoothing); // default smoothing = 0			
			
//			Table Function Generators (perform some kind of buffering)		
			add("table", new AudioTable());	// Uses linear interpolation
//			* tablei    <- function (input, from, to, resolution, channels);
			
			// Line generators
			add("LinSeg", new AudioLinSeg());	
			add("Line", new AudioLinSeg());	
			add("ExpSeg", new AudioExpSeg());
			add("Expon", new AudioExpSeg());
			
			// Release Sensative Envelopes Generators
			add("Adsr", new AudioAdsr());	
			add("XAdsr", new AudioXAdsr());	
			add("Release", new AudioRelease());	
			add("XRelease", new AudioXRelease());
			
			// Release Sensative Envelopes Generator for SoundFonts, LIN-ATTACK, EXP-RELEASE and DECAY and a HOLD variable
			add("Ahdsr", new AudioAhdsr());	
			
			add("Envelope", new AudioAdsr());  // depricated	
			
			
			// Auto Gain Controllers and Compressors filters
			add("Agc", new AudioAGC());
			
			
			//Special case Low-Pass / High-Pass filters
			add("diff", new AudioDiff());
			add("integ", new AudioInteg());			
			
			// Audio Operators			
			add("ChannelGain", new AudioChannelGain());
			add("ChannelMux", new AudioChannelMux());
			add("ChannelMixer", new AudioChannelMixer());
			add("Downmix", new AudioDownMix());
			add("Gain", new AudioOperator(0));
			add("*", new AudioOperator(0));
			add("/", new AudioOperator(1));
			add("-", new AudioOperator(2));
			add("asum", new AudioOperator(3)); 
			add("%", new AudioOperator(4));			
			
			add(">", new AudioOperator(5));
			add("<", new AudioOperator(6));
			add(">=", new AudioOperator(7));
			add("<=", new AudioOperator(8));
			add("!=", new AudioOperator(9));
			add("=", new AudioOperator(10));
			
			add("?", new AudioOperator2(0)); 
			add("if", new AudioOperator2(0)); 
			add("clip", new AudioOperator2(1));
			
			add("srate", new AudioSampleRate());
			
			add("one", new AudioOne());
			add("signal", new AudioSignal());						
			add("noise", new AudioRandom());
			add("clock", new AudioClock());
			add("counter", new AudioCounter());
			add("abs", new AudioOperator(100));
			add("acos", new AudioOperator(101));
			add("asin", new AudioOperator(102));
			add("atan", new AudioOperator(103));
			add("ceil", new AudioOperator(104));
			
			add("fastcos", new AudioOperator(152));
			add("cos", new AudioOperator(105));
			
			add("exp", new AudioOperator(106));
			add("floor", new AudioOperator(107));
			add("log", new AudioOperator(108));
			add("rint", new AudioOperator(109));
			add("round", new AudioOperator(110));
			
			add("fastsin", new AudioOperator(151));
			add("sin", new AudioOperator(111));
			
			add("sqrt", new AudioOperator(112));
			
			add("fasttan", new AudioOperator(153));
			add("tan", new AudioOperator(113));
			
			add("toDegrees", new AudioOperator(114));
			add("toRadians", new AudioOperator(115));
			add("sgn", new AudioOperator(116));
			add("IEEEremainder", new AudioOperator(200));
			add("atan2", new AudioOperator(201));
			add("max", new AudioOperator(202));
			add("min", new AudioOperator(203));
			add("pow", new AudioOperator(204));			
			add("^", new AudioOperator(204));			
			
			interpreter = new Interpreter(namespace);
			interpreter.setAutoCommit(false);
			try {
				interpreter.eval("group_audio_dev <- RegisterGroup(\"Input\") <- group_audio_dev_input;");
			} catch (ScriptParserException e1) {
				e1.printStackTrace();
			}
			try {
				interpreter.eval("group_audio_dev <- RegisterGroup(\"Output\") <- group_audio_dev_output;");
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}
			
			Mixer.Info[] mixers = AudioSystem.getMixerInfo();		
			
			for (int i = 0; i < mixers.length; i++) {
				Mixer.Info mixerinfo = mixers[i];
				Mixer audmixer = AudioSystem.getMixer(mixerinfo);
				//if(audmixer.isLineSupported(dataLineInfo))
				{
					
					boolean ok = false;
					boolean ok2 = false;
					Line.Info[] lineinfos = audmixer.getSourceLineInfo();
					for (int j = 0; j < lineinfos.length; j++) {
						if(lineinfos[j] instanceof DataLine.Info)
						{
							if(lineinfos[j].getLineClass() == SourceDataLine.class)
							{
								ok = true;
							}
						}								
					}
					
					lineinfos = audmixer.getTargetLineInfo();
					for (int j = 0; j < lineinfos.length; j++) {
						if(lineinfos[j] instanceof DataLine.Info)
						{
							if(lineinfos[j].getLineClass() == TargetDataLine.class)
							{
								ok2 = true;
							}		
						}								
					}				
					if(ok)
						try {
							interpreter.eval("group_audio_dev_output <- RegisterConstant(\"" + mixerinfo.getName().replaceAll("\\\"", "\"+'\"'+\"") + "\", \"" + mixerinfo.getDescription() + "\");");
						} catch (ScriptParserException e) {
							e.printStackTrace();
						}
					if(ok2)
						try {
							interpreter.eval("group_audio_dev_input  <- RegisterConstant(\"" + mixerinfo.getName().replaceAll("\\\"", "\"+'\"'+\"") + "\", \"" + mixerinfo.getDescription() + "\");");
						} catch (ScriptParserException e) {
							e.printStackTrace();
						}
				}
			}	
			
			/*
			 MidiDevice.Info devinfo[] = MidiSystem.getMidiDeviceInfo();
			 MidiDevice.Info midimapdev = null;			  
			 for (int i = 0; i < devinfo.length; i++) {
			 try
			 {
			 MidiDevice mididev = MidiSystem.getMidiDevice(devinfo[i]);
			 try
			 {
			 if(mididev.getReceiver() != null)
			 interpreter.eval("group_midi_dev_output <- RegisterConstant(\"" + devinfo[i].getName() + "\", \"" + devinfo[i].getDescription() + "\");");
			 }
			 catch(Exception e)
			 {				    	
			 }
			 
			 try
			 {
			 if(mididev.getTransmitter() != null)
			 interpreter.eval("group_midi_dev_input <- RegisterConstant(\"" + devinfo[i].getName() + "\", \"" + devinfo[i].getDescription() + "\");");
			 }
			 catch(Exception e)
			 {				    	
			 }				  }
			 catch(Exception e)
			 {
			 e.printStackTrace();
			 }				  
			 }			*/
		}
		
		Interpreter interpreter;
		public void close()
		{
			interpreter.close();
			super.close();
		}
		
	}
	
	public UnitInstancePart newInstance(NameSpace namespace) {
		return new ModuleInstance(namespace);
	}
	
}
