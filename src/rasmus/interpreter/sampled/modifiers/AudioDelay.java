/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioDelayInstance extends UnitInstanceAdapter
{
	public Variable output;
	public Variable input;
	
	Variable transpose;	
	double floatpos = 0;
	Variable answer = null;
	AudioEvents audioevents;
	
	public Map pevents = new HashMap();
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			pevents.clear();
		}	
	}
	
	public AudioEvent processEvent(AudioEvent midievent) {
		AudioEvent newevent = new AudioEvent(midievent.start + floatpos, midievent.streamable);
		return newevent;
	}	
	
	public void calc()
	{
		floatpos = DoublePart.asDouble(transpose);
		
		clear();
		answer = new Variable();
		
		audioevents = AudioEvents.getInstance(answer);
		
		List events = AudioEvents.getInstance(input).getObjects();
		Iterator iterator = events.iterator();
		while (iterator.hasNext()) {
			AudioEvent element = (AudioEvent) iterator.next();        	
			AudioEvent pelement = processEvent(element);
			if(pelement != null)
			{
				pevents.put(element, pelement);
				audioevents.addObject(pelement);
			}
		}
		
		output.add(answer);		
	}
	
	class RListListenerAndRecv implements ListPartListener
	{
		public void objectAdded(ListPart source, Object object) {			
			AudioEvent element = (AudioEvent) object;
			AudioEvent pelement = processEvent(element);
			if(pelement != null)
			{        		        		
				pevents.put(element, pelement);        	   
				audioevents.addObject(pelement);
			}
		}
		public void objectRemoved(ListPart source, Object object) {
			AudioEvents audioevents = AudioEvents.getInstance(answer);
			Object pelement = pevents.get(object);
			if(pelement != null) audioevents.removeObject(pelement);			
		}
		public void objectsAdded(ListPart source, List objects) {
			Iterator iterator = objects.iterator();
			while(iterator.hasNext())
			{
				objectAdded(source, iterator.next());
			}
		}
		public void objectsRemoved(ListPart source, List objects) {
			Iterator iterator = objects.iterator();
			while(iterator.hasNext())
			{
				objectRemoved(source, iterator.next());
			}
		}
		public void close() {
			
		}		
		
	}
	
	RListListenerAndRecv eventslistener = new RListListenerAndRecv();	
	public ListPartListener getEventsListener()
	{
		return eventslistener;
	}	
	
	public AudioDelayInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		transpose = parameters.getParameterWithDefault(1, "pos");
		input = parameters.getParameterWithDefault(2, "input");
		
		DoublePart.getInstance(transpose).addListener(this);
		AudioEvents.getInstance(input).addListener(getEventsListener());
		
		calc();
	}
	public void close() {
		DoublePart.getInstance(transpose).removeListener(this);
		AudioEvents.getInstance(input).removeListener(getEventsListener());
		clear();
	}
	
}

public class AudioDelay implements UnitFactory  , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Audio Delay");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1, "pos",			"Position",		null, "beats", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "input",		"Input",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioDelayInstance(parameters);
	}
}