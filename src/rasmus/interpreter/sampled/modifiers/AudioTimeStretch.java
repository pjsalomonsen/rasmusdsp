/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.sampled.util.FFT;
import rasmus.interpreter.sampled.util.PitchShift;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioTimeStretchInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	Variable stretch;
	Variable fftFrameSize;
	Variable osamp;
	
	public void calc()
	{
	}
	
	public AudioTimeStretchInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		stretch = parameters.getParameterWithDefault(1, "amount");
		
		fftFrameSize = parameters.getParameterWithDefault(2, "fftFrameSize");
		osamp = parameters.getParameterWithDefault(3, "osamp"); 
		/*
		 RFloat.getInstance(gain).addListener(this);
		 calc();*/
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		AudioFallBackStream inputstream;
		PitchShift[] pitchshift;
		int channels;
		AudioCache audiocache;
		AudioSession session;
		FFT fft;	
		int i_fftFrameSize;
		int i_osamp;
		int i_stepSize;
		boolean is_eof = false;
		double[] window;
		double[] readbuffer;
		double[] outputbuffer;
		double[][] fftbuffer1;
		double[][] fftbuffer2;
		double[][] fftbufferT;
		
		double[][] fftbufferPhaseReal;
		double[][] fftbufferPhaseImag;
		
		double readpos = 0;
		
		int filterreadpos = 0;
		
		boolean inputstream2_eof = false;
		AudioFallBackStream inputstream2;
		double d_stretch = 1;
		
		boolean firsttime = false;
		public FilterStreamInstance(AudioStream inputstream, AudioSession session)
		{			
			this.session = session;
			audiocache = session.getAudioCache();
			this.channels = session.getChannels();
			
			d_stretch = DoublePart.asDouble(stretch);			
			
			i_fftFrameSize = (int)DoublePart.asDouble(fftFrameSize);
			i_osamp = (int)DoublePart.asDouble(osamp);
			
			if(i_fftFrameSize == 0) i_fftFrameSize = 2048;
			if(i_osamp == 0) i_osamp = 4;
			
			i_stepSize = i_fftFrameSize / i_osamp; // 256
			
			fft = new FFT(i_fftFrameSize);		
			window = fft.wHanning();
			this.inputstream = new AudioFallBackStream(inputstream);
			
			if(AudioEvents.getInstance(stretch).track.size() == 0)
			{
				inputstream2_eof = true;
			}
			else
				inputstream2 = new AudioFallBackStream(AudioEvents.openStream(stretch, session.getMonoSession()));
			
			
			readbuffer = new double[i_fftFrameSize * channels];
			outputbuffer = new double[i_fftFrameSize * channels];
			
			fftbuffer1 = new double[channels][i_fftFrameSize];
			fftbuffer2 = new double[channels][i_fftFrameSize];
			fftbufferT = new double[channels][i_fftFrameSize];
			
			fftbufferPhaseReal = new double[channels][i_fftFrameSize];			
			fftbufferPhaseImag = new double[channels][i_fftFrameSize];
			for (int c = 0; c < channels; c++) {
				Arrays.fill(fftbufferPhaseReal[c], 1);
			}				
			for (int c = 0; c < channels; c++) {
				Arrays.fill(fftbufferPhaseImag[c], 0);
			}				
			
			
			
			
		}
		
		
		boolean firstone = true;
		public void readFFTBuffer(double fftbuffer[][])
		{
			if(is_eof) 
			{
				for (int c = 0; c < channels; c++) {
					Arrays.fill(fftbuffer[c], 0);
				}				
				return;
			}
			double[] stockbuffer = readbuffer;
			int ret;
			if(firstone)
			{
				ret = inputstream.replace(stockbuffer, 0, i_fftFrameSize*channels);
				if(ret != -1)
					for (int i = ret; i < i_fftFrameSize*channels; i++) {
						stockbuffer[i] = 0;
					}
				firstone = false;
			}
			else
			{
				int c_stepsize = i_stepSize*channels;
				for (int i = 0; i < (i_fftFrameSize - i_stepSize)*channels; i++) {
					stockbuffer[i] = stockbuffer[i + c_stepsize];  
				}			
				ret = inputstream.replace(stockbuffer, (i_fftFrameSize - i_stepSize)*channels, i_fftFrameSize*channels);
				if(ret != -1)
					if(ret != i_stepSize*channels)
					{
						int offset = (i_fftFrameSize - i_stepSize)*channels;
						for (int i = offset + ret; i < i_fftFrameSize*channels; i++) {
							stockbuffer[i] = 0;
						}
					}				
			}
			if(ret == -1)
			{
				for (int c = 0; c < channels; c++) {
					Arrays.fill(fftbuffer[c], 0);
				}				
				is_eof = true;
				return;
			}
			
			
			int channels = this.channels;
			for (int c = 0; c < channels; c++) {				
				double[] cfftbuffer = fftbuffer[c];
				for (int j = 0; j < i_fftFrameSize; j++) {
					cfftbuffer[j] = (stockbuffer[j*channels + c] * window[j]);
					//cfftbuffer[2*j+1] = 0.;					
				}
				fft.calcReal(cfftbuffer, -1);
			}
			
			
			
		}
		
		public void skipFFTBuffer()
		{
			if(is_eof) 
			{				
				return;
			}
			double[] stockbuffer = readbuffer;
			int ret;
			if(firstone)
			{
				ret = inputstream.replace(stockbuffer, 0, i_fftFrameSize*channels);
				if(ret != -1)
					for (int i = ret; i < i_fftFrameSize*channels; i++) {
						stockbuffer[i] = 0;
					}
				firstone = false;
			}
			else
			{
				int c_stepsize = i_stepSize*channels;
				for (int i = 0; i < (i_fftFrameSize - i_stepSize)*channels; i++) {
					stockbuffer[i] = stockbuffer[i + c_stepsize];  
				}			
				ret = inputstream.replace(stockbuffer, (i_fftFrameSize - i_stepSize)*channels, i_fftFrameSize*channels);
				if(ret != -1)
					if(ret != i_stepSize*channels)
					{
						int offset = (i_fftFrameSize - i_stepSize)*channels;
						for (int i = offset + ret; i < i_fftFrameSize*channels; i++) {
							stockbuffer[i] = 0;
						}
					}				
			}
			if(ret == -1)
			{
				is_eof = true;
				return;
			}
		}		
		
		public void readInterpoledBuffer(double fftbuffer[][])
		{
			while(readpos >= 3)
			{				
				skipFFTBuffer();		
				readpos -= 1;
			}
			
			while(readpos >= 1)
			{				
				double[][] tmp = fftbuffer1;
				fftbuffer1 = fftbuffer2;
				fftbuffer2 = tmp;
				readFFTBuffer(fftbuffer2);
				readpos -= 1;
			}
			if(d_stretch == 0) d_stretch = 1;				
			double r = 1 / d_stretch;						
			readpos += r;			
			
			for (int c = 0; c < channels; c++) {				
				double[] cfftbuffer = fftbuffer[c];
				double[] cfftbuffer1 = fftbuffer1[c];
				double[] cfftbuffer2 = fftbuffer2[c];
				
				double[] cPhaseReal = fftbufferPhaseReal[c];			
				double[] cPhaseImag = fftbufferPhaseImag[c];
				
				
				for (int j = 0; j < i_fftFrameSize; j+=+2) {
					double real1 = cfftbuffer1[j]; 
					double imag1 = cfftbuffer1[j + 1]; 
					double real2 = cfftbuffer2[j]; 
					double imag2 = cfftbuffer2[j + 1];
					
					double phase_real = cPhaseReal[j];
					double phase_imag = cPhaseImag[j];
					
					
					double magn1 = Math.sqrt(real1*real1 + imag1*imag1);
					double magn2 = Math.sqrt(real2*real2 + imag2*imag2);					
					
					double out_magn = magn1*(1-readpos) + magn2*(readpos);
					cfftbuffer[j] = phase_real * out_magn;
					cfftbuffer[j+1] = phase_imag * out_magn;
					
					double phase1_real;
					double phase1_imag;					
					double phase2_real;
					double phase2_imag;					
					
					if(magn1 < 0.0000001)
					{
						phase1_real = 1;
						phase1_imag = 0;
					}
					else
					{
						double imagn1 = 1/magn1;
						phase1_real = real1 * imagn1;
						phase1_imag = imag1 * imagn1;
					}
					
					if(magn2 < 0.0000001)
					{
						phase2_real = 1;
						phase2_imag = 0;
					}
					else
					{
						double imagn2 = 1/magn2;
						phase2_real = real2 * imagn2;
						phase2_imag = imag2 * imagn2;
					}					
					
					// phaseAdv = phase1 * phase2^T; 
					double phaseAdv_real = phase2_real * phase1_real + phase1_imag * phase2_imag;
					double phaseAdv_imag = phase2_imag * phase1_real - phase1_imag * phase2_real;
					
					//double phaseAdv_real = phase1_real * phase2_real + phase2_imag * phase1_imag;
					//double phaseAdv_imag = phase1_imag * phase2_real - phase2_imag * phase1_real;
					
					// cPhase = cPhase * phaseAdv;
					cPhaseReal[j] = phase_real * phaseAdv_real - phaseAdv_imag * phase_imag;
					cPhaseImag[j] = phase_imag * phaseAdv_real + phaseAdv_imag * phase_real;
					
					
					
				}
			}			
		}
		
		public void readIFFT()
		{
			double[] stockbuffer = outputbuffer;
			for (int i = 0; i < (i_fftFrameSize - i_stepSize)*channels; i++) {
				stockbuffer[i] = stockbuffer[i + i_stepSize*channels];  
			}							
			for (int i = (i_fftFrameSize - i_stepSize)*channels; i < (i_fftFrameSize)*channels; i++) {
				stockbuffer[i] = 0;
			}
			readInterpoledBuffer(fftbufferT);
			
			int channels = this.channels;
			double scale = (1.0 / (double)i_fftFrameSize)*(8.0/(3.0*i_osamp));
			
			for (int c = 0; c < channels; c++) {				
				double[] cfftbuffer = fftbufferT[c];
				//Arrays.fill(cfftbuffer, i_fftFrameSize, i_fftFrameSize*2,0);
				fft.calcReal(cfftbuffer, 1);
				int k = c; 
				
				
				for (int j = 0; j < i_fftFrameSize; j++) {					
					stockbuffer[k] += cfftbuffer[j] * window[j] * scale;
					k += channels;    					
				}
			}			
			
		}
		
		
		public int replace(double[] buffer, int start, int end) {
			
			double[] stockbuffer2 = null;
			d_stretch = DoublePart.asDouble(stretch);
			if(!inputstream2_eof)
			{
				int cstart = start / channels;
				int cend = end / channels;				
				stockbuffer2 = new double[1];
				if(inputstream2.isStatic(stockbuffer2, cend - cstart) == -1)
				{								
					stockbuffer2 = audiocache.getBuffer(cend);
					int ret2 = inputstream2.replace(stockbuffer2, cstart, cend);
					if(ret2 == -1)
					{
						inputstream2_eof = true;
						audiocache.returnBuffer(stockbuffer2);
						stockbuffer2 = null;
					}
					else
					{
						Arrays.fill(stockbuffer2, cstart + ret2, cend, DoublePart.asDouble(stretch));
						d_stretch = stockbuffer2[0];
					}
				}
				else
				{
					d_stretch = stockbuffer2[0];
					stockbuffer2 = null;
				}
				
			}			
			
			if(firsttime)
			{
				readFFTBuffer(fftbuffer1);
				readFFTBuffer(fftbuffer2);
				readIFFT();				
				firsttime = false;
			}
			
			int fx = this.filterreadpos;
			int c_stepsize = i_stepSize*channels;
			for (int i = start; i < end; i++) {
				buffer[i] = outputbuffer[fx];				
				fx++;
				if(fx == c_stepsize)
				{
					fx = 0;
					readIFFT();
				}
			}
			this.filterreadpos = fx;
			if(is_eof) return -1; else return end - start;			
		}
		public int mix(double[] buffer, int start, int end) {
			double[] stockbuffer = audiocache.getBuffer(end);
			
			int ret = replace(stockbuffer, start, end);
			int cret = ret; if(cret == -1) cret = 0;
			for (int i = start; i < (start + cret); i++) {
				buffer[i] += stockbuffer[i];
			}			
			audiocache.returnBuffer(stockbuffer);
			return ret;
		}
		
		public int skip(int len)
		{
			int ret  = inputstream.skip(len);
			return ret;
		}				
		public int isStatic(double[] buffer, int len) {
			
			return -1; //inputstream.isStatic(buffer, len);
		}				
		public void close() {
			inputstream.close();
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(AudioEvents.openStream(input, session.newSession()), session);
	}
	
}

public class AudioTimeStretch implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Time Stretcher");
		metadata.add(-1, "output",			"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "amount",			"Amount",  		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2,  "fftFrameSize",	"FFT size",		"2048", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3,  "osamp",			"FFT Overlap",  "4", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",			"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioTimeStretchInstance(parameters);
	}
}