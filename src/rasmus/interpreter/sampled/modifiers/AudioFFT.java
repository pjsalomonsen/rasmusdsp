/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.sampled.util.FFT;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioFFTInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	Variable buffersize = new Variable();
	
	public void calc()
	{
	}
	
	public AudioFFTInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");		
		buffersize = parameters.getParameterWithDefault(1, "buffersize");
		
		/*
		 RFloat.getInstance(gain).addListener(this);
		 calc();*/
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		AudioStream inputstream;
		AudioSession session;
		double[] stockbuffer = null;
		//double[] chbuff;
		//int channels;
		
		int buffersize_i;		
		int fftrover = 0;
		double[] fftbuffer;
		FFT fft;
		boolean inputstream_eof = false;
		
		public FilterStreamInstance(AudioSession session)
		{			
			this.session = session;
			buffersize_i = (int)DoublePart.asDouble(buffersize);			
			fftbuffer = session.getAudioCache().getBuffer(buffersize_i*2);
			fft = new FFT(buffersize_i);
			
			//chbuff = new double[channels];
			//Arrays.fill(chbuff, 0);
			inputstream = AudioEvents.openStream(input, session.getMonoSession());
			processFFT();
		}
		public int skip(int len)
		{
			int ret  = inputstream.skip(len);
			return ret;
		}		
		
		public void processFFT()
		{
			if(inputstream_eof)
			{
				Arrays.fill(fftbuffer, 0, buffersize_i*2, 0);
				return;
			}
			double[] buffer = session.getAudioCache().getBuffer(4096);
			for (int i = 0; i < buffersize_i; i+= 4096) {
				int start = i;
				int end = i + 4096;
				if(end > buffersize_i) end = buffersize_i;
				int ret  = inputstream.replace(buffer, 0, end - start);
				if(ret == -1)
				{
					Arrays.fill(fftbuffer, start*2, buffersize_i*2 , 0);
					inputstream_eof = true;
					break;
				}
				int k = start * 2;
				for (int j = 0; j < ret; j++, k+=2) {
					fftbuffer[k]   = buffer[j];  // realpart
					fftbuffer[k+1] = 0;          // imgpart
				}
				Arrays.fill(fftbuffer, (start + ret)*2 , end*2, 0);
			}				
			session.getAudioCache().returnBuffer(buffer);
			fft.calc(fftbuffer, -1);
		}
		
		public int mix(double[] buffer, int start, int end) {
			
			if(inputstream_eof) return -1;
			
			int rover = fftrover;
			int buffersize_i = this.buffersize_i;
			int buffersize_i2 = buffersize_i * 2;
			double[] fftbuffer = this.fftbuffer;
			for (int i = start; i < end; i++) {
				buffer[i] += fftbuffer[rover];
				rover = (rover + 1) % buffersize_i2;
				/*
				 if(rover == buffersize_i2)
				 {
				 rover = 0;
				 processFFT();
				 if(inputstream_eof)
				 {
				 return 1 + i - start;
				 }
				 }*/
			}
			fftrover = rover;
			return end - start;
		}
		public int replace(double[] buffer, int start, int end) {
			/*
			 *  VER�UR SKIPT �T �EGAR AudioOperator hefur veri� n�gilega pr�fa�ur
			 */
			/*
			 Arrays.fill(buffer, start, end, 0);
			 return mix(buffer, start, end);		*/
			
			
			if(inputstream_eof) return -1;
			
			int rover = fftrover;
			int buffersize_i = this.buffersize_i;
			int buffersize_i2 = buffersize_i * 2;
			double[] fftbuffer = this.fftbuffer;
			for (int i = start; i < end; i++) {
				buffer[i] = fftbuffer[rover];
				rover = (rover + 1) % buffersize_i2;
				/*
				 if(rover == buffersize_i2)
				 {
				 rover = 0;
				 processFFT();
				 if(inputstream_eof)
				 {
				 return 1 + i - start;
				 }
				 }*/
			}
			fftrover = rover;
			return end - start;			
		}
		public int isStatic(double[] buffer, int len) {
			return -1;
		}				
		public void close() {
			inputstream.close();
			session.getAudioCache().returnBuffer(fftbuffer);
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioFFT implements UnitFactory {
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioFFTInstance(parameters);
	}
}