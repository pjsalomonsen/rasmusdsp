/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.ArrayList;
import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioChannelMixerInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	Variable[] channels;
	
	public void calc()
	{
	}
	
	public AudioChannelMixerInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		
		ArrayList vars = new ArrayList();
		/*
		 Iterator keys = parameters.keySet().iterator();
		 while (keys.hasNext()) {
		 String keyname = (String) keys.next();
		 if(!keyname.equals("0"))
		 if(!keyname.equals("output"))
		 {
		 vars.add(parameters.get(keyname));				
		 }
		 }*/
		
		int ii = 1;
		Variable var = parameters.getParameter(ii);
		while(var != null)
		{
			vars.add(var);
			ii++;
			var = parameters.getParameter(ii);
		}
		
		channels = new Variable[vars.size()];
		for (int i = 0; i < channels.length; i++) {
			channels[i] = (Variable)vars.get(i);
		}
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		//double ff_1 = RFloat.asFloat(input);
		double[] ff = new double[channels.length];
		AudioStream inputstream;
		AudioStream[] inputstreams ;
		//double[][] stockbuffers ;		
		boolean[] inputstreams_eof ;
		//double[] stockbuffer = null;
		int totalchannels;
		int chs;
		AudioCache audiocache;
		public FilterStreamInstance(AudioSession session)
		{
			audiocache = session.getAudioCache();
			this.chs = session.getChannels();
			//totalchannels = chs;
			//if(totalchannels > channels.length)
			totalchannels = channels.length;
			
			ff = new double[totalchannels];
			inputstreams = new AudioStream[totalchannels];
			inputstreams_eof = new boolean[totalchannels];
			
			for (int i = 0; i < totalchannels; i++) {
				
				
				
				if(AudioEvents.getInstance(channels[i]).track.size() == 0)
				{
					inputstreams_eof[i] = true;
					inputstreams[i] = null;
				}
				else
				{
					inputstreams_eof[i] = false;
					inputstreams[i] = AudioEvents.openStream(channels[i], session.getMonoSession());
				}
				ff[i] = DoublePart.asDouble(channels[i]);
			}	
			
			inputstream = AudioEvents.openStream(input, session);
		}
		public int skip(int len) {
			
			
			int ret  = inputstream.skip(len);
			
//			int cstart = start / chs;
//			int cend = end / chs;
			int clen = len / chs;
//			double[] stockbufferc = audiocache.getBuffer(cend);
			for (int c = 0; c < totalchannels; c++) {
				
				if(!inputstreams_eof[c])
				{
					int retc = inputstreams[c].skip(clen);
					if(retc == -1)
					{
						inputstreams_eof[c] = true;
					}
					else
					{
						/*
						 double fff = ff[c];
						 Arrays.fill(stockbufferc, cstart + retc, cend, fff);			
						 int ix = 0;
						 int endret = start + ret;
						 for (int i = start + c; i < endret; i+=chs) {
						 buffer[i] += stockbufferc[ix]*stockbuffer[i];
						 ix++;
						 }*/
						
					}
				}
				/*	
				 if(inputstreams_eof[c])
				 {
				 double fff = ff[c];					
				 int ix = 0;
				 int endret = start + ret;
				 for (int i = start + c; i < endret; i+=chs) {
				 buffer[i] += fff*stockbuffer[i];
				 ix++;
				 }
				 }*/
				
				
			}
//			audiocache.returnBuffer(stockbufferc);
//			audiocache.returnBuffer(stockbuffer);
			
			return ret;
		}
		
		public int mix(double[] buffer, int start, int end) {
			
			
			double[] stockbuffer = audiocache.getBuffer(end);
			
			int ret  = inputstream.replace(stockbuffer, start, end);
			
			int cstart = start / chs;
			int cend = end / chs;
			double[] stockbufferc = audiocache.getBuffer(cend);
			for (int c = 0; c < totalchannels; c++) {
				
				int targetchannel = (int)(c / chs);
				int fromchannel = c % chs;
				while(targetchannel > chs) targetchannel -= chs; 
				
				if(!inputstreams_eof[c])
				{
					int retc = inputstreams[c].replace(stockbufferc, cstart, cend);
					if(retc == -1)
					{
						inputstreams_eof[c] = true;
					}
					else
					{
						double fff = ff[c];
						Arrays.fill(stockbufferc, cstart + retc, cend, fff);			
						int ix = cstart;
						int endret = start + ret;				
						for (int i = start ; i < endret; i+=chs) {
							buffer[i + targetchannel] += stockbufferc[ix]*stockbuffer[i + fromchannel];
							ix++;
						}
						
					}
				}
				
				if(inputstreams_eof[c])
				{
					double fff = ff[c];					
					//int ix = 0;
					int endret = start + ret;
					if(Math.abs(fff) > 0.00000001)
						for (int i = start; i < endret; i+=chs) {
							buffer[i + targetchannel] += fff*stockbuffer[i + fromchannel];
							//ix++;
						}
				}
				
				
			}
			audiocache.returnBuffer(stockbufferc);
			audiocache.returnBuffer(stockbuffer);
			
			return ret;
		}
		public int replace(double[] buffer, int start, int end) {
			/*
			 *  VER�UR SKIPT �T �EGAR �essi AudioOperator hefur veri� n�gilega pr�fa�ur
			 */
			
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);
			
			//double[] stockbuffer = buffer; //audiocache.getBuffer(end);
			/*
			 int ret  = inputstream.replace(buffer, start, end);
			 
			 int cstart = start / chs;
			 int cend = end / chs;
			 double[] stockbufferc = audiocache.getBuffer(cend);
			 for (int c = 0; c < totalchannels; c++) {
			 
			 if(!inputstreams_eof[c])
			 {
			 int retc = inputstreams[c].replace(stockbufferc, cstart, cend);
			 if(retc == -1)
			 {
			 inputstreams_eof[c] = true;
			 }
			 else
			 {
			 double fff = ff[c];
			 Arrays.fill(stockbufferc, cstart + retc, cend, fff);			
			 int ix = cstart;
			 int endret = start + ret;
			 for (int i = start + c; i < endret; i+=chs) {
			 buffer[i] *= stockbufferc[ix]; //*stockbuffer[i]
			 ix++;
			 }
			 
			 }
			 }
			 
			 if(inputstreams_eof[c])
			 {
			 double fff = ff[c];					
			 //int ix = 0;
			  int endret = start + ret;
			  for (int i = start + c; i < endret; i+=chs) {
			  buffer[i] *= fff; //*stockbuffer[i];
			  //ix++;
			   }
			   }
			   
			   
			   }
			   audiocache.returnBuffer(stockbufferc);
			   //audiocache.returnBuffer(stockbuffer);
			    
			    return ret;			*/
		}
		public int isStatic(double[] buffer, int len) {
			return -1;
		}				
		public void close() {
			for (int i = 0; i < inputstreams.length; i++) {
				if(inputstreams[i] != null)
					inputstreams[i].close();
			}			
			inputstream.close();			
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioChannelMixer implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Multiple Channel Mixer");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "input",		"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		metadata.setHasVarargs(true);
		return metadata;
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioChannelMixerInstance(parameters);
	}
}