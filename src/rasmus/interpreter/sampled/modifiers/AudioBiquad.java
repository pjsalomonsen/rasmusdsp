/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioBiquadInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public final static int PNUM = 6;
	
	public Variable output;
	Variable answer = new Variable();
	Variable input;
	Variable[] a = new Variable[PNUM];
	
	public void calc()
	{
	}	
	public AudioBiquadInstance(Parameters parameters)
	{				
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		a[0] = parameters.getParameterWithDefault(1, "b0");
		a[1] = parameters.getParameterWithDefault(2, "b1");
		a[2] = parameters.getParameterWithDefault(3, "b2");
		a[3] = parameters.getParameterWithDefault(4, "a0");
		a[4] = parameters.getParameterWithDefault(5, "a1");
		a[5] = parameters.getParameterWithDefault(6, "a2");
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		
		
		
		AudioFallBackStream input_stream = null;
		
		double[] f_a = new double[PNUM];
		boolean input_stream_eof = false;
		boolean[] a_stream_eof = new boolean[PNUM];	    	    
		AudioStream[] a_stream = new AudioStream[PNUM];
		
		int channels;	
		double rate;    
		AudioCache audiocache;
		
		double[] xn_1;
		double[] xn_2;
		double[] yn_1;
		double[] yn_2;		        
		
		public FilterStreamInstance(AudioSession session)
		{				
			audiocache = session.getAudioCache();
			this.channels = session.getChannels();
			this.rate = session.getRate();
			
			xn_1 = new double[channels];
			xn_2 = new double[channels];
			yn_1 = new double[channels];
			yn_2 = new double[channels];				
			Arrays.fill(xn_1, 0);
			Arrays.fill(xn_2, 0);
			Arrays.fill(yn_1, 0);
			Arrays.fill(yn_2, 0);
			
			input_stream_eof =(AudioEvents.getInstance(input).track.size() == 0);
			if(!input_stream_eof) input_stream = new AudioFallBackStream(AudioEvents.openStream(input, session));
			
			AudioSession monosession = session.getMonoSession();
			for (int i = 0; i < PNUM; i++) {
				f_a[i] = DoublePart.asDouble(a[i]);
				a_stream_eof[i] =(AudioEvents.getInstance(a[i]).track.size() == 0);
				if(!a_stream_eof[i]) 
					a_stream[i] = AudioEvents.openStream(a[i], monosession);
				else 
					a_stream[i] = null;
			}
			
		}
		
		
		public int skip(int len)
		{
			if(input_stream_eof) return -1;
			int ret  = input_stream.skip(len);
			
			int clen = len / channels;
			for (int i = 0; i < PNUM; i++) {
				if(!a_stream_eof[i])
				{
					int aret = a_stream[i].skip(clen);
					if(aret == -1)
					{
						a_stream_eof[i] = true;
					}
				}
			}			
			
			return ret;
		}		
		public int mix(double[] buffer, int start, int end) {
			
			if(input_stream_eof) return -1;
			
			double[] stockbuffer = audiocache.getBuffer(end);
			int ret = input_stream.replace(stockbuffer, start, end);
			if(ret == -1)
			{
				input_stream_eof = true;
				audiocache.returnBuffer(stockbuffer);
				return -1;				
			}			
			end = start + ret;
			
			int cend = end / channels;
			int cstart = start / channels;
			
			double[][] cbuffer = new double[PNUM][];
			
			boolean variablebuffers = false;
			
			for (int i = 0; i < PNUM; i++) {
				if(!a_stream_eof[i])
				{
					cbuffer[i] = audiocache.getBuffer(cend);
					int aret = a_stream[i].replace(cbuffer[i], cstart, cend);
					if(aret == -1)
					{
						a_stream_eof[i] = true;
					}
					else
					{
						variablebuffers = true;
						Arrays.fill(cbuffer[i], cstart + aret, cend, f_a[i]);
					}
				}
				else
				{
					cbuffer[i] = null;
				}
			}
			if(variablebuffers)
				for (int i = 0; i < PNUM; i++) {
					if(cbuffer[i] == null)
					{
						cbuffer[i] = audiocache.getBuffer(cend);
						Arrays.fill(cbuffer[i], cstart, cend, f_a[i]);
					}
				}
			
			
			
			
			if(variablebuffers)
			{
				double[] xbb0 = cbuffer[0];
				double[] xbb1 = cbuffer[1];
				double[] xbb2 = cbuffer[2];
				double[] xaa0 = cbuffer[3];
				double[] xaa1 = cbuffer[4];
				double[] xaa2 = cbuffer[5];
				
				double bb0;
				double bb1;
				double bb2;
				double aa0;
				double aa1;
				double aa2;				
				
				int chs = channels;
				
				for (int c = 0; c < chs; c++) {
					int ix = start + c;
					
					double xn_1 = this.xn_1[c];
					double xn_2 = this.xn_2[c];
					double yn_1 = this.yn_1[c];
					double yn_2 = this.yn_2[c];						
					for (int i = cstart; i < cend; i++) {
						
						aa0 = 1 / xaa0[i];
						
						bb0 = xbb0[i];
						bb1 = xbb1[i];
						bb2 = xbb2[i];
						aa1 = xaa1[i];
						aa2 = xaa2[i];
						
						double xn = stockbuffer[ix];
						double yn = aa0 * (bb0*xn + bb1*xn_1 + bb2*xn_2 - aa1*yn_1 - aa2*yn_2);
						
						if(yn > 0.0) if(yn < 1.0E-10) yn = 0;
						if(yn < 0.0) if(yn > -1.0E-10) yn = 0;				
						
						xn_2 = xn_1;
						xn_1 = xn;
						yn_2 = yn_1;					
						yn_1 = yn;
						
						buffer[ix] += yn;
						ix += chs;
					}
					
					if(xn_1 > 0.0) if(xn_1 < 1.0E-10) xn_1 = 0;
					if(xn_2 < 0.0) if(xn_2 > -1.0E-10) xn_2 = 0;				
					if(yn_1 > 0.0) if(yn_1 < 1.0E-10) yn_1 = 0;
					if(yn_2 < 0.0) if(yn_2 > -1.0E-10) yn_2 = 0;				
					
					this.xn_1[c] = xn_1;
					this.xn_2[c] = xn_2;
					this.yn_1[c] = yn_1;
					this.yn_2[c] = yn_2;							
				}
			}
			else
			{
				double aa0 = 1 / f_a[3];
				
				double bb0 = f_a[0];
				double bb1 = f_a[1];
				double bb2 = f_a[2];
				
				double aa1 = f_a[4];
				double aa2 = f_a[5];
				
				int chs = channels;
				
				for (int c = 0; c < chs; c++) {
					int ix = start + c;
					double xn_1 = this.xn_1[c];
					double xn_2 = this.xn_2[c];
					double yn_1 = this.yn_1[c];
					double yn_2 = this.yn_2[c];							
					for (int i = cstart; i < cend; i++) {
						
						double xn = stockbuffer[ix];
						double yn = aa0 * (bb0*xn + bb1*xn_1 + bb2*xn_2 - aa1*yn_1 - aa2*yn_2);
						
						if(yn > 0.0) if(yn < 1.0E-10) yn = 0;
						if(yn < 0.0) if(yn > -1.0E-10) yn = 0;			
						
						xn_2 = xn_1;
						xn_1 = xn;
						yn_2 = yn_1;					
						yn_1 = yn;
						
						buffer[ix] += yn;
						ix += chs;
					}
					
					//if(xn_1 > 0.0) if(xn_1 < 1.0E-10) xn_1 = 0;
					if(xn_2 < 0.0) if(xn_2 > -1.0E-10) xn_2 = 0;				
					if(yn_1 > 0.0) if(yn_1 < 1.0E-10) yn_1 = 0;
					if(yn_2 < 0.0) if(yn_2 > -1.0E-10) yn_2 = 0;								
					
					this.xn_1[c] = xn_1;
					this.xn_2[c] = xn_2;
					this.yn_1[c] = yn_1;
					this.yn_2[c] = yn_2;							
					
				}			     				
				
			}
			
			
			
			audiocache.returnBuffer(stockbuffer);
			for (int i = 0; i < PNUM; i++) {
				if(cbuffer[i] != null) audiocache.returnBuffer(cbuffer[i]);
			}
			
			return ret;
			
		}
		
		public int isStatic(double[] buffer, int len) {
			
			
			for (int i = 0; i < channels; i++) {
				if(xn_1[i] != 0) return -1;
				if(xn_2[i] != 0) return -1;
				if(yn_1[i] != 0) return -1;
				if(yn_2[i] != 0) return -1;
			}					
			if(input_stream_eof) return -1;
			
			int ret = input_stream.isStatic(buffer, len);
			if(ret == -1) return -1;
			if(buffer[0] != 0)
			{
				input_stream.fallBack();
				return -1;
			}
			
			int cend = len / channels;
			int cstart = 0 / channels;			
			double[] cbuffer = audiocache.getBuffer(cend);
			//boolean variablebuffers = false;			
			for (int i = 0; i < PNUM; i++) {
				if(!a_stream_eof[i])
				{
					int aret = a_stream[i].replace(cbuffer, cstart, cend); // Vantar skip fall � AudioStream
					if(aret == -1)
					{
						a_stream_eof[i] = true;
					}
				}
			}
			audiocache.returnBuffer(cbuffer);
			
			return len;
		}				
		public int replace(double[] buffer, int start, int end) {
			
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);									
		}
		public void close() {
			if(input_stream != null) input_stream.close();
			for (int i = 0; i < PNUM; i++) {
				//if(!a_stream_eof[i])
				//{
				if(a_stream[i] != null)
				{
					a_stream[i].close();
					a_stream[i] = null;
				}
				//}
			}			
			
		}		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioBiquad implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Biquad IIR Filter");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);	
		metadata.add(1, "b0",			"b[0]",	    null, null, MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "b1",			"b[1]",	    null, null, MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "b2",			"b[2]",	    null, null, MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "a0",			"a[0]",	    null, null, MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(5, "a1",			"a[1]",	    null, null, MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(6, "a2",			"a[2]",	    null, null, MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}			 
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioBiquadInstance(parameters);
	}
}