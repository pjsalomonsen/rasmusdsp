/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioResampleCInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	Variable amount;
	
	public void calc()
	{
	}
	
	public AudioResampleCInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		amount = parameters.getParameterWithDefault(1, "amount");
		/*
		 RFloat.getInstance(gain).addListener(this);
		 calc();*/
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		double f_amount = DoublePart.asDouble(amount);
		AudioStream inputstream;
		AudioStream finputstream;
		
		int channels;
		double[] lastvalues;
		public FilterStreamInstance(AudioSession session)
		{	
			this.channels = session.getChannels();
			lastvalues = new double[channels];
			
			inputstream = AudioEvents.openStream(input, session.newSession());
			finputstream = AudioEvents.openStream(amount, session);			
		}
		double ix = 0;
		double[] stockbuffer = null;
		double[] freqbuffer = null;
		boolean finputstream_eof = false;
		
		public int mix(double[] buffer, int start, int end) {
			
			
			if(freqbuffer == null) freqbuffer = new double[buffer.length / channels];
			else if((freqbuffer.length*channels) < buffer.length)
			{
				freqbuffer = new double[buffer.length / channels];
				if(finputstream_eof)
					Arrays.fill(freqbuffer, f_amount);
			}
			
			int cend = end / channels;
			int cstart = start / channels;
			
			if(!finputstream_eof)
			{
				
				int ret2 = finputstream.replace(freqbuffer, cstart, cend);
				if(ret2 == -1)
				{
					Arrays.fill(freqbuffer, f_amount);
					finputstream_eof = true;
				}
				else
				{
					int cendret = cstart + ret2;
					for (int i = cstart; i < cendret; i++) {
						freqbuffer[i] = freqbuffer[i];
					}
					Arrays.fill(freqbuffer, cstart + ret2, cend, f_amount);
					/*
					 for (int i = cstart + ret2; i < cend; i++) {
					 freqbuffer[i] = f_stepsize;
					 }*/
				}
			}						
			
			
			
			int skorun = 4;
			int sbuffsize = 2048;
			int xloopbuffsize = sbuffsize - skorun;
			int loopbuffsize = xloopbuffsize*channels;;
			int ret = 0;
			if(stockbuffer == null) 
			{
				stockbuffer = new double[sbuffsize * channels];
				ret = inputstream.replace(stockbuffer, 0, sbuffsize*channels);
				if(ret == -1)
					Arrays.fill(stockbuffer, channels, sbuffsize*channels, 0);
				else
					Arrays.fill(stockbuffer, ret, sbuffsize*channels,0 );
			}			
			
			int channels1 = channels * 1;
			int channels2 = channels * 2;
			int channels3 = channels * 3;
			//double ixstep = f_amount;
			int ixx = 0;
			for (int i = start; i < end; i+=channels) {		
				int j = ((int)(ix))*channels;
				double interp = (ix % 1.0);
				double interp2 = interp * interp; 
				for (int c = 0; c < channels; c++) {		
					
					/*
					 Reference: http://astronomy.swin.edu.au/~pbourke/other/interpolation/
					 */					
					double y0 = stockbuffer[j];
					double y1 = stockbuffer[j + channels1];
					double y2 = stockbuffer[j + channels2];
					double y3 = stockbuffer[j + channels3];
					
					double a0 = y3 - y2 - y0 + y1;
					double a1 = y0 - y1 - a0;
					double a2 = y2 - y0;
					double a3 = y1;		
					
					buffer[i + c] += a0*interp*interp2+a1*interp2+a2*interp+a3; //(stockbuffer[j] * (1 - interp)) + (stockbuffer[j + channels]*interp);
					j++;
				}
				if(freqbuffer[ixx] > 0)
					ix+=freqbuffer[ixx]; //ixstep;	
				
				ixx++;
				if(ix > xloopbuffsize)
				{
					ix -= xloopbuffsize;
					
					for (int c = 0; c < skorun*channels; c++) {								
						stockbuffer[c] = stockbuffer[loopbuffsize + c];					
					}
					ret = inputstream.replace(stockbuffer, channels*skorun, sbuffsize*channels);					
					if(ret == -1)
						Arrays.fill(stockbuffer, channels*skorun, 2048*channels, 0);
					else
						Arrays.fill(stockbuffer, channels*skorun + ret, 2048*channels,0 );
				}
			}
			
			if(ret == -1) return -1; else return end - start;
			
		}
		public int replace(double[] buffer, int start, int end) {
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);
		}
		public int isStatic(double[] buffer, int len) {
			return -1;
		}				
		public int skip(int len)
		{
			return mix(new double[len], 0, len);
		}		
		public void close() {
			inputstream.close();
			finputstream.close();
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioResampleC implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Resample with cubic interpolation");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1, "amount",		"Amount",	    null, null, MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioResampleCInstance(parameters);
	}
}