/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioEnvelopeFollower2Instance extends UnitInstanceAdapter implements AudioStreamable
{
	public final static int PNUM = 6;
	
	public Variable output;
	Variable answer = new Variable();
	Variable input;
	Variable attack;
	Variable release;
	
	public void calc()
	{
	}	
	public AudioEnvelopeFollower2Instance(Parameters parameters)
	{				
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		attack = parameters.getParameter(1, "attack");
		release = parameters.getParameter(2, "release");
		if(attack == null) attack = DoublePart.asVariable(0.01);
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		
		
		
		AudioFallBackStream input_stream = null;
		
		boolean both_attack_release = false;
		
		double f_attack = 0;
		double f_release = 0;
		boolean input_stream_eof = false;
		boolean a_attack_eof = false;
		boolean a_release_eof = false;
		AudioStream a_attack = null;
		AudioStream a_release = null;
		
		int channels;	
		double rate;    
		AudioCache audiocache;
		
		double[] xn_1;
		
		public FilterStreamInstance(AudioSession session)
		{				
			audiocache = session.getAudioCache();
			this.channels = session.getChannels();			
			this.rate = session.getRate();
			
			xn_1 = new double[channels];
			Arrays.fill(xn_1, 0);
			
			input_stream_eof =(AudioEvents.getInstance(input).track.size() == 0);
			if(!input_stream_eof) input_stream = new AudioFallBackStream(AudioEvents.openStream(input, session));
			 
			AudioSession monosession = session.getMonoSession();
			
			f_attack = DoublePart.asDouble(attack);
			a_attack_eof =(AudioEvents.getInstance(attack).track.size() == 0);
			if(!a_attack_eof) 
				a_attack = AudioEvents.openStream(attack, monosession);
			else 
				a_attack = null;
			
			if(release != null)
			{
			
				both_attack_release = true;
				f_release = DoublePart.asDouble(release);
				a_release_eof =(AudioEvents.getInstance(release).track.size() == 0);
				if(!a_release_eof) 
					a_release = AudioEvents.openStream(release, monosession);
				else 
					a_release = null;			
			
			}
			
		}
		
		
		public int skip(int len)
		{
			if(input_stream_eof) return -1;
			int ret  = input_stream.skip(len);
			
			int clen = len / channels;
			
			if(!a_attack_eof)
			{
				int aret = a_attack.skip(clen);
				if(aret == -1)
				{
					a_attack_eof = true;
				}
			}	
			
			if(both_attack_release)
			if(!a_release_eof)
			{
				int aret = a_release.skip(clen);
				if(aret == -1)
				{
					a_release_eof = true;
				}
			}					
			
			return ret;
		}		
		public int mix(double[] buffer, int start, int end) {
			
			if(input_stream_eof) return -1;
			
			double[] stockbuffer = audiocache.getBuffer(end);
			int ret = input_stream.replace(stockbuffer, start, end);
			if(ret == -1)
			{
				input_stream_eof = true;
				audiocache.returnBuffer(stockbuffer);
				return -1;				
			}			
			end = start + ret;
			
			int cend = end / channels;
			int cstart = start / channels;
			
			double[] cbuffer_attack = null;
			double[] cbuffer_release = null;
			
			boolean variablebuffers = false;
			
		
			if(!a_attack_eof)
			{
				cbuffer_attack = audiocache.getBuffer(cend);
				int aret = a_attack.replace(cbuffer_attack, cstart, cend);
				if(aret == -1)
				{
					a_attack_eof = true;
				}
				else
				{
					variablebuffers = true;
					Arrays.fill(cbuffer_attack, cstart + aret, cend, f_attack);
				}
			}
			else
			{
				cbuffer_attack = null;
			}		
			
			if(both_attack_release)
			{
			if(!a_release_eof)
			{
				cbuffer_release = audiocache.getBuffer(cend);
				int aret = a_release.replace(cbuffer_release, cstart, cend);
				if(aret == -1)
				{
					a_release_eof = true;
				}
				else
				{
					variablebuffers = true;
					Arrays.fill(cbuffer_release, cstart + aret, cend, f_release);
				}
			}
			else
			{
				cbuffer_release = null;
			}		
			}
						
			
			if(variablebuffers)
			{				
				if(cbuffer_attack == null)
				{
					cbuffer_attack = audiocache.getBuffer(cend);
					Arrays.fill(cbuffer_attack, cstart, cend, f_attack);
				}
				
				if(both_attack_release)
				if(cbuffer_release == null)
				{
					cbuffer_release = audiocache.getBuffer(cend);
					Arrays.fill(cbuffer_release, cstart, cend, f_release);
				}				
			}
			
			
			if(both_attack_release)
			{
			
			if(variablebuffers)
			{
				double[] xbb_attack = cbuffer_attack;
				double[] xbb_release = cbuffer_release;				
				double bb_attack;
				double bb_release;
				
				int chs = channels;
				
				for (int c = 0; c < chs; c++) {
					int ix = start + c;
					
					double xn_1 = this.xn_1[c];
					for (int i = cstart; i < cend; i++) {
						
						bb_attack = Math.exp(Math.log(0.001) / (xbb_attack[i] * rate));
						bb_release = Math.exp(Math.log(0.001) / (xbb_release[i] * rate));
						
						double xn = Math.abs(stockbuffer[ix]);
						if(xn_1 < xn )
							xn_1 = xn + bb_attack * ( xn_1 - xn );
						else
							xn_1 = xn + bb_release * ( xn_1 - xn );
											
						buffer[ix] += xn_1;
						ix += chs;
					}
					
					if(xn_1 > 0.0) if(xn_1 < 1.0E-10) xn_1 = 0;
					this.xn_1[c] = xn_1;
				}
			}
			else
			{
				double bb_attack = Math.exp(Math.log(0.001) / (f_attack * rate));
				double bb_release = Math.exp(Math.log(0.001) / (f_release * rate));
				
				int chs = channels;
				
				for (int c = 0; c < chs; c++) {
					int ix = start + c;
					double xn_1 = this.xn_1[c];
					for (int i = cstart; i < cend; i++) {
						
						double xn = Math.abs(stockbuffer[ix]);
						if(xn_1 < xn )
							xn_1 = xn + bb_attack * ( xn_1 - xn );
						else
							xn_1 = xn + bb_release * ( xn_1 - xn );
											
						buffer[ix] += xn_1;
						ix += chs;
					}
					
					if(xn_1 < 0.0) if(xn_1 > -1.0E-10) xn_1 = 0;				
					this.xn_1[c] = xn_1;
					
				}			     				
				
			}
			
			}
			else
			{
				
				if(variablebuffers)
				{
					double[] xbb_attack = cbuffer_attack;
					double bb_attack;
					
					int chs = channels;
					
					for (int c = 0; c < chs; c++) {
						int ix = start + c;
						
						double xn_1 = this.xn_1[c];
						for (int i = cstart; i < cend; i++) {
							
							bb_attack = Math.exp(Math.log(0.001) / (xbb_attack[i] * rate));
							
							double xn = Math.abs(stockbuffer[ix]);
							xn_1 = xn + bb_attack * ( xn_1 - xn );
												
							buffer[ix] += xn_1;
							ix += chs;
						}
						
						if(xn_1 > 0.0) if(xn_1 < 1.0E-10) xn_1 = 0;
						this.xn_1[c] = xn_1;
					}
				}
				else
				{
					double bb_attack = Math.exp(Math.log(0.001) / (f_attack * rate));
					
					int chs = channels;
					
					for (int c = 0; c < chs; c++) {
						int ix = start + c;
						double xn_1 = this.xn_1[c];
						for (int i = cstart; i < cend; i++) {
							
							double xn = Math.abs(stockbuffer[ix]);
							xn_1 = xn + bb_attack * ( xn_1 - xn );
												
							buffer[ix] += xn_1;
							ix += chs;
						}
						
						if(xn_1 < 0.0) if(xn_1 > -1.0E-10) xn_1 = 0;				
						this.xn_1[c] = xn_1;
						
					}			     				
					
				}				
				
			}
			
			
			
			audiocache.returnBuffer(stockbuffer);
			if(cbuffer_attack != null) audiocache.returnBuffer(cbuffer_attack);
			if(cbuffer_release != null) audiocache.returnBuffer(cbuffer_release);
			
			return ret;
			
		}
		
		public int isStatic(double[] buffer, int len) {
			
			
			for (int i = 0; i < channels; i++) {
				if(xn_1[i] != 0) return -1;
			}					
			if(input_stream_eof) return -1;
			
			int ret = input_stream.isStatic(buffer, len);
			if(ret == -1) return -1;
			if(buffer[0] != 0)
			{
				input_stream.fallBack();
				return -1;
			}
			
			int cend = len / channels;
			int cstart = 0 / channels;			
			double[] cbuffer = audiocache.getBuffer(cend);
			//boolean variablebuffers = false;			

			if(!a_attack_eof)
			{
				int aret = a_attack.replace(cbuffer, cstart, cend); // Vantar skip fall � AudioStream
				if(aret == -1)
				{
					a_attack_eof = true;
				}
			}
			
			if(both_attack_release)
			if(!a_release_eof)
			{
				int aret = a_release.replace(cbuffer, cstart, cend); // Vantar skip fall � AudioStream
				if(aret == -1)
				{
					a_release_eof = true;
				}
			}			
			audiocache.returnBuffer(cbuffer);
			
			return len;
		}				
		public int replace(double[] buffer, int start, int end) {
			
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);									
		}
		public void close() {
			if(input_stream != null) input_stream.close();
						
			if(a_attack != null)
			{
				a_attack.close();
				a_attack = null;
			}			

			if(both_attack_release)
			if(a_release != null)
			{
				a_release.close();
				a_release = null;
			}			
			
		}		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioEnvelopeFollower2 implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Envelope Follower/Detector using lowpass filter");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);	
		metadata.add(1, "attack",		"attack",    	"0.01", "sec", MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "release",		"release",	   	"0.01", "sec", MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}			 
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioEnvelopeFollower2Instance(parameters);
	}
}