/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioDownMixInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	Variable channelcount;
	public void calc()
	{
	}
	
	public AudioDownMixInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		channelcount = parameters.getParameterWithDefault(1, "channels");
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		AudioStream inputstream;
		double[] stockbuffer = null;
		
		int chs;
		int readchannels;
		public FilterStreamInstance(AudioSession session)
		{
			this.chs = session.getChannels();
			readchannels = (int)DoublePart.asDouble(channelcount);
			if(readchannels == 0) readchannels = 1;
			inputstream = AudioEvents.openStream(input, session.newSession(session.getRate(), readchannels));
		}
		public int skip(int len) {			
			int ret  = inputstream.skip(readchannels * (len / chs));			
			if(ret == -1) return -1;
			return len;
		}		
		public int mix(double[] buffer, int start, int end) {
			
			int cstart = readchannels * (start / chs);
			int cend = readchannels * (end / chs);
			
			if(stockbuffer == null) stockbuffer = new double[readchannels*(buffer.length/chs)];
			else if(stockbuffer.length < readchannels*(buffer.length/chs)) stockbuffer = new double[readchannels*(buffer.length/chs)];
			
			int ret  = inputstream.replace(stockbuffer, cstart, cend);
			
			if(ret != -1)
				for (int c = 0; c < readchannels; c++) {
					int ix = start;
					int retend = cstart+ret;
					for (int i = cstart+c; i < retend; i+=readchannels) {
						buffer[ix] += stockbuffer[i];
						ix += chs;
					}
				}		
			if(ret == -1) return -1;
			return end - start;
		}
		public int replace(double[] buffer, int start, int end) {
			/*
			 *  VER�UR SKIPT �T �EGAR AudioOperator hefur veri� n�gilega pr�fa�ur
			 */
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);
		}
		public int isStatic(double[] buffer, int len) {
			return -1;
		}				
		public void close() {
			inputstream.close();			
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioDownMix implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Downmix multiple channel");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);	
		metadata.add(1, "channels",		"Channels",	    null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}		
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioDownMixInstance(parameters);
	}
}