/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.ArrayList;
import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioChannelMuxInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	Variable answer = new Variable();
	Variable[] channels;
	
	public void calc()
	{
	}
	
	public AudioChannelMuxInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		
		ArrayList vars = new ArrayList();
		/*
		 Iterator keys = parameters.keySet().iterator();
		 while (keys.hasNext()) {
		 String keyname = (String) keys.next();
		 if(!keyname.equals("0"))
		 if(!keyname.equals("output"))
		 {
		 vars.add(parameters.get(keyname));				
		 }
		 }*/
		
		int ii = 1;
		Object var = parameters.getParameter(ii);
		while(var != null)
		{
			vars.add(var);
			ii++;
			var = parameters.getParameter(ii);
		}
		
		channels = new Variable[vars.size()];
		for (int i = 0; i < channels.length; i++) {
			channels[i] = (Variable)vars.get(i);
		}
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		//double ff_1 = RFloat.asFloat(input);
		//double[] ff = new double[channels.length];
		AudioFallBackStream[] inputstreams ;
		//double[][] stockbuffers ;		
		boolean[] inputstreams_eof ;
		int totalchannels;
		int chs;
		AudioCache audiocache;
		public FilterStreamInstance(AudioSession session)
		{
			audiocache = session.getAudioCache();
			this.chs = session.getChannels();
			totalchannels = chs;
			if(totalchannels > channels.length) totalchannels = channels.length;
			
			//ff = new double[totalchannels];
			inputstreams = new AudioFallBackStream[totalchannels];
			//stockbuffers = new double[totalchannels][];		
			inputstreams_eof = new boolean[totalchannels];
			
			if(totalchannels > 0)
			{
				AudioSession monosession = session.getMonoSession();
				for (int i = 0; i < totalchannels; i++) {
					inputstreams_eof[i] = false;
					//stockbuffers[i] = null;
					inputstreams[i] = new AudioFallBackStream(AudioEvents.openStream(channels[i], monosession));
					//ff[i] = RDouble.asDouble(channels[i]);
				}	
			}
			
		}
		public int mix(double[] buffer, int start, int end) {
			
			boolean anyact = false;
			int cstart = start / chs;
			int cend = end / chs;
			double[] stockbuffer = audiocache.getBuffer(cend); //buffer.length / chs);
			for (int c = 0; c < totalchannels; c++) {
				if(!inputstreams_eof[c])
				{
					/*
					 if(stockbuffers[c] == null) stockbuffers[c] = new double[buffer.length / chs];
					 else if(stockbuffers[c].length*chs < buffer.length)
					 {
					 stockbuffers[c] = new double[buffer.length];
					 if(inputstreams_eof[c])
					 {
					 Arrays.fill(stockbuffers[c], ff[c]);
					 }
					 }			
					 
					 double[] stockbufferc = stockbuffers[c];*/
					
					//if(!inputstreams_eof[c])
					//{
					int retc = inputstreams[c].replace(stockbuffer, cstart, cend);
					if(retc == -1)
					{
						//Arrays.fill(stockbuffer, ff[c]);
						inputstreams_eof[c] = true;
					}
					else
					{
						anyact = true;
						//double fff = ff[c];
						Arrays.fill(stockbuffer, cstart + retc, cend, 0);
						/*
						 for (int i = retc; i < cend; i++) {
						 stockbufferc[i] = fff;
						 }*/
						
						int ix = cstart;
						for (int i = start + c; i < end; i+=chs) {
							buffer[i] += stockbuffer[ix];
							ix++;
						}
						
						
					}
					//}
					
					
					
					
					
					
				}
			}
			
			audiocache.returnBuffer(stockbuffer);
			
			if(!anyact) return -1;
			return end - start;
		}							
		public int replace(double[] buffer, int start, int end) {
			/*
			 *  VER�UR SKIPT �T �EGAR AudioOperator hefur veri� n�gilega pr�fa�ur
			 */
			//Arrays.fill(buffer, start, end, 0);
			//return mix(buffer, start, end);
			
			boolean anyact = false;
			int cstart = start / chs;
			int cend = end / chs;
			double[] stockbuffer = audiocache.getBuffer(cend); //buffer.length / chs);
			for (int c = 0; c < totalchannels; c++) {
				if(!inputstreams_eof[c])
				{
					/*
					 if(stockbuffers[c] == null) stockbuffers[c] = new double[buffer.length / chs];
					 else if(stockbuffers[c].length*chs < buffer.length)
					 {
					 stockbuffers[c] = new double[buffer.length];
					 if(inputstreams_eof[c])
					 {
					 Arrays.fill(stockbuffers[c], ff[c]);
					 }
					 }			
					 
					 double[] stockbufferc = stockbuffers[c];*/
					
					//if(!inputstreams_eof[c])
					//{
					int retc = inputstreams[c].replace(stockbuffer, cstart, cend);
					if(retc == -1)
					{
						//Arrays.fill(stockbuffer, ff[c]);
						inputstreams_eof[c] = true;
					}
					else
					{
						anyact = true;
						//double fff = ff[c];
						Arrays.fill(stockbuffer, cstart + retc, cend, 0);
						/*
						 for (int i = retc; i < cend; i++) {
						 stockbufferc[i] = fff;
						 }*/
						
						int ix = cstart;
						for (int i = start + c; i < end; i+=chs) {
							buffer[i] = stockbuffer[ix];
							ix++;
						}
						
						
					}
					//}
					
					
					
					
					
					
				}
			}
			
			audiocache.returnBuffer(stockbuffer);
			
			if(!anyact) return -1;
			return end - start;			
		}
		public int skip(int len) {
			
			boolean anyact = false;
//			int cstart = start / chs;
//			int cend = end / chs;
//			double[] stockbuffer = audiocache.getBuffer(cend); //buffer.length / chs);
			for (int c = 0; c < totalchannels; c++) {
				if(!inputstreams_eof[c])
				{
					/*
					 if(stockbuffers[c] == null) stockbuffers[c] = new double[buffer.length / chs];
					 else if(stockbuffers[c].length*chs < buffer.length)
					 {
					 stockbuffers[c] = new double[buffer.length];
					 if(inputstreams_eof[c])
					 {
					 Arrays.fill(stockbuffers[c], ff[c]);
					 }
					 }			
					 
					 double[] stockbufferc = stockbuffers[c];*/
					
					//if(!inputstreams_eof[c])
					//{
					anyact = true;
					int retc = inputstreams[c].skip(len / chs);
					if(retc == -1)
					{
						//Arrays.fill(stockbuffer, ff[c]);
						inputstreams_eof[c] = true;
					}
					else
					{
						//double fff = ff[c];
						//Arrays.fill(stockbuffer, cstart + retc, cend, 0);
						/*
						 for (int i = retc; i < cend; i++) {
						 stockbufferc[i] = fff;
						 }*/
						/*
						 int ix = cstart;
						 for (int i = start + c; i < end; i+=chs) {
						 buffer[i] += stockbuffer[ix];
						 ix++;
						 }*/
						
						
					}
					//}
					
					
					
					
					
					
				}
			}
			
//			audiocache.returnBuffer(stockbuffer);
			
			if(!anyact) return -1;
			return len;
		}							
		
		double[] sbuffer = new double[1];
		public int isStatic(double[] buffer, int len) {			
			//return -1;
			
			
			boolean anyact = false;
			int clen = len / chs;
			for (int c = 0; c < totalchannels; c++) {
				if(!inputstreams_eof[c])
				{
					
					int retc = inputstreams[c].isStatic(sbuffer, clen);
					if(retc == -1)
					{
						for (int i= 0; i < c; c++) {
							inputstreams[c].fallBack();
						}
						return -1;
					}
					else
					{
						if(sbuffer[0] != 0)
						{
							for (int i= 0; i <= c; c++) {
								inputstreams[c].fallBack();
							}
							return -1;						  
						}
						anyact = true;
					}
				}
			}					
			
			if(!anyact) return -1;
			sbuffer[0] = 0;
			return len;					
			
		}				
		public void close() {
			for (int i = 0; i < inputstreams.length; i++) {
				inputstreams[i].close();
			}
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioChannelMux implements UnitFactory  , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Multiple Channel Mux");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.setHasVarargs(true);
		return metadata;
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioChannelMuxInstance(parameters);
	}
}