/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import rasmus.interpreter.Variable;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioAGCInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	
	public void calc()
	{
	}
	
	public AudioAGCInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault(1, "input");
		
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		AudioStream inputstream;
		double[] stockbuffer = null;
		int channels;
		
		
		int aheadbuffer_len = 0;
		int aheadbuffer_rover = 0;
		int aheadbuffer_scaninterval = 0;
		double aheadbuffer_releasetime = 0;
		double[] aheadbuffer;
		
		double currentamp = 1.0;
		double plannedamp = 1.0;
		double amp_stepping = 0.0;
		
		public FilterStreamInstance(AudioSession session)
		{			
			this.channels = session.getChannels();
			inputstream = AudioEvents.openStream(input, session);
			
			double agc_buffer_len = 0.0160; // 16 msec
			
			aheadbuffer_len = session.getChannels() * ((int)(agc_buffer_len * session.getRate()));
			
			aheadbuffer_scaninterval = session.getChannels() * ((int)(agc_buffer_len * 0.5 * session.getRate()));
			
			aheadbuffer_releasetime = session.getChannels() * 25 * session.getRate();
			///   40 faster, and 10 slower,  5.0 is default 
			aheadbuffer_releasetime = Math.pow(2.0, 100.0 / aheadbuffer_releasetime  );
			
			
			//Math.pow((nextvalue/lastvalue), 1.0 / durs
			
			aheadbuffer = new double[aheadbuffer_len];
		}
		public int skip(int len)
		{
			int ret  = inputstream.skip(len);
			return ret;
		}				
		public int mix(double[] buffer, int start, int end) {
			
			if(stockbuffer == null) stockbuffer = new double[buffer.length];
			else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];		
			int ret  = inputstream.replace(stockbuffer, start, end);
			
			double currentamp = this.currentamp;
			end = start + ret;
			
			double aheadbuffer_releasetime = this.aheadbuffer_releasetime; 
			for (int i = start; i < end; i++) {	
				
				
				// Scan State (pos = 80 ms,  pos = 0 ms ); 
				if(aheadbuffer_rover % aheadbuffer_scaninterval == 0)
				{	
					
					double agc_max = 0;
					// Scan Max/Min in the buffer
					for (int j = 0; j < aheadbuffer_len; j++) {
						double s = Math.abs(aheadbuffer[j]);
						if(s > agc_max) agc_max = s;
					}
					agc_max *= 1.1;
					
					// Current amplification level is : currentamp
					// But what we need is least the : agc_max 
					
					if((1.0/agc_max) - plannedamp < 0)
					{
						// Make it a litle crude / until i program smoother
						plannedamp = (1.0/agc_max); 
						double ampcorrection = plannedamp - currentamp;
						amp_stepping = ampcorrection /  aheadbuffer_scaninterval;
					}
					
					if(agc_max < 1.0) agc_max = 1.0;
					if((1.0/agc_max) - plannedamp > 0.2)					
					{
						// Make it a litle crude / until i program smoother
						plannedamp = (1.0/agc_max);
						//double ampcorrection = 0.01; //plannedamp - currentamp;
						//amp_stepping = ampcorrection / aheadbuffer_releasetime;
						amp_stepping = 1.0;
					}					 
					
					
				}
				
				if(amp_stepping != 0)
				{
					if(amp_stepping > 0)
					{
						currentamp *= aheadbuffer_releasetime;
						/*
						 currentamp += amp_stepping;
						 */
						if(currentamp > plannedamp)
						{
							currentamp = plannedamp;
							amp_stepping = 0;
						}
					}
					else
					{
						currentamp += amp_stepping;
						if(currentamp < plannedamp)
						{
							currentamp = plannedamp;
							amp_stepping = 0;
						}						 
					}
				}
				
				// Delay buffer 160 ms in length
				
				
				if(aheadbuffer_rover == aheadbuffer_len) aheadbuffer_rover = 0;
				double b = aheadbuffer[aheadbuffer_rover]*currentamp;
				aheadbuffer[aheadbuffer_rover] = stockbuffer[i];
				aheadbuffer_rover++;
				if(b > 1.0) 
					b = 1.0;
				else if(b < -1.0) 
					b = -1.0;
				buffer[i] += b;
				
			}			
			this.currentamp = currentamp;
			return ret;
		}
		public int replace(double[] buffer, int start, int end) {
			if(stockbuffer == null) stockbuffer = new double[buffer.length];
			else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];		
			int ret  = inputstream.replace(stockbuffer, start, end);
			
			double currentamp = this.currentamp;
			end = start + ret;
			
			double aheadbuffer_releasetime = this.aheadbuffer_releasetime; 
			for (int i = start; i < end; i++) {	
				
				
				// Scan State (pos = 80 ms,  pos = 0 ms ); 
				if(aheadbuffer_rover % aheadbuffer_scaninterval == 0)
				{	
					
					double agc_max = 0;
					// Scan Max/Min in the buffer
					for (int j = 0; j < aheadbuffer_len; j++) {
						double s = Math.abs(aheadbuffer[j]);
						if(s > agc_max) agc_max = s;
					}
					agc_max *= 1.1;
					
					// Current amplification level is : currentamp
					// But what we need is least the : agc_max 
					
					if((1.0/agc_max) - plannedamp < 0)
					{
						// Make it a litle crude / until i program smoother
						plannedamp = (1.0/agc_max); 
						double ampcorrection = plannedamp - currentamp;
						amp_stepping = ampcorrection /  aheadbuffer_scaninterval;
					}
					
					if(agc_max < 1.0) agc_max = 1.0;
					if((1.0/agc_max) - plannedamp > 0.2)					
					{
						// Make it a litle crude / until i program smoother
						plannedamp = (1.0/agc_max);
						//double ampcorrection = 0.01; //plannedamp - currentamp;
						//amp_stepping = ampcorrection / aheadbuffer_releasetime;
						amp_stepping = 1.0;
					}					 
					
					
				}
				
				if(amp_stepping != 0)
				{
					if(amp_stepping > 0)
					{
						currentamp *= aheadbuffer_releasetime;
						/*
						 currentamp += amp_stepping;
						 */
						if(currentamp > plannedamp)
						{
							currentamp = plannedamp;
							amp_stepping = 0;
						}
					}
					else
					{
						currentamp += amp_stepping;
						if(currentamp < plannedamp)
						{
							currentamp = plannedamp;
							amp_stepping = 0;
						}						 
					}
				}
				
				// Delay buffer 160 ms in length
				
				
				if(aheadbuffer_rover == aheadbuffer_len) aheadbuffer_rover = 0;
				double b = aheadbuffer[aheadbuffer_rover]*currentamp;
				aheadbuffer[aheadbuffer_rover] = stockbuffer[i];
				aheadbuffer_rover++;
				if(b > 1.0) 
					b = 1.0;
				else if(b < -1.0) 
					b = -1.0;
				buffer[i] = b;
				
			}			
			this.currentamp = currentamp;
			return ret;
		}
		public int isStatic(double[] buffer, int len) {
			return -1;
		}				
		public void close() {
			inputstream.close();
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioAGC implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Auto Gain Control");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "input",		"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioAGCInstance(parameters);
	}
}