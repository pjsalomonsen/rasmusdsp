/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioEnvelopeFollowerInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public final static int PNUM = 6;
	
	public Variable output;
	Variable answer = new Variable();
	Variable input;
	Variable window;
	
	public void calc()
	{
	}	
	public AudioEnvelopeFollowerInstance(Parameters parameters)
	{				
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		window = parameters.getParameter(1, "window");
		if(window == null) window = DoublePart.asVariable(0.01);
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		
		
		
		AudioFallBackStream input_stream = null;
		
		boolean both_attack_release = true;
				
		boolean input_stream_eof = false;
		
		int channels;	
		double rate;    
		AudioCache audiocache;
		
		int windowlen = 0;
		int windowpos = 0;
		double[][] window_runner;
		double[] window_total;
		
		double window_factor;
		
		public FilterStreamInstance(AudioSession session)
		{				
			audiocache = session.getAudioCache();
			this.channels = session.getChannels();			
			this.rate = session.getRate();
						
			input_stream_eof =(AudioEvents.getInstance(input).track.size() == 0);
			if(!input_stream_eof) input_stream = new AudioFallBackStream(AudioEvents.openStream(input, session));
			 
			windowlen = (int)(DoublePart.asDouble(window) * rate);
			
			window_runner = new double[channels][];
			window_total = new double[channels];
			for (int i = 0; i < channels; i++) {
				window_runner[i] = new double[windowlen];
				window_total[i] = 0;
			}
			
			window_factor = 1.0 / (double)windowlen;

		}
		
		
		public int skip(int len)
		{
			if(input_stream_eof) return -1;
			int ret  = input_stream.skip(len);
						
			return ret;
		}		
		public int mix(double[] buffer, int start, int end) {
			
			if(input_stream_eof) return -1;
			
			double[] stockbuffer = audiocache.getBuffer(end);
			int ret = input_stream.replace(stockbuffer, start, end);
			if(ret == -1)
			{
				input_stream_eof = true;
				audiocache.returnBuffer(stockbuffer);
				return -1;				
			}			
			end = start + ret;
			
			int cend = end / channels;
			int cstart = start / channels;
			
				
			int chs = channels;
									
			for (int c = 0; c < chs; c++) {
					int ix = start + c;
					double[] window_runner = this.window_runner[c];
					double window_total = this.window_total[c];
					int windowpos = this.windowpos;
					
					for (int i = cstart; i < cend; i++) {
						double xn = Math.abs(stockbuffer[ix]);
						double last = window_runner[windowpos];
						window_runner[windowpos] = xn;
						window_total = window_total - last + xn;						
						double window_avg = window_total * window_factor;
						buffer[ix] += window_avg;
						ix += chs;															
						windowpos = (windowpos + 1) % windowlen;
					}
					
					 this.window_total[c] = window_total;
										
			}			     		
			
			this.windowpos += cend - cstart;
			this.windowpos = this.windowpos % windowlen;
			
			audiocache.returnBuffer(stockbuffer);
			
			return ret;
			
		}
		
		public int isStatic(double[] buffer, int len) {
			
			
			for (int i = 0; i < channels; i++) {
				if(window_total[i] != 0) return -1;
			}					
			if(input_stream_eof) return -1;
			
			int ret = input_stream.isStatic(buffer, len);
			if(ret == -1) return -1;
			if(buffer[0] != 0)
			{
				input_stream.fallBack();
				return -1;
			}			
			
			return len;
		}				
		public int replace(double[] buffer, int start, int end) {
			
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);									
		}
		public void close() {
			if(input_stream != null) input_stream.close();				
		}		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioEnvelopeFollower implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Envelope Follower/Detector using running sum");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);	
		metadata.add(1, "window",		"window",    	"0.01", "sec", MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}			 
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioEnvelopeFollowerInstance(parameters);
	}
}