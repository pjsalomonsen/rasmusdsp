/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.sampled.util.DeHiss;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioDeHissInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	Variable fftFrameSize;
	Variable osamp;
	
	Variable floor;
	Variable width;
	Variable reduce;
	Variable sumsmooth;
	
	public void calc()
	{
	}
	
	public AudioDeHissInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		
		floor = parameters.getParameter(1, "floor");
		width = parameters.getParameter(2, "width");
		reduce = parameters.getParameter(3, "reduce");	
		sumsmooth = parameters.getParameter(4, "window");
		
		fftFrameSize = parameters.getParameterWithDefault(5, "fftFrameSize");
		osamp = parameters.getParameterWithDefault(6, "osamp"); 
		/*
		 RFloat.getInstance(gain).addListener(this);
		 calc();*/
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		//	float f_gain = RFloat.asFloat(gain);
		AudioFallBackStream inputstream;
		//double[] stockbuffer = null;
		DeHiss[] pitchshift;
		int channels;
		AudioCache audiocache;
		AudioSession session;
		
		
		public FilterStreamInstance(AudioStream inputstream, AudioSession session)
		{			
			this.session = session;
			audiocache = session.getAudioCache();
			this.channels = session.getChannels();
			
			int i_fftFrameSize = (int)DoublePart.asDouble(fftFrameSize);
			int i_osamp = (int)DoublePart.asDouble(osamp);
			
			if(i_fftFrameSize == 0) i_fftFrameSize = 4096;
			if(i_osamp == 0) i_osamp = 6;
			
			int sumsmooth = 2;
			if(AudioDeHissInstance.this.sumsmooth != null) sumsmooth = (int)DoublePart.asDouble(AudioDeHissInstance.this.sumsmooth);
			
			pitchshift = new DeHiss[channels];
			for (int i = 0; i < channels; i++) {
				pitchshift[i] = new DeHiss(i_fftFrameSize, i_osamp, session.getRate(), sumsmooth);
				if(floor != null) { pitchshift[i].floor = DoublePart.asDouble(floor); }
				if(width != null) { pitchshift[i].width = DoublePart.asDouble(width); }
				if(reduce != null) { pitchshift[i].reduce = DoublePart.asDouble(reduce); }
				
			}
			
			this.inputstream = new AudioFallBackStream(inputstream);
			
			
		}
		
		
		public int mix(double[] buffer, int start, int end) {
			double[] stockbuffer = audiocache.getBuffer(end);
			
			int ret = replace(stockbuffer, start, end);
			int cret = ret; if(cret == -1) cret = 0;
			for (int i = start; i < (start + cret); i++) {
				buffer[i] += stockbuffer[i];
			}			
			audiocache.returnBuffer(stockbuffer);
			return ret;
		}
		
		public int skip(int len)
		{
			int ret  = inputstream.skip(len);
			return ret;
		}		
		
		public int replace(double[] buffer, int start, int end) {
			/*
			 if(stockbuffer == null) stockbuffer = new double[buffer.length];
			 else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];		
			 
			 *										*/
			
			double[] stockbuffer2 = null;
			double[] stockbuffer3 = null;
			int ret = inputstream.replace(buffer, start, end);
			
			
			//ret = end - start;
			//end = start + ret;
			int cret = ret; if(cret == -1) cret = 0;
			/*
			 for (int i = start+cret; i < end; i++) {
			 stockbuffer[i] = 0;
			 }			*/
			
			
			Arrays.fill(buffer, start+cret, end, 0);
			
			
			for (int i = 0; i < channels; i++) {
				pitchshift[i].process(start + i, end, channels, buffer, buffer);
			}
			
			if(stockbuffer2 != null) audiocache.returnBuffer(stockbuffer2);
			if(stockbuffer3 != null) audiocache.returnBuffer(stockbuffer3);
			
			if(ret == -1)
			{
				for (int i = start; i < end; i++) {
					if(buffer[i] > 1.0E-10) return end - start;
				}
				return -1;
			}
			
			
			
			
			return ret; //end - start;
		}
		public int isStatic(double[] buffer, int len) {
			
			return -1; //inputstream.isStatic(buffer, len);
		}				
		public void close() {
			inputstream.close();
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(AudioEvents.openStream(input, session), session);
	}
	
}

public class AudioDeHiss implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Hiss Reduction");
		metadata.add(-1, "output",			"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "floor",			"Floor",		"0.1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2,  "width",			"Width",		"1.001", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3,  "reduce",			"Reduce",		"0.001", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4,  "window",			"Window",		"2", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(5,  "fftFrameSize",	"FFT size",		"4096", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(6,  "osamp",			"FFT Overlap",  "6", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",			"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioDeHissInstance(parameters);
	}
}