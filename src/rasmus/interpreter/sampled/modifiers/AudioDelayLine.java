/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioDelayLineInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	
	public Variable delay;	
	public Variable feedback;	
	public Variable maxdelay;
	public Variable damp;
	
	public void calc()
	{
	}
	
	public AudioDelayLineInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		delay = parameters.getParameterWithDefault(1, "delay");
		feedback = parameters.getParameterWithDefault(2, "feedback");
		maxdelay = parameters.getParameter(3, "maxdelay");
		damp = parameters.getParameter(4, "damp");
		if(maxdelay == null) maxdelay = delay;
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		AudioStream inputstream;
		boolean inputstream_eof = false;
		double[] stockbuffer = null;
		int channels;
		
		double[][] delaybuffer;
		int delaybuffer_rover = 0;
		int delaybuffer_len = 0;
		double d_maxdelay = 0;
		double rate;
		double[] filterstore;
		
		boolean inputstream2_eof = false;
		AudioStream inputstream2;
		AudioCache audiocache;
		
		boolean damp_active = false;
		double damp1 = 0;
		double damp2 = 0;
		
		public FilterStreamInstance(AudioSession session)
		{			
			audiocache = session.getAudioCache();
			this.channels = session.getChannels();
			inputstream = AudioEvents.openStream(input, session);
			rate = session.getRate();
			d_maxdelay = DoublePart.asDouble(maxdelay);
			if(d_maxdelay < DoublePart.asDouble(delay)) d_maxdelay = DoublePart.asDouble(delay); 
			delaybuffer_len = ((int)(d_maxdelay * rate)) + 1;
			d_maxdelay = ((double)delaybuffer_len) / ((double)rate);
			delaybuffer = new double[channels][];
			
			for (int c = 0; c < channels; c++) {
				delaybuffer[c] = audiocache.getBuffer(delaybuffer_len+1);
				Arrays.fill(delaybuffer[c], 0, delaybuffer_len+1, 0);
			}
			
			
			
			if(AudioEvents.getInstance(delay).track.size() == 0)
			{
				inputstream2_eof = true;
			}
			else
				inputstream2 = AudioEvents.openStream(delay, session.getMonoSession());
			
			
			if( damp != null)
			{
				double cutoff = DoublePart.asDouble( damp );
				if(cutoff > 0.0 )
				{
					/*
					 One pole LP and HP
					 References : Posted by Bram
					 http://www.musicdsp.org/showone.php?id=116
					 */
					
					double x = 2*Math.PI*cutoff/session.getRate();
					double p = (2-Math.cos(x)) - Math.sqrt(Math.pow(2-Math.cos(x),2) - 1);					
					// APRX: p = (1 - 2*cutoff/samplerate)^2
					
					damp1 = p;
					damp2 = 1 - p;					
					damp_active = true;
					
				}
			}
			
			filterstore = new double[channels];
			
		}
		public int skip(int len)
		{
			int ret  = inputstream.skip(len);
			return ret;
		}				
		public int mix(double[] buffer, int start, int end) {
			
			//undenormalise(filterstore);
			if(damp_active)
			{
				for (int i = 0; i < filterstore.length; i++) {
					if(filterstore[i] > 0.0) if(filterstore[i] < 1.0E-10) filterstore[i] = 0;
					if(filterstore[i] < 0.0) if(filterstore[i] > -1.0E-10) filterstore[i] = 0;					
				}
			}
			
			
			if(stockbuffer == null)
			{
				stockbuffer = audiocache.getBuffer(buffer.length);
			}
			else if(stockbuffer.length < buffer.length)
			{
				audiocache.returnBuffer(stockbuffer);
				stockbuffer = audiocache.getBuffer(buffer.length);
			}
			
			if(!inputstream_eof)
			{
				int ret  = inputstream.replace(stockbuffer, start, end);
				if(ret == -1)
				{
					inputstream_eof = true;
					ret = 0;
				}
				for (int i = start+ret; i < end; i++) {
					stockbuffer[i] = 0;
				}
				
			}
			
			double d_delay = DoublePart.asDouble(delay);
			
			
			double[] stockbuffer2 = null;
			int cstart = start / channels;
			int cend = end / channels;
			if(!inputstream2_eof)
			{
				stockbuffer2 = new double[1];
				if(inputstream2.isStatic(stockbuffer2, cend - cstart) == -1)
				{								
					stockbuffer2 = audiocache.getBuffer(cend);
					int ret2 = inputstream2.replace(stockbuffer2, cstart, cend);
					if(ret2 == -1)
					{
						inputstream2_eof = true;
						audiocache.returnBuffer(stockbuffer2);
						stockbuffer2 = null;
					}
					else
					{
						Arrays.fill(stockbuffer2, cstart + ret2, cend, DoublePart.asDouble(delay));
					}
				}
				else
				{
					d_delay = stockbuffer2[0];
					stockbuffer2 = null;
				}
				
			}
			
			
			
			double d_feedback = DoublePart.asDouble(feedback);
			
			int delaybuffer_rover = 0;
			int chs = channels;
			
			if(stockbuffer2 != null)
			{		
				delaybuffer_rover = this.delaybuffer_rover;
				double bufflen = d_maxdelay*rate;
				for (int i = cstart; i < cend; i++) {
					stockbuffer2[i] = ((delaybuffer_rover + bufflen - stockbuffer2[i]*rate) + delaybuffer_len) % delaybuffer_len;
					delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
					
					//stockbuffer2[i] = delaybuffer_rover + bufflen - stockbuffer2[i]*rate;
					//delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
					//delaybuffer_rover = (((delaybuffer_rover + 1) % delaybuffer_len) + delaybuffer_len) % delaybuffer_len;
					
				}
			}	
			
			double damp1 = this.damp1;
			double damp2 = this.damp2;
			
			for (int c = 0; c < channels; c++) {
				
				delaybuffer_rover = this.delaybuffer_rover;
				
				double filterstore = this.filterstore[c];
				
				double[] delaybuffer = this.delaybuffer[c];
				if(stockbuffer2 != null)
				{
					int cx = cstart;
					
					try
					{
						
						
						if(damp_active)
							for (int i = start + c; i < end; i+=chs) {
								double delaybuffer_readrover = stockbuffer2[cx];
								int ix = ((int)delaybuffer_readrover);
								double s = delaybuffer_readrover - ix;
								ix = ix % delaybuffer_len;
								double lastb = delaybuffer[ix];
								ix = (ix + 1) % delaybuffer_len;
								double curb = delaybuffer[ix];
								double b = lastb *(1 - s) + curb*(s);
								buffer[i] += b;	
								filterstore = (b*damp2) + (filterstore*damp1);
								b = filterstore;							
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;						
								cx++;
							}
						else
							for (int i = start + c; i < end; i+=chs) {
								double delaybuffer_readrover = stockbuffer2[cx];
								int ix = ((int)delaybuffer_readrover);
								double s = delaybuffer_readrover - ix;
								ix = ix % delaybuffer_len;
								double lastb = delaybuffer[ix];
								ix = (ix + 1) % delaybuffer_len;
								double curb = delaybuffer[ix];
								double b = lastb *(1 - s) + curb*(s);
								buffer[i] += b;	
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;						
								cx++;
							}
						
						
					}
					catch(ArrayIndexOutOfBoundsException e)
					{
						e.printStackTrace();					
					}
				}
				else
				{
					double delaybuffer_readrover = this.delaybuffer_rover + ((d_maxdelay - d_delay)*rate);
					double s = delaybuffer_readrover - Math.floor(delaybuffer_readrover);
					int ix = (int)delaybuffer_readrover;
					while(ix < 0) ix += delaybuffer_len;
					while(ix >= delaybuffer_len) ix -= delaybuffer_len;
					if(s < 1.0E-10)
					{
						if(damp_active)
							for (int i = start + c; i < end; i+=chs) {
								double b = delaybuffer[ix];								
								buffer[i] += b;				 
								filterstore = (b*damp2) + (filterstore*damp1);
								b = filterstore;
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
								ix = (ix + 1) % delaybuffer_len;
							}						
						else
							for (int i = start + c; i < end; i+=chs) {
								double b = delaybuffer[ix];
								buffer[i] += b;				 
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
								ix = (ix + 1) % delaybuffer_len;
							}						
					}
					else
					{
						
						double lastb = delaybuffer[ix];					
						ix++;
						if(ix == delaybuffer_len) ix = 0;
						
						if(damp_active)
							for (int i = start + c; i < end; i+=chs) {
								double curb = delaybuffer[ix];
								double b = lastb*(1 - s) + curb*(s);
								lastb = curb;								
								buffer[i] += b;	
								
								filterstore = (b*damp2) + (filterstore*damp1);
								b = filterstore;
								
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
								ix = (ix + 1) % delaybuffer_len;
							}
						else
							for (int i = start + c; i < end; i+=chs) {
								double curb = delaybuffer[ix];
								double b = lastb*(1 - s) + curb*(s);
								lastb = curb;								
								buffer[i] += b;				 
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
								ix = (ix + 1) % delaybuffer_len;
							}
					}
				}
				
				this.filterstore[c] = filterstore;
			}
			this.delaybuffer_rover = delaybuffer_rover;
			
			if(stockbuffer2 != null) audiocache.returnBuffer(stockbuffer2);
			
			// End of stream detection
			if(inputstream_eof)
			{
				for (int c = 0; c < channels; c++) 
					for (int i = 0; i < delaybuffer_len; i++) 
						if((delaybuffer[c][i] > 1.0E-10) || (delaybuffer[c][i] < -1.0E-10))
							return end - start;
				return -1;
			}
			else			
				return end - start;
		}
		public int replace(double[] buffer, int start, int end) {
			//Arrays.fill(buffer, start, end, 0);
			//return mix(buffer, start, end);
			
			
			//undenormalise(filterstore);
			if(damp_active)
			{
				for (int i = 0; i < filterstore.length; i++) {
					if(filterstore[i] > 0.0) if(filterstore[i] < 1.0E-10) filterstore[i] = 0;
					if(filterstore[i] < 0.0) if(filterstore[i] > -1.0E-10) filterstore[i] = 0;					
				}
			}
			
			
			if(stockbuffer == null)
			{
				stockbuffer = audiocache.getBuffer(buffer.length);
			}
			else if(stockbuffer.length < buffer.length)
			{
				audiocache.returnBuffer(stockbuffer);
				stockbuffer = audiocache.getBuffer(buffer.length);
			}
			
			if(!inputstream_eof)
			{
				int ret  = inputstream.replace(stockbuffer, start, end);
				if(ret == -1)
				{
					inputstream_eof = true;
					ret = 0;
				}
				for (int i = start+ret; i < end; i++) {
					stockbuffer[i] = 0;
				}
				
			}
			
			double d_delay = DoublePart.asDouble(delay);
			
			
			double[] stockbuffer2 = null;
			int cstart = start / channels;
			int cend = end / channels;
			if(!inputstream2_eof)
			{
				stockbuffer2 = new double[1];
				if(inputstream2.isStatic(stockbuffer2, cend - cstart) == -1)
				{								
					stockbuffer2 = audiocache.getBuffer(cend);
					int ret2 = inputstream2.replace(stockbuffer2, cstart, cend);
					if(ret2 == -1)
					{
						inputstream2_eof = true;
						audiocache.returnBuffer(stockbuffer2);
						stockbuffer2 = null;
					}
					else
					{
						Arrays.fill(stockbuffer2, cstart + ret2, cend, DoublePart.asDouble(delay));
					}
				}
				else
				{
					d_delay = stockbuffer2[0];
					stockbuffer2 = null;
				}
				
			}
			
			
			
			double d_feedback = DoublePart.asDouble(feedback);
			
			int delaybuffer_rover = 0;
			int chs = channels;
			
			if(stockbuffer2 != null)
			{		
				delaybuffer_rover = this.delaybuffer_rover;
				double bufflen = d_maxdelay*rate;
				for (int i = cstart; i < cend; i++) {
					stockbuffer2[i] = ((delaybuffer_rover + bufflen - stockbuffer2[i]*rate) + delaybuffer_len) % delaybuffer_len;
					delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
					//delaybuffer_rover = (((delaybuffer_rover + 1) % delaybuffer_len) + delaybuffer_len) % delaybuffer_len;
				}
			}	
			
			double damp1 = this.damp1;
			double damp2 = this.damp2;
			
			for (int c = 0; c < channels; c++) {
				
				delaybuffer_rover = this.delaybuffer_rover;
				
				double filterstore = this.filterstore[c];
				
				double[] delaybuffer = this.delaybuffer[c];
				if(stockbuffer2 != null)
				{
					int cx = cstart;
					
					try
					{
						
						
						if(damp_active)
							for (int i = start + c; i < end; i+=chs) {
								double delaybuffer_readrover = stockbuffer2[cx];
								int ix = ((int)delaybuffer_readrover);
								double s = delaybuffer_readrover - ix;
								//ix = ix % delaybuffer_len;
								double lastb = delaybuffer[ix];
								ix = (ix + 1) % delaybuffer_len;
								double curb = delaybuffer[ix];
								double b = lastb *(1 - s) + curb*(s);
								buffer[i] = b;	
								filterstore = (b*damp2) + (filterstore*damp1);
								b = filterstore;							
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;						
								cx++;
							}
						else
							for (int i = start + c; i < end; i+=chs) {
								double delaybuffer_readrover = stockbuffer2[cx];
								int ix = ((int)delaybuffer_readrover);
								double s = delaybuffer_readrover - ix;
								//ix = ix % delaybuffer_len;
								double lastb = delaybuffer[ix];
								ix = (ix + 1) % delaybuffer_len;
								double curb = delaybuffer[ix];
								double b = lastb *(1 - s) + curb*(s);
								buffer[i] = b;	
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;						
								cx++;
							}
						
						
					}
					catch(ArrayIndexOutOfBoundsException e)
					{
						e.printStackTrace();					
					}
				}
				else
				{
					double delaybuffer_readrover = this.delaybuffer_rover + ((d_maxdelay - d_delay)*rate);
					double s = delaybuffer_readrover - Math.floor(delaybuffer_readrover);
					int ix = (int)delaybuffer_readrover;
					while(ix < 0) ix += delaybuffer_len;
					while(ix >= delaybuffer_len) ix -= delaybuffer_len;
					if(s < 1.0E-10)
					{
						if(damp_active)
							for (int i = start + c; i < end; i+=chs) {
								double b = delaybuffer[ix];								
								buffer[i] = b;				 
								filterstore = (b*damp2) + (filterstore*damp1);
								b = filterstore;
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
								ix = (ix + 1) % delaybuffer_len;
							}						
						else
							for (int i = start + c; i < end; i+=chs) {
								double b = delaybuffer[ix];
								buffer[i] = b;				 
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
								ix = (ix + 1) % delaybuffer_len;
							}						
					}
					else
					{
						
						double lastb = delaybuffer[ix];					
						ix++;
						if(ix == delaybuffer_len) ix = 0;
						
						if(damp_active)
							for (int i = start + c; i < end; i+=chs) {
								double curb = delaybuffer[ix];
								double b = lastb*(1 - s) + curb*(s);
								lastb = curb;								
								buffer[i] = b;	
								
								filterstore = (b*damp2) + (filterstore*damp1);
								b = filterstore;
								
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
								ix = (ix + 1) % delaybuffer_len;
							}
						else
							for (int i = start + c; i < end; i+=chs) {
								double curb = delaybuffer[ix];
								double b = lastb*(1 - s) + curb*(s);
								lastb = curb;								
								buffer[i] = b;				 
								delaybuffer[delaybuffer_rover] = stockbuffer[i] + b*d_feedback;			
								delaybuffer_rover = (delaybuffer_rover + 1) % delaybuffer_len;
								ix = (ix + 1) % delaybuffer_len;
							}
					}
				}
				
				this.filterstore[c] = filterstore;
			}
			this.delaybuffer_rover = delaybuffer_rover;
			
			if(stockbuffer2 != null) audiocache.returnBuffer(stockbuffer2);
			
			// End of stream detection
			if(inputstream_eof)
			{
				for (int c = 0; c < channels; c++) 
					for (int i = 0; i < delaybuffer_len; i++) 
						if((delaybuffer[c][i] > 1.0E-10) || (delaybuffer[c][i] < -1.0E-10))
							return end - start;
				return -1;
			}
			else			
				return end - start;			
			
		}
		public int isStatic(double[] buffer, int len) {
			return -1;
		}				
		public void close() {
			
			
			if(stockbuffer != null)
			{
				audiocache.returnBuffer(stockbuffer);
				stockbuffer = null;
			}
			for (int c = 0; c < channels; c++) {
				audiocache.returnBuffer(delaybuffer[c]); 
			}			
			
			inputstream.close();
			if(inputstream2 != null) inputstream2.close();
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioDelayLine implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Variable Delay Line");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1, "delay",		"Delay",	    null, "sec", MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "feedback",		"Feedback",	    null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "maxdelay",		"Max delay",	null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "damp",		    "Damping",	    null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}		
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioDelayLineInstance(parameters);
	}
}