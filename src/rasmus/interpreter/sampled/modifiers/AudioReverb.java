/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.modifiers;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.sampled.util.Freeverb;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioReverbInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
//	RVariable gain;
	Variable dry = null;
	Variable wet = null;
	Variable width = null;
	Variable roomsize = null;
	Variable damp = null;
	Variable scale = null;    
	
	public void calc()
	{
	}
	
	public AudioReverbInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		
		dry = parameters.getParameter(1, "dry");
		wet = parameters.getParameter(2, "wet");
		width = parameters.getParameter(3, "width");
		roomsize = parameters.getParameter(4, "roomsize");
		damp = parameters.getParameter(5, "damp");
		scale = parameters.getParameter(6, "scale");
		
		//gain = parameters.getParameter(2, "gain");
		/*
		 RFloat.getInstance(gain).addListener(this);
		 calc();*/
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		//	float f_gain = RFloat.asFloat(gain);
		AudioFallBackStream inputstream;
		//double[] stockbuffer = null;
		Freeverb freeverb;
		int channels;
		AudioCache audiocache;
		
		public FilterStreamInstance(AudioStream inputstream, AudioSession session)
		{			
			audiocache = session.getAudioCache();
			this.channels = session.getChannels();
			
			float scale = 1;
			if(AudioReverbInstance.this.scale != null) scale = (float)DoublePart.asDouble(AudioReverbInstance.this.scale);
			freeverb = new Freeverb(session.getRate(), scale);
			freeverb.setdry(0);
			freeverb.setroomsize(0.9f);				
			freeverb.setdamp(0.9f);			
			
			if(dry != null) freeverb.setdry((float)DoublePart.asDouble(dry));
			if(wet != null) freeverb.setwet((float)DoublePart.asDouble(wet));
			if(width != null) freeverb.setwidth((float)DoublePart.asDouble(width));
			if(roomsize != null) freeverb.setroomsize((float)DoublePart.asDouble(roomsize));
			if(damp != null) freeverb.setdamp((float)DoublePart.asDouble(damp));
			
			this.inputstream = new AudioFallBackStream(inputstream);
		}
		
		
		public int mix(double[] buffer, int start, int end) {
			/*		
			 if(stockbuffer == null) stockbuffer = new double[buffer.length];
			 else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];
			 */
			double[] stockbuffer = audiocache.getBuffer(end);
			
			
			int ret = replace(stockbuffer, start, end);
			int cret = ret; if(cret == -1) cret = 0;
			for (int i = start; i < (start + cret); i++) {
				buffer[i] += stockbuffer[i];
			}
			
			audiocache.returnBuffer(stockbuffer);
			return ret;
			/*
			 //ret = end - start;
			  //end = start + ret;
			   int cret = ret; if(cret == -1) cret = 0;
			   
			   Arrays.fill(stockbuffer, start+cret, end, 0);
			   
			   freeverb.processMix(stockbuffer, buffer, start, end, channels);
			   
			   if(ret == -1)
			   {
			   for (int i = start; i < end; i++) {
			   if(buffer[i] > 1.0E-10) return end - start;
			   }
			   return -1;
			   }
			   
			   return end - start;*/
		}
		
		public int skip(int len)
		{
			int ret  = inputstream.skip(len);
			return ret;
		}		
		
		public int replace(double[] buffer, int start, int end) {
			/*
			 if(stockbuffer == null) stockbuffer = new double[buffer.length];
			 else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];		
			 
			 *										*/
			
			
			int ret = inputstream.replace(buffer, start, end);
			
			
			//ret = end - start;
			//end = start + ret;
			int cret = ret; if(cret == -1) cret = 0;
			/*
			 for (int i = start+cret; i < end; i++) {
			 stockbuffer[i] = 0;
			 }			*/
			
			
			Arrays.fill(buffer, start+cret, end, 0);
			
			freeverb.processReplace(buffer, buffer, start, end, channels);
			
			if(ret == -1)
			{
				for (int i = start; i < end; i++) {
					if(buffer[i] > 1.0E-10) return end - start;
				}
				return -1;
			}
			
			return end - start;
		}
		public int isStatic(double[] buffer, int len) {
			
			int ret = inputstream.isStatic(buffer, len);
			if(ret != -1) 
				if(freeverb.isSilent()) 
					return ret; 
				else 
					inputstream.fallBack();
			return -1;
		}				
		public void close() {
			inputstream.close();
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(AudioEvents.openStream(input, session), session);
	}
	
}

public class AudioReverb implements UnitFactory  , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Reverberation Effect");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1, "dry",			"Dry level",   	"0", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "wet",			"Wet level",   	"0.3333333", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "width",		"Width",   	    "1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "roomsize",		"Room size",   	"0.9", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(5, "damp",			"Damping",   	"0.9", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(6, "scale",		"Scale",   	    "1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioReverbInstance(parameters);
	}
}