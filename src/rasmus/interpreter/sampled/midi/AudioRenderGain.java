/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.midi;

import java.util.Arrays;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

import rasmus.interpreter.Variable;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.sampled.BeatToTimeMapper;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioGainInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	
	public void calc()
	{
	}
	
	public AudioGainInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	class FilterStreamInstance implements AudioStream, Receiver
	{
		
		Sequence iseq;
		//double[] stockbuffer = null;
		long position = 0;
		Track track;
		MidiEvent midievent = null;
		int trackpos = 0;
		double beatfactor = 1.0 / MidiSequence.DEFAULT_RES;
		BeatToTimeMapper bmap;
		long nexttime = 0;
		
		double rate;
		int channels; 
		boolean realtime;
		double max_step_size = 1;
		
		//int midi_defaultvalue = 0; //(int)RDouble.asDouble(defaultvalue);
		
		double pan = 0.5;
		double balance = 0.5;
		double volume = 1.0;
		double expression = 1.0;
		double reverb = 0.5;
		double chorus = 0.0;
		
		double[] current_value;
		double[] target_value;
		MidiSequence midiseq = null;
		AudioSession session;
		AudioCache audiocache;
		
		public void calcValues()
		{
			int chs = current_value.length;
			
			double balanceL;  
			double balanceR; 
			if(balance > 0.5)
			{
				balanceL = 1 - 2 * (balance - 0.5);
				balanceR = 1;
			}
			else
			{
				balanceL = 1;
				balanceR = 2 * balance;
			}
			
			
			if(chs > 0) target_value[0] = volume * expression * Math.cos(pan * Math.PI * 0.5) * balanceL;
			if(chs > 1) target_value[1] = volume * expression * Math.sin(pan * Math.PI * 0.5) * balanceR;
			if(chs > 2) target_value[2] = reverb;
			if(chs > 3) target_value[2] = chorus;
			
		}
		
		public FilterStreamInstance(AudioSession session)
		{
			audiocache = session.getAudioCache();
			
			
			this.session = session;
			this.bmap = session.getBeatToTimeMap();
			this.rate = session.getRate();
			this.channels = session.getChannels();
			this.realtime = session.isRealTime();
			
			current_value = new double[session.getChannels()];
			target_value = new double[session.getChannels()];
			{
				for (int i = 0; i < current_value.length; i++) {
					current_value[i] = 0;
					target_value[i] = 0;
				}
				calcValues();
			}
			
			max_step_size = 200 / (channels * rate); // �.s. 200000 breytingar/sec
			
			
			if(realtime)
			{
				midiseq = MidiSequence.getInstance(input);
				midiseq.addReceiver(this);
			}
			else
			{
				
				iseq = MidiSequence.asSequence(input);
				track = iseq.getTracks()[0];
				if(track.size() != 0)
				{
					midievent = track.get(0);
					nexttime = AudioEvents.TimeToStreamTime(bmap.getTime(midievent.getTick() * beatfactor),rate,channels);
					if(nexttime < 0) nexttime = 0;
				}
				trackpos = 0;
				
			}
			
			
		}
		
		public void processMidiMessage(MidiMessage mmsg)
		{		
			
			if(mmsg instanceof ShortMessage)
			{
				ShortMessage smmsg = (ShortMessage)mmsg;
				if(smmsg.getCommand() == ShortMessage.CONTROL_CHANGE)
				{				 			
					int data1 = smmsg.getData1();
					if(data1 == 0x07) // Volume
					{
						volume = ((double)smmsg.getData2()) / 127.0;
						calcValues();
						return;
					}
					if(data1 == 0x08) // Balance
					{
						balance = ((double)(smmsg.getData2() - 1)) / 126.0;
						if(balance < 0) balance = 0;
						calcValues();
						return;
					}				
					if(data1 == 0x0A) // Pan
					{
						pan = ((double)(smmsg.getData2() - 1)) / 126.0;
						if(pan < 0) pan = 0;
						calcValues();
						return;
					}
					if(data1 == 0x0B) // Expression
					{
						expression = smmsg.getData1() / 127.0;
						calcValues();
						return;
					}			
					if(data1 == 0x5B) // Reverb
					{
						reverb = smmsg.getData1() / 127.0;
						calcValues();
						return;
					}		
					if(data1 == 0x5D) // chorus
					{
						chorus = smmsg.getData1() / 127.0;
						calcValues();
						return;
					}					
					if(data1 == 0x79) // Reset All Controllers
					{
						pan = 0.5;
						balance = 0.5;
						volume = 1.0;
						expression = 1.0;
						reverb = 0.5;
						chorus = 0.0;
						calcValues();
						return;							
					}							
				}
			}	
			
		}
		
		boolean firstbuffer = true;
		
		
		public void processSkip(int len)
		{
			for (int m = 0; m < current_value.length; m++) {
				processSkip(len, m);
			}			
		}
		
		public void processSkip(int len, int m)
		{
			
			
			
			int ix = 0;
			double cur = current_value[m];
			double tar = target_value[m];
			
			if(firstbuffer)
			{
				firstbuffer = false;
				cur = tar;
			}
			
			if(cur > tar)
			{				
				double step_value = -max_step_size;
				while(cur != tar)
				{			
					cur += step_value;
					if(cur < tar)
					{
						cur = tar;
						break;
					}					
					ix++;
					if(ix == len)
					{
						current_value[m] = cur; 
						return;
					}
				}						
			}
			
			if(cur < tar)
			{
				double step_value = max_step_size;
				while(cur != tar)
				{			
					cur += step_value;
					if(cur > tar)
					{
						cur = tar;
						break;
					}
					ix++;
					if(ix == len)
					{
						current_value[m] = cur; 
						return;
					}
				}					
			}
			current_value[m] = cur; 
			
			
		}
		
		public void processAudio(double[] buffer, int start, int end)
		{
			for (int m = 0; m < current_value.length; m++) 
				processAudio(buffer, start, end, m);
		}
		
		public void processAudio(double[] buffer, int start, int end, int m)
		{
			
			int chs = session.getChannels();	
			int ix = start + m;
			double cur = current_value[m];
			double tar = target_value[m];
			
			if(firstbuffer)
			{
				firstbuffer = false;
				cur = tar;
			}
			
			if(cur > tar)
			{				
				double step_value = -max_step_size;
				while(cur != tar)
				{			
					cur += step_value;
					if(cur < tar)
					{
						cur = tar;
						break;
					}					
					buffer[ix] += cur;
					ix+= chs;
					if(ix >= end)
					{
						current_value[m] = cur; 
						return;
					}
				}						
			}
			
			if(cur < tar)
			{
				double step_value = max_step_size;
				while(cur != tar)
				{			
					cur += step_value;
					if(cur > tar)
					{
						cur = tar;
						break;
					}
					buffer[ix] += cur;
					ix+= chs;
					if(ix >= end)
					{
						current_value[m] = cur; 
						return;
					}
				}					
			}
			current_value[m] = cur;
			
			for (; ix < end; ix += chs) {
				buffer[ix] += cur;
			}
			
		}
		
		public int skip(int len) {
			
			if(realtime)
			{
				processSkip(len);
				return len;
			}
			
			long endposition = position + len;
			int writepos = 0;
			
			while((nexttime <= endposition) && (midievent != null))
			{
				
				/*
				 *  Process Current Midi Event
				 */
				MidiMessage mmsg = midievent.getMessage();
				processMidiMessage(mmsg);
				
				/*
				 Process voices for deltatime			
				 */
				long deltatime = nexttime - position;
				if(deltatime != 0)
				{
					processSkip((int)(deltatime));
				}
				position = nexttime;
				writepos += deltatime;
				
				/*
				 Fetch next midi event			
				 */				
				trackpos++;
				if(trackpos < track.size())
				{
					midievent = track.get(trackpos);
					nexttime = AudioEvents.TimeToStreamTime(bmap.getTime(midievent.getTick() * beatfactor), rate, channels);
					if(nexttime < 0) nexttime = 0;
				}
				else
				{
					midievent = null;
				}				 
			}
			
			/*
			 Process voices for deltatime			
			 */			
			long deltatime = endposition - position;
			if(deltatime != 0)
			{			
				processSkip((int)(deltatime));
			}
			position = endposition;
			
			return len;
		}
		
		
		public int mix(double[] buffer, int start, int end) {
			
			if(realtime)
			{
				processAudio(buffer, start, end);
				return end - start;
			}
			
			long endposition = position + end - start;
			int writepos = 0;
			
			while((nexttime <= endposition) && (midievent != null))
			{
				
				/*
				 *  Process Current Midi Event
				 */
				MidiMessage mmsg = midievent.getMessage();
				processMidiMessage(mmsg);
				
				/*
				 Process voices for deltatime			
				 */
				long deltatime = nexttime - position;
				if(deltatime != 0)
				{
					processAudio(buffer, writepos, (int)(writepos + deltatime));
				}
				position = nexttime;
				writepos += deltatime;
				
				/*
				 Fetch next midi event			
				 */				
				trackpos++;
				if(trackpos < track.size())
				{
					midievent = track.get(trackpos);
					nexttime = AudioEvents.TimeToStreamTime(bmap.getTime(midievent.getTick() * beatfactor), rate, channels);
					if(nexttime < 0) nexttime = 0;
				}
				else
				{
					midievent = null;
				}				 
			}
			
			/*
			 Process voices for deltatime			
			 */			
			long deltatime = endposition - position;
			if(deltatime != 0)
			{			
				processAudio(buffer, writepos, (int)(writepos + deltatime));
			}
			position = endposition;
			
			return end - start;
		}
		
		public int replace(double[] buffer, int start, int end) {
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);			
		}
		public int isStatic(double[] buffer, int len) {
			// Athuga hvort breytingar s�u � gangi
			//return -1;
			
			double cur = current_value[0];
			for (int m = 0; m < current_value.length; m++) {
				if(Math.abs(current_value[m] - cur) > 0.000000001) return -1;
				if(Math.abs(target_value[m] - cur) > 0.000000001) return -1;
			}
			
			
			buffer[0] = current_value[0];
			// Athuga hvort um realtime straum s� a� r��a
			if(!realtime) 		{
				
				// Athuga hvort einhverjar raddir � notast innan �essa t�maramma
				
				if(midievent == null) return len;
				
				long endposition = position + len;		
				
				if((nexttime <= endposition) && (midievent != null)) return -1;
				
				position = endposition;
			}
			
			return len;
		}				
		public void close() {
			
			if(midiseq != null)
				midiseq.removeReceiver(this);
			
			/*
			 iterator =  voicereusing.iterator();
			 while (iterator.hasNext()) {
			 SynthesizerVoice voice = (SynthesizerVoice)iterator.next(); 
			 voice.close();
			 }
			 voicereusing.clear();*/
			//voices.clear();
		}
		
		
		public void send(MidiMessage arg0, long arg1) {
			processMidiMessage(arg0);			
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioRenderGain implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Render Gain");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioGainInstance(parameters);
	}
}