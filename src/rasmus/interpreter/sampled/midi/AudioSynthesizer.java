/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.midi;

import rasmus.interpreter.Variable;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.math.AudioOperator;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioSynthesizerInstance implements UnitInstancePart
{
	UnitInstancePart rendernotes;
	UnitInstancePart renderpitch;
	UnitInstancePart rendergain;
	UnitInstancePart gain;
	public AudioSynthesizerInstance(Parameters parameters)
	{
		/*
		 *   pitch  <- RenderPitch(input);
		 * 
		 *   ginput <- RenderGain(input);
		 * 
		 *   output <- gain(ginput) <- RenderNotes(input, pitch)
		 * 
		 */
		Parameters imap = parameters.clone();		
		
		Parameters pmap = new Parameters(parameters.getNameSpace());
		Variable poutput = new Variable();
		pmap.setParameter("output", poutput);
		imap.setParameter("pitch", poutput);
		pmap.setParameter("input", parameters.getParameter("input"));
		renderpitch = new AudioRenderPitch().newInstance(pmap);
		
		Parameters vmap = new Parameters(parameters.getNameSpace());
		Variable voutput = new Variable();
		vmap.setParameter("output", voutput);
		vmap.setParameter("input", parameters.getParameterWithDefault("input"));
		rendergain = new AudioRenderGain().newInstance(vmap);
		
		Parameters gmap = new Parameters(parameters.getNameSpace());
		Variable ginput = new Variable();
		gmap.setParameter("output", parameters.getParameterWithDefault("output"));
		gmap.setParameter("input", ginput);
		gmap.setParameter("input2", voutput);
		gain = new AudioOperator(0).newInstance(gmap);
		
		imap.setParameter("output", ginput);
		rendernotes = new AudioRenderNotes().newInstance(imap);
	}
	public void close() {
		rendernotes.close();
		renderpitch.close();
		rendergain.close();
		gain.close();
	}
	
	
}

public class AudioSynthesizer implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Synthesizer");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.setHasVarargs(true);
		return metadata;		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioSynthesizerInstance(parameters);
	}
}
