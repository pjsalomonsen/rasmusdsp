/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.midi;

import java.util.ArrayList;
import java.util.Iterator;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioRegisterInstrumentInstance extends UnitInstanceAdapter implements Commitable
{
	Variable output;
	Variable answer = null;   
	
	Variable voices;
	Variable program;
	Variable bank;
	Variable description;	
	Variable[] channels = new Variable[16];
	private NameSpace namespace;
	
	public AudioRegisterInstrumentInstance(Parameters parameters)
	{

		namespace = parameters.getNameSpace();
		output = parameters.getParameterWithDefault("output");
		voices = parameters.getParameterWithDefault("input");
		program = parameters.getParameterWithDefault(1, "program");
		bank = parameters.getParameterWithDefault(2, "bank");
		description = parameters.getParameterWithDefault(3, "description");
		for (int i = 0; i < channels.length; i++) {
			channels[i] = parameters.getParameter("ch" + (i+1));
			if(channels[i] != null)
				DoublePart.getInstance(channels[i]).addListener(this);
		}
		
		DoublePart.getInstance(program).addListener(this);
		DoublePart.getInstance(bank).addListener(this);
		ObjectsPart.getInstance(description).addListener(this);
		ObjectsPart.getInstance(voices).addListener(this);
		
		calc();		
	}
	public void close() {		
		
		for (int i = 0; i < channels.length; i++) {
			if(channels[i] != null)
				DoublePart.getInstance(channels[i]).removeListener(this);
		}
		
		DoublePart.getInstance(program).removeListener(this);
		DoublePart.getInstance(bank).removeListener(this);
		ObjectsPart.getInstance(description).removeListener(this);
		ObjectsPart.getInstance(voices).removeListener(this);
				
		if(answer != null) output.remove(answer);
	}
	public void calc() {
		namespace.addToCommitStack(this);
	}
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit()
	{
		if(answer != null) output.remove(answer); 
		
		InstrumentRecord voicedata = new InstrumentRecord();
		voicedata.voices = new ArrayList<VoiceRecord>();
		
		Iterator iter = ObjectsPart.asList(voices).iterator();
		while (iter.hasNext()) {
			Object obj = iter.next();
			if(obj instanceof VoiceRecord) voicedata.voices.add((VoiceRecord)obj);	
		}
		
		if(voicedata.voices.size() == 0)
		{
			VoiceRecord rvoice = new VoiceRecord();
			rvoice.voice = voices;
			voicedata.voices.add(rvoice);								
		}
		
		boolean[] b_channels = new boolean[16];
		boolean defaultval = false;
		boolean allnull = true;
		for (int i = 0; i < channels.length; i++) {			
			if(channels[i] != null)
			{
				boolean val = (((int)DoublePart.asDouble(channels[i]))) != 0;
				if(!val) defaultval = true;
				b_channels[i] = val;
				allnull = false;
			}
		}
		
		if(allnull) defaultval = true;

		for (int i = 0; i < channels.length; i++) {
			if(channels[i] == null) b_channels[i] = defaultval;
		}
				
		voicedata.program = (int)DoublePart.asDouble(program);
		voicedata.bank = (int)DoublePart.asDouble(bank);
		voicedata.description = ObjectsPart.toString(description);
		voicedata.channels = b_channels;
				
		answer = ObjectsPart.asVariable(voicedata);		
		output.add(answer);		
	}
	
}

public class AudioRegisterInstrument implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Register Insrument");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "program",		"Program number",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2,  "bank",		"Bank number",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3,  "description",	"Description",	null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input Voice Generator", null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;		
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioRegisterInstrumentInstance(parameters);
	}
}