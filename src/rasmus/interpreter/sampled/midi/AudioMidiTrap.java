/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.midi;

import java.util.ArrayList;
import java.util.Arrays;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioMidiTrapInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	Variable midi;
	Variable midiout;
	
	public void calc()
	{
	}
	
	public AudioMidiTrapInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		midi = parameters.getParameterWithDefault(1, "midiin");
		midiout = parameters.getParameterWithDefault(2, "midiout");
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream, Receiver
	{
		
		volatile boolean trapped = true;
		boolean a_trapped = true;
		volatile boolean m_trapped = true;
		
		AudioStream inputstream;		
		AudioSession session;
		MidiSequence mout;
		
		public FilterStreamInstance(AudioSession session)
		{
			if(!session.isRealTime())
			{
				inputstream = AudioEvents.openStream(input, session);
				a_trapped = false;
				trapped = false;
				return;
			}
			this.session = session;			
			mout = MidiSequence.getInstance(midiout);
			MidiSequence.getInstance(midi).addReceiver(this);
		}		
		
		public int skip(int len) {			
			
			return len;
		}
		
		public boolean isTrapped()
		{
			if(a_trapped)
			{
				a_trapped = trapped;
				if(!a_trapped)
				{
					synchronized (midiqueue) {			
						AudioSession new_session = session.newSession();
						inputstream = AudioEvents.openStream(input, new_session);
						m_trapped = false;
						for(MidiMessage item : midiqueue)
						{
							mout.send(item, -1);
						}
						midiqueue.clear();
					}
				}
			}
			return a_trapped;
		}
		
		public int mix(double[] buffer, int start, int end) {
			if(isTrapped()) return end - start;
			return inputstream.mix(buffer, start, end);
		}
		
		public int replace(double[] buffer, int start, int end) {
			if(isTrapped())
			{
				Arrays.fill(buffer, start, end, 0);
				return end - start;
			}			
			return inputstream.replace(buffer, start, end);			
		}
		public int isStatic(double[] buffer, int len) {
			if(isTrapped())
			{
				buffer[0] = 0;
				return len;
			}			
			return inputstream.isStatic(buffer, len);			
		}				
		public void close() {
			if(inputstream != null) inputstream.close();
			MidiSequence.getInstance(midi).removeReceiver(this);
		}
				
		ArrayList<MidiMessage> midiqueue = new ArrayList<MidiMessage>();
		public void send(MidiMessage arg0, long arg1) {
			
			if(m_trapped)
			{
				synchronized (midiqueue) {					
				midiqueue.add(arg0);
				if(arg0 instanceof ShortMessage)
				if(((ShortMessage)arg0).getCommand() == ShortMessage.NOTE_ON)
				if(((ShortMessage)arg0).getData2() > 0)
					trapped = false;
				}
			
			}
			else
			{
				mout.send(arg0, arg1);	
			}
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioMidiTrap implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Audio Midi Trap");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "midiin",		"MIDI Input",  	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(1,  "midiout",		"MIDI Output",  null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		
		return metadata;		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioMidiTrapInstance(parameters);
	}
}