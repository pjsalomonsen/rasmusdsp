/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.midi;

import java.util.Arrays;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

import rasmus.interpreter.Variable;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.sampled.BeatToTimeMapper;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioRenderPitchInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = new Variable();
	
	public void calc()
	{
	}
	
	public AudioRenderPitchInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
	}
	class FilterStreamInstance implements AudioStream, Receiver
	{
		
		Sequence iseq;
		//double[] stockbuffer = null;
		long position = 0;
		Track track;
		MidiEvent midievent = null;
		int trackpos = 0;
		double beatfactor = 1.0 / MidiSequence.DEFAULT_RES;
		BeatToTimeMapper bmap;
		long nexttime = 0;
		
		double rate;
		int channels; 
		boolean realtime;
		double max_step_size = 1;
		
		//int midi_defaultvalue = 0; //(int)RDouble.asDouble(defaultvalue);
		double target_value = 1.0;
		double current_value = target_value;
		
		
		MidiSequence midiseq = null;
		AudioSession session;
		AudioCache audiocache;
		public FilterStreamInstance(AudioSession session)
		{
			audiocache = session.getAudioCache();
			
			
			this.session = session;
			this.bmap = session.getBeatToTimeMap();
			this.rate = session.getRate();
			this.channels = session.getChannels();
			this.realtime = session.isRealTime();
			
			this.rate_factor = AudioRenderPitch.sinbuffer_len * 7 / rate; // 7 hZ
			
			max_step_size = 2000 / (channels * rate); // �.s. 200000 breytingar/sec
			
			if(realtime)
			{
				midiseq = MidiSequence.getInstance(input);
				midiseq.addReceiver(this);
			}
			else
			{
				
				iseq = MidiSequence.asSequence(input);
				track = iseq.getTracks()[0];
				if(track.size() != 0)
				{
					midievent = track.get(0);
					nexttime = AudioEvents.TimeToStreamTime(bmap.getTime(midievent.getTick() * beatfactor),rate,channels);
					if(nexttime < 0) nexttime = 0;
				}
				trackpos = 0;
				
			}
			
			
		}
		
		public void calcPitchValue()
		{
			target_value = pitch * coarse_tune * fine_tune;
		}
		int RPN_STATUS = 0x3FFF;
		double pitch = 1.0;
		double coarse_tune = 1.0;
		double fine_tune = 1.0;
		double modulation = 0.0;
		int pitchsens = 200;		
		public void processMidiMessage(MidiMessage mmsg)
		{		
			if(mmsg instanceof ShortMessage)
			{
				ShortMessage smmsg = (ShortMessage)mmsg;
				
				if(smmsg.getCommand() == ShortMessage.CONTROL_CHANGE)
				{
					int control = smmsg.getData1();
					
					if(control == 0x01) // Modulation Wheel
					{
						modulation =  smmsg.getData2() / 127.0;
						return;
					}				
					
					if(control == 0x4c) // Vibrato Rate (10x cent), 7 hz is default
					{
						this.rate_factor = Math.pow(2, (smmsg.getData2() - 64)/120.0) * AudioRenderPitch.sinbuffer_len * 7 / rate; // 7 hZ
					}
					if(control == 0x4d) // Vibrato Depth 
					{
						modulation_depth = ( 100 * Math.pow(4, (smmsg.getData2() - 64) / 64.0 ))/1200.0; // 100 cent is default
					}				
					//this.rate_factor = AudioRenderPitch.sinbuffer_len * 7 / rate; // 7 hZ
					
					
					// RPN Selection
					if(control == 0x65)
					{
						RPN_STATUS = (smmsg.getData2()*0x80) + (RPN_STATUS%0x80);
						return;
					}
					if(control == 0x64)
					{
						RPN_STATUS = RPN_STATUS - (RPN_STATUS%0x80) + smmsg.getData2();
						return;
					}
					
					// Data Entry
					if(control == 0x06 || control == 0x26)
					{
						// 6 coarse (MSB), 38 fine (LSB)
						
						if(RPN_STATUS == 0x0000) // Pitch Bend Range
						{
							// MSB (semitones)
							if(control == 0x06)
							{
								pitchsens = (pitchsens % 100) + (smmsg.getData2()*100);
								return;
							}
							// LSB (cents)
							if(control == 0x26) // was 0x38
							{
								pitchsens = pitchsens - (pitchsens % 100) + smmsg.getData2();
								return;
							}
						}
						
						if(RPN_STATUS == 0x0001) // Master Fine Tuning (in cents)
						{
							if(control == 0x06)
							{
								fine_tune = Math.pow(2, (1.0/1200.0) * (smmsg.getData2() - 64) );
								calcPitchValue();
								return;
							}
						}
						if(RPN_STATUS == 0x0002) // Master Coarse Tuning (in half-steps)
						{
							if(control == 0x06)
							{
								coarse_tune = Math.pow(2, (1.0/12.0) * (smmsg.getData2() - 64) );
								calcPitchValue();
								return;
							}
						}					
					}
					
					if(control == 0x79) // Reset All Controllers
					{
						RPN_STATUS = 0x3FFF;							
						pitch = 1.0;
						coarse_tune = 1.0;
						fine_tune = 1.0;
						modulation = 0.0;
						pitchsens = 200; 			
						calcPitchValue();
						return;							
					}
													
				}
				
				if(smmsg.getCommand() == ShortMessage.PITCH_BEND)
				{				 								
					int pitchvalue = smmsg.getData1() + (smmsg.getData2()*128);
					pitch =  Math.pow(2.0, ((((double)pitchsens) / 1200.0)) * (pitchvalue - 8192.0) / 8192.0);
					calcPitchValue();
					return;
					//System.out.println("PITCH = " + target_value);
				}
				
			}	
		}
		
		boolean firstbuffer = true;
		
		public void processSkip(int len)
		{
			
			int ix = 0;
			double cur = current_value;
			double tar = target_value;
			
			if(firstbuffer)
			{
				firstbuffer = false;
				cur = tar;
			}
			
			if(cur > tar)
			{				
				double step_value = -max_step_size;
				while(cur != tar)
				{			
					cur += step_value;
					if(cur < tar)
					{
						cur = tar;
						break;
					}					
					ix++;
					if(ix == len)
					{
						current_value = cur; 
						return;
					}
				}						
			}
			
			if(cur < tar)
			{
				double step_value = max_step_size;
				while(cur != tar)
				{			
					cur += step_value;
					if(cur > tar)
					{
						cur = tar;
						break;
					}
					ix++;
					if(ix == len)
					{
						current_value = cur; 
						return;
					}
				}					
			}
			current_value = cur; 
			
		}
		
		
		double rate_factor; // = AudioRenderPitch.sinbuffer_len * 10.0; // 10.0 hZ
		double vibration_clock = 0;
		double modulation_depth = 100.0/1200.0; // 100 cent
		
		public void processAudio(double[] buffer, int start, int end)
		{
			if(modulation != 0.0)
			{
				double sinbuffer_len = AudioRenderPitch.sinbuffer_len;
				double[] sinbuffer = AudioRenderPitch.sinbuffer;
				double[] pbuffer = session.getAudioCache().getBuffer(end);
				double modulation_depth = this.modulation_depth;
				double vibration_clock = this.vibration_clock;
				processAudio2(pbuffer, start, end);
				for (int i = start; i < end; i++) {
					double li = vibration_clock % 1.0;
					int ii = (int)vibration_clock;
					double vibvalue = sinbuffer[ii] * (1 - li) + sinbuffer[ii] * (li); 
					buffer[i] += pbuffer[i] * Math.pow(2, vibvalue * modulation * modulation_depth);
					vibration_clock = (vibration_clock + rate_factor) % sinbuffer_len;
				}
				this.vibration_clock = vibration_clock;
				session.getAudioCache().returnBuffer(pbuffer);
			}
			else
				processAudio2(buffer, start, end);
		}
		
		public void processAudio2(double[] buffer, int start, int end)
		{
			
			int ix = start;
			double cur = current_value;
			double tar = target_value;
			
			if(firstbuffer)
			{
				firstbuffer = false;
				cur = tar;
			}
			
			if(cur > tar)
			{				
				double step_value = -max_step_size;
				while(cur != tar)
				{			
					cur += step_value;
					if(cur < tar)
					{
						cur = tar;
						break;
					}					
					buffer[ix] += cur;
					ix++;
					if(ix >= end)
					{
						current_value = cur; 
						return;
					}
				}						
			}
			
			if(cur < tar)
			{
				double step_value = max_step_size;
				while(cur != tar)
				{			
					cur += step_value;
					if(cur > tar)
					{
						cur = tar;
						break;
					}
					buffer[ix] += cur;
					ix++;
					if(ix >= end)
					{
						current_value = cur; 
						return;
					}
				}					
			}
			current_value = cur; 
			Arrays.fill(buffer, ix, end, current_value);
			
		}
		
		public int skip(int len) {
			
			if(realtime)
			{
				processSkip(len);
				return len;
			}
			
			long endposition = position + len;
			int writepos = 0;
			
			while((nexttime <= endposition) && (midievent != null))
			{
				
				/*
				 *  Process Current Midi Event
				 */
				MidiMessage mmsg = midievent.getMessage();
				processMidiMessage(mmsg);
				
				/*
				 Process voices for deltatime			
				 */
				long deltatime = nexttime - position;
				if(deltatime != 0)
				{
					processSkip((int)(deltatime));
				}
				position = nexttime;
				writepos += deltatime;
				
				/*
				 Fetch next midi event			
				 */				
				trackpos++;
				if(trackpos < track.size())
				{
					midievent = track.get(trackpos);
					nexttime = AudioEvents.TimeToStreamTime(bmap.getTime(midievent.getTick() * beatfactor), rate, channels);
					if(nexttime < 0) nexttime = 0;
				}
				else
				{
					midievent = null;
				}				 
			}
			
			/*
			 Process voices for deltatime			
			 */			
			long deltatime = endposition - position;
			if(deltatime != 0)
			{			
				processSkip((int)(deltatime));
			}
			position = endposition;
			
			return len;
		}
		
		
		public int mix(double[] buffer, int start, int end) {
			
			if(realtime)
			{
				processAudio(buffer, start, end);
				return end - start;
			}
			
			long endposition = position + end - start;
			int writepos = 0;
			
			while((nexttime <= endposition) && (midievent != null))
			{
				
				/*
				 *  Process Current Midi Event
				 */
				MidiMessage mmsg = midievent.getMessage();
				processMidiMessage(mmsg);
				
				/*
				 Process voices for deltatime			
				 */
				long deltatime = nexttime - position;
				if(deltatime != 0)
				{
					processAudio(buffer, writepos, (int)(writepos + deltatime));
				}
				position = nexttime;
				writepos += deltatime;
				
				/*
				 Fetch next midi event			
				 */				
				trackpos++;
				if(trackpos < track.size())
				{
					midievent = track.get(trackpos);
					nexttime = AudioEvents.TimeToStreamTime(bmap.getTime(midievent.getTick() * beatfactor), rate, channels);
					if(nexttime < 0) nexttime = 0;
				}
				else
				{
					midievent = null;
				}				 
			}
			
			/*
			 Process voices for deltatime			
			 */			
			long deltatime = endposition - position;
			if(deltatime != 0)
			{			
				processAudio(buffer, writepos, (int)(writepos + deltatime));
			}
			position = endposition;
			
			return end - start;
		}
		
		public int replace(double[] buffer, int start, int end) {
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);			
		}
		public int isStatic(double[] buffer, int len) {
			// Athuga hvort breytingar s�u � gangi
			if(Math.abs(modulation) > 0.000000001) return -1;
			if(Math.abs(current_value - target_value) > 0.000000001) return -1;
			
			buffer[0] = current_value;
			// Athuga hvort um realtime straum s� a� r��a
			if(!realtime) 		{
				
				// Athuga hvort einhverjar raddir � notast innan �essa t�maramma
				
				if(midievent == null) return len;
				
				long endposition = position + len;		
				
				if((nexttime <= endposition) && (midievent != null)) return -1;
				
				position = endposition;
			}
			
			return len;
		}				
		public void close() {
			
			if(midiseq != null)
				midiseq.removeReceiver(this);
			
			/*
			 iterator =  voicereusing.iterator();
			 while (iterator.hasNext()) {
			 SynthesizerVoice voice = (SynthesizerVoice)iterator.next(); 
			 voice.close();
			 }
			 voicereusing.clear();*/
			//voices.clear();
		}
		
		
		public void send(MidiMessage arg0, long arg1) {
			processMidiMessage(arg0);			
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioRenderPitch implements UnitFactory, MetaDataProvider {
	
	static double[] sinbuffer = new double[1002];
	static int sinbuffer_len = 1000;
	static
	{
		double coeff = Math.PI * 2.0 / ((double)sinbuffer_len);
		for (int i = 0; i < sinbuffer.length; i++) {
			sinbuffer[i] = Math.sin(i * coeff );
		}
	}
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Render Pitch");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}		
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioRenderPitchInstance(parameters);
	}
}