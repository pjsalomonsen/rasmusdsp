/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.midi;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.list.SynchronizedList;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioClosable;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.sampled.AudioVoice;
import rasmus.interpreter.sampled.AudioVoiceManager;
import rasmus.interpreter.sampled.BeatToTimeMapper;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.Unit;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstance;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class SynthesizerVoice implements AudioClosable, AudioVoice
{
	//double velocity = 0;
	//double note = 0;
	//double active = 0;
	
	int i_note = 0;
	Variable v_note = new Variable();
	Variable v_velocity = new Variable();
	Variable v_active;
	Variable t_note = null;
	Variable t_velocity = null;
	//Variable t_active = null;
	
	Variable voice;
	AudioSession session;
	AudioVoiceManager voicemanager = null;
	
	boolean isActive = true;
	
	long processId = 0;
	
	List group = null;
	
	long ontime = -1;
	long offtime = -1;
	
	public long getOnTime()
	{
		return ontime;
	}
	public long getOffTime()
	{
		return offtime;
	}
	
	public void setActive(boolean value)
	{
		
		if(voicemanager != null)
		if(!value)
		{
			offtime = voicemanager.getTime();
		}
		isActive = value;
	}			
	
	public boolean isActive()
	{
		return isActive;
	}
	
	public void setNote(int note)
	{
		i_note = note;
		if(t_note != null) v_note.remove(t_note);
		t_note = DoublePart.asVariable(note);
		v_note.add(t_note);
	}
	public void setVelocity(double velocity)
	{
		if(t_velocity != null) v_velocity.remove(t_velocity);
		t_velocity = DoublePart.asVariable(velocity);
		v_velocity.add(t_velocity);				
	}	
	UnitInstance voci = null;
	Variable voiceo = new Variable();
	
	public Object registerdvoice;
	NameSpace namespace;
	
	AudioStreamable activeStreamable = new AudioStreamable()
	{
		public AudioStream openStream(AudioSession session) {
			return new AudioStream()
			{
				public int mix(double[] buffer, int start, int end) {
					if(!isActive) return -1;
					for (int i = start; i < end; i++) {
						buffer[i] += 1.0;							
					}
					return end-start;
				}
				public int replace(double[] buffer, int start, int end) {
					if(!isActive) return -1;
					Arrays.fill(buffer, start, end, 1.0);
					return end-start;
				}
				public int isStatic(double[] buffer, int len) {
					if(!isActive) return -1;
					buffer[0] = 1.0;
					return len;
				}
				public int skip(int len) {
					if(!isActive) return -1;
					return len;
				}
				public void close() {
				}
			};
		}
	};

	public SynthesizerVoice(NameSpace namespace, Variable voice, Map ext_input_map, Map o_ext_input_map)
	{
		
		v_active = AudioEvents.asVariable(new AudioEvent(0, activeStreamable));
		this.voice = voice;
		this.namespace = namespace;
		Parameters params = new Parameters(namespace);
		params.setParameter("output", voiceo);
		params.setParameter("note", v_note);      
		params.setParameter("velocity", v_velocity);
		params.setParameter("active", v_active);   // � v�st a� vera gate en ekki active
		params.setParameter("gate", v_active);	 
		
		Iterator iterator = ext_input_map.keySet().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			Variable obj = ((AudioRenderNotesInstance.FilterStreamInstance.ExtendedStreamReader)ext_input_map.get(key)).variable;
			params.setParameter(key, obj);
		}
		iterator = o_ext_input_map.keySet().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			if(ext_input_map.get(key) == null)
				params.setParameter(key, (Variable)o_ext_input_map.get(key));			
		}		
		
		voci = Unit.getInstance(voice).newInstance(params);
		// Created voices shall not be updatable
		voci.setUpdateable(false);
		
		//RInterpreter.commit();
		
	}
	
	AudioStream voicestream = null;
	
	
	public AudioStream getStream()
	{
		
		if(voicestream != null) return voicestream;



		
		
		//Interpreter.commit();

		namespace.commit();
		
		synchronized (AudioRenderNotes.class) {
			AudioRenderNotes.activevoices++;
		}
		AudioSession voicesession = session.newSession();
		voicesession.setRealTime(false);
		//voicesession.type = AudioSession.VOICE_STREAM;
		
		AudioEvents audioevents = AudioEvents.getInstance(voiceo);
		if(audioevents.track.size() == 1)
		{
			AudioEvent aevent = (AudioEvent)audioevents.track.get(0);
			if(aevent.start == 0)
			{
				voicestream = aevent.streamable.openStream(voicesession);
				
				return voicestream;
			}
		}
		
		voicestream = AudioEvents.openStream(voiceo, voicesession);
		
		
		return voicestream;
	}
	
	boolean softKillActive = false;
	double softKill_vol;
	double softKill_vol_step;
	
	public void softKill()
	{
		if(softKillActive) {
			return;
		}
		softKillActive = true;
		// Fade note down in 5 msec, linear
		softKill_vol = 1.0;
		softKill_vol_step = 1.0 / (0.005 * session.getRate());
		if(group != null)
		{		
			group.remove(this);
			group = null;
		}
		if(voicemanager != null)
		{
			voicemanager.removeVoice(this);
			voicemanager = null;
		}
	}
	
	public void stopNow()
	{
		if(group != null)
		{
			softKill_vol_step = 1.0 / (0.005 * session.getRate());
			
			Iterator iterator = group.iterator();
			while (iterator.hasNext()) {
				SynthesizerVoice voice = (SynthesizerVoice) iterator.next();

				if(!voice.softKillActive) {
					
					voice.softKillActive = true;
					voice.softKill_vol = 1.0;
					voice.softKill_vol_step = softKill_vol_step;
					voice.group = null;
					if(voice.voicemanager != null)
					{
						voice.voicemanager.removeVoice(voice);
						voice.voicemanager = null;
					}				
				}
				
			}
		}
	}
	
	public void reset()
	{
		if(voicestream != null)
		{
			group = null;
			
			if(group != null)
			{		
				group.remove(this);
				group = null;
			}
			if(voicemanager != null)
			{
				voicemanager.removeVoice(this);
				voicemanager = null;
			}			
			
			synchronized (AudioRenderNotes.class) {
				AudioRenderNotes.activevoices--;
			}
			
			
			voicestream.close();
			voicestream = null;
			
			softKillActive = false;
		}
	}					
	public int mix(double[] buffer, int start, int end) {
		if(softKillActive) 
		{
			
			int channels = session.getChannels();
			double evol = softKill_vol;
			if(evol == 0) return -1; // EOF
			double estep = softKill_vol_step;
			
			double[] abuffer = session.getAudioCache().getBuffer(end);
			Arrays.fill(abuffer, 0);
			int ret = getStream().mix(abuffer, start, end);
			
			for (int c = 0; c < channels; c++) {	
				evol = softKill_vol;;
				for (int i = start + c; i < end; i+=channels) {				
					evol -= estep; 
					if(evol < 0)
					{
						evol = 0;
						break;
					}
					buffer[i] += evol * abuffer[i];
				}
			}
			
			session.getAudioCache().returnBuffer(abuffer);
			
			softKill_vol = evol;
			
			return ret;
			
		}
		
		int ret = getStream().mix(buffer, start, end);
		return ret;
	}
	public int skip(int len)
	{
		return getStream().skip(len);
	}
	public void close() {				
		reset();
		if(voci != null) voci.close();
	}			
}



class AudioRenderNotesInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;
	Map ext_input_map = new HashMap();
	Variable answer = new Variable();
	Variable voice;
	SynchronizedList voicelist;
	
	public void calc()
	{
	}
	
	private boolean isNumber(String number)
	{
		for (int i = 0; i < number.length(); i++) {
			if(!Character.isDigit(number.charAt(i))) return false;
		}
		return true;
	}
	public AudioRenderNotesInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		voice = parameters.getParameterWithDefault(1, "voices");		
		voicelist = ObjectsPart.synchronizedList(voice);
		
		Iterator pkeys = parameters.getParameters().keySet().iterator();
		while (pkeys.hasNext()) {
			String key = (String)pkeys.next();
			if(!isNumber(key))
			{
				if(!key.equals("output"))
					if(!key.equals("input"))
						if(!key.equals("voice"))
						{
							Variable v = parameters.getParameter(key);
							if(v != null) ext_input_map.put(key, v);
						}
			}			
		}
		/*
		 RFloat.getInstance(gain).addListener(this);
		 calc();*/
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		output.remove(answer);
		voicelist.close();
	}
	
	public class FilterStreamInstance implements AudioStream, Receiver
	{	
		
		public class ExtendedStreamReader implements AudioStreamable 
		{
			
			public class ExtendedAudioStream implements AudioStream
			{
				AudioSession session;
				public ExtendedAudioStream(AudioSession session)
				{
					this.session = session;
				}
				public int mix(double[] buffer, int start, int end) {
					
					int len = end - start;
					len = len / session.getChannels();				
					int wp = writepos / wsession.getChannels();
					int channels = session.getChannels();
					
					if(isStatic)
					{
						double staticval =  staticbuffer[0];
						for (int i = start; i < end; i++) {
							buffer[i] += staticval;
						}
						return end - start;
					}				
					
					int ext_len = extbuffer_len;
					for (int c = 0; c < channels; c++) {
						int pos = c;
						int epos = wp;
						for (int i = 0; i < len; i++) {						
							buffer[pos] += extbuffer[epos];
							pos += channels;
							epos++;
							if(epos >= ext_len) 
								epos = ext_len - 1;						
							//if(epos >= extbuffer_len) break;
						}
					}
					
					return end - start;
				}
				public int replace(double[] buffer, int start, int end) {
					
					int len = end - start;
					len = len / session.getChannels();				
					int wp = writepos / wsession.getChannels();
					int channels = session.getChannels();
					
					if(isStatic)
					{
						double staticval =  staticbuffer[0];
						for (int i = start; i < end; i++) {
							buffer[i] = staticval;
						}
						return end - start;
					}
					
					int ext_len = extbuffer_len;
					for (int c = 0; c < channels; c++) {
						int pos = start + c;
						int epos = wp;
						for (int i = 0; i < len; i++) {						
							buffer[pos] = extbuffer[epos];
							pos += channels;
							epos++;
							if(epos >= ext_len) 
								epos = ext_len - 1;
						}
					}
					
					return end - start;
				}
				public int isStatic(double[] buffer, int len) {
					if(!isStatic) return -1;
					buffer[0] = staticbuffer[0];
					return len;
				}
				public int skip(int len)
				{
					return len;
				}
				public void close() {
				}					
			}
			
			
			public AudioStream audiostream;
			public boolean eof_audiostream = false;
			public AudioCache audiocache;
			public AudioSession session;
			public boolean isStatic = true;
			public int extbuffer_len = 0;
			public double[] staticbuffer = new double[1];
			public double[] extbuffer = null;
			public Variable variable = new Variable();
			public Variable tvariable;
			
			public AudioStream openStream(AudioSession session)
			{
				return new ExtendedAudioStream(session);
			}
			
			public ExtendedStreamReader()
			{
				AudioEvent audioevent = new AudioEvent(0, this);
				tvariable = AudioEvents.asVariable(audioevent);
				variable.add(tvariable);
			}
			public void close()
			{			
				if(extbuffer != null) audiocache.returnBuffer(extbuffer);
				extbuffer = null;
				audiostream.close();
				variable.remove(tvariable);
				variable = null;
				tvariable = null;
			}
			public void skip(int len)
			{
				len = len / session.getChannels();
				
				if(eof_audiostream)
				{
					staticbuffer[0] = 0;
					isStatic = true;
					return;
				}
				
				isStatic = false;														
				int ret = audiostream.skip(len);
				if(ret == -1)
				{
					eof_audiostream = true;
					staticbuffer[0] = 0;
					isStatic = true;
					return;				
				}
				extbuffer_len = len;
				
			}
			public void read(int len)
			{			
				len = len / session.getChannels();
				
				if(eof_audiostream)
				{
					staticbuffer[0] = 0;
					isStatic = true;
					return;
				}
				
				//int ret;
				
				int ret = audiostream.isStatic(staticbuffer, 1);
				if(ret != -1)
				{
					isStatic = true;
					return;
				}
				isStatic = false;			
				
				if(extbuffer != null) 
					if(extbuffer.length < len) 
					{
						audiocache.returnBuffer(extbuffer);
						extbuffer = null;
					}
				if(extbuffer == null) 
				{
					extbuffer = audiocache.getBuffer(len);
				}
				
				ret = audiostream.replace(extbuffer, 0, len);
				if(ret == -1)
				{
					eof_audiostream = true;
					staticbuffer[0] = 0;
					isStatic = true;
					return;				
				}
				extbuffer_len = len;
				if(ret != len) Arrays.fill(extbuffer, ret, len, 0);
				
			}
		}
		
		
		
		Sequence iseq;
		//double[] stockbuffer = null;
		long position = 0;
		Track track;
		MidiEvent midievent = null;
		int trackpos = 0;
		double beatfactor = 1.0 / MidiSequence.DEFAULT_RES;
		BeatToTimeMapper bmap;
		long nexttime = 0;
		Hashtable actnotes = new Hashtable();
		
		List voices = new ArrayList();
		double rate;
		int channels; 
		boolean realtime;
		Map<VoiceRecord, RegisterdVoice> registeredvoices = new HashMap<VoiceRecord, RegisterdVoice>();
		//List realvoices = new ArrayList();
		//Map programvoices = new HashMap();
		Map ext_input_streams = new HashMap();
		
		
		MidiSequence midiseq = null;
		AudioSession wsession;
		AudioVoiceManager voicemanager = null;
		AudioCache audiocache;
		
		
		
		class RegisterdVoice
		{
			Variable voice;
			int exclusiveClass = 0;
			LinkedList voicereusing = new LinkedList();
			
			ListPartListener listlistener = new ListPartListener()
			{
				public void objectAdded(ListPart source, Object object) {
					clear();
				}
				public void objectRemoved(ListPart source, Object object) {
					clear();
				}
				public void objectsAdded(ListPart source, List objects) {
					clear();
				}
				public void objectsRemoved(ListPart source, List objects) {
					clear();
				}
			};
			
			public RegisterdVoice(VoiceRecord voice)
			{
				Unit.getInstance(voice.voice).addListener(listlistener);
				
				synchronized (voicereusing) {
					registeredvoices.put(voice, this);
				}
				this.voice = voice.voice;
				exclusiveClass = voice.exclusiveClass;
			}
			
			public SynthesizerVoice getVoice()
			{
				synchronized (voicereusing) {
					if(voicereusing.size() == 0)
					{
						return null;
					}
					return (SynthesizerVoice)voicereusing.remove(0);
				}
				
			}			
			public void returnVoice(SynthesizerVoice synvoice)
			{
				synchronized (voicereusing) {
					if(voicereusing.size() > 8)
					{
						synvoice.close();
						return;
					}
					voicereusing.add(synvoice);
				}
			}
			/*
			 public void init()
			 {
			 voicereusing = (LinkedList)audiocache.cachetable.get(voice);
			 if(voicereusing == null)
			 {
			 voicereusing = new LinkedList();
			 audiocache.cachetable.put(voice, voicereusing);
			 */
			/*
			 *  Vi� Vilum fylgjast vi� �essari r�dd
			 *  og ey�a henni �r cache ef breytingar ver�a �
			 *  henni �.s. ef unitfactory er fjarl�gt �r
			 *  henni �� � a� hreinsa allar raddir �r minni
			 */
			/*
			 *  Einnig viljum ekki lengur fylgjast me� ef
			 *  session er loka� :!!
			 */
			/*}
			 
			 
			 } */
			
			public void clear()
			{
				synchronized (voicereusing) {
					Iterator itr = voicereusing.iterator();
					while (itr.hasNext()) {
						Object element = itr.next();
						if(element instanceof AudioClosable)
						{
							((AudioClosable)element).close();
						}
						
					}
					voicereusing.clear();					
				}
			}
			public void close()
			{
				Unit.getInstance(voice).removeListener(listlistener);
				clear();
			}
		}		
		
		AudioSession session;
		public FilterStreamInstance(AudioSession session)
		{
			
			this.session = session;
			audiocache = session.getAudioCache();
			
			Iterator i_iterator = ext_input_map.keySet().iterator();
			while (i_iterator.hasNext()) {
				String key = (String) i_iterator.next();
				Variable variable = (Variable)ext_input_map.get(key);
				
				if(AudioEvents.getInstance(variable).track.size() != 0)
				{
					ExtendedStreamReader esr = new ExtendedStreamReader();
					esr.audiocache = audiocache;
					esr.session = session;
					esr.audiostream = AudioEvents.openStream(variable, session.getMonoSession());
					
					ext_input_streams.put(key, esr);
				}
			}
			
			
			//List voicelist = RObjects.asList(voice);
			/*
			synchronized(voicelist)
			{
			Iterator iterator = voicelist.iterator();
			while (iterator.hasNext()) {
				Object element = (Object) iterator.next();
				if(element instanceof RVariable[])
				{
					VoiceRecord variables = (VoiceRecord)element;
					
					RegisterdVoice rvoice = new RegisterdVoice(variables.voice);
					//rvoice.voice = variables[0];
					rvoice.keyfrom = (int)RDouble.asDouble(variables.keyfrom);
					rvoice.keyto = (int)RDouble.asDouble(variables.keyto);
					rvoice.velfrom = (int)RDouble.asDouble(variables.velfrom);
					rvoice.velto = (int)RDouble.asDouble(variables.velto);
					if(variables[5] != null)
						rvoice.program = (int)RDouble.asDouble(variables[5]); 
					if(variables[6] != null)
						rvoice.bank = (int)RDouble.asDouble(variables[6]);
					if(variables.exclusiveClass != null)
						rvoice.exclusiveClass = (int)RDouble.asDouble(variables.exclusiveClass);
					//rvoice.init();
					
					if(rvoice.program != -1)
					{
						List pgmvoicelist = (List)programvoices.get(rvoice.program+"."+rvoice.bank);
						if(pgmvoicelist == null)
						{
							pgmvoicelist = new ArrayList();
							programvoices.put(rvoice.program+"."+rvoice.bank, pgmvoicelist);
						}
						pgmvoicelist.add(rvoice);
					}
					else
						realvoices.add(rvoice);
				}
			}
			}
			
			if(realvoices.size() == 0)
			{
				RegisterdVoice rvoice = new RegisterdVoice(voice);
				//rvoice.voice = voice;
				rvoice.keyfrom = 0;
				rvoice.keyto = 127;
				rvoice.velfrom = 0;
				rvoice.velto = 127;
				//rvoice.init();
				realvoices.add(rvoice);								
			}*/
			
			this.wsession = session;
			this.voicemanager = session.getVoiceManager();
			this.bmap = session.getBeatToTimeMap();
			this.rate = session.getRate();
			this.channels = session.getChannels();
			this.realtime = session.isRealTime();
			
			if(realtime)
			{
				midiseq = MidiSequence.getInstance(input);
				midiseq.addReceiver(this);
			}
			else
			{
				
				iseq = MidiSequence.asSequence(input);
				track = iseq.getTracks()[0];
				if(track.size() != 0)
				{
					midievent = track.get(0);
					nexttime = AudioEvents.TimeToStreamTime(bmap.getTime(midievent.getTick() * beatfactor),rate,channels);
					if(nexttime < 0) nexttime = 0;
				}
				trackpos = 0;
				
			}
			
			
		}
		
		int selbank = -1;
		int selprogram = -1;		
		int selchannel = -1;
		List<VoiceRecord> selvoices = null; 
		
		public List<VoiceRecord> getInstrumentVoices(int program, int bank, int channel)
		{
			ArrayList<VoiceRecord> voices = new ArrayList<VoiceRecord>();
			synchronized(voicelist)
			{
				Iterator iterator = voicelist.iterator();
				while (iterator.hasNext()) {
					Object element = (Object) iterator.next();
					if(element instanceof VoiceRecord)
					{
						voices.add((VoiceRecord)element);
					}
					if(element instanceof InstrumentRecord)
					{
						InstrumentRecord ins = (InstrumentRecord)element;
						if(ins.bank == bank)
						if(ins.program == program)  
						if(ins.channels[channel])
							voices.addAll(ins.voices);
					}					
				}
			}
			if(voices.size() == 0)
			{
					VoiceRecord rvoice = new VoiceRecord();
					rvoice.voice = voice;
					voices.add(rvoice);					
			}
			return voices;
		}
		
		
		public List<VoiceRecord> getVoices(int channel)
		{
			if(selbank == currentbank && selprogram == currentprogram && selchannel == channel) return selvoices;
			selvoices = getInstrumentVoices(currentprogram, currentbank, channel);
			selprogram = currentprogram;
			selbank = currentbank;
			selchannel = channel;
			return selvoices;
		}
		
		int currentbank = 0;
		int currentprogram = 0;
		boolean[] keys_down = new boolean[128];
		public void processMidiMessage(MidiMessage mmsg)
		{
			synchronized(voices)
			{			
				if(mmsg instanceof ShortMessage)
				{
					ShortMessage smmsg = (ShortMessage)mmsg;
					
					if(smmsg.getCommand() == ShortMessage.NOTE_ON
							|| smmsg.getCommand() == ShortMessage.NOTE_OFF)
					{
						int note = smmsg.getData1();
						int vel = smmsg.getData2();
						
						if(smmsg.getCommand() == ShortMessage.NOTE_OFF) vel = 0;

						
						keys_down[note] = vel != 0;
												
						if(vel == 0)
						{
							if(hold_active) return;
							if(sustenuto_notes != null)
							{
								for (int i = 0; i < sustenuto_notes.length; i++) {
									if(sustenuto_notes[i] == note) return;
								}
							}							
						}
						
						Integer i_note = new Integer(note);

						List newvoices = (List)actnotes.get(i_note);
						if(newvoices != null)
						{
							Iterator iterator = newvoices.iterator();
							while (iterator.hasNext()) {
								SynthesizerVoice newvoice = (SynthesizerVoice) iterator.next();
								newvoice.setActive(false);		
							}
							actnotes.remove(i_note);
						}						
						
						if(vel != 0)
						{
							
							List voicegroup = null;
							
							// Note ON
							//if(actnotes.get(i_note) == null)
							{
								
								List actvoicelist = null;
								
								Iterator iterator = getVoices(smmsg.getChannel()).iterator();
								
								List enotelist = null;
								while (iterator.hasNext()) {
									VoiceRecord element = (VoiceRecord) iterator.next();
									if(element.keyfrom <= note)
										if(element.keyto >= note)
											if(element.velfrom <= vel)
												if(element.velto >= vel)
												{
													
													RegisterdVoice regvoice = registeredvoices.get(element);
													if(regvoice == null) regvoice = new RegisterdVoice(element);
														
													
													SynthesizerVoice newvoice;
													newvoice = (SynthesizerVoice)regvoice.getVoice();
													if(newvoice == null)
													{
														newvoice = new SynthesizerVoice(session.getNameSpace(), regvoice.voice, ext_input_streams, ext_input_map);
														newvoice.registerdvoice = regvoice;
													}
													
													if(regvoice.exclusiveClass != 0)
													{
														if(enotelist == null) enotelist = new ArrayList(4);
														enotelist.add(newvoice);
														
														int exclusiveClass = regvoice.exclusiveClass;
														Iterator viterator = voices.iterator();
														while (viterator.hasNext()) {
															SynthesizerVoice voice = (SynthesizerVoice)viterator.next();
															if(voice.registerdvoice instanceof RegisterdVoice)
															{
																RegisterdVoice rvoice = (RegisterdVoice)voice.registerdvoice;
																if(rvoice.exclusiveClass == exclusiveClass)
																	if(!enotelist.contains(voice))
																	{												
																		voice.softKill();
																	}									
															}
														}
													}						
													
													if(voicemanager != null)
													{
													if(voicegroup == null) voicegroup = new ArrayList();
													voicegroup.add(newvoice);
													newvoice.group = voicegroup;
													newvoice.voicemanager = voicemanager;
													newvoice.ontime = voicemanager.getTime();
													newvoice.offtime = -1;
													newvoice.voicemanager.addVoice(newvoice);
													}
													
													newvoice.session = wsession;
													newvoice.setNote(note); /* Math.pow(2, (note - 60)/12.0)); */
													newvoice.setVelocity(vel / 127.0);
													newvoice.setActive(true);
													if(actvoicelist == null) actvoicelist = new ArrayList();
													actvoicelist.add(newvoice);
													voices.add(newvoice);					
													
													
												}
								}
								
								if(actvoicelist != null)
									actnotes.put(i_note, actvoicelist);
							}
							
						}/*
						else
						{
							List newvoices = (List)actnotes.get(i_note);
							if(newvoices != null)
							{
								Iterator iterator = newvoices.iterator();
								while (iterator.hasNext()) {
									SynthesizerVoice newvoice = (SynthesizerVoice) iterator.next();
									newvoice.setActive(0);		
									newvoice.isActive = false;
								}
								actnotes.remove(i_note);
							}
						}*/
					}	
					else
						if(smmsg.getCommand() == ShortMessage.PROGRAM_CHANGE)
						{
							selbank = -1;
							selprogram = -1;											
							currentprogram = smmsg.getData1();
						}
						else
							if(smmsg.getCommand() == ShortMessage.CONTROL_CHANGE)
							{
								switch (smmsg.getData1()) {
								case 0: // BANK MSB (coarse)							
									currentbank = (smmsg.getData2()*128) + (currentbank % 128);
									selbank = -1;
									selprogram = -1;											
									break;
								case 0x20: // BANK LSB (fine)									
									currentbank = (currentbank - (currentbank % 128)) + smmsg.getData2();
									selbank = -1;
									selprogram = -1;											
									break;
								case 0x40: // Hold pedal, hold all notes
									hold_active = smmsg.getData2() >= 0x40;
									if(!hold_active)
									{
										ArrayList delnotes = null;
										{
										  Iterator iterator = actnotes.entrySet().iterator();
										  while (iterator.hasNext()) {
											Map.Entry entry = (Map.Entry) iterator.next();
											int i_note = (Integer)entry.getKey();
											if(!keys_down[i_note])
											{
												boolean ok = true;
												if(sustenuto_notes != null)
												{
													for (int i = 0; i < sustenuto_notes.length; i++) {
														if(sustenuto_notes[i] == i_note)
														{
															ok = false;
															break;
														}
													}
												}
												if(ok)
												{
													if(delnotes == null) delnotes= new ArrayList();												
													delnotes.add(i_note);
												}
											}
										  }
										}
										if(delnotes != null)
										{
											 Iterator iterator2 = delnotes.iterator();
											 while (iterator2.hasNext()) {
												 int i_note = (Integer)iterator2.next();
													List newvoices = (List)actnotes.get(i_note);
													if(newvoices != null)
													{
														Iterator iterator = newvoices.iterator();
														while (iterator.hasNext()) {
															SynthesizerVoice newvoice = (SynthesizerVoice) iterator.next();
															newvoice.setActive(false);		
														}
														actnotes.remove(i_note);
													}														 
											 }
										}
										
									}
									break;
								case 0x42: // Sustenuto pedal, hold only currently active notes
									if(smmsg.getData2() >= 0x40)
									{
										sustenuto_notes = new int[actnotes.size()];
										Iterator iterator = actnotes.keySet().iterator();
										for (int i = 0; i < sustenuto_notes.length; i++) {
											sustenuto_notes[i] = (Integer)iterator.next();										
										}										
									}
									else
									{
										for (int i = 0; i < sustenuto_notes.length; i++) {
											if(!keys_down[sustenuto_notes[i]])
											{
												int i_note = sustenuto_notes[i];
												List newvoices = (List)actnotes.get(i_note);
												if(newvoices != null)
												{
													Iterator iterator = newvoices.iterator();
													while (iterator.hasNext()) {
														SynthesizerVoice newvoice = (SynthesizerVoice) iterator.next();
														newvoice.setActive(false);		
													}
													actnotes.remove(i_note);
												}																		
											}
										}
										sustenuto_notes = null; 
									}
									
									break;
									
								case 0x78: // All Sound Off (GS)									
									Iterator viterator = voices.iterator();
									while (viterator.hasNext()) {
										SynthesizerVoice voice = (SynthesizerVoice)viterator.next();
										voice.softKill();
									}									
									break;

								case 0x7B: // All Notes Off									
									for (int i = 0; i < keys_down.length; i++) {
										if(keys_down[i])
										{
											try
											{
												ShortMessage sm = new ShortMessage();
												sm.setMessage(ShortMessage.NOTE_OFF, 0, i, 0);
												send(sm, -1);
											}
											catch(Exception e)
											{
												e.printStackTrace();
											}
										}
									}									
									break;
									
								default:
									break;
								}
								
							}
					
				}
			}
		}

		int[] sustenuto_notes = null;
		boolean hold_active = false;
		
		
		public void processSkip(int len)
		{
			synchronized(voices)
			{
				Iterator iterator = voices.iterator();
				while (iterator.hasNext()) {
					SynthesizerVoice voice = (SynthesizerVoice)iterator.next(); 
					if(voice.skip(len) == -1)
					{
						voice.reset();
						((RegisterdVoice)voice.registerdvoice).returnVoice(voice);										
						iterator.remove();
					}
				}
			}			
		}
		public void processAudio(double[] buffer, int start, int end)
		{
			//synchronized(voices)
			//{
			Iterator iterator = voices.iterator();
			while (iterator.hasNext()) {
				SynthesizerVoice voice = (SynthesizerVoice)iterator.next();
				if(voice.processId != processCounter)
					if(voice.mix(buffer, start, end) == -1)
					{
						voice.reset();				
						((RegisterdVoice)voice.registerdvoice).returnVoice(voice);
						iterator.remove();
					}
			}
			//}
		}
		
		public void processAudioFull(double[] buffer, int start, int end)
		{
			//synchronized(voices)
			//{
			Iterator iterator = voices.iterator();
			while (iterator.hasNext()) {
				SynthesizerVoice voice = (SynthesizerVoice)iterator.next();
				if(!voice.isActive())
				{
					voice.processId = processCounter;
					if(voice.mix(buffer, start, end) == -1)
					{
						voice.reset();				
						((RegisterdVoice)voice.registerdvoice).returnVoice(voice);
						iterator.remove();
					}
				}
			}
			//}
		}		
		
		int writepos = 0;
		
		public int skip(int len) {
			
			
			killAllVoices();
			
			Iterator iterator = ext_input_streams.entrySet().iterator();
			
			while (iterator.hasNext()) {
				ExtendedStreamReader exr = (ExtendedStreamReader) ((Map.Entry)iterator.next()).getValue();
				exr.skip(len);				
			}			
			
			if(realtime)
			{				
				//processSkip(len);								
				return len;
			}
			
			if((voices.size() == 0) && (midievent == null)) return -1;
			
			//if(stockbuffer == null) stockbuffer = new double[buffer.length];
			//else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];		
			
			long endposition = position + len;
			writepos = 0;
			
			while((nexttime <= endposition) && (midievent != null))
			{
				
				/*
				 *  Process Current Midi Event
				 */
				
				/*
				 Process voices for deltatime			
				 */
				long deltatime = nexttime - position;
				if(deltatime != 0)
				{
					//processSkip((int)deltatime);
				}
				position = nexttime;
				writepos += deltatime;
				
				
				/*
				 Fetch next midi event			
				 */				
				trackpos++;
				if(trackpos < track.size())
				{
					midievent = track.get(trackpos);
					nexttime = AudioEvents.TimeToStreamTime(bmap.getTime(midievent.getTick() * beatfactor), rate, channels);
					if(nexttime < 0) nexttime = 0;
				}
				else
				{
					midievent = null;
				}				 
			}
			
			/*
			 Process voices for deltatime			
			 */			
			long deltatime = endposition - position;
			if(deltatime != 0)
			{			
				//processSkip((int)(deltatime));
			}
			position = endposition;
			
			return len;
		}
		
		ArrayList<MidiMessage> delayed_messages = new ArrayList<MidiMessage>();
		
		long processCounter = 0;
		
		private boolean isNoteOnMessage(MidiMessage mmsg)
		{
			if(!(mmsg instanceof ShortMessage)) return false;
			
			ShortMessage smmsg = (ShortMessage)mmsg;					
			return (smmsg.getCommand() == ShortMessage.NOTE_ON)
					&& (smmsg.getData2() > 0);			
		}
		
		public int mix(double[] buffer, int start, int end) {
			
			
			synchronized (voices) {
				
				processCounter++;
				
				Iterator iterator = ext_input_streams.entrySet().iterator();
				
				while (iterator.hasNext()) {
					ExtendedStreamReader exr = (ExtendedStreamReader) ((Map.Entry)iterator.next()).getValue();
					exr.read(end - start);
				}			
				
				if(realtime)
				{
					processAudio(buffer, start, end);
					
					
					return end - start;
				}
				
				if((voices.size() == 0) && (midievent == null)) return -1;
				
				processAudioFull(buffer, start, end);
				
				//if(stockbuffer == null) stockbuffer = new double[buffer.length];
				//else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];		
				
				long endposition = position + end - start;
				writepos = 0;
				
				delayed_messages.clear();
				
				while((nexttime <= endposition) && (midievent != null))
				{
					
					/*
					 *  Process Current Midi Event
					 */
					MidiMessage mmsg = midievent.getMessage();
					
					if(isNoteOnMessage(mmsg))
						delayed_messages.add(mmsg);
					else
						processMidiMessage(mmsg);
					
					/*
					 Process voices for deltatime			
					 */
					long deltatime = nexttime - position;
					
					// Process delayed note messages
					if(deltatime != 0)
					if(!delayed_messages.isEmpty())
					{
						Iterator<MidiMessage> iter = delayed_messages.iterator();
						while (iter.hasNext()) 
							processMidiMessage(iter.next());
						delayed_messages.clear();
					}
					
					if(deltatime != 0)
					{
						processAudio(buffer, writepos, (int)(writepos + deltatime));
					}
					position = nexttime;
					writepos += deltatime;
					
					
					/*
					 Fetch next midi event			
					 */				
					trackpos++;
					if(trackpos < track.size())
					{
						midievent = track.get(trackpos);
						nexttime = AudioEvents.TimeToStreamTime(bmap.getTime(midievent.getTick() * beatfactor), rate, channels);
						if(nexttime < 0) nexttime = 0;
					}
					else
					{
						midievent = null;
					}				 
				}
				
				// Process delayed note messages
				if(!delayed_messages.isEmpty())
				{
					Iterator<MidiMessage> iter = delayed_messages.iterator();
					while (iter.hasNext()) 
						processMidiMessage(iter.next());
					delayed_messages.clear();
				}				
				
				/*
				 Process voices for deltatime			
				 */			
				long deltatime = endposition - position;
				if(deltatime != 0)
				{			
					processAudio(buffer, writepos, (int)(writepos + deltatime));
				}
				position = endposition;
				
				return end - start;
			}
		}
		
		public int replace(double[] buffer, int start, int end) {
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);			
		}
		public int isStatic(double[] buffer, int len) {
			synchronized(voices)
			{					
				// Athuga hvort einhverjir raddir s�u virkar
				if(voices.size() != 0) return -1;
				
				// Athuga hvort um realtime straum s� a� r��a
				if(!realtime) 
				{
					
					// Athuga hvort vi� s�u � enda hlj��straums
					if((voices.size() == 0) && (midievent == null)) return -1;
					
					// Athuga hvort einhverjar raddir � notast innan �essa t�maramma
					long endposition = position + len;					
					if((nexttime <= endposition) && (midievent != null)) return -1;
					
					position = endposition;
				}
			}
			Iterator iterator = ext_input_streams.entrySet().iterator();
			
			while (iterator.hasNext()) {
				ExtendedStreamReader exr = (ExtendedStreamReader) ((Map.Entry)iterator.next()).getValue();				
				exr.read(len);
			}						
			
			buffer[0] = 0;
			return len;
		}				
		public void killAllVoices()
		{
			Iterator iterator = voices.iterator();
			while (iterator.hasNext()) {
				SynthesizerVoice voice = (SynthesizerVoice)iterator.next();				
				voice.reset();
				((RegisterdVoice)voice.registerdvoice).returnVoice(voice);
			}			
			voices.clear();			
		}
		public void close() {
			
			if(midiseq != null)
				midiseq.removeReceiver(this);
			
			Iterator iterator = voices.iterator();
			while (iterator.hasNext()) {
				SynthesizerVoice voice = (SynthesizerVoice)iterator.next();				
				voice.reset();
				((RegisterdVoice)voice.registerdvoice).returnVoice(voice);
				//voice.close();
			}			
			voices.clear();
			
			
			iterator = registeredvoices.values().iterator();
			while (iterator.hasNext()) {
				((RegisterdVoice)iterator.next()).close();				
			}			
			registeredvoices.clear();
			
			iterator = ext_input_streams.entrySet().iterator();
			while (iterator.hasNext()) {
				ExtendedStreamReader exr = (ExtendedStreamReader) ((Map.Entry)iterator.next()).getValue();
				exr.close();
			}
			ext_input_streams.clear();
			
			
			/*
			 iterator =  voicereusing.iterator();
			 while (iterator.hasNext()) {
			 SynthesizerVoice voice = (SynthesizerVoice)iterator.next(); 
			 voice.close();
			 }
			 voicereusing.clear();*/
			
			//realvoices.clear();
			//programvoices.clear();
			//voicereusing = null;
			//voices.clear();
		}
		
		
		public void send(MidiMessage arg0, long arg1) {
			processMidiMessage(arg0);			
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioRenderNotes implements UnitFactory, MetaDataProvider {
	public static int activevoices = 0;
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Render Notes");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.setHasVarargs(true);
		return metadata;		
	}	
		
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioRenderNotesInstance(parameters);
	}
}