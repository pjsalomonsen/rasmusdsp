/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.midi;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioRegisterVoiceInstance extends UnitInstanceAdapter implements Commitable
{
	Variable output;
	Variable answer = null;
	
	Variable voice;
	Variable keyfrom;
	Variable keyto;
	Variable velfrom;
	Variable velto;
	Variable exclusiveClass;
	Variable name;
	private NameSpace namespace;
	
	public AudioRegisterVoiceInstance(Parameters parameters)
	{

		namespace = parameters.getNameSpace();
		output = parameters.getParameterWithDefault("output");
		voice = parameters.getParameterWithDefault("input");
		keyfrom = parameters.getParameterWithDefault(1, "keyfrom");
		keyto = parameters.getParameterWithDefault(2, "keyto");
		velfrom = parameters.getParameterWithDefault(3, "velfrom");
		velto = parameters.getParameterWithDefault(4, "velto");
		exclusiveClass = parameters.getParameter("exclusiveClass");
		name = parameters.getParameter("name");
		
		if(keyfrom != null) DoublePart.getInstance(keyfrom).addListener(this);
		if(keyto != null) DoublePart.getInstance(keyto).addListener(this);
		if(velfrom != null) DoublePart.getInstance(velfrom).addListener(this);
		if(velto != null) DoublePart.getInstance(velto).addListener(this);
		if(exclusiveClass != null) DoublePart.getInstance(exclusiveClass).addListener(this);
		if(name != null) ObjectsPart.getInstance(name).addListener(this);
		
		calc();				
		

	}
	public void close() {
		
		if(keyfrom != null) DoublePart.getInstance(keyfrom).removeListener(this);
		if(keyto != null) DoublePart.getInstance(keyto).removeListener(this);
		if(velfrom != null) DoublePart.getInstance(velfrom).removeListener(this);
		if(velto != null) DoublePart.getInstance(velto).removeListener(this);
		if(exclusiveClass != null) DoublePart.getInstance(exclusiveClass).removeListener(this);
		if(name != null) ObjectsPart.getInstance(name).removeListener(this);
	}
	public void calc() {
		namespace.addToCommitStack(this);
	}
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit() {
		if(answer != null) output.remove(answer);

		VoiceRecord voicedata = new VoiceRecord();
		voicedata.voice = voice;
		if(keyfrom != null) voicedata.keyfrom = (int)DoublePart.asDouble(keyfrom);
		if(keyto != null) voicedata.keyto = (int)DoublePart.asDouble(keyto);
		if(velfrom != null) voicedata.velfrom = (int)DoublePart.asDouble(velfrom);
		if(velto != null) voicedata.velto = (int)DoublePart.asDouble(velto);
		if(exclusiveClass != null) voicedata.exclusiveClass = (int)DoublePart.asDouble(exclusiveClass);
		if(name != null) voicedata.name = ObjectsPart.toString(name);
		
		answer = ObjectsPart.asVariable(voicedata);		
		output.add(answer);				
	}
	
}

public class AudioRegisterVoice implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Register Voice");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "keyfrom",		"Key from",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2,  "keyto",		"Key to",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3,  "velfrom",		"Velocity from",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4,  "velto",		"Velocity to",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1,  "input",		"Input Voice Generator", null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;		
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioRegisterVoiceInstance(parameters);
	}
}
