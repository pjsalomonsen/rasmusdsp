/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.midi;

import java.util.ArrayList;
import java.util.Arrays;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.Unit;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstance;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

/*
 *   An implementation of wavetable voice synthesis 
 *   that supports for example SoundFont voices
 *   
 *   Current WaveTable Model:
 *
 *   VOL ENV -> Volume Control
 *   MOD LFO -> Volume Control
 *
 *   MOD ENV -> Pitch
 *   MOD LFO -> Pitch
 *   VIB LFO -> Pitch
 *   
 *   MOD LFO -> LowPass Filter
 *   MOD ENV -> LowPass Filter
 *   
 *   Pitch -> [Wave Oscillator] --> [Volume Control] -> [LowPass Filter] --> Output
 *    
 *   Lowpass Filter is -6dB/Oct and is resonance !!!
 *    
 *   ModEnv : Attack is convex and decay, release are linear in Semitones
 *   VolEnv : Attack is convex and decay, release are linear in dB
 *            (or linear in dB)
 *   
 *   -----------------------------------------------
 *   
 *   (a) = audio source
 *   (g) = gain           ( 0  = no gain, 1 = full gain )
 *   (s) = seconds        ( 1  = 1 sec)
 *   (r) = rate           ( 1  = 1 hz)
 *   (n) = note value     ( 60 = middle C note )
 *   (c) = course tune    ( 0  = no tuning, 1 = 1 semitone )
 *   (t) = fine tune      ( 1  = no tuning, 2 = 1 octvave )
 *   (b) = boolean value  ( 0  = false ,  1 = true )
 *   (d) = dB value       ( 0  = 0db  , -10 = -10dB )
 *   (o) = octaves        ( 0  = no change,  1 = up one octave )
 *   
 *   Amplitude Parameters
 *   =====================
 *   velocity(g)
 *   keynum(n)
 *   ch0Vol(g)
 *   ch1Vol(g)
 *   ch2Vol(g)
 *   ch3Vol(g)
 *   initialAttenuation(g)
 *   
 *   Wavetable Parameters
 *   ====================
 *   sample(a)
 *   sampleRate(r)
 *   scaleTune(t)
 *   coarseTune(c)
 *   
 *   Volume & Modulation Envelopes
 *   =============================
 *   * = { delay(s), attack(s), hold(s), decay(s), sustain(s), release(s) }
 *   *VolEnv                  : Volume envelope     (AHDSR)
 *   *ModEnv                  : Modulation envelope (AHDSR)
 *   
 *   keynumToModEnvHold(n)
 *   keynumToModEnvDecay(n)
 *   keynumToVolEnvHold(n)
 *   keynumToVolEnvDecay(n)
 *   
 *   Low Frequency Oscillators
 *   ==========================
 *   * = { delay(s), freq(r) }
 *   *ModLFO  : Modulation Low Frequency Oscillator (Table based, size=4096)
 *   *VibLFO  : Vibration  Low Frequency Oscillator (Table based, size=4096)
 *   
 *   modLfoToPitch(o)
 *   modLfoToFilterFc(o)
 *   modLfoToVolume(g)
 *   vibLfoToPitch(o)
 *     
 *   Filtering Parameters
 *   ====================
 *   lowpass(b)         : LP 1-pole resonant RC-C lowpass filter
 *   initialFilterFc(r) 
 *   initialFilterQ(d) 
 *   
 *   
 *   
 */

class AudioVoiceInstance extends UnitInstanceAdapter implements AudioStreamable 
{	
	Variable output;
	Variable note;
	Variable velocity;
	Variable active;
	Variable answer;
	Variable tuning;
	AudioVoiceFactoryInstance factory;
	
	class AudioVoiceStream implements AudioStream
	{		
		final static double MINUS40DB  = 0.01; // 0.000001;
		final static double MINUS60DB  = 0.001; // 0.000001;
		final static double MINUS70DB  = 0.000316228; // 0.0000001;
		final static double MINUS80DB  = 0.0001;  // 0.00000001;
		final static double MINUS100DB = 0.00001; //0.0000000001;
		
		final static double MOD_CONVEX_START_FACTOR = 0.0001; 
		
		AudioSession session;
		AudioStream samplestream = null;
		AudioCache audiocache;
		
		int channels;
		
		boolean eof = false;
		//DoublePart active;
		double streamfactor = 0;
		double velocity;
		double rate;
		
		int volEnvMode = 0;  // Attack, Hold, Decay, Sustain
		double volEnvAttackStep = 0;
		double volEnvDecayStep = 0;
		double volEnvReleaseStep = 0;		
		double volEnvSustain = 0;
		double volEnvCurrentValue = 0;		
		long volHoldCounter = 0;
		long volDelayCounter = 0;
		
		int modEnvMode_Pitch = 0;  // Attack, Hold, Decay, Sustain
		int modEnvMode_Freq = 0;  // Attack, Hold, Decay, Sustain
		boolean modEnvActive = false;
		
		double modEnvAttackOffset_Freq = 0;
		double modEnvAttackStep_Freq = 0;
		double modEnvDecayStep_Freq = 0;
		double modEnvReleaseStep_Freq = 0;	
		
		double modEnvAttackOffset_Pitch = 0;
		double modEnvAttackStep_Pitch = 0;
		double modEnvDecayStep_Pitch = 0;
		double modEnvReleaseStep_Pitch = 0;
		
		long modEnvAttackCounter = 0;
		double modEnvReleaseTime = 0;
		boolean modEnvReleaseCalculated_Pitch = false;
		boolean modEnvReleaseCalculated_Freq = false;
		
		double modEnvSustain = 1;
		double modEnvCurrentValue_Freq = 1;		
		double modEnvCurrentValue_Pitch = 1;		
		long modHoldCounter_Freq = 0;
		long modDelayCounter_Freq = 0;		
		long modHoldCounter_Pitch = 0;
		long modDelayCounter_Pitch = 0;		
		
		boolean modEnvEof_Pitch = false;
		boolean modEnvEof_Freq = false;
		
		double modEnvToFilterFc = 0;
		double modEnvToPitch = 0;		
		
		double initialFilterFc = 0; 
		double initialFilterQ = 0;
		double currentFilterFc = 0;
		
		double[] channelsVols; 
		
		
//		http://www.musicdsp.org/archive.php?classid=3#64
//		Type : LP 2-pole resonant tweaked butterworth
//		References : Posted by daniel_jacob_werner [AT] yaho [DOT] com [DOT] au
//		resonance is in dB,   cutoff is in Hz		
		double a0,a1,a2,b1,b2,q;
		double xn_2 = 0;
		double xn_1 = 0;
		double yn_2 = 0;					
		double yn_1 = 0;	
		
		
		boolean vibActive = false;
		boolean modActive = false;
		double vibCounter = 0;
		double modCounter = 0;
		double freqModLFO_step;
		double freqVibLFO_step;		
		int delayModLFO_counter = 0;
		int delayVibLFO_counter = 0;
		
		double[] lfoTable_vibToPitch = null;     	
		double[] lfoTable_modToPitch = null;
		double[] lfoTable_modToFilterFc = null;
		double[] lfoTable_modToVolume = null;		
		
		// Lowpass filter like FluidSynth use
		// which is based on Robert Bristow-Johnson Cookbook
		//    http://www.musicdsp.org/files/biquad.c
		// really not usable, doesn't sound right!!!
		/*
		 public void initLP3(double cutoff, double resonancedB)
		 {			
		 double resonance = Math.pow(10.0, -(resonancedB / 20));
		 q = resonance;
		 
		 double omega = 2.0 * Math.PI * cutoff / rate;
		 double sn    = Math.sin(omega);
		 double cs    = Math.cos(omega);
		 double alpha = q * sn * 0.5; // same way as fluidsynth does it
		 
		 double a0_inv = 1.0 / (1.0 + alpha); 
		 a1 = -2.0 * cs * a0_inv;
		 a2 = (1.0 - alpha) * a0_inv;
		 b1 = (1.0 - cs) * a0_inv * q;
		 b2 = b1 * 0.5;
		 
		 }
		 public void calcLP3(double[] buffer, int start, int end)
		 {
		 double a1=this.a1;
		 double a2=this.a2;
		 double b1=this.b1;
		 double b2=this.b2;
		 double xn_1 = this.xn_1;
		 double xn_2 = this.xn_2;
		 
		 int recalcrate = (int)(rate / 100.0); // 100 Hz
		 int i = start;
		 while(i < end)
		 {
		 if(xn_1 > 0.0) { if(xn_1 < 1.0E-10) xn_1 = 0; }
		 else
		 if(xn_1 > -1.0E-10) xn_1 = 0;				
		 if(xn_2 > 0.0) { if(xn_2 < 1.0E-10) xn_2 = 0; }
		 else
		 if(xn_2 > -1.0E-10) xn_2 = 0;										
		 
		 int len = i + recalcrate;
		 if(len > end) len = end;
		 for (; i < len; i++) {
		 double c = buffer[i] - a1 * xn_1 - a2 * xn_2;
		 buffer[i] = b2 * (c + xn_2) + b1 * xn_1;
		 xn_2 = xn_1;
		 xn_1 = c;
		 }
		 }
		 
		 this.xn_1 = xn_1;
		 this.xn_2 = xn_2;			
		 }
		 
		 
		 
		 public void calcLP3(double[] buffer, double[] freq, int start, int end )
		 {
		 double a1=this.a1;
		 double a2=this.a2;
		 double b1=this.b1;
		 double b2=this.b2;
		 double xn_1 = this.xn_1;
		 double xn_2 = this.xn_2;
		 
		 double a1_delta = 0;
		 double a2_delta = 0;
		 double b1_delta = 0;
		 double b2_delta = 0;
		 
		 
		 int recalcrate = (int)(rate / 100.0); // 100 Hz
		 int i = start;
		 while(i < end)
		 {
		 if(xn_1 > 0.0) { if(xn_1 < 1.0E-10) xn_1 = 0; }
		 else
		 if(xn_1 > -1.0E-10) xn_1 = 0;				
		 if(xn_2 > 0.0) { if(xn_2 < 1.0E-10) xn_2 = 0; }
		 else
		 if(xn_2 > -1.0E-10) xn_2 = 0;										
		 
		 
		 int len = i + recalcrate;
		 if(len > end) len = end;
		 double f = freq[i];
		 if(f != currentfreqmod)
		 {
		 currentfreqmod = f;
		 
		 double calcfreq = initialFilterFc * currentfreqmod;
		 if(calcfreq < 100) calcfreq = 100;                   // MIN
		 if(calcfreq > (rate*0.45)) calcfreq = (rate*0.45); // MAX
		 
		 double omega = 2.0 * Math.PI * calcfreq / rate;
		 double sn    = Math.sin(omega);
		 double cs    = Math.cos(omega);
		 double alpha = q * sn * 0.5; // same way as fluidsynth does it
		 
		 double a0_inv = 1.0 / (1.0 + alpha); 
		 double new_a1 = -2.0 * cs * a0_inv;
		 double new_a2 = (1.0 - alpha) * a0_inv;
		 double new_b1 = (1.0 - cs) * a0_inv * q;
		 double new_b2 = b1 * 0.5;
		 double fixln = 1.0 / ((double)(len - i));
		 a1_delta = (new_a1 - a1) * fixln;
		 a2_delta = (new_a2 - a2) * fixln;
		 b1_delta = (new_b1 - b1) * fixln;
		 b2_delta = (new_b2 - b2) * fixln;
		 }
		 for (; i < len; i++) {
		 
		 a1 += a1_delta;
		 a2 += a2_delta;
		 b1 += b1_delta;
		 b2 += b2_delta;
		 
		 double c = buffer[i] - a1 * xn_1 - a2 * xn_2;
		 buffer[i] = b2 * (c + xn_2) + b1 * xn_1;
		 xn_2 = xn_1;
		 xn_1 = c;
		 
		 }
		 
		 a1_delta = 0;
		 a2_delta = 0;
		 b1_delta = 0;
		 b2_delta = 0;
		 
		 }
		 
		 
		 
		 this.xn_1 = xn_1;
		 this.xn_2 = xn_2;
		 this.a1=a1;
		 this.a2=a2;
		 this.b1=b1;
		 this.b2=b2;
		 
		 }
		 */
		
		double v0 = 0;
		double v1 = 0;
		double s = 0;
		double dc = 0; //1.77; //Math.PI / 8;
		
		public void initLP(double cutoff, double resonancedB)
		{
			currentfreq = cutoff;		
			// 7/6 is a frequency correction for the filter !!!!!
			if(cutoff < 120) cutoff = 120;
			double c = (7.0/6.0) * Math.PI * 2 * cutoff / rate;		   
			if(c > 0.44*Math.PI) c = 0.44*Math.PI; // get's unstable at 0.45
			a0 = Math.sqrt(1 - Math.cos(c)) * Math.sqrt(0.5*Math.PI);
			if(resonancedB < 0) resonancedB = 0;  
			if(resonancedB > 20) resonancedB = 20; // in soundblaster EMU 20dB is the max
			q = Math.sqrt(0.5) * Math.pow(10.0, -(resonancedB / 20));  // Math.pow(0.5, (resonancedB*5.0/26.0));
			//q = Math.pow(0.5, 0.5 + (resonancedB*5.0/26.0)); <-- �ESSI VIRKA�I
			
			//double resonance = Math.pow(10.0, -(resonancedB / 20));
			// 2000 -> 1781
			
			// 36
		}		
		
		// RC filter 6 dB/oct
		public void calcLP(double[] buffer, int start, int end)
		{
			
			//initialFilterQ = 0;
			
			// SoundBlaster fer ekki h�rra en 8250 hZ
			//dc += 0.01;
			//if(dc > Math.PI*0.45) dc = Math.PI*0.45;
			
			// DC er fr� 0 til 2*PI, �ar sem 2*PI er rate/2
			// �egar resonance er 0 �� m� DC ekki vera h�rra en 0.45*PI 
			
			//	 c = Math.PI - (Math.sin((c - Math.PI) - Math.PI/2)+1); //*2;
			//else
			
			//double c = Math.sqrt((Math.sin(dc - Math.PI/2)+1)*0.5*Math.PI); //*2;
			
			//double c = Math.sqrt((1 - Math.cos(dc))*0.5*Math.PI); //*2;
			
			//double c = Math.sqrt(1 - Math.cos(dc)) * Math.sqrt(0.5*Math.PI); 
			
			
			// c er � bilinu 0 til sqrt(pi) 
			
			//c = Math.asin(c - 1) + Math.PI / 2;
			
			// �essi R gefur svipa� og 20 dB � SF2
			//double r = Math.pow(0.5, (30+24) / 16.0);
			
			
			//s = 40; //+= 0.1;
			// �essi R gefur svipa� og 20 dB � SF2
			//double r = Math.pow(0.5, (0+8 + s) / 16.0);
			
			
			
			//double r = Math.pow(0.5, 0.5); // 0.5 gefur engan resonance
			//double r = Math.pow(0.5, 5.0);   // 5.0 gefur 24 dB resonance
			//double r = Math.pow(0.5, 2.75);    // 5.0 gefur 12 dB resonance
			
			// 0.125 gefur 12 dB � resonance
			
			
			//System.out.println("freq = " + dc*22050.0/Math.PI + ", r = " + r);
			
//			Loop:
			
			double c = a0;
			double r = q;
			double v1 = this.v1;
			double v0 = this.v0;
			
			
			int recalcrate = (int)(rate / 100.0); // 100 Hz
			int i = start;
			while(i < end)
			{
				int len = i + recalcrate;
				if(len > end) len = end;
				
				if(v1 > 0.0) { if(v1 < 1.0E-10) v1 = 0; }
				else
					if(v1 > -1.0E-10) v1 = 0;				
				
				for (; i < len; i++) {
					v0 = (1-r*c)*v0 - (c)*v1 + (c)*buffer[i];
					v1 = (1-r*c)*v1 + (c)*v0;
					buffer[i] = v1;
				}
			}
			
			this.v1 = v1;			 
			this.v0 = v0;
			/*
			 double x = 2*Math.PI*16000/rate;
			 x = (2-Math.cos(x));
			 
			 double p = (2-Math.cos(x)) - Math.sqrt(x*x - 1);
			 p = p*p;
			 
			 for (int i = start; i < end; i++) {
			 buffer[i] = v0 = (1-p)*buffer[i] + p*v0;
			 }*/
			
			
			
			/*
			 recursion: tmp = (1-p)*in + p*tmp with output = tmp
			 coefficient: p = (2-cos(x)) - sqrt((2-cos(x))^2 - 1) with x = 2*pi*cutoff/samplerate
			 coeficient approximation: p = (1 - 2*cutoff/samplerate)^2*/
			
		}		
		
		public void calcLP(double[] buffer, double[] freq, int start, int end )
		{
			
			
			double a0 = this.a0;
			double r = q;
			double v1 = this.v1;
			double v0 = this.v0;
			
			double a0_delta = 0;
			
			
			int recalcrate = (int)(rate / 250.0); // 100 Hz
			int i = start;
			
			
			
			
			while(i < end)
			{
				int len = i + recalcrate;
				if(len > end) len = end;
				
				if(currentfreqmod != freq[len - 1])
				{ 
					//System.out.println("freq[i] = " + initialFilterFc * freq[i]);
					currentfreqmod = freq[len - 1];
					double cutoff = initialFilterFc * freq[len -1];
					if(cutoff < 120) cutoff = 120;
					
					//c = Math.PI * 2 * calcfreq / rate;
					double c = (7.0/6.0) * Math.PI * 2 * cutoff / rate;
					if(c > 0.44*Math.PI) c = 0.44*Math.PI; // get's unstable at 0.45
					double a0_new = Math.sqrt(1 - Math.cos(c)) * Math.sqrt(0.5*Math.PI);
					
					double fixlen = len - i;
					a0_delta = (a0_new - a0) / fixlen; 
				}
				
				
				if(v1 > 0.0) { if(v1 < 1.0E-10) v1 = 0; }
				else
					if(v1 > -1.0E-10) v1 = 0;				
				
				for (; i < len; i++) {
					a0 += a0_delta;
					v0 = (1-r*a0)*v0 - (a0)*v1 + (a0)*buffer[i];
					v1 = (1-r*a0)*v1 + (a0)*v0;
					buffer[i] = v1;
					
				}
				a0_delta = 0;
			}
			
			if(end - start > 0)
			{
				currentfreqmod = freq[end-1];
				double cutoff = initialFilterFc * freq[end-1];
				if(cutoff < 120) cutoff = 120;
				
				//c = Math.PI * 2 * calcfreq / rate;
				double c = (7.0/6.0) * Math.PI * 2 * cutoff / rate;
				if(c > 0.44*Math.PI) c = 0.44*Math.PI; // get's unstable at 0.45
				a0 = Math.sqrt(1 - Math.cos(c)) * Math.sqrt(0.5*Math.PI);			
			}			
			
			this.v1 = v1;
			this.v0 = v0;
			this.a0 = a0;
			
		}
		
		
		// tweaked butterworth lowpass filter
		// 12 dB/oct, resonant  
		public void initLP2(double cutoff, double resonancedB)
		{
			currentfreq = cutoff; 
			double c = 1.0 / (Math.tan(Math.PI * (cutoff / rate)));
			double csq = c * c;
			double resonance = Math.pow(10.0, -(resonancedB / 20));
			q = Math.sqrt(2.0f) * resonance;
			//q = 1;
			a0 = 1.0 / (1.0 + (q * c) + (csq));
			a1 = 2.0 * a0;
			a2 = a0;
			b1 = (2.0 * a0) * (1.0 - csq);
			b2 = a0 * (1.0 - (q * c) + csq);		
			
		}
		
		
		
		public void calcLP2(double[] buffer, int start, int end)
		{
			
			double a0=this.a0,a1=this.a1,a2=this.a2,b1=this.b1,b2=this.b2;		
			double xn_1 = this.xn_1;
			double xn_2 = this.xn_2;
			double yn_1 = this.yn_1;
			double yn_2 = this.yn_2;
			
			int recalcrate = (int)(rate / 100.0); // 100 Hz
			
			int i = start;
			while(i < end)
			{
				int len = i + recalcrate;
				if(len > end) len = end;
				for (; i < len; i++) {
					double xn = buffer[i];				
					//double yn = a0 * (b0*xn + b1*xn_1 + b2*xn_2 - a1*yn_1 - a2*yn_2);
					double yn = (a0*xn + a1*xn_1 + a2*xn_2 - b1*yn_1 - b2*yn_2);
					
					
					/*
					 if(yn > 0.0) { if(yn < 1.0E-10) yn = 0; }
					 else
					 if(yn > -1.0E-10) yn = 0;	*/					
					
					buffer[i] = yn;
					xn_2 = xn_1;
					xn_1 = xn;
					yn_2 = yn_1;					
					yn_1 = yn;				
				}
				if(yn_1 > 0.0) { if(yn_1 < 1.0E-10) yn_1 = 0; }
				else
					if(yn_1 > -1.0E-10) yn_1 = 0;				
				if(yn_2 > 0.0) { if(yn_2 < 1.0E-10) yn_2 = 0; }
				else
					if(yn_2 > -1.0E-10) yn_2 = 0;										
				
			}
			
			this.xn_1 = xn_1;
			this.xn_2 = xn_2;
			this.yn_1 = yn_1;
			this.yn_2 = yn_2;
			
		}
		
		//int recalccounter = 0;
		double currentfreq = 0;
		double currentfreqmod = 1;
		public void calcLP2(double[] buffer, double[] freq, int start, int end )
		{
			
			double initialFilterFc = this.initialFilterFc;
			double a0=this.a0,a1=this.a1,a2=this.a2,b1=this.b1,b2=this.b2,q=this.q,rate=this.rate;		
			double xn_1 = this.xn_1;
			double xn_2 = this.xn_2;
			double yn_1 = this.yn_1;
			double yn_2 = this.yn_2;
			double currentfreqmod = this.currentfreqmod;
			/*
			 double a0_delta = 0;
			 double a1_delta = 0;
			 double a2_delta = 0;
			 double b1_delta = 0;
			 double b2_delta = 0;*/
			
			int recalcrate = (int)(rate / 100.0); // 100 Hz
			
			int i = start;
			while(i < end)
			{
				int len = i + recalcrate;
				if(len > end) len = end;
				
				if(currentfreqmod != freq[i])
				{ 
					//System.out.println("freq[i] = " + initialFilterFc * freq[i]);
					currentfreqmod = freq[i];
					double calcfreq = initialFilterFc * freq[i];
					if(calcfreq < 200) calcfreq = 200;                   // MIN
					if(calcfreq > (rate*0.45)) calcfreq = (rate*0.45); // MAX
					double c = 1.0 / (Math.tan(Math.PI * (calcfreq / rate)));
					double csq = c * c;
					a0 = 1.0 / (1.0 + (q * c) + (csq));
					a1 = 2.0 * a0;
					a2 = a0;
					b1 = (2.0 * a0) * (1.0 - csq);
					b2 = a0 * (1.0 - (q * c) + csq);
					
					//double fixln = 1.0 / ((double)(len - i));
					/*
					 a0_delta = (new_a0 - a0) * fixln;
					 a1_delta = (new_a1 - a1) * fixln;
					 a2_delta = (new_a2 - a2) * fixln;
					 b1_delta = (new_b1 - b1) * fixln;
					 b2_delta = (new_b2 - b2) * fixln;*/
					
				}
				if(yn_1 > 0.0) { if(yn_1 < 1.0E-10) yn_1 = 0; }
				else
					if(yn_1 > -1.0E-10) yn_1 = 0;
				if(yn_2 > 0.0) { if(yn_2 < 1.0E-10) yn_2 = 0; }
				else
					if(yn_2 > -1.0E-10) yn_2 = 0;
				
				
				for (; i < len; i++) 
				{
					
					/*
					 a0 += a0_delta;
					 a1 += a1_delta;
					 a2 += a2_delta;
					 b1 += b1_delta;
					 b2 += b2_delta;*/
					
					double xn = buffer[i];				
					//double yn = a0 * (b0*xn + b1*xn_1 + b2*xn_2 - a1*yn_1 - a2*yn_2);
					double yn = (a0*xn + a1*xn_1 + a2*xn_2 - b1*yn_1 - b2*yn_2);
					/*
					 if(yn > 0.0) { if(yn < 1.0E-10) yn = 0; }
					 else
					 if(yn > -1.0E-10) yn = 0;										
					 */
					buffer[i] = yn;
					
					
					xn_2 = xn_1;
					xn_1 = xn;
					yn_2 = yn_1;					
					yn_1 = yn;
					
					
					
					
					
					
				}
				/*
				 a0_delta = 0;
				 a1_delta = 0;
				 a2_delta = 0;
				 b1_delta = 0;
				 b2_delta = 0;*/			
			}
			this.currentfreqmod = currentfreqmod;
			//this.recalccounter = recalccounter;
			
			this.a0=a0;
			this.a1=a1;
			this.a2=a2;
			this.b1=b1;
			this.b2=b2;
			
			this.xn_1 = xn_1;
			this.xn_2 = xn_2;
			this.yn_1 = yn_1;
			this.yn_2 = yn_2;			
		}
		
		boolean b_lowpass;
		AudioStream pitchstream = null;
		AudioStream activestream = null;
		boolean activestream_eof = false;
		
		public AudioVoiceStream(AudioSession session)
		{			
			
			
			if(pitch != null)
			{
				pitchstream = AudioEvents.openStream(pitch, session.getMonoSession());
			}
			
			activestream = AudioEvents.openStream(active, session.getMonoSession());
			
			this.session = session;
			this.b_lowpass = factory.b_lowpass;
			
			//System.out.println("factory.d_initialFilterFc = " + factory.d_initialFilterFc);
			if(factory.d_initialFilterFc >= 19900) b_lowpass = false;
			
			audiocache = session.getAudioCache();
			rate = session.getRate();
			channels = session.getChannels();		
			
			delayModLFO_counter = (int)(rate*factory.d_delayModLFO);
			delayVibLFO_counter = (int)(rate*factory.d_delayVibLFO);			
			freqModLFO_step = (factory.d_freqModLFO / rate) * AudioVoiceFactoryInstance.lfoTable_size;
			freqVibLFO_step = (factory.d_freqVibLFO / rate) * AudioVoiceFactoryInstance.lfoTable_size;
			
			this.lfoTable_vibToPitch = factory.lfoTable_vibToPitch;			
			this.lfoTable_modToPitch  = factory.lfoTable_modToPitch ;
			this.lfoTable_modToFilterFc  = factory.lfoTable_modToFilterFc ;
			this.lfoTable_modToVolume  = factory.lfoTable_modToVolume ;
			
			if(lfoTable_vibToPitch != null) vibActive = true;		
			if(lfoTable_modToPitch != null) modActive = true;
			if(lfoTable_modToFilterFc != null) modActive = true;
			if(lfoTable_modToVolume != null) modActive = true;
			
			int i_note = (int)DoublePart.asDouble(note);
			double i_velocity = DoublePart.asDouble(AudioVoiceInstance.this.velocity);
			if(factory.i_keynum != -1) i_note = factory.i_keynum; 
			if(factory.i_velocity != -1) i_velocity = factory.i_velocity/127.0; 
			
			//active = DoublePart.getInstance(AudioVoiceInstance.this.active);
			
			AudioEvents audioevents = AudioEvents.getInstance(factory.sample);
			if(audioevents.track.size() == 1)
			{
				AudioEvent aevent = (AudioEvent)audioevents.track.get(0);
				if(aevent.start == 0)
				{
					samplestream = aevent.streamable.openStream(session.getMonoSession());
				}
			}
			if(samplestream == null)
			{
				samplestream = AudioEvents.openStream(factory.sample, session.getMonoSession());
			}
			
			// Calculate Volume and Pitch Factor
			if(factory.d_scaleTune == 0)
			{
				streamfactor = (factory.d_sampleRate / rate);
			}
			else
				if(tuning_out != null)
				{
					streamfactor = (factory.d_sampleRate / rate)*Math.pow(factory.d_scaleTune, (factory.d_coarseTune) / 12.0) * DoublePart.asDouble(tuning_out);				
				}
				else
					streamfactor = (factory.d_sampleRate / rate)*Math.pow(factory.d_scaleTune, (factory.d_coarseTune + i_note) / 12.0);
			
			velocity = factory.d_initialAttenuation * i_velocity;
			if(velocity == 0) eof = true;
			channelsVols = factory.d_channelsVols;
			
			// Calculation Filter Constants
			
			//double i_fc = (1 - i_velocity) - i_note/127.0 ; // TODO LAGA
			//if(i_fc < 0) i_fc = 0;
			//if(i_fc > 1) i_fc = 1;
			
			if(velocity < 0.5)
				this.initialFilterFc = factory.d_initialFilterFc * ((velocity * 2)*0.75 + 0.25);
			else
				this.initialFilterFc = factory.d_initialFilterFc;
			
			//this.initialFilterFc = factory.d_initialFilterFc * Math.pow(4, -i_fc);
			if(initialFilterFc > 20000) initialFilterFc = 20000;
			this.initialFilterQ = factory.d_initialFilterQ;
			
			if(b_lowpass)
			{
				if(initialFilterQ < 0) initialFilterQ = 0;
				if(initialFilterQ != 0)
				{
					//initialFilterQ = Math.abs(initialFilterQ);
					velocity = velocity *  ( Math.pow(10, -((initialFilterQ))/40.0) ) ;
				}
				initLP(initialFilterFc, initialFilterQ);
			}
			
			
			// Calculation Modulation Envelope Constants for LowPass Filter and Pitch
			if(factory.d_modEnvToFilterFc != 0 || factory.d_modEnvToPitch != 0)
			{
				
				modEnvToFilterFc = factory.d_modEnvToFilterFc;
				modEnvToPitch = factory.d_modEnvToPitch;
				
				modEnvActive = true;
				
				double decayKeyNumModifier;
				if(factory.d_keynumToModEnvDecay != 0)
					decayKeyNumModifier = Math.pow(2, -((i_note-60)/12)*factory.d_keynumToModEnvDecay);
				else
					decayKeyNumModifier = 1;
				double holdKeyNumModifier;
				if(factory.d_keynumToModEnvHold != 0)
					holdKeyNumModifier = Math.pow(2, -((i_note-60)/12)*factory.d_keynumToModEnvHold);
				else
					holdKeyNumModifier = 1;				
				
				// Einhvera hluta �arf a� margfalda me� * 0.5 � SoundBlaster EMU
				long l_attack = (long)(factory.d_attackModEnv * rate * 0.5);
				long l_decay = (long)(decayKeyNumModifier * factory.d_decayModEnv * rate * 0.5);
				
				long l_release = (long)(factory.d_releaseModEnv * rate * 0.5);
				
				modEnvSustain = factory.d_sustainModEnv;
				modHoldCounter_Freq =  (long)(holdKeyNumModifier * factory.d_holdModEnv * rate);
				modDelayCounter_Freq =  (long)(factory.d_delayVolEnv * rate);
				modHoldCounter_Pitch =  (long)(holdKeyNumModifier * factory.d_holdModEnv * rate);
				modDelayCounter_Pitch =  (long)(factory.d_delayVolEnv * rate);
				
				modEnvCurrentValue_Freq = 1;		
				modEnvCurrentValue_Pitch = 1;		
				
				if(l_attack == 0)
				{
					modEnvMode_Pitch = 1;
					modEnvMode_Freq = 1;
					
					modEnvCurrentValue_Freq = Math.pow(2, modEnvToFilterFc);
					modEnvCurrentValue_Pitch = Math.pow(2, modEnvToPitch);
				}
				else
				{
					
					modEnvAttackOffset_Pitch = Math.pow(2,modEnvToPitch);
					modEnvAttackOffset_Freq = Math.pow(2,modEnvToFilterFc);
					
					modEnvAttackCounter = l_attack;
					modEnvAttackStep_Pitch = Math.pow(MOD_CONVEX_START_FACTOR, 1.0 / l_attack);
					modEnvAttackStep_Freq = Math.pow(MOD_CONVEX_START_FACTOR, 1.0 / l_attack);
					
					//modEnvAttackStep_Pitch = Math.pow((Math.pow(2, 1 - modEnvAttackOffset_Pitch) * MOD_CONVEX_START_FACTOR) / (1 - Math.pow(2, modEnvAttackOffset_Pitch)), 1.0 / l_attack);
					//modEnvAttackStep_Freq = Math.pow((Math.pow(2, 1 - modEnvAttackOffset_Freq) * MOD_CONVEX_START_FACTOR) / (1 - Math.pow(2, modEnvAttackOffset_Freq)), 1.0 / l_attack);
					
					
					// modEnvAttackStep_Pitch = Math.pow((Math.pow(2, modEnvToPitch)/1.0), 1.0 / l_attack);
					// modEnvAttackStep_Freq = Math.pow((Math.pow(2, modEnvToFilterFc)/1.0), 1.0 / l_attack);
					
					
				}
				
				if(l_decay != 0)
				{
					modEnvDecayStep_Pitch = Math.pow(((Math.pow(2, modEnvSustain*modEnvToPitch))/Math.pow(2, modEnvToPitch)), 1.0 / l_decay);
					modEnvDecayStep_Freq = Math.pow(((Math.pow(2, modEnvSustain*modEnvToFilterFc))/Math.pow(2, modEnvToFilterFc)), 1.0 / l_decay);
				}
				if(l_release != 0)
				{
					modEnvReleaseTime = l_release;
					modEnvReleaseCalculated_Pitch = false;
					modEnvReleaseCalculated_Freq = false;
					
				}
				
				
			}
			
			// Calculation Volume Envelope Constants
			{
				
				double decayKeyNumModifier;
				if(factory.d_keynumToVolEnvDecay != 0)
					decayKeyNumModifier = Math.pow(2, -((i_note-60)/12)*factory.d_keynumToVolEnvDecay);
				else
					decayKeyNumModifier = 1;
				double holdKeyNumModifier;
				if(factory.d_keynumToVolEnvHold != 0)
					holdKeyNumModifier = Math.pow(2, -((i_note-60)/12)*factory.d_keynumToVolEnvHold);
				else
					holdKeyNumModifier = 1;					
				
				long l_attack = (long)(factory.d_attackVolEnv * rate);
				long l_decay = (long)(decayKeyNumModifier * factory.d_decayVolEnv * rate);
				double f_sustain = factory.d_sustainVolEnv * velocity;
				long l_release = (long)(factory.d_releaseVolEnv * rate);
				
				volHoldCounter =  (long)(holdKeyNumModifier * factory.d_holdVolEnv * rate);
				volDelayCounter =  (long)(factory.d_delayVolEnv * rate);
				volEnvSustain = f_sustain;
				
				
				if(l_attack == 0)
				{
					volEnvMode = 1;
					volEnvCurrentValue = velocity;
				}
				else
					volEnvAttackStep = velocity / (l_attack);
				
				if(l_decay != 0) volEnvDecayStep = Math.pow((f_sustain/velocity), 1.0 / l_decay);
				if(l_release != 0) volEnvReleaseStep = Math.pow((MINUS100DB/velocity), 1.0 / l_release);
				
				
			}
			
		}
		
		double[] rbuffer = new double[2048];
		double rbuffer_pos = 2048-2;
		
		public void readNextInputBuffer()
		{
			for (int i = 0; i < 2; i++) {
				rbuffer[i] = rbuffer[i + 2048 - 2];
			}
			int ret = samplestream.replace(rbuffer, 2, 2048);
			if(ret == -1)
			{
				Arrays.fill(rbuffer, 2, 2048,0);
				eof = true;
			}
			else
				if(ret != 2048 - 2)
				{
					Arrays.fill(rbuffer, ret+2, 2048, 0);
					//eof = true;
				}
		}
		
		
		double[] sbuff = new double[1];
		public int readActiveStream(int len)
		{
			if(activestream_eof) return 0;
			
			int clen = len / channels;
			int ret = activestream.isStatic(sbuff, clen);
			
			if(ret != -1)
			{						
				if(sbuff[0] < 0.5)
				{
					activestream_eof = true;
					return 0;				
				}
				if(ret != clen) activestream_eof = true;
				return ret * channels;
			}
			
			double[] cbuffer = audiocache.getBuffer(clen);
			ret = activestream.replace(cbuffer, 0, clen);			
			if(ret == -1)
			{
				audiocache.returnBuffer(cbuffer);
				activestream_eof = true;
				return 0;
			}				
			for (int i = 0; i < clen; i++) 
			{
				if(cbuffer[i] < 0.5)
				{
					audiocache.returnBuffer(cbuffer);
					activestream_eof = true;
					return i * channels;
				}
			}		
			audiocache.returnBuffer(cbuffer);
			if(ret != clen) activestream_eof = true;
			return ret * channels;
		}
		
		
		public int mix(double[] buffer, int start, int end) {
			
			int activelen = readActiveStream(end - start);
			if(activelen == 0) return mix(buffer, start, end, false);
			if(activelen == end - start) return mix(buffer, start, end, true);			
			int bend = start + activelen;
			int ret = mix(buffer, start, bend, true);
			if(ret < bend - start) return ret;
			int ret2 = mix(buffer, bend, end, false);
			if(ret == -1) return ret;
			return ret + ret2;
		}		
		
		public int mix(double[] buffer, int start, int end, boolean active) {
			
			if(eof) return -1;
			
			boolean noteActive = active; //active.getDouble() > 0.5;
			int channels = this.channels;			
			int cstart = start / channels;
			int cend = end / channels;
			double[] cbuffer = audiocache.getBuffer(cend);
			boolean dynamic_pitch = false;
			boolean constantPitch = false;
			
			//*********************************
			// Preparing VIB and MOD LFO's
			//*********************************				
			int vibStart = cend;
			int modStart = cend;
			if(vibActive)
			{
				if(delayVibLFO_counter != 0)
				{
					delayVibLFO_counter -= (cend - cstart);
					if(delayVibLFO_counter < 0)
					{						
						vibStart += delayVibLFO_counter;
						delayVibLFO_counter = 0;
					}
				}
				else
					vibStart = cstart;
			}
			if(modActive)
			{
				if(delayModLFO_counter != 0)
				{
					delayModLFO_counter -= (cend - cstart);
					if(delayModLFO_counter < 0)
					{			
						modStart += delayModLFO_counter;
						delayModLFO_counter = 0;
					}
				}
				else
					modStart = cstart;
			}			
			
			
			//*********************************
			// Apply pitch modulation envelope
			//*********************************			
			if(modEnvActive && (modEnvToPitch != 0) && !modEnvEof_Pitch)
			{
				dynamic_pitch = true;
				double modEnvCurrentValue_Pitch = this.modEnvCurrentValue_Pitch;
				int ix = cstart;
				if(noteActive)
				{
					if(modDelayCounter_Pitch != 0) // DELAY
					{				
						if(modDelayCounter_Pitch > (cend - ix))
						{
							for (int i = ix; i < cend; i++) {
								cbuffer[i] = modEnvCurrentValue_Pitch;
							}										
							modDelayCounter_Pitch -= cend - ix;
						}
						else
						{
							for (int i = ix; i < ix+modDelayCounter_Pitch; i++) {
								cbuffer[i] = modEnvCurrentValue_Pitch;
							}					
							ix += modDelayCounter_Pitch;
							modDelayCounter_Pitch = 0;					
						}				
					}
					
					
					if(modEnvMode_Pitch == 0) // ATTACK
					{
						double modEnvToPitch = Math.pow(2,this.modEnvToPitch);
						double modEnvAttackStep_Pitch = this.modEnvAttackStep_Pitch;
						
						
						//if(modEnvToPitch < 1)  // TODO LAGA ATTACK
						while(ix < cend)
						{
							//modEnvCurrentValue_Pitch *= modEnvAttackStep_Pitch;
							modEnvCurrentValue_Pitch = (modEnvCurrentValue_Pitch - modEnvAttackOffset_Pitch)* modEnvAttackStep_Pitch + modEnvAttackOffset_Pitch;
							
							if(modEnvAttackCounter == 0) // modEnvCurrentValue_Pitch <= modEnvToPitch
							{
								modEnvCurrentValue_Pitch = modEnvToPitch;
								modEnvMode_Pitch = 1;
								break;
							}
							modEnvAttackCounter--;
							cbuffer[ix] = modEnvCurrentValue_Pitch;
							ix++;
						}/*						
						else
						while(ix < cend)
						{
						modEnvCurrentValue_Pitch = (modEnvCurrentValue_Pitch - modEnvAttackOffset_Pitch)* modEnvAttackStep_Pitch + modEnvAttackOffset_Pitch;
						
						if(modEnvCurrentValue_Pitch >= modEnvToPitch)
						{
						modEnvCurrentValue_Pitch = modEnvToPitch;
						modEnvMode_Pitch = 1;
						break;
						}
						cbuffer[ix] = modEnvCurrentValue_Pitch;
						ix++;
						}
						*/			
					}
					
					
					if(modEnvMode_Pitch == 1) // HOLD
					{
						if(modHoldCounter_Pitch == 0)
							modEnvMode_Pitch = 2;
						if(modHoldCounter_Pitch > (cend - ix))
						{
							for (int i = ix; i < cend; i++) {
								cbuffer[i] = modEnvCurrentValue_Pitch;
							}										
							modHoldCounter_Pitch -= cend - ix;
						}
						else
						{
							modEnvMode_Pitch = 2;
							for (int i = ix; i < ix+modHoldCounter_Pitch; i++) {
								cbuffer[i] = modEnvCurrentValue_Pitch;
							}					
							ix += modHoldCounter_Pitch;
							
						}
					}
					
					
					if(modEnvMode_Pitch == 2) // DECAY
					{
						if(modEnvDecayStep_Pitch == 0) 
							modEnvMode_Pitch = 3;
						else
						{
							
							double sustain = Math.pow(2, this.modEnvSustain * modEnvToPitch);
							double envStep = this.modEnvDecayStep_Pitch;
							if(envStep >= 1)				
								while(ix < cend)
								{
									modEnvCurrentValue_Pitch *= envStep;
									if(modEnvCurrentValue_Pitch >= sustain)
									{
										modEnvCurrentValue_Pitch = sustain;
										modEnvMode_Pitch = 3;
										//if(modEnvCurrentValue_Pitch <= 1) modEnvEof_Pitch = true;
										break;
									}
									cbuffer[ix] = modEnvCurrentValue_Pitch;
									
									ix++;
								}									
							else
								while(ix < cend)
								{
									modEnvCurrentValue_Pitch *= envStep;
									if(modEnvCurrentValue_Pitch <= sustain)
									{
										modEnvCurrentValue_Pitch = sustain;
										modEnvMode_Pitch = 3;
										//if(modEnvCurrentValue_Pitch <= 1) modEnvEof_Pitch = true;
										break;
									}
									cbuffer[ix] = modEnvCurrentValue_Pitch;
									
									ix++;
								}
						}
					}
					
					
					if(modEnvMode_Pitch == 3) // SUSTAIN
					{			
						if(ix == 0)
						{
							constantPitch = true;
						}
						else
							for (int i = ix; i < cend; i++) {
								cbuffer[i] = modEnvCurrentValue_Pitch;
								
							}
						ix = cend;
					}
				}
				else
				{
					//	RELEASE
					if(!modEnvReleaseCalculated_Pitch)
					{
						modEnvReleaseCalculated_Pitch = true;
						if(modEnvReleaseTime != 0)
							this.modEnvReleaseStep_Pitch = Math.pow(1.0/modEnvCurrentValue_Pitch, 1.0 / modEnvReleaseTime);
						else
							this.modEnvReleaseStep_Pitch = 1;
					}
					double envStep = this.modEnvReleaseStep_Pitch;
					if(envStep >= 1)
						while(ix < cend)
						{
							modEnvCurrentValue_Pitch *= envStep;
							
							if(modEnvCurrentValue_Pitch >= 1)
							{
								//cend = ix;
								modEnvEof_Pitch = true;
								break;
							}
							cbuffer[ix] = modEnvCurrentValue_Pitch;
							
							ix++;
						}					
					else
						while(ix < cend)
						{
							modEnvCurrentValue_Pitch *= envStep;
							if(modEnvCurrentValue_Pitch <= 1)
							{
								//cend = ix;
								modEnvEof_Pitch = true;
								break;
							}
							cbuffer[ix] = modEnvCurrentValue_Pitch;
							ix++;
						}
					
				}
				
				
				Arrays.fill(cbuffer, ix, cend, modEnvCurrentValue_Pitch);
				
				this.modEnvCurrentValue_Pitch = modEnvCurrentValue_Pitch;
				
			}
			
			//*********************************
			// Apply VIB LFO to Pitch
			//*********************************
			if(vibActive)
				if(vibStart != -1)
					if(lfoTable_vibToPitch != null)
					{
						double ix = vibCounter;
						double ix_step = freqVibLFO_step;
						double ix_len = AudioVoiceFactoryInstance.lfoTable_size;
						double[] ix_table = lfoTable_vibToPitch;
						
						if(dynamic_pitch)
						{					
							if(!constantPitch)
							{
								for (int i = vibStart; i < cend; i++) {					
									cbuffer[i] *= ix_table[(int)ix];
									ix += ix_step;
									if(ix > ix_len) ix = ix % ix_len; 
								}															
							}
							else
							{
								constantPitch = false;
								Arrays.fill(cbuffer, cstart, vibStart, modEnvCurrentValue_Pitch);
								for (int i = vibStart; i < cend; i++) {					
									cbuffer[i] = modEnvCurrentValue_Pitch*ix_table[(int)ix];
									ix += ix_step;
									if(ix > ix_len) ix = ix % ix_len; 
								}						
							}
						}
						else
						{				
							for (int i = cstart; i < vibStart; i++) {
								cbuffer[i] = 1;
							}					
							for (int i = vibStart; i < cend; i++) {					
								cbuffer[i] = ix_table[(int)ix];
								ix += ix_step;
								if(ix > ix_len) 
									ix = ix % ix_len; 
							}				
							dynamic_pitch = true;
						}								
					}	
			
			
			//*********************************
			// Apply MOD LFO to Pitch
			//*********************************
			if(modActive)
				if(modStart != -1)
					if(lfoTable_modToPitch != null)			
					{
						double ix = modCounter;
						double ix_step = freqModLFO_step;
						double ix_len = AudioVoiceFactoryInstance.lfoTable_size;
						double[] ix_table = lfoTable_modToPitch;
						
						if(dynamic_pitch)
						{					
							if(!constantPitch)
							{
								for (int i = modStart; i < cend; i++) {					
									cbuffer[i] *= ix_table[(int)ix];
									ix += ix_step;
									if(ix > ix_len) ix = ix % ix_len; 
								}															
							}
							else
							{
								constantPitch = false;
								Arrays.fill(cbuffer, cstart, modStart, modEnvCurrentValue_Pitch);
								for (int i = modStart; i < cend; i++) {					
									cbuffer[i] = modEnvCurrentValue_Pitch*ix_table[(int)ix];
									ix += ix_step;
									if(ix > ix_len) ix = ix % ix_len; 
								}						
							}
						}
						else
						{				
							
							for (int i = cstart; i < modStart; i++) {
								cbuffer[i] = 1;
							}
							for (int i = modStart; i < cend; i++) {
								cbuffer[i] = ix_table[(int)ix];
								ix += ix_step;
								if(ix > ix_len) ix = ix % ix_len; 
							}			
							dynamic_pitch = true;
						}
					}						
			
			//**********************************
			// Read Pitch value from PitchStream
			//**********************************
			double pitch = 1.0;
			if(pitchstream != null)
			{
				double[] sbuffer = new double[1];;
				int ret = pitchstream.isStatic(sbuffer, cend - cstart);
				
				if(ret != -1)
				{
					// Static pitch
					pitch = sbuffer[0];
					
				}
				else
				{
					// Non-Static pitch
					double[] pbuffer = audiocache.getBuffer(cend);
					ret = pitchstream.replace(pbuffer, cstart, cend);
					if(ret == -1)
					{
						ret = 0;
						pitchstream.close();
						pitchstream = null;
					}
					Arrays.fill(pbuffer, cstart + ret, cend, 1.0);
					
					
					if(dynamic_pitch)
					{					
						if(!constantPitch)
						{
							for (int i = cstart; i < cend; i++) {					
								cbuffer[i] *= pbuffer[i]; 
							}															
						}
						else
						{
							constantPitch = false;
							for (int i = cstart; i < cend; i++) {					
								cbuffer[i] = pbuffer[i]; 
							}						
						}
					}
					else
					{				
						for (int i = cstart; i < cend; i++) {
							cbuffer[i] = pbuffer[i]; 
						}			
						dynamic_pitch = true;
					}
					
					
					audiocache.returnBuffer(pbuffer);
				}
			}
			
			//*********************************
			// Resample stream
			//*********************************			
			{
				double streamfactor = this.streamfactor * pitch;
				double[] rbuffer = this.rbuffer;
				int ix = cstart;
				double rbuffer_pos = this.rbuffer_pos;
				
				if(dynamic_pitch)
				{
					if(constantPitch)
					{
						streamfactor *= modEnvCurrentValue_Pitch;
						while(ix < cend)
						{
							rbuffer_pos += streamfactor;								
							while(rbuffer_pos >= (2048-2)) 
							{
								rbuffer_pos -= 2048 - 2;
								readNextInputBuffer();
							}
							
							double interp = rbuffer_pos % 1.0;
							int i_rbuffer_pos = (int)rbuffer_pos;
							cbuffer[ix] = (rbuffer[i_rbuffer_pos] * (1 - interp)) + (rbuffer[i_rbuffer_pos + 1]*interp);
							ix++;				
						}					
					}
					else
						while(ix < cend)
						{
							rbuffer_pos += streamfactor*cbuffer[ix];								
							while(rbuffer_pos >= (2048-2)) 
							{
								rbuffer_pos -= 2048 - 2;
								readNextInputBuffer();
							}
							
							double interp = rbuffer_pos % 1.0;
							int i_rbuffer_pos = (int)rbuffer_pos;
							cbuffer[ix] = (rbuffer[i_rbuffer_pos] * (1 - interp)) + (rbuffer[i_rbuffer_pos + 1]*interp);
							ix++;				
						}
				}
				else
				{
					while(ix < cend)
					{
						rbuffer_pos += streamfactor;								
						while(rbuffer_pos >= (2048-2)) 
						{
							rbuffer_pos -= 2048 - 2;
							readNextInputBuffer();
						}
						
						double interp = rbuffer_pos % 1.0;
						int i_rbuffer_pos = (int)rbuffer_pos;
						cbuffer[ix] = (rbuffer[i_rbuffer_pos] * (1 - interp)) + (rbuffer[i_rbuffer_pos + 1]*interp);
						ix++;				
					}
				}
				
				
				this.rbuffer_pos = rbuffer_pos;
			}
			
			/*
			 double streamfactor = streamfactor;
			 int rlen = (cstart - cend) * 
			 double[] rbuffer = audiocache.getBuffer()
			 double[] cbuffer = audiocache.getBuffer(cend);			
			 int ret = resampledstream.replace(cbuffer, cstart, cend);
			 if(ret == -1) return -1;
			 cend = cstart + ret; */
			
			//*********************************
			// Apply volume envelope
			//*********************************
			{
				double volEnvCurrentValue = this.volEnvCurrentValue;
				int ix = cstart;
				if(noteActive)
				{
					if(volDelayCounter != 0) // DELAY
					{
						if(volDelayCounter > (cend - ix))
						{
							for (int i = ix; i < cend; i++) {
								cbuffer[i] *= volEnvCurrentValue;
							}										
							volDelayCounter -= cend - ix;
						}
						else
						{
							for (int i = ix; i < ix+volDelayCounter; i++) {
								cbuffer[i] *= volEnvCurrentValue;
							}					
							ix += volDelayCounter;
							volDelayCounter = 0;					
						}				
					}
					if(volEnvMode == 0) // ATTACK
					{
						double velocity = this.velocity;
						double volEnvAttackStep = this.volEnvAttackStep;
						while(ix < cend)
						{
							volEnvCurrentValue += volEnvAttackStep;
							if(volEnvCurrentValue > velocity)
							{
								volEnvCurrentValue = velocity;
								volEnvMode = 1;
								break;
							}
							cbuffer[ix] *= volEnvCurrentValue;
							ix++;
						}			
					}
					if(volEnvMode == 1) // HOLD
					{						
						if(volHoldCounter == 0)
							volEnvMode = 2;
						if(volHoldCounter > (cend - ix))
						{
							for (int i = ix; i < cend; i++) {
								cbuffer[i] *= volEnvCurrentValue;
							}										
							volHoldCounter -= cend - ix;
						}
						else
						{
							volEnvMode = 2;
							for (int i = ix; i < ix+volHoldCounter; i++) {
								cbuffer[i] *= volEnvCurrentValue;
							}					
							ix += volHoldCounter;
							
						}
					}
					if(volEnvMode == 2) // DECAY
					{
						if(volEnvDecayStep == 0) 
							volEnvMode = 3;
						else
						{
							double sustain = this.volEnvSustain;
							double envStep = this.volEnvDecayStep;
							while(ix < cend)
							{
								volEnvCurrentValue *= envStep;
								if(volEnvCurrentValue <= sustain)
								{
									volEnvCurrentValue = sustain;
									volEnvMode = 3;
									if(volEnvCurrentValue <= MINUS60DB) eof = true;
									break;
								}
								cbuffer[ix] *= volEnvCurrentValue;
								ix++;
							}
						}
					}
					if(volEnvMode == 3) // SUSTAIN
					{			
						for (int i = ix; i < cend; i++) {
							cbuffer[i] *= volEnvCurrentValue;
						}
					}
				}
				else
				{
					//	RELEASE
					double envStep = this.volEnvReleaseStep;
					while(ix < cend)
					{
						volEnvCurrentValue *= envStep;
						if(volEnvCurrentValue <= MINUS60DB)
						{
							cend = ix;
							eof = true;
							break;
						}
						cbuffer[ix] *= volEnvCurrentValue;
						ix++;
					}
					
				}
				
				end = cend*channels;
				this.volEnvCurrentValue = volEnvCurrentValue;
				
				
			}
			
			if(modStart > cend) modStart = cend;
			if(vibStart > cend) vibStart = cend;
			
			//*********************************
			// Apply modulation LFO to Volume
			//*********************************
			if(modActive)
				if(modStart != -1)				
					if(lfoTable_modToVolume != null)
					{
						double ix = modCounter;
						double ix_step = freqModLFO_step;
						double ix_len = AudioVoiceFactoryInstance.lfoTable_size;
						double[] ix_table = lfoTable_modToVolume;
						for (int i = modStart; i < cend; i++) {					
							cbuffer[i] *= ix_table[(int)ix];
							ix += ix_step;
							if(ix > ix_len) ix = ix % ix_len; 
						}				
					}
			
			//*********************************
			// Apply lowpass modulation envelope
			//*********************************		
			double[] fbuffer = null;
			
			if(b_lowpass)
			{
				
				if(modEnvActive && (modEnvToFilterFc != 0) && !modEnvEof_Freq)
				{
					fbuffer = audiocache.getBuffer(cbuffer.length);
					double modEnvCurrentValue_Freq = this.modEnvCurrentValue_Freq;
					int ix = cstart;
					if(noteActive)
					{
						if(modDelayCounter_Freq != 0) // DELAY
						{				
							if(modDelayCounter_Freq > (cend - ix))
							{
								for (int i = ix; i < cend; i++) {
									fbuffer[i] = modEnvCurrentValue_Freq;
								}										
								modDelayCounter_Freq -= cend - ix;
							}
							else
							{
								for (int i = ix; i < ix+modDelayCounter_Freq; i++) {
									fbuffer[i] = modEnvCurrentValue_Freq;
								}					
								ix += modDelayCounter_Freq;
								modDelayCounter_Freq = 0;					
							}				
						}
						if(modEnvMode_Freq == 0) // ATTACK
						{
							double modEnvToPitch = Math.pow(2,this.modEnvToFilterFc);
							double modEnvAttackStep_Freq = this.modEnvAttackStep_Freq;
							//if(modEnvToPitch < 1)
							while(ix < cend)
							{
								modEnvCurrentValue_Freq = (modEnvCurrentValue_Freq - modEnvAttackOffset_Freq)* modEnvAttackStep_Freq + modEnvAttackOffset_Freq;
								
								if(modEnvAttackCounter == 0) //modEnvCurrentValue_Freq < modEnvToPitch)
								{
									modEnvCurrentValue_Freq = modEnvToPitch;
									modEnvMode_Freq = 1;
									break;
								}
								modEnvAttackCounter--;
								fbuffer[ix] = modEnvCurrentValue_Freq;
								ix++;
							}			/*					
							else
							while(ix < cend)
							{
							modEnvCurrentValue_Freq = (modEnvCurrentValue_Freq - modEnvAttackOffset_Freq)* modEnvAttackStep_Freq + modEnvAttackOffset_Freq;
							if(modEnvCurrentValue_Freq > modEnvToPitch)
							{
							modEnvCurrentValue_Freq = modEnvToPitch;
							modEnvMode_Freq = 1;
							break;
							}
							fbuffer[ix] = modEnvCurrentValue_Freq ;
							ix++;
							}*/			
						}
						if(modEnvMode_Freq == 1) // HOLD
						{
							if(modHoldCounter_Freq == 0)
								modEnvMode_Freq = 2;
							if(modHoldCounter_Freq > (cend - ix))
							{
								for (int i = ix; i < cend; i++) {
									fbuffer[i] = modEnvCurrentValue_Freq ;
								}										
								modHoldCounter_Freq -= cend - ix;
							}
							else
							{
								modEnvMode_Freq = 2;
								for (int i = ix; i < ix+modHoldCounter_Freq; i++) {
									fbuffer[i] = modEnvCurrentValue_Freq ;
								}					
								ix += modHoldCounter_Freq;
								
							}
						}
						if(modEnvMode_Freq == 2) // DECAY
						{
							if(modEnvDecayStep_Freq == 0) 
								modEnvMode_Freq = 3;
							else
							{
								double sustain = Math.pow(2, this.modEnvSustain * modEnvToFilterFc);
								double envStep = this.modEnvDecayStep_Freq;
								if(envStep >= 1)
									while(ix < cend)
									{
										modEnvCurrentValue_Freq *= envStep;
										if(modEnvCurrentValue_Freq >= sustain)
										{
											modEnvCurrentValue_Freq = sustain;
											modEnvMode_Freq = 3;
											//if(modEnvCurrentValue_Freq <= 1) modEnvEof_Freq = true;
											break;
										}
										fbuffer[ix] = modEnvCurrentValue_Freq;
										ix++;
									}					
								else
									while(ix < cend)
									{
										modEnvCurrentValue_Freq *= envStep;
										if(modEnvCurrentValue_Freq <= sustain)
										{
											modEnvCurrentValue_Freq = sustain;
											modEnvMode_Freq = 3;
											//if(modEnvCurrentValue_Freq <= 1) modEnvEof_Freq = true;
											break;
										}
										fbuffer[ix] = modEnvCurrentValue_Freq;
										ix++;
									}
							}
						}
						if(modEnvMode_Freq == 3) // SUSTAIN
						{
							if(ix == cstart)
							{
								if(fbuffer != null) audiocache.returnBuffer(fbuffer);
								
								fbuffer = null;
							}
							else				
								for (int i = ix; i < cend; i++) {
									fbuffer[i] = modEnvCurrentValue_Freq;
								}
							ix = cend;
						}
					}
					else
					{
						if(!modEnvReleaseCalculated_Freq)
						{
							modEnvReleaseCalculated_Freq = true;
							if(modEnvReleaseTime != 0)
								this.modEnvReleaseStep_Freq = Math.pow(1.0/modEnvCurrentValue_Freq, 1.0 / modEnvReleaseTime);
							else
								this.modEnvReleaseStep_Freq = 1;
						}				
						//	RELEASE
						double envStep = this.modEnvReleaseStep_Freq;
						if(envStep == 1)
						{
							if(ix == cstart)
							{
								modEnvEof_Freq = true;
								if(fbuffer != null) audiocache.returnBuffer(fbuffer);
								fbuffer = null;
							}
						}
						else
							if(envStep >= 1)
								while(ix > cend)
								{
									modEnvCurrentValue_Freq *= envStep;
									if(modEnvCurrentValue_Freq >= 1)
									{
										//cend = ix;
										modEnvEof_Freq = true;
										break;
									}
									fbuffer[ix] = modEnvCurrentValue_Freq;
									ix++;
								}					
							else
								while(ix < cend)
								{
									modEnvCurrentValue_Freq *= envStep;
									//if(modEnvCurrentValue_Freq <= 1)
									if((envStep <= 1 && modEnvCurrentValue_Freq <= 1) ||
											(envStep > 1 && modEnvCurrentValue_Freq >= 1))
									{
										//cend = ix;
										modEnvEof_Freq = true;
										break;
									}
									fbuffer[ix] = modEnvCurrentValue_Freq;
									ix++;
								}
						
					}
										
					if(fbuffer != null)
						Arrays.fill(fbuffer, ix, cend, modEnvCurrentValue_Freq);
					this.modEnvCurrentValue_Freq = modEnvCurrentValue_Freq;
					
				}
				
				//*********************************
				// Apply modulation LFO to Filter
				//*********************************
				if(modActive)
					if(modStart != -1)				
						if(lfoTable_modToFilterFc != null)
						{
							double ix = modCounter;
							double ix_step = freqModLFO_step;
							double ix_len = AudioVoiceFactoryInstance.lfoTable_size;
							double[] ix_table = lfoTable_modToFilterFc;
							
							if(fbuffer == null)
							{				
								fbuffer = audiocache.getBuffer(cbuffer.length);
								for (int i = cstart; i < modStart; i++) {
									fbuffer[i] = modEnvCurrentValue_Freq;
								}								
								for (int i = modStart; i < cend; i++) {					
									fbuffer[i] = modEnvCurrentValue_Freq * ix_table[(int)ix];
									ix += ix_step;
									if(ix > ix_len) ix = ix % ix_len; 
								}			
								
							}
							else
							{
								for (int i = modStart; i < cend; i++) {					
									fbuffer[i] *= ix_table[(int)ix];
									ix += ix_step;
									if(ix > ix_len) ix = ix % ix_len; 
								}				
							}
						}			
				
				//*********************************
				// Low Pass Filter
				//*********************************
				
				if(fbuffer != null)
					calcLP(cbuffer, fbuffer, cstart, cend);
				else
				{
					if(!(modEnvCurrentValue_Freq == 1 && initialFilterFc > 19000 && initialFilterQ < 0.01))
						calcLP(cbuffer, cstart, cend);
				}
				
			}
			
			//*********************************
			// Apply add buffer to sterio output stream
			//*********************************	    
			for (int c = 0; c < channels; c++) {
				int ix = cstart;
				double ch_vol = 0;		    
				if(c < channelsVols.length) ch_vol = channelsVols[c];
				if(ch_vol > MINUS70DB)
					for (int i = start+c; i < end; i+=channels) {
						buffer[i] += cbuffer[ix]*ch_vol;
						ix++;
					}
			}
			audiocache.returnBuffer(cbuffer);
			if(fbuffer != null) audiocache.returnBuffer(fbuffer);
			
			if(vibActive)
			{				
				if(vibStart != -1)
				{
					vibCounter += freqVibLFO_step * (cend-vibStart);
					vibCounter = vibCounter % AudioVoiceFactoryInstance.lfoTable_size; 
				}
			}		
			
			if(modActive)
			{
				if(modStart != -1)
				{
					modCounter += freqModLFO_step * (cend-modStart);
					modCounter = modCounter % AudioVoiceFactoryInstance.lfoTable_size;
				}			
			}
			
			return (end-start)*channels;
			
		}
		
		public int replace(double[] buffer, int start, int end) {
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);
		}
		public int skip(int len)
		{
			return -1;
		}		
		public int isStatic(double[] buffer, int len) {
			return -1;
		}		
		
		public void close() {	
			samplestream.close();
			activestream.close();
			if(pitchstream != null)
			{
				pitchstream.close();
				pitchstream = null;
			}
		}
		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new AudioVoiceStream(session);
	}
	
	Variable tuning_out = null;
	UnitInstance tuning_instance = null;
	Variable pitch;
	NameSpace namespace;
	
		
	public AudioVoiceInstance(AudioVoiceFactoryInstance factory, Parameters parameters)
	{
		this.factory = factory;
		namespace = parameters.getNameSpace();
		
		pitch = parameters.getParameter("pitch");
		
		output = parameters.getParameterWithDefault("output");
		note = parameters.getParameterWithDefault(1, "note");
		velocity = parameters.getParameterWithDefault(2, "velocity");
		active = parameters.getParameterWithDefault(3, "active");		
		tuning = parameters.getParameter(4, "tuning");						
		
		if(tuning != null)
		{
			tuning_out = new Variable();
			Parameters p = new Parameters(namespace);
			p.setParameter("output", tuning_out);
			p.setParameter(1, note);
			tuning_instance = Unit.newInstance(tuning, p);				
		}
		
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void calc() {
	}
	
	public void close() {
		output.remove(answer);
		if(tuning_instance != null)
			tuning_instance.close();
	}
	
}

class AudioVoice implements UnitFactory {
	
	AudioVoiceFactoryInstance factory;
	public AudioVoice(AudioVoiceFactoryInstance factor)
	{
		this.factory = factor;
	}
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioVoiceInstance(factory, parameters);
	}
}

class AudioVoiceFactoryInstance extends UnitInstanceAdapter implements Commitable 
{	
	Variable velocity;
	Variable keynum;
	Variable[] channelsVols;
	Variable initialAttenuation;
	
	Variable delayVolEnv;
	Variable attackVolEnv;
	Variable holdVolEnv;
	Variable decayVolEnv;
	Variable sustainVolEnv;
	Variable releaseVolEnv;
	
	Variable delayModEnv;
	Variable attackModEnv;
	Variable holdModEnv;
	Variable decayModEnv;
	Variable sustainModEnv;
	Variable releaseModEnv;	
	
	Variable modEnvToPitch;
	Variable modEnvToFilterFc;
	
	Variable sample;
	Variable sampleRate;		
	
	Variable scaleTune;
	Variable coarseTune;
	
	Variable initialFilterFc;
	Variable initialFilterQ;
	Variable delayModLFO;
	Variable freqModLFO;
	Variable modLfoToPitch;
	Variable modLfoToFilterFc;
	Variable modLfoToVolume;
	Variable delayVibLFO;
	Variable freqVibLFO;
	Variable vibLfoToPitch;	
	Variable keynumToModEnvHold;	
	Variable keynumToModEnvDecay;	
	Variable keynumToVolEnvHold;	
	Variable keynumToVolEnvDecay;
	Variable lowpass;
	
	
	int i_velocity = -1;
	int i_keynum = -1;
	
	boolean b_lowpass = true;               // LowPass Filter Activated
	
	double[] d_channelsVols = {0.5, 0.5};
	double d_initialAttenuation = 1.0;
	
	double d_delayVolEnv = 0;              // seconds
	double d_attackVolEnv = 0;             // seconds
	double d_holdVolEnv = 0;               // seconds
	double d_decayVolEnv = 0;              // seconds
	double d_sustainVolEnv = 1.0;          // amp(*)
	double d_releaseVolEnv = 0.001;        // seconds
	
	double d_delayModEnv = 0;              // seconds
	double d_attackModEnv = 0;             // seconds
	double d_holdModEnv = 0;               // seconds
	double d_decayModEnv = 0;              // seconds
	double d_sustainModEnv = 1.0;          // amp(*)
	double d_releaseModEnv = 0.001;        // seconds
	
	double d_modEnvToPitch = 0;            // octaves
	double d_modEnvToFilterFc = 0;         // octaves
	
	double d_sample;                       // AudioEvents
	double d_sampleRate = 44100;	       // hz
	
	double d_scaleTune = 2;                // frequency ratio
	double d_coarseTune = -60;             // semitones
	
	double d_initialFilterFc = 20000;      // hz
	double d_initialFilterQ = 0;           // db
	
	double d_keynumToModEnvHold = 0;
	double d_keynumToModEnvDecay = 0;   
	double d_keynumToVolEnvHold = 0;
	double d_keynumToVolEnvDecay = 0;	
	
	// SF2 GENERATORS TO BE IMPLEMENTED (LFO)
	// LFO MOD (needs a fast LFO code to work)
	double d_delayModLFO = 0;              // seconds
	double d_freqModLFO = 8.176;           // hz
	double d_modLfoToPitch = 0;            // octaves
	double d_modLfoToFilterFc = 0;         // octaves
	double d_modLfoToVolume = 0;           // amp(*)
	// LFO VIB (needs a fast LFO code to work)
	double d_delayVibLFO = 0;              // seconds
	double d_freqVibLFO = 8.176;           // hz
	double d_vibLfoToPitch = 0;            // octaves
	
	public static final int lfoTable_size         = 512; //4096;
	public static final int lfoTable_size_overlap = 520; //5000;
	double[] lfoTable_vibToPitch = null;     	
	double[] lfoTable_modToPitch = null;
	double[] lfoTable_modToFilterFc = null;
	double[] lfoTable_modToVolume = null;
	
	Variable output;
	Variable answer;
	private NameSpace namespace;
	//Map parameters;
	public AudioVoiceFactoryInstance(Parameters parameters)
	{
		namespace = parameters.getNameSpace();
		output = parameters.getParameterWithDefault(0, "output");
		
		
		answer = Unit.asVariable(new AudioVoice(this));		
		output.add(answer);
		
		velocity = parameters.getParameter("velocity");
		keynum = parameters.getParameter("keynum");
		//pan = parameters.getParameterWithNull("pan");
		
		ArrayList<Variable> chlist = new ArrayList<Variable>();
		int c = 1;
		while(true)
		{
			Variable value = parameters.getParameter("ch" + c +"Vol");
			if(value == null) break;
			chlist.add(value);
			c++;
		}
		channelsVols = chlist.toArray(new Variable[c]);
		
		initialAttenuation = parameters.getParameter("initialAttenuation");
		
		delayVolEnv = parameters.getParameter("delayVolEnv");
		attackVolEnv = parameters.getParameter("attackVolEnv");
		holdVolEnv = parameters.getParameter("holdVolEnv");
		decayVolEnv = parameters.getParameter("decayVolEnv");
		sustainVolEnv = parameters.getParameter("sustainVolEnv");
		releaseVolEnv = parameters.getParameter("releaseVolEnv");
		
		delayModEnv = parameters.getParameter("delayModEnv");
		attackModEnv = parameters.getParameter("attackModEnv");
		holdModEnv = parameters.getParameter("holdModEnv");
		decayModEnv = parameters.getParameter("decayModEnv");
		sustainModEnv = parameters.getParameter("sustainModEnv");
		releaseModEnv = parameters.getParameter("releaseModEnv");		
		modEnvToPitch = parameters.getParameter("modEnvToPitch");
		modEnvToFilterFc = parameters.getParameter("modEnvToFilterFc");		
		
		
		sample = parameters.getParameter("sample");
		sampleRate = parameters.getParameter("sampleRate");		
		scaleTune = parameters.getParameter("scaleTune");
		coarseTune = parameters.getParameter("coarseTune");
		
		initialFilterFc = parameters.getParameter("initialFilterFc");
		initialFilterQ = parameters.getParameter("initialFilterQ");
		delayModLFO = parameters.getParameter("delayModLFO");
		freqModLFO = parameters.getParameter("freqModLFO");
		modLfoToPitch = parameters.getParameter("modLfoToPitch");
		modLfoToFilterFc = parameters.getParameter("modLfoToFilterFc");
		modLfoToVolume = parameters.getParameter("modLfoToVolume");
		delayVibLFO = parameters.getParameter("delayVibLFO");
		freqVibLFO = parameters.getParameter("freqVibLFO");
		vibLfoToPitch = parameters.getParameter("vibLfoToPitch");	
		keynumToModEnvHold = parameters.getParameter("keynumToModEnvHold");	
		keynumToModEnvDecay = parameters.getParameter("keynumToModEnvDecay");	
		keynumToVolEnvHold = parameters.getParameter("keynumToVolEnvHold");	
		keynumToVolEnvDecay = parameters.getParameter("keynumToVolEnvDecay");
		lowpass = parameters.getParameter("lowpass");
		
		
		if(lowpass != null) DoublePart.getInstance(lowpass).addListener(this);
		if(velocity != null) DoublePart.getInstance(velocity).addListener(this);
		if(keynum != null) DoublePart.getInstance(keynum).addListener(this);
		for (int j = 0; j < channelsVols.length; j++) {
			if(channelsVols[j] != null) DoublePart.getInstance(channelsVols[j]).addListener(this);			
		}
		if(initialAttenuation != null) DoublePart.getInstance(initialAttenuation).addListener(this);
		
		if(delayVolEnv != null) DoublePart.getInstance(delayVolEnv).addListener(this);
		if(attackVolEnv != null) DoublePart.getInstance(attackVolEnv).addListener(this);
		if(holdVolEnv != null) DoublePart.getInstance(holdVolEnv).addListener(this);
		if(decayVolEnv != null) DoublePart.getInstance(decayVolEnv).addListener(this);
		if(sustainVolEnv != null) DoublePart.getInstance(sustainVolEnv).addListener(this);
		if(releaseVolEnv != null) DoublePart.getInstance(releaseVolEnv).addListener(this);
		if(delayModEnv != null) DoublePart.getInstance(delayModEnv).addListener(this);
		if(attackModEnv != null) DoublePart.getInstance(attackModEnv).addListener(this);
		if(holdModEnv != null) DoublePart.getInstance(holdModEnv).addListener(this);
		if(decayModEnv != null) DoublePart.getInstance(decayModEnv).addListener(this);
		if(sustainModEnv != null) DoublePart.getInstance(sustainModEnv).addListener(this);
		if(releaseModEnv != null) DoublePart.getInstance(releaseModEnv).addListener(this);
		if(modEnvToPitch != null) DoublePart.getInstance(modEnvToPitch).addListener(this);
		if(modEnvToFilterFc != null) DoublePart.getInstance(modEnvToFilterFc).addListener(this);
		
		
		if(sampleRate != null) DoublePart.getInstance(sampleRate).addListener(this);
		
		if(scaleTune != null) DoublePart.getInstance(scaleTune).addListener(this);
		if(coarseTune != null) DoublePart.getInstance(coarseTune).addListener(this);
		
		if(initialFilterFc != null) DoublePart.getInstance(initialFilterFc).addListener(this);
		if(initialFilterQ != null) DoublePart.getInstance(initialFilterQ).addListener(this);
		if(delayModLFO != null) DoublePart.getInstance(delayModLFO).addListener(this);
		if(freqModLFO != null) DoublePart.getInstance(freqModLFO).addListener(this);
		if(modLfoToPitch != null) DoublePart.getInstance(modLfoToPitch).addListener(this);
		if(modLfoToFilterFc != null) DoublePart.getInstance(modLfoToFilterFc).addListener(this);
		if(modLfoToVolume != null) DoublePart.getInstance(modLfoToVolume).addListener(this);
		if(delayVibLFO != null) DoublePart.getInstance(delayVibLFO).addListener(this);
		if(freqVibLFO != null) DoublePart.getInstance(freqVibLFO).addListener(this);
		if(vibLfoToPitch != null) DoublePart.getInstance(vibLfoToPitch).addListener(this);	
		if(keynumToModEnvHold != null) DoublePart.getInstance(keynumToModEnvHold).addListener(this);	
		if(keynumToModEnvDecay != null) DoublePart.getInstance(keynumToModEnvDecay).addListener(this);	
		if(keynumToVolEnvHold != null) DoublePart.getInstance(keynumToVolEnvHold).addListener(this);	
		if(keynumToVolEnvDecay != null) DoublePart.getInstance(keynumToVolEnvDecay).addListener(this);
		
		
		
		//this.parameters = parameters.getMap();
		
		calc();
		
		
	}
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit()
	{
		
		if(lowpass != null)
		{
			b_lowpass = DoublePart.asDouble(lowpass) > 0.5; 			
		}
		if(velocity != null) i_velocity =(int)DoublePart.asDouble(velocity);
		if(keynum != null) i_keynum = (int)DoublePart.asDouble(keynum);
		if(channelsVols.length != 0) d_channelsVols = new double[channelsVols.length];
		for (int j = 0; j < channelsVols.length; j++) {
			if(channelsVols[j] != null) d_channelsVols[j] = DoublePart.asDouble(channelsVols[j]);			
		}		
		if(initialAttenuation != null) d_initialAttenuation = DoublePart.asDouble(initialAttenuation);
		
		if(delayVolEnv != null) d_delayVolEnv = DoublePart.asDouble(delayVolEnv);
		if(attackVolEnv != null) d_attackVolEnv = DoublePart.asDouble(attackVolEnv);
		if(holdVolEnv != null) d_holdVolEnv = DoublePart.asDouble(holdVolEnv);
		if(decayVolEnv != null) d_decayVolEnv = DoublePart.asDouble(decayVolEnv);
		if(sustainVolEnv != null) d_sustainVolEnv = DoublePart.asDouble(sustainVolEnv);
		if(releaseVolEnv != null) d_releaseVolEnv = DoublePart.asDouble(releaseVolEnv);
		
		if(delayModEnv != null) d_delayModEnv = DoublePart.asDouble(delayModEnv);
		if(attackModEnv != null) d_attackModEnv = DoublePart.asDouble(attackModEnv);
		if(holdModEnv != null) d_holdModEnv = DoublePart.asDouble(holdModEnv);
		if(decayModEnv != null) d_decayModEnv = DoublePart.asDouble(decayModEnv);
		if(sustainModEnv != null) d_sustainModEnv = DoublePart.asDouble(sustainModEnv);
		if(releaseModEnv != null) d_releaseModEnv = DoublePart.asDouble(releaseModEnv);
		
		if(modEnvToPitch != null) d_modEnvToPitch = DoublePart.asDouble(modEnvToPitch);
		if(modEnvToFilterFc != null) d_modEnvToFilterFc = DoublePart.asDouble(modEnvToFilterFc);
		
		if(sampleRate != null) d_sampleRate = DoublePart.asDouble(sampleRate);
		
		if(scaleTune != null) d_scaleTune = DoublePart.asDouble(scaleTune);
		if(coarseTune != null) d_coarseTune = DoublePart.asDouble(coarseTune);	    
		
		if(initialFilterFc != null) d_initialFilterFc = DoublePart.asDouble(initialFilterFc);
		if(initialFilterQ != null) d_initialFilterQ = DoublePart.asDouble(initialFilterQ);
		if(delayModLFO != null) d_delayModLFO = DoublePart.asDouble(delayModLFO);
		if(freqModLFO != null) d_freqModLFO = DoublePart.asDouble(freqModLFO);
		if(modLfoToPitch != null) d_modLfoToPitch = DoublePart.asDouble(modLfoToPitch);
		if(modLfoToFilterFc != null) d_modLfoToFilterFc = DoublePart.asDouble(modLfoToFilterFc);
		if(modLfoToVolume != null) d_modLfoToVolume = DoublePart.asDouble(modLfoToVolume);
		if(delayVibLFO != null) d_delayVibLFO = DoublePart.asDouble(delayVibLFO);
		if(freqVibLFO != null) d_freqVibLFO = DoublePart.asDouble(freqVibLFO);
		if(vibLfoToPitch != null) d_vibLfoToPitch = DoublePart.asDouble(vibLfoToPitch);	
		if(keynumToModEnvHold != null) d_keynumToModEnvHold = DoublePart.asDouble(keynumToModEnvHold);	
		if(keynumToModEnvDecay != null) d_keynumToModEnvDecay = DoublePart.asDouble(keynumToModEnvDecay);	
		if(keynumToVolEnvHold != null) d_keynumToVolEnvHold = DoublePart.asDouble(keynumToVolEnvHold);	
		if(keynumToVolEnvDecay != null) d_keynumToVolEnvDecay = DoublePart.asDouble(keynumToVolEnvDecay);
		
		
		if(d_vibLfoToPitch != 0)
		{
			lfoTable_vibToPitch = new double[lfoTable_size_overlap];
			for (int i = 0; i < lfoTable_size_overlap; i++) {
				lfoTable_vibToPitch[i] = Math.pow(2, d_vibLfoToPitch*Math.sin((i * 2.0* Math.PI) /(double)lfoTable_size) ); 
			}
		}
		if(d_modLfoToPitch != 0)
		{
			lfoTable_modToPitch = new double[lfoTable_size_overlap];
			for (int i = 0; i < lfoTable_size_overlap; i++) {
				lfoTable_modToPitch[i] = Math.pow(2, d_modLfoToPitch*Math.sin((i * 2.0* Math.PI) /(double)lfoTable_size) ); 
			}			
		}
		if(d_modLfoToFilterFc != 0)
		{
			lfoTable_modToFilterFc = new double[lfoTable_size_overlap];
			for (int i = 0; i < lfoTable_size_overlap; i++) {
				lfoTable_modToFilterFc[i] = Math.pow(2, d_modLfoToFilterFc*(  (Math.cos((i * 2.0* Math.PI) /(double)lfoTable_size))  )); 
			}			
		}
		if(d_modLfoToVolume != 1)
		{
			lfoTable_modToVolume = new double[lfoTable_size_overlap];
			for (int i = 0; i < lfoTable_size_overlap; i++) {
				lfoTable_modToVolume[i] = Math.pow(d_modLfoToVolume, Math.sin((i * 2.0* Math.PI) /(double)lfoTable_size)); 
			}				
		}
		
		
		if(!b_lowpass)
		{
			d_modLfoToFilterFc = 0;
			d_modEnvToFilterFc = 0;
			d_initialFilterFc = 20000;
			d_initialFilterQ = 0;
		}

		
		if(isImmutable(initialAttenuation)) initialAttenuation = null;
		
		if(isImmutable(delayVolEnv)) delayVolEnv = null;
		if(isImmutable(attackVolEnv)) attackVolEnv = null;
		if(isImmutable(holdVolEnv)) holdVolEnv = null;
		if(isImmutable(decayVolEnv)) decayVolEnv = null;
		if(isImmutable(sustainVolEnv)) sustainVolEnv = null;
		if(isImmutable(releaseVolEnv)) releaseVolEnv = null;
		
		if(isImmutable(delayModEnv)) delayModEnv = null;
		if(isImmutable(attackModEnv)) attackModEnv = null;
		if(isImmutable(holdModEnv)) holdModEnv = null;
		if(isImmutable(decayModEnv)) decayModEnv = null;
		if(isImmutable(sustainModEnv)) sustainModEnv = null;
		if(isImmutable(releaseModEnv)) releaseModEnv = null;	
		if(isImmutable(modEnvToPitch)) modEnvToPitch = null;
		if(isImmutable(modEnvToFilterFc)) modEnvToFilterFc = null;	
		
		
		//sample = null;
		if(isImmutable(sampleRate)) sampleRate = null;	
		if(isImmutable(scaleTune)) scaleTune = null;
		if(isImmutable(coarseTune)) coarseTune = null;
		
		if(isImmutable(coarseTune)) initialFilterFc = null;
		if(isImmutable(initialFilterQ)) initialFilterQ = null;
		if(isImmutable(delayModLFO)) delayModLFO = null;
		if(isImmutable(freqModLFO)) freqModLFO = null;
		if(isImmutable(modLfoToPitch)) modLfoToPitch = null;
		if(isImmutable(modLfoToFilterFc)) modLfoToFilterFc = null;
		if(isImmutable(modLfoToVolume)) modLfoToVolume = null;
		if(isImmutable(delayVibLFO)) delayVibLFO = null;
		if(isImmutable(freqVibLFO)) freqVibLFO = null;
		if(isImmutable(vibLfoToPitch)) vibLfoToPitch = null;
		if(isImmutable(keynumToModEnvHold)) keynumToModEnvHold = null;
		if(isImmutable(keynumToModEnvDecay)) keynumToModEnvDecay = null;	
		if(isImmutable(keynumToVolEnvHold)) keynumToVolEnvHold = null;
		if(isImmutable(keynumToVolEnvDecay)) keynumToVolEnvDecay = null;
		if(isImmutable(lowpass)) lowpass = null;
		
		
	}
	
	public boolean isImmutable(Variable variable)
	{
		if(variable == null) return false;
		return DoublePart.getInstance(variable).isImmutable();
	}
	
	public void calc() {	
		
		namespace.addToCommitStack(this);		
	}
	
	public void close() {
		
		if(lowpass != null) DoublePart.getInstance(lowpass).removeListener(this);
		if(velocity != null) DoublePart.getInstance(velocity).removeListener(this);
		if(keynum != null) DoublePart.getInstance(keynum).removeListener(this);
		for (int j = 0; j < channelsVols.length; j++) {
			if(channelsVols[j] != null) DoublePart.getInstance(channelsVols[j]).removeListener(this);			
		}		
		if(initialAttenuation != null) DoublePart.getInstance(initialAttenuation).removeListener(this);
		
		if(delayVolEnv != null) DoublePart.getInstance(delayVolEnv).removeListener(this);
		if(attackVolEnv != null) DoublePart.getInstance(attackVolEnv).removeListener(this);
		if(holdVolEnv != null) DoublePart.getInstance(holdVolEnv).removeListener(this);
		if(decayVolEnv != null) DoublePart.getInstance(decayVolEnv).removeListener(this);
		if(sustainVolEnv != null) DoublePart.getInstance(sustainVolEnv).removeListener(this);
		if(releaseVolEnv != null) DoublePart.getInstance(releaseVolEnv).removeListener(this);
		
		if(delayModEnv != null) DoublePart.getInstance(delayModEnv).removeListener(this);
		if(attackModEnv != null) DoublePart.getInstance(attackModEnv).removeListener(this);
		if(holdModEnv != null) DoublePart.getInstance(holdModEnv).removeListener(this);
		if(decayModEnv != null) DoublePart.getInstance(decayModEnv).removeListener(this);
		if(sustainModEnv != null) DoublePart.getInstance(sustainModEnv).removeListener(this);
		if(releaseModEnv != null) DoublePart.getInstance(releaseModEnv).removeListener(this);
		
		if(modEnvToPitch != null) DoublePart.getInstance(modEnvToPitch).removeListener(this);
		if(modEnvToFilterFc != null) DoublePart.getInstance(modEnvToFilterFc).removeListener(this);
		
		if(sampleRate != null) DoublePart.getInstance(sampleRate).removeListener(this);
		
		if(scaleTune != null) DoublePart.getInstance(scaleTune).removeListener(this);
		if(coarseTune != null) DoublePart.getInstance(coarseTune).removeListener(this);	    
		
		if(initialFilterFc != null) DoublePart.getInstance(initialFilterFc).removeListener(this);
		if(initialFilterQ != null) DoublePart.getInstance(initialFilterQ).removeListener(this);
		if(delayModLFO != null) DoublePart.getInstance(delayModLFO).removeListener(this);
		if(freqModLFO != null) DoublePart.getInstance(freqModLFO).removeListener(this);
		if(modLfoToPitch != null) DoublePart.getInstance(modLfoToPitch).removeListener(this);
		if(modLfoToFilterFc != null) DoublePart.getInstance(modLfoToFilterFc).removeListener(this);
		if(modLfoToVolume != null) DoublePart.getInstance(modLfoToVolume).removeListener(this);
		if(delayVibLFO != null) DoublePart.getInstance(delayVibLFO).removeListener(this);
		if(freqVibLFO != null) DoublePart.getInstance(freqVibLFO).removeListener(this);
		if(vibLfoToPitch != null) DoublePart.getInstance(vibLfoToPitch).removeListener(this);	
		if(keynumToModEnvHold != null) DoublePart.getInstance(keynumToModEnvHold).removeListener(this);	
		if(keynumToModEnvDecay != null) DoublePart.getInstance(keynumToModEnvDecay).removeListener(this);	
		if(keynumToVolEnvHold != null) DoublePart.getInstance(keynumToVolEnvHold).removeListener(this);	
		if(keynumToVolEnvDecay != null) DoublePart.getInstance(keynumToVolEnvDecay).removeListener(this);					
		
		output.remove(answer);
	}
	
}

public class AudioVoiceFactory implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Voice factory");
		metadata.add(-1, "output",				"Output",		null, null, MetaData.TYPE_UNKNOWN, MetaData.DIRECTION_OUT);		
		metadata.add(-1, "velocity",			"Velocity", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "keynum",				"keynum", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "ch1Vol",				"ch1Vol", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "ch2Vol",				"ch2Vol", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "ch3Vol",				"ch3Vol", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "ch4Vol",				"ch4Vol", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "initialAttenuation",	"initialAttenuation", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "delayVolEnv",			"delayVolEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "attackVolEnv",		"attackVolEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "holdVolEnv",			"holdVolEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "decayVolEnv",			"decayVolEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "sustainVolEnv",		"sustainVolEnv", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "releaseVolEnv",		"releaseVolEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "delayModEnv",			"delayModEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "attackModEnv",		"attackModEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "holdModEnv",			"holdModEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "decayModEnv",			"decayModEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "sustainModEnv",		"sustainModEnv", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "releaseModEnv",		"releaseModEnv", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "modEnvToPitch",		"modEnvToPitch", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "modEnvToFilterFc",	"modEnvToFilterFc", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "sample",				"sample", null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		metadata.add(-1, "sampleRate",			"sampleRate", null, "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "scaleTune",			"scaleTune", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "coarseTune",			"coarseTune", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);		
		metadata.add(-1, "initialFilterFc",		"initialFilterFc", null, "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "initialFilterQ",		"initialFilterQ", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "delayModLFO",			"delayModLFO", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "freqModLFO",			"freqModLFO", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "modLfoToPitch",		"modLfoToPitch", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "modLfoToFilterFc",	"modLfoToFilterFc", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "modLfoToVolume",		"modLfoToVolume", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "delayVibLFO",			"delayVibLFO", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "freqVibLFO",			"freqVibLFO", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "vibLfoToPitch",		"vibLfoToPitch", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "keynumToModEnvHold",	"keynumToModEnvHold", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "keynumToModEnvDecay",	"keynumToModEnvDecay", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "keynumToVolEnvHold",	"keynumToVolEnvHold", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "keynumToVolEnvDecay",	"keynumToVolEnvDecay", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "lowpass",				"lowpass", null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		return metadata;		
	}		
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioVoiceFactoryInstance(parameters);
	}
}