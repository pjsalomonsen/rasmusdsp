/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioPlayerInstance implements UnitInstancePart
{
	
	static ShortMessage tickmessage = new ShortMessage();
	static {
		try {
			tickmessage.setMessage(0xF9);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
	}
	static ShortMessage stopmessage = new ShortMessage();
	static {
		try {
			stopmessage.setMessage(ShortMessage.STOP);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
	}
	static ShortMessage startmessage = new ShortMessage();
	static {
		try {
			startmessage.setMessage(ShortMessage.START);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
	}	
	
	
	Variable output;
	Variable input;	
	Variable controlin;
	Variable controlout;
	Variable sync;
	Receiver syncout;
	Variable prebufferlen;
	Variable priority;
	Variable maxpolyphony;	
	
	AudioPlayerInstance mpi = this;
	
	class AudioFeeder implements Runnable
	{

		int bufferlen = 500;
		ArrayBlockingQueue<double[]> queue = null;
		double[][] buffers = null;
		double[] buffer;
		int curbuffindex = 0;
		AudioSession session;
		boolean buffering_active = true;
		
		public boolean isBuffering()
		{
			return buffering_active;
		}
		public boolean isFull()
		{
			return queue.remainingCapacity() == 0;
		}
		
		public AudioFeeder(double rate, int channels)
		{					
			if(syncout != null)
			{
				syncout.send(startmessage, -1);
			}
			
			session = new AudioSession(rate, channels);
			session.setRealTime(false);
			
			buffer10mseclen = ((int)(rate * 0.01)) * channels;
			totallen = buffer10mseclen;
			
			if(maxpolyphony != null)
				session.setMaxPolyphony((int)DoublePart.asDouble(maxpolyphony));
			else
				session.setMaxPolyphony(40);
				
			double d_prebufferlen;
			if(prebufferlen != null)
				d_prebufferlen = DoublePart.asDouble(prebufferlen);
			else
				d_prebufferlen = 2;
			
			if(d_prebufferlen < 0.0001) buffering_active = false;
						
			bufferlen -= bufferlen % session.getChannels();
			
			if(!buffering_active)
			{
				buffer = new double[bufferlen];
				stream = session.openStream(input);
				return;
			}
			int totalbufflen = ((int)(session.getRate() * d_prebufferlen)) * session.getChannels();			
			int buffcount = (totalbufflen / bufferlen) + 1;			
			queue = new ArrayBlockingQueue<double[]>(buffcount);
			buffers = new double[buffcount+10][bufferlen];			
			
		}
		
		boolean active = true;
		AudioStream stream = null;
		
		public boolean isActive()
		{
			synchronized (this) {
				return active;
			}
		}
		
		public void stopProcessing()
		{			
			synchronized (this) {
				active = false;
			}
			
			if(!buffering_active)
			{
				stream.close();
				session.close();
				stream = null;
				return;
			}
			
			queue.clear();
			
			if(syncout != null)
			{				
				syncout.send(stopmessage, -1);
			}			
		}
		
		int totallen = 0;
		int buffer10mseclen = 0;
		boolean firstread = false;
		
		public double[] read()
		{
			
			if(syncout != null)
			{
				if(totallen >= totallen)
				{
						int c = totallen / buffer10mseclen;
						totallen %= buffer10mseclen;
						for (int i = 0; i < c; i++) {
							syncout.send(tickmessage, -1);
						}
				}

				totallen += bufferlen;
			}				
			
			if(!buffering_active)
			{				
				if(stream == null) return null;
				int ret = stream.replace(buffer, 0, buffer.length);
				if(ret == -1) return null;
				if(ret != bufferlen) Arrays.fill(buffer, ret, bufferlen, 0);
				return buffer;							
			}
			
			return queue.poll();
		}
				
		
		public void run() {
			AudioStream stream = session.openStream(input);
			while(true)
			{
				synchronized (this) {
					if(!active) break;
				}
				curbuffindex++;
				curbuffindex = curbuffindex % buffers.length;
				double[] buffer = buffers[curbuffindex];
				int ret = stream.replace(buffer, 0, bufferlen);
				if(ret == -1)
				{
					break;
				}
				if(ret != bufferlen) Arrays.fill(buffer, ret, bufferlen, 0);
				try {
					queue.put(buffer);
				} catch (InterruptedException e) {
					e.printStackTrace();
					break;
				}
			}			
			stream.close();
			session.close();
			synchronized (this) {
				active = false;
			}			
		}
		

	}
	
	Receiver recv = new Receiver()
	{
		public void send(MidiMessage arg0, long arg1) {
			if(arg0 instanceof ShortMessage)
			{
				ShortMessage sms = (ShortMessage)arg0;
				if(sms.getStatus() == ShortMessage.START) start();
				if(sms.getStatus() == ShortMessage.STOP) stop();
				if(sms.getStatus() == ShortMessage.TIMING_CLOCK) clock();
			}
		}
		public void close() {
		}
	};
	
	private class MediaPlayerAudioStream implements AudioStream
	{
		AudioFeeder af;
		Thread thread;
		int syncmode;
		boolean af_active = false;
		boolean started = false;
		boolean active = false;
		AudioSession session;
		public MediaPlayerAudioStream(AudioSession session)
		{			
			started = false;				
			this.session = session;
		}
		
		public void start()
		{
			if(af_active) return;
			af_active = true;
			syncmode = (int)DoublePart.asDouble(sync);
			af = new AudioFeeder(session.getRate(), session.getChannels());
			if(!af.isBuffering())
			{			
				if(syncmode == 1) started = false; else started = true;
				return;
			}
			thread = new Thread(af);
			
			if(priority != null)
			{
				double pri = DoublePart.asDouble(priority);
				if(pri > 0.5) thread.setPriority(Thread.MAX_PRIORITY);
				else if(pri < -0.5) thread.setPriority(Thread.MIN_PRIORITY);				
			}
			else
				thread.setPriority(Thread.NORM_PRIORITY);
			
			thread.start();					
		}
		public void stop()
		{
			if(!af_active) return;
			af_active = false;
			started = false;
			af.stopProcessing();
			synchronized (AudioPlayerInstance.this) {
				playing = false;
				syncstart = false;
			}
		}
		
		public boolean isStarted()
		{
			boolean playing;
			boolean syncstart;
			synchronized (AudioPlayerInstance.this) {
				playing = AudioPlayerInstance.this.playing;
				syncstart = AudioPlayerInstance.this.syncstart;
			}
			
			if(!started)
			{
				if(playing)
				{
					if(!af_active) start();
					
					if(syncmode == 1)
					{					
						// sync on first clock message
						started = syncstart;
					}
					else
					{
						// no synching, just wait for buffer to be full
						if(af.isFull() || !af.isActive())
						{
							started = true;
						}
					}
				}
			}	
			else
			{
				if(!playing)
				{
					started = false;
					stop();
				}
			}
			return started;
		}
		
		double[] readbuffer = null;
		int readpos = 0;
		
		public int mix(double[] buffer, int start, int end) {				
			if(!isStarted()) 
				return end - start;
					
			int curpos = start;
			int r = readpos;
			while(curpos != end)
			{
				
				if(readbuffer == null)
				{
					readbuffer = af.read();
					r = 0;
					if(readbuffer == null)
					{
						if(af.isActive())
						{
							syncmode = 0;
							started = false;
							break;
						}
						if(curpos == 0) 
						{
							stop();
							return end - start;
						}
						break;
					}
				}

				int remain = readbuffer.length - r;
				int out_remain = end - curpos;

				if(remain >= out_remain)
				{					
					for (; curpos < end; r++, curpos++) {
						buffer[curpos] += readbuffer[r];					
					}
					curpos = end;
					break;
				}
				else
				{
					for (; r < readbuffer.length; r++, curpos++) {
						buffer[curpos] += readbuffer[r];
					}
					readbuffer = null;
				}
			}			
			readpos = r;
			return curpos - start;
		}
		
		public int replace(double[] buffer, int start, int end) {
			if(!isStarted())
			{
				Arrays.fill(buffer, start, end, 0);
				return end - start;
			}
			
			int curpos = start;
			int r = readpos;
			while(curpos != end)
			{
				
				if(readbuffer == null)
				{
					readbuffer = af.read();
					r = 0;
					if(readbuffer == null)
					{
						if(af.isActive())
						{
							syncmode = 0;
							started = false;
							break;
						}
						
						if(curpos == 0) 
						{
							stop();
							Arrays.fill(buffer, start, end, 0);
							return end - start;
						}
						
						break;
					}
				}
				
				int remain = readbuffer.length - r;
				int out_remain = end - curpos;
				
				if(remain >= out_remain)
				{					
					for (; curpos < end; r++, curpos++) {
						buffer[curpos] = readbuffer[r];					
					}
					curpos = end;
					break;
				}
				else
				{
					for (; r < readbuffer.length; r++, curpos++) {
						buffer[curpos] = readbuffer[r];
					}
					readbuffer = null;
				}
			}			
			readpos = r;
			return curpos - start;
		}
		
		public int isStatic(double[] buffer, int len) {
			if(!isStarted()) 
				return len;			
			return -1;
		}
		
		public int skip(int len) {
			if(!isStarted())
			{
				return len;
			}
			
			int curpos = 0;
			int r = readpos;
			while(curpos != len)
			{
				
				if(readbuffer == null)
				{
					readbuffer = af.read();
					r = 0;
					if(readbuffer == null)
					{
						if(af.isActive())
						{
							syncmode = 0;
							started = false;
							break;
						}
						
						if(curpos == 0) 
						{
							stop();
							return len;
						}
						
						break;
					}
				}

				int remain = readbuffer.length - r;
				int out_remain = len - curpos;

				if(remain >= out_remain)
				{					
					curpos = len;
					break;
				}
				else
				{
					readbuffer = null;
				}
			}			
			readpos = r;
			return curpos;
		}
		
		public void close() {
			stop();
		}
	}
	
	AudioStreamable streamable = new AudioStreamable()
	{
		public AudioStream openStream(AudioSession session) {
			return new MediaPlayerAudioStream(session);
		}
	};	
	
	Variable audioevents = AudioEvents.asVariable(new AudioEvent(0, streamable));
	
	boolean playing = false;
	boolean syncstart = false;
		
	public void start()
	{
		stop();
		synchronized (this) {
			if(!playing)
			{
				syncstart = false;
				playing = true;
			}
		}
	}
	public void stop()
	{
		synchronized (this) {
			if(playing)
			{
				syncstart = false;
				playing = false;
			}			
		}		
	}	
	public void clock()
	{
		synchronized (this) {
			syncstart = true;
		}
	}

	public AudioPlayerInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		controlin = parameters.getParameterWithDefault(1, "controlin");
		controlout = parameters.getParameterWithDefault(2, "controlout");
		sync = parameters.getParameter(3, "sync");
		prebufferlen = parameters.getParameter(4, "prebuffer");
		priority = parameters.getParameter(5, "priority");
		maxpolyphony = parameters.getParameter(6, "maxpolyphony");		
		Variable syncout = parameters.getParameter("syncout");
		if(syncout != null)
		{
			this.syncout = MidiSequence.getInstance(syncout);
		}
		
		MidiSequence.getInstance(controlin).addReceiver(recv);		
		output.add(audioevents);
	}
	public void close() {
		stop();
		MidiSequence.getInstance(controlin).removeReceiver(recv);
		output.remove(audioevents);
	}
	
}

public class AudioPlayer implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Audio Player");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		
		metadata.add(1, "controlin",	"controlin",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(2, "controlout",	"controlout",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(3, "sync",			"sync",			"0",  null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		// Sync Mode
		//  0 = no synching
		//  1 = sync on first clock message
		//  2 = slave on clock messages (external tempo support) (not supported in Audio Player)
		
		metadata.add(4, "prebuffer",	"prebuffer",	"2", "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(5, "priority",		"priority",	    null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(6, "maxpolyphony",	"Max Voice Polyphony",	"40", "voices", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		
		return metadata;		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioPlayerInstance(parameters);
	}
}
