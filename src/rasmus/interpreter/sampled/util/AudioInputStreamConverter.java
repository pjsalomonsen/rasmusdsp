/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import rasmus.interpreter.sampled.AudioStream;

public class AudioInputStreamConverter extends InputStream {

	AudioStream stream;
	int sampleSizeInBits;
	int sampleSizeInBytes;
	boolean signed;
	boolean bigEndian;
	public AudioInputStreamConverter(AudioStream stream, int sampleSizeInBits, boolean signed, boolean bigEndian)
	{
		this.stream = stream;
		this.sampleSizeInBits = sampleSizeInBits;
		this.signed = signed;
		this.bigEndian = bigEndian;
		sampleSizeInBytes = sampleSizeInBits / 8;
	}
	
	public void close() throws IOException {
		stream.close();
	}

	double[] buffer = null;
	public int read(byte[] b, int off, int len) throws IOException {
		
		int dlen = len / sampleSizeInBytes;
		if(buffer == null || buffer.length < len)
			buffer = new double[dlen];
		
		
		int ret = stream.isStatic(buffer, dlen);
		if(ret != -1)
		{
			double val = buffer[0];
			Arrays.fill(buffer, 0, dlen, val);
		}
		else
		{
			ret = stream.replace(buffer, 0, dlen);
			if(ret == -1) return -1;
		}
		
		// Perform clipping (-1 >= x >= 1);
		for (int k = 0; k < dlen; k++) {
			double fvalue = buffer[k];
			if(fvalue > 1.0) buffer[k] = 1.0;
			else if(fvalue < -1.0) buffer[k] = -1.0;
		}
		
		int z = off;
		
		if(sampleSizeInBytes == 1) // 8 bit
		{
			if(signed)
			{
			for (int k = 0; k < dlen; k++) {
				double fvalue = buffer[k];
				int valueL = ((int)(fvalue * (128.0)));
				b[z] = (byte) valueL;
				z ++;
			}
			}
			else
			{
				for (int k = 0; k < dlen; k++) {
					double fvalue = buffer[k];
					int valueL = ((int)(fvalue * (128.0)));
					b[z] = (byte) (valueL & 0xFF); // TODO Has not been tested!!!!
					z ++;
				}
			}
		}
		else
		{
		    if(bigEndian)
		    {
				if(sampleSizeInBytes == 2) // 16 bit
				{
					for (int k = 0; k < dlen; k++) {
						double fvalue = buffer[k];
						int valueL = ((int)(fvalue * (32768.0)));
						b[z + 1] = (byte) valueL;
						b[z] = (byte) (valueL >>> 8);			
						z += 2;
					}
				}
				else
					if(sampleSizeInBytes == 3) // 24 bit
					{
						for (int k = 0; k < dlen; k++) {
							double fvalue = buffer[k];
							int valueL = ((int)(fvalue * (8388608.0))); 
							b[z + 2] = (byte) valueL;
							b[z + 1] = (byte) (valueL >>> 8);			
							b[z] = (byte) (valueL >>> 16);			
							z += 3;
						}
					}		
					else
						if(sampleSizeInBytes == 4) // 32 bit
						{
							for (int k = 0; k < dlen; k++) {
								double fvalue = buffer[k];
								int valueL = ((int)(fvalue * (2147483648.0)));
								b[z + 2] = (byte) valueL;
								b[z + 1] = (byte) (valueL >>> 8);			
								b[z] = (byte) (valueL >>> 16);			
								z += 3;
							}
						}					    	
		    }
		    else
		    {
			if(sampleSizeInBytes == 2) // 16 bit
			{
				for (int k = 0; k < dlen; k++) {
					double fvalue = buffer[k];
					int valueL = ((int)(fvalue * (32768.0)));
					b[z] = (byte) valueL;
					b[z + 1] = (byte) (valueL >>> 8);			
					z += 2;
				}
			}
			else
				if(sampleSizeInBytes == 3) // 24 bit
				{
					for (int k = 0; k < dlen; k++) {
						double fvalue = buffer[k];
						int valueL = ((int)(fvalue * (8388608.0))); 
						b[z] = (byte) valueL;
						b[z + 1] = (byte) (valueL >>> 8);			
						b[z + 2] = (byte) (valueL >>> 16);			
						z += 3;
					}
				}		
				else
					if(sampleSizeInBytes == 4) // 32 bit
					{
						for (int k = 0; k < dlen; k++) {
							double fvalue = buffer[k];
							int valueL = ((int)(fvalue * (2147483648.0)));
							b[z] = (byte) valueL;
							b[z + 1] = (byte) (valueL >>> 8);			
							b[z + 2] = (byte) (valueL >>> 16);			
							b[z + 3] = (byte) (valueL >>> 24);			
							z += 4;
						}
					}			
		    }
		}
		
		return ret * sampleSizeInBytes;
	}

	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}

	public long skip(long n) throws IOException {
		long ret = stream.skip((int)(n / sampleSizeInBytes));
		if(ret == -1) return -1;
		return ret*sampleSizeInBytes;
	}

	public int read() throws IOException {
		byte[] b = new byte[1];
		int ret = read(b);
		if(ret == -1) return -1; 
		return b[0];
	}
	
}
