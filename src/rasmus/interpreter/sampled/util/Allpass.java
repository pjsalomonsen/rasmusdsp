/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;
import java.util.Arrays;

public class Allpass {
	
	float	feedback;
	double[] buffer;
	int		bufsize;
	int		bufidx = 0;		
	
	public boolean isSilent()
	{
		for (int i = 0; i < buffer.length; i++) {
			if(buffer[i] != 0) return false;
		}
		return true;
	}
	
	public Allpass(int size)
	{
		buffer = new double[size];
		bufsize = size;
	}			
	
	public void setbuffer(double[] buf, int size)
	{
		buffer = buf;
		bufsize = size;
	}
	public void mute()
	{				
		Arrays.fill(buffer, 0);
	}
	public void setfeedback(float value)
	{
		feedback = value;
	}
	public float getfeedback()
	{
		return feedback;
	}			
	public void processReplace(double inputs[], double outputs[], int from, int to, int channels)
	{
		for (int c = 0; c < channels; c++) 
			for (int i = from; i < to; i+=channels) {
				
				double bufout = buffer[bufidx];
				
				//undenormalise 
				if(bufout > 0.0) if(bufout < 1.0E-10) bufout = 0;
				if(bufout < 0.0) if(bufout > -1.0E-10) bufout = 0;
				
				double input = inputs[i];
				outputs[i] = -input + bufout;
				buffer[bufidx] = input + bufout*feedback;
				if(++bufidx>=bufsize) bufidx = 0;										
			}
	}			
	public void processMix(double inputs[], double outputs[], int from, int to, int channels)
	{
		for (int c = 0; c < channels; c++) 
			for (int i = from + c; i < to; i+=channels) {
				
				double bufout = buffer[bufidx];
				
				//undernormalise
				if(bufout > 0.0) if(bufout < 1.0E-10) bufout = 0;
				if(bufout < 0.0) if(bufout > -1.0E-10) bufout = 0;
				
				double input = inputs[i];
				outputs[i] += -input + bufout;
				buffer[bufidx] = input + bufout*feedback;
				if(++bufidx>=bufsize) bufidx = 0;										
			}
	}	
}
