/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;


public class HarmonicsBooster extends FFTWorker {
	
	public HarmonicsBooster(int fftFrameSize, int osamp, double sampleRate)
	{
		super(fftFrameSize, osamp, sampleRate);
		
		fft_magn_buffer = new double[fftFrameSize*2];
		fft_magn = new FFT(fftFrameSize);
		
	}
	
	FFT fft_magn;
	double[] fft_magn_buffer;
	
	public void processFFT(int index, double[] fftdata)
	{		
		for (int i = 0; i < fftFrameSize; i++) {
			double real = fftdata[2*i];
			double imag = fftdata[2*i+1];
			fft_magn_buffer[2*i] =  Math.sqrt(real*real + imag*imag) ;
			fft_magn_buffer[2*i + 1] = 0;			
		}
		
		fft_magn.calc(fft_magn_buffer, -1);
		
		for (int i = 64; i < 256 ; i++) {
			fft_magn_buffer[2*i]     = fft_magn_buffer[2*i] * 1.5;
			fft_magn_buffer[2*i + 1] = fft_magn_buffer[2*i+ 1] * 1.5;
		}
		
		for (int i = fftFrameSize - 256; i < fftFrameSize - 64 ; i++) {
			fft_magn_buffer[2*i]     = fft_magn_buffer[2*i] * 1.5;
			fft_magn_buffer[2*i + 1] = fft_magn_buffer[2*i+ 1] * 1.5;
		}
		
		
		//for (int i = fftFrameSize2; i < fftFrameSize; i++) {
		//	fft_magn_buffer[2* i]   =  0;
		//    fft_magn_buffer[2* i+1] =  0 ;			
		//}
		
		
		fft_magn.calc(fft_magn_buffer, 1);
		
		for (int i = 0; i < fftFrameSize-0; i++) {
			
			double real = fftdata[2*i];
			double imag = fftdata[2*i+1];
			
			double magn = Math.sqrt(real*real + imag*imag);
			double target_magn = (fft_magn_buffer[2*i] / (fftFrameSize) );
			double diff = target_magn / magn;
			
			fftdata[2*i] = real * diff;
			fftdata[2*i + 1] = imag * diff;		
		}							
		
	}
	
	
	
	
}
