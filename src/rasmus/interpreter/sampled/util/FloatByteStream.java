/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import rasmus.interpreter.sampled.AudioStream;

public class FloatByteStream extends InputStream
{
	AudioStream stream;
	ByteOrder order;
	public FloatByteStream(AudioStream stream, ByteOrder order)
	{
		this.stream = stream;
		this.order = order;
	}
	public void close() throws IOException {
		stream.close();
	}
	double[] buffer = null;
	float[] fbuffer = null;
	FloatBuffer dbuffer = null;
	ByteBuffer bbuffer = null;
	
	public int read(byte[] b, int off, int len) throws IOException {
		
		int dlen = len / 4;
		if(buffer == null || buffer.length < len)
		{
			buffer = new double[dlen];
			fbuffer = new float[dlen];
			bbuffer = ByteBuffer.allocate(len).order(order);
			dbuffer = bbuffer.asFloatBuffer();
		}
				
		int ret = stream.replace(buffer, 0, dlen);
		if(ret == -1) return -1;
		
		// Convert Double to Float
		for (int i = 0; i < dlen; i++) {
			fbuffer[i] = (float)buffer[i];
		}
		
		dbuffer.position(0);		
		dbuffer.put(fbuffer);
		
		bbuffer.position(0);
		bbuffer.get(b, off, len);
		
		return ret * 4;
	}
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}
	public long skip(long n) throws IOException {
		long ret = stream.skip((int)(n / 4));
		if(ret == -1) return -1;
		return ret*4;
	}
	public int read() throws IOException {
		byte[] b = new byte[1];
		int ret = read(b);
		if(ret == -1) return -1; 
		return b[0];
	}
}