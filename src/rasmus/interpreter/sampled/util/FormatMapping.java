/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.util.Arrays;

public class FormatMapping extends FFTWorker {
	
	public FormatMapping(int fftFrameSize, int osamp, double sampleRate)
	{
		super(fftFrameSize, osamp, sampleRate);
		
		binsperformants = (int)(250 / freqPerBin);
		if(binsperformants <= 0) binsperformants = 1;
		
		magnmap = new double[fftFrameSize2];
	}
	
	int binsperformants;
	double[] formatbuffer = null;
	double[] magnmap = null;
	public void processFormatScan(double[] magnmap, double[] outdata)
	{	
		int runner_len = binsperformants;	
		
		if(formatbuffer == null)
			formatbuffer = new double[runner_len];
		
		double[] formatbuffer = this.formatbuffer;
		Arrays.fill(formatbuffer, 0.0); 
		
		int runner = 0;	
		double totalsum = 0;
		for (int i = 0; i < fftFrameSize2; i++) {
			double m = magnmap[i];
			
			totalsum -= formatbuffer[runner];
			totalsum += m;
			formatbuffer[runner] = m;
			runner++;
			runner%=runner_len;
			
			outdata[i] = (totalsum / runner_len); // The running sum	
		}
		
	}	
	
	public void processFFT(int index, double[] fftdata)
	{		
		
		/* zero negative frequencies */
		Arrays.fill(fftdata, fftFrameSize , 2*fftFrameSize , 0);
		
		
		// Before Intensity Map
		for (int ii = 0; ii < fftFrameSize2; ii++) {
			double real = fftdata[2*ii];
			double imag = fftdata[2*ii+1];
			magnmap[ii] = Math.sqrt(real*real + imag*imag);
		}
		processFormatScan(magnmap, magnmap);
		
		int fftFrameSize2_1 = fftFrameSize2 - 1;
		for (int x = 0; x < fftFrameSize2; x++) {
			
			// The Format Mapping Function
			//double y = x/formatpitchShift;
			
			double xn = (x / ((double)fftFrameSize2));			
			double yn = xn / 2; //Math.pow(xn, 0.75 + (xn/4)); //Math.sin(xn * 2*Math.PI) / Math.tan(xn * 2*Math.PI);					
			double y = yn * fftFrameSize2; 
			//double y = x;
			
			
			int yi = (int)(y);			
			double magn1 = magnmap[x];
			if(magn1 > 0)
			{				
				if ((yi < fftFrameSize2_1) && (yi >= 0))
				{			
					double yj = y - yi;			
					double magn2 = magnmap[yi]*(1 - yj) + magnmap[yi+1]*(yj);
					magn1 = magn2 / magn1;
					
					fftdata[2*x] = fftdata[2*x]*magn1;
					fftdata[2*x+1] = fftdata[2*x+1]*magn1;			    				
					
					//magn2 = Math.sqrt(magn2);
					//fftdata[2*x] = (Math.random()*2 - 1)*magn2;
					//fftdata[2*x+1] = (Math.random()*2 - 1)*magn2;			    
				}
				else
				{
					fftdata[2*x] = 0;
					fftdata[2*x+1] = 0;			    				
				}
			}
			
		}				
		
	}
	
	
	
	
}
