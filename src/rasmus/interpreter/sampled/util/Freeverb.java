/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;
import java.util.Arrays;

public class Freeverb			
{
	
	static final float muted			= 0;
	static final float fixedgain		= 0.015f * 2;
	static final float scalewet		= 3;
	static final float scaledry		= 2;
	static final float scaledamp		= 0.4f;
	static final float scaleroom		= 0.28f;
	static final float offsetroom		= 0.7f;
	static final float initialroom		= 0.5f;
	static final float initialdamp		= 0.5f;
	static final float initialwet		= 1/scalewet;
	static final float initialdry		= 1;//0;
	static final float initialwidth	= 1f;
	static final float initialmode		= 0.0f;
	static final float freezemode		= 0.5f;
	
	
	float	gain;
	float	roomsize,roomsize1;
	float	damp,damp1;
	float	wet,wet1,wet2;
	float	dry;
	float	width;
	float	mode;		
	
	// Comb filters
	int	numcombs;
	Comb[] combL;
	Comb[] combR;
	
	// Allpass filters
	int numallpasses;
	Allpass[] allpassL;
	Allpass[] allpassR;		
	
	boolean r_isSilent = true;
	public boolean isSilent()
	{
		if(r_isSilent) return true;
		
		for (int i = 0; i < numcombs; i++) {
			if(!combL[i].isSilent()) return false;
			if(!combR[i].isSilent()) return false;
		}
		
		for (int i = 0; i < numallpasses; i++) {
			if(!allpassL[i].isSilent()) return false;
			if(!allpassR[i].isSilent()) return false;
		}
		
		r_isSilent = true;
		return true;
	}
	
	
	public Freeverb(double samplerate, double scale)
	{						
		double freqscale =  scale * ((double)samplerate) / 44100.0  ;
		int	stereospread	    = 23;	
		
		/* Init Comb filters */
		
		int combtuningL1		= (int)(freqscale*(1116));
		int combtuningR1		= (int)(freqscale*(1116+stereospread));
		int combtuningL2		= (int)(freqscale*(1188));
		int combtuningR2		= (int)(freqscale*(1188+stereospread));
		int combtuningL3		= (int)(freqscale*(1277));
		int combtuningR3		= (int)(freqscale*(1277+stereospread));
		int combtuningL4		= (int)(freqscale*(1356));
		int combtuningR4		= (int)(freqscale*(1356+stereospread));
		int combtuningL5		= (int)(freqscale*(1422));
		int combtuningR5		= (int)(freqscale*(1422+stereospread));
		int combtuningL6		= (int)(freqscale*(1491));
		int combtuningR6		= (int)(freqscale*(1491+stereospread));
		int combtuningL7		= (int)(freqscale*(1557));
		int combtuningR7		= (int)(freqscale*(1557+stereospread));
		int combtuningL8		= (int)(freqscale*(1617));
		int combtuningR8		= (int)(freqscale*(1617+stereospread));
		
		numcombs = 8;
		combL = new Comb[numcombs];
		combR = new Comb[numcombs];
		combL[0] = new Comb(combtuningL1);
		combR[0] = new Comb(combtuningR1);
		combL[1] = new Comb(combtuningL2);
		combR[1] = new Comb(combtuningR2);
		combL[2] = new Comb(combtuningL3);
		combR[2] = new Comb(combtuningR3);
		combL[3] = new Comb(combtuningL4);
		combR[3] = new Comb(combtuningR4);
		combL[4] = new Comb(combtuningL5);
		combR[4] = new Comb(combtuningR5);
		combL[5] = new Comb(combtuningL6);
		combR[5] = new Comb(combtuningR6);
		combL[6] = new Comb(combtuningL7);
		combR[6] = new Comb(combtuningR7);
		combL[7] = new Comb(combtuningL8);
		combR[7] = new Comb(combtuningR8);
		
		/* Init Allpass filters */		
		
		int allpasstuningL1	= (int)(freqscale*(556));
		int allpasstuningR1	= (int)(freqscale*(556+stereospread));
		int allpasstuningL2	= (int)(freqscale*(441));
		int allpasstuningR2	= (int)(freqscale*(441+stereospread));
		int allpasstuningL3	= (int)(freqscale*(341));
		int allpasstuningR3	= (int)(freqscale*(341+stereospread));
		int allpasstuningL4	= (int)(freqscale*(225));
		int allpasstuningR4	= (int)(freqscale*(225+stereospread)); 				
		
		numallpasses = 4;
		allpassL = new Allpass[numallpasses];
		allpassR = new Allpass[numallpasses];			
		allpassL[0] = new Allpass(allpasstuningL1);
		allpassR[0] = new Allpass(allpasstuningR1);
		allpassL[1] = new Allpass(allpasstuningL2);
		allpassR[1] = new Allpass(allpasstuningR2);
		allpassL[2] = new Allpass(allpasstuningL3);
		allpassR[2] = new Allpass(allpasstuningR3);
		allpassL[3] = new Allpass(allpasstuningL4);
		allpassR[3] = new Allpass(allpasstuningR4);				
		
		for (int i = 0; i < numallpasses; i++) {
			allpassL[i].setfeedback(0.5f);
			allpassR[i].setfeedback(0.5f);					
		}
		
		/* Init other settings */				
		
		setwet(initialwet);
		setroomsize(initialroom);
		setdry(initialdry);
		setdamp(initialdamp);
		setwidth(initialwidth);
		setmode(initialmode);
		
		/* Prepare all buffers */	
		
		mute();
		
	}
	
	
	double[] input = null;
	double outL[] = null;
	double outR[] = null;
	
	public void processReplace(double[] inputA, double[] output, int from, int to, int channels)
	{
		r_isSilent = false;
		int numsamples = (to - from) / channels;
		if(input == null || input.length < numsamples)
		{
			input = new double[numsamples];
			outL = new double[numsamples];
			outR = new double[numsamples];
		}		
		
		double again = gain / channels;
		int ix = 0;
		for (int i = from; i < to; i+=channels) {
			input[ix] = inputA[i] * again;				
			ix++;
		}		
		
		for (int c = 1; c < channels; c++) {
			ix = 0;
			for (int i = from + c; i < to; i+=channels) {
				input[ix] += inputA[i] * again;				
				ix++;
			}		
		}			
		
		Arrays.fill(outL, 0);
		Arrays.fill(outR, 0);
		
		for(int i=0; i<numcombs; i++)
		{
			combL[i].processMix(input, outL, 0, numsamples, 1);
			combR[i].processMix(input, outR, 0, numsamples, 1);
		}				
		
		for(int i=0; i<numallpasses; i++)
		{
			allpassL[i].processReplace(outL, outL, 0, numsamples, 1);
			allpassR[i].processReplace(outR, outR, 0, numsamples, 1);
		}	
		
		if(channels == 2)
		{
			ix = 0;
			if(dry == 0)
			{
				for (int i = from; i < to; i+=channels) {
					output[i] = outL[ix]*wet1 + outR[ix]*wet2;
					output[i+1] = outR[ix]*wet1 + outL[ix]*wet2;	
					ix++;
				}		  	
			}
			else
			{
				for (int i = from; i < to; i+=channels) {
					output[i] = outL[ix]*wet1 + outR[ix]*wet2 + inputA[i]*dry;
					output[i+1] = outR[ix]*wet1 + outL[ix]*wet2 + inputA[i+1]*dry;	
					ix++;
				}
			}
		}
		else if(channels == 1)
		{
			if(dry == 0)
			{
				ix = 0;
				for (int i = from; i < to; i++) {
					output[i] = outL[ix]*wet1 + outR[ix]*wet2;
					ix++;
				}
			}
			else
			{
				ix = 0;
				for (int i = from; i < to; i++) {
					output[i] = outL[ix]*wet1 + outR[ix]*wet2 + inputA[ix]*dry;
					ix++;
				}		  	
			}
		}
		
		
	}	
	/*
	 public void processMix(double[] inputA, double[] output, int from, int to, int channels)
	 {
	 
	 int numsamples = (to - from) / channels;
	 double[] input = new double[numsamples];
	 double again = gain / channels;
	 int ix = 0;
	 for (int i = from; i < to; i+=channels) {
	 input[ix] = inputA[i] * again;				
	 ix++;
	 }		
	 
	 for (int c = 1; c < channels; c++) {
	 ix = 0;
	 for (int i = from + c; i < to; i+=channels) {
	 input[ix] += inputA[i] * again;				
	 ix++;
	 }		
	 }
	 
	 
	 double outL[] = new double[numsamples];
	 double outR[] = new double[numsamples];
	 
	 Arrays.fill(outL, 0);
	 Arrays.fill(outR, 0);
	 
	 for(int i=0; i<numcombs; i++)
	 {
	 combL[i].processMix(input, outL, 0, numsamples, 1);
	 combR[i].processMix(input, outR, 0, numsamples, 1);
	 }				
	 
	 for(int i=0; i<numallpasses; i++)
	 {
	 allpassL[i].processReplace(outL, outL, 0, numsamples, 1);
	 allpassR[i].processReplace(outR, outR, 0, numsamples, 1);
	 }	
	 
	 if(channels == 2)
	 {
	 ix = 0;
	 for (int i = from; i < to; i+=channels) {
	 output[i] += outL[ix]*wet1 + outR[ix]*wet2 + inputA[i]*dry;
	 output[i+1] += outR[ix]*wet1 + outL[ix]*wet2 + inputA[i+1]*dry;	
	 ix++;
	 }
	 }
	 else if(channels == 1)
	 {
	 ix = 0;
	 for (int i = from; i < to; i+=channels) {
	 output[i] += outL[ix]*wet1 + outR[ix]*wet2 + inputA[i]*dry;
	 ix++;
	 }
	 }
	 
	 }	*/	
	
	public void mute()
	{
		if (getmode() >= freezemode)
			return;
		
		for (int i=0;i<numcombs;i++)
		{
			combL[i].mute();
			combR[i].mute();
		}
		for (int i=0;i<numallpasses;i++)
		{
			allpassL[i].mute();
			allpassR[i].mute();
		}
	}
	
	
	private void update()
	{
		
		int i;
		
		wet1 = wet*(width/2 + 0.5f);
		wet2 = wet*((1-width)/2);
		
		if (mode >= freezemode)
		{
			roomsize1 = 1;
			damp1 = 0;
			gain = muted;
		}
		else
		{
			roomsize1 = roomsize;
			damp1 = damp;
			gain = fixedgain;
		}
		
		for(i=0; i<numcombs; i++)
		{
			combL[i].setfeedback(roomsize1);
			combR[i].setfeedback(roomsize1);
		}
		
		for(i=0; i<numcombs; i++)
		{
			combL[i].setdamp(damp1);
			combR[i].setdamp(damp1);
		}
	}			
	
	public void setroomsize(float value)
	{
		roomsize = (value*scaleroom) + offsetroom;
		update();
	}
	
	public float getroomsize()
	{
		return (roomsize-offsetroom)/scaleroom;
	}
	
	public void setdamp(float value)
	{
		damp = value*scaledamp;
		update();
	}
	
	public float getdamp()
	{
		return damp/scaledamp;
	}
	
	public void setwet(float value)
	{
		wet = value*scalewet;
		update();
	}
	
	public float getwet()
	{
		return wet/scalewet;
	}
	
	public void setdry(float value)
	{
		dry = value*scaledry;
	}
	
	public float getdry()
	{
		return dry/scaledry;
	}
	
	public void setwidth(float value)
	{
		width = value;
		update();
	}
	
	public float getwidth()
	{
		return width;
	}
	
	public void setmode(float value)
	{
		mode = value;
		update();
	}
	
	public float getmode()
	{
		if (mode >= freezemode)
			return 1;
		else
			return 0;
	}			
	
}