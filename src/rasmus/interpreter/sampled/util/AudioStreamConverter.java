/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.util.Arrays;

import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;

public class AudioStreamConverter implements AudioStream {
	
	AudioSession session = null;
	AudioSession sourcesession = null;
	AudioSession convertsession = null;
	AudioStream sourcestream = null;
	AudioCache audiocache = null;
	
	public AudioStreamConverter(AudioSession session)
	{
		this.session = session;
		audiocache = session.getAudioCache();
	}
	
	private void setSourceSession(AudioSession targetsession)
	{
		if(this.sourcesession != targetsession)
		{
			this.sourcesession = targetsession;
			if(targetsession == null)
			{
				convertsession = null;
				return;
			}			
			convertsession = session.newSession(targetsession.getRate(), targetsession.getChannels());			
		}		
	}
	
	AudioStream currenttargetstream = null;
	private void setSourceStream(AudioStream targetstream)
	{
		if(targetstream != currenttargetstream)
		{
			if(targetstream == null)
			{
				currenttargetstream = null;
				this.sourcestream = null;
				return;
			}
			currenttargetstream = targetstream;
			if(sourcesession.getRate() != session.getRate()) 
			{
				this.sourcestream = new WaveUtils.ResampleStream(targetstream, session.getRate() / sourcesession.getRate(), sourcesession.getChannels());
			}
			else
				this.sourcestream = targetstream;
			
		}
	}
	
	public void setSource(AudioSession targetsession, AudioStream targetstream)
	{
		setSourceSession(targetsession);
		setSourceStream(targetstream);
	}
	
	public AudioSession getSession()
	{
		return convertsession;
	}
	
	private int convertToSource(int pos)
	{
		if(pos == -1) return -1;
		
		// Convert to mono channels
		pos /= session.getChannels();
		// Convert to source channels
		pos *= sourcesession.getChannels();
		
		return pos;
	}
	
	private int convertFromSource(int pos)
	{
		if(pos == -1) return -1;
		
		// Convert to mono channels
		pos /= sourcesession.getChannels();
		// Convert to source channels
		pos *= session.getChannels();
		
		return pos;
	}	
	
	private boolean isConvertNeeded()
	{
		if(sourcesession.getChannels() != session.getChannels()) return true;
		return false;
	}
	
	public int mix(double[] buffer, int start, int end) {
		if(!isConvertNeeded()) 
			return sourcestream.mix(buffer, start, end); 
		
		int len = end - start;
		int clen = convertToSource(len);
		double[] readbuffer = audiocache.getBuffer(clen); 
		int cret = sourcestream.replace(readbuffer, 0, clen);
		if(cret == -1) return -1;
		if(cret != clen) Arrays.fill(readbuffer, cret, clen, 0);
		
		int session_channels = session.getChannels();
		int sourcesession_channels = sourcesession.getChannels();
		
		
		// Mono channel case, perform downmix
		if(session_channels == 1)
		{
			for (int c = 0; c < sourcesession_channels; c++) {
				int j = c;
				for (int i = start; i < end; i++) {
					buffer[i] += readbuffer[j];
					j += sourcesession_channels;
				}
			}
			audiocache.returnBuffer(readbuffer);
			return len;
		}
		
		// Multi channel case
		for (int c = 0; c < session_channels; c++) {
			int j = c;
			if(j >= sourcesession_channels) j = sourcesession_channels - 1;			
			for (int i = start + c; i < end; i+= session_channels) {
				buffer[i] += readbuffer[j];
				j += sourcesession_channels;
			}
		}
		audiocache.returnBuffer(readbuffer);		
		return len;
	}
	public int replace(double[] buffer, int start, int end) {
		if(!isConvertNeeded()) return sourcestream.replace(buffer, start, end);
		
		Arrays.fill(buffer, start, end, 0);
		return mix(buffer, start, end);
	}
	public int isStatic(double[] buffer, int len) {
		if(!isConvertNeeded()) return sourcestream.isStatic(buffer, len);
		
		return convertFromSource(sourcestream.isStatic(buffer, convertToSource(len)));
	}
	public int skip(int len) {
		if(!isConvertNeeded()) return sourcestream.skip(len);
		
		return convertFromSource(sourcestream.skip(convertToSource(len)));
	}
	public void close() {
		sourcestream.close();
	}
	
}
