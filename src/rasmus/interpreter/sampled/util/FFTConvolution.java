/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.util.Arrays;

public class FFTConvolution {

	FFT fft;
	int convFrameSize;
	int convFrameSize2;	
	int fftFrameSize;
	int fftFrameSize2;
	double fftGain;
	double[][] convBuffers;
	double[][] smplBuffers;
	int smplBufferPos = 0;
	int convBufferCount;
		
	double[] inputBuffer;
	double[] outputBuffer;
	double[] outputBuffer2;
	int bufferPos = 0;
	
	public FFTConvolution(double[] convolutionbuffer, int fftFrameSize)
	{
		fft = new FFT(fftFrameSize);
		this.fftFrameSize = fftFrameSize;
		this.fftFrameSize2 = fftFrameSize * 2;
		this.convFrameSize = fftFrameSize / 2;
		this.convFrameSize2 = fftFrameSize;
		fftGain = 2.0 / (fftFrameSize);
		
		inputBuffer = new double[fftFrameSize];
		outputBuffer = new double[fftFrameSize];
		outputBuffer2 = new double[fftFrameSize];
		
		convBufferCount = (convolutionbuffer.length / convFrameSize);
		if(convolutionbuffer.length % convFrameSize != 0) convBufferCount++;		
		
		convBuffers = new double[convBufferCount][];
		smplBuffers = new double[convBufferCount][];
		int pos = 0;
		for (int i = 0; i < convBufferCount; i++) {
			double[] convBuffer = new double[fftFrameSize];
			smplBuffers[i] = new double[fftFrameSize];
			convBuffers[i] = convBuffer;
			
			int end = pos + convFrameSize;
			if(end > convolutionbuffer.length) end = convolutionbuffer.length;
			int p = 0;
			for (int j = pos; j < end; j++) {
				convBuffer[p] = convolutionbuffer[j];
				p ++;
			}			
			fft.calcReal(convBuffer, -1);
			pos = end;
		}
	}
	
	public void process(int start, int end, int interlace, double[] indata, double[] outdata)
	{
		int convFrameSize = this.convFrameSize;
		int bufferPos = this.bufferPos;
		for (int i = start; i < end; i+= interlace) {
			inputBuffer[bufferPos] = indata[i];
			outdata[i] = outputBuffer[bufferPos] + outputBuffer2[bufferPos + convFrameSize];
			bufferPos ++;//= 2;
			if(bufferPos == convFrameSize)
			{
				double[] bak = outputBuffer2;
				outputBuffer2 = outputBuffer;
				outputBuffer = inputBuffer;
				inputBuffer = bak;
				Arrays.fill(inputBuffer, 0);
				process(outputBuffer);
				bufferPos = 0;
			}			
		}
		this.bufferPos = bufferPos;
	}
	
	public void process(double[] buffer)
	{
	
		double fftGain = this.fftGain;
		int sp = smplBufferPos;
		
		fft.calcReal(buffer, -1);				
		double[] smplBuffer = smplBuffers[sp];
		for (int i = 0; i < fftFrameSize; i++) {
			smplBuffer[i] = buffer[i] * fftGain;
		}
		
		Arrays.fill(buffer, 0);
		for (int i = 0; i < convBufferCount; i++) {
			double[] sampleBuff = smplBuffers[sp];
			double[] convBuff = convBuffers[i];
			for (int j = 0; j < fftFrameSize; j+= 2) {
				double s_r = sampleBuff[j];
				double s_i = sampleBuff[j+1]; 
				double c_r = convBuff[j];
				double c_i = convBuff[j+1]; 
				buffer[j]   += s_r * c_r - s_i * c_i;
				buffer[j+1] += s_r * c_i + c_r * s_i;
			}			
			sp--;
			if(sp == -1) sp = convBufferCount - 1;
		} 		
		
		fft.calcReal(buffer, 1);
				
		smplBufferPos++;
		smplBufferPos %= convBufferCount;  
		
	}

}
