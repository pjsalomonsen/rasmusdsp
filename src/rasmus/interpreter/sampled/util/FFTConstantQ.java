/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

// Implementation of Constant Q Transform 
//
// References:
//
// Judith C. Brown, 
// Calculation of a constant Q spectral transform, J. Acoust. Soc. Am., 89(1): 425-434, 1991.
//    see http://www.wellesley.edu/Physics/brown/pubs/cq1stPaper.pdf
//
// Judith C. Brown and MillerS. Puckette, 
// An efficient algorithm for the calculation of a constant Q transform, J. Acoust. Soc. Am., Vol. 92, No. 5, November 1992
//    see http://www.wellesley.edu/Physics/brown/pubs/effalgV92P2698-P2701.pdf
//
// Benjamin Blankertz, 
// The Constant Q Transform
//    see http://wwwmath1.uni-muenster.de/logik/org/staff/blankertz/constQ/constQ.pdf
//



public class FFTConstantQ {

	
	double q;   // Constant Q
	int k;      // Number of output bands
	int fftlen; // FFT size
	
	double[] freqs;
	double[][] qKernel;
	int[][] qKernel_indexes;
	FFT fft;
	
	File file = null;
	
	public FFT getFFT()
	{
		return fft;
	}
	
	public int getFFTSize()
	{
		return fftlen;
	}
	public int getNumberOfOutputBands()
	{
		return k;
	}
	
	
	double sampleRate = 44100; 
    double minFreq = 100;
    double maxFreq = 3000;
    double binsPerOctave = 12;
    double threshold = 0.001;  // Lower number, better quality !!! 0 = best
    
    String kernelsident;
	
	public FFTConstantQ(double sampleRate, 
            double minFreq, 
            double maxFreq, 
            double binsPerOctave)
	{
		this.sampleRate = sampleRate;
		this.minFreq = minFreq;
		this.maxFreq = maxFreq;
		this.binsPerOctave = binsPerOctave;
		init();
	}
	
	public FFTConstantQ(double sampleRate, 
            double minFreq, 
            double maxFreq, 
            double binsPerOctave,
            double threshold)
	{
		this.sampleRate = sampleRate;
		this.minFreq = minFreq;
		this.maxFreq = maxFreq;
		this.binsPerOctave = binsPerOctave;
		this.threshold = threshold;
		init();
	}	
	
	public FFTConstantQ(double sampleRate, 
            double minFreq, 
            double maxFreq, 
            double binsPerOctave,
            File file)
	{
		this.sampleRate = sampleRate;
		this.minFreq = minFreq;
		this.maxFreq = maxFreq;
		this.binsPerOctave = binsPerOctave;
		this.file = file;
		init();
	}		
	
	private void init()
	{
		
		
		
		// Calculate Constant Q
		q = 1.0 / ( Math.pow(2, 1.0 / binsPerOctave) - 1.0 );
		
		// Calculate number of output bins
	    k = (int)Math.ceil(binsPerOctave * Math.log(maxFreq /  minFreq) / Math.log(2));
	    
	    // Calculate length of FFT
	    double calc_fftlen = Math.ceil(q * sampleRate / minFreq);
	    fftlen = (int)Math.pow(2, Math.ceil(Math.log(calc_fftlen)/Math.log(2)));

	    // Create FFT object
	    fft = new FFT(fftlen);
	    qKernel = new double[k][]; 
	    qKernel_indexes = new int[k][]; 
	    freqs= new double[k];
	    
	    kernelsident = "R" + sampleRate + "S" + minFreq + "_E" + maxFreq + "_B" + binsPerOctave + "_T" + threshold + ".fftqkernels";
	    
	    //InputStream is = getClass().getResourceAsStream("/rasmus/interpreter/sampled/util/" + kernelsident);
	    /*
	    File userhome = new File(System.getProperty("user.home"));
	    File rasmusdsphome = new File(userhome, ".rasmusdsp");
	    if(!rasmusdsphome.exists()) rasmusdsphome.mkdirs();
	    File file = new File(rasmusdsphome,kernelsident);
	    */
	    if(file != null)
	    if(file.exists())
	    {	    		    	
	    	try
	    	{
	    		InputStream is = new FileInputStream(file);
	    		try
	    		{
	    			readKernels(is);
	    		}
	    		finally
	    		{
	    			is.close();
	    		}
	    		return;
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    	
	    }
	    
	    // Calculate Constant Q kernels
	    
	    double[] temp = new double[fftlen*2];
	    double[] ctemp = new double[fftlen*2];
	    int[] cindexes = new int[fftlen];
	    for (int i = 0; i < k; i++) {
	    	double[] sKernel = temp;
	    	// Calculate the frequency of current bin
	    	freqs[i] = minFreq * Math.pow(2, i/binsPerOctave );
	    	
	    	// Calculate length of window
	    	int len = (int)Math.ceil( q * sampleRate / freqs[i]);
	    	for (int j = 0; j < len; j++) {
	    		
	    		double window = -.5*Math.cos(2.*Math.PI*(double)j/(double)len)+.5;; // Hanning Window
	    		// double window = -.46*Math.cos(2.*Math.PI*(double)j/(double)len)+.54; // Hamming Window

	    		window /= len;
	    		
	    		// Calculate kernel
	    	    double x = 2*Math.PI * q * (double)j/(double)len;
	    		sKernel[j*2] = window * Math.cos(x);
	    		sKernel[j*2+1] = window * Math.sin(x);	
	    		
	    		
			}
	    	for (int j = len*2; j < fftlen*2; j++) {
	    		sKernel[j] = 0;
	    	}
	    		    	
	    	// Perform FFT on kernel
	    	fft.calc(sKernel, -1);
	    		    
	    	// Remove all zeros from kernel to improve performance
	    	double[] cKernel = ctemp; 
	    	
	    	int k = 0;
	    	for (int j = 0, j2 = sKernel.length - 2; j < sKernel.length/2; j+=2,j2-=2)
	    	{
	    		double absval = Math.sqrt(sKernel[j]*sKernel[j] + sKernel[j+1]*sKernel[j+1]);
	    	    absval += Math.sqrt(sKernel[j2]*sKernel[j2] + sKernel[j2+1]*sKernel[j2+1]);	    	    
	    		if(absval > threshold)
	    		{
	    			cindexes[k] = j;
	    			cKernel[2*k] = sKernel[j] + sKernel[j2];
	    			cKernel[2*k + 1] = sKernel[j + 1] + sKernel[j2 + 1];
	    			k++;
	    		}	    		
	    	}
	    	
	    		
	    	sKernel = new double[k*2];
	    	int[] indexes = new int[k];
	    		    	
	    	for (int j = 0; j < k*2; j++) 
	    		sKernel[j] = cKernel[j];			
	    	for (int j = 0; j < k; j++) 
	    		indexes[j] = cindexes[j];
	    	
	    	// Normalize fft output
	    	for (int j = 0; j < sKernel.length; j++) sKernel[j] /= fftlen;
	    	
	    	// Perform complex conjugate on sKernel	    	
	    	for (int j = 0; j < sKernel.length; j+=2) sKernel[j] = -sKernel[j] ;
	    	
	    	
	    	qKernel_indexes[i] = indexes;
	    	qKernel[i] = sKernel;	    	
	    	
		}	    

	    //writeKernels(file); //new File(kernelsident));
	}
	
	public void readKernels(InputStream is) throws IOException
	{
		for (int i = 0; i < k; i++) {
	    	ByteBuffer kbuffer = ByteBuffer.allocate(4); // Single 32 bit Integer	    	
	    	is.read(kbuffer.array(), kbuffer.arrayOffset(), kbuffer.capacity());
	    	kbuffer.position(0);
	    	int len = kbuffer.getInt();
	    	
	    	qKernel_indexes[i] = new int[len];
	    	qKernel[i]  = new double[len*2];			
	    	
	    	ByteBuffer kernbuffer = ByteBuffer.allocate(len*8*2); // 64 bit Double, complex pairs
	    	ByteBuffer indxbuffer = ByteBuffer.allocate(len*4); // 32 bit integer
	    	
	    	is.read(kernbuffer.array(), kernbuffer.arrayOffset(), kernbuffer.capacity());
	    	is.read(indxbuffer.array(), indxbuffer.arrayOffset(), indxbuffer.capacity());
	    	
	    	kernbuffer.position(0);
	    	indxbuffer.position(0);
	    	
	    	kernbuffer.asDoubleBuffer().get(qKernel[i]);
	    	indxbuffer.asIntBuffer().get(qKernel_indexes[i]);
		}
	}
	
	public void writeKernels(File file)
	{
		
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();

	    for (int i = 0; i < k; i++) {
	    	int len = qKernel_indexes[i].length;
	    	ByteBuffer kbuffer = ByteBuffer.allocate(4); // Single 32 bit Integer
	    	kbuffer.putInt(len);
	    	baos.write(kbuffer.array(), kbuffer.arrayOffset(), kbuffer.capacity());
	    	
	    	ByteBuffer kernbuffer = ByteBuffer.allocate(len*8*2); // 64 bit Double, complex pairs
	    	kernbuffer.asDoubleBuffer().put(qKernel[i]);
	    	baos.write(kernbuffer.array(), kernbuffer.arrayOffset(), kernbuffer.capacity());
	    	
	    	ByteBuffer indxbuffer = ByteBuffer.allocate(len*4); // 32 bit integer
	    	indxbuffer.asIntBuffer().put(qKernel_indexes[i]);
	    	baos.write(indxbuffer.array(), indxbuffer.arrayOffset(), indxbuffer.capacity());
	    }
	    try
	    {
	    FileOutputStream faos = new FileOutputStream(file);
	    faos.write(baos.toByteArray());
	    faos.close();	    
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    System.out.println(file.getPath() + " writen " + baos.size());
				
	}
	
	public void calc(double[] buff_in, double[] buff_out)
	{				
		fft.calcReal(buff_in, -1);
		for (int i = 0; i < qKernel.length; i++) {			
			double[] kernel = qKernel[i];
			int[] indexes = qKernel_indexes[i];
			double t_r = 0;
			double t_i = 0;		
			for (int j = 0, l = 0; j < kernel.length; j+=2, l++) {
				int jj = indexes[l];				
				double b_r = buff_in[jj];
				double b_i = buff_in[jj + 1];
				double k_r = kernel[j];
				double k_i = kernel[j + 1];
				// COMPLEX: T += B * K
				t_r += b_r * k_r - b_i * k_i;
				t_i += b_r * k_i + b_i * k_r;
			}
			buff_out[i*2] = t_r;
			buff_out[i*2 + 1] = t_i;
		}
	}

}
