/* 
 * Part of this code is Copyright (c) 1996 S.M.Bernsee under The Wide Open License.
 * 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */package rasmus.interpreter.sampled.util;
 
 import java.util.Arrays;
 
 public class PitchShiftOrg {
	 
	 /* CONVERSION TO JAVA BY: Karl Helgason (kalli@bigfoot.com), 2005 */
	 
	 /* ***************************************************************************
	  *
	  * NAME: smbPitchShift.cpp
	  * VERSION: 1.1
	  * HOME URL: http://www.dspdimension.com
	  * KNOWN BUGS: none
	  *
	  * SYNOPSIS: Routine for doing pitch shifting while maintaining
	  * duration using the Short Time Fourier Transform.
	  *
	  * DESCRIPTION: The routine takes a pitchShift factor value which is between 0.5
	  * (one octave down) and 2. (one octave up). A value of exactly 1 does not change
	  * the pitch. numSampsToProcess tells the routine how many samples in indata[0...
	  * numSampsToProcess-1] should be pitch shifted and moved to outdata[0 ...
	  * numSampsToProcess-1]. The two buffers can be identical (ie. it can process the
	  * data in-place). fftFrameSize defines the FFT frame size used for the
	  * processing. Typical values are 1024, 2048 and 4096. It may be any value <=
	  * MAX_FFT_FRAME_LENGTH but it MUST be a power of 2. osamp is the STFT
	  * oversampling factor which also determines the overlap between adjacent STFT
	  * frames. It should at least be 4 for moderate scaling ratios. A value of 32 is
	  * recommended for best quality. sampleRate takes the sample rate for the signal 
	  * in unit Hz, ie. 44100 for 44.1 kHz audio. The data passed to the routine in 
	  * indata[] should be in the range [-1.0, 1.0), which is also the output range 
	  * for the data, make sure you scale the data accordingly (for 16bit signed integers
	  * you would have to divide (and multiply) by 32768). 
	  *
	  * COPYRIGHT 1999-2003 Stephan M. Bernsee <smb [AT] dspdimension [DOT] com>
	  *
	  * 						The Wide Open License (WOL)
	  *
	  * Permission to use, copy, modify, distribute and sell this software and its
	  * documentation for any purpose is hereby granted without fee, provided that
	  * the above copyright notice and this license appear in all source copies. 
	  * THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF
	  * ANY KIND. See http://www.dspguru.com/wol.htm for more information.
	  *
	  * ****************************************************************************/ 
	 
	 public final static double M_PI = 3.14159265358979323846;
	 public final static int MAX_FRAME_LENGTH = 8192;
	 
	 private double[] gInFIFO = new double[MAX_FRAME_LENGTH];
	 private double[] gOutFIFO = new double[MAX_FRAME_LENGTH];
	 private double[] gFFTworksp = new double[2*MAX_FRAME_LENGTH];
	 private double[] gLastPhase = new double[MAX_FRAME_LENGTH/2+1];
	 private double[] gSumPhase = new double[MAX_FRAME_LENGTH/2+1];
	 private double[] gOutputAccum = new double[2*MAX_FRAME_LENGTH];
	 private double[] gAnaFreq = new double[MAX_FRAME_LENGTH];
	 private double[] gAnaMagn = new double[MAX_FRAME_LENGTH];
	 private double[] gSynFreq = new double[MAX_FRAME_LENGTH];
	 private double[] gSynMagn = new double[MAX_FRAME_LENGTH];
	 private int gRover = -1;
	 
	 int fftFrameSize; int osamp; double sampleRate;
	 int fftFrameSize2;
	 double[] window_table;
	 /*
	  private final static int table_len = 80000;    
	  private final static double ffactor = table_len / ( 2* Math.PI);
	  private static double[] sintable = new double[table_len];
	  private static double[] costable = new double[table_len];
	  
	  private synchronized static void precalc()
	  {
	  sintable = new double[table_len];
	  double flen  = sintable.length;
	  int len = sintable.length;
	  for (int i = 0; i < len; i++) {
	  double k = ((i*1.0) / flen) * Math.PI * 2;
	  sintable[i] = Math.sin(k);
	  costable[i] = Math.cos(k);
	  }
	  }		
	  {
	  precalc();
	  }
	  
	  public final static double sin(double inp)
	  {
	  long s_index = ((long)(inp*ffactor)) % table_len;
	  if(s_index < 0) s_index += table_len;
	  return sintable[(int)s_index];
	  
	  }
	  public final static double cos(double inp)
	  {
	  long s_index = ((long)(inp*ffactor)) % table_len;
	  if(s_index < 0) s_index += table_len;
	  return costable[(int)s_index];
	  } */
	 
	 
	 
	 public PitchShiftOrg(int fftFrameSize, int osamp, double sampleRate)
	 {
		 /* initialize our static arrays */
		 Arrays.fill(gInFIFO, 0);
		 Arrays.fill(gOutFIFO, 0);
		 Arrays.fill(gFFTworksp, 0);
		 Arrays.fill(gLastPhase, 0);
		 Arrays.fill(gSumPhase, 0);
		 Arrays.fill(gOutputAccum, 0);
		 Arrays.fill(gAnaFreq, 0);
		 Arrays.fill(gAnaMagn, 0);
		 this.fftFrameSize = fftFrameSize;
		 this.osamp = osamp;
		 this.sampleRate = sampleRate;
		 
		 fftFrameSize2 = fftFrameSize/2;
		 
		 window_table = new double[fftFrameSize];
		 for (int k = 0; k < fftFrameSize;k++) {
			 window_table[k] = -.5*Math.cos(2.*M_PI*(double)k/(double)fftFrameSize)+.5;
		 }		
	 }
	 
	 public void smbPitchShift(double pitchShift, double[] pitchtable, int p_start, int start, int end, int interlace, double[] indata, double[] outdata)
	 /*
	  Routine smbPitchShift(). See top of file for explanation
	  Purpose: doing pitch shifting while maintaining duration using the Short
	  Time Fourier Transform.
	  Author: (c)1999-2002 Stephan M. Bernsee <smb [AT] dspdimension [DOT] com>
	  */
	 {
		 double magn, phase, tmp, window, real, imag;
		 double freqPerBin, expct;
		 int i,k, qpd, index, inFifoLatency, stepSize;
		 
		 /* set up some handy variables */
		 stepSize = fftFrameSize/osamp;
		 freqPerBin = sampleRate/(double)fftFrameSize;
		 expct = 2.*M_PI*(double)stepSize/(double)fftFrameSize;
		 inFifoLatency = fftFrameSize-stepSize;
		 if (gRover == -1) gRover = inFifoLatency;
		 
		 /* main processing loop */
		 int pi = p_start;
		 for (i = start; i < end; i+= interlace){
			 
			 /* As long as we have not yet collected enough data just read in */
			 gInFIFO[gRover] = indata[i];
			 outdata[i] = gOutFIFO[gRover-inFifoLatency];
			 gRover++;
			 pi++;
			 
			 /* now we have enough data for processing */
			 if (gRover >= fftFrameSize) {
				 gRover = inFifoLatency;
				 
				 if(pitchtable != null) pitchShift = pitchtable[pi];
				 
				 /* do windowing and re,im interleave */
				 for (k = 0; k < fftFrameSize;k++) {
					 //window = -.5*Math.cos(2.*M_PI*(double)k/(double)fftFrameSize)+.5;
					 window = window_table[k];
					 gFFTworksp[2*k] = gInFIFO[k] * window;
					 gFFTworksp[2*k+1] = 0.;
				 }
				 
				 
				 /* ***************** ANALYSIS ******************* */
				 /* do transform */
				 smbFft(gFFTworksp, fftFrameSize, -1);
				 
				 /* this is the analysis step */
				 for (k = 0; k <= fftFrameSize2; k++) {
					 
					 /* de-interlace FFT buffer */
					 real = gFFTworksp[2*k];
					 imag = gFFTworksp[2*k+1];
					 
					 /* compute magnitude and phase */
					 magn = 2.*Math.sqrt(real*real + imag*imag);
					 phase = Math.atan2(imag,real);
					 
					 /* compute phase difference */
					 tmp = phase - gLastPhase[k];
					 gLastPhase[k] = phase;
					 
					 /* subtract expected phase difference */
					 tmp -= (double)k*expct;
					 
					 /* map delta phase into +/- Pi interval */
					 qpd = (int)(tmp/M_PI);
					 if (qpd >= 0) qpd += qpd&1;
					 else qpd -= qpd&1;
					 tmp -= M_PI*(double)qpd;
					 
					 /* get deviation from bin frequency from the +/- Pi interval */
					 tmp = osamp*tmp/(2.*M_PI);
					 
					 /* compute the k-th partials' true frequency */
					 tmp = (double)k*freqPerBin + tmp*freqPerBin;
					 
					 /* store magnitude and true frequency in analysis arrays */
					 gAnaMagn[k] = magn;
					 gAnaFreq[k] = tmp;
					 
				 }
				 
				 
				 
				 /* ***************** PROCESSING ******************* */
				 /* this does the actual pitch shifting */
				 Arrays.fill(gSynMagn, 0);
				 Arrays.fill(gSynFreq, 0);
				 for (k = 0; k <= fftFrameSize2; k++) {
					 index = (int)(k/pitchShift);
					 if (index <= fftFrameSize2) {
						 gSynMagn[k] += gAnaMagn[index];
						 gSynFreq[k] = gAnaFreq[index] * pitchShift;
					 }
				 }
				 
				 /* ***************** SYNTHESIS ******************* */
				 /* this is the synthesis step */
				 for (k = 0; k <= fftFrameSize2; k++) {
					 
					 /* get magnitude and true frequency from synthesis arrays */
					 magn = gSynMagn[k];
					 tmp = gSynFreq[k];
					 
					 /* subtract bin mid frequency */
					 tmp -= (double)k*freqPerBin;
					 
					 /* get bin deviation from freq deviation */
					 tmp /= freqPerBin;
					 
					 /* take osamp into account */
					 tmp = 2.*M_PI*tmp/osamp;
					 
					 /* add the overlap phase advance back in */
					 tmp += (double)k*expct;
					 
					 /* accumulate delta phase to get bin phase */
					 gSumPhase[k] += tmp;
					 phase = gSumPhase[k];
					 
					 /* get real and imag part and re-interleave */
					 gFFTworksp[2*k] = magn*Math.cos(phase);
					 gFFTworksp[2*k+1] = magn*Math.sin(phase); 
					 /*			
					  long s_index = ((long)(phase*ffactor)) % table_len;
					  if(s_index < 0) s_index += table_len;
					  
					  gFFTworksp[2*k] = magn*costable[(int)s_index];
					  gFFTworksp[2*k] = magn*sintable[(int)s_index];				  	
					  */
				 } 
				 
				 /* zero negative frequencies */
				 for (k = fftFrameSize+2; k < 2*fftFrameSize; k++) gFFTworksp[k] = 0.;
				 
				 /* do inverse transform */
				 smbFft(gFFTworksp, fftFrameSize, 1);
				 
				 /* do windowing and add to output accumulator */ 
				 for(k=0; k < fftFrameSize; k++) {
					 window = -.5*Math.cos(2.*M_PI*(double)k/(double)fftFrameSize)+.5;
					 gOutputAccum[k] += 2.*window*gFFTworksp[2*k]/(fftFrameSize2*osamp);
				 }
				 for (k = 0; k < stepSize; k++) gOutFIFO[k] = gOutputAccum[k];
				 
				 /* shift accumulator */
				 //memmove(gOutputAccum, gOutputAccum+stepSize, fftFrameSize*sizeof(float));
				 int shift_len = gOutputAccum.length / 2;
				 for (int j = 0; j < shift_len; j++) {
					 gOutputAccum[j] = gOutputAccum[j + stepSize]; 
				 }
				 
				 
				 /* move input FIFO */
				 for (k = 0; k < inFifoLatency; k++) gInFIFO[k] = gInFIFO[k+stepSize];
			 }
		 }
	 }
	 
//	 -----------------------------------------------------------------------------------------------------------------
	 
	 
	 public void smbFft(double[] fftBuffer, int fftFrameSize, int sign)
	 /* 
	  FFT routine, (C)1996 S.M.Bernsee. Sign = -1 is FFT, 1 is iFFT (inverse)
	  Fills fftBuffer[0...2*fftFrameSize-1] with the Fourier transform of the
	  time domain data in fftBuffer[0...2*fftFrameSize-1]. The FFT array takes
	  and returns the cosine and sine parts in an interleaved manner, ie.
	  fftBuffer[0] = cosPart[0], fftBuffer[1] = sinPart[0], asf. fftFrameSize
	  must be a power of 2. It expects a complex input signal (see footnote 2),
	  ie. when working with 'common' audio signals our input signal has to be
	  passed as {in[0],0.,in[1],0.,in[2],0.,...} asf. In that case, the transform
	  of the frequencies of interest is in fftBuffer[0...fftFrameSize].
	  */
	 {
		 int p1, p2;
		 int p1r, p1i, p2r, p2i;
		 
		 double wr, wi, arg, temp;
		 double tr, ti, ur, ui;
		 int i, bitm, j, le, le2, k;
		 
		 for (i = 2; i < 2*fftFrameSize-2; i += 2) {
			 for (bitm = 2, j = 0; bitm < 2*fftFrameSize; bitm <<= 1) {
				 if ((i & bitm) != 0) j++;
				 j <<= 1;
			 }
			 if (i < j) {				
				 p1  = i; p2 = j;
				 temp = fftBuffer[p1]; fftBuffer[p1++] = fftBuffer[p2];
				 fftBuffer[p2++] = temp; temp = fftBuffer[p1];
				 fftBuffer[p1] = fftBuffer[p2]; fftBuffer[p2] = temp;
			 }
		 }
		 
		 int k_len = (int)(Math.log(fftFrameSize)/Math.log(2.));
		 for (k = 0, le = 2; k < k_len; k++) {
			 le <<= 1;
			 le2 = le>>1;
			 ur = 1.0;
			 ui = 0.0;
			 arg = M_PI / (le2>>1);
			 
			 
			 wr = Math.cos(arg);
			 wi = sign*Math.sin(arg); 
			 /*
			  long s_index = ((long)(arg*ffactor)) % table_len;
			  if(s_index < 0) s_index += table_len;		  	
			  wr = costable[(int)s_index];
			  wi = sign*sintable[(int)s_index];				
			  */
			 
			 for (j = 0; j < le2; j += 2) {
				 p1r = j; p1i = p1r+1;
				 p2r = p1r+le2; p2i = p2r+1;
				 for (i = j; i < 2*fftFrameSize; i += le) {
					 
					 tr = fftBuffer[p2r] * ur - fftBuffer[p2i] * ui;
					 ti = fftBuffer[p2r] * ui + fftBuffer[p2i] * ur;
					 fftBuffer[p2r] = fftBuffer[p1r] - tr; fftBuffer[p2i] = fftBuffer[p1i] - ti;
					 fftBuffer[p1r] += tr; fftBuffer[p1i] += ti;
					 p1r += le; p1i += le;
					 p2r += le; p2i += le;
					 
				 }
				 tr = ur*wr - ui*wi;
				 ui = ur*wi + ui*wr;
				 ur = tr;
			 }
		 }
	 }
	 
	 
//	 -----------------------------------------------------------------------------------------------------------------
	 
	 /*
	  
	  12/12/02, smb
	  
	  PLEASE NOTE:
	  
	  There have been some reports on domain errors when the atan2() function was used
	  as in the above code. Usually, a domain error should not interrupt the program flow
	  (maybe except in Debug mode) but rather be handled "silently" and a global variable
	  should be set according to this error. However, on some occasions people ran into
	  this kind of scenario, so a replacement atan2() function is provided here.
	  
	  If you are experiencing domain errors and your program stops, simply replace all
	  instances of atan2() with calls to the smbAtan2() function below.
	  
	  */
	 /*
	  
	  double smbAtan2(double x, double y)
	  {
	  double signx;
	  if (x > 0.) signx = 1.;  
	  else signx = -1.;
	  
	  if (x == 0.) return 0.;
	  if (y == 0.) return signx * M_PI / 2.;
	  
	  return Math.atan2(x, y);
	  } */
	 
	 
//	 -----------------------------------------------------------------------------------------------------------------
//	 -----------------------------------------------------------------------------------------------------------------
//	 -----------------------------------------------------------------------------------------------------------------
	 
	 
 }
