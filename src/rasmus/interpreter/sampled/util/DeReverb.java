/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;


public class DeReverb extends FFTWorker {
	
	double[] lastmagn_table;
	double[] lastmagn_table_diff;
	
	int runner = 0;
	double[][] lastmagn_table_runsum;
	double[] lastmagn_table_runsum_total;
	
	public DeReverb(int fftFrameSize, int osamp, double sampleRate, int sumsmooth)
	{
		super(fftFrameSize, osamp, sampleRate);
		
		lastmagn_table_runsum = new double[fftFrameSize2][sumsmooth];
		lastmagn_table_runsum_total = new double[fftFrameSize2];
		
		
		lastmagn_table = new double[fftFrameSize2];
		lastmagn_table_diff = new double[fftFrameSize2];
	}
	
	
	public double attack = 0.1;   // Attack Smoother
	public double release = 0.8;  // Release Smoother
	public double feedback = 0.9; // Reverb feedback gain 
	public double amount = 1;     // Perform full deverbation 
	public double thresold = 1.5; // +1.5 AMP h�kkun
	
	
	public void processFFT(int index, double[] fftdata)
	{		
		
		/* zero negative frequencies */
		//Arrays.fill(fftdata, fftFrameSize , 2*fftFrameSize , 0);
		
		/* Process Magnitude */
		
		double attack = this.attack;
		
		
		double release = this.release;
		double feedback = this.feedback;
		double amount = this.amount;		
		amount = amount * ( 1 -  feedback);
		double thresold = this.thresold;
		
		int runnerlen = lastmagn_table_runsum[0].length;
		
		if(runnerlen > 1)
		{
			//lrunner = runner;
			runner++;
			if(runner == runnerlen) runner = 0;		
			
			int j = 0;
			for (int i = 0; i < fftFrameSize2; i++) {			
				
				int a = j++;
				int b = j++;
				double real = fftdata[a];
				double imag = fftdata[b];			
				
				// Smooth out magn values with a running sum, RMS window
				double magn = real*real + imag*imag; //Math.sqrt(real*real + imag*imag); // 	
				double total = lastmagn_table_runsum_total[i];
				double lastmagn = lastmagn_table_runsum[i][runner];
				lastmagn_table_runsum[i][runner] = magn;
				total += magn - lastmagn;;
				lastmagn_table_runsum_total[i] = total;
				magn = Math.sqrt(total/runnerlen);
				
				// Calculate newmagn			
				double l_magn = lastmagn_table[i];
				if(l_magn < 0.00000001) l_magn = 0; // Denormal fix
				lastmagn_table[i] = magn + l_magn*feedback; 
				double newmagn = magn - l_magn*amount;									 
				
				if(newmagn < 0) newmagn = 0;
				
				
				// Apply newmagn
				if(magn != 0)
				{
					double ldiff = lastmagn_table_diff[i];
					double diff = (2 * newmagn / magn);				
					double smooth = (diff - ldiff > thresold)?attack:release;					
					diff = diff * (1-smooth) + smooth*ldiff;
					lastmagn_table_diff[i] = diff;
					fftdata[a] = real * diff;
					fftdata[b] = imag * diff;
				}
				
			}
			
		}
		else
		{
			
			int j = 0;			
			for (int i = 0; i < fftFrameSize2; i++) {			
				
				int a = j++;
				int b = j++;
				double real = fftdata[a];
				double imag = fftdata[b];			
				double magn = Math.sqrt(real*real + imag*imag); // 	
				
				// Calculate newmagn
				double l_magn = lastmagn_table[i];
				if(l_magn < 0.00000001) l_magn = 0; // Denormal fix
				lastmagn_table[i] = magn + l_magn*feedback; 			
				double newmagn = magn - l_magn*amount;    
				if(newmagn < 0) newmagn = 0;
				
				// Apply newmagn
				if(magn != 0)
				{				
					
					double ldiff = lastmagn_table_diff[i];
					double diff = (2 * newmagn / magn);				
					double smooth = (diff - ldiff > thresold)?attack:release;					
					diff = diff * (1-smooth) + smooth*ldiff;
					lastmagn_table_diff[i] = diff;
					fftdata[a] = real * diff;
					fftdata[b] = imag * diff;
					
				}
				
			}		
			
		}
		
		
		
	}
	
	
	
	
}
