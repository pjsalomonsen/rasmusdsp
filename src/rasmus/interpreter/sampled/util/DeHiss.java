/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;


public class DeHiss extends FFTWorker {
	
	double[] lastmagn_table;
	double[] lastmagn_table_diff;
	
	int runner = 0;
	int runner_len = 0;
	double[][] lastmagn_table_runsum;
	double[] lastmagn_table_runsum_total;
	
	double[][] lastmagn_table_runsum2;
	double[] lastmagn_table_runsum2_total;
	
	public DeHiss(int fftFrameSize, int osamp, double sampleRate, int sumsmooth)
	{
		super(fftFrameSize, osamp, sampleRate);
		
		runner_len = sumsmooth;
		
		lastmagn_table_runsum = new double[sumsmooth][fftFrameSize2];
		lastmagn_table_runsum_total = new double[fftFrameSize2];
		
		lastmagn_table_runsum2 = new double[sumsmooth][fftFrameSize2];
		lastmagn_table_runsum2_total = new double[fftFrameSize2];
		
		
		lastmagn_table = new double[fftFrameSize2];
		lastmagn_table_diff = new double[fftFrameSize2];
	}
	
	
	
	public double floor      =  0.1;     // Noise Floor      = db(-20)
	public double width      =  1.001;   // Transition Width = db(.5)
	public double reduce     =  0.001;   // Reduce Hiss by   = db(-60)
	
	
	public void processFFT(int index, double[] fftdata)
	{		
		
		/* zero negative frequencies */
		//Arrays.fill(fftdata, fftFrameSize , 2*fftFrameSize , 0);
		
		/* Process Magnitude */
		
		double floor  = this.floor;
		double floorb;
		if(this.width == 0) floorb = floor; else floorb = this.floor / this.width;				
		
		double floorw = floor - floorb;		
		double reduce = this.reduce;
		
		int runner = this.runner;
		int runner_len = this.runner_len;
		
		double[] lastmagn_table_runsum = this.lastmagn_table_runsum[runner];
		double[] lastmagn_table_runsum_total = this.lastmagn_table_runsum_total;
		
		double[] lastmagn_table_runsum2 = this.lastmagn_table_runsum2[runner];
		double[] lastmagn_table_runsum2_total = this.lastmagn_table_runsum2_total;	
		
		int j = 0;
		for (int i = 0; i < fftFrameSize2; i++) {			
			
			int a = j++;
			int b = j++;
			double real = fftdata[a];
			double imag = fftdata[b];				
			double magn = real*real + imag*imag;
			
			double n = lastmagn_table_runsum[i];
			lastmagn_table_runsum_total[i] -= n;
			lastmagn_table_runsum_total[i] += magn;
			lastmagn_table_runsum[i] = magn;
			
			double avgmagn = Math.sqrt(lastmagn_table_runsum_total[i] / runner_len);
			
			double targetgain;
			if(avgmagn < floor)
				if(avgmagn > floorb)
				{
					double d = (avgmagn - floorb) / floorw;
					targetgain = reduce*(1-d) + d;
				}
				else
					targetgain = reduce; 
			else 
				targetgain = 1.0;
			
			n = lastmagn_table_runsum2[i];
			lastmagn_table_runsum2_total[i] -= n;
			lastmagn_table_runsum2_total[i] += targetgain;
			lastmagn_table_runsum2[i] = targetgain;		
			
			double avggain = Math.sqrt(lastmagn_table_runsum2_total[i] / runner_len);
			fftdata[a] = real * avggain;
			fftdata[b] = imag * avggain;
			
		}
		
		
		this.runner = (runner + 1) % runner_len;
		
	}
	
	
	
	
}
