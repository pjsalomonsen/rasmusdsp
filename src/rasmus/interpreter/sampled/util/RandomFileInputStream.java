/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

public class RandomFileInputStream extends InputStream
{
	RandomAccessFile randomfile = null;
	
	public RandomFileInputStream(File file) throws IOException
	{
		randomfile = new RandomAccessFile(file, "r");		
	}
	
	public void close() throws IOException
	{
		randomfile.close();
	}
	
	public int read() throws IOException {
		
		return randomfile.read();
	}
	
	public int available() throws IOException {
		
		return (int)(randomfile.length() - randomfile.getFilePointer());
	}
	
	long markpos = 0;	
	public synchronized void mark(int arg0) {
		try
		{
			markpos = randomfile.getFilePointer();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public boolean markSupported() {
		return true;
	}
	
	public int read(byte[] arg0) throws IOException {
		return read(arg0, 0, arg0.length);
	}
	
	public int read(byte[] arg0, int arg1, int arg2) throws IOException {
		return randomfile.read(arg0, arg1, arg2) ;
	}
	
	public synchronized void reset() throws IOException {
		randomfile.seek(markpos);
	}
	
	public long skip(long arg0) throws IOException {
		
		return randomfile.skipBytes((int)(arg0));
	}	
}