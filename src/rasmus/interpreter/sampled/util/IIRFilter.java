/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;
public 	class IIRFilter
{		
	double samplerate;
	double a1;
	double a2;
	double a3;
	double b1;
	double b2;
	
	public IIRFilter(double samplerate)
	{
		this.samplerate = samplerate;
	}
	
	public void lowpass(double cutoff, double rez)
	{
		
		/*
		 
		 http://www.musicdsp.org/showArchiveComment.php?ArchiveID=38
		 
		 Type : biquad, tweaked butterworth
		 References : Posted by Patrice Tarrabia
		 
		 r  = rez amount, from sqrt(2) to ~ 0.1
		 f  = cutoff frequency
		 (from ~0 Hz to SampleRate/2 - though many
		 synths seem to filter only  up to SampleRate/4)
		 
		 The filter algo:
		 out(n) = a1 * in + a2 * in(n-1) + a3 * in(n-2) - b1*out(n-1) - b2*out(n-2)
		 
		 */
		
		if(cutoff<10) cutoff = 10f;
		if(cutoff*2>samplerate) cutoff = samplerate / 2.0f;
		if(rez < 0.1f) rez = 0.1f;
		if(rez > 1.414f) rez = 1.414f;
		
		double c = 1.0 / Math.tan(Math.PI * cutoff / samplerate);
		
		double r = rez;
		
		a1 = 1.0 / ( 1.0 + r * c + c * c);
		a2 = 2* a1;
		a3 = a1;
		b1 = 2.0 * ( 1.0 - c*c) * a1;
		b2 = ( 1.0 - r * c + c * c) * a1;
	}			
	
	public void hipass(double cutoff, double rez)
	{				
		/*
		 
		 http://www.musicdsp.org/showArchiveComment.php?ArchiveID=38
		 
		 Type : biquad, tweaked butterworth
		 References : Posted by Patrice Tarrabia
		 
		 r  = rez amount, from sqrt(2) to ~ 0.1
		 f  = cutoff frequency
		 (from ~0 Hz to SampleRate/2 - though many
		 synths seem to filter only  up to SampleRate/4)
		 
		 The filter algo:
		 out(n) = a1 * in + a2 * in(n-1) + a3 * in(n-2) - b1*out(n-1) - b2*out(n-2)
		 
		 */
		
		if(cutoff<10) cutoff = 10f;
		if(cutoff*2>samplerate) cutoff = samplerate / 2.0f;
		if(rez < 0.1f) rez = 0.1f;
		if(rez > 1.414f) rez = 1.414f;
		
		double c = Math.tan(Math.PI * cutoff / samplerate);
		double r = rez;
		
		a1 = 1.0 / ( 1.0 + r * c + c * c);
		a2 = -2*a1;
		a3 = a1;
		b1 = 2.0 * ( c*c - 1.0) * a1;
		b2 = ( 1.0 - r * c + c * c) * a1;		
		
	}
	
	
	
	public void processReplace(double inputs[], double outputs[], int from, int to, int channels)
	{
		
		double o1 = this.o1;
		double o2 = this.o2;
		double i1 = this.i1;
		double i2 = this.i2;			
		for (int i = from; i < to; i+=channels) {			   
			double i_value = inputs[i];					   
			double o_value = (a1 * i_value + a2 * i1 + a3 * i2 - b1*o1 - b2*o2);					   
			outputs[i] = o_value;					   
			i2 = i1;
			i1 = i_value;					   
			o2 = o1;			   
			o1 = o_value;			    		  					   											 
		}						
		this.o1 = o1;
		this.o2 = o2;
		this.i1 = i1;
		this.i2 = i2;		
	}			
	
	public void processMix(double inputs[], double outputs[], int from, int to, int channels)
	{
		
		double o1 = this.o1;
		double o2 = this.o2;
		double i1 = this.i1;
		double i2 = this.i2;			
		for (int i = from; i < to; i+=channels) {			   
			double i_value = inputs[i];					   
			double o_value = (a1 * i_value + a2 * i1 + a3 * i2 - b1*o1 - b2*o2);					   
			outputs[i] += o_value;					   
			i2 = i1;
			i1 = i_value;					   
			o2 = o1;			   
			o1 = o_value;			    		  					   											 
		}						
		this.o1 = o1;
		this.o2 = o2;
		this.i1 = i1;
		this.i2 = i2;		
	}				
	
	double o1;
	double o2;
	double i1;
	double i2;				
}