/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

import javax.sound.sampled.AudioFormat;

import rasmus.util.ByteConversion;

public class WAVEAudioOutputWriter extends OutputStream {
	
	RandomAccessFile ra;
	AudioFormat format;
	public WAVEAudioOutputWriter(File file, AudioFormat format) throws IOException
	{
		ra = new RandomAccessFile(file, "rw");
		ra.setLength(44);
		this.format = format;
		writeHeader();
	}
	
	public void writeHeader() throws IOException
	{
		ra.seek(0);
		
		// RIFF Chunk
		ra.write("RIFF".getBytes("latin1"));
		long totallen = ra.length() - 8;
		if(totallen < 0) totallen = 0;
		ra.write(ByteConversion.longToDWord(totallen));		
		ra.write("WAVE".getBytes("latin1"));
		
		// FORMAT Chunk
		ra.write("fmt ".getBytes("latin1"));
		ra.write(ByteConversion.longToDWord(16));
		ra.write(ByteConversion.intToShort(1));
		ra.write(ByteConversion.intToShort(format.getChannels()));
		long srate = (long)format.getSampleRate();
		ra.write(ByteConversion.longToDWord(srate));		
		ra.write(ByteConversion.longToDWord(srate * format.getChannels() * format.getSampleSizeInBits() / 8));		
		ra.write(ByteConversion.intToShort(format.getChannels() * format.getSampleSizeInBits() / 8));
		ra.write(ByteConversion.intToShort(format.getSampleSizeInBits()));
		
		// DATA Chunk
		ra.write("data".getBytes("latin1"));
		totallen = ra.length() - 44;
		if(totallen < 0) totallen = 0;
		ra.write(ByteConversion.longToDWord(totallen));
	}
	
	public void close() throws IOException {
		writeHeader();
		ra.close();
	}
	
	public void write(byte[] b, int off, int len) throws IOException {
		ra.write(b, off, len);
	}
	public void write(int data) throws IOException {
		ra.write(data);
	}
	public void write(byte[] b) throws IOException {
		ra.write(b);
	}
}
