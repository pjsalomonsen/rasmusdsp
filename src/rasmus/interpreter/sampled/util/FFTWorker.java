/* 
 * Part of this code is Copyright (c) 1996 S.M.Bernsee under The Wide Open License.
 * see FFT.java
 * 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */




package rasmus.interpreter.sampled.util;

import java.util.Arrays;

public class FFTWorker {
	
	FFT fft;
	int fftFrameSize;
	int fftFrameSize2;
	int osamp;
	int stepSize;
	double sampleRate;
	double freqPerBin;
	double[] window_table;
	
	double[] gFFTworksp;
	double[] gOutputAccum;
	double[] gInFIFO;
	double[] gOutFIFO;
	
	int gRover = -1;
	int inFifoLatency;
	
	public FFTWorker(int fftFrameSize, int osamp, double sampleRate)
	{
		fft = new FFT(fftFrameSize);
		this.fftFrameSize = fftFrameSize;
		this.stepSize = fftFrameSize/osamp;
		this.sampleRate = sampleRate;
		this.freqPerBin = sampleRate/(double)fftFrameSize;
		this.osamp = osamp;
		window_table = fft.wHanning();
		inFifoLatency = fftFrameSize-stepSize;
		
		fftFrameSize2 = fftFrameSize/2;
		gFFTworksp = new double[fftFrameSize];
		gOutputAccum = new double[fftFrameSize*2];
		gInFIFO = new double[fftFrameSize];
		gOutFIFO = new double[fftFrameSize];	
		
		Arrays.fill(gInFIFO, 0);
		Arrays.fill(gOutFIFO, 0);		
		Arrays.fill(gOutputAccum, 0);
	}
	
	public void processFFT(int index, double[] fftdata)
	{		
	}
	
	public void process(int start, int end, int interlace, double[] indata, double[] outdata)
	{
		double window;
		int i,k;
		
		/* set up some handy variables */
		if (gRover == -1) gRover = inFifoLatency;
		
		/* main processing loop */	
		double[] gInFIFO = this.gInFIFO;
		double[] gOutFIFO = this.gOutFIFO;
		double[] gOutputAccum = this.gOutputAccum;
		double[] window_table = this.window_table;
		double[] gFFTworksp = this.gFFTworksp;
		
		for (i = start; i < end; i+= interlace){
			
			/* As long as we have not yet collected enough data just read in */
			gInFIFO[gRover] = indata[i];
			outdata[i] = gOutFIFO[gRover-inFifoLatency];
			gRover++;
			
			/* now we have enough data for processing */
			if (gRover >= fftFrameSize) {
				gRover = inFifoLatency;
				
				/* do windowing and re,im interleave */
				for (k = 0; k < fftFrameSize;k++) {
					window = window_table[k];
					gFFTworksp[k] = (gInFIFO[k] * window);
					//gFFTworksp[2*k+1] = 0.;
				}
				
				fft.calcReal(gFFTworksp, -1);
				processFFT(i, gFFTworksp);
				fft.calcReal(gFFTworksp, 1);
				
				/* do windowing and add to output accumulator */ 
				for(k=0; k < fftFrameSize; k++) {
					window = window_table[k];					
					gOutputAccum[k] += 2.*window*gFFTworksp[k]/(fftFrameSize2*osamp);
				}
				for (k = 0; k < stepSize; k++) gOutFIFO[k] = gOutputAccum[k];
				int shift_len = gOutputAccum.length / 2;
				for (int j = 0; j < shift_len; j++) {
					gOutputAccum[j] = gOutputAccum[j + stepSize]; 
				}
				for (k = 0; k < inFifoLatency; k++) gInFIFO[k] = gInFIFO[k+stepSize];
			}
		}
	}
	
	
	
}
