/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.util.Arrays;

public class LogDFT {

	private double[] log_aa = null;
	private double[] log_ptable = null;
	
	int fftFrameSize;
	public LogDFT(int fftFrameSize)
	{		
		this.fftFrameSize = fftFrameSize;
		log_aa = new double[fftFrameSize];
		
		int partials = fftFrameSize/2;
		
		//double[] window = wHanning();
		int p = 0;
		log_ptable = new double[2 * fftFrameSize * partials];
		for (int i=0;i<fftFrameSize;i++)
		{
			for(int h=0;h<(partials);h++)
			{
				// �egar h = partials-1 �� scale = 1
				// �egar h = 0          �� scale = 32
				
				
				double dpartials = partials;
				double hfs=fftFrameSize/2 * Math.pow(32, (dpartials - 1 - h) / (dpartials-1));
				double pd=Math.PI/hfs;
				double im = i-hfs;
				double th=(pd*(h+1))*im;
				
				
				
				// Hanning Windowing
				double window = -.5*Math.cos(2.*Math.PI*(double)i/(double)fftFrameSize)+.5;		    	
				log_ptable[p++] = window * Math.cos(th);
				log_ptable[p++] = window * Math.sin(th);
				
			}
		}		   
		
	}
	
	
	/*
	 Based on:
	 http://www.musicdsp.org/archive.php?classid=2#21
	 Type : fourier transform
	 References : Posted by Andy Mucho
	 
	 */
	
	// Build in Windowing :)
	public final void calcLOGDFT(double[] data)
	{
		
		int framesize = fftFrameSize;
		
		Arrays.fill(log_aa, 0);
				
		for (int i=0,p=0;i<framesize;i++)
		{
			double w=data[i*2];
			for(int h=0;h<framesize;h++)
				log_aa[h]+=w*log_ptable[p++];
		}	      
		for (int h=0;h<fftFrameSize;h++)
			data[h] = log_aa[h];
		
	}	
	
	
	

}
