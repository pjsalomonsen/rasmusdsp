/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.util.Arrays;

public class Vocoder {
	
	
	private FFT fft;
	private int fftFrameSize;
	private int fftFrameSize2;
	private int osamp;
	private int stepSize;
	private double freqPerBin;	private double[] window_table;
	
	private double[] gFFTworksp;
	private double[] gFFTworksp2;
	private double[] gOutputAccum;
	private double[] gInFIFO;
	private double[] gInFIFO2;
	private double[] gOutFIFO;
	
	
	private int gRover = -1;
	private int inFifoLatency;
	
	public Vocoder(int fftFrameSize, int osamp, double sampleRate, int vocoderSize)
	{
		fft = new FFT(fftFrameSize);
		this.fftFrameSize = fftFrameSize;
		this.stepSize = fftFrameSize/osamp;
		this.freqPerBin = sampleRate/(double)fftFrameSize;
		this.osamp = osamp;
		window_table = fft.wHanning();
		inFifoLatency = fftFrameSize-stepSize;
		
		fftFrameSize2 = fftFrameSize/2;
		gFFTworksp = new double[fftFrameSize];
		gFFTworksp2 = new double[fftFrameSize];
		gOutputAccum = new double[2*fftFrameSize];
		gInFIFO = new double[fftFrameSize];
		gInFIFO2 = new double[fftFrameSize];
		gOutFIFO = new double[fftFrameSize];	
		
		Arrays.fill(gInFIFO, 0);
		Arrays.fill(gInFIFO2, 0);
		Arrays.fill(gOutFIFO, 0);		
		Arrays.fill(gOutputAccum, 0);
		
		// Analyze Formants of fftdata and fftmod
		// calculate peaks 750hz apart
		binsperformants = (int)(vocoderSize / freqPerBin);
		
		magnmap1 = new double[fftFrameSize2];
		magnmap2 = new double[fftFrameSize2];
		
		amagnmap1 = new double[fftFrameSize2];
		amagnmap2 = new double[fftFrameSize2];
		
		first100HZ = (int)(100.0 / sampleRate * fftFrameSize2);
		
	}
	
	private int binsperformants;
	
	private double[] magnmap1;
	private double[] magnmap2;
	private double[] amagnmap1;
	private double[] amagnmap2;
	
	private int first100HZ;
	
	private double[] runningsum = null;	
	public void processFormatScan(double[] magnmap, double[] outdata)
	{	
		int runner_len = binsperformants;	
		
		if(runningsum == null)
			runningsum = new double[runner_len];
		
		double[] runningsum = this.runningsum;
		Arrays.fill(runningsum, 0.0); 
		
		int runner = 0;	
		double totalsum = 0;
		for (int i = 0; i < fftFrameSize2; i++) {
			double m = magnmap[i];
			
			totalsum -= runningsum[runner];
			totalsum += m;
			runningsum[runner] = m;
			runner++;
			runner%=runner_len;
			
			outdata[i] = (totalsum / runner_len);	
		}
		
	}		
	
	public void processFFT(double[] fftdata, double[] fftmod)
	{	
		
		for (int i = 0; i < fftFrameSize2; i++) {
			double real = fftdata[2*i];
			double imag = fftdata[2*i+1];
			double real2 = fftmod[2*i];
			double imag2 = fftmod[2*i+1];					
			magnmap1[i] = real*real + imag*imag;
			magnmap2[i] = real2*real2 + imag2*imag2;			
		}
		
		processFormatScan(magnmap1, amagnmap1);
		processFormatScan(magnmap2, amagnmap2);
		
		
		for (int i = 0; i < fftFrameSize2; i++) {			
			double amp;
			
			if(amagnmap1[i] < 0.000001)
			{
				amp = 0;				
			}
			else
				amp = Math.sqrt(amagnmap2[i] / amagnmap1[i]);
			
			//amp = Math.sqrt(amagnmap2[i]);
			
			if(i < first100HZ) amp = 0;
			
			fftdata[2*i] = fftdata[2*i]*amp;
			fftdata[2*i+1] = fftdata[2*i+1]*amp;
		}		
		//Arrays.fill(gFFTworksp, fftFrameSize , 2*fftFrameSize , 0);
		
		
	}
	
	public void process(int start, int end, int interlace, double[] indata, double[] modulator, double[] outdata)
	{
		double window;
		int i,k;
		
		/* set up some handy variables */
		if (gRover == -1) gRover = inFifoLatency;
		
		/* main processing loop */	
		double[] gInFIFO = this.gInFIFO;
		double[] gOutFIFO = this.gOutFIFO;
		double[] gOutputAccum = this.gOutputAccum;
		double[] window_table = this.window_table;
		double[] gFFTworksp = this.gFFTworksp;
		double[] gFFTworksp2 = this.gFFTworksp2;
		
		for (i = start; i < end; i+= interlace){
			
			/* As long as we have not yet collected enough data just read in */
			gInFIFO[gRover] = indata[i];
			gInFIFO2[gRover] = modulator[i];
			outdata[i] = gOutFIFO[gRover-inFifoLatency];
			gRover++;
			
			/* now we have enough data for processing */
			if (gRover >= fftFrameSize) {
				gRover = inFifoLatency;
				
				/* do windowing and re,im interleave */
				for (k = 0; k < fftFrameSize;k++) {
					window = window_table[k];
					gFFTworksp[k] = (gInFIFO[k] * window);
					//gFFTworksp[2*k+1] = 0.;
					
					gFFTworksp2[k] = (gInFIFO2[k] * window);
					//gFFTworksp2[2*k+1] = 0.;					
				}
				
				fft.calcReal(gFFTworksp, -1);
				fft.calcReal(gFFTworksp2, -1);
				processFFT(gFFTworksp, gFFTworksp2);
				fft.calcReal(gFFTworksp, 1);
				
				/* do windowing and add to output accumulator */ 
				for(k=0; k < fftFrameSize; k++) {
					window = window_table[k];					
					gOutputAccum[k] += 2.*window*gFFTworksp[k]/(fftFrameSize2*osamp);
				}
				for (k = 0; k < stepSize; k++) gOutFIFO[k] = gOutputAccum[k];
				int shift_len = gOutputAccum.length / 2;
				for (int j = 0; j < shift_len; j++) {
					gOutputAccum[j] = gOutputAccum[j + stepSize]; 
				}
				for (k = 0; k < inFifoLatency; k++) gInFIFO[k] = gInFIFO[k+stepSize];
				for (k = 0; k < inFifoLatency; k++) gInFIFO2[k] = gInFIFO2[k+stepSize];
			}
		}
	}
	
	
	
}
