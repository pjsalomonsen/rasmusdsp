/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.util;

import java.util.Arrays;

import rasmus.interpreter.sampled.AudioStream;

public class WaveUtils {
	
	
	static public class ResampleStream implements AudioStream
	{
		double f_amount;
		AudioStream inputstream;
		int channels;
		double[] lastvalues;
		public ResampleStream(AudioStream input, double amount, int channels)
		{	
			this.channels = channels;
			lastvalues = new double[channels];
			
			inputstream = input;
			f_amount = amount;
		}
		double ix = 0;
		double[] stockbuffer = null;
		//double[] freqbuffer = null;
		//boolean finputstream_eof = false;
		
		public int mix(double[] buffer, int start, int end) {
			
			int skorun = 2;
			int sbuffsize = 2048;
			int xloopbuffsize = sbuffsize - skorun;
			int loopbuffsize = xloopbuffsize*channels;;
			int ret = 0;
			if(stockbuffer == null) 
			{
				stockbuffer = new double[sbuffsize * channels];
				ret = inputstream.replace(stockbuffer, 0, sbuffsize*channels);
				if(ret == -1)
					Arrays.fill(stockbuffer, channels, sbuffsize*channels, 0);
				else
					Arrays.fill(stockbuffer, ret, sbuffsize*channels,0 );
			}			
			
			int ixx = 0;
			for (int i = start; i < end; i+=channels) {		
				int j = ((int)(ix))*channels;
				double interp = ix % 1.0;						 
				for (int c = 0; c < channels; c++) {								
					buffer[i + c] += (stockbuffer[j] * (1 - interp)) + (stockbuffer[j + channels]*interp);
					j++;
				}
				ix+=f_amount; //ixstep;	
				ixx++;
				if(ix > xloopbuffsize)
				{
					ix -= xloopbuffsize;
					
					for (int c = 0; c < skorun*channels; c++) {								
						stockbuffer[c] = stockbuffer[loopbuffsize + c];					
					}
					ret = inputstream.replace(stockbuffer, channels*skorun, sbuffsize*channels);					
					if(ret == -1)
						Arrays.fill(stockbuffer, channels*skorun, 2048*channels, 0);
					else
						Arrays.fill(stockbuffer, channels*skorun + ret, 2048*channels,0 );
				}
			}
			
			if(ret == -1) return -1; else return end - start;			
		}
		public int replace(double[] buffer, int start, int end) {
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);
		}
		public int skip(int len)
		{
			double[] buffer = new double[len];
			return mix(buffer, 0,len);
		}
		public int isStatic(double[] buffer, int len) {
			return -1;
		}		
		public void close() {
			inputstream.close();
		}
		
	}	
	
	public static byte[] get16BitBytes(float[] sample) {
		
		byte[] bytesample = new byte[sample.length*2];
		/*
		 try
		 {
		 
		 FileOutputStream fo = new FileOutputStream("C:\\stream.log",true);
		 */
		for (int i = 0; i < sample.length; i++) {
			int l = i * 2;			
			// h�r er downsampla� fr� float yfir � short
			int value = ((int)(sample[i]));
			
			bytesample[l] = (byte) value;
			bytesample[l + 1] = (byte) (value >>> 8);
			
			//String str = sample[i] + " ==> " + value + " ==> " + bytesample[l] +" , " +  bytesample[l + 1] + "\r\n";
			//fo.write(str.getBytes()); 
		}
		/*fo.close();
		 }
		 catch(Exception e)
		 {
		 }*/
		
		
		return bytesample;
	}	
	
	final static public float interpolate(float[] wavetable, double location)
	{
		/* 5-point spline*/
		
		// code based on: http://musicdsp.org/archive.php?classid=5#60
		
		int nearest_sample = (int) location;
		double x = location - (float) nearest_sample;
		
		float p0;
		float p1;
		float p2=wavetable[nearest_sample];
		float p3;
		float p4;
		float p5;
		if(nearest_sample > 1)
		{
			p0=wavetable[(nearest_sample-2)];
			p1=wavetable[(nearest_sample-1)];
		}
		else
		{
			p0=p2;
			if(nearest_sample > 0)
			{
				p1=wavetable[(nearest_sample-1)];
			}
			else
				p1 = p2;
		}
		
		int size = wavetable.length;
		if(nearest_sample < (size-3))
		{
			p3=wavetable[(nearest_sample+1)];
			p4=wavetable[(nearest_sample+2)];
			p5=wavetable[(nearest_sample+3)];
		}
		else
		{
			p5 = wavetable[size-1];
			if(nearest_sample < (size-2))
			{
				p3=wavetable[(nearest_sample+1)];
				p4=wavetable[(nearest_sample+2)];
			}			
			else
			{
				p4 = p5;
				if(nearest_sample < (size-1))
				{
					p3=wavetable[(nearest_sample+1)];
				}
				else
				{
					p3 = p4;			
				}
			}
		}
		
		return (float)(p2 + 0.04166666666*x*((p3-p1)*16.0+(p0-p4)*2.0
				+ x *((p3+p1)*16.0-p0-p2*30.0- p4
						+ x *(p3*66.0-p2*70.0-p4*33.0+p1*39.0+ p5*7.0- p0*9.0
								+ x *( p2*126.0-p3*124.0+p4*61.0-p1*64.0- p5*12.0+p0*13.0
										+ x *((p3-p2)*50.0+(p1-p4)*25.0+(p5-p0)*5.0))))));
	};	
}
