/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.io;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Line;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioOutputInstance implements UnitInstancePart, Commitable {
	
	
	WaveFeeder wf = null;
	class WaveFeeder extends Thread
	{
		InputStream audiostream = null;
		Mixer mixer = null;
		SourceDataLine sourceDataLine = null;
		volatile boolean wisplaying = false;
		boolean useblocking = true;
		
		public WaveFeeder(Variable output)
		{
			if(priority != null)
			{
				double pri = DoublePart.asDouble(priority);
				if(pri > 0.5) setPriority(Thread.MAX_PRIORITY);
				else if(pri < -0.5) setPriority(Thread.MIN_PRIORITY);;
				
			}
			else
				setPriority(Thread.MAX_PRIORITY);
			
			if(blocking != null)
				useblocking = DoublePart.asDouble(blocking) > 0.5;
				
			
			AudioFormat format = getOutputFormat();
			samplerate = format.getSampleRate();
			channels = format.getChannels();
			bits = format.getSampleSizeInBits();
			framesize = channels * (bits / 8);

			double s_buflen = ((bufferlen==null)?0.1:DoublePart.asDouble(bufferlen));
			buffersize = framesize * ((int)(samplerate * s_buflen));
			buffersize_small = 500 * framesize;			
			buffersize_small -= buffersize_small % framesize;
			
			try
			{
				
				String audiooutputname = ObjectsPart.toString(devname); //ideframe.properties.getProperty("audio.output", "");
				if(audiooutputname != null) 
					if(audiooutputname.trim().length() == 0) audiooutputname = null;
				
				DataLine.Info dataLineInfo =
					new DataLine.Info(
							SourceDataLine.class, format, (int)format.getSampleRate()); //, 4096);
				
				Hashtable mixerinfotable = new Hashtable();
				if(audiooutputname != null)
				{
					Mixer.Info[] mixers = AudioSystem.getMixerInfo();					
					for (int i = 0; i < mixers.length; i++) {
						Mixer.Info mixerinfo = mixers[i];
						Mixer audmixer = AudioSystem.getMixer(mixerinfo);
						if(audmixer.isLineSupported(dataLineInfo))
						{
							
							boolean ok = false;
							Line.Info[] lineinfos = audmixer.getSourceLineInfo();
							for (int j = 0; j < lineinfos.length; j++) {
								if(lineinfos[j] instanceof DataLine.Info)
									if(lineinfos[j].getLineClass() == SourceDataLine.class)
									{
										ok = true;
									}
								
							}
							if(ok)
							{
								mixerinfotable.put(mixerinfo.getName(),  audmixer);
							}
						}
					}		
				}
				
				audiosession = new AudioSession(format.getSampleRate(), format.getChannels());
				audiosession.setRealTime(true);
				if(maxpolyphony != null)
				{
					int maxpoly = (int)DoublePart.asDouble(maxpolyphony);
					if(maxpoly > 0) audiosession.setMaxPolyphony(maxpoly);
				}	
				else
					audiosession.setMaxPolyphony(40); 
				audiostream = audiosession.asByteStream(output, format); //bits, true, false);
				if(audiostream == null) return;
				
				byte[] buffer = new byte[bits * channels * 2];
				audiostream.read(buffer);
				
				if(audiooutputname != null)
					mixer = (Mixer)mixerinfotable.get(audiooutputname);
				else
				{
					mixer = AudioSystem.getMixer(null); // Get Default Mixer
				}
				
				if(mixer == null)
				{					
					throw new Exception("Invalid name for line mixer.");
				}		
				mixer.open();
				sourceDataLine = (SourceDataLine) mixer.getLine(dataLineInfo);
				sourceDataLine.open(format, buffersize);
				sourceDataLine.start();
				//System.out.println(buffersize + " : " + sourceDataLine.getBufferSize());
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}			
			wisplaying = true;		
			
			
		}		
		
		double samplerate = 44100;
		int channels = 2;
		int bits = 16;
		int framesize = 4;
		int buffersize;
		int buffersize_small;
		
		AudioSession audiosession = null;
		
		public void run() 
		{			
				/*				
			if(useblocking)
			{
			    if(buffersize > buffersize_small) buffersize = buffersize_small;
			}
			else
			{*/
				if(buffersize > buffersize_small)
				{
					buffersize = buffersize_small;
					useblocking = true;
				}
			//}
			byte[] buffer = new byte[buffersize];			
			
			long bufferNanoTime = (long)(1000000000.0 * (((double)buffersize) / ((double)framesize)) / samplerate);
			long expiredtime = 0;
			
			try
			{
				
				while(true)
				{
					
					audiosession.commit();
					int ret = audiostream.read(buffer);					
					if(ret == -1)
					{
						sourceDataLine.drain();
						Thread.sleep(250);
						sourceDataLine.close();
						break;
					}
					if(!wisplaying)
					{
						sourceDataLine.close();
						break;
					}
					sourceDataLine.write(buffer,0,ret);
					
					if(!useblocking)
					{
					if(expiredtime<System.nanoTime())
						expiredtime = System.nanoTime() + bufferNanoTime;
                    else
						expiredtime += bufferNanoTime;
					
					long sleepTime = expiredtime - bufferNanoTime - System.nanoTime();
				    if(sleepTime > 0)
				    	Thread.sleep(sleepTime / 1000000, (int)(sleepTime % 1000000));
				    
					}

				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			wisplaying = false;
			sourceDataLine.stop();
			sourceDataLine.close();
			//if(mixer != null) mixer.close();
			try { audiostream.close(); } catch(Exception e) {}
			audiosession.close();
			audiosession = null;
			
			
		}
		
	};
	
	
	public AudioFormat getOutputFormat()
	{
		double samplerate = 44100; 
		if(rate != null)
		{
			samplerate = DoublePart.asDouble(rate);
		}
		if(samplerate < 1) samplerate = 44100;
		
		int nrofchannels = 2; 
		if(channels != null)
		{
			nrofchannels = (int)DoublePart.asDouble(channels);
		}
		
		if(nrofchannels == 0) nrofchannels = 2;
		
		int i_bits = 16;
		if(bits != null)
		{
			i_bits = (int)DoublePart.asDouble(bits);
		}
		
		return  new AudioFormat((float)samplerate,i_bits, nrofchannels, true, false);
	}
	
	Variable input;
	Variable devname;
	
	Variable maxpolyphony;
	Variable bits;
	Variable rate;
	Variable channels;
	Variable priority;
	Variable bufferlen;
	Variable blocking;
	private NameSpace namespace;
	
	public void start()
	{
		synchronized(this)
		{
			if(wf != null) stop();
			wf = new WaveFeeder(input);
			wf.start();
		}
	}
	public void stop()
	{
		
		synchronized(this)
		{
			
			if(wf != null)
			{
				wf.wisplaying = false;
				try {
					wf.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}				
				wf = null;
			}
		}
	}
	
	//boolean active = true;
	
	public AudioOutputInstance(Parameters parameters)
	{

		namespace = parameters.getNameSpace();
		input = parameters.getParameterWithDefault("input");
		devname = parameters.getParameterWithDefault(1, "devname");
		rate = parameters.getParameter(2, "rate");
		channels = parameters.getParameter(3, "channels");
		bits = parameters.getParameter(4, "bits");
		priority = parameters.getParameter(5, "priority");
		bufferlen = parameters.getParameter(6, "bufferlen");
		blocking = parameters.getParameter(7, "blocking");		
		maxpolyphony = parameters.getParameter(8, "maxpolyphony"); 
		
		namespace.addToCommitStack(this);
	}
		
	public void close() {
		
		namespace.addToCommitStack(new Commitable()
				{
			
				public int getRunLevel() {
					return RUNLEVEL_INIT;
				}			
				public void commit()
				{
					stop();
				}
			
				});
	}
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit() {
		start();
	}
	
}

public class AudioOutput implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Audio Output");
		Map map = metadata.add(1, "devname",		"Device name",			null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		
		List options = new ArrayList();
		map.put("options", options);		
		Mixer.Info[] mixers = AudioSystem.getMixerInfo();		
		for (int i = 0; i < mixers.length; i++) {
			Mixer.Info mixerinfo = mixers[i];
			Mixer audmixer = AudioSystem.getMixer(mixerinfo);
			{
				
				Line.Info[] lineinfos = audmixer.getSourceLineInfo();
				for (int j = 0; j < lineinfos.length; j++) {
					if(lineinfos[j] instanceof DataLine.Info)
					{
						if(lineinfos[j].getLineClass() == SourceDataLine.class)
						{
							options.add("\"" + mixerinfo.getName() + "\"");
							break;
						}
					}								
				}										
			}
		}	

		
		metadata.add(2, "rate",			"Sample rate",			"44100","Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "channels",		"Channels",				"2", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "bits",			"Sample length",		"16", "bit",  MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(5, "bufferlen",	"Buffer length",		"0.1", "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(6, "priority",		"Priority",				"1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(7, "blocking",		"Java Style Blocking",	"1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(8, "maxpolyphony",	"Max Voice Polyphony",	"40", "voices", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",				null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioOutputInstance(parameters);
	}
}
