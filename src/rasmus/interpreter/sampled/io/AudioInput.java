/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Line;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioInputInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	Variable devname;
	
	Variable answer = new Variable();
	
	public void calc()
	{
	}	
	public AudioInputInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		devname = parameters.getParameterWithDefault(1, "devname");
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		
		AudioCache audiocache;
		AudioSession session;
		TargetDataLine targatDataLine;
		Mixer input_mixer;
		byte[] byte_buffer;
		
		int preread;
		int overread;
		
		int buffersize;
		
		int readbuffersize = 3200;
		
		byte[] circbuffer;
		int recordpos = 0;
		int readpos = 0;
		
		boolean session_realtime;
		
		public FilterStreamInstance(AudioSession session)
		{				
			audiocache = session.getAudioCache();
			this.session = session;
			session_realtime = session.isRealTime();
			byte_buffer = new byte[2*session.getChannels() * ((int)session.getRate())]; // 1 sec buffer
			
			circbuffer = new byte[4*2*session.getChannels() * ((int)session.getRate())]; // 4 sec buffer
			
			buffersize = 2*session.getChannels()* ((int)(session.getRate() * 0.1)); // 10 msec buffer; 
			
			preread = 2*session.getChannels() * ((int)(session.getRate() * 0.01)); // 10 msec pre-read; 
			overread = 2*session.getChannels() * ((int)(session.getRate() * 0.1)); // 100 msec over-read; 
			
			try
			{				
				//String audioinputname = "Primary Sound Capture Driver";
				String audioinputname = ObjectsPart.toString(devname); //ideframe.properties.getProperty("audio.output", "");
				if(audioinputname != null) 
					if(audioinputname.trim().length() == 0) audioinputname = null;
				
				AudioFormat format = new AudioFormat((float)session.getRate(),16, session.getChannels(), true, false);
				
				DataLine.Info input_dataLineInfo =
					new DataLine.Info(
							TargetDataLine.class, format); //, 4096);
				
				Mixer.Info[] mixers = AudioSystem.getMixerInfo();		
				
				Hashtable capture_mixerinfotable = new Hashtable();
				for (int i = 0; i < mixers.length; i++) {
					Mixer.Info mixerinfo = mixers[i];
					Mixer audmixer = AudioSystem.getMixer(mixerinfo);
					
					boolean okrecord = false;				
					Line.Info[] lineinfos = audmixer.getTargetLineInfo();
					for (int j = 0; j < lineinfos.length; j++) {
						if(lineinfos[j] instanceof DataLine.Info)
							if(lineinfos[j].getLineClass() == TargetDataLine.class)
							{
								okrecord = true;
							}
						
					}
					if(okrecord)
					{									
						capture_mixerinfotable.put(mixerinfo.getName(), audmixer);
					}					
					
				}
				
				
				
				if(audioinputname == null)
					targatDataLine = (TargetDataLine) AudioSystem.getLine(input_dataLineInfo);
				else
				{
					input_mixer = (Mixer)capture_mixerinfotable.get(audioinputname);
					if(input_mixer == null) return;
					input_mixer.open();
					targatDataLine = (TargetDataLine) input_mixer.getLine(input_dataLineInfo);
				}
				
				
				
				targatDataLine.open(format, readbuffersize);			
				targatDataLine.start();
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		int readdiff = 0;
		boolean readstarted = false;
		public int mix(double[] buffer, int start, int end) {
			
			int buffersize = (end - start) * 2;
			if(byte_buffer.length < buffersize) byte_buffer = new byte[buffersize];
			
			int aval = targatDataLine.available();
			
			if(!readstarted)
			{
				if((buffersize+preread) < aval) readstarted = true;
			}
			else
			{
				if((buffersize) > aval) readstarted = false;			
			}
			
			if(!session_realtime) readstarted = true;
			
			if(readstarted)
			{
				if(aval == readbuffersize)
				{
					if(aval - (buffersize+preread) > 0 )
						targatDataLine.read(byte_buffer, 0, aval - (buffersize+preread));					
				}
				targatDataLine.read(byte_buffer, 0, buffersize);
				
				//WaveUtils.interpolate()
				
				int z = 0;			
				for (int k = start; k < end; k++) {
					buffer[k] += ((short) ((byte_buffer[z + 1] << 8) + (byte_buffer[z] & 0xFF))) / (32768.0f);
					z += 2;
				}			
				
			}
			
			return end - start;
		}
		
		
		public int replace(double[] buffer, int start, int end) {			
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);						
		}
		
		
		public int skip(int len) {		
			double[] buffer = audiocache.getBuffer(len);
			int ret = replace(new double[len], 0, len);
			audiocache.returnBuffer(buffer);
			return ret;
		}
		
		
		public int isStatic(double[] buffer, int len) {
			return -1;
		}		
		
		public void close() {
			if(targatDataLine != null) targatDataLine.close();
			if(input_mixer != null) input_mixer.close();
		}		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioInput implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Audio Output");
		metadata.add(-1, "output",		"Output",				null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		Map map = metadata.add(1, "devname",		"Device name",			null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		
		List options = new ArrayList();
		map.put("options", options);		
		Mixer.Info[] mixers = AudioSystem.getMixerInfo();		
		for (int i = 0; i < mixers.length; i++) {
			Mixer.Info mixerinfo = mixers[i];
			Mixer audmixer = AudioSystem.getMixer(mixerinfo);
			{
				Line.Info[] lineinfos = audmixer.getTargetLineInfo();
				for (int j = 0; j < lineinfos.length; j++) {
					if(lineinfos[j] instanceof DataLine.Info)
					{
						if(lineinfos[j].getLineClass() == TargetDataLine.class)
						{
							options.add("\"" + mixerinfo.getName() + "\"");
							break;
						}		
					}								
				}				
			}
		}	
		
		return metadata;
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioInputInstance(parameters);
	}
}