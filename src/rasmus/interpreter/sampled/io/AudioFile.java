/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.io;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.spi.AudioFileReader;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.io.Resource;
import rasmus.interpreter.io.ResourceManager;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.sampled.util.WaveUtils;
import rasmus.interpreter.status.GlobalStatus;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;
import rasmus.util.NonBlockingReadableByteChannel;
import rasmus.util.RasmusUtil;

class AudioFileStream implements AudioStream
{	
	
	AudioInputStream audiostream;
	
	float file_framrate;
	int file_channels;
	float samplesize;
	
	boolean float_format = false;
	boolean big_endian = true; 
	
	AudioFormat format;
	
	int samplebits;
	
	double rate;
	int channels; 
	//int type;
	
	InputStream fis = null;
	public void close() 
	{
		
		if(audiostream != null)
		{
			try
			{
				audiostream.close();
				
			}
			catch(Exception e)
			{
			}
			audiostream = null;
		}
		
		if(fis != null)
		{
			try
			{
				fis.close();
			}
			catch(Exception e)
			{
			}		
			fis = null;
		}
		/*
		 if(randomfile != null)
		 {
		 try
		 {
		 randomfile.close();
		 }
		 catch(Exception e)
		 {				
		 }
		 }*/
		
	}
	
	/*	
	 public static double getSampleRate(File file)
	 {
	 AudioInputStream audiostream;
	 try {
	 //audiostream = AudioSystem.getAudioInputStream(file);
	  AudioFormat readformat = getFormat(file);
	  double srate = readformat.getSampleRate();
	  //audiostream.close();
	   return srate;
	   } catch (Exception e) {
	   e.printStackTrace();
	   }
	   return 0;
	   
	   } */
	
	public static AudioFormat getFormat(File file)
	{
		
		
		try {
			AudioFormat readformat = AudioSystem.getAudioFileFormat(file).getFormat();
			return readformat;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*
		 try {
		 AudioFormat readformat = null;
		 AudioInputStream audiostream = AudioSystem.getAudioInputStream(file);
		 try
		 {			
		 readformat = audiostream.getFormat();
		 }
		 finally
		 {
		 audiostream.close();
		 }
		 return readformat;
		 } catch (Exception e) {
		 e.printStackTrace();
		 }
		 
		 
		 try {
		 
		 RandomFileInputStream fi = new RandomFileInputStream(file);
		 AudioFormat readformat ;
		 try
		 {
		 AudioInputStream audiostream = AudioSystem.getAudioInputStream(fi);
		 try
		 {
		 readformat = audiostream.getFormat(); 
		 }
		 finally
		 {
		 audiostream.close();
		 }
		 }
		 finally
		 {
		 fi.close();
		 }
		 return readformat;			
		 } catch (Exception e) {
		 e.printStackTrace();
		 }				
		 */
		
		return null;
		
	}	
	
	long loopend = 0;
	long loopstart = 0;
	long offset = 0;
	AudioFormat iformat;
	File file;
	
	public void restartLoop()
	{		
		if(datacache != null)
		{   
			try
			{			
				audiostream.reset();
				audiostream.skip(loopstart); 
				return;
			}
			catch(Exception e)
			{
			}
		    return; 
		}
		
		
		try
		{
			close();
			fis = Channels.newInputStream(new NonBlockingReadableByteChannel(afi.channel_loop));
			audiostream = new AudioInputStream(fis, iformat, (loopend - loopstart) / iformat.getFrameSize());
			initStream();
			return;
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	AudioFileInstance afi;
	byte[] datacache = null;
	byte[] loopcache = null;
	
	public AudioFileStream(AudioFileInstance afi, File file, double rate, int channels, /*int type,*/ long offset, long length, long loopstart, long loopend, AudioFormat format) throws Exception
	{
		
		this.loopcache = afi.loopcache;
		this.afi = afi;
		this.rate = rate;
		this.channels = channels; 
		//this.type = type;
		this.offset = offset;
		this.file = file;
		this.iformat = format;
		this.loopend = loopend;
		this.loopstart = loopstart;
		this.datacache = afi.datacache;	
		
		//loopstart = 0;
		//loopend = 0;
		
		//this.loopstart = 0;
		//this.loopend = 0;
		
		if(loopend != 0) length = loopend;			
		
		if(datacache != null)
			fis = new ByteArrayInputStream(datacache);
		else
			fis = Channels.newInputStream(new NonBlockingReadableByteChannel(afi.channel));
		
		/*
		if(datacache != null)
			fis = new ByteArrayInputStream(datacache);
		else
		{
			if(afi.partialcache == null) afi.partialcache = new byte[0];
			fis = new PreBufferedFileInputStream(file, offset, afi.partialcache);			
		}*/
		
		if(format != null && length != 0)
		{
			audiostream = new AudioInputStream(fis, format, length / format.getFrameSize());
		}
		else
		{			
			try
			{
				if(afi.audioreader != null)
					audiostream = afi.audioreader.getAudioInputStream(fis);
				else
					audiostream = AudioSystem.getAudioInputStream(fis);
			}
			catch(Exception e)
			{
				// Filaed loading stream thru PreBufferedFileInputStream, try alternative
				//e.printStackTrace();
				audiostream = AudioSystem.getAudioInputStream(file);
			}
		}
		initStream();
		
	}
	public void initStream()
	{
		
		AudioFormat readformat = audiostream.getFormat();
		
		/*
		 *  SPECIAL HANLDING ULAW ENCODING 
		 * 
		 */
		if (
				(readformat.getEncoding() ==
					AudioFormat.Encoding.ULAW)
					||(readformat.getEncoding() ==
						AudioFormat.Encoding.ALAW)
						|| (readformat.getSampleSizeInBits() == -1)
		) 
		{
			
			if(readformat.getSampleSizeInBits() == -1)
				samplebits = 16;
			else
				samplebits = readformat.getSampleSizeInBits() * 2;
			
			float framerate = readformat.getFrameRate();
			int framesize = readformat.getFrameSize();
			
			if(framesize == -1)
			{
				framesize = readformat.getChannels() * 2;
				framerate = readformat.getSampleRate();
			}
			else
			{
				framesize *= 2;
			}
			
			AudioFormat newFormat = new AudioFormat
			(AudioFormat.Encoding.PCM_SIGNED,
					readformat.getSampleRate(),
					samplebits,
					readformat.getChannels(),
					framesize,
					framerate,
					false);
			
			audiostream =AudioSystem
			.getAudioInputStream(newFormat, audiostream);
			
			format = newFormat;
		}
		else
		{
			
			
			/*
			 *   CONVERTING TO OUTPUT FORMAT
			 */
			
			file_framrate = readformat.getFrameRate();
			file_channels = readformat.getChannels();
			samplebits = readformat.getSampleSizeInBits();
			samplesize = readformat.getSampleRate();
			big_endian = readformat.isBigEndian();
			
			if(readformat.getEncoding() == AudioFormat.Encoding.PCM_SIGNED)
				if(readformat.isBigEndian() == false)
					return;
			
			if(readformat.getEncoding().toString().equals("PCM_FLOAT"))
			{
				float_format = true;
				return;
			}
			
			
			format =
				new AudioFormat(
						AudioFormat.Encoding.PCM_SIGNED,
						file_framrate,
						samplebits,
						file_channels, 
						readformat.getFrameSize(),
						readformat.getFrameRate(),
						false);		
			
			audiostream = AudioSystem.getAudioInputStream(format, audiostream);
		}
		
		file_framrate = format.getFrameRate();
		file_channels = format.getChannels();
		samplebits = format.getSampleSizeInBits();
		samplesize = format.getSampleRate();		
		
		/*
		 format =
		 new AudioFormat(
		 file_framrate,
		 samplebits,
		 file_channels, 
		 true,
		 false); */
		
		
		/* readchannel = 0;
		 if(readchannel >= file_channels) mono = true; */
		
		
	}
	/*
	 int readchannel = 0;
	 boolean mono = true; */
	/*
	 public long available() throws Exception
	 {
	 if(samplebits == 16)
	 return (audiostream.available()/2)/file_channels;
	 if(samplebits == 8)
	 return audiostream.available()/file_channels;
	 
	 throw new Exception("Invalid samplesize, only 16 and 8 allowed, samplebits=" + samplebits);
	 }*/
	
	public int isStatic(double[] buffer, int len) {
		return -1;
	}				
	
	
	byte[] bytesample = null;
	private byte[] getByteBuffer(int len)
	{
		if(bytesample != null)
		{
			if(len > bytesample.length) bytesample = null;
		}
		if(bytesample == null)
		{
			bytesample = new byte[len];
		}
		return bytesample;
		
	}
	
	ByteBuffer bytebuffer = null;
	FloatBuffer floatbuffer = null;
	float[] fbuffer = null;
	DoubleBuffer doublebuffer = null;
	double[] dbuffer = null;
	
	public int mix(double[] buffer, int start, int end)
	{		
		if(eof) return -1;
		int length = (end - start) / channels;
		try
		{
			int i_offset = (int)start;
			//long avail = available();
			//if(avail == 0) return -1;
			//if(length > avail) length = (int)avail;
			
			if(float_format)
			{
				if (samplebits == 32) {
									
				int size = (int)length;

				int dsize = size*4*file_channels;
				if(bytebuffer == null || bytebuffer.capacity() < dsize)
				{
					
					bytebuffer = ByteBuffer.allocate(dsize);
					if(!big_endian) bytebuffer = bytebuffer.order(ByteOrder.LITTLE_ENDIAN);
					
					floatbuffer = bytebuffer.asFloatBuffer();
					fbuffer = new float[file_channels*size];
				}
				bytebuffer.position(0);
				int ret = read(bytebuffer.array(), size*4*file_channels);
				if(ret == -1) return -1;
				size = ret / (4*file_channels);						
				int bsize = size;
				
				floatbuffer.position(0);
				floatbuffer.get(fbuffer); // Convert to floating point
				
				int z = 0;			
				
				if(file_channels == channels)
				{
					bsize *= file_channels;
					for (int i = 0; i < bsize; i++) {
						buffer[i_offset] +=  fbuffer[z];
						z ++;
						i_offset++;
					}				
				}
				else
				{
					for (int i = 0; i < bsize; i++) {
						for (int c = 0; c < file_channels; c++) {
							if(c < channels)
							{
								buffer[i_offset+c] +=  fbuffer[z];								
							}
							z ++;
						}
						i_offset+=channels;
					}
				}						
				
				}
				else
				if (samplebits == 64) 
				{

					int size = (int)length;

					int dsize = size*8*file_channels;
					if(bytebuffer == null || bytebuffer.capacity() < dsize)
					{
						
						bytebuffer = ByteBuffer.allocate(dsize);
						if(!big_endian) bytebuffer = bytebuffer.order(ByteOrder.LITTLE_ENDIAN);
						
						doublebuffer = bytebuffer.asDoubleBuffer();
						dbuffer = new double[file_channels*size];
					}
					bytebuffer.position(0);
					int ret = read(bytebuffer.array(), size*8*file_channels);
					if(ret == -1) return -1;
					size = ret / (8*file_channels);						
					int bsize = size;
					
					doublebuffer.position(0);
					doublebuffer.get(dbuffer); // Convert to floating point
					
					int z = 0;			
					
					if(file_channels == channels)
					{
						bsize *= file_channels;
						for (int i = 0; i < bsize; i++) {
							buffer[i_offset] +=  dbuffer[z];
							z ++;
							i_offset++;
						}				
					}
					else
					{
						for (int i = 0; i < bsize; i++) {
							for (int c = 0; c < file_channels; c++) {
								if(c < channels)
								{
									buffer[i_offset+c] +=  dbuffer[z];								
								}
								z ++;
							}
							i_offset+=channels;
						}
					}									
					
				}
			}
			else			
			if (samplebits == 32) {
				
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*4*file_channels);				
				int ret = read(bytesample, size*4*file_channels);
				if(ret == -1) return -1;
				size = ret / (4*file_channels);						
				int bsize = size;
				int z = 0;				
				if(file_channels == channels)
				{
					bsize *= file_channels;
					for (int i = 0; i < bsize; i++) {
						buffer[i_offset] +=  ( ((((bytesample[z + 3] & 0xFF ) << 24) | (bytesample[z + 2] & 0xFF) << 16) | ((bytesample[z + 1] & 0xFF) << 8) + (bytesample[z] & 0xFF))) / (2147483648.0f);
						z += 4;
						i_offset++;
					}				
				}
				else
				{
					for (int i = 0; i < bsize; i++) {
						for (int c = 0; c < file_channels; c++) {
							if(c < channels)
							{
								buffer[i_offset+c] +=  ( ((( (bytesample[z + 3] ) << 24) | (bytesample[z + 2] & 0xFF) << 16) | ((bytesample[z + 1] & 0xFF) << 8) | (bytesample[z] & 0xFF))) / (2147483648.0f);								
							}
							z += 4;
						}
						i_offset+=channels;
					}
				}
								
				
			}			
			else			
			if (samplebits == 24 || samplebits == 20) {
				
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*3*file_channels);				
				int ret = read(bytesample, size*3*file_channels);
				if(ret == -1) return -1;
				size = ret / (3*file_channels);						
				int bsize = size;
				int z = 0;				
				if(file_channels == channels)
				{
					bsize *= file_channels;
					for (int i = 0; i < bsize; i++) {
						buffer[i_offset] +=  ( (((bytesample[z + 2] & 0xFF) << 16) + ((bytesample[z + 1] & 0xFF) << 8) + (bytesample[z] & 0xFF))) / (8388608.0f);
						if(buffer[i_offset] > 1.0) buffer[i_offset] = buffer[i_offset] - 2.0;
						z += 3;
						i_offset++;
					}				
				}
				else
				{
					for (int i = 0; i < bsize; i++) {
						for (int c = 0; c < file_channels; c++) {
							if(c < channels)
							{
								buffer[i_offset+c] +=  ( (((bytesample[z + 2] & 0xFF) << 16) + ((bytesample[z + 1] & 0xFF) << 8) + (bytesample[z] & 0xFF))) / (8388608.0f);
								if(buffer[i_offset+c] > 1.0) buffer[i_offset+c] = buffer[i_offset+c] - 2.0;								
							}
							z += 3;
						}
						i_offset+=channels;
					}
				}
										
				
			}			
			else			
			if (samplebits == 16) {
				
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*2*file_channels);
				//int ret = audiostream.read(bytesample);
				
				int ret = read(bytesample, size*2*file_channels);
				if(ret == -1) return -1;
				size = ret / (2*file_channels);
				
				int bsize = size;/// (file_channels);
				int z = 0;
				//sample = new float[file_channels][bsize];
				/*
				 if(mono)
				 for (int i = 0; i < bsize; i++) {
				 float sample = 0;
				 for (int c = 0; c < file_channels; c++) {
				 sample +=  
				 (short) ((bytesample[z + 1] << 8)
				 + (bytesample[z] & 0xFF));
				 z += 2;;
				 }
				 sample = sample / file_channels;
				 buffer[i_offset] += sample / (32768.0f);
				 i_offset+=channels;
				 }
				 else*/
				
				if(file_channels == channels)
				{
					bsize *= file_channels;
					for (int i = 0; i < bsize; i++) {
						
						buffer[i_offset] +=  ((short) ((bytesample[z + 1] << 8) + (bytesample[z] & 0xFF))) / (32768.0f);													
						z += 2;
						i_offset++;
					}				
				}
				else
				{
					for (int i = 0; i < bsize; i++) {
						for (int c = 0; c < file_channels; c++) {
							if(c < channels)
							{
								buffer[i_offset+c] +=  ((short) ((bytesample[z + 1] << 8) + (bytesample[z] & 0xFF))) / (32768.0f);													
							}
							z += 2;
						}
						i_offset+=channels;
					}
				}
				
				
				
			}
			else
			if (samplebits == 8) {
				
				int size = (int)length;
				byte[] bytesample = new byte[size*file_channels];
				
				int ret = audiostream.read(bytesample);
				if(ret == -1)
				{
					eof = true; return -1;
				}
				size = ret / (file_channels);			
				
				int bsize = size ;
				int z = 0;
				/*
				 if(mono)
				 for (int i = 0; i < bsize; i++) {
				 float sample = 0;
				 for (int c = 0; c < file_channels; c++) {
				 sample += 
				 ((short) (bytesample[z]) );
				 //( (bytesample[z] & 0xFF));
				  z += 1;
				  }
				  sample = sample / file_channels;
				  buffer[i_offset] += sample / (128.0f);
				  i_offset+=channels;
				  }
				  else*/
				for (int i = 0; i < bsize; i++) {
					for (int c = 0; c < file_channels; c++) {
						if(c < channels)
						{
							buffer[i_offset+c] += 
								((short) (bytesample[z])) / (128.0f);
							//( (bytesample[z] & 0xFF));
						}
						z +=1;
					}
					i_offset+=channels;
				}				
				
			}
			return i_offset - start;
			//return length * channels;
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			return -1;
		}
	}
	
	
	/* Er afrit af ofan nema � sta� buffer[i_offset+c] += er gert buffer[i_offset+c] = */
	
	boolean eof = false;
	
	byte[] externalbuffer = null;
	int externalbufferloc = -1;
	int externalbuffersize = -1;
	int externalbufferleft = 0;
	
	public int read(byte[] bytesample, int len) throws IOException
	{
		if(eof) return -1;
		
		int ret = 0;
		
		if(externalbuffer == null)
		{
			ret = audiostream.read(bytesample, 0, len);
			if(ret == -1)
			{
				if(loopend != 0)
				{
					restartLoop();		
					ret = audiostream.read(bytesample, 0, len);
				}
				else
				{
					eof = true; return -1;
				}
			}
		}
		
		
		
		if(ret != len)
		{
			int leftover = len - ret; 
			if(externalbuffer == null) externalbuffer = new byte[4096*file_channels];
			int ix = ret;
			
			while(!eof && leftover != 0)
			{
				if(externalbufferleft == 0)
				{
					externalbuffersize = audiostream.read(externalbuffer);
					if(externalbuffersize == -1)
					{
						if(loopend != 0)
						{
							restartLoop();		
							externalbuffersize = audiostream.read(externalbuffer);
						}
						else
						{
							eof = true;
							return ret;
						}
					}
					externalbufferloc = 0;
					externalbufferleft = externalbuffersize;
				}
				
				if(externalbufferleft > leftover)
				{
					for (int i = 0; i < leftover; i++) {
						bytesample[ix] = externalbuffer[externalbufferloc];
						ix++;
						externalbufferloc++;
					}			   	
					ret += leftover;
					externalbufferleft -= leftover;
					leftover = 0;
				}
				else
				{
					
					for (int i = 0; i < externalbufferleft; i++) {
						bytesample[ix] = externalbuffer[externalbufferloc];
						ix++;
						externalbufferloc++;
					}
					ret += externalbufferleft;
					leftover -= externalbufferleft;
					externalbufferleft = 0;
				}
				
				
			}
			
			
		}
		
		return ret;		
	}
	
	public int replace(double[] buffer, int start, int end)
	{		
		
		if(eof) return -1;
		int length = (end - start) / channels;
		try
		{
			int i_offset = (int)start;
			//long avail = available();
			//if(avail == 0) return -1;
			//if(length > avail) length = (int)avail;
			
			if(float_format)
			{
				if (samplebits == 32) {
									
				int size = (int)length;

				int dsize = size*4*file_channels;
				if(bytebuffer == null || bytebuffer.capacity() < dsize)
				{
					bytebuffer = ByteBuffer.allocate(dsize);
					if(!big_endian) bytebuffer = bytebuffer.order(ByteOrder.LITTLE_ENDIAN);
					floatbuffer = bytebuffer.asFloatBuffer();					
					fbuffer = new float[size*file_channels];
				}
				bytebuffer.position(0);
				int ret = read(bytebuffer.array(), size*4*file_channels);
				if(ret == -1) return -1;
				size = ret / (4*file_channels);						
				int bsize = size;
				
				floatbuffer.position(0);
				floatbuffer.get(fbuffer); // Convert to floating point
				
				int z = 0;			
				
				if(file_channels == channels)
				{
					bsize *= file_channels;
					for (int i = 0; i < bsize; i++) {
						buffer[i_offset] =  fbuffer[z];
						z ++;
						i_offset++;
					}				
				}
				else
				{
					for (int i = 0; i < bsize; i++) {
						for (int c = 0; c < file_channels; c++) {
							if(c < channels)
							{
								buffer[i_offset+c] =  fbuffer[z];								
							}
							z ++;
						}
						i_offset+=channels;
					}
				}			
				
				}
				else
				if (samplebits == 64) {
					
					int size = (int)length;

					int dsize = size*8*file_channels;
					if(bytebuffer == null || bytebuffer.capacity() < dsize)
					{
						bytebuffer = ByteBuffer.allocate(dsize);
						if(!big_endian) bytebuffer = bytebuffer.order(ByteOrder.LITTLE_ENDIAN);
						doublebuffer = bytebuffer.asDoubleBuffer();					
						dbuffer = new double[size*file_channels];
					}
					bytebuffer.position(0);
					int ret = read(bytebuffer.array(), size*8*file_channels);
					if(ret == -1) return -1;
					size = ret / (8*file_channels);						
					int bsize = size;
					
					doublebuffer.position(0);
					doublebuffer.get(dbuffer); // Convert to floating point
					
					int z = 0;			
					
					if(file_channels == channels)
					{
						bsize *= file_channels;
						for (int i = 0; i < bsize; i++) {
							buffer[i_offset] =  dbuffer[z];
							z ++;
							i_offset++;
						}				
					}
					else
					{
						for (int i = 0; i < bsize; i++) {
							for (int c = 0; c < file_channels; c++) {
								if(c < channels)
								{
									buffer[i_offset+c] =  dbuffer[z];								
								}
								z ++;
							}
							i_offset+=channels;
						}
					}							
					
				}
			}
			else
			if (samplebits == 32) {
				
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*4*file_channels);				
				int ret = read(bytesample, size*4*file_channels);
				if(ret == -1) return -1;
				size = ret / (4*file_channels);						
				int bsize = size;
				int z = 0;				
				if(file_channels == channels)
				{
					bsize *= file_channels;
					for (int i = 0; i < bsize; i++) {
						buffer[i_offset] =  ( ((((bytesample[z + 3] & 0xFF ) << 24) | (bytesample[z + 2] & 0xFF) << 16) | ((bytesample[z + 1] & 0xFF) << 8) + (bytesample[z] & 0xFF))) / (2147483648.0f);
						z += 4;
						i_offset++;
					}				
				}
				else
				{
					for (int i = 0; i < bsize; i++) {
						for (int c = 0; c < file_channels; c++) {
							if(c < channels)
							{
								buffer[i_offset+c] =  ( ((( (bytesample[z + 3] ) << 24) | (bytesample[z + 2] & 0xFF) << 16) | ((bytesample[z + 1] & 0xFF) << 8) | (bytesample[z] & 0xFF))) / (2147483648.0f);								
							}
							z += 4;
						}
						i_offset+=channels;
					}
				}
								
				
			}			
			else
			if (samplebits == 24 || samplebits == 20) {
				
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*3*file_channels);				
				int ret = read(bytesample, size*3*file_channels);
				if(ret == -1) return -1;
				size = ret / (3*file_channels);						
				int bsize = size;
				int z = 0;				
				if(file_channels == channels)
				{
					bsize *= file_channels;
					for (int i = 0; i < bsize; i++) {
						buffer[i_offset] =  ( (((bytesample[z + 2] & 0xFF) << 16) + ((bytesample[z + 1] & 0xFF) << 8) + (bytesample[z] & 0xFF))) / (8388608.0f);
						if(buffer[i_offset] > 1.0) buffer[i_offset] = buffer[i_offset] - 2.0;
						z += 3;
						i_offset++;
					}				
				}
				else
				{
					for (int i = 0; i < bsize; i++) {
						for (int c = 0; c < file_channels; c++) {
							if(c < channels)
							{
								buffer[i_offset+c] =  ( (((bytesample[z + 2] & 0xFF) << 16) + ((bytesample[z + 1] & 0xFF) << 8) + (bytesample[z] & 0xFF))) / (8388608.0f);
								if(buffer[i_offset+c] > 1.0) buffer[i_offset+c] = buffer[i_offset+c] - 2.0;								
							}
							z += 3;
						}
						i_offset+=channels;
					}
				}
								
				
			}		
			else
			if (samplebits == 16) {
				
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*2*file_channels);				
				int ret = read(bytesample, size*2*file_channels);
				if(ret == -1) return -1;
				size = ret / (2*file_channels);						
				int bsize = size;
				int z = 0;				
				if(file_channels == channels)
				{
					bsize *= file_channels;
					for (int i = 0; i < bsize; i++) {
						
						buffer[i_offset] =  ((short) ((bytesample[z + 1] << 8) + (bytesample[z] & 0xFF))) / (32768.0f);													
						z += 2;
						i_offset++;
					}				
				}
				else
				{
					for (int i = 0; i < bsize; i++) {
						for (int c = 0; c < file_channels; c++) {
							if(c < channels)
							{
								buffer[i_offset+c] =  ((short) ((bytesample[z + 1] << 8) + (bytesample[z] & 0xFF))) / (32768.0f);													
							}
							z += 2;
						}
						i_offset+=channels;
					}
				}
								
				
			}
			else
			if (samplebits == 8) {
				
				int size = (int)length;
				byte[] bytesample = new byte[size*file_channels];
				int ret = audiostream.read(bytesample);
				if(ret == -1)
				{
					eof = true; return -1;
				}
				size = ret / (file_channels);
				
				int bsize = size ;
				int z = 0;
				/*
				 if(mono)
				 for (int i = 0; i < bsize; i++) {
				 float sample = 0;
				 for (int c = 0; c < file_channels; c++) {
				 sample += 
				 ((short) (bytesample[z]) );
				 //( (bytesample[z] & 0xFF));
				  z += 1;
				  }
				  sample = sample / file_channels;
				  buffer[i_offset] += sample / (128.0f);
				  i_offset+=channels;
				  }
				  else*/
				for (int i = 0; i < bsize; i++) {
					for (int c = 0; c < file_channels; c++) {
						if(c < channels)
						{
							buffer[i_offset+c] = 
								((short) (bytesample[z])) / (128.0f);
							//( (bytesample[z] & 0xFF));
						}
						z +=1;
					}
					i_offset+=channels;
				}				
				
			}
			
			//System.out.println("i_offset = " + i_offset);
			//System.out.println("ret = " + length * channels);
			//return length * channels;
			
			return i_offset - start;
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			return -1;
		}
	}	
	
	public int skip(int len)
	{		
		if(eof) return -1;
		int length = (len) / channels;
		try
		{			
			if (samplebits == 32) {
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*4*file_channels);
				/*int ret =*/ read(bytesample, size*4*file_channels);
			}						
			if (samplebits == 24 || samplebits == 20) {
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*3*file_channels);
				/*int ret =*/ read(bytesample, size*3*file_channels);
			}						
			if (samplebits == 16) {
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*2*file_channels);
				/*int ret =*/ read(bytesample, size*2*file_channels);
			}			
			if (samplebits == 8) {				
				int size = (int)length;
				byte[] bytesample = getByteBuffer(size*file_channels);
				/*int ret =*/ read(bytesample, size*file_channels);
			}
			
			return length * channels;
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			return -1;
		}
	}	
	
	/*
	 public int replace(float[] buffer, int start, int end) {
	 for (int i = start; i < end; i++) {
	 buffer[i] = 0;
	 }
	 return mix(buffer, start, end);
	 }
	 */
	
}




class AudioFileWriteStream implements AudioStream
{
	
	AudioStream peerstream;
	AudioSession session;
	AudioCache cache;
	
	OutputStream pcm_outputstream;
	File file;
	AudioInputStream audio_inputstream;
	AudioFileFormat.Type file_type = null;
	
	public AudioFileWriteStream(AudioFileInstance afi, AudioStream peerstream, AudioSession session) throws IOException
	{		
		this.session = session;
		cache = session.getAudioCache();
		this.peerstream = peerstream;
		
		file = afi.file;
		
		
		//pcm_outputstream = new WAVEAudioOutputWriter(file, audioFormat);
		
		PipedOutputStream pcm_outputstream = new PipedOutputStream();
		this.pcm_outputstream = pcm_outputstream;
		PipedInputStream pipe_inputstream = new PipedInputStream(pcm_outputstream);
		
		int channels = session.getChannels();
		
		AudioFormat audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
				(float)AudioFileWriteStream.this.session.getRate(), 16, channels, 2*channels, (float)AudioFileWriteStream.this.session.getRate(), false);
		audio_inputstream = new AudioInputStream(pipe_inputstream, audioFormat, AudioSystem.NOT_SPECIFIED);
		
		
		AudioFileFormat.Type[] types = AudioSystem.getAudioFileTypes(audio_inputstream);
		for (int i = 0; i < types.length; i++) {
			if(file.getName().toLowerCase().endsWith("." + types[i].getExtension().toLowerCase()))
			{
				file_type = types[i];
				break;
			}			  
		}
		
		if(file_type == null)
		{
			pcm_outputstream = null;
			return;
		}
		
		new Thread()
		{
			public void run()
			{
				try
				{
					AudioSystem.write(audio_inputstream, file_type, file);
				}
				catch(Exception e)
				{
					e.printStackTrace();
					AudioFileWriteStream.this.pcm_outputstream = null;
				}
			}
		}.start();
		
		
		//new AudioInputStream()
		
	}
	
	public int mix(double[] buffer, int start, int end) {
		double[] readbuffer = cache.getBuffer(end);
		int ret = replace(readbuffer, start, end);
		if(ret != -1)
		{
			for (int i = start; i < start + ret; i++) {
				buffer[i] += readbuffer[i];
			}
		}
		cache.returnBuffer(readbuffer);
		return 0;
	}
	
	byte[] data = null;
	public int replace(double[] buffer, int start, int end) {
		
		int ret = peerstream.replace(buffer, start, end);
		int clen = (end - start)* 2;
		if(data == null) 
			data = new byte[clen];
		else
			if(data.length < clen) data = new byte[clen];
		
		if(ret == -1) return -1;
		
		end = start + ret;
		int j = 0;
		for (int i = start; i < end; i++) {
			double b = buffer[i] ;			
			int valueL = ((int)(b * (32768.0f)));
			data[j++] = (byte) valueL;
			data[j++] = (byte) (valueL >>> 8);					
		}
		
		try
		{
			OutputStream pcm_outputstream = this.pcm_outputstream;
			if(pcm_outputstream != null)
				pcm_outputstream.write(data);
		}
		catch(Exception e)
		{			
		}
		
		return ret;
	}
	
	public int isStatic(double[] buffer, int len) {
		return -1;
	}
	
	public int skip(int len) {
		return peerstream.skip(len);
	}
	
	public void close() {
		peerstream.close();
		try
		{
			pcm_outputstream.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
}

class AudioFileInstance extends UnitInstanceAdapter implements Commitable
{
	public final static int FILE_BUFFER_SIZE = 8192; // 8192; // 2048; // 16384 //65000; 
	
	public byte[] loopcache;
	boolean noresample = false;
	Variable returnvar;
	Variable inputvar;
	Variable filename;
	Variable wrkdir;
	
	Variable offset = null;
	Variable length = null;
	Variable format = null;
	
	Variable loopstart = null;	
	Variable loopend = null;			
	
	Variable answer;
	Variable streammode;
	/*
	 List answers = new ArrayList();
	 
	 public void clear()
	 {
	 if(answers.size() != 0)
	 {
	 Iterator iterator = answers.iterator();
	 while (iterator.hasNext()) {
	 RVariable answer = (RVariable) iterator.next();
	 returnvar.remove(answer);
	 }			
	 answers.clear();
	 }		
	 }*/
	AudioFormat audioformat;
	
	byte[] datacache = null;
	
	File file;
	//FileChannel fc = null;
	NonBlockingReadableByteChannel channel = null;
	NonBlockingReadableByteChannel channel_loop = null;
	
	class AudioFileStreamable implements AudioStreamable
	{
		//File file;
		
		
		//boolean noresample;
		
		
		
		
		public AudioStream openStream(AudioSession session) {
			
			
			//if(session.type == AudioSession.REALTIME_STREAM) return null;
			try
			{
				
				if(s_filename == null) return null;
				
				if(inputvar != null)
				{
					AudioStream audiostream = session.openStream(inputvar);
					try {
						audiostream = new AudioFileWriteStream(AudioFileInstance.this, audiostream, session);
					} catch (IOException e) {
						e.printStackTrace();
					}				
					return audiostream;
				}
				
				/*
				 Object[] s_filenames = RObjects.asObjects(filename);
				 if(s_filenames.length == 0) return null;
				 String s_filename = s_filenames[0].toString();
				 if(s_filename.toLowerCase().endsWith(".mid")) return null;
				 File file = new File(s_filename);
				 if(!file.exists()) return null; */
				AudioFormat audioformat = getFormat(file);
				if(audioformat == null) return null;			    			    				
				double samplerate = audioformat.getSampleRate(); // AudioFileStream.getSampleRate(file);
				
				if( noresample || (Math.abs(samplerate - session.getRate()) < 0.001) )
				{
					return new AudioFileStream(AudioFileInstance.this, file, session.getRate(), session.getChannels(), (long)DoublePart.asDouble(offset), (long)DoublePart.asDouble(length), (long)DoublePart.asDouble(loopstart), (long)DoublePart.asDouble(loopend), audioformat);					
				}
				else
				{
					return new WaveUtils.ResampleStream(new AudioFileStream(AudioFileInstance.this, file, session.getRate(), session.getChannels(), (long)DoublePart.asDouble(offset), (long)DoublePart.asDouble(length), (long)DoublePart.asDouble(loopstart), (long)DoublePart.asDouble(loopend), audioformat), samplerate / session.getRate(), session.getChannels()); 
					//return new AudioFileStream(file, rate, channels, type);					
				}
				
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
	}
	
	
	public AudioFormat getFormat(File file)
	{
		if(cformat != null) return cformat;
		
		if(length != null)
		{
			float sampleRate = 48000;
			int sampleSizeInBits = 16;
			int channels = 1;
			boolean signed = true;
			boolean bigEndian = false;
			
			if(format != null)
			{
				Object[] s_formatstrings = ObjectsPart.asObjects(format);
				for (int i = 0; i < s_formatstrings.length; i++) {
					String formatstring = s_formatstrings[i].toString();
					StringTokenizer st = new StringTokenizer(formatstring, " ");
					while (st.hasMoreTokens()) {
						String formattoken = (String) st.nextToken();
						if(formattoken.endsWith("khz"))
							sampleRate = Float.parseFloat(formattoken.substring(0, formattoken.length() - 3)) * 1000;
						else if(formattoken.endsWith("hz"))
							sampleRate = Float.parseFloat(formattoken.substring(0, formattoken.length() - 2));
						else if(formattoken.endsWith("bit"))
							sampleSizeInBits = Integer.parseInt(formattoken.substring(0, formattoken.length() - 3));
						else if(formattoken.endsWith("ch"))
							channels = Integer.parseInt(formattoken.substring(0, formattoken.length() - 2));
						else if(formattoken.equalsIgnoreCase("stereo"))
							channels = 2;
						else if(formattoken.equalsIgnoreCase("bigEndian"))
							bigEndian = true;					
						else if(formattoken.equalsIgnoreCase("signed"))
							signed = true;
						
					}
				}
			}
			
			return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
		}
		else
		{
			AudioFormat audioformat = AudioFileStream.getFormat(file);
			return audioformat;
		}
	}
	
	NameSpace namespace;
	ResourceManager manager;	

	public AudioFileInstance(boolean noresample, Parameters parameters)
	{
		this.noresample = noresample;

		namespace = parameters.getNameSpace();
		manager = ResourceManager.getInstance(namespace);
		
		returnvar = parameters.getParameterWithDefault("output");
		inputvar = parameters.getParameter("input");
		filename = parameters.getParameterWithDefault(1, "filename");
		offset = parameters.getParameterWithDefault(2, "offset");
		length = parameters.getParameter(3, "length");
		format = parameters.getParameter(4, "format");		
		loopstart = parameters.getParameter(5, "loopstart");		
		loopend = parameters.getParameter(6, "loopend");
		streammode = parameters.getParameter(7, "streammode");
		
		wrkdir = parameters.get("wrkdir");
		
		AudioFileStreamable afs = new AudioFileStreamable();		 
		answer = AudioEvents.asVariable(new AudioEvent(0, afs));
		
		returnvar.add(answer);
		
		ObjectsPart.getInstance(filename).addListener(this);
		ObjectsPart.getInstance(wrkdir).addListener(this);
		if(offset != null)
			DoublePart.getInstance(offset).addListener(this);
		if(length != null)
			DoublePart.getInstance(length).addListener(this);
		if(loopstart != null)
			DoublePart.getInstance(loopstart).addListener(this);
		if(streammode != null)
			DoublePart.getInstance(streammode).addListener(this);
		
		/*
		 offset = parameters.getParameterWithNull(2, "offset");
		 length = parameters.getParameterWithNull( 3, "length");
		 format = parameters.getParameterWithNull( 4, "format");
		 
		 RObjects.getInstance(filename).addListener(this);
		 
		 if(offset != null) RDouble.getInstance(offset).addListener(this);
		 if(length != null) RDouble.getInstance(length).addListener(this);
		 if(format != null) RObjects.getInstance(format).addListener(this);
		 
		 */
		calc();
	}
	
	/* NEVER CALLED ANYWAY
	protected void finalize() {

		System.out.println("AUDOFILE FINALYZED RUNNED");
		if(fc != null)
		{
			try {
				SharedFileChannels.getInstance().returnFileChannel(fc);
			} catch (IOException e) {
				e.printStackTrace();
			}
			fc = null;
		}
	} */
	
	public void close() {
		/*
		if(fc != null)
			try {
				SharedFileChannels.getInstance().returnFileChannel(fc);
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			
		if(channel != null)
		{
			try {
				channel.close();
				channel = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		returnvar.remove(answer);
		
		if(resource != null)
		{
			resource.close();
			resource  = null;
		}				
		
		ObjectsPart.getInstance(filename).removeListener(this);
		ObjectsPart.getInstance(wrkdir).removeListener(this);
		if(offset != null)
			DoublePart.getInstance(offset).removeListener(this);
		if(length != null)
			DoublePart.getInstance(length).removeListener(this);		
		if(loopstart != null)
			DoublePart.getInstance(loopstart).removeListener(this);
		if(streammode != null)
			DoublePart.getInstance(streammode).addListener(this);
		
		
		/*
		 RObjects.getInstance(filename).removeListener(this);
		 
		 if(offset != null) RDouble.getInstance(offset).removeListener(this);
		 if(length != null) RDouble.getInstance(length).removeListener(this);
		 if(format != null) RObjects.getInstance(format).removeListener(this);
		 
		 clear();*/
	}
	
	
	boolean commitmode = false;
	/* (non-Javadoc)
	 * @see rasmus.interpreter.RUnitInstanceAdapter#calc()
	 */
	public void calc() {
		
		
		if(!commitmode)
		{
			commitmode = true;
			namespace.addToCommitStack(this);
		}
	}

	public String expandPath(String path)
	{		
		path = path.trim();
		if(path.length() == 0) return path;
		if(path.startsWith(File.separator)) return path;		
		if(path.length() > 2 && Character.isLetter(path.charAt(0)) && path.charAt(1) == ':' && path.charAt(2) == File.separatorChar)
				return path;
	
		String wrkdir = ObjectsPart.toString(this.wrkdir);
		if(wrkdir.trim().length() == 0)
			return path;					
		
		if(wrkdir.endsWith(File.separator))
		{
			return wrkdir + path;
		}
		else
		{
			return wrkdir + File.separator + path;
		}
	}
	
	Resource resource = null;
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit()
	{
		if(commitmode)
		{
			commitmode = false;
			
			Object[] s_filenames = ObjectsPart.asObjects(filename);
			if(s_filenames.length == 0) 
			{
				s_filename = null;
				c_filename = null;
				return;
			}
			s_filename = expandPath(s_filenames[0].toString());
			if(s_filename.toLowerCase().endsWith(".mid")) 
			{
				s_filename = null;
				c_filename = null;
				return;
			}
			if(s_filename.toLowerCase().endsWith(".rlm")) 
			{
				s_filename = null;
				c_filename = null;
				return;
			}		    
			if(inputvar != null)
				{
					s_filename = null;
					c_filename = null;
					return;		    	
				}
			
			if(resource != null) resource.close();
			resource = manager.getResource(s_filename);
			if(resource == null)
			{
				s_filename = null;
				c_filename = null;
				return;		    	
			}				
			file = resource.getFile();			
			s_filename = file.getPath();
			
			
			long i_length = 0;
			long i_offset = 0;
			int i_streammode = 0;
			if(streammode != null)
				i_streammode = (int)DoublePart.asDouble(streammode);
			
			String id_filename;
			if(offset == null || length == null)
			{
				id_filename = s_filename;
			}
			else
			{
				i_offset = (int)DoublePart.asDouble(offset);
				i_length = (long)DoublePart.asDouble(length);		    
				id_filename = s_filename + ":" + i_offset + ":" + i_length;
			}
			
			if(c_filename != null) 
				if(c_filename.equals(id_filename)) return;
			c_filename = id_filename;
			file = new File(s_filename);
			
			if(inputvar != null)
			{
				return;
			}
			
			
			if(i_length == 0) i_length = file.length();
			
			if(i_offset != 0)
				GlobalStatus.setStatus("loading " + s_filename + " " + i_offset + "-" + (i_length +i_offset));	
			else
				GlobalStatus.setStatus("loading " + s_filename );	
			
			if(i_streammode == 1 || (i_length < FILE_BUFFER_SIZE))
			{		
				datacache = new byte[(int)i_length];
				try
				{
					FileInputStream fis = new FileInputStream(file);
					fis.skip(i_offset);		
					fis.read(datacache);
					fis.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				loopcache = null;
				partialcache = null;
				
			}
			else 
			{
			/*
			if(fc != null)
				try {
					SharedFileChannels.getInstance().returnFileChannel(fc);
				} catch (IOException e) {
					e.printStackTrace();
				}
			*/
				
			try
			{
				//fc = SharedFileChannels.getInstance().getFileChannel(file);
				if(channel != null) channel.close();
				channel = new NonBlockingReadableByteChannel(file, i_offset, FILE_BUFFER_SIZE);
				
				int i_loopstart = (int)DoublePart.asDouble(loopstart);
				if(i_loopstart == 0)
					channel_loop = channel;
				else
					channel_loop = new NonBlockingReadableByteChannel(file, i_offset + i_loopstart, FILE_BUFFER_SIZE);
					
				
			}
			catch(Exception e)
			{
				channel = null;
				e.printStackTrace();
			}
			
			
			}
			
			cformat = null;
			
			
			audioreader = null;
			if(length == null)
			{
				boolean audioreaderfound = false;
				Iterator providers = RasmusUtil.getProviders(AudioFileReader.class).iterator();
				while (providers.hasNext()) {
					AudioFileReader reader = (AudioFileReader) providers.next();
					try {
						AudioInputStream stream = reader.getAudioInputStream(file);
						AudioFormat format = stream.getFormat();
						if(format != null) cformat = format;		
						stream.close();
						audioreader = reader;
						audioreaderfound = true;
						break;
					} catch (UnsupportedAudioFileException e) {
						continue;
					} catch (IOException e) {
						continue;
					}
				}
				if(!audioreaderfound)
				{
					audioreader = null;
					cformat = getFormat(file);
				}
			}
			else
			    cformat = getFormat(file);
			
			GlobalStatus.setStatus("");	
			
			
		}
	}
	
	AudioFileReader audioreader = null;
	AudioFormat cformat = null;
	String s_filename = null;
	String c_filename = null;
	byte[] partialcache = null;
	
}

public class AudioFile implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Audio File");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "filename",		"Filename",		null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		metadata.add(2, "offset",		"Offset",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "length",		"Length",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "format",		"Format",		null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		metadata.add(5, "loopstart",	"loopstart",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(6, "loopend",		"loopend",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		
		List options = new ArrayList();
		options.add("0");
		options.add("1");				
		metadata.add(7, "streammode",	"Stream mode (0=disk,1=memory)",   "0", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN).put("options", options);
		return metadata;		
	}	
	
	boolean noresample = false;
	
	public AudioFile(boolean noresample)
	{
		this.noresample = noresample;
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioFileInstance(noresample, parameters);
	}
}