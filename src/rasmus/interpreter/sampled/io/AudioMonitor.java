/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.io;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.sampled.util.AudioStreamConverter;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioMonitorInstance extends UnitInstanceAdapter
{
	
	public Variable outputvar_anwer;
	public Variable monitorvar_anwer;
	
	public Variable outputvar;
	public Variable inputvar;
	public Variable monitorvar;
	
	AudioMonitorStreamInstance amsi = null;
	
	class AudioMonitorStreamInstance implements AudioStream
	{
		AudioStream peerstream;
		AudioCache audiocache;
		AudioSession session;
		
		final double[] monitorbuffer = new double[32000];
		int bufferpos = 0;
		long monitorreadpos = 0;
		
		
		public AudioMonitorStreamInstance(AudioSession session)
		{
			peerstream = session.openStream(inputvar);
			audiocache = session.getAudioCache();
			this.session = session;
		}
		
		public int mix(double[] buffer, int start, int end) 
		{
			double[] readbuffer = audiocache.getBuffer(end);
			int ret = replace(readbuffer, start, end);
			if(ret == -1)
			{
				audiocache.returnBuffer(readbuffer);
				return -1;
			}
			end = start + ret;
			for (int i = start; i < end; i++) 
				buffer[i] += readbuffer[i];					
			audiocache.returnBuffer(readbuffer);
			return ret;
		}
		public int replace(double[] buffer, int start, int end) {
			int ret = peerstream.replace(buffer, start, end);
			if(ret == -1) return -1;
			
			end = start + ret;
			monitorreadpos += ret;
			int monitorbuffer_len = monitorbuffer.length;
			for (int i = start; i < end; i++) {
				double data = buffer[i];
				monitorbuffer[bufferpos] = data;
				bufferpos = (bufferpos + 1) % monitorbuffer_len;
			}
			return ret;
		}
		public int isStatic(double[] buffer, int len) {			
			return -1; // Not supported
		}
		public int skip(int len) {
			return peerstream.skip(len);
		}
		public void close() {
			synchronized(AudioMonitorInstance.this)
			{
				amsi = null;
				peerstream.close();
			}
		}
	}
	
	class AudioMonitorStreamable_output implements AudioStreamable
	{
		public AudioStream openStream(AudioSession session) {
			synchronized(AudioMonitorInstance.this)
			{
				if(amsi != null)
				{
					return session.openStream(inputvar);
				}
				else
				{
					amsi = new AudioMonitorStreamInstance(session); 
					return amsi;
				}
			}
		}
	}
	
	
	class AudioMonitorStream_MonitorInstance implements AudioStream
	{
		
		AudioMonitorStreamInstance amsi = null;
		boolean session_realtime;
		int bufferpos = 0;
		long monitorreadpos = 0;
		AudioSession session;
		
		public AudioMonitorStream_MonitorInstance(AudioSession session)
		{
			session_realtime = session.isRealTime();
			this.session = session;
		}
		
		public AudioMonitorStreamInstance getAMSI()
		{
			AudioMonitorStreamInstance current_amsi = AudioMonitorInstance.this.amsi;
			if(current_amsi == null)
			{
				return null;
			}
			if(current_amsi != amsi)
			{
				amsi = current_amsi;
				bufferpos = amsi.bufferpos;
				monitorreadpos = amsi.monitorreadpos;
			}
			return amsi;
		}
		
		AudioStreamConverter asc = null;
		
		public int mix(double[] buffer, int start, int end) {
			
			int len = end - start;
			boolean ok = true;
			while(ok)
			{				
				ok = false;
				AudioMonitorStreamInstance amsi = getAMSI();
				if(amsi == null)
				{				
					if(!session_realtime)
					{					
						double slen = ((double)len) / ((double)(session.getChannels() * session.getRate()));
						try {
							// Controls the speed the reader get's the stream !!!
							Thread.sleep((int)(slen * 1000));
						} catch (InterruptedException e) {
							e.printStackTrace();
							return 0;
						}
					}
					return len;
				}
				
				if(  (session.getChannels() != amsi.session.getChannels())
						|| (session.getRate() != amsi.session.getRate()))
				{			
					if(asc == null) asc = new AudioStreamConverter(session);
					asc.setSource(amsi.session, this);
					AudioSession baksession = session;
					session = asc.getSession();
					int ret = asc.mix(buffer, start, end);
					session = baksession;
					return ret;
				}
				
				int curbufferpos = amsi.bufferpos;
				long curmonitorreadpos = amsi.monitorreadpos;
				double[] monitorbuffer = amsi.monitorbuffer;
				int monitorbuffer_len = monitorbuffer.length;
				
				int avail = (int)(curmonitorreadpos - monitorreadpos);
				if(avail > monitorbuffer_len) 
				{				
					monitorreadpos = curmonitorreadpos - len;
					bufferpos = ((curbufferpos - len) + monitorbuffer_len) % monitorbuffer_len; 
					avail = len;			    
				}
				
				if(avail < len)
				{
					if(!session_realtime)
					{
						double slen = ((double)(len - avail)) / ((double)(session.getChannels() * session.getRate()));
						try {
							// Wait for the avail to be available 
							Thread.sleep((int)(slen * 1000));
						} catch (InterruptedException e) {
							e.printStackTrace();
							return 0;
						}
						
						ok = true;
						continue;
					}				
				}
				else
				{
					avail = len;
				}
				
				int r_bufferpos = bufferpos;
				int s_pos = start;
				
				for (int i = 0; i < avail; i++) {				
					buffer[s_pos] += monitorbuffer[r_bufferpos];
					r_bufferpos = (r_bufferpos + 1) % monitorbuffer_len;
					s_pos++;
				}
				
				monitorreadpos += avail;
				bufferpos = r_bufferpos;
				
			}
			
			return len;
		}
		
		public int replace(double[] buffer, int start, int end) {
			Arrays.fill(buffer, start, end, 0);		
			return mix(buffer, start, end);
		}
		
		public int isStatic(double[] buffer, int len) {
			return -1;
		}
		public int skip(int len) {
			return mix(new double[len], 0, len);
		}
		
		public void close() {
		}
		
	}
	
	class AudioMonitorStreamable_monitor implements AudioStreamable
	{
		public AudioStream openStream(AudioSession session) {
			return new AudioMonitorStream_MonitorInstance(session);
		}
	}	
	
	public AudioMonitorInstance(Parameters parameters) {
		
		outputvar = parameters.getParameterWithDefault("output");
		inputvar = parameters.getParameterWithDefault("input");
		monitorvar = parameters.getParameterWithDefault(1, "monitor");
		
		outputvar_anwer = AudioEvents.asVariable(new AudioEvent(0, new AudioMonitorStreamable_output()));		
		outputvar.add(outputvar_anwer);
		
		monitorvar_anwer = AudioEvents.asVariable(new AudioEvent(0, new AudioMonitorStreamable_monitor()));		
		monitorvar.add(monitorvar_anwer);
		
	}
	
	public void close() {
		
		if(outputvar_anwer != null)
		{			
			outputvar.remove(outputvar_anwer);
			outputvar_anwer = null;
		}
		
		if(monitorvar_anwer != null)
		{			
			monitorvar.remove(monitorvar_anwer);
			monitorvar_anwer = null;
		}		
	}
	
	public void calc() {
	}
	
}

public class AudioMonitor implements UnitFactory, MetaDataProvider {

	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Audio Output");
		metadata.add(-1, "output",		"Output",				null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "monitor",	    "Monitor Output",	    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(-1, "input",		"Input",				null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioMonitorInstance(parameters);
	}	
}
