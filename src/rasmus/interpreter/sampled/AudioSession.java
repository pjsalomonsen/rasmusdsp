/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.sampled.util.AudioInputStreamConverter;
import rasmus.interpreter.sampled.util.DoubleByteStream;
import rasmus.interpreter.sampled.util.FloatByteStream;
import rasmus.wavefloat.FloatEncoding;

public class AudioSession {
	
	//public static int BUFFERED_STREAM = 0; // Buffered Audio Stream
	
	//public static int REALTIME_STREAM = 1; // RealTime Audio Stream
	
	private double rate;
	
	private int channels;
	
	private boolean realtime = false;
	
	private static AudioCache audiocache = new AudioCache();
	
	private BeatToTimeMapper bmap = null;
	
	private AudioSession session = this;
	
	private AudioSession monosession = null;
	
	private AudioSession root = this;
	
	private class AudioStreamableSession {
		AudioStreamableSession streamablesession = this;
		
		AudioStream audiostream;
		
		AudioStreamable streamable;
		
		LinkedList streamsessions = new LinkedList();
		
		int frame = -1;
		
		double[] framebuffer = null;
		
		double[] staticbuffer = new double[1];
		
		int framebufferret = 0;
		
		boolean isstaticbuffer = false;
		
		boolean isinskipmode = false;
		
		boolean readNextBufferStaticValue(int len) {
			frame++;
			
			if (audiostream == null)
				framebufferret = -1;
			else
				framebufferret = audiostream.isStatic(staticbuffer, len);
			
			if (framebufferret == -1) {
				frame--;
				return false;
			}
			
			if (framebuffer != null) {
				getAudioCache().returnBuffer(framebuffer);
				framebuffer = null;
			}
			isstaticbuffer = true;
			isinskipmode = false;
			return true;
		}
		
		void skipNextBuffer(int len) {
			if (frame == 1)
				if (readNextBufferStaticValue(len))
					return;
			isstaticbuffer = false;
			isinskipmode = true;
			frame++;
			if (framebuffer != null) {
				getAudioCache().returnBuffer(framebuffer);
				framebuffer = null;
			}
			// framebuffer = audiocache.getBuffer(end);
			if (audiostream == null)
				framebufferret = -1;
			else {
				framebufferret = audiostream.skip(len);
				
			}
			
		}
		
		void readNextBuffer(int start, int end) {
			if (frame == 1)
				if (readNextBufferStaticValue(end - start))
					return;
			isstaticbuffer = false;
			isinskipmode = false;
			frame++;
			if (framebuffer != null) {
				getAudioCache().returnBuffer(framebuffer);
				framebuffer = null;
			}
			framebuffer = getAudioCache().getBuffer(end);
			if (audiostream == null)
				framebufferret = -1;
			else {
				framebufferret = audiostream.replace(framebuffer, start, end);
				
			}
			
		}
		
		class AudioStreamSession implements AudioStream {
			int cframe = -1;
			
			public int mix(double[] buffer, int start, int end) {
				if (fcount == 1)
					if (frame != -1)
						return audiostream.mix(buffer, start, end);
				cframe++;
				
				if (frame != cframe)
					readNextBuffer(start, end);
				
				if (isinskipmode) {
					System.out.println("Skip error read");
					return end - start;
				}
				
				if (framebufferret == -1)
					return -1;
				
				if (isstaticbuffer) {
					double s = staticbuffer[0];
					if (s != 0)
						for (int i = start; i < start + framebufferret; i++) {
							buffer[i] += s;
						}
				} else {
					for (int i = start; i < start + framebufferret; i++) {
						buffer[i] += framebuffer[i];
					}
				}
				
				if (framebufferret > (end - start)) {
					System.out.println("read error");
				}
				return framebufferret;
			}
			
			public int replace(double[] buffer, int start, int end) {
				if (fcount == 1)
					if (frame != -1)
						return audiostream.replace(buffer, start, end);
				cframe++;
				
				if (frame != cframe)
					readNextBuffer(start, end);
				
				if (isinskipmode) {
					System.out.println("Skip error read");
					Arrays.fill(buffer, start, end, 0);
					return end - start;
				}
				
				if (framebufferret == -1)
					return -1;
				
				if (isstaticbuffer) {
					Arrays.fill(buffer, start, start + framebufferret,
							staticbuffer[0]);
				} else {
					for (int i = start; i < start + framebufferret; i++) {
						buffer[i] = framebuffer[i];
					}
				}
				
				if (framebufferret > (end - start)) {
					System.out.println("read error");
					framebufferret = end - start;
				}
				return framebufferret;
			}
			
			public int skip(int len) {
				if (fcount == 1)
					if (frame != -1)
						return audiostream.skip(len);
				cframe++;
				
				if (frame != cframe)
					skipNextBuffer(len); // readNextBuffer(0, len);
				
				if (framebufferret == -1)
					return -1;
				
				if (framebufferret > (len)) {
					System.out.println("read error");
					framebufferret = len;
				}
				return framebufferret;
			}
			
			public int isStatic(double[] buffer, int len) {
				if (fcount == 1)
					if (frame != -1)
						return audiostream.isStatic(buffer, len);
				cframe++;
				
				if (frame != cframe) {
					if (!readNextBufferStaticValue(len)) {
						cframe--;
						return -1;
					}
				}
				
				if (!isstaticbuffer) {
					cframe--;
					return -1;
				}
				
				buffer[0] = staticbuffer[0];
				return framebufferret;
			}
			
			public void close() {
				streamsessions.remove(this);
				if (streamsessions.size() == 0) {
					if (audiostream != null)
						audiostream.close();
					audiostream = null;
					
					audionstreamablesession.remove(streamable);
					streamable = null;
					if (framebuffer != null) {
						getAudioCache().returnBuffer(framebuffer);
						framebuffer = null;
					}
				}
			}
			
		}
		
		AudioStreamableSession(AudioStreamable streamable) {
			this.streamable = streamable;
			audiostream = streamable.openStream(session);
			
		}
		
		int fcount = 0;
		
		public AudioStream openStream() {
			if (audiostream == null)
				return null;
			AudioStreamSession streamsession = new AudioStreamSession();
			fcount++;
			streamsessions.add(streamsession);
			return streamsession;
		}
	}
	
	private Hashtable audionstreamablesession = new Hashtable();
	
	// private AudioCache audiocache_owner = null;
	
	private boolean commitcompleted = false;
	
	private ArrayList commits = new ArrayList();
	
	private Commitable initcommit = new Commitable() {
		
		public int getRunLevel() {
			return RUNLEVEL_INIT;
		}
		
		public void commit() {
			synchronized (commits) {
				commitcompleted = true;
			}
		}
	};
	
	AudioVoiceManager voicemanager;
	NameSpace namespace = null;
	Interpreter interpreter = null;
	
	public NameSpace getNameSpace()
	{
		if(root != this)
			return root.getNameSpace();
		
		if(namespace == null)
		{
			if(interpreter == null) interpreter = new Interpreter();
			namespace = interpreter;
			// Use isolated commitsystem!!! 
			//Struct.getInstance(namespace.get("this")).setCommitSystem(new CommitSystem());
		}
		return namespace;
	}
	public void setNameSpace(NameSpace namespace)
	{
		this.namespace = namespace;
	}
	
	public AudioVoiceManager getVoiceManager()
	{
		if(voicemanager != null) return voicemanager;
		if(this == root) return null;
		return root.getVoiceManager();
	}
	public void setMaxPolyphony(int maxpoly)
	{
		if(maxpoly > 0)
			voicemanager = new AudioVoiceManager(maxpoly);
		else
			voicemanager = null;
	}
	public int getMaxPolyphony()
	{
		AudioVoiceManager vm = getVoiceManager();
		if(vm == null) return -1;
		return vm.getMaxPolyphony();
	}
	
	public void commit() {
		
		//getNameSpace().commit();
		
		if (this != root) {
			root.commit();
			return;
		}
		
		Runnable[] commitlist ; 
		synchronized (commits) {
			if (!commitcompleted) return;
			commitlist = new Runnable[commits.size()];
			commits.toArray(commitlist);
			commits.clear();
			commitcompleted = false;
		}
		
		for (int i = 0; i < commitlist.length; i++) {
			commitlist[i].run();
		}
		
		
		/*
		 if (commitcompleted) {
		 commitcompleted = false;
		 Iterator iterator = commits.iterator();
		 while (iterator.hasNext()) {
		 Runnable runnable = (Runnable) iterator.next();
		 runnable.run();
		 }
		 commits.clear();
		 }
		 }*/
	}
	
	public int getChannels() {
		return channels;
	}
	
	public double getRate() {
		return rate;
	}
	
	public AudioCache getAudioCache() {
		return audiocache;
	}
	
	public BeatToTimeMapper getBeatToTimeMap() {
		return bmap;
	}
	
	public void setBeatToTimeMap(BeatToTimeMapper bmap) {
		this.bmap = bmap;
	}
	
	public boolean isRealTime() {
		return realtime;
	}
	
	public void setRealTime(boolean value) {
		realtime = value;
	}
	
	public void addToBatch(Runnable runnable) {
		synchronized (root.commits) {
			root.commits.add(runnable);
			getNameSpace().addToCommitStack(initcommit);
		}
	}
	
	public AudioSession(double rate, int channels) {
		this.rate = rate;
		this.channels = channels;
	}
	
	public AudioSession newSession(double rate, int channels) {
		
		AudioSession audiosession = new AudioSession(rate, channels);
		audiosession.bmap = this.bmap;
		audiosession.realtime = this.realtime;
		audiosession.root = this.root;
		return audiosession;
	}
	
	public AudioSession newSession() {
		return newSession(getRate(), getChannels());
	}
	
	public AudioSession getMonoSession() {
		if (monosession != null)
			return monosession;
		monosession = newSession(getRate(), 1);		
		return monosession;
		
	}
	
	public AudioStream openStream(NameSpace namespace) {
		return openStream(namespace.get("output"));
	}
	
	public AudioStream openStream(Variable variable) {
		return AudioEvents.openStream(variable, this);
	}
	
	public AudioStream openStream(AudioStreamable streamable) {
		
		AudioStreamableSession streamablesession = (AudioStreamableSession) audionstreamablesession
		.get(streamable);
		if (streamablesession == null) {
			streamablesession = new AudioStreamableSession(streamable);
			audionstreamablesession.put(streamable, streamablesession);
		}
		return streamablesession.openStream();
	}
	
	
	public double[] asDoubleArray(NameSpace namespace) {
		return asDoubleArray(namespace.get("output"));
	}
	
	public double[] asDoubleArray(Variable variable) {
		AudioStream audiostream = openStream(variable);
		double[] ret = asDoubleArray(audiostream);
		audiostream.close();
		return ret;
	}	
	public double[] asDoubleArray(AudioStreamable streamable)
	{
		AudioStream audiostream = openStream(streamable);
		double[] ret = asDoubleArray(audiostream);
		audiostream.close();
		return ret;
	}
	public double[] asDoubleArray(AudioStream stream)
	{
		ArrayList buffers = new ArrayList();
		int totalsize = 0;
		int ret;
		do
		{
			double[] buffer = new double[500];
			ret = stream.replace(buffer, 0, buffer.length);
			if(ret != -1) totalsize += ret;
			buffers.add(buffer);
		}
		while(ret != -1);
		double[] retbuffer = new double[totalsize];
		Iterator iterator = buffers.iterator();
		int pos = 0;
		while (iterator.hasNext()) {
			double[] buffer = (double[]) iterator.next();
			int end = pos + buffer.length;
			if(end > totalsize) end = totalsize;
			int j = 0;
			for (int i = pos; i < end; i++) {
				retbuffer[i] = buffer[j];
				j++;
			}
			pos = end;
		}
		return retbuffer;
	}
				
	public AudioInputStream asByteStream(AudioStream stream, AudioFormat format)
	{
		if(!(format.getEncoding().equals(AudioFormat.Encoding.PCM_SIGNED)
				|| format.getEncoding().equals(AudioFormat.Encoding.PCM_UNSIGNED)
				|| format.getEncoding().equals(FloatEncoding.PCM_FLOAT)))
			{
				throw new IllegalArgumentException("Only PCM_SIGNED, PCM_UNSIGNED, PCM_FLOAT encoding supported!");
			}
						
		
		if(format.getEncoding().equals(FloatEncoding.PCM_FLOAT))
		{
			ByteOrder order;
			if(format.isBigEndian())
				order = ByteOrder.BIG_ENDIAN;
			else
				order = ByteOrder.LITTLE_ENDIAN;
			
			if(format.getSampleSizeInBits() == 64) 
				return new AudioInputStream(new DoubleByteStream(stream, order), format, AudioSystem.NOT_SPECIFIED);
			else if(format.getSampleSizeInBits() == 32) 					
				return new AudioInputStream(new FloatByteStream(stream, order), format, AudioSystem.NOT_SPECIFIED);
			else
				throw new IllegalArgumentException("Only 32 or 64 bit float supported!");
		}		
			
		// int sampleSizeInBits, boolean signed, boolean bigEndian
		int sampleSizeInBits = format.getSampleSizeInBits();
		boolean signed = format.getEncoding().equals(AudioFormat.Encoding.PCM_SIGNED) ;
		boolean bigEndian = format.isBigEndian();		
		return new AudioInputStream(new AudioInputStreamConverter(stream, sampleSizeInBits, signed, bigEndian), format, AudioSystem.NOT_SPECIFIED);
	}
	
	public AudioInputStream asByteStream(AudioStreamable streamable, AudioFormat format)
	{
		return asByteStream(openStream(streamable), format);
	}	
	
	public AudioInputStream asByteStream(Variable variable, AudioFormat format)
	{
		return asByteStream(openStream(variable), format);
	}		

	public AudioInputStream asByteStream(NameSpace namespace, AudioFormat format)
	{
		return asByteStream(openStream(namespace), format);
	}			
	
	public void close() {
		monosession = null;
		bmap = null;
		root = null;
		/*
		if(interpreter != null)
		{
			interpreter.close();
			interpreter.commit();
		}*/
	}
	
}
