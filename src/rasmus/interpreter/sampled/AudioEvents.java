/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.Sequence;
import javax.sound.midi.Track;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.midi.MidiSequence;

public class AudioEvents extends ListPart implements AudioStreamable {
	
	public List track = new LinkedList();
	
	public static long TimeToStreamTime(double time, double rate, int channels)
	{
		long tcalctime = (long)(time*rate*channels);
		tcalctime -= tcalctime % channels;
		return tcalctime;		
	}
	public static double StreamTimeToTime(double time, double rate, int channels)
	{
		return time/(rate*channels);
	}
	
	static class SequencerBeatMap implements BeatToTimeMapper
	{
		//Sequencer seqs;
		//double rate; 
		//int channels;
		//double constant;
		Track track;
		int trackpos = 0;
		double trackbeatpos = 0;
		ArrayList trackpairs = new ArrayList();
		/* , double rate, int channels*/
		public SequencerBeatMap(Sequence seq)
		{
			track = seq.getTracks()[0];
			
			// To be programmed in better way!!!
			//constant = rate * channels;
			/*
			 try
			 {
			 seqs = MidiSystem.getSequencer(false);
			 seqs.setSequence(seq);
			 }
			 catch(Exception e)
			 {
			 e.printStackTrace();
			 }*/
			//this.rate = rate;
			//this.channels = channels;
		}
		
		double tconstant = 1.0 / ((double) MidiSequence.DEFAULT_RES);
		long lasttickpos = 0;
		
		public double getTime(double beat) {
			double calctime = 0;	
			double beatleft = beat;
			double[] pair = {0, (1) / (120.0 / 60.0)};
			double lastfactor = pair[1];
			for (int i = 0; i < trackpairs.size(); i++) {
				pair = (double[])trackpairs.get(i);
				if(pair[0] > beatleft)
				{
					calctime += beatleft * lastfactor;
					return calctime;
				}
				calctime += pair[0] * lastfactor;
				lastfactor = pair[1];
				beatleft -= pair[0];
			}
			
			int tracklen = track.size();	
			while(trackpos < tracklen)
			{
				MidiEvent midievent = track.get(trackpos);
				if(midievent.getMessage() instanceof MetaMessage)
				{
					MetaMessage mmsg = (MetaMessage)midievent.getMessage();
					if(mmsg.getType() == 0x51)
					{
						byte[] msg = mmsg.getData();
						int tempoInMPQ = 
							((msg[0] & 0xFF) << 16) +
							((msg[1] & 0xFF) << 8) +
							((msg[2] & 0xFF));
						
						double tempo = 60000000.0 / ((double)tempoInMPQ ); 
						
						pair = new double[2];
						pair[0] = (midievent.getTick() - lasttickpos) * tconstant;
						pair[1] = (1) / (tempo / 60.0);
						lasttickpos = midievent.getTick();
						trackpairs.add(pair);
						if(pair[0] > beatleft)
						{
							calctime += beatleft * lastfactor;
							return (calctime);
						}
						calctime += pair[0] * lastfactor;
						lastfactor = pair[1];
						beatleft -= pair[0];						
					}
				}
				trackpos++;
			}
			
			calctime += beatleft * lastfactor;
			
			return calctime;
		}
		
		
		public double getBeat(double time) {
			double calcbeat = 0;	
			double timeleft = time;
			double[] pair = {0, (1) / (120.0 / 60.0)};
			double lastfactor = pair[1];
			for (int i = 0; i < trackpairs.size(); i++) {
				pair = (double[])trackpairs.get(i);
				double beattime = pair[0] * lastfactor;
				if(pair[0] > timeleft)
				{
					calcbeat += timeleft / lastfactor;
					return calcbeat;
				}
				calcbeat += pair[0];
				lastfactor = pair[1];
				timeleft -= beattime;
			}
			
			int tracklen = track.size();	
			while(trackpos < tracklen)
			{
				MidiEvent midievent = track.get(trackpos);
				if(midievent.getMessage() instanceof MetaMessage)
				{
					MetaMessage mmsg = (MetaMessage)midievent.getMessage();
					if(mmsg.getType() == 0x51)
					{
						byte[] msg = mmsg.getData();
						int tempoInMPQ = 
							((msg[0] & 0xFF) << 16) +
							((msg[1] & 0xFF) << 8) +
							((msg[2] & 0xFF));
						
						double tempo = 60000000.0 / ((double)tempoInMPQ ); 
						
						pair = new double[2];
						pair[0] = (midievent.getTick() - lasttickpos) * tconstant;
						pair[1] = (1) / (tempo / 60.0);
						lasttickpos = midievent.getTick();
						trackpairs.add(pair);
						double beattime = pair[0] * lastfactor;
						if(pair[0] > timeleft)
						{
							calcbeat += timeleft / lastfactor;
							return calcbeat;
						}
						calcbeat += pair[0];
						lastfactor = pair[1];
						timeleft -= beattime;				
					}
				}
				trackpos++;
			}
			
			calcbeat += timeleft / lastfactor;
			
			return calcbeat;
		}		
		/*
		 public long fixC(double time)
		 {
		 long tcalctime = (long)time;
		 tcalctime -= tcalctime % channels;
		 return tcalctime;
		 
		 }*/
		
	}
	/*
	 public static AudioStream openStream(RVariable variable, double rate, int channels, int type) {
	 
	 AudioSession audiosession = new AudioSession(new AudioCache(), new SequencerBeatMap(MidiSequence.asSequence(variable), rate, channels), rate, channels, type);
	 return getInstance(variable).openStream(audiosession);
	 }	*/
	
	public static BeatToTimeMapper getBeatToTimeMap(Variable variable)
	{
		return new SequencerBeatMap(MidiSequence.asSequence(variable));
	}
	
	public static AudioStream openStream(Variable variable, AudioSession session) {
		if(session.getBeatToTimeMap() == null)
		{
			session.setBeatToTimeMap(new SequencerBeatMap(MidiSequence.asSequence(variable)));
		}
		return getInstance(variable).openStream(session);
	}	
	
	public static AudioEvents getInstance(Variable variable)
	{
		return (AudioEvents)variable.get(AudioEvents.class);
	}		
	public static void addEvent(Variable variable, AudioEvent event)
	{
		((AudioEvents)variable.get(AudioEvents.class)).addObject(event);
	}
	public static Variable asVariable(AudioEvent event)
	{
		Variable variable = new Variable();
		AudioEvents events = ((AudioEvents)variable.get(AudioEvents.class));
		events.addObject(event);
		events.setImmutable(true);
		return variable;
		
	}
	
	private ListPartListener listlistener = new ListPartListener()
	{
		
		public void objectAdded(ListPart source, Object object) {
			synchronized(track)
			{					
				addEvent(object);
			}
		}
		
		private void eventRemoved(AudioEvent aevent)
		{
			if(aevent.start == 0)
			{
				synchronized(realtimestreams)
				{
					Iterator iterator = realtimestreams.iterator();
					while (iterator.hasNext()) {
						AudioEventsStream aes = (AudioEventsStream) iterator.next();
						RemoveEventRunnable rer = new RemoveEventRunnable();
						rer.aes = aes;
						rer.aevent = aevent;								
						aes.audiosession.addToBatch(rer);
					}
				}				
				
			}				
		}
		
		public void objectRemoved(ListPart source, Object object) {
			synchronized(track)
			{					
				//	if(track != null)
				if(object instanceof AudioEvent)
				{
					track.remove((AudioEvent)object);
					eventRemoved((AudioEvent)object);
					
					
				}
			}
		}
		
		public void objectsAdded(ListPart source, List list) {
			synchronized(track)
			{					
				//if(track != null)
				//{
				Iterator iterator = list.iterator();
				while (iterator.hasNext()) {
					Object object = iterator.next();				 
					if(object instanceof AudioEvent) addEvent((AudioEvent)object);			 
				}
			}
		}
		
		public void objectsRemoved(ListPart source, List list) {
			synchronized(track)
			{					
				
				if(track != null)
				{
					track.removeAll(list);
					
					Iterator iterator = list.iterator();
					while (iterator.hasNext()) {
						Object object = iterator.next();	
						if(object instanceof AudioEvent)
						{
							eventRemoved((AudioEvent)object);
						}
					}		
					
					
				}
			}
		}
		
	};
	public AudioEvents()
	{
		forceAddListener(listlistener);
	}
	
	private class AddEventRunnable implements Runnable
	{
		AudioEventsStream aes;
		AudioEvent aevent;
		public void run() {
			//synchronized (aes.audiosession.root)
			//{
			
				
				if(aes.audiosession == null) return;
				
				AudioStream astream = aes.audiosession.openStream(aevent.getStreamable());
				if(astream != null)
				{
					astream = new AudioFallBackStream(astream);
					aes.currentevents.add(astream);
					aes.streamsowner.put(astream, aevent.getStreamable());
				}
			
			//}
		}
		
	}
	
	private class RemoveEventRunnable implements Runnable
	{
		AudioEventsStream aes;
		AudioEvent aevent;
		public void run() {
			
			
			//synchronized (aes.audiosession.root)
			//{
			
				
				Iterator citerator = aes.currentevents.iterator();
				while (citerator.hasNext()) {
					AudioStream element = (AudioStream) citerator.next();
					if(aevent.streamable == aes.streamsowner.get(element))
					{
						
						aes.streamsowner.remove(element);
						citerator.remove();
						element.close();
					}
				}				
				/*
				 AudioStream astream = aes.audiosession.openStream(aevent.getStreamable());
				 if(astream != null)
				 {
				 astream = new AudioFallBackStream(astream);
				 aes.currentevents.add(astream);
				 aes.streamsowner.put(astream, aevent.getStreamable());
				 }*/
			
			//}
		}
		
	}	
	
	public void addEvent(Object object)
	{
		//if(track != null)
		if(object instanceof AudioEvent)
		{
			AudioEvent aevent = (AudioEvent)object;
			if(aevent.start == 0)
			{
				synchronized(realtimestreams)
				{
					Iterator iterator = realtimestreams.iterator();
					while (iterator.hasNext()) {
						AudioEventsStream aes = (AudioEventsStream) iterator.next();
						AddEventRunnable aer = new AddEventRunnable();
						aer.aes = aes;
						aer.aevent = aevent;
						aes.audiosession.addToBatch(aer);
					}
				}			    	
			}
			
			
			ListIterator iter = track.listIterator();
			while (iter.hasNext()) {
				AudioEvent event = (AudioEvent) iter.next();
				if(event.start >= aevent.start)
				{
					iter.set(aevent);
					iter.add(event);
					
					//track.((AudioEvent)object);
					//super.addObject(object);
					return;
				}				
			}
			track.add(aevent);
			
			
		}
	}
	/*
	 public void addObject(Object object)
	 {
	 synchronized(track)
	 {					
	 addEvent(object);
	 }
	 super.addObject(object);
	 
	 }
	 public void removeObject(Object object)
	 {
	 synchronized(track)
	 {					
	 //	if(track != null)
	  if(object instanceof AudioEvent) track.remove((AudioEvent)object);
	  }
	  super.removeObject(object);		
	  }	
	  public void addObjects(List list)
	  {
	  synchronized(track)
	  {					
	  //if(track != null)
	   //{
	    Iterator iterator = list.iterator();
	    while (iterator.hasNext()) {
	    Object object = iterator.next();				 
	    if(object instanceof AudioEvent) addEvent((AudioEvent)object);			 
	    }
	    }
	    //}
	     super.addObjects(list);
	     }
	     public void removeObjects(List list)
	     {
	     synchronized(track)
	     {					
	     
	     if(track != null)
	     {
	     track.removeAll(list);
	     }
	     }
	     super.removeObjects(list);
	     }	*/
	
	class BeatMapDelay implements BeatToTimeMapper
	{
		BeatToTimeMapper bmap;
		double delta;
		double starttime;
		public BeatMapDelay(BeatToTimeMapper bmap, double deltatime, double delta)
		{
			this.bmap = bmap;
			this.delta = delta;
			starttime = deltatime; //bmap.getTime(delta);
		}
		public double getTime(double beat) {
			return bmap.getTime(beat + delta) - starttime;
		}		
		public double getBeat(double time) {
			return bmap.getBeat(time + starttime) - delta;
		}
	}
	
	private ArrayList realtimestreams = new ArrayList();
	class AudioEventsStream implements AudioStream
	{
		double rate;
		int channels; 
		//int type;
		boolean realtime;
		long position;
		AudioEvent nextevent = null;
		long iposition = 0;
		long nextpos = 0;
		double nextbeat = 0;
		int trackpos = 0;
		BeatToTimeMapper bmap;
		LinkedList currentevents = new LinkedList();
		AudioSession audiosession;
		Hashtable streamsowner = new Hashtable();
		//long sessionpos = 0;
		public AudioEventsStream(AudioSession session)
		{			
			this.audiosession = session;
			this.bmap = session.getBeatToTimeMap();
			this.rate = session.getRate();
			this.channels = session.getChannels();
			this.realtime = session.isRealTime();
			if(realtime)
			{
				synchronized(realtimestreams)
				{
					realtimestreams.add(this);
				}
			}
			synchronized(track)
			{			
				if(trackpos < track.size())
				{
					nextevent = (AudioEvent)track.get(trackpos);
					nextpos = TimeToStreamTime(bmap.getTime(nextevent.start),rate,channels);
					trackpos++;
				}
			}
		}
		public int skip(int len) {
			
			int readed = -1;
			
			Iterator citerator = currentevents.iterator();
			
			while(citerator.hasNext())
			{
				
				AudioStream astream = (AudioStream)citerator.next();
				int aread = astream.skip(len);
				if(aread > readed) readed = aread;
				
				
				if(aread == -1)
				{
					citerator.remove();
					astream.close();
					streamsowner.remove(astream);
					
				}
				
				
			}
			
			while((nextevent != null))
			{
				long estart = (nextpos - position);
				if(estart > len)
				{
					readed = len;
					break;
				}
				if(estart < 0) break;
				
				//if(nextpos != sessionpos) audiosession = audiosession.newSession(new BeatMapDelay(bmap, estart));
				AudioStream astream = null;
				
				// A SMALL CHEAT FOR BETTER PERFORMANCE
				if((position+nextpos) == 0)
				{
					astream = audiosession.openStream(nextevent.getStreamable());
				}
				else
					if(len - estart < audiosession.getRate()*audiosession.getChannels())
					{
						AudioSession newsession = audiosession.newSession();
						newsession.setBeatToTimeMap(new BeatMapDelay(bmap, StreamTimeToTime(nextpos,rate,channels), nextbeat));
						astream = newsession.openStream(nextevent.getStreamable());
					}
				
				if(astream != null)
				{
					int aread = astream.skip(len - ((int)estart));
					int bread = (int)(estart + aread);
					if(bread > readed) readed = bread;
					if(aread != -1)
					{
						astream = new AudioFallBackStream(astream);
						currentevents.add(astream);
						streamsowner.put(astream, nextevent.getStreamable());
						
					}
					else
					{
						//	citerator.remove();
						astream.close();					
					}
				}
				
				
				synchronized(track)
				{
					if(trackpos < track.size())
					{
						nextevent = (AudioEvent)track.get(trackpos);
						nextbeat = nextevent.start;
						nextpos = TimeToStreamTime(bmap.getTime(nextevent.start),rate,channels);
						trackpos++;
					}
					else
					{
						nextevent = null;
						nextbeat = 0;
						nextpos = 0;
					}
				}
			}
			
			//audiosession = audiosession.newSession(new BeatMapDelay(bmap, len));
			//sessionpos += len;
			
			
			position += len;
			
			return readed;
			
		}
		
		double[] sbuffer = new double[1];
		public int mix(double[] buffer, int start, int end) {
			
			
			if(realtime)
			{
				if(currentevents.size() == 1) if(nextevent == null)
				{
					AudioFallBackStream astream = (AudioFallBackStream)currentevents.get(0);
					return astream.audiostream.mix(buffer, start, end);
				}		
			}			
			
			int len = (end - start);
			int readed = -1;
						
			{
				Iterator citerator = currentevents.iterator();
				
				while(citerator.hasNext())
				{
					
					AudioStream astream = (AudioStream)citerator.next();
					int aread = astream.isStatic(sbuffer, end - start);
					if(aread != -1)
					{
						if(sbuffer[0] != 0)
						{
							for (int i = start; i < start + aread; i++) {
								buffer[i] += sbuffer[0];
							}
						}
					}
					else
						aread = astream.mix(buffer, start, end);
					if(aread > readed) readed = aread;
					
					
					if(aread == -1)
					{
						streamsowner.remove(astream);
						citerator.remove();
						astream.close();
						
					}
					
					
				}
			}
			
			AudioSession audiosession = this.audiosession;
			long cursespos = 0;
			while((nextevent != null))
			{
				long estart = (nextpos - position);
				if(estart > len)
				{
					readed = len;
					break;
				}
				if(estart < 0) break;
				
				if(nextpos != cursespos)
				{
					audiosession = this.audiosession.newSession();
					audiosession.setBeatToTimeMap(new BeatMapDelay(bmap, StreamTimeToTime(nextpos,rate,channels), nextbeat));
					cursespos = nextpos;
				}
				AudioStream astream = audiosession.openStream(nextevent.getStreamable());
				
				
				if(astream != null)
				{
					int aread = astream.mix(buffer, ((int)estart) + start, end);
					int bread = (int)(estart + aread);
					if(bread > readed) readed = bread;
					if(aread != -1)
					{
						astream = new AudioFallBackStream(astream);
						streamsowner.put(astream, nextevent.getStreamable());
						currentevents.add(astream);
						
					}
					else
					{
						//	citerator.remove();
						astream.close();					
					}
				}
				
				
				synchronized(track)
				{
					if(trackpos < track.size())
					{
						nextevent = (AudioEvent)track.get(trackpos);
						nextbeat = nextevent.start;
						nextpos = TimeToStreamTime(bmap.getTime(nextbeat),rate,channels);
						trackpos++;
					}
					else
					{
						nextevent = null;
						nextpos = 0;
						nextbeat = 0;
					}
				}
			}
			
			position += len;
			
			if(realtime) readed = end - start;
			
			return readed;
			
		}
		public int replace(double[] buffer, int start, int end) {
			
			if(realtime)
			{				
				{				
					if(currentevents.size() == 1) if(nextevent == null)
					{
						AudioFallBackStream astream = (AudioFallBackStream)currentevents.get(0);
						return astream.audiostream.replace(buffer, start, end);
					}		
				}
			}
			
			/*
			 for (int i = start; i < end; i++) {
			 buffer[i] = 0;
			 }*/
			Arrays.fill(buffer, start, end, 0);
			return mix(buffer, start, end);
		}
		public int isStatic(double[] buffer, int len) {
			
			
			if(realtime)
			{				
				{				
					if(currentevents.size() == 1) if(nextevent == null)
					{
						AudioFallBackStream astream = (AudioFallBackStream)currentevents.get(0);
						return astream.audiostream.isStatic(buffer, len);
					}		
				}
			}
			
			// Athuga hven�r n�sta event ver�ur, og hvort �a� s� fyrir utan okkar ramma
			if(nextevent != null)
			{
				long estart = (nextpos - position);
				if(estart >= 0)
					if(estart < len) return -1;
			}			
			position += len;
			
			
			// Athuga hvort einhverjir eventar s�u eftir
			if(currentevents.size() == 0) if(nextevent == null)
			{
				if(realtime) return len;
				return -1;
			}
			
			// Athuga hvort einhverjir eventar s�u virkir
			double[] abuffer = new double[1];
			ArrayList fallbacklist = new ArrayList();
			Iterator citer = currentevents.iterator();
			while (citer.hasNext()) {
				AudioFallBackStream audiostream = (AudioFallBackStream) citer.next();
				
				int ret = audiostream.isStatic(abuffer, len);
				if(ret != -1) fallbacklist.add(audiostream);
				
				if(ret != len)
				{
					// bufferst�r� fr� straum ekki af r�ttri st�r�
					// ��rf er � fallback h�rna
					position -= len;
					Iterator fiter = fallbacklist.iterator();
					while (fiter.hasNext()) ((AudioFallBackStream) fiter.next()).fallBack();						
					return -1;
				}
				
				if(ret == -1)
				{
					// buffer endar skyndilega, ��rf � fallback
					position -= len;
					Iterator fiter = fallbacklist.iterator();
					while (fiter.hasNext()) ((AudioFallBackStream) fiter.next()).fallBack();						
					return -1;
				}
			}
			
			return len;
			
		}				
		
		public void close()
		{			
			
			if(realtime)
			{
				synchronized(realtimestreams)
				{
					realtimestreams.remove(this);
				}
			}
			
			Iterator citerator = currentevents.iterator();
			while(citerator.hasNext())
			{
				AudioStream astream = (AudioStream)citerator.next();
				astream.close();
			}			
			streamsowner.clear();
			currentevents.clear();
			nextevent = null;
			audiosession = null;
		}
	};
	
	class DoubleAddStream implements AudioStream
	{
		AudioStream stream;
		double val;
		public DoubleAddStream(AudioStream stream, double val)
		{
			this.stream = stream;
			this.val = val;
		}
		public int mix(double[] buffer, int start, int end) {
			int ret = stream.mix(buffer, start, end);
			if(ret != -1)
			{
				end = start + ret;
				double fval = val;
				for (int i = start; i < end; i++) {
					buffer[i] += fval;
				}
			}
			return ret;
		}
		public int replace(double[] buffer, int start, int end) {
			int ret = stream.replace(buffer, start, end);
			if(ret != -1)
			{
				end = start + ret;
				double fval = val;
				for (int i = start; i < end; i++) {
					buffer[i] += fval;
				}
			}			
			return ret;
		}
		public int isStatic(double[] buffer, int len) {
			int ret = stream.isStatic(buffer, len);
			if(ret != -1)
			{
				buffer[0] += val;
			}
			return ret;
		}				
		public int skip(int len)
		{
			return -1;
		}
		public void close() {
			stream.close();
		}
		
	}
	
	public AudioStream openStream(AudioSession session) {
		AudioStream stream = openStream2(session);
		double dval = DoublePart.asDouble(getVariable());
		if(Math.abs(dval) > 0.000000000000000000001)
		{
			stream = new DoubleAddStream(stream, dval);
		}
		return stream;
	}
	
	private AudioStream openStream2(AudioSession session) {
		if(!session.isRealTime())
			if(track.size() == 1)
			{
				AudioEvent aevent = (AudioEvent)track.get(0);
				if(aevent.start == 0)
				{
					AudioStream ret = session.openStream(aevent.streamable);
					if(ret == null)
					{
						return new AudioStream()
						{
							public int mix(double[] buffer, int start, int end) {
								return -1;
							}
							public int replace(double[] buffer, int start, int end) {
								return -1;
							}
							public int isStatic(double[] buffer, int len) {
								return -1;
							}					
							public int skip(int len)
							{
								return -1;
							}
							public void close() {
							}
						};
					}
					return ret;
				}
			}
		return new AudioEventsStream(session) ;
	}
	
	
}
