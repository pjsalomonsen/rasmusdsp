/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled;

import java.util.ArrayList;
import java.util.Iterator;

public class AudioVoiceManager {
	
	ArrayList voices;
	
	int maxpoly;
	
	long time = 0;
	
	public long getTime()
	{
		return time++;
	}
	
	public int getMaxPolyphony()
	{
		return maxpoly;
	}
	
	public AudioVoiceManager(int maxpoly)
	{
		this.maxpoly = maxpoly;		
		voices = new ArrayList(maxpoly + 10);
	}
	
	public void addVoice(AudioVoice voice)
	{
		if(voices.size() >= maxpoly)
		{
			
			Iterator itr = voices.iterator();
			AudioVoice found_voice = null;
			long found_offtime = -1;
			while (itr.hasNext()) {
				AudioVoice element = (AudioVoice) itr.next();
				long a_offtime = element.getOffTime();
				if(a_offtime != -1)
				{
					if(found_offtime == -1)
					{
						found_offtime = a_offtime;
						found_voice = element; 
					}
					else
						if(a_offtime < found_offtime)
						{
							found_offtime = a_offtime;
							found_voice = element; 
						}
				}
			}
			if(found_voice != null)
			{
				found_voice.stopNow();
			}
			else
			{
				
				itr = voices.iterator();
				found_voice = null;
				long found_ontime = -1;
				while (itr.hasNext()) {
					AudioVoice element = (AudioVoice) itr.next();
					long a_ontime = element.getOnTime();
					if(a_ontime != -1)
					{
						if(found_ontime == -1)
						{
							found_ontime = a_ontime;
							found_voice = element; 
						}
						else
							if(a_ontime < found_ontime)
							{
								found_ontime = a_ontime;
								found_voice = element; 
							}
					}
				}
				if(found_voice != null)
				{
					found_voice.stopNow();
				}				
				
			}
		}
		
		
		voices.add(voice);
		
		
	}
	public void removeVoice(AudioVoice voice)
	{
		voices.remove(voice);	
	}
}
