/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AudioCache {
	
	public AudioCache() 
	{		
	}
	
	private static final  int buffers3_size = 250000;
	private static final  int buffers2_size = 50000;
	private static final  int buffers1_size = 5000;
	private static final  int buffers0_size = 500;
	/*
	private static final double[][] buffers3 = new double[5][buffers3_size]; 
	private static final  double[][] buffers2 = new double[20][buffers2_size]; 
	private static final  double[][] buffers1 = new double[150][buffers1_size]; 
	private static final  double[][] buffers0 = new double[500][buffers0_size]; 
*/
	private static final double[][] buffers3 = new double[0][buffers3_size]; 
	private static final  double[][] buffers2 = new double[0][buffers2_size]; 
	private static final  double[][] buffers1 = new double[0][buffers1_size]; 
	private static final  double[][] buffers0 = new double[100][buffers0_size]; 
	
	private static final  List<double[]> stack3 = Collections.synchronizedList(new ArrayList<double[]>(buffers3_size));
	private static final  List<double[]> stack2 = Collections.synchronizedList(new ArrayList<double[]>(buffers2_size));
	private static final  List<double[]> stack1 = Collections.synchronizedList(new ArrayList<double[]>(buffers1_size));
	private static final  List<double[]> stack0 = Collections.synchronizedList(new ArrayList<double[]>(buffers0_size));
	
	
	static 
	{
		for (int i = 0; i < buffers3.length; i++) {
			double[] b = buffers3[i];
			stack3.add(b);
		}
		
		for (int i = 0; i < buffers2.length; i++) {
			double[] b = buffers2[i];
			stack2.add(b);
		}
		
		for (int i = 0; i < buffers1.length; i++) {
			double[] b = buffers1[i];
			stack1.add(b);
		}
		
		for (int i = 0; i < buffers0.length; i++) {
			double[] b = buffers0[i];
			stack0.add(b);
		}			
		
		System.gc();
	}
	
	
	private long memusage = 0;
	
	static final boolean debugprintout = false;
	
	public double[] getBuffer(int len)	
	{		
		
		//count++; System.out.println("count = " + count);
		
		if(len <= buffers0_size)
		{
			synchronized (stack0) {
				if(!stack0.isEmpty())
				{
					return stack0.remove(stack0.size()-1);
				}
			}
			memusage += buffers0_size;
			if(debugprintout) System.out.println(System.currentTimeMillis() + " : getBuffer - " + len + " : " + memusage + " <- " + buffers0_size);
			return new double[buffers0_size];
		}
		
		
		if(len <= buffers1_size)
		{				
			synchronized (stack1) {
				if(!stack1.isEmpty())
				{
					return stack1.remove(stack1.size()-1);
				}
			}
			memusage += buffers1_size;
			if(debugprintout) System.out.println(System.currentTimeMillis() + " : getBuffer - " + len + " : " + memusage + " <- " + buffers1_size);
			return new double[buffers1_size];
		}
		
		
		if(len <= buffers2_size)
		{				
			synchronized (stack2) {
				if(!stack2.isEmpty())
				{
					return stack2.remove(stack2.size()-1);
				}
			}
			memusage += buffers2_size;
			if(debugprintout) System.out.println(System.currentTimeMillis() + " : getBuffer - " + len + " : " + memusage + " <- " + buffers2_size);
			return new double[buffers2_size];
		}
		
		
		if(len <= buffers3_size)
		{				
			synchronized (stack3) {
				if(!stack3.isEmpty())
				{
					return stack3.remove(stack3.size()-1);
				}
			}
			memusage += buffers3_size;
			if(debugprintout) System.out.println(System.currentTimeMillis() + " : getBuffer - " + len + " : " + memusage + " <- " + buffers3_size);
			return new double[buffers3_size];
		}
		
		
		if(debugprintout) System.out.println(System.currentTimeMillis() + " : getBuffer - " + len);
		return new double[len];
		
		
		
		
	}
	
	public void returnBuffer(double[] buffer)
	{
		//count--; if(debugprintout) System.out.println("count = " + count);
		
		if(buffer.length == buffers0_size) { synchronized (stack0) { stack0.add(buffer); } return; }
		if(buffer.length == buffers1_size) { synchronized (stack1) { stack1.add(buffer); } return; }
		if(buffer.length == buffers2_size) { synchronized (stack2) { stack2.add(buffer); } return; }
		if(buffer.length == buffers3_size) { synchronized (stack3) { stack3.add(buffer); } return; }
	}
	
}
