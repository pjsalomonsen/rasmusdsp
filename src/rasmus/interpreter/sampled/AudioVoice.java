package rasmus.interpreter.sampled;

public interface AudioVoice {

	public long getOnTime();
	public long getOffTime();
	public void stopNow();
}
