/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.math;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioOperatorInstance extends UnitInstanceAdapter implements AudioStreamable
{
	Variable output;
	Variable input;	
	Variable answer = null;
	Variable input2;
	int opr = 0;
	int oopr = 0;
	Variable oinput;
	Variable oinput2;
	
	
	public static void fastSin_MIX(double[] outbuffer, double[] buffer, int start, int end)
	{
		final double piD2 = Math.PI / 2;
		final double NpiD2 = -piD2;
		final double pi = Math.PI;
		final double pi2 = Math.PI*2;
		final double pi3 = Math.PI*3;		
		for (int i = start; i < end; i++) {	    			    		
			double x = buffer[i];
			x = ((x + pi3) % pi2) - pi;	    	  
			if(x > piD2) x = pi - x; else if(x < NpiD2) x = -pi - x;	  
			double x2 = x*x;	    	  
			outbuffer[i] += x*(x2*(x2*(x2*(x2*(1.0/362880.0)
					- (1.0/5040.0)) + (1.0/120.0))
					- (1.0/6.0)) + 1.0);	    	  	    	  	  		 	  		  		  		   		
		}		  
		
		
	}	
	
	public static void fastSin_REPLACE(double[] outbuffer, double[] buffer, int start, int end)
	{
		
		final double piD2 = Math.PI / 2;
		final double NpiD2 = -piD2;
		final double pi = Math.PI;
		final double pi2 = Math.PI*2;
		final double pi3 = Math.PI*3;		
		for (int i = start; i < end; i++) {	    			    		
			double x = buffer[i];
			x = ((x + pi3) % pi2) - pi;	    	  
			if(x > piD2) x = pi - x; else if(x < NpiD2) x = -pi - x;	  
			double x2 = x*x;	    	  
			outbuffer[i] = x*(x2*(x2*(x2*(x2*(1.0/362880.0)
					- (1.0/5040.0)) + (1.0/120.0))
					- (1.0/6.0)) + 1.0);	    	  	    	  	  		 	  		  		  		   		
		}		  		
	}			
	
	public static void fastCos_MIX(double[] outbuffer, double[] buffer, int start, int end)
	{
		final double piD2 = Math.PI / 2;
		final double NpiD2 = -piD2;
		final double pi = Math.PI;
		final double pi2 = Math.PI*2;
		final double pi3 = Math.PI*3 + piD2;		
		for (int i = start; i < end; i++) {	    			    		
			double x = buffer[i];
			x = ((x + pi3) % pi2) - pi;	    	  
			if(x > piD2) x = pi - x; else if(x < NpiD2) x = -pi - x;	  
			double x2 = x*x;	    	  
			outbuffer[i] += x*(x2*(x2*(x2*(x2*(1.0/362880.0)
					- (1.0/5040.0)) + (1.0/120.0))
					- (1.0/6.0)) + 1.0);	    	  	    	  	  		 	  		  		  		   		
		}		  
		
		
	}	
	
	public static void fastCos_REPLACE(double[] outbuffer, double[] buffer, int start, int end)
	{
		final double piD2 = Math.PI / 2;
		final double NpiD2 = -piD2;
		final double pi = Math.PI;
		final double pi2 = Math.PI*2;
		final double pi3 = Math.PI*3 + piD2;		
		for (int i = start; i < end; i++) {	    			    		
			double x = buffer[i];
			x = ((x + pi3) % pi2) - pi;	    	  
			if(x > piD2) x = pi - x; else if(x < NpiD2) x = -pi - x;	  
			double x2 = x*x;	    	  
			outbuffer[i] = x*(x2*(x2*(x2*(x2*(1.0/362880.0)
					- (1.0/5040.0)) + (1.0/120.0))
					- (1.0/6.0)) + 1.0);	    	  	    	  	  		 	  		  		  		   		
		}		  		
	}				
	
	
	
	public static void fastTan_REPLACE(double[] outbuffer, double[] buffer, int start, int end)
	{
		
		// tan(x) = x + x^3/3 + 2x^5/15 + 17x^7/315 + ...		
		double PI_d2 = Math.PI / 2;
		double PI_d4 = Math.PI / 4;
		for (int i = start; i < end; i++) {	    			    		
			double x = buffer[i];    	
			if(x > PI_d4)
			{
				x = PI_d2 - x;
				double x2 = x*x;      			
				double x4 = x2*x2;
				double x8 = x4*x4;
				double x16 = x8*x8;    		     		 
				outbuffer[i] = 1 / (x*(x2/13 + 2*x4/15 + 2*x16/315));
			}
			else
			{
				double x2 = x*x;
				double x4 = x2*x2;
				double x8 = x4*x4;
				double x16 = x8*x8;    		     		    
				outbuffer[i] = x*(x2/13 + 2*x4/15 + 2*x16/315);
			}
		}		  		
	}			
	
	public static void fastTan_MIX(double[] outbuffer, double[] buffer, int start, int end)
	{
		
		// tan(x) = x + x^3/3 + 2x^5/15 + 17x^7/315 + ...		
		double PI_d2 = Math.PI / 2;
		double PI_d4 = Math.PI / 4;
		for (int i = start; i < end; i++) {	    			    		
			double x = buffer[i];    	
			if(x > PI_d4)
			{
				x = PI_d2 - x;
				double x2 = x*x;      			
				double x4 = x2*x2;
				double x8 = x4*x4;
				double x16 = x8*x8;    		     		 
				outbuffer[i] += 1 / (x*(x2/13 + 2*x4/15 + 2*x16/315));
			}
			else
			{
				double x2 = x*x;
				double x4 = x2*x2;
				double x8 = x4*x4;
				double x16 = x8*x8;    		     		    
				outbuffer[i] += x*(x2/13 + 2*x4/15 + 2*x16/315);
			}
		}		 	
	}		
	
	
	public void recalc()
	{
		/*
		 RVariable bakinput = input2;
		 input2 = input;
		 input = bakinput;
		 */
		input2 = oinput;
		input = oinput2;		
		
		/*		 	
		 
		 (v�xlunarreglur)
		 
		 0 til 99
		 ========
		 (/)  lopr = 1 ,  (\)  lopr = 11;
		 (>)  lopr = 5,   (<)  lopr = 6;
		 (>=) lopr = 7,   (<=) lopr = 8;
		 
		 200 til 299
		 ===========
		 (pow)          lopr=204;   (rpow) lopr=205;
		 (ieeremainder) lopr=200;   (ieeremainder) lopr=206
		 (atan2)        lopr=201;   (atan2) lopr=207
		 
		 */				
		
		if(opr == 1) opr = 11;
		else if(opr == 5) opr = 6;
		else if(opr == 7) opr = 8;
		else if(opr == 204) opr = 205;
		else if(opr == 200) opr = 206;
		else if(opr == 201) opr = 207;		
		else if(opr == 11) opr = 1;
		else if(opr == 6) opr = 5;
		else if(opr == 8) opr = 7;
		else if(opr == 205) opr = 204;
		else if(opr == 206) opr = 200;
		else if(opr == 207) opr = 201;		
		
		if(answer == null)
		{
			answer = AudioEvents.asVariable(new AudioEvent(0, this));		
			output.add(answer);
		}			
		
	}
	
	public void calc()
	{
		input = oinput;
		input2 = oinput2;
		opr = oopr;
		
		if(AudioEvents.getInstance(input).track.size() == 0)
		{
			
			if(input2 != null)
				if(AudioEvents.getInstance(input2).track.size() != 0)
					if((opr >= 0 && opr < 100) || (opr >= 200 && opr < 300))
					{
						recalc();
						return;
					}
			
			
			if(answer != null)
			{
				output.remove(answer);
				answer = null;
			}
		}
		else
		{
			if(answer == null)
			{
				answer = AudioEvents.asVariable(new AudioEvent(0, this));		
				output.add(answer);
			}			
		}
	}
	
	public AudioOperatorInstance(Parameters parameters, int opr)
	{	
		this.opr = opr;
		
		output = parameters.getParameterWithDefault("output");
		
		input = parameters.getParameter("input");
		if(input != null)
		{
			input2 = parameters.getParameterWithDefault(1, "input2");
		}
		else
		{
			input = parameters.getParameterWithDefault(1, "input");
			input2 = parameters.getParameterWithDefault(2, "input2");
		}
		
		oinput = input;
		oinput2 = input2;
		oopr = opr;
		
		/*
		 RFloat.getInstance(gain).addListener(this);
		 calc();*/
		
		AudioEvents.getInstance(input).addListener(this);
		if(input2 != null)
			AudioEvents.getInstance(input2).addListener(this);
		calc();			
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		if(answer != null)
			output.remove(answer);
		
	}
	
	class FilterStreamInstance implements AudioStream
	{
		//double ff_1 = RFloat.asFloat(input);
		double ff_2 = DoublePart.asDouble(input2);
		AudioFallBackStream inputstream;
		AudioFallBackStream inputstream2;
		//double[] stockbuffer = null;
		//double[] stockbuffer2 = null;
		int lopr = opr;
		boolean inputstream2_eof = false;
		AudioCache audiocache;
		public FilterStreamInstance(AudioSession session)
		{				
			audiocache = session.getAudioCache();
			inputstream = new AudioFallBackStream(AudioEvents.openStream(input, session));
			if((opr != 2) && (opr < 100 || opr >= 200))
			{
				if(AudioEvents.getInstance(input2).track.size() == 0)
				{
					inputstream2_eof = true;
				}
				else
					inputstream2 = new AudioFallBackStream(AudioEvents.openStream(input2, session));
			}
			else
			{
				inputstream2_eof = true;
			}
		}
		
		public int isStatic(double[] buffer, int len) {
			
			double[] stockbuffer = new double[1]; 
			double[] stockbuffer2 = new double[1];
			int ret  = inputstream.isStatic(stockbuffer, len);
			if(ret == -1) return -1;			
			if(inputstream2_eof)
			{				
				stockbuffer2[0] = ff_2;
			}
			else
			{
				if(inputstream2 != null)
				{
					int ret2 = inputstream2.isStatic(stockbuffer2, len);
					if(ret2 == -1)
					{
						inputstream.fallBack();
						return -1;
					}
					if(ret2 != ret)
					{
						inputstream.fallBack();
						inputstream2.fallBack();
						return -1;						
					}
				}
			}
			
			int start = 0;
			int end = 1;
			if(lopr == 0)				
				for (int i = start; i < end; i++) {
					buffer[i] = (stockbuffer[i]) * (stockbuffer2[i])  ;
				}
			else if(lopr == 1)
				for (int i = start; i < end; i++) {
					buffer[i] = (stockbuffer[i]) / (stockbuffer2[i])  ;
				}
			else if(lopr == 2)
				for (int i = start; i < end; i++) {
					buffer[i] = -(stockbuffer[i]) ;
				}
			else if(lopr == 3)
				for (int i = start; i < end; i++) {
					buffer[i] = (stockbuffer[i]) + (stockbuffer2[i])  ;
				}
			else if(lopr == 4)
				for (int i = start; i < end; i++) {
					buffer[i] = (stockbuffer[i]) % (stockbuffer2[i])  ;
				}			
			else if(lopr == 5)
				for (int i = start; i < end; i++) {
					if((stockbuffer[i]) > (stockbuffer2[i])) buffer[i] = 1; else buffer[i] = 0;
				}		
			if(lopr == 6)
				for (int i = start; i < end; i++) {
					if((stockbuffer[i]) < (stockbuffer2[i])) buffer[i] = 1; else buffer[i] = 0;
				}			
			if(lopr == 7)
				for (int i = start; i < end; i++) {
					if((stockbuffer[i]) >= (stockbuffer2[i])) buffer[i] = 1; else buffer[i] = 0;
				}				    
			if(lopr == 8)
				for (int i = start; i < end; i++) {
					if((stockbuffer[i]) <= (stockbuffer2[i])) buffer[i] = 1; else buffer[i] = 0;
				}				
			if(lopr == 9)
				for (int i = start; i < end; i++) {
					double d = stockbuffer[i] - stockbuffer2[i];
					if(d > 0.00001 || d < -0.00001) buffer[i] = 1; else buffer[i] = 0;
				}		
			if(lopr == 10)
				for (int i = start; i < end; i++) {
					double d = stockbuffer[i] - stockbuffer2[i];				  	
					if(d < 0.00001 && d > -0.00001) buffer[i] = 1; else buffer[i] = 0;
				}
			else if(lopr == 11)
				for (int i = start; i < end; i++) {
					buffer[i] = (stockbuffer2[i]) / (stockbuffer[i])  ;
				}		    
			else if(lopr == 100)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.abs(stockbuffer[i]) ;
				}			
			else if(lopr == 101)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.acos(stockbuffer[i]) ;
				}			
			else if(lopr == 102)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.asin(stockbuffer[i]) ;
				}			
			else if(lopr == 103)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.atan(stockbuffer[i]) ;
				}			
			else if(lopr == 104)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.ceil(stockbuffer[i]) ;
				}			
			else if(lopr == 105)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.cos(stockbuffer[i]) ;
				}			
			else if(lopr == 106)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.exp(stockbuffer[i]) ;
				}			
			else if(lopr == 107)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.floor(stockbuffer[i]) ;
				}			
			else if(lopr == 108)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.log(stockbuffer[i]) ;
				}			
			else if(lopr == 109)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.rint(stockbuffer[i]) ;
				}			
			else if(lopr == 110)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.round(stockbuffer[i]) ;
				}			
			else if(lopr == 111)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.sin(stockbuffer[i]) ;
				}			
			else if(lopr == 112)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.sqrt(stockbuffer[i]) ;
				}			
			else if(lopr == 113)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.tan(stockbuffer[i]) ;
				}			
			else if(lopr == 114)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.toDegrees(stockbuffer[i]) ;
				}			
			else if(lopr == 115)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.toRadians(stockbuffer[i]) ;
				}				
			else if(lopr == 116)
				for (int i = start; i < end; i++) {
					double calc = stockbuffer[i];
					if(calc > 0) calc = 1; else if(calc < 0) calc = -1; else calc = 0;
					buffer[i] = calc;
				}				
			else if(lopr == 151)				  
				fastSin_REPLACE(buffer, stockbuffer, start, end) ;
			else if(lopr == 152)				  
				fastCos_REPLACE(buffer, stockbuffer, start, end) ;
			else if(lopr == 153)				  
				fastTan_REPLACE(buffer, stockbuffer, start, end) ;
			else if(lopr == 200)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.IEEEremainder((stockbuffer[i]) , (stockbuffer2[i]))  ;
				}					
			else if(lopr == 201)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.atan2((stockbuffer[i]) , (stockbuffer2[i]))  ;
				}					
			else if(lopr == 202)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.min((stockbuffer[i]) , (stockbuffer2[i]))  ;
				}					
			else if(lopr == 203)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.max((stockbuffer[i]) , (stockbuffer2[i]))  ;
				}			
			else if(lopr == 204)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.pow((stockbuffer[i]) , (stockbuffer2[i]))  ;
				}		
			else if(lopr == 205)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.pow((stockbuffer2[i]) , (stockbuffer[i]))  ;
				}		
			else if(lopr == 206)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.IEEEremainder((stockbuffer2[i]) , (stockbuffer[i]))  ;
				}					
			else if(lopr == 207)
				for (int i = start; i < end; i++) {
					buffer[i] = Math.atan2((stockbuffer2[i]) , (stockbuffer[i]))  ;
				}				
			
			
			return ret;
		}		
		
		
		public int skip(int len)
		{
			int ret = inputstream.skip(len);
			if(inputstream2 != null)
				if(!inputstream2_eof)
				{
					int ret2 = inputstream2.skip(len);
					if(ret2 == -1) inputstream2_eof = true;
				}
			return ret;
		}
		
		public int mix(double[] buffer, int start, int end) {
			
			//double f_1 = ff_1;
			double f_2 = ff_2;
			
			/*
			 if(stockbuffer == null) stockbuffer = new double[buffer.length];
			 else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length]; */		
			double[] stockbuffer = audiocache.getBuffer(end);
			int ret  = inputstream.replace(stockbuffer, start, end);
			if(ret == -1)
			{
				if(stockbuffer != null) audiocache.returnBuffer(stockbuffer);
				return -1;
			}
			
			boolean onlyvar = false;
			
			double[] stockbuffer2 = null;
			if(inputstream2_eof)
			{
				onlyvar = true;
			}
			else      			
				if(inputstream2 != null)
				{
					/*
					 if(stockbuffer2 == null) 
					 { 
					 
					 stockbuffer2 = new double[buffer.length];
					 }
					 else if(stockbuffer2.length < buffer.length)
					 {
					 stockbuffer2 = new double[buffer.length];
					 if(inputstream2_eof)
					 {
					 Arrays.fill(stockbuffer2, f_2);
					 }
					 }*/
					
					if(!inputstream2_eof)
					{
						stockbuffer2 = audiocache.getBuffer(end);
						int ret2 = inputstream2.replace(stockbuffer2, start, end);
						if(ret2 == -1)
						{
							//Arrays.fill(stockbuffer2, f_2);
							inputstream2_eof = true;
							onlyvar = true;
						}
						else
						{
							Arrays.fill(stockbuffer2, start + ret2, end, f_2);
						}
						
					}
				}
			
			
			end = start + ret;
			
			if(onlyvar)
			{
				
				if(lopr == 0)	
				{   
					if(f_2 == 0)
					{
						if(stockbuffer != null) audiocache.returnBuffer(stockbuffer);
						return -1;
					}
					else 
						for (int i = start; i < end; i++) {
							buffer[i] += (stockbuffer[i]) * (f_2)  ;
						}
				}
				else if(lopr == 1)
					for (int i = start; i < end; i++) {
						buffer[i] += (stockbuffer[i]) / (f_2)  ;
					}
				else if(lopr == 2)
					for (int i = start; i < end; i++) {
						buffer[i] -= (stockbuffer[i]) ;
					}
				else if(lopr == 3)
					for (int i = start; i < end; i++) {
						buffer[i] += (stockbuffer[i]) + (f_2)  ;
					}
				else if(lopr == 4)
					for (int i = start; i < end; i++) {
						buffer[i] += (stockbuffer[i]) % (f_2)  ;
					}			
				else if(lopr == 5)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) > (f_2)) buffer[i] += 1;
					}		
				if(lopr == 6)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) < (f_2)) buffer[i] += 1;
					}			
				if(lopr == 7)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) >= (f_2)) buffer[i] += 1;
					}				    
				if(lopr == 8)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) <= (f_2)) buffer[i] += 1;
					}				
				if(lopr == 9)
					for (int i = start; i < end; i++) {
						double d = stockbuffer[i] - f_2;
						if(d > 0.00001 || d < -0.00001) buffer[i] += 1;
					}		
				if(lopr == 10)
					for (int i = start; i < end; i++) {
						double d = stockbuffer[i] - f_2;				  	
						if(d < 0.00001 && d > -0.00001) buffer[i] += 1;
					}				
				else if(lopr == 11)
					for (int i = start; i < end; i++) {
						buffer[i] += (f_2) / (stockbuffer[i])   ;
					}				    
				else if(lopr == 100)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.abs(stockbuffer[i]) ;
					}			
				else if(lopr == 101)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.acos(stockbuffer[i]) ;						     
					}			
				else if(lopr == 102)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.asin(stockbuffer[i]) ;
					}			
				else if(lopr == 103)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.atan(stockbuffer[i]) ;
					}			
				else if(lopr == 104)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.ceil(stockbuffer[i]) ;
					}			
				else if(lopr == 105)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.cos(stockbuffer[i]) ;
					}			
				else if(lopr == 106)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.exp(stockbuffer[i]) ;
					}			
				else if(lopr == 107)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.floor(stockbuffer[i]) ;						     
					}			
				else if(lopr == 108)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.log(stockbuffer[i]) ;
					}			
				else if(lopr == 109)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.rint(stockbuffer[i]) ;
					}			
				else if(lopr == 110)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.round(stockbuffer[i]) ;
					}			
				else if(lopr == 111)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.sin(stockbuffer[i]) ;
					}			
				else if(lopr == 112)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.sqrt(stockbuffer[i]) ;
					}			
				else if(lopr == 113)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.tan(stockbuffer[i]) ;
					}			
				else if(lopr == 114)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.toDegrees(stockbuffer[i]) ;
					}			
				else if(lopr == 115)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.toRadians(stockbuffer[i]) ;
					}			
				else if(lopr == 116)
					for (int i = start; i < end; i++) {
						double calc = stockbuffer[i];
						if(calc > 0) calc = 1; else if(calc < 0) calc = -1; else calc = 0;
						buffer[i] += calc;
					}				
				else if(lopr == 151)				  
					fastSin_MIX(buffer, stockbuffer, start, end) ;
				else if(lopr == 151)				  
					fastCos_MIX(buffer, stockbuffer, start, end) ;
				else if(lopr == 151)				  
					fastTan_MIX(buffer, stockbuffer, start, end) ;
				else if(lopr == 200)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.IEEEremainder((stockbuffer[i]) , (f_2))  ;
					}					
				else if(lopr == 201)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.atan2((stockbuffer[i]) , (f_2))  ;
					}					
				else if(lopr == 202)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.min((stockbuffer[i]) , (f_2))  ;
					}					
				else if(lopr == 203)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.max((stockbuffer[i]) , (f_2))  ;
					}			
				else if(lopr == 204)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.pow((stockbuffer[i]) , (f_2))  ;
					}			
				else if(lopr == 205)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.pow((f_2) , (stockbuffer[i]))  ;
					}		
				else if(lopr == 206)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.IEEEremainder((f_2) , (stockbuffer[i]))  ;
					}					
				else if(lopr == 207)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.atan2((f_2) , (stockbuffer[i]))  ;
					}								
				
			}
			else
			{
				if(lopr == 0)				
					for (int i = start; i < end; i++) {
						buffer[i] += (stockbuffer[i]) * (stockbuffer2[i])  ;
					}
				else if(lopr == 1)
					for (int i = start; i < end; i++) {
						buffer[i] += (stockbuffer[i]) / (stockbuffer2[i])  ;
					}
				else if(lopr == 2)
					for (int i = start; i < end; i++) {
						buffer[i] -= (stockbuffer[i]) ;
					}
				else if(lopr == 3)
					for (int i = start; i < end; i++) {
						buffer[i] += (stockbuffer[i]) + (stockbuffer2[i])  ;
					}
				else if(lopr == 4)
					for (int i = start; i < end; i++) {
						buffer[i] += (stockbuffer[i]) % (stockbuffer2[i])  ;
					}			
				else if(lopr == 5)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) > (stockbuffer2[i])) buffer[i] += 1;
					}		
				if(lopr == 6)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) < (stockbuffer2[i])) buffer[i] += 1;
					}			
				if(lopr == 7)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) >= (stockbuffer2[i])) buffer[i] += 1;
					}				    
				if(lopr == 8)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) <= (stockbuffer2[i])) buffer[i] += 1;
					}				
				if(lopr == 9)
					for (int i = start; i < end; i++) {
						double d = stockbuffer[i] - stockbuffer2[i];
						if(d > 0.00001 || d < -0.00001) buffer[i] += 1;
					}		
				if(lopr == 10)
					for (int i = start; i < end; i++) {
						double d = stockbuffer[i] - stockbuffer2[i];				  	
						if(d < 0.00001 && d > -0.00001) buffer[i] += 1;
					}
				else if(lopr == 11)
					for (int i = start; i < end; i++) {
						buffer[i] += (stockbuffer2[i]) / (stockbuffer[i])  ;
					}		    	
				else if(lopr == 100)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.abs(stockbuffer[i]) ;
					}			
				else if(lopr == 101)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.acos(stockbuffer[i]) ;
					}			
				else if(lopr == 102)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.asin(stockbuffer[i]) ;
					}			
				else if(lopr == 103)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.atan(stockbuffer[i]) ;
					}			
				else if(lopr == 104)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.ceil(stockbuffer[i]) ;
					}			
				else if(lopr == 105)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.cos(stockbuffer[i]) ;
					}			
				else if(lopr == 106)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.exp(stockbuffer[i]) ;
					}			
				else if(lopr == 107)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.floor(stockbuffer[i]) ;
					}			
				else if(lopr == 108)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.log(stockbuffer[i]) ;
					}			
				else if(lopr == 109)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.rint(stockbuffer[i]) ;
					}			
				else if(lopr == 110)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.round(stockbuffer[i]) ;
					}			
				else if(lopr == 111)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.sin(stockbuffer[i]) ;
					}			
				else if(lopr == 112)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.sqrt(stockbuffer[i]) ;
					}			
				else if(lopr == 113)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.tan(stockbuffer[i]) ;
					}			
				else if(lopr == 114)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.toDegrees(stockbuffer[i]) ;
					}			
				else if(lopr == 115)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.toRadians(stockbuffer[i]) ;
					}			
				else if(lopr == 116)
					for (int i = start; i < end; i++) {
						double calc = stockbuffer[i];
						if(calc > 0) calc = 1; else if(calc < 0) calc = -1; else calc = 0;
						buffer[i] += calc;
					}								
				else if(lopr == 151)				  
					fastSin_MIX(buffer, stockbuffer, start, end) ;
				else if(lopr == 151)				  
					fastCos_MIX(buffer, stockbuffer, start, end) ;
				else if(lopr == 151)				  
					fastTan_MIX(buffer, stockbuffer, start, end) ;
				else if(lopr == 200)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.IEEEremainder((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}					
				else if(lopr == 201)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.atan2((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}					
				else if(lopr == 202)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.min((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}					
				else if(lopr == 203)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.max((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}			
				else if(lopr == 204)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.pow((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}			
				else if(lopr == 205)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.pow((stockbuffer2[i]) , (stockbuffer[i]))  ;
					}		
				else if(lopr == 206)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.IEEEremainder((stockbuffer2[i]) , (stockbuffer[i]))  ;
					}					
				else if(lopr == 207)
					for (int i = start; i < end; i++) {
						buffer[i] += Math.atan2((stockbuffer2[i]) , (stockbuffer[i]))  ;
					}								
			}
			
			if(stockbuffer != null) audiocache.returnBuffer(stockbuffer);
			if(stockbuffer2 != null) audiocache.returnBuffer(stockbuffer2);
			
			return ret;
		}
		public int replace(double[] buffer, int start, int end) {
			/*
			 *  VER�UR SKIPT �T �EGAR AudioOperator hefur veri� n�gilega pr�fa�ur
			 */
			/*			Arrays.fill(buffer, start, end, 0);
			 return mix(buffer, start, end);*/
			
			//Arrays.fill(buffer, start, end, 0);
			
			
			/* GENERATING FROM MIX PART */
			
			//double f_1 = ff_1;
			double f_2 = ff_2;
			double[] stockbuffer = buffer;
			/*
			 if(stockbuffer == null) stockbuffer = new double[buffer.length];
			 else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];*/		
			int ret  = inputstream.replace(stockbuffer, start, end);
			if(ret == -1)
			{
				return ret;
			}
			boolean onlyvar = false;
			double[] stockbuffer2 = null;
			
			if(inputstream2_eof)
			{
				onlyvar = true;
			}
			else            
				if(inputstream2 != null)
				{
					/*
					 if(stockbuffer2 == null) 
					 {
					 stockbuffer2 = new double[buffer.length];
					 }
					 else if(stockbuffer2.length < buffer.length)
					 {
					 stockbuffer2 = new double[buffer.length];
					 if(inputstream2_eof)
					 {
					 Arrays.fill(stockbuffer2, f_2);
					 }
					 }*/
					
					if(!inputstream2_eof)
					{
						stockbuffer2 = audiocache.getBuffer(end);
						int ret2 = inputstream2.replace(stockbuffer2, start, end);
						if(ret2 == -1)
						{
							//Arrays.fill(stockbuffer2, f_2);
							inputstream2_eof = true;
							onlyvar = true;
						}
						else
						{
							Arrays.fill(stockbuffer2, start + ret2, end, f_2);
						}
					}				
				}
			
			end = start + ret;
			
			if(onlyvar)
			{
				if(lopr == 0)				
				{  
					if(f_2 == 0)
					{
						if(stockbuffer2 != null) audiocache.returnBuffer(stockbuffer2);
						return -1;
					}
					else					
						for (int i = start; i < end; i++) {
							buffer[i] = (stockbuffer[i]) * (f_2)  ;
						}
				}
				else if(lopr == 1)
					for (int i = start; i < end; i++) {
						buffer[i] = (stockbuffer[i]) / (f_2)  ;
					}
				else if(lopr == 2)
					for (int i = start; i < end; i++) {
						buffer[i] = -(stockbuffer[i]) ;
					}
				else if(lopr == 3)
					for (int i = start; i < end; i++) {
						buffer[i] = (stockbuffer[i]) + (f_2)  ;
					}
				else if(lopr == 4)
					for (int i = start; i < end; i++) {
						buffer[i] = (stockbuffer[i]) % (f_2)  ;
					}			
				else if(lopr == 5)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) > (f_2)) buffer[i] = 1; else buffer[i] = 0;
					}		
				if(lopr == 6)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) < (f_2)) buffer[i] = 1; else buffer[i] = 0;
					}			
				if(lopr == 7)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) >= (f_2)) buffer[i] = 1; else buffer[i] = 0;
					}				    
				if(lopr == 8)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) <= (f_2)) buffer[i] = 1; else buffer[i] = 0;
					}				
				if(lopr == 9)
					for (int i = start; i < end; i++) {
						double d = stockbuffer[i] - f_2;
						if(d > 0.00001 || d < -0.00001) buffer[i] = 1; else buffer[i] = 0;
					}		
				if(lopr == 10)
					for (int i = start; i < end; i++) {
						double d = stockbuffer[i] - f_2;				  	
						if(d < 0.00001 && d > -0.00001) buffer[i] = 1; else buffer[i] = 0;
					}				
				else if(lopr == 11)
					for (int i = start; i < end; i++) {
						buffer[i] = (f_2) / (stockbuffer[i])  ;
					}				    
				else if(lopr == 100)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.abs(stockbuffer[i]) ;
					}			
				else if(lopr == 101)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.acos(stockbuffer[i]) ;
					}			
				else if(lopr == 102)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.asin(stockbuffer[i]) ;
					}			
				else if(lopr == 103)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.atan(stockbuffer[i]) ;
					}			
				else if(lopr == 104)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.ceil(stockbuffer[i]) ;
					}			
				else if(lopr == 105)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.cos(stockbuffer[i]) ;
					}			
				else if(lopr == 106)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.exp(stockbuffer[i]) ;
					}			
				else if(lopr == 107)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.floor(stockbuffer[i]) ;
					}			
				else if(lopr == 108)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.log(stockbuffer[i]) ;
					}			
				else if(lopr == 109)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.rint(stockbuffer[i]) ;
					}			
				else if(lopr == 110)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.round(stockbuffer[i]) ;
					}			
				else if(lopr == 111)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.sin(stockbuffer[i]) ;
					}			
				else if(lopr == 112)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.sqrt(stockbuffer[i]) ;
					}			
				else if(lopr == 113)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.tan(stockbuffer[i]) ;
					}			
				else if(lopr == 114)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.toDegrees(stockbuffer[i]) ;
					}			
				else if(lopr == 115)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.toRadians(stockbuffer[i]) ;
					}			
				else if(lopr == 116)
					for (int i = start; i < end; i++) {
						double calc = stockbuffer[i];
						if(calc > 0) calc = 1; else if(calc < 0) calc = -1; else calc = 0;
						buffer[i] = calc;
					}								
				else if(lopr == 151)				  
					fastSin_REPLACE(buffer, stockbuffer, start, end) ;
				else if(lopr == 152)				  
					fastCos_REPLACE(buffer, stockbuffer, start, end) ;
				else if(lopr == 153)				  
					fastTan_REPLACE(buffer, stockbuffer, start, end) ;
				else if(lopr == 200)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.IEEEremainder((stockbuffer[i]) , (f_2))  ;
					}					
				else if(lopr == 201)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.atan2((stockbuffer[i]) , (f_2))  ;
					}					
				else if(lopr == 202)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.min((stockbuffer[i]) , (f_2))  ;
					}					
				else if(lopr == 203)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.max((stockbuffer[i]) , (f_2))  ;
					}			
				else if(lopr == 204)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.pow((stockbuffer[i]) , (f_2))  ;
					}			
				else if(lopr == 205)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.pow(((f_2)) , (stockbuffer[i]))  ;
					}		
				else if(lopr == 206)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.IEEEremainder(((f_2)) , (stockbuffer[i]))  ;
					}					
				else if(lopr == 207)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.atan2(((f_2)) , (stockbuffer[i]))  ;
					}						
			}
			else
			{
				
				if(lopr == 0)				
					for (int i = start; i < end; i++) {
						buffer[i] = (stockbuffer[i]) * (stockbuffer2[i])  ;
					}
				else if(lopr == 1)
					for (int i = start; i < end; i++) {
						buffer[i] = (stockbuffer[i]) / (stockbuffer2[i])  ;
					}
				else if(lopr == 2)
					for (int i = start; i < end; i++) {
						buffer[i] = -(stockbuffer[i]) ;
					}
				else if(lopr == 3)
					for (int i = start; i < end; i++) {
						buffer[i] = (stockbuffer[i]) + (stockbuffer2[i])  ;
					}
				else if(lopr == 4)
					for (int i = start; i < end; i++) {
						buffer[i] = (stockbuffer[i]) % (stockbuffer2[i])  ;
					}			
				else if(lopr == 5)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) > (stockbuffer2[i])) buffer[i] = 1; else buffer[i] = 0;
					}		
				if(lopr == 6)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) < (stockbuffer2[i])) buffer[i] = 1; else buffer[i] = 0;
					}			
				if(lopr == 7)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) >= (stockbuffer2[i])) buffer[i] = 1; else buffer[i] = 0;
					}				    
				if(lopr == 8)
					for (int i = start; i < end; i++) {
						if((stockbuffer[i]) <= (stockbuffer2[i])) buffer[i] = 1; else buffer[i] = 0;
					}				
				if(lopr == 9)
					for (int i = start; i < end; i++) {
						double d = stockbuffer[i] - stockbuffer2[i];
						if(d > 0.00001 || d < -0.00001) buffer[i] = 1; else buffer[i] = 0;
					}		
				if(lopr == 10)
					for (int i = start; i < end; i++) {
						double d = stockbuffer[i] - stockbuffer2[i];				  	
						if(d < 0.00001 && d > -0.00001) buffer[i] = 1; else buffer[i] = 0;
					}
				else if(lopr == 11)
					for (int i = start; i < end; i++) {
						buffer[i] = (stockbuffer2[i]) / (stockbuffer[i])  ;
					}		    
				else if(lopr == 100)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.abs(stockbuffer[i]) ;
					}			
				else if(lopr == 101)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.acos(stockbuffer[i]) ;
					}			
				else if(lopr == 102)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.asin(stockbuffer[i]) ;
					}			
				else if(lopr == 103)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.atan(stockbuffer[i]) ;
					}			
				else if(lopr == 104)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.ceil(stockbuffer[i]) ;
					}			
				else if(lopr == 105)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.cos(stockbuffer[i]) ;
					}			
				else if(lopr == 106)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.exp(stockbuffer[i]) ;
					}			
				else if(lopr == 107)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.floor(stockbuffer[i]) ;
					}			
				else if(lopr == 108)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.log(stockbuffer[i]) ;
					}			
				else if(lopr == 109)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.rint(stockbuffer[i]) ;
					}			
				else if(lopr == 110)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.round(stockbuffer[i]) ;
					}			
				else if(lopr == 111)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.sin(stockbuffer[i]) ;
					}			
				else if(lopr == 112)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.sqrt(stockbuffer[i]) ;
					}			
				else if(lopr == 113)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.tan(stockbuffer[i]) ;
					}			
				else if(lopr == 114)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.toDegrees(stockbuffer[i]) ;
					}			
				else if(lopr == 115)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.toRadians(stockbuffer[i]) ;
					}				
				else if(lopr == 116)
					for (int i = start; i < end; i++) {
						double calc = stockbuffer[i];
						if(calc > 0) calc = 1; else if(calc < 0) calc = -1; else calc = 0;
						buffer[i] = calc;
					}								
				else if(lopr == 151)				  
					fastSin_REPLACE(buffer, stockbuffer, start, end) ;
				else if(lopr == 151)				  
					fastCos_REPLACE(buffer, stockbuffer, start, end) ;
				else if(lopr == 152)				  
					fastTan_REPLACE(buffer, stockbuffer, start, end) ;
				else if(lopr == 200)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.IEEEremainder((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}					
				else if(lopr == 201)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.atan2((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}					
				else if(lopr == 202)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.min((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}					
				else if(lopr == 203)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.max((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}			
				else if(lopr == 204)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.pow((stockbuffer[i]) , (stockbuffer2[i]))  ;
					}		
				else if(lopr == 205)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.pow((stockbuffer2[i]) , (stockbuffer[i]))  ;
					}		
				else if(lopr == 206)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.IEEEremainder((stockbuffer2[i]) , (stockbuffer[i]))  ;
					}					
				else if(lopr == 207)
					for (int i = start; i < end; i++) {
						buffer[i] = Math.atan2((stockbuffer2[i]) , (stockbuffer[i]))  ;
					}				
				
			}
			
			if(stockbuffer2 != null) audiocache.returnBuffer(stockbuffer2);
			
			return ret;
			
		}
		public void close() {
			inputstream.close();
			if(inputstream2 != null)
			{
				inputstream2.close();
				inputstream2 = null;
			}
			
			inputstream = null;			
			
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioOperator implements UnitFactory {
	
	int operator;
	public AudioOperator(int operator)
	{
		this.operator = operator;
	}	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioOperatorInstance(parameters, operator);
	}
}