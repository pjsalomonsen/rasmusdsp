/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.math;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioGain2Instance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	public Variable input;	
	Variable answer = null;
	Variable input2;
	Variable input3;
	int opr = 0;
	
	public void calc()
	{
		if(AudioEvents.getInstance(input).track.size() == 0)
		{
			if(answer != null)
			{
				output.remove(answer);
				answer = null;
			}
		}
		else
		{
			if(answer == null)
			{
				answer = AudioEvents.asVariable(new AudioEvent(0, this));		
				output.add(answer);
			}			
		}		
	}
	
	public AudioGain2Instance(Parameters parameters, int opr)
	{
		this.opr = opr;
		output = parameters.getParameterWithDefault("output");
		
		input = (Variable)parameters.getParameter("input");
		if(input != null)
		{
			input2 = parameters.getParameterWithDefault(1, "input2");
			input3 = parameters.getParameterWithDefault(2, "input3");
		}
		else
		{
			input = parameters.getParameterWithDefault(1, "input");
			input2 = parameters.getParameterWithDefault(2, "input2");
			input3 = parameters.getParameterWithDefault(3, "input3");
		}
		/*
		 RFloat.getInstance(gain).addListener(this);
		 calc();*/
		/*
		 answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		 output.add(answer);		*/
		AudioEvents.getInstance(input).addListener(this);        
		calc();
	}
	public void close() {
		//RFloat.getInstance(gain).removeListener(this);
		if(answer != null)
			output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		//double ff_1 = RFloat.asFloat(input);
		double ff_2 = DoublePart.asDouble(input2);
		double ff_3 = DoublePart.asDouble(input3);
		AudioFallBackStream inputstream;
		AudioFallBackStream inputstream2;
		AudioFallBackStream inputstream3;
		double[] stockbuffer = null;
		double[] stockbuffer2 = null;
		double[] stockbuffer3 = null;
		int lopr = opr;
		boolean inputstream2_eof = false;
		boolean inputstream3_eof = false;
		public FilterStreamInstance(AudioSession session)
		{			
			inputstream = new AudioFallBackStream(AudioEvents.openStream(input, session));
			inputstream2 = new AudioFallBackStream(AudioEvents.openStream(input2, session));
			inputstream3 = new AudioFallBackStream(AudioEvents.openStream(input3, session));
		}
		public int mix(double[] buffer, int start, int end) {
			
			//double f_1 = ff_1;
			double f_2 = ff_2;
			double f_3 = ff_3;
			
			if(stockbuffer == null) stockbuffer = new double[buffer.length];
			else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];		
			int ret  = inputstream.replace(stockbuffer, start, end);
			
			if(inputstream2 != null)
			{
				if(stockbuffer2 == null) stockbuffer2 = new double[buffer.length];
				else if(stockbuffer2.length < buffer.length)
				{
					stockbuffer2 = new double[buffer.length];
					if(inputstream2_eof)
					{
						Arrays.fill(stockbuffer2, f_2);
					}
				}
				
				if(!inputstream2_eof)
				{
					int ret2 = inputstream2.replace(stockbuffer2, start, end);
					if(ret2 == -1)
					{
						Arrays.fill(stockbuffer2, f_2);
						inputstream2_eof = true;
					}
					else
					{
						Arrays.fill(stockbuffer2, start + ret2, end, f_2);
					}
				}
			}
			
			if(inputstream3 != null)
			{
				if(stockbuffer3 == null) stockbuffer3 = new double[buffer.length];
				else if(stockbuffer3.length < buffer.length)
				{
					stockbuffer3 = new double[buffer.length];
					if(inputstream3_eof)
					{
						Arrays.fill(stockbuffer3, f_3);
					}
				}
				
				if(!inputstream3_eof)
				{
					int ret3 = inputstream3.replace(stockbuffer3, start, end);
					if(ret3 == -1)
					{
						Arrays.fill(stockbuffer3, f_3);
						inputstream3_eof = true;
					}
					else
					{
						Arrays.fill(stockbuffer3, start + ret3, end, f_3);
					}
				}
			}			
			
			end = start + ret;
			
			if(lopr == 0)				
				for (int i = start; i < end; i++) {
					
					buffer[i] += (stockbuffer[i] > 0.5) ? (stockbuffer2[i]) : (stockbuffer3[i]) ;
				}
			else if(lopr == 1)
				for (int i = start; i < end; i++) {			  	
					double b = stockbuffer[i];
					double min = stockbuffer2[i];
					double max = stockbuffer3[i];
					if(b > max) b = max; else if(b < min) b = min;			  	
					buffer[i] += b ;
				}
			
			return ret;
		}
		
		public int skip(int len)
		{
			int ret = inputstream.skip(len);
			if(inputstream2 != null)
				if(!inputstream2_eof)
				{
					int ret2 = inputstream2.skip(len);
					if(ret2 == -1) inputstream2_eof = true;
				}
			if(inputstream3 != null)
				if(!inputstream3_eof)
				{
					int ret3 = inputstream3.skip(len);
					if(ret3 == -1) inputstream3_eof = true;
				}
			return ret;
		}
		
		public int replace(double[] buffer, int start, int end) {
			
			//double f_1 = ff_1;
			double f_2 = ff_2;
			double f_3 = ff_3;
			
			if(stockbuffer == null) stockbuffer = new double[buffer.length];
			else if(stockbuffer.length < buffer.length) stockbuffer = new double[buffer.length];		
			int ret  = inputstream.replace(stockbuffer, start, end);
			
			if(inputstream2 != null)
			{
				if(stockbuffer2 == null) stockbuffer2 = new double[buffer.length];
				else if(stockbuffer2.length < buffer.length)
				{
					stockbuffer2 = new double[buffer.length];
					if(inputstream2_eof)
					{
						Arrays.fill(stockbuffer2, f_2);
					}
				}
				
				if(!inputstream2_eof)
				{
					int ret2 = inputstream2.replace(stockbuffer2, start, end);
					if(ret2 == -1)
					{
						Arrays.fill(stockbuffer2, f_2);
						inputstream2_eof = true;
					}
					else
					{
						Arrays.fill(stockbuffer2, start + ret2, end, f_2);
					}
				}
			}
			
			if(inputstream3 != null)
			{
				if(stockbuffer3 == null) stockbuffer3 = new double[buffer.length];
				else if(stockbuffer3.length < buffer.length)
				{
					stockbuffer3 = new double[buffer.length];
					if(inputstream3_eof)
					{
						Arrays.fill(stockbuffer3, f_3);
					}
				}
				
				if(!inputstream3_eof)
				{
					int ret3 = inputstream3.replace(stockbuffer3, start, end);
					if(ret3 == -1)
					{
						Arrays.fill(stockbuffer3, f_3);
						inputstream3_eof = true;
					}
					else
					{
						Arrays.fill(stockbuffer3, start + ret3, end, f_3);
					}
				}
			}			
			
			end = start + ret;
			
			if(lopr == 0)				
				for (int i = start; i < end; i++) {
					
					buffer[i] = (stockbuffer[i] > 0.5) ? (stockbuffer2[i]) : (stockbuffer3[i]) ;
				}
			else if(lopr == 1)
				for (int i = start; i < end; i++) {			  	
					double b = stockbuffer[i];
					double min = stockbuffer2[i];
					double max = stockbuffer3[i];
					if(b > max) b = max; else if(b < min) b = min;			  	
					buffer[i] = b ;
				}
			
			return ret;
		}
		public int isStatic(double[] buffer, int len) {
			if(lopr == 1)
			{
				double[] stockbuffer = new double[1];
				int ret  = inputstream.isStatic(stockbuffer, len);
				if(ret == -1) return -1;
				
				double[] stockbuffer2 = new double[1];
				int sret  = inputstream2.isStatic(stockbuffer2, len);
				if(ret == -1)
				{
					inputstream.fallBack();
					return -1;
				}
				if(ret != sret)
				{
					inputstream.fallBack();
					inputstream2.fallBack();
					return -1;
				}
				
				double[] stockbuffer3 = new double[1];
				sret  = inputstream3.isStatic(stockbuffer3, len);
				if(ret == -1)
				{
					inputstream.fallBack();
					inputstream2.fallBack();
					return -1;
				}
				if(ret != sret)
				{
					inputstream.fallBack();
					inputstream2.fallBack();
					inputstream3.fallBack();
					return -1;
				}					
				
				double b = stockbuffer[0];
				double min = stockbuffer2[0];
				double max = stockbuffer3[0];
				if(b > max) b = max; else if(b < min) b = min;			  	
				buffer[0] = b ;
				
				return ret;
				
			}
			if(lopr == 0) 
			{
				
				double[] stockbuffer = new double[1];
				int ret  = inputstream.isStatic(stockbuffer, len);
				if(ret == -1) return -1;
				
				if(stockbuffer[1] > 0.5)
				{
					double[] stockbuffer2 = new double[1];
					int sret  = inputstream2.isStatic(stockbuffer2, len);
					if(ret == -1)
					{
						inputstream.fallBack();
						return -1;
					}
					if(ret != sret)
					{
						inputstream.fallBack();
						inputstream2.fallBack();
						return -1;
					}				
					buffer[0] = stockbuffer2[0];
				}
				else
				{
					double[] stockbuffer2 = new double[1];
					int sret  = inputstream3.isStatic(stockbuffer, len);
					if(ret == -1)
					{
						inputstream.fallBack();
						return -1;
					}
					if(ret != sret)
					{
						inputstream.fallBack();
						inputstream3.fallBack();
						return -1;
					}				
					buffer[0] = stockbuffer2[0];
				}					
				return ret;					
				
			}					
			return -1;
		}				
		public void close() {
			inputstream.close();
			if(inputstream2 != null)
				inputstream2.close();
			if(inputstream3 != null)
				inputstream3.close();
			
			inputstream = null;
			inputstream2 = null;
			inputstream3 = null;
			
		}
		
	}
	
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioOperator2 implements UnitFactory {
	
	int operator;
	public AudioOperator2(int operator)
	{
		this.operator = operator;
	}	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioGain2Instance(parameters, operator);
	}
}