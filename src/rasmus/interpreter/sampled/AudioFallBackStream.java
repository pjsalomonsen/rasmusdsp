/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled;

import java.util.Arrays;

/*
 *  A AudioStream helper class, can fallback read if last one was STATIC
 *  and has a extra function: isSilent(len) 
 *
 */
public class AudioFallBackStream implements AudioStream {
	
	public AudioStream audiostream;
	public AudioFallBackStream(AudioStream audiostream)
	{
		if(audiostream == null)
		{
			System.out.println("null error");
		}
		this.audiostream = audiostream;
	}
	public AudioStream getAudioStream()
	{
		return audiostream;
	}
	boolean fallbackable = false;
	boolean fallbackmode = false;
	double fallbackvalue = 0;
	int fallbackret = -1;
	
	public int skip(int len) {
		fallbackable = false;
		if(fallbackmode)
		{
			fallbackmode = false;
			return fallbackret;
		}
		else		
			return audiostream.skip(len);		
	}	
	
	public int mix(double[] buffer, int start, int end) {
		fallbackable = false;
		if(fallbackmode)
		{
			fallbackmode = false;
			if(end - start > fallbackret) end = fallbackret + start;			
			double fv = fallbackvalue;
			for (int i = start; i < end; i++) {
				buffer[i] += fv;
			}
			return fallbackret;
		}
		else		
			return audiostream.mix(buffer,start,end);		
	}
	public int replace(double[] buffer, int start, int end) {
		fallbackable = false;
		if(fallbackmode)
		{
			fallbackmode = false;
			if(end - start > fallbackret) end = fallbackret + start;
			Arrays.fill(buffer,start,end,fallbackvalue);
			return fallbackret;
		}
		else
			return audiostream.replace(buffer,start,end);
	}
	public int isStatic(double[] buffer, int len) {
		if(fallbackmode) return -1;
		fallbackable = true;
		int ret = audiostream.isStatic(buffer, len);
		if(ret != -1)
		{
			fallbackret = ret;
			fallbackable = true;
			fallbackvalue = buffer[0];
		}
		return ret;
	}
	public int isSilent(int len)
	{
		double[] buffer = new double[1];
		int ret = isStatic(buffer, len);
		if(ret == -1) return -1;
		if(buffer[0] == 0) return ret;
		fallBack();
		return -1;		
	}
	public boolean fallBack()
	{
		if(!fallbackable) return false;
		fallbackable = false;
		fallbackmode = true;
		return true;
	}
	public void close() {
		if(audiostream != null)
			audiostream.close();
	}
}
