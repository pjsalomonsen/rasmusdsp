/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.generators;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioReleaseInstance implements AudioStreamable, UnitInstancePart
{
	public Variable output;
	Variable answer = new Variable();
	Variable active;
	Variable release;
	public AudioReleaseInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		active = parameters.getParameterWithDefault(1, "gate");
		release = parameters.getParameterWithDefault(2, "release");
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		AudioStream inputstream;
		double l_release_step;		
		double f_value = 1;
		int channels;	
		
		double rate;
		
		int mode = 2;
		
		AudioStream activestream = null;
		boolean activestream_eof = false;
		AudioCache audiocache;
		
		public FilterStreamInstance(AudioSession session)
		{			
			
			activestream = AudioEvents.openStream(active, session.getMonoSession());
			audiocache = session.getAudioCache();
			
			
			long l_release = (long)(DoublePart.asDouble(release) * rate);
			if(l_release == 0)
				l_release_step = 1;
			else
				l_release_step = ((1.0) / l_release);
			
			this.channels = session.getChannels();
			this.rate = session.getRate();		
			
			
		}
		
		public int isStatic(double[] buffer, int len) {			
			if(mode == 2)
			{
				int a = readActiveStream(len);
				if(a == len)
				{
					buffer[0] = 1;
					return len;
				}
				fallback = a;
			}			
			return -1;
		}				
		
		double[] sbuff = new double[1];
		int fallback = -1;
		public int readActiveStream(int len)
		{
			if(fallback != -1)
			{
				int r = fallback;
				fallback = -1;
				return r;
			}
			return readActiveStream2(len);
		}		
		public int readActiveStream2(int len)
		{
			if(activestream_eof) return 0;
			
			int clen = len / channels;
			int ret = activestream.isStatic(sbuff, clen);
			
			if(ret != -1)
			{						
				if(sbuff[0] < 0.5)
				{
					activestream_eof = true;
					return 0;				
				}
				if(ret != clen) activestream_eof = true;
				return ret * channels;
			}
			
			double[] cbuffer = audiocache.getBuffer(clen);
			ret = activestream.replace(cbuffer, 0, clen);			
			if(ret == -1)
			{
				audiocache.returnBuffer(cbuffer);
				activestream_eof = true;
				return 0;
			}				
			for (int i = 0; i < clen; i++) 
			{
				if(cbuffer[i] < 0.5)
				{
					audiocache.returnBuffer(cbuffer);
					activestream_eof = true;
					return i * channels;
				}
			}		
			audiocache.returnBuffer(cbuffer);
			if(ret != clen) activestream_eof = true;
			return ret * channels;
		}		
		
		
		public int mix(double[] buffer, int start, int end) {
			
			int activelen = readActiveStream(end - start);
			if(activelen == 0) return mix(buffer, start, end, false);
			if(activelen == end - start) return mix(buffer, start, end, true);			
			int bend = start + activelen;
			int ret = mix(buffer, start, bend, true);
			if(ret < bend - start) return ret;
			int ret2 = mix(buffer, bend, end, false);
			if(ret == -1) return ret;
			return ret + ret2;
		}		
		public int replace(double[] buffer, int start, int end) {
			
			int activelen = readActiveStream(end - start);
			if(activelen == 0) return replace(buffer, start, end, false);
			if(activelen == end - start) return replace(buffer, start, end, true);			
			int bend = start + activelen;
			int ret = replace(buffer, start, bend, true);
			if(ret < bend - start) return ret;
			int ret2 = replace(buffer, bend, end, false);
			if(ret == -1) return ret;
			return ret + ret2;
		}
		
		public int skip(int len) {			
			int activelen = readActiveStream(len);
			if(activelen == 0) return skip(len, false);
			if(activelen == len) return skip(len, true);			
			int bend = activelen;
			int ret = skip(bend, true);
			if(ret < bend) return ret;
			int ret2 = skip(len - bend, false);
			if(ret == -1) return ret;
			return ret + ret2;
		}				
		
		
		public int mix(double[] buffer, int start, int end, boolean active) {
						
			if(mode == 0) if(!active) mode = 5;
			if(mode == 5) return -1;
			
			double ff_value = f_value;
			int fchannels = channels;
			int ix = start;
			
			if(mode == 2)
			{
				if(!active) { mode = 4; }
				else
					while((ix < end))
					{					
						
						for (int c = 0; c < fchannels; c++) {
							buffer[ix] += 1;
							ix++;
						}				
					}					
			}
			
			// RELEASE	
			while((ix < end))
			{						
				ff_value -= l_release_step;
				if(ff_value <= 0)
				{
					ff_value = 0;
					mode = 5; 
					break;
				}
				for (int c = 0; c < fchannels; c++) {
					buffer[ix] += ff_value;
					ix++;
				}							
			}					
			
			f_value = ff_value;
			
			return end - start;
		}
		
		
		public int replace(double[] buffer, int start, int end, boolean active) {
 
			if(mode == 0) if(!active) mode = 5;
			if(mode == 5) return -1;
			
			double ff_value = f_value;
			int fchannels = channels;
			int ix = start;
			
			if(mode == 2)
			{
				if(!active) { mode = 4; }
				else
				{
					Arrays.fill(buffer, ix, end, 1);
					ix = end;				
				}
				/*
				 while((ix < end))
				 {					
				 
				 for (int c = 0; c < fchannels; c++) {
				 buffer[ix] = 1;
				 ix++;
				 }				
				 }					*/
			}
			
			// RELEASE	
			while((ix < end))
			{						
				ff_value -= l_release_step;
				if(ff_value <= 0)
				{
					ff_value = 0;
					mode = 5; 
					break;
				}
				for (int c = 0; c < fchannels; c++) {
					buffer[ix] = ff_value;
					ix++;
				}							
			}				
			Arrays.fill(buffer, ix, end, 0);
			
			f_value = ff_value;
			
			return end - start;	
		}
		
		public int skip(int len, boolean active) {
			 
			if(mode == 0) if(!active) mode = 5;
			if(mode == 5) return -1;
			
			double ff_value = f_value;
			int fchannels = channels;
			int ix = 0;
			
			if(mode == 2)
			{
				if(!active) { mode = 4; }
				else
				{
					ix = len;				
				}
			}
			
			// RELEASE	
			if(ix < len)
			{
				ff_value -= l_release_step*(len - ix)/fchannels;
				if(ff_value <= 0)
				{
					mode = 5; 
					ff_value = 0;
				}				
			}					
			f_value = ff_value;			
			return len;	
		}
		
		
		public void close() {
			activestream.close();
			
		}		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioRelease implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Linear Release Envelope");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "gate",		"Gate",	    	null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		metadata.add(2,  "release",		"Release time", null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		return metadata;		
	}
		
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioReleaseInstance(parameters);
	}
}