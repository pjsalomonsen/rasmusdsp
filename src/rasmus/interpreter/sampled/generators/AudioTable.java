/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.generators;

import java.util.Arrays;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.Unit;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;


class AudioTableUnitInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	Variable answer = new Variable();
	Variable input;
	AudioTableInstance ati;
	
	
	public void calc()
	{
	}	
	
	class FilterStreamInstance implements AudioStream
	{
		AudioCache audiocache;
		TableData tabledata;
		int channels;
		AudioStream inputstream = null;
		boolean inputstream_eof = false;
		public FilterStreamInstance(AudioSession session)
		{				
			this.channels = session.getChannels();
			audiocache = session.getAudioCache();
			tabledata = ati.getTableData(); 
			/*
			 (TableData)audiocache.cachetable.get(ati);
			 if(tabledata == null)
			 {
			 tabledata = ati.createTableData();
			 audiocache.cachetable.put(ati, tabledata);
			 }*/
			
			if(AudioEvents.getInstance(input).track.size() == 0)
			{
				inputstream_eof = true;
			}
			else
			{
				inputstream = AudioEvents.openStream(input, session.getMonoSession());
			}			
			
		}
		
		
		int staticfallback = -1;
		double staticfallbackvalue = 0;
		public int isStatic(double[] buffer, int len) {
			
			// Check if we are in FALLBACK mode
			if(staticfallback != -1) return -1;
			
			if(inputstream_eof) return -1;
			
			double[] table = tabledata.table;
			int tchannels = tabledata.channels;
			double trate = tabledata.rate;
			int tablelenc = tabledata.table_length / tchannels;
			
			int clen = len / channels;
			
			double[] freqbuffer = new double[1];
			int ret = inputstream.isStatic(freqbuffer, clen);
			if(ret == -1)
			{
				audiocache.returnBuffer(freqbuffer);				
				return -1;
			}		
			
			int ti = ((int)(freqbuffer[0] * trate)) % tablelenc;
			if(ti < 0) ti += tablelenc;		
			
			buffer[0] = table[(ti * tchannels)];
			
			// Check if all channels of the table we are using contain the same value
			// if not then we need fo fallback the read from the inputstream
			for (int c = 0; c < channels; c++) {
				int rc = c;
				if(rc >= tchannels) return ret;				
				if(buffer[0] != table[(ti * tchannels) + rc])
				{
					// FALLBACK is needed
					staticfallback = ret;
					staticfallbackvalue = freqbuffer[0];
					audiocache.returnBuffer(freqbuffer);
					
					return -1;
				}				
			}			
			
			audiocache.returnBuffer(freqbuffer);
			
			
			// Everything is ok, the buffer is static
			return ret;					
		}	
		
		public int skip(int len)
		{
			if(inputstream_eof) return -1;
			
			int ret;
			if(staticfallback != -1)
			{
				ret = staticfallback;
				staticfallback = -1;
			}
			else
			{
				ret = inputstream.skip(len);
			}
			
			if(ret == -1)
			{
				inputstream_eof = true;
				return -1;
			}					
			return ret;
		}
		
		public int mix(double[] buffer, int start, int end) {
			
			if(inputstream_eof) return -1;
			
			double[] table = tabledata.table;
			int tchannels = tabledata.channels;
			double trate = tabledata.rate;
			int tablelenc = tabledata.table_length / tchannels;
			
			int cend = end / channels;
			int cstart = start / channels;						
			
			double[] freqbuffer = audiocache.getBuffer(cend);
			
			int ret;	
			// Check if we are in FALLBACK mode			
			if(staticfallback != -1)
			{
				ret = staticfallback;
				Arrays.fill(freqbuffer, cstart, cend, staticfallbackvalue);
				staticfallback = -1;
			}
			else
			{
				ret = inputstream.replace(freqbuffer, cstart, cend);
			}
			
			if(ret == -1)
			{
				audiocache.returnBuffer(freqbuffer);
				inputstream_eof = true;
				return -1;
			}		
			else
			{
				int chs = channels;
				int cendret = cstart + ret;											
				for (int c = 0; c < chs; c++) {
					int rc = c;
					if(rc >= tchannels) rc = tchannels - 1;
					int ix = start + c;
					for (int i = cstart; i < cendret; i++) {
						double loc = (freqbuffer[i] * trate);
						long iloc = (long)loc;
						int ti = (int)( (iloc) % tablelenc);						
						double interp = loc - iloc;						
						if(ti < 0) ti += tablelenc;											
						buffer[ix] += table[(ti * tchannels) + rc]*(1- interp) + table[((ti + 1) * tchannels) + rc]*interp; 
						ix += chs;
					}					
				}			
				audiocache.returnBuffer(freqbuffer);
				return ret * channels;								
			}															
		}
		public int replace(double[] buffer, int start, int end) {
			if(inputstream_eof) return -1;
			
			double[] table = tabledata.table;
			int tchannels = tabledata.channels;
			double trate = tabledata.rate;
			int tablelenc = tabledata.table_length / tchannels;
			
			int cend = end / channels;
			int cstart = start / channels;						
			
			double[] freqbuffer = audiocache.getBuffer(cend);
			
			int ret;	
			// Check if we are in FALLBACK mode			
			if(staticfallback != -1)
			{
				ret = staticfallback;
				Arrays.fill(freqbuffer, cstart, cend, staticfallbackvalue);
				staticfallback = -1;
			}
			else
			{
				ret = inputstream.replace(freqbuffer, cstart, cend);
			}
			
			
			if(ret == -1)
			{
				inputstream_eof = true;
				audiocache.returnBuffer(freqbuffer);
				return -1;
			}		
			else
			{
				int chs = channels;
				int cendret = cstart + ret;											
				for (int c = 0; c < chs; c++) {
					int rc = c;
					if(rc >= tchannels) rc = tchannels - 1;
					int ix = start + c;
					for (int i = cstart; i < cendret; i++) {
						/*
						 int ti = ((int)(freqbuffer[i] * trate)) % tablelenc;
						 if(ti < 0) ti += tablelenc;											
						 buffer[ix] = table[(ti * tchannels) + rc]; 
						 ix += chs;*/
						
						double loc = (freqbuffer[i] * trate);
						long iloc = (long)loc;
						int ti = (int)( (iloc) % tablelenc);						
						double interp = loc - iloc;						
						if(ti < 0) ti += tablelenc;											
						buffer[ix] = table[(ti * tchannels) + rc]*(1- interp) + table[((ti + 1) * tchannels) + rc]*interp; 
						ix += chs;
						
					}					
				}			
				audiocache.returnBuffer(freqbuffer);
				return ret * channels;								
			}				
		}
		public void close() {
			if(inputstream != null)
			{
				inputstream.close();
				inputstream = null;
			}
		}
	}
	
	public AudioTableUnitInstance(AudioTableInstance ati, Parameters parameters)
	{
		this.ati = ati;
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		output.remove(answer);
	}
	
	public AudioStream openStream(AudioSession session) {
		
		return new FilterStreamInstance(session);
	}	
}

class TableData
{
	int table_length;
	double[] table;
	
	int channels;
	double rate;
	double from;
	double to;
	double dur;
	int datadur;
	public TableData(AudioTableInstance ati)
	{
		channels = (int)DoublePart.asDouble(ati.channels);
		rate = (int)DoublePart.asDouble(ati.rate);
		from = DoublePart.asDouble(ati.from);
		to = DoublePart.asDouble(ati.to);
		dur = to - from;
		
		if(channels == 0) channels = 1;
		if(rate == 0) rate = 48000;
		
		datadur = ((int)(dur * rate)) * channels;
		table = new double[datadur + channels];
		int fromdur = ((int)(from * rate)) * channels;
		
		AudioSession audiosession = new AudioSession(rate, channels);
		
		
		AudioStream audiostream = AudioEvents.openStream(ati.input, audiosession);
		
		if(fromdur != 0)
		{
			double[] tempbuffer = new double[512*channels];
			while(fromdur > tempbuffer.length)
			{
				audiostream.replace(tempbuffer, 0, tempbuffer.length);
				fromdur -= tempbuffer.length;
			}
			double[] garbagebuffer = new double[fromdur];
			audiostream.replace(garbagebuffer, 0, garbagebuffer.length);
		}
		
		audiostream.replace(table, 0, table.length - channels);
		for (int i = 0; i < channels; i++) {
			table[table.length - channels + i] = table[i];
		}
		
		table_length = table.length - channels;
		audiostream.close();
		audiosession.close();
		
	}
}

class AudioTableInstance extends UnitInstanceAdapter implements UnitFactory, Commitable
{
	public Variable output;
	Variable answer = new Variable();
	Variable input;
	Variable from;
	Variable to;
	Variable rate;
	Variable channels;
	
	TableData tabledata = null;
	
	public TableData getTableData()
	{
		if(tabledata == null)
			tabledata =  new TableData(this);
		return tabledata;
	}    
	
	NameSpace namespace;
	public void calc()
	{
		namespace.addToCommitStack(this);
	}	
	public AudioTableInstance(Parameters parameters)
	{
		namespace = parameters.getNameSpace();
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		from = parameters.getParameterWithDefault(1, "from");
		to = parameters.getParameterWithDefault(2, "to");
		rate = parameters.getParameterWithDefault(3, "rate");
		channels = parameters.getParameterWithDefault(4, "channels");
		
		AudioEvents.getInstance(input).addListener(this);
		DoublePart.getInstance(from).addListener(this);
		DoublePart.getInstance(to).addListener(this);
		DoublePart.getInstance(rate).addListener(this);
		DoublePart.getInstance(channels).addListener(this);
		
		answer = Unit.asVariable(this);
		//answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		
		AudioEvents.getInstance(input).removeListener(this);
		DoublePart.getInstance(from).removeListener(this);
		DoublePart.getInstance(to).removeListener(this);
		DoublePart.getInstance(rate).removeListener(this);
		DoublePart.getInstance(channels).removeListener(this);
		
		output.remove(answer);
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioTableUnitInstance(this, parameters);
	}
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit() {
		tabledata = null;
		//tabledata =  new TableData(this);
	}
	
}

public class AudioTable implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Audio tabled based function generator");
		metadata.add(-1, "output",		"Output",		    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "from",		"From",	    	null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2,  "to",		    "To",	    	null, "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3,  "rate",		"Sample rate",  "48000", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4,  "channels",	"Channels",  	"1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1,  "input",		"Input",	    	null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		return metadata;
	}	
		
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioTableInstance(parameters);
	}
}