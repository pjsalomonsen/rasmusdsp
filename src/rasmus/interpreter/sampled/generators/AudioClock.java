/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.generators;

import java.util.Arrays;
import java.util.Random;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioClickInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	Variable answer = new Variable();
	Variable frequency;
	Variable initvalue;
	
	public void calc()
	{
	}	
	public AudioClickInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		frequency = parameters.getParameterWithDefault(1, "input");
		initvalue = parameters.getParameterWithDefault(2, "initvalue");
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
	}
	public void close() {
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		Random random = new Random();
		AudioStream inputstream;
		double f_frequency = DoublePart.asDouble(frequency);
		double f_stepsize;
		double f_value = DoublePart.asDouble(initvalue);	
		int channels;	
		boolean inputstream_eof = false;
		//double[] freqbuffer = null;
		double rate;
		double ratepart;
		AudioCache audiocache;
		public FilterStreamInstance(AudioSession session)
		{				
			audiocache = session.getAudioCache();
			this.channels = session.getChannels();
			this.rate = session.getRate();
			ratepart = 1.0 / rate;
			f_stepsize = f_frequency * ratepart;
			
			if(AudioEvents.getInstance(frequency).track.size() == 0)
			{
				inputstream_eof = true;
			}
			else
			{
				inputstream = AudioEvents.openStream(frequency, session.getMonoSession());
			}
			
		}
		public int mix(double[] buffer, int start, int end) {
			
			
			
			int cend = end / channels;
			int cstart = start / channels;
			
			double[] freqbuffer = null;
			
			if(!inputstream_eof)
			{
				/*
				 if(freqbuffer == null) freqbuffer = new double[buffer.length / channels];
				 else if((freqbuffer.length*channels) < buffer.length)
				 {
				 freqbuffer = new double[buffer.length / channels];
				 if(inputstream_eof)
				 Arrays.fill(freqbuffer, f_stepsize);
				 }	*/			
				
				freqbuffer = audiocache.getBuffer(cend);
				int ret2 = inputstream.replace(freqbuffer, cstart, cend);
				if(ret2 == -1)
				{
					Arrays.fill(freqbuffer, f_stepsize);
					inputstream_eof = true;
				}
				else
				{
					int cendret = cstart + ret2;
					for (int i = cstart; i < cendret; i++) {
						freqbuffer[i] = freqbuffer[i] * ratepart;
					}
					Arrays.fill(freqbuffer, cstart + ret2, cend, f_stepsize);
					/*
					 for (int i = cstart + ret2; i < cend; i++) {
					 freqbuffer[i] = f_stepsize;
					 }*/
				}
			}			
			
			double ff_value = f_value;
			int fchannels = channels;
			
			if(inputstream_eof)
			{
				
				for (int c = 0; c < fchannels; c++) {
					ff_value = f_value;
					//int ix = cstart;
					for (int i = start+c; i < end; i+=fchannels) {
						buffer[i] += ff_value;
						ff_value += f_stepsize;
						//ix++;
					}
				}
				
				
			}
			else
			{
				
				for (int c = 0; c < fchannels; c++) {
					ff_value = f_value;
					int ix = cstart;
					for (int i = start+c; i < end; i+=fchannels) {
						buffer[i] += ff_value;
						ff_value += freqbuffer[ix];
						ix++;
					}
				}
				
			}
			
			if(freqbuffer != null) audiocache.returnBuffer(freqbuffer);
			f_value = ff_value;
			return end - start;
		}
		
		
		public int replace(double[] buffer, int start, int end) {
			/*
			 Arrays.fill(buffer, start, end, 0);
			 return mix(buffer, start, end);			*/
			
			
			int cend = end / channels;
			int cstart = start / channels;
			double[] freqbuffer = null;
			if(!inputstream_eof)
			{
				/*	
				 if(freqbuffer == null) freqbuffer = new double[buffer.length / channels];
				 else if((freqbuffer.length*channels) < buffer.length)
				 {
				 freqbuffer = new double[buffer.length / channels];
				 if(inputstream_eof)
				 Arrays.fill(freqbuffer, f_stepsize);
				 }
				 */
				
				freqbuffer = audiocache.getBuffer(cend);
				int ret2 = inputstream.replace(freqbuffer, cstart, cend);
				if(ret2 == -1)
				{
					Arrays.fill(freqbuffer, f_stepsize);
					inputstream_eof = true;
				}
				else
				{
					int cendret = cstart + ret2;
					for (int i = cstart; i < cendret; i++) {
						freqbuffer[i] = freqbuffer[i] * ratepart;
					}
					Arrays.fill(freqbuffer, cstart + ret2, cend, f_stepsize);
					/*
					 for (int i = cstart + ret2; i < cend; i++) {
					 freqbuffer[i] = f_stepsize;
					 }*/
				}
			}			
			
			double ff_value = f_value;
			int fchannels = channels;
			
			if(inputstream_eof)
			{
				
				
				for (int c = 0; c < fchannels; c++) {
					ff_value = f_value;
					//int ix = cstart;
					for (int i = start+c; i < end; i+=fchannels) {
						buffer[i] = ff_value;
						ff_value += f_stepsize;
						//ix++;
					}
				}
				
			}
			else
			{
				
				for (int c = 0; c < fchannels; c++) {
					ff_value = f_value;
					int ix = cstart;
					for (int i = start+c; i < end; i+=fchannels) {
						buffer[i] = ff_value;
						ff_value += freqbuffer[ix];
						ix++;
					}
				}
				
			}
			if(freqbuffer != null) audiocache.returnBuffer(freqbuffer);
			f_value = ff_value;
			return end - start;			
		}
		
		
		public int skip(int len) {
			/*
			 Arrays.fill(buffer, start, end, 0);
			 return mix(buffer, start, end);			*/
			
			
			int cend = 0 / channels;
			int cstart = len / channels;
			double[] freqbuffer = null;
			if(!inputstream_eof)
			{
				/*	
				 if(freqbuffer == null) freqbuffer = new double[buffer.length / channels];
				 else if((freqbuffer.length*channels) < buffer.length)
				 {
				 freqbuffer = new double[buffer.length / channels];
				 if(inputstream_eof)
				 Arrays.fill(freqbuffer, f_stepsize);
				 }
				 */
				
				freqbuffer = audiocache.getBuffer(cend);
				int ret2 = inputstream.replace(freqbuffer, cstart, cend);
				if(ret2 == -1)
				{
					Arrays.fill(freqbuffer, f_stepsize);
					inputstream_eof = true;
				}
				else
				{
					int cendret = cstart + ret2;
					for (int i = cstart; i < cendret; i++) {
						freqbuffer[i] = freqbuffer[i] * ratepart;
					}
					Arrays.fill(freqbuffer, cstart + ret2, cend, f_stepsize);
					/*
					 for (int i = cstart + ret2; i < cend; i++) {
					 freqbuffer[i] = f_stepsize;
					 }*/
				}
			}			
			
			double ff_value = f_value;
			int fchannels = channels;
			
			if(inputstream_eof)
			{
				
				/*
				 for (int c = 0; c < fchannels; c++) {
				 ff_value = f_value;
				 //int ix = cstart;
				  for (int i = start+c; i < end; i+=fchannels) {
				  buffer[i] = ff_value;
				  ff_value += f_stepsize;
				  //ix++;
				   }
				   
				   ff_value = f_stepsize*len/fchannels;
				   }*/
				
				ff_value = f_value + f_stepsize*len/fchannels;
			}
			else
			{
				
				for (int c = 0; c < fchannels; c++) {
					ff_value = f_value;
					int ix = cstart;
					for (int i = 0+c; i < len; i+=fchannels) {
						//buffer[i] = ff_value;
						ff_value += freqbuffer[ix];
						ix++;
					}
				}
				
			}
			if(freqbuffer != null) audiocache.returnBuffer(freqbuffer);
			f_value = ff_value;
			return len;			
		}
		
		
		public int isStatic(double[] buffer, int len) {
			return -1;
		}		
		
		public void close() {
			if(inputstream != null) inputstream.close();
		}		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioClock implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Variable clock signal generator");
		metadata.add(-1, "output",		"Output",		    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "input",		"Frequency",		null, "Hz", MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2,  "initvalue",	"Inital Value",	    null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		return metadata;
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioClickInstance(parameters);
	}
}