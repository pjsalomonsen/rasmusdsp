/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.generators;

import java.util.ArrayList;
import java.util.Iterator;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioLinSegInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	Variable answer = new Variable();
	Variable lastvar = null;
	/*
	 RVariable active;
	 RVariable attack;
	 RVariable decay;
	 RVariable sustain;
	 RVariable release;*/
	
	ArrayList elements = new ArrayList();
	
	double activevalue = 0;
	
	public void calc()
	{
	}	
	public AudioLinSegInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		
		int ii = 1;
		Variable var = parameters.getParameter(ii);
		while(var != null)
		{
			elements.add(DoublePart.getInstance((Variable)var));		
			ii++;
			var = parameters.getParameter(ii);
			
			lastvar = var;
		}		
		
		answer = AudioEvents.asVariable(new AudioEvent(0, this));
		
		if(lastvar != null) output.add(lastvar);
		output.add(answer);
	}
	public void close() {
		if(lastvar != null) output.remove(lastvar);
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		double f_value = 0;
		int channels;	
		double rate;
		double[] env_values;
		double[] env_step;
		long[] env_durs;
		boolean[] env_inc;
		int env_index = 0;
		int last_index = 0;
		long esteps = 0;
		
		public FilterStreamInstance(AudioSession session)
		{			
			rate = session.getRate();
			
			
			int l = elements.size() - 1;
			if(l % 2 != 0) l++;			
			int fcount = l / 2;
			
			Iterator iterator = elements.iterator();
			
			if(iterator.hasNext())
			{
				f_value = ((DoublePart)iterator.next()).getDouble();
			}
			
			env_values = new double[fcount];
			env_step = new double[fcount];
			env_inc = new boolean[fcount];
			env_durs = new long[fcount];
			
			last_index = fcount;
			double lastvalue = f_value;
			for (int i = 0; i < fcount; i++) {
				long durs;
				double nextvalue;
				if(iterator.hasNext()) durs = (long)(rate * ((DoublePart)iterator.next()).getDouble()); else durs = 0;				
				if(iterator.hasNext()) nextvalue = ((DoublePart)iterator.next()).getDouble(); else nextvalue = 0;
				env_values[i] = nextvalue;
				env_inc[i] = nextvalue > lastvalue;	
				env_durs[i] = durs;
				if(durs == 0)
				{
					env_step[i] = 0;
				}
				else
					env_step[i] = ((nextvalue - lastvalue) / durs);
				
				lastvalue = nextvalue;
			}
			
			channels = session.getChannels();
			rate = session.getRate();				
			
		}
		public int mix(double[] buffer, int start, int end) {
			
			if(env_index == last_index) return -1;
			double ff_value = f_value;
			int ix = start;
			while(ix < end)
			{
				if(env_index == last_index) return ix - start;
				
				double cur_env_step = env_step[env_index];
				double cur_env_values = env_values[env_index];				
				long dur = env_durs[env_index];
				
				if(dur < 0.0)
				{
					while(ix < end)
					{
						buffer[ix] += ff_value;
						ix++;
					}	
				}
				else					
					while(ix < end)
					{
						if(esteps == dur)
						{
							ff_value = cur_env_values;
							env_index++;
							esteps = 0;
							break;
						}
						ff_value += cur_env_step;
						buffer[ix] += ff_value;
						esteps++;					
						ix++;
					}		
			}
			f_value = ff_value;						
			return end - start;
		}
		
		public int isStatic(double[] buffer, int len) {
			if(env_index == last_index) return -1;
			
			if(esteps == env_durs[env_index])
			{
				f_value = env_values[env_index];
				env_index++;
				esteps = 0;
				
				if(env_index == last_index) return -1;
			}		
			
			
			
			double cur_env_step = env_step[env_index];
			if(cur_env_step != 0) return -1;
			
			long dur = env_durs[env_index];		
			dur -= esteps;
			
			if(dur < len) return -1;
			esteps += len;
			
			buffer[0] = f_value;
			return len;								
		}	
		
		public int replace(double[] buffer, int start, int end) {
			if(env_index == last_index) return -1;
			double ff_value = f_value;
			int ix = start;
			while(ix < end)
			{
				if(env_index == last_index) return ix - start;
				
				double cur_env_step = env_step[env_index];
				double cur_env_values = env_values[env_index];				
				long dur = env_durs[env_index];
				
				if(dur < 0.0)
				{
					while(ix < end)
					{
						buffer[ix] = ff_value;
						ix++;
					}	
				}
				else					
					while(ix < end)
					{
						if(esteps == dur)
						{
							ff_value = cur_env_values;
							env_index++;
							esteps = 0;
							break;
						}
						ff_value += cur_env_step;
						buffer[ix] = ff_value;
						esteps++;					
						ix++;
					}		
			}
			f_value = ff_value;						
			return end - start;
		}
		public int skip(int len)
		{
			double[] buffer = new double[1];
			int ret = isStatic(buffer, len);
			if(ret != -1) return len;
			buffer = new double[len];
			return mix(buffer, 0, len);
		}		
		public void close() {
		}		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioLinSeg implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Linear Segments");
		metadata.add(-1, "output",		"Output",		    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.setHasVarargs(true);
		return metadata;
	}	
		
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioLinSegInstance(parameters);
	}
}