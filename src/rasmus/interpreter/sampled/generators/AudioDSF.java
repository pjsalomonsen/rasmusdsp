/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sampled.generators;

import java.util.Arrays;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.sampled.AudioCache;
import rasmus.interpreter.sampled.AudioEvent;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioFallBackStream;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.AudioStreamable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class AudioDSFInstance extends UnitInstanceAdapter implements AudioStreamable
{
	public Variable output;
	Variable answer = new Variable();
	Variable value;
	
	
	Variable in_F;
	Variable in_A;
	Variable in_N;
	Variable in_FI;
	
	
	
	
	/*
	 static double[] sintable;
	 static double sintable_factor;    
	 private synchronized static void precalc()
	 {
	 sintable = new double[480000];
	 double flen  = sintable.length;
	 sintable_factor = flen / ( 2* Math.PI);		
	 int len = sintable.length;
	 for (int i = 0; i < len; i++) {
	 double k = (i*1.0) / sintable_factor;
	 sintable[i] = Math.sin(k);
	 }
	 }
	 static 
	 {
	 precalc();
	 }*/
	
	public void calc()
	{
	}	
	public AudioDSFInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		answer = AudioEvents.asVariable(new AudioEvent(0, this));		
		output.add(answer);			
		
		in_F = parameters.getParameterWithDefault(1, "f");
		in_A = parameters.getParameterWithDefault(2, "a");
		in_N = parameters.getParameterWithDefault(3, "n");
		in_FI = parameters.getParameter(4, "fi");
		
	}
	public void close() {
		output.remove(answer);
	}
	
	class FilterStreamInstance implements AudioStream
	{
		AudioSession session;
		AudioCache audiocache;
		double x_counter = 0;				
		
		double[] stockbuffer2 = null;
		double[] stockbuffer3 = null;
		double[] stockbuffer4 = null;
		
		boolean inputstream2_eof = false;
		AudioFallBackStream inputstream2 = null;
		boolean inputstream3_eof = false;
		AudioFallBackStream inputstream3 = null;
		boolean inputstream4_eof = false;
		AudioFallBackStream inputstream4 = null;
		
		
		public FilterStreamInstance(AudioSession session)
		{			
			this.session = session;
			audiocache = session.getAudioCache();
			
			if(AudioEvents.getInstance(in_F).track.size() == 0)
			{
				inputstream2_eof = true;
			}
			else
				inputstream2 = new AudioFallBackStream(AudioEvents.openStream(in_F, session.getMonoSession()));
			
			if(AudioEvents.getInstance(in_A).track.size() == 0)
			{
				inputstream3_eof = true;
			}
			else
				inputstream3 = new AudioFallBackStream(AudioEvents.openStream(in_A, session.getMonoSession()));
			
			
			if(AudioEvents.getInstance(in_N).track.size() == 0)
			{
				inputstream4_eof = true;
			}
			else
				inputstream4 = new AudioFallBackStream(AudioEvents.openStream(in_N, session.getMonoSession()));
			
			
			
		}
		public int mix(double[] buffer, int start, int end) {
			
			double f = DoublePart.asDouble(in_F);
			
			double f0 = Math.PI * 2 * f / session.getRate();
			//double N = Math.floor(session.rate / (2 * 200.0)) - 5;
			
			double a = DoublePart.asDouble(in_A);   // 0.9;  // a<1.0			
			double N = DoublePart.asDouble(in_N);   // 60;    // Harmonics
			
			double fi;
			if(in_FI == null)
				fi = Math.PI / 2;
			else
				fi = DoublePart.asDouble(in_FI); // Math.PI/2;   // Inital Phase
			
			//if(N == 0) N = Math.floor(session.rate / f);
			
			if(a > 0.999) a = 0.999;	
			
			boolean variable_data = !inputstream2_eof || !inputstream3_eof || !inputstream4_eof;
			
			int channels = session.getChannels();
			
			int cstart = start / channels;
			int cend = end / channels;
			
			if(variable_data)
			{
				/*
				 f0 = stockbuffer2[ix];
				 a = stockbuffer3[ix];
				 N = stockbuffer4[ix];
				 */
				variable_data = false;
				boolean static_2 = false;
				boolean static_3 = false;
				boolean static_4 = false;
				
				double[] cbuffer = new double[1]; 
				if(!inputstream2_eof)
				{
					int ret = inputstream2.isStatic(cbuffer, cend - cstart);
					if(ret != -1)
					{
						f0 = Math.PI * 2 * cbuffer[0] / session.getRate();
						static_2 = true;
					}	    		
					else
						variable_data = true;
				}
				if(!inputstream3_eof)
				{
					int ret = inputstream3.isStatic(cbuffer, cend - cstart);
					if(ret != -1)
					{
						a = cbuffer[0];
						static_2 = true;
					}	    	
					else
						variable_data = true;
					
				}
				if(!inputstream4_eof)
				{
					int ret = inputstream4.isStatic(cbuffer, cend - cstart);
					if(ret != -1)
					{
						N = cbuffer[0];
						static_2 = true;
					}	   
					else
						variable_data = true;
					
				}	    		
				
				//if(static_2 && static_3 && static_4) variable_data = false;
				if(variable_data)
				{
					if(static_2) inputstream2.fallBack();
					if(static_3) inputstream2.fallBack();
					if(static_4) inputstream2.fallBack();
				}
			}
			
			if(variable_data)
			{
				
				
				// F	    			    		
				if(!inputstream2_eof)
				{
					if(stockbuffer2 == null) stockbuffer2 = audiocache.getBuffer(cend);
					if(stockbuffer2.length < cend)
					{
						audiocache.returnBuffer(stockbuffer2);
						stockbuffer2 = audiocache.getBuffer(cend);
					}
					int ret = inputstream2.replace(stockbuffer2, cstart, cend);
					if(ret == -1)
					{
						Arrays.fill(stockbuffer2, f0);
						inputstream2_eof = true;	    				
					}
					else
					{
						
						double factor = Math.PI * 2 / session.getRate();
						for (int i = cstart; i < cstart + ret; i++) {
							stockbuffer2[i] *= factor;
						}
						Arrays.fill(stockbuffer2, cstart + ret, cend, f0);
					}
				}
				else
				{
					if(stockbuffer2==null? true : stockbuffer2.length < cend)
					{
						if(stockbuffer2 != null) audiocache.returnBuffer(stockbuffer2);
						stockbuffer2 = audiocache.getBuffer(cend);
						Arrays.fill(stockbuffer2, f0);
					}
				}
				
				
				// A
				if(!inputstream3_eof)
				{
					if(stockbuffer3 == null) stockbuffer3 = audiocache.getBuffer(cend);
					if(stockbuffer3.length < cend)
					{
						audiocache.returnBuffer(stockbuffer3);
						stockbuffer3 = audiocache.getBuffer(cend);
					}
					int ret = inputstream3.replace(stockbuffer3, cstart, cend);
					if(ret == -1)
					{
						inputstream3_eof = true;
						Arrays.fill(stockbuffer3, a);
					}
					else
						Arrays.fill(stockbuffer3, cstart + ret, cend, a);
				}
				else
				{
					if(stockbuffer3==null? true : stockbuffer3.length < cend)
					{
						if(stockbuffer3 != null) audiocache.returnBuffer(stockbuffer3);
						stockbuffer3 = audiocache.getBuffer(cend);
						Arrays.fill(stockbuffer3, a);
					}
				}
				
				// N
				
				if(!inputstream4_eof)
				{
					if(stockbuffer4 == null) stockbuffer4 = audiocache.getBuffer(cend);
					if(stockbuffer4.length < cend)
					{
						audiocache.returnBuffer(stockbuffer4);
						stockbuffer4 = audiocache.getBuffer(cend);
					}
					int ret = inputstream4.replace(stockbuffer4, cstart, cend);
					if(ret == -1)
					{
						Arrays.fill(stockbuffer4, N);
						inputstream4_eof = true;	    				
					}
					else
						Arrays.fill(stockbuffer4, cstart + ret, cend, N);
				}
				else
				{
					if(stockbuffer4==null? true : stockbuffer4.length < cend)
					{
						if(stockbuffer4 != null) audiocache.returnBuffer(stockbuffer4);	    				
						stockbuffer4 = audiocache.getBuffer(cend);
						Arrays.fill(stockbuffer4, N);
					}
				}
				
			}
			
			
			// F getur veri� breytanlegt
			// a getur veri� breytanlegt
			// N getur veir� breytanlegt , �.s. 3 Audio Stream breytur leyf�ar
			
			/*
			 double[] l_sintable = sintable;
			 int l_sintable_len = l_sintable.length;
			 double l_sintable_factor = sintable_factor;   			
			 
			 double table_fi = fi*l_sintable_factor;			
			 double table_pih2 = Math.PI*l_sintable_factor/2;			
			 */
			/*
			 
			 double table_x = l_sintable_factor * x;		    		  
			 double s1 = aa*a* l_sintable[((int)((N - 1.0)*table_x + table_fi)) % l_sintable_len];
			 double s2 = aa* l_sintable[((int)((N)*table_x + table_fi)) % l_sintable_len];
			 double s3 = a* l_sintable[((int)(table_x + table_fi)) % l_sintable_len];
			 double s4 = a*a + 1.0 - 2*a* l_sintable[((int)(table_x + table_pih2)) % l_sintable_len];
			 
			 
			 */
			
			double calc_fi = Math.sin(fi);			
			double x = 0;
			double aa = Math.pow(a,N);
			
			// FOR USE IN TAYLOR SIN
			/*
			 final double piD2 = Math.PI / 2;
			 final double NpiD2 = -piD2;
			 final double pi = Math.PI;
			 final double pi2 = Math.PI*2;
			 final double pi3 = Math.PI*3 +  Math.asin(1);;*/
			
			for (int c = 0; c < channels; c++) {		
				x = this.x_counter;
				
				if(variable_data)
				{
					int ix = cstart;
					if(in_FI == null)
						for (int i = start+c; i < end; i+=channels) {
							
							
							f0 = stockbuffer2[ix];
							a = stockbuffer3[ix];
							N = stockbuffer4[ix];
							
							//N = 5.0;
							
							aa = Math.pow(a,N);
							//sin(a + b) = sin(a)cos(b) + cos(a)sin(b)
							//cos(a + b) = cos(a)cos(b) - sin(a)sin(b)
							
							
							double cosx = Math.cos(x);
							double s1 = aa*a*Math.cos((N-1.0)*x);
							double s2 = aa*Math.cos(N*x);
							double s3 = a*cosx;
							double s4 = 1.0 - (2*a*cosx) +(a*a);	  
							/*
							 double cx = x;
							 {
							 cx = ((cx + pi3) % pi2) - pi;	    	  
							 if(cx > piD2) cx = pi - cx; else if(cx < NpiD2) cx = -pi - cx;	  
							 double cx2 = cx*cx;	    	  
							 cx = cx*(cx2*(cx2*(cx2*(cx2*(1.0/362880.0)
							 - (1.0/5040.0)) + (1.0/120.0))
							 - (1.0/6.0)) + 1.0);	 		
							 }
							 double cosx = Math.cos(x); //cx;
							 
							 cx = (N-1.0)*x;
							 {
							 cx = ((cx + pi3) % pi2) - pi;	    	  
							 if(cx > piD2) cx = pi - cx; else if(cx < NpiD2) cx = -pi - cx;	  
							 double cx2 = cx*cx;	    	  
							 cx = cx*(cx2*(cx2*(cx2*(cx2*(1.0/362880.0)
							 - (1.0/5040.0)) + (1.0/120.0))
							 - (1.0/6.0)) + 1.0);	 		
							 }
							 double s1 = aa*a*cx; //aa*a*Math.cos((N-1.0)*x); //aa*a*cx;
							 
							 cx = N*x;
							 {
							 cx = ((cx + pi3) % pi2) - pi;	    	  
							 if(cx > piD2) cx = pi - cx; else if(cx < NpiD2) cx = -pi - cx;	  
							 double cx2 = cx*cx;	    	  
							 cx = cx*(cx2*(cx2*(cx2*(cx2*(1.0/362880.0)
							 - (1.0/5040.0)) + (1.0/120.0))
							 - (1.0/6.0)) + 1.0);	 		
							 }
							 double s2 = aa*cx; // aa*Math.cos(N*x); //aa*cx;				    		  
							 
							 double s3 = a*cosx;
							 double s4 = 1.0 - (2*a*cosx) +(a*a);	*/
							
							buffer[i] += (s4==0.0) ? 0:(calc_fi - s3 - s2 +s1)/s4; 
							
							x += f0;
							ix++;
						}
					else	    	
						for (int i = start+c; i < end; i+=channels) {
							
							f0 = stockbuffer2[ix];
							a = stockbuffer3[ix];
							N = stockbuffer4[ix];
							
							double s1 = aa*a*Math.sin((N-1.0)*x+fi);
							double s2 = aa*Math.sin(N*x+fi);
							double s3 = a*Math.sin(x+fi);
							double s4 = 1.0 - (2*a*Math.cos(x)) +(a*a);	    		  
							
							
							buffer[i] += (s4==0.0) ? 0:(calc_fi - s3 - s2 +s1)/s4; 
							
							x += f0;   		
							ix++;
							
						}		    		
				}
				else
				{
					if(in_FI == null)
					{
						
						double s1_m_cos = Math.cos((N-1.0)*f0);
						double s1_m_sin = Math.sin((N-1.0)*f0);			
						double s2_m_cos = Math.cos(N*f0);
						double s2_m_sin = Math.sin(N*f0);			
						double s3_m_cos = Math.cos(f0);
						double s3_m_sin = Math.sin(f0);	
						
						double s1_sin = Math.sin((N-1.0)*x);
						double s2_sin = Math.sin(N*x);
						double s3_sin = Math.sin(x);
						double s1_cos = Math.cos((N-1.0)*x);
						double s2_cos = Math.cos(N*x);
						double s3_cos = Math.cos(x);
						
						for (int i = start+c; i < end; i+=channels) {
							
							
							
							//sin(a + b) = sin(a)cos(b) + cos(a)sin(b)
							//cos(a + b) = cos(a)cos(b) - sin(a)sin(b)
							
							double s1 = aa*a*s1_cos;
							double s2 = aa*s2_cos;
							double s3 = a*s3_cos;
							double s4 = 1.0 - (2*a*s3_cos) +(a*a);	  		  
							
							/*
							 double cosx = Math.cos(x);
							 double s1 = aa*a*Math.cos((N-1.0)*x);
							 double s2 = aa*Math.cos(N*x);
							 double s3 = a*cosx;
							 double s4 = 1.0 - (2*a*cosx) +(a*a);	  */  		  
							
							buffer[i] += (s4==0.0) ? 0:(calc_fi - s3 - s2 +s1)/s4;
							
							/*
							 double bbb = Math.sin(x);
							 if(bbb == 0) buffer[i] += 1;
							 else
							 {
							 double aaa = Math.sin(x*(2*N+1));
							 buffer[i] += (1/(2*N))*((aaa/bbb)-1);
							 } */
							
							double ss1_sin = s1_sin;
							s1_sin = ss1_sin * s1_m_cos + s1_cos * s1_m_sin; 
							s1_cos = s1_cos * s1_m_cos - ss1_sin * s1_m_sin; 				  		  
							double ss2_sin = s2_sin;
							s2_sin = ss2_sin * s2_m_cos + s2_cos * s2_m_sin; 
							s2_cos = s2_cos * s2_m_cos - ss2_sin * s2_m_sin; 				  		  
							double ss3_sin = s3_sin;
							s3_sin = ss3_sin * s3_m_cos + s3_cos * s3_m_sin; 
							s3_cos = s3_cos * s3_m_cos - ss3_sin * s3_m_sin; 			    		  
							
							x += f0;   		 
						}
					}
					else	    	
						for (int i = start+c; i < end; i+=channels) {
							
							
							double s1 = aa*a*Math.sin((N-1.0)*x+fi);
							double s2 = aa*Math.sin(N*x+fi);
							double s3 = a*Math.sin(x+fi);
							double s4 = 1.0 - (2*a*Math.cos(x)) +(a*a);	    		  
							
							
							buffer[i] += (s4==0.0) ? 0:(calc_fi - s3 - s2 +s1)/s4; 
							
							x += f0;   		 
						}
				}
				
			}
			this.x_counter = x % (Math.PI * 2);
			return end - start;
		}
		public int replace(double[] buffer, int start, int end) {
			/*
			 if(true)
			 {
			 Arrays.fill(buffer, start, end,0 );
			 return mix(buffer, start, end);
			 } */
			
			double f = DoublePart.asDouble(in_F);
			
			double f0 = Math.PI * 2 * f / session.getRate();
			//double N = Math.floor(session.rate / (2 * 200.0)) - 5;
			
			double a = DoublePart.asDouble(in_A);   // 0.9;  // a<1.0			
			double N = DoublePart.asDouble(in_N);   // 60;    // Harmonics
			
			double fi;
			if(in_FI == null)
				fi = Math.PI / 2;
			else
				fi = DoublePart.asDouble(in_FI); // Math.PI/2;   // Inital Phase
			
			//if(N == 0) N = Math.floor(session.rate / f);
			
			if(a > 0.999) a = 0.999;	
			
			boolean variable_data = !inputstream2_eof || !inputstream3_eof || !inputstream4_eof;
			
			int channels = session.getChannels();
			
			int cstart = start / channels;
			int cend = end / channels;
			
			if(variable_data)
			{
				/*
				 f0 = stockbuffer2[ix];
				 a = stockbuffer3[ix];
				 N = stockbuffer4[ix];
				 */
				variable_data = false;
				boolean static_2 = false;
				boolean static_3 = false;
				boolean static_4 = false;
				
				double[] cbuffer = new double[1]; 
				if(!inputstream2_eof)
				{
					int ret = inputstream2.isStatic(cbuffer, cend - cstart);
					if(ret != -1)
					{
						f0 = Math.PI * 2 * cbuffer[0] / session.getRate();
						static_2 = true;
					}	    		
					else
						variable_data = true;
				}
				if(!inputstream3_eof)
				{
					int ret = inputstream3.isStatic(cbuffer, cend - cstart);
					if(ret != -1)
					{
						a = cbuffer[0];
						static_2 = true;
					}	    	
					else
						variable_data = true;
					
				}
				if(!inputstream4_eof)
				{
					int ret = inputstream4.isStatic(cbuffer, cend - cstart);
					if(ret != -1)
					{
						N = cbuffer[0];
						static_2 = true;
					}	   
					else
						variable_data = true;
					
				}	    		
				
				//if(static_2 && static_3 && static_4) variable_data = false;
				if(variable_data)
				{
					if(static_2) inputstream2.fallBack();
					if(static_3) inputstream2.fallBack();
					if(static_4) inputstream2.fallBack();
				}
			}
			
			
			if(variable_data)
			{
				
				
				// F	    			    		
				if(!inputstream2_eof)
				{
					if(stockbuffer2 == null) stockbuffer2 = audiocache.getBuffer(cend);
					if(stockbuffer2.length < cend)
					{
						audiocache.returnBuffer(stockbuffer2);
						stockbuffer2 = audiocache.getBuffer(cend);
					}
					int ret = inputstream2.replace(stockbuffer2, cstart, cend);
					if(ret == -1)
					{
						Arrays.fill(stockbuffer2, f0);
						inputstream2_eof = true;	    				
					}
					else
					{
						
						double factor = Math.PI * 2 / session.getRate();
						for (int i = cstart; i < cstart + ret; i++) {
							stockbuffer2[i] *= factor;
						}
						Arrays.fill(stockbuffer2, cstart + ret, cend, f0);
					}
				}
				else
				{
					if(stockbuffer2==null? true : stockbuffer2.length < cend)
					{
						if(stockbuffer2 != null) audiocache.returnBuffer(stockbuffer2);
						stockbuffer2 = audiocache.getBuffer(cend);
						Arrays.fill(stockbuffer2, f0);
					}
				}
				
				
				// A
				if(!inputstream3_eof)
				{
					if(stockbuffer3 == null) stockbuffer3 = audiocache.getBuffer(cend);
					if(stockbuffer3.length < cend)
					{
						audiocache.returnBuffer(stockbuffer3);
						stockbuffer3 = audiocache.getBuffer(cend);
					}
					int ret = inputstream3.replace(stockbuffer3, cstart, cend);
					if(ret == -1)
					{
						inputstream3_eof = true;
						Arrays.fill(stockbuffer3, a);
					}
					else
						Arrays.fill(stockbuffer3, cstart + ret, cend, a);
				}
				else
				{
					if(stockbuffer3==null? true : stockbuffer3.length < cend)
					{
						if(stockbuffer3 != null) audiocache.returnBuffer(stockbuffer3);
						stockbuffer3 = audiocache.getBuffer(cend);
						Arrays.fill(stockbuffer3, a);
					}
				}
				
				// N
				
				if(!inputstream4_eof)
				{
					if(stockbuffer4 == null) stockbuffer4 = audiocache.getBuffer(cend);
					if(stockbuffer4.length < cend)
					{
						audiocache.returnBuffer(stockbuffer4);
						stockbuffer4 = audiocache.getBuffer(cend);
					}
					int ret = inputstream4.replace(stockbuffer4, cstart, cend);
					if(ret == -1)
					{
						Arrays.fill(stockbuffer4, N);
						inputstream4_eof = true;	    				
					}
					else
						Arrays.fill(stockbuffer4, cstart + ret, cend, N);
				}
				else
				{
					if(stockbuffer4==null? true : stockbuffer4.length < cend)
					{
						if(stockbuffer4 != null) audiocache.returnBuffer(stockbuffer4);	    				
						stockbuffer4 = audiocache.getBuffer(cend);
						Arrays.fill(stockbuffer4, N);
					}
				}
				
			}
			
			
			// F getur veri� breytanlegt
			// a getur veri� breytanlegt
			// N getur veir� breytanlegt , �.s. 3 Audio Stream breytur leyf�ar
			
			/*
			 double[] l_sintable = sintable;
			 int l_sintable_len = l_sintable.length;
			 double l_sintable_factor = sintable_factor;   			
			 
			 double table_fi = fi*l_sintable_factor;			
			 double table_pih2 = Math.PI*l_sintable_factor/2;			
			 */
			/*
			 
			 double table_x = l_sintable_factor * x;		    		  
			 double s1 = aa*a* l_sintable[((int)((N - 1.0)*table_x + table_fi)) % l_sintable_len];
			 double s2 = aa* l_sintable[((int)((N)*table_x + table_fi)) % l_sintable_len];
			 double s3 = a* l_sintable[((int)(table_x + table_fi)) % l_sintable_len];
			 double s4 = a*a + 1.0 - 2*a* l_sintable[((int)(table_x + table_pih2)) % l_sintable_len];
			 
			 
			 */
			
			double calc_fi = Math.sin(fi);			
			double x = 0;
			double aa = Math.pow(a,N);
			
			// FOR USE IN TAYLOR SIN
			/*
			 final double piD2 = Math.PI / 2;
			 final double NpiD2 = -piD2;
			 final double pi = Math.PI;
			 final double pi2 = Math.PI*2;
			 final double pi3 = Math.PI*3 +  Math.asin(1);*/
			
			
			for (int c = 0; c < channels; c++) {		
				x = this.x_counter;
				
				if(variable_data)
				{
					int ix = cstart;
					if(in_FI == null)
						for (int i = start+c; i < end; i+=channels) {
							
							
							f0 = stockbuffer2[ix];
							a = stockbuffer3[ix];
							N = stockbuffer4[ix];
							
							//N = 5.0;
							aa = Math.pow(a,N);
							//sin(a + b) = sin(a)cos(b) + cos(a)sin(b)
							//cos(a + b) = cos(a)cos(b) - sin(a)sin(b)
							
							
							double cosx = Math.cos(x);
							double s1 = aa*a*Math.cos((N-1.0)*x);
							double s2 = aa*Math.cos(N*x);
							double s3 = a*cosx;
							double s4 = 1.0 - (2*a*cosx) +(a*a);	    	
							
							/*
							 double cx = x;
							 {
							 cx = ((cx + pi3) % pi2) - pi;	    	  
							 if(cx > piD2) cx = pi - cx; else if(cx < NpiD2) cx = -pi - cx;	  
							 double cx2 = cx*cx;	    	  
							 cx = cx*(cx2*(cx2*(cx2*(cx2*(1.0/362880.0)
							 - (1.0/5040.0)) + (1.0/120.0))
							 - (1.0/6.0)) + 1.0);	 		
							 }
							 double cosx = cx;
							 
							 cx = (N-1.0)*x;
							 {
							 cx = ((cx + pi3) % pi2) - pi;	    	  
							 if(cx > piD2) cx = pi - cx; else if(cx < NpiD2) cx = -pi - cx;	  
							 double cx2 = cx*cx;	    	  
							 cx = cx*(cx2*(cx2*(cx2*(cx2*(1.0/362880.0)
							 - (1.0/5040.0)) + (1.0/120.0))
							 - (1.0/6.0)) + 1.0);	 		
							 }
							 double s1 = aa*a*cx;
							 
							 cx = N*x;
							 {
							 cx = ((cx + pi3) % pi2) - pi;	    	  
							 if(cx > piD2) cx = pi - cx; else if(cx < NpiD2) cx = -pi - cx;	  
							 double cx2 = cx*cx;	    	  
							 cx = cx*(cx2*(cx2*(cx2*(cx2*(1.0/362880.0)
							 - (1.0/5040.0)) + (1.0/120.0))
							 - (1.0/6.0)) + 1.0);	 		
							 }
							 double s2 = aa*cx;				    		  
							 
							 double s3 = a*cosx;
							 double s4 = 1.0 - (2*a*cosx) +(a*a);	
							 */
							
							buffer[i] = (s4==0.0) ? 0:(calc_fi - s3 - s2 +s1)/s4; 
							
							x += f0;
							ix++;
						}
					else	    	
						for (int i = start+c; i < end; i+=channels) {
							
							f0 = stockbuffer2[ix];
							a = stockbuffer3[ix];
							N = stockbuffer4[ix];
							
							double s1 = aa*a*Math.sin((N-1.0)*x+fi);
							double s2 = aa*Math.sin(N*x+fi);
							double s3 = a*Math.sin(x+fi);
							double s4 = 1.0 - (2*a*Math.cos(x)) +(a*a);	    		  
							
							
							buffer[i] = (s4==0.0) ? 0:(calc_fi - s3 - s2 +s1)/s4; 
							
							x += f0;   		
							ix++;
							
						}		    		
				}
				else
				{
					if(in_FI == null)
					{
						
						double s1_m_cos = Math.cos((N-1.0)*f0);
						double s1_m_sin = Math.sin((N-1.0)*f0);			
						double s2_m_cos = Math.cos(N*f0);
						double s2_m_sin = Math.sin(N*f0);			
						double s3_m_cos = Math.cos(f0);
						double s3_m_sin = Math.sin(f0);	
						
						double s1_sin = Math.sin((N-1.0)*x);
						double s2_sin = Math.sin(N*x);
						double s3_sin = Math.sin(x);
						double s1_cos = Math.cos((N-1.0)*x);
						double s2_cos = Math.cos(N*x);
						double s3_cos = Math.cos(x);
						
						for (int i = start+c; i < end; i+=channels) {
							
							
							
							//sin(a + b) = sin(a)cos(b) + cos(a)sin(b)
							//cos(a + b) = cos(a)cos(b) - sin(a)sin(b)
							
							double s1 = aa*a*s1_cos;
							double s2 = aa*s2_cos;
							double s3 = a*s3_cos;
							double s4 = 1.0 - (2*a*s3_cos) +(a*a);	  		  
							
							/*
							 double cosx = Math.cos(x);
							 double s1 = aa*a*Math.cos((N-1.0)*x);
							 double s2 = aa*Math.cos(N*x);
							 double s3 = a*cosx;
							 double s4 = 1.0 - (2*a*cosx) +(a*a);	  */  		  
							
							buffer[i] = (s4==0.0) ? 0:(calc_fi - s3 - s2 +s1)/s4;
							
							/*
							 double bbb = Math.sin(x);
							 if(bbb == 0) buffer[i] += 1;
							 else
							 {
							 double aaa = Math.sin(x*(2*N+1));
							 buffer[i] += (1/(2*N))*((aaa/bbb)-1);
							 } */
							
							double ss1_sin = s1_sin;
							s1_sin = ss1_sin * s1_m_cos + s1_cos * s1_m_sin; 
							s1_cos = s1_cos * s1_m_cos - ss1_sin * s1_m_sin; 				  		  
							double ss2_sin = s2_sin;
							s2_sin = ss2_sin * s2_m_cos + s2_cos * s2_m_sin; 
							s2_cos = s2_cos * s2_m_cos - ss2_sin * s2_m_sin; 				  		  
							double ss3_sin = s3_sin;
							s3_sin = ss3_sin * s3_m_cos + s3_cos * s3_m_sin; 
							s3_cos = s3_cos * s3_m_cos - ss3_sin * s3_m_sin; 			    		  
							
							x += f0;   		 
						}
					}
					else	    	
						for (int i = start+c; i < end; i+=channels) {
							
							
							double s1 = aa*a*Math.sin((N-1.0)*x+fi);
							double s2 = aa*Math.sin(N*x+fi);
							double s3 = a*Math.sin(x+fi);
							double s4 = 1.0 - (2*a*Math.cos(x)) +(a*a);	    		  
							
							
							buffer[i] = (s4==0.0) ? 0:(calc_fi - s3 - s2 +s1)/s4; 
							
							x += f0;   		 
						}
				}
				
			}
			this.x_counter = x % (Math.PI * 2);
			return end - start;
			
		}
		
		public int skip(int len)
		{
			return len;
		}
		
		public int isStatic(double[] buffer, int len) {
			return -1;
		}		
		
		public void close() {
			if(inputstream2 != null) inputstream2.close();
			if(inputstream3 != null) inputstream3.close();
			if(inputstream4 != null) inputstream4.close();
			
			if(stockbuffer2 != null) { audiocache.returnBuffer(stockbuffer2); stockbuffer2 = null; }
			if(stockbuffer3 != null) { audiocache.returnBuffer(stockbuffer3); stockbuffer3 = null; }
			if(stockbuffer4 != null) { audiocache.returnBuffer(stockbuffer4); stockbuffer4 = null; }
		}		
	}
	
	public AudioStream openStream(AudioSession session) {
		return new FilterStreamInstance(session);
	}
	
}

public class AudioDSF implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Buzz Generator");
		metadata.add(-1, "output",		"Output",		    null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1,  "f",		    "Frequency",		null, "Hz", MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2,  "a",		    "Amount",		    null, null, MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3,  "n",		    "Harmonics",		null, null, MetaData.TYPE_AUDIO_AND_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4,  "fi",		    "Inital Phase",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		return metadata;
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new AudioDSFInstance(parameters);
	}
}