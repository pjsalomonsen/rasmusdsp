/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter;

import java.util.ArrayList;
import java.util.Iterator;

public final class Variable {

	private VariablePart[] partarray = null;

	private ArrayList<Variable> variables = null;

	private Variable singlevar = null;

	public Variable() {
	}

	public Variable(VariablePart variablepart) {
		partarray = new VariablePart[1];
		partarray[0] = variablepart;
	}

	final public void add(Variable variable) {
		synchronized (this) {
			if (variables == null && singlevar == null) {
				singlevar = variable;
			} else {
				if (variables == null) {
					variables = new ArrayList<Variable>();
					variables.add(singlevar);
				}
				variables.add(variable);
				singlevar = null;
			}

			if (partarray != null)
				for (int i = 0; i < partarray.length; i++) {
					VariablePart variablepart = partarray[i];
					variablepart.add(variable);
				}

		}
	}

	final public void remove(Variable variable) {
		synchronized (this) {
			if (variables != null) {
				variables.remove(variable);
				if (variables.size() == 0)
					variables = null;
			} else {
				if (singlevar != null)
					if (singlevar == variable)
						singlevar = null;
			}

			if (partarray != null)
				for (int i = 0; i < partarray.length; i++) {
					VariablePart variablepart = partarray[i];
					variablepart.remove(variable);
				}

		}
	}

	final public void clear() {
		synchronized (this) {
			if (partarray != null)
				for (int i = 0; i < partarray.length; i++) {
					VariablePart variablepart = partarray[i];
					variablepart.clear();
				}
			partarray = null;
			variables = null;
			singlevar = null;
		}
	}

	final public boolean has(Class classobject) {
		synchronized (this) {
			if (partarray != null)
				for (int i = 0; i < partarray.length; i++)
					if (partarray[i].getClass() == classobject)
						return true;

			if (singlevar != null)
				if (singlevar.has(classobject))
					return true;

			if (variables != null) {
				Iterator iterator = variables.iterator();
				while (iterator.hasNext()) {
					Variable vvar = (Variable) iterator.next();
					if (vvar.has(classobject))
						return true;
				}
			}

			return false;
		}
	}

	final public VariablePart get(Class classobject) {
		synchronized (this) {
			if (partarray != null)
				for (int i = 0; i < partarray.length; i++)
					if (partarray[i].getClass() == classobject)
						return partarray[i];

			VariablePart o;
			try {
				o = (VariablePart) classobject.newInstance();
				o.init(this);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

			if (variables != null) {
				Iterator iterator = variables.iterator();
				while (iterator.hasNext())
					o.add((Variable) iterator.next());
			} else if (singlevar != null)
				o.add(singlevar);

			VariablePart[] new_partarray;
			if (partarray == null)
				new_partarray = new VariablePart[1];
			else {
				new_partarray = new VariablePart[partarray.length + 1];
				for (int i = 0; i < partarray.length; i++) {
					new_partarray[i] = partarray[i];
				}
				;
			}
			new_partarray[new_partarray.length - 1] = o;
			partarray = new_partarray;

			return o;
		}

	}

}