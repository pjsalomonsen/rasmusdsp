package rasmus.interpreter;

import java.util.Set;

public class NameSpaceAdapter implements NameSpace {
	
	private NameSpace namespace;
	
	public NameSpaceAdapter(NameSpace namespace)
	{
		this.namespace = namespace;
	}
	
	public void addToCommitStack(Commitable commitable)
	{
		namespace.addToCommitStack(commitable);
	}
	
	public NameSpaceAdapter()
	{
	}
	
	public void commit()
	{
		namespace.commit();
	}
	
	
	protected void setNameSpace(NameSpace namespace)
	{
		this.namespace = namespace;
	}

	public NameSpace getNameSpace()
	{
		return namespace;
	}
	
	public void registerAsPrivate(String name) {
		namespace.registerAsPrivate(name);
	}

	public void unRegisterAsPrivate(String name) {
		namespace.unRegisterAsPrivate(name);
	}

	public Variable get(String name) {
		return namespace.get(name);
	}

	public void getAllNames(Set set) {
		namespace.getAllNames(set);
	}

}
