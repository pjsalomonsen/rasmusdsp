/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.util.List;

import rasmus.interpreter.Closeable;
import rasmus.interpreter.Executable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.namespace.InheritNameSpace;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitInstancePart;

public class ScriptUnitFactoryInstance implements UnitInstancePart {
	private Closeable instance;

	public ScriptUnitFactoryInstance(Parameters parameters, List unit_params,
			List unit_defaults, Executable unit_body, NameSpace namespace, int unit_type) {
		NameSpace ns = parameters.getNameSpace();
		if (unit_type == 0) {
			InheritNameSpace unitns = new InheritNameSpace(namespace);
			ns = unitns;
			for (int i = 0; i < unit_params.size(); i++) {
				String paramname = (String) unit_params.get(i);
				Variable paramvar = parameters.getParameter(i, paramname);
				if (paramvar == null)
					paramvar = (Variable) unit_defaults.get(i);
				if (paramvar == null)
					paramvar = new Variable();
				unitns.put(paramname, paramvar);				
			}
		}
		
		instance = unit_body.execute(ns);
	}

	public void close() {
		instance.close();
		instance = null;
	}
}
