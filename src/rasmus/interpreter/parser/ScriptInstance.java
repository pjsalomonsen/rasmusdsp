/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.ExecutableInstance;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.namespace.InheritNameSpace;
import rasmus.interpreter.struct.Struct;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.Unit;
import rasmus.interpreter.unit.UnitInstance;

public class ScriptInstance implements ExecutableInstance
{		
	private Variable returnvariable;	
	private NameSpace namespace;
	
	private String[] regpriv;		
	private Variable[] added;		
	private Variable[] scriptunits;
	private ScriptInstance[] interpreters;
	private UnitInstance[] units;
	
	private InheritNameSpace inheritnamespace = null;
	
	private class NewTask implements Commitable 
	{
		String[] private_variabls;
		String[] vardef_Names;
		Variable[] vardef_Constants;
		int[] varadd_to;
		int[] varadd_from;
		int[] varunit;
		List<String>[] varunit_params;
		List<Integer>[] varunit_defaults;
		CompiledScript[] varunit_body;
		int[] varunit_types;
		int[] varcall;
		Map<String, Integer>[] varcall_params;
		String[] varnamespace_name;
		CompiledScript[] varnamespace_body;
		
		Variable[] variables ;
		
		public NewTask(NameSpace namespace, CompiledScript compiledscript)
		{
			private_variabls = compiledscript.private_variables;
			vardef_Names = compiledscript.vardef_Names;
			vardef_Constants = compiledscript.vardef_Constants;
			varadd_to = compiledscript.varadd_to;
			varadd_from = compiledscript.varadd_from;
			varunit = compiledscript.varunit;
			varunit_params = compiledscript.varunit_params;
			varunit_defaults = compiledscript.varunit_defaults;
			varunit_body = compiledscript.varunit_body;
			varunit_types = compiledscript.varunit_types;
			varcall = compiledscript.varcall;
			varcall_params = compiledscript.varcall_params;
			varnamespace_name = compiledscript.varnamespace_name;
			varnamespace_body = compiledscript.varnamespace_body;

			// 1
			// Names/Constants   
			variables = new Variable[vardef_Names.length];
			//boolean[] publicvar = new boolean[vardef_Names.length] ;  // ALLT PUBLICVAR remmed �taf DEBUG �st��um
			for (int i = 0; i < variables.length; i++) {
				String varname = vardef_Names[i];
				Variable constant = vardef_Constants[i];
				if (constant != null) {
					variables[i] = constant;
					//publicvar[i] = true;
				} else if (varname == null) {
					//publicvar[i] = false; 
					variables[i] = new Variable();
				} else {
					//publicvar[i] = true;
												
					StringTokenizer st = new StringTokenizer(varname, ".");
					
					Variable var;
					if(st.hasMoreTokens())
						var = namespace.get(st.nextToken());
					else
						var = new Variable();
					
					while (st.hasMoreTokens()) 
						var = Struct.get(var, st.nextToken());
					
					variables[i] = var;
				}
			}
			
			if (compiledscript.returnVar == -1)
				returnvariable = null;
			else
				returnvariable = variables[compiledscript.returnVar];
						
			
		}
		public int getRunLevel()
		{
			return RUNLEVEL_DEFAULT;
		}
		
		public void commit() {
			// 0
			// Private Variables
			
			regpriv = new String[private_variabls.length];
			
			for (int i = 0; i < private_variabls.length; i++) {
				String varname = private_variabls[i];
				int li = varname.indexOf(".");
				if(li != -1) varname = varname.substring(0, li);
				namespace.registerAsPrivate(varname);
				regpriv[i] = varname;
			}



			// 2
			// Variable Adding	   
			
			added = new Variable[(varadd_to.length + varunit.length) * 2];
			int added_index = 0;
			for (int i = 0; i < varadd_to.length; i++) {
				int v = varadd_to[i];
				Variable to = variables[v];
				Variable from = variables[varadd_from[i]];
				to.add(from);
				//if(publicvar[v])
				//{
				added[added_index++] = to;
				added[added_index++] = from;
				//}
			}

			// 3
			// Units
			scriptunits = new Variable[varunit.length];
			
			for (int i = 0; i < varunit.length; i++) {

				int v = varunit[i];
				Variable var = variables[v];

				Iterator iterator = varunit_defaults[i].iterator();
				List defaults = new ArrayList(varunit_defaults[i].size());
				while (iterator.hasNext()) {
					Integer varid = (Integer) iterator.next();
					if (varid == null)
						defaults.add(null);
					else
						defaults.add(variables[varid.intValue()]);
				}
				ScriptUnitFactory factory = new ScriptUnitFactory(
						varunit_params[i], defaults, varunit_body[i], namespace, varunit_types[i]);
				Variable scriptunit = Unit.asVariable(factory);
				scriptunits[i] = scriptunit;
				var.add(scriptunit);

				//if(publicvar[v])
				//{
				added[added_index++] = var;
				added[added_index++] = scriptunit;
				
				//}
			}

			// 4
			// Namespaces	   
			
			interpreters = new ScriptInstance[varnamespace_name.length];
			for (int i = 0; i < varnamespace_name.length; i++) {
				
				InheritNameSpace inheritnamespace = new InheritNameSpace(namespace, varnamespace_name[i]);
				interpreters[i] = new ScriptInstance(inheritnamespace, varnamespace_body[i]);
				interpreters[i].inheritnamespace = inheritnamespace;
			}

			
			// 5
			// Calls (slowest)
			units = new UnitInstance[varcall.length];
			for (int i = 0; i < varcall.length; i++) {
				Variable var = variables[varcall[i]];
				Map<String, Integer> params = varcall_params[i];
				Iterator<Map.Entry<String, Integer>> iterator = params
						.entrySet().iterator();

				Parameters uparams = new Parameters(namespace);
				while (iterator.hasNext()) {
					Map.Entry<String, Integer> entry = iterator.next();
					String pname = entry.getKey();
					Variable pvar = variables[entry.getValue()];
					uparams.setParameter(pname, pvar);

				}

				UnitInstance unitinstance = Unit.newInstance(var, uparams);
				units[i] = unitinstance;
			}

			
		}
		
	}
	
	public ScriptInstance(NameSpace namespace, CompiledScript compiledscript)
	{

		this.namespace = namespace;
		namespace.addToCommitStack(new NewTask(namespace, compiledscript));		
	}
	
	public Variable getReturnVariable()
	{
		return returnvariable;
	}
	
	public void close()
	{
		namespace.addToCommitStack(new Commitable()
		{
			
		public int getRunLevel()
		{
			return RUNLEVEL_DEFAULT;
		}
			
		public void commit()
		{
	
		for (int i = 0; i < units.length; i++) 
			units[i].close();			
		
		for (int i = 0; i < scriptunits.length; i++) 
			scriptunits[i].clear();			
		
		for (int i = 0; i < interpreters.length; i++) 
			interpreters[i].close();			
				
		for (int i = 0; i < regpriv.length; i++) 
			namespace.unRegisterAsPrivate(regpriv[i]);

		for (int i = 0; i < added.length; )
		{
			Variable to = added[i++];
			Variable from = added[i++];
			to.remove(from);
		}
		
		regpriv = null;
		added = null;		
		scriptunits = null;
		interpreters = null;
		units = null;			

		if (inheritnamespace != null)
		{
			inheritnamespace.close();
			inheritnamespace = null;
		}
		}
		});
	}
}
