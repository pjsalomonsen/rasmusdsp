/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.util.List;
import java.util.Map;

import rasmus.interpreter.Executable;
import rasmus.interpreter.ExecutableInstance;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;

public class CompiledScript implements Executable {
	public int returnVar = -1;	
	
	// 0.
	// Private Variables    ,  private
	public String[]             private_variables;
		
	// 1.
	// Names/Constants      ,  var_name = constant
	public String[] 			vardef_Names;
	public Variable[]          vardef_Constants;
	
	// 2.
	// Adding               ,  to <- from
	public int[]    			varadd_to;
	public int[]    			varadd_from;

	// 3.
	// Units                '  varunit(varunitparams) { varunit_body }
	public int[]    			varunit;
	public List<String>[]    	varunit_params;	
	public List<Integer>[]		varunit_defaults;
	public CompiledScript[]     varunit_body;
	public int[]			    varunit_types; // 0=function, 1=module
	
	// 4.                   
	// Calls                '  varcall(varcall_params)
	public int[]    				varcall;
	public Map<String, Integer>[] 	varcall_params;
	
	// 5.
	// NameSpaces           '  namespace("name") { varnamespace_body }
	public String[]    			varnamespace_name;
	public CompiledScript[]    varnamespace_body;
		
	public ExecutableInstance execute(NameSpace namespace) {
			return new ScriptInstance(namespace, this);
	}	
	
}
