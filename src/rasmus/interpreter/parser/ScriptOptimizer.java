/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ScriptOptimizer {
	
	public static ScriptElement optimize(ScriptElement element)
	{
		return foldConstants(expandPlusMinus(expandMinus(element)));
	}

	private static List binaryOperators = new ArrayList();
	private static List unaryOperators = new ArrayList();
	
	static 
	{
		binaryOperators.add("+");
		binaryOperators.add("-");
		binaryOperators.add("*");
		binaryOperators.add("/");
		binaryOperators.add("^");
		binaryOperators.add("pow");
		
		binaryOperators.add("min");
		binaryOperators.add("max");
		
		unaryOperators.add("-");
		
		unaryOperators.add("round");
		unaryOperators.add("floor");
		unaryOperators.add("rint");
		
		unaryOperators.add("abs");
		
		unaryOperators.add("exp");
		unaryOperators.add("log");		
	
		unaryOperators.add("sin");
		unaryOperators.add("cos");
		unaryOperators.add("tan");
		
		unaryOperators.add("asin");
		unaryOperators.add("acos");
		unaryOperators.add("atan");		
	}
	
	public static ScriptElement foldConstants(ScriptElement element) {
		
		List<ScriptElement> elements = element.getElements();
		if(elements != null)		
			for (int i = 0; i < elements.size(); i++) 
				elements.set(i, foldConstants(elements.get(i)));
		
		element = foldNumberConstants(element);
		element = foldStringConstants(element);
		return element;
	}
		
	public static ScriptElement foldNumberConstants(ScriptElement element) {
		
		if(element.getType() != ScriptElement.TYPE_CALL) return element;
		List<ScriptElement> elements = element.getElements();
		if(elements == null) return element;

		if(element.getValue() == null) return element;			
		String callname = ((String)element.getValue()).toLowerCase();
		
		if(callname.equals("+"))
		{
			// multi parameter plus operator
			Double number = null;
			ArrayList<ScriptElement> extraparameters = null;
			for (int i = 0; i < elements.size(); i++) 
				if(elements.get(i).getType() == ScriptElement.TYPE_PARAMETER)
				{
					ScriptElement param = elements.get(i);
					if(param.getElements() != null)
					if(param.getElements().size() == 1)
					{
						ScriptElement paramvalue = param.getElements().get(0);
						if(paramvalue.getType() == ScriptElement.TYPE_NUMBER) 
						{
							if(number == null)
							{
								number = (Double)paramvalue.getValue();
							}
							else
							{
								number += (Double)paramvalue.getValue();
							}
						}
						else
						{
							if(extraparameters == null) extraparameters = new ArrayList<ScriptElement>();
							extraparameters.add(paramvalue);
						}
					}						
				}
			
			if(number == null) return element;
			if(extraparameters == null)
			{
				return new ScriptElement(ScriptElement.TYPE_NUMBER, number);
			}
			if(number != null)
			{
				extraparameters.add(new ScriptElement(ScriptElement.TYPE_NUMBER, number));
			}
			
			element = new ScriptElement(ScriptElement.TYPE_CALL, "+");
			int paramno = 1;
			Iterator<ScriptElement> iterator = extraparameters.iterator();
			while(iterator.hasNext())
			{
				 ScriptElement scriptelement = iterator.next();
				 ScriptElement param = new ScriptElement(ScriptElement.TYPE_PARAMETER, Integer.toString(paramno));
				 param.add(scriptelement);
				 element.add(param);
				 paramno++;
			}			
			return element;
		}
		
		
		ScriptElement param1 = null;
		ScriptElement param2 = null;
		for (int i = 0; i < elements.size(); i++) 
			if(elements.get(i).getType() == ScriptElement.TYPE_PARAMETER)
			{
				ScriptElement param = elements.get(i);
			    if(param.getValue() == null) return element;			    
			    if(param.getElements() == null) return element;
			    if(param.getElements().size() != 1) return element;
			    
			    String paramname = (String)param.getValue();
			    ScriptElement paramvalue = param.getElements().get(0);
			    if(paramvalue.getType() != ScriptElement.TYPE_NUMBER) return element;
			    if(paramname.equals("1"))
			    	param1 = paramvalue;
			    else if(paramname.equals("2"))
			    	param2 = paramvalue;
			}
		
						

		Double ret = null;
		
		if(unaryOperators.contains(callname) && param1 != null && param2 == null)
		{
			double p1 = (Double)param1.getValue();
			
			     if(callname.equals("-")) ret = -p1;			
			else if(callname.equals("round")) ret = (double)Math.round(p1);
			else if(callname.equals("floor")) ret = Math.floor(p1);
			else if(callname.equals("rint")) ret = Math.rint(p1);			
			else if(callname.equals("abs")) ret = Math.abs(p1);			
			else if(callname.equals("exp")) ret = Math.exp(p1);
			else if(callname.equals("log")) ret = Math.log(p1);			
			else if(callname.equals("sin")) ret = Math.sin(p1);
			else if(callname.equals("cos")) ret = Math.cos(p1);
			else if(callname.equals("tan")) ret = Math.tan(p1);
			else if(callname.equals("asin")) ret = Math.asin(p1);
			else if(callname.equals("acos")) ret = Math.acos(p1);
			else if(callname.equals("atan")) ret = Math.atan(p1);
		}		
		else		
		if(binaryOperators.contains(callname)&& param1 != null && param2 != null)
		{
			double p1 = (Double)param1.getValue(); 			
			double p2 = (Double)param2.getValue(); 			

			     if(callname.equals("+")) ret = p1+p2;
			else if(callname.equals("-")) ret = p1-p2;
			else if(callname.equals("*")) ret = p1*p2;
			else if(callname.equals("/")) ret = p1/p2;
			else if(callname.equals("^") || 
			        callname.equals("pow")) ret = Math.pow(p1,p2);			
			else if(callname.equals("min")) ret = Math.min(p1,p2);
			else if(callname.equals("max")) ret = Math.max(p1,p2);
		}
		
		if(ret == null) return element; 
		return new ScriptElement(ScriptElement.TYPE_NUMBER, ret);								
		
	}
	
	public static ScriptElement foldStringConstants(ScriptElement element) {
		
		if(element.getType() != ScriptElement.TYPE_CALL) return element;
		List<ScriptElement> elements = element.getElements();
		if(elements == null) return element;

		if(element.getValue() == null) return element;			
		String callname = ((String)element.getValue()).toLowerCase();
		
		if(!callname.equals("&")) return element;
		
		ScriptElement param1 = null;
		ScriptElement param2 = null;
		for (int i = 0; i < elements.size(); i++) 
			if(elements.get(i).getType() == ScriptElement.TYPE_PARAMETER)
			{
				ScriptElement param = elements.get(i);
			    if(param.getValue() == null) return element;			    
			    if(param.getElements() == null) return element;
			    if(param.getElements().size() != 1) return element;
			    
			    String paramname = (String)param.getValue();
			    ScriptElement paramvalue = param.getElements().get(0);
			    if(paramvalue.getType() != ScriptElement.TYPE_STRING) return element;
			    if(paramname.equals("1"))
			    	param1 = paramvalue;
			    else if(paramname.equals("2"))
			    	param2 = paramvalue;
			}
		
						
		if(callname.equals("&")) 
		{
			String ret = (String)param1.getValue() + (String)param2.getValue();
			return new ScriptElement(ScriptElement.TYPE_STRING, ret);	
		}
		return element;

	}	
	
	
	public static ScriptElement expandPlusMinus(ScriptElement element) {
		
		List<ScriptElement> elements = element.getElements();
		if(elements != null)		
			for (int i = 0; i < elements.size(); i++) 
				elements.set(i, expandPlusMinus(elements.get(i)));
		
		
		if(element.getType() == ScriptElement.TYPE_CALL) 
		{
			if(element.getValue().equals("-"))
			if(elements.size() > 1)
			{
											
				for (int i = 0; i < elements.size(); i++) 
					if(elements.get(i).getType() == ScriptElement.TYPE_PARAMETER)
					{
						
						ScriptElement param = elements.get(i);
						String paramname = (String)param.getValue();
						if(!paramname.equals("1"))
						{													
							List<ScriptElement> subelements = param.getElements();
							if(subelements != null)		
								for (int j = 0; j < subelements.size(); j++) 
								{
									ScriptElement subelement = new ScriptElement(ScriptElement.TYPE_CALL, "-");
									ScriptElement subparam = new ScriptElement(ScriptElement.TYPE_PARAMETER, "1");
									subparam.add(subelements.get(j));
									subelement.add(subparam);									
									subelements.set(j, subelement);
								}
														
						}
					}
						
				element = new ScriptElement(ScriptElement.TYPE_CALL, "+");				
				if(elements != null)		
					for (int i = 0; i < elements.size(); i++) 				
						element.add(elements.get(i));							
			}
						
			if(element.getValue().equals("+"))
			{
				 elements = element.getElements();
				 ArrayList<ScriptElement> newelements = new ArrayList<ScriptElement>();
				 for (int i = 0; i < elements.size(); i++) 
					if(elements.get(i).getType() == ScriptElement.TYPE_PARAMETER)
						{
							ScriptElement param = elements.get(i);
							List<ScriptElement> subelements = param.getElements();
							if(subelements != null)
							if(subelements.size() == 1)
							{
								ScriptElement subelement = subelements.get(0);
								if(subelement.getType() == ScriptElement.TYPE_CALL && subelement.getValue().equals("+"))
								{
									List<ScriptElement> subsubelements = subelement.getElements();
									for (int j = 0; j < subsubelements.size(); j++) {
										ScriptElement subsubelement = subsubelements.get(j);
										if(subsubelement.getElements() != null)
										if(subsubelement.getElements().size() == 1)
										{
											newelements.add(subsubelement.getElements().get(0));
										}
									}
								}
								else
									newelements.add(subelement);
							}
						
						}
				 
				 elements.clear();
				 Iterator<ScriptElement> iterator = newelements.iterator();				 
				 int paramno = 1;
				 while (iterator.hasNext()) {
					 ScriptElement scriptelement = iterator.next();
					 ScriptElement param = new ScriptElement(ScriptElement.TYPE_PARAMETER, Integer.toString(paramno));
					 param.add(scriptelement);
					 element.add(param);
					 paramno++;
				}
				
			}
		}		
		return element;
	}
			
	

	public static ScriptElement expandMinus(ScriptElement element) {
		
		List<ScriptElement> elements = element.getElements();
		if(elements != null)		
			for (int i = 0; i < elements.size(); i++) 
				elements.set(i, expandMinus(elements.get(i)));
		
		
		if(element.getType() == ScriptElement.TYPE_CALL) 
		{
			if(element.getValue().equals("-"))
			if(elements.size() > 1)
			{

				 elements = element.getElements();
				 ArrayList<ScriptElement> newelements = new ArrayList<ScriptElement>();
				 for (int i = 0; i < elements.size(); i++) 
					if(elements.get(i).getType() == ScriptElement.TYPE_PARAMETER)
						{
							ScriptElement param = elements.get(i);
							String paramname = (String)param.getValue();							
							List<ScriptElement> subelements = param.getElements();
							if(subelements != null)
							if(subelements.size() == 1)
							{
								ScriptElement subelement = subelements.get(0);
								
								if(paramname.equals("1"))
								{													
								
								if(subelement.getType() == ScriptElement.TYPE_CALL 
									&& subelement.getValue().equals("-")
									&& subelement.getElements() != null
									&& subelement.getElements().size() != 1)
								{
									List<ScriptElement> subsubelements = subelement.getElements();
									for (int j = 0; j < subsubelements.size(); j++) {
										ScriptElement subsubelement = subsubelements.get(j);
										if(subsubelement.getElements() != null)
										if(subsubelement.getElements().size() == 1)
										{
											newelements.add(subsubelement.getElements().get(0));
										}
									}
								}
								else
									newelements.add(subelement);
								
								}
								else
									newelements.add(subelement);
							}
						
						}
				 
				 elements.clear();
				 Iterator<ScriptElement> iterator = newelements.iterator();				 
				 int paramno = 1;
				 while (iterator.hasNext()) {
					 ScriptElement scriptelement = iterator.next();
					 ScriptElement param = new ScriptElement(ScriptElement.TYPE_PARAMETER, Integer.toString(paramno));
					 param.add(scriptelement);
					 element.add(param);
					 paramno++;
				}				
				
			}
		}
				
		return element;
		
	}
	

}
