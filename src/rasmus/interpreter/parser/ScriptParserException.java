package rasmus.interpreter.parser;

public class ScriptParserException extends Exception {
	private static final long serialVersionUID = 1L;

	int line;

	int col;

	int offset;

	String message;

	String detail;

	public ScriptParserException(int offset, int line, int col, String message, String detail) {
		this.offset = offset;
		this.message = message;
		this.line = line ;
		this.col = col;
		this.detail = detail;
	}

	public String getMessage() {
		return detail + message;
	}

	public int getLine() {
		return line;
	}

	public int getCol() {
		return col;
	}
}
