/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ScriptElement {
	
	final public static int TYPE_BODY = 0;

	final public static int TYPE_ASSIGNMENT = 1;

	final public static int TYPE_CALL = 2;

	final public static int TYPE_PARAMETER = 3;

	final public static int TYPE_STRING = 4;

	final public static int TYPE_NUMBER = 5;

	final public static int TYPE_IDENTIFIER = 6;

	final public static int TYPE_UNIT = 7;
	
	private static List typelist = new ArrayList();
	static {
		typelist.add("body");
		typelist.add("assignment");
		typelist.add("call");
		typelist.add("parameter");
		typelist.add("string");
		typelist.add("number");
		typelist.add("identifier");
		typelist.add("unit");
	}

	private int type = 0;

	private Object value = null;

	private ArrayList<ScriptElement> elements = null;
	
	public List<ScriptElement> getElements()
	{
		return elements;
	}
	
	public Object getValue()
	{
		return value;
	}
		
	public int getType()
	{
		return type;
	}

	public ScriptElement(int type, Object value) {
		this.type = type;
		this.value = value;
	}

	public ScriptElement(int type) {
		this.type = type;
	}

	public void add(ScriptElement element) {
		if (elements == null)
			elements = new ArrayList<ScriptElement>();
		if(element == null)
		{
			throw new NullPointerException();
		}
		elements.add(element);
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		toString(sb, 0);
		return sb.toString();
	}

	private void toString(StringBuffer sb, int ident) {
		for (int i = 0; i < ident; i++)
			sb.append(' ');

		sb.append(typelist.get(type) + ": ");
		if (value != null)
			sb.append(value);
		
		sb.append('\n');

		if (elements != null) {
			Iterator iter = elements.iterator();
			while (iter.hasNext()) {
				ScriptElement childelement = (ScriptElement) iter.next();
				childelement.toString(sb, ident + 3);
			}
		}
	}
	
	public String toXML() {
		StringBuffer sb = new StringBuffer();
		toXML(sb, 0);
		return sb.toString();
	}	
	
	private void toXML(StringBuffer sb, int ident) {
		for (int i = 0; i < ident; i++)
			sb.append(' ');

		sb.append("<" + typelist.get(type));
		if (value != null)
			sb.append(" value='" + value.toString()+"'");

		if (elements != null) {			
			sb.append(">");			
			sb.append('\n');
			Iterator iter = elements.iterator();
			while (iter.hasNext()) {
				ScriptElement childelement = (ScriptElement) iter.next();
				childelement.toXML(sb, ident + 3);
			}
			
			for (int i = 0; i < ident; i++)
				sb.append(' ');
			
			sb.append("</" + typelist.get(type) + ">");
			sb.append('\n');			
		}
		else
		{
			sb.append("/>");			
			sb.append('\n');			
		}
			
					
	}	
	
}
