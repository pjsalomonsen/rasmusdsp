/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.util.Enumeration;
import java.util.NoSuchElementException;

public class ScriptTokenizer implements Enumeration {

	private String str;
	private String token;
	private int index = 0;
	private int lastindex = -1;
	private int strlastindex = -1;
	private int length;
	private int status = ScriptTokenParser.STATUS_NORMAL;
	
	public ScriptTokenizer clone()
	{
		ScriptTokenizer st = new ScriptTokenizer(str);
		st.index = index;
		st.lastindex = lastindex;
		st.strlastindex = strlastindex;
		st.status = status;
		st.token = token;
		return st;		
	}
		
	public ScriptTokenizer(String str)
	{
		this.str = str;
		length = str.length();
		token = parseNext();
	}
	
	public boolean hasMoreElements() {
		return hasMoreTokens();
	}

	public Object nextElement() {
		return nextToken();
	}
		
	public boolean hasMoreTokens() {
		return token != null;
	}
	
	public String nextToken()
	{
		if(token == null) throw new NoSuchElementException();
		String ret = token;			
		strlastindex = lastindex;
		token = parseNext();
		return ret;
	}
	
	public int lastIndex()
	{
		return strlastindex;
	}

	public int nextIndex()
	{
		return lastindex;
	}
	
	public String peekNextToken()
	{
		if(token == null) throw new NoSuchElementException();
		return token;		
	}
	
	private String parseNext()
	{
		for (; index < length; index++) {
			char c = str.charAt(index);
			status = ScriptTokenParser.parseStatus(c, status);
			if(status == ScriptTokenParser.STATUS_PRE_COMMENTS)
			{
				if(index + 1 < length)
				{
					int nextstatus = ScriptTokenParser.parseStatus(str.charAt(index+1), status);
					if(ScriptTokenParser.isCommentStatus(nextstatus))
					{
						index++;
						for (; index < length; index++) {
							c = str.charAt(index);
							status = ScriptTokenParser.parseStatus(c, status);
							if(status == ScriptTokenParser.STATUS_NORMAL) 
								break;
						}
						continue;
					}
					else
						if(ScriptTokenParser.isStringStatus(nextstatus))
						{
							int startindex = index;
							index++;
							for (; index < length; index++) {
								c = str.charAt(index);
								status = ScriptTokenParser.parseStatus(c, status);
								if(status == ScriptTokenParser.STATUS_NORMAL) break;
							}			
							index++;
							lastindex = startindex;
							return str.substring(startindex, index);	
						}
				}
			}			
			if(!(Character.isWhitespace(c) || c == (char)160))
			{
				if(ScriptTokenParser.isStringStatus(status))
				{
					int startindex = index;
					index++;
					for (; index < length; index++) {
						c = str.charAt(index);
						status = ScriptTokenParser.parseStatus(c, status);
						if(status == ScriptTokenParser.STATUS_NORMAL) break;
					}			
					index++;
					lastindex = startindex;
					return str.substring(startindex, index);
				}
				else
				{
					if(ScriptTokenParser.symbols.indexOf(c) == -1)
					{
						int startindex = index;
						for (; index < length; index++) {
							c = str.charAt(index);		
							if((Character.isWhitespace(c) || c == (char)160)) break;
							if(ScriptTokenParser.symbols.indexOf(c) != -1) break;
							if(ScriptTokenParser.parseStatus(c, status) != ScriptTokenParser.STATUS_NORMAL) break;						
							if(ScriptTokenParser.symbols.indexOf(c) != -1) break;																
						}
						lastindex = startindex;
						return str.substring(startindex, index);
					}		
					if(c == '<' || c == '>' || c == '!')
					{
						if(index + 1 < length)
						{
							String ret = str.substring(index, index + 2);						
							if(ret.equals("<-") || ret.equals("<=") || ret.equals(">=") || ret.equals("!=")) 
							{
								index += 2;
								lastindex = index;
								return ret;
							}
						}
					}			
					String ret = str.substring(index, index+1);
					lastindex = index;
					index++;				
					return ret;
				}				
			}
		}
		lastindex = -1;
		return null;
	}
	

}
