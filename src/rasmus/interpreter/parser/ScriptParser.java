/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ScriptParser {

	public static ScriptElement parse(String script)
			throws ScriptParserException {
		ScriptParser parser = new ScriptParser(script);
		return parser.root;
	}

	private int[] extractLineCol(int index) {
		int line = 1;
		int col = 1;
		char lastc = ' ';
		for (int i = 0; i < index; i++) {
			char c = script.charAt(i);
			if (c == '\n' || c == '\r') {
				if (!((lastc == '\n' || lastc == '\r') && lastc != c)) {
					line++;
					col = 1;
				} else
					c = ' ';
			} else {
				col++;
			}

			lastc = c;
		}

		int[] linecol = new int[2];
		linecol[0] = line;
		linecol[1] = col;
		return linecol;
	}

	private ScriptParserException makeException(int offset, String message) {
		int[] linecol = extractLineCol(offset);
		int line = linecol[0];
		int col = linecol[1];

		int firstindex = index - 20;
		if (firstindex < 0)
			firstindex = 0;
		String detail = script.substring(firstindex, index);
		detail += " <-- !!! \n";
		detail += "line " + line + ", col = " + col + ", offset = " + offset
				+ " : ";

		return new ScriptParserException(offset, line, col, message, detail);
	}

	private ScriptTokenizer tokenzier;

	private ScriptElement root = new ScriptElement(ScriptElement.TYPE_BODY);

	private int index = 0;

	private String script;

	private ScriptParser(String script) throws ScriptParserException {
		tokenzier = new ScriptTokenizer(script);
		this.script = script;
		parseScript(root);
	}

	private Double parseNumber(String token) throws ScriptParserException {
		try {

			if (token.startsWith("0x")) {
				return (double) Long.parseLong(token.substring(2), 16);
			}

			double identity = 1;
			while (token.endsWith("%")) {
				identity *= 0.01;
				token = token.substring(0, token.length() - 1);
			}

			return identity * Double.parseDouble(token);

		} catch (Throwable t) {
			throw makeException(index, t.getMessage());
		}
	}

	private ScriptElement parseValueToken(String token)
			throws ScriptParserException {
		if (token == null)
			return null;
		if (token.startsWith("/\""))
		{
			if(!token.endsWith("\"/")) throw makeException(index, "String ending is missing.");
			return new ScriptElement(ScriptElement.TYPE_STRING, token
					.substring(2, token.length() - 2));			
		}
		if (token.startsWith("/\'"))
		{
			if(!token.endsWith("\'/")) throw makeException(index, "String ending is missing.");
			return new ScriptElement(ScriptElement.TYPE_STRING, token
					.substring(2, token.length() - 2));			
		} 
		if (token.startsWith("\"") || token.startsWith("\'")) {
			if (!token.endsWith(token.substring(0, 1)))
				throw makeException(index, "String ending is missing.");
			return new ScriptElement(ScriptElement.TYPE_STRING, token
					.substring(1, token.length() - 1));
		} else if (ScriptTokenParser.isNumber(token)) {
			return new ScriptElement(ScriptElement.TYPE_NUMBER,
					parseNumber(token));
		}
		if (token.length() == 1) {

			char c = token.charAt(0);
			if (c == '(') {
				return parseExpression(")", false);
			}
			if (c == '!' || c == '-') {
				ScriptElement callelement = new ScriptElement(
						ScriptElement.TYPE_CALL, token);
				ScriptElement param = new ScriptElement(
						ScriptElement.TYPE_PARAMETER, "1");
				callelement.add(param);
				if (!tokenzier.hasMoreTokens())
					throw makeException(index, "Was expecting value token.");
				token = tokenzier.nextToken();
				index = tokenzier.lastIndex();
				ScriptElement valueelement = parseValueToken(token);
				if (valueelement == null)
					throw makeException(index, "Was expecting value token.");
				param.add(valueelement);
				return callelement;
			}

			if (ScriptTokenParser.symbols.indexOf(token.charAt(0)) != -1)
				return null;
		} else if (token.length() == 2) {
			if (token.equals("<-"))
				return null;
			else if (token.equals(">="))
				return null;
			else if (token.equals("<="))
				return null;
			else if (token.equals("!="))
				return null;
		}
		
		try
		{			
			if(tokenzier.peekNextToken().equals("("))
			{
				String value = token.toLowerCase();			
				if (!value.equals("function") 
				&& !value.equals("unit")
				&& !value.equals("module")
				&& !value.equals("namespace"))
				{   
					tokenzier.nextToken();
					ScriptElement callelement = new ScriptElement(
						ScriptElement.TYPE_CALL, token);
					parseParameters(callelement);
					return callelement;
				}
			}
		}
		catch(NoSuchElementException e)
		{			
		}
		

		return new ScriptElement(ScriptElement.TYPE_IDENTIFIER, token);
	}

	private ArrayList infixOperators = new ArrayList();
	{
		infixOperators.add("^");
		infixOperators.add("/");
		infixOperators.add("*");
		infixOperators.add("-");
		infixOperators.add("+");
		infixOperators.add("&");
		infixOperators.add("<");
		infixOperators.add(">");
		infixOperators.add("<=");
		infixOperators.add(">=");
		infixOperators.add("!=");
		infixOperators.add("=");
	}

	private void processOperator(List objects, List operators) {
		int opr = (Integer) operators.remove(operators.size() - 1);
		String oprvalue = (String) infixOperators.get(opr);

		ScriptElement right_object = (ScriptElement) objects.remove(objects
				.size() - 1);
		ScriptElement left_object = (ScriptElement) objects.remove(objects
				.size() - 1);

		ScriptElement callelement = new ScriptElement(ScriptElement.TYPE_CALL,
				oprvalue);

		ScriptElement param;

		if (left_object == null) {
			left_object = right_object;
			right_object = null;
		}

		if (left_object != null) {
			param = new ScriptElement(ScriptElement.TYPE_PARAMETER, "1");
			param.add(left_object);
			callelement.add(param);
		}

		if (right_object != null) {
			param = new ScriptElement(ScriptElement.TYPE_PARAMETER, "2");
			param.add(right_object);
			callelement.add(param);
		}

		objects.add(callelement);
	}

	private String lastendtoken = null;

	private void parseParameters(ScriptElement parent)
			throws ScriptParserException {
		int paramno = 1;

		while (tokenzier.hasMoreTokens()) {
			String paramname = null;

			ScriptTokenizer st = tokenzier.clone();
			if (st.hasMoreTokens()) {
				String nametoken = st.nextToken();
				if (st.hasMoreTokens()) {
					if (st.nextToken().equals("=")) {
						if (!nametoken.equals("("))
							if (!infixOperators.contains(nametoken))
								if (!(nametoken.length() == 1 && ScriptTokenParser.symbols
										.indexOf(nametoken.charAt(0)) != -1)) {
									paramname = nametoken.toLowerCase();
									tokenzier.nextToken();
									tokenzier.nextToken();
								}
					}
				}
			}

			if (paramname == null) {
				paramname = Integer.toString(paramno);
				paramno++;
			}

			ScriptElement expression = parseExpression(",)", false);

			if (expression != null) {
				ScriptElement param = new ScriptElement(
						ScriptElement.TYPE_PARAMETER, paramname);
				param.add(expression);
				parent.add(param);
			}

			if (lastendtoken.equals(")"))
				return;
		}

		throw makeException(index, "Was expecting ')' token.");

	}

	private void parseUnitParameters(ScriptElement parent)
			throws ScriptParserException {

		while (tokenzier.hasMoreTokens()) {

			String token = tokenzier.nextToken();
			if (token.equals(")"))
				return;

			if (infixOperators.contains(token))
				throw makeException(index, "Wasn't expecting '" + token
						+ "' token.");

			String paramname = token;

			if (!tokenzier.hasMoreTokens())
				throw makeException(index, "Was expecting ')' or ',' token.");

			token = tokenzier.nextToken();

			if (token.equals(")") || token.equals(",")) {
				ScriptElement param = new ScriptElement(
						ScriptElement.TYPE_PARAMETER, paramname);
				parent.add(param);
				if (token.equals(")"))
					return;
				if (token.equals(","))
					continue;
			}

			if (!token.equals("="))
				throw makeException(index,
						"Was expecting ')', ',' or '=' token.");

			ScriptElement expression = parseExpression(",)", false);

			if (expression == null)
				throw makeException(index, "Was expecting expression.");

			ScriptElement param = new ScriptElement(
					ScriptElement.TYPE_PARAMETER, paramname);
			param.add(expression);
			parent.add(param);

			if (lastendtoken.equals(")"))
				return;
		}

		throw makeException(index, "Was expecting ')' token.");

	}

	private ScriptElement parseUnit(String unittype)
			throws ScriptParserException {

		ScriptElement unitobject = new ScriptElement(ScriptElement.TYPE_UNIT,
				unittype);

		if (unittype.equals("module")) {
			if (!tokenzier.hasMoreTokens())
				throw makeException(index, "Was expecting ')' token.");
			String token = tokenzier.nextToken();
			if (!token.equals(")"))
				throw makeException(index, "Was expecting ')' token.");
		} else
			parseUnitParameters(unitobject);

		ScriptElement body = new ScriptElement(ScriptElement.TYPE_BODY);
		unitobject.add(body);

		if (!tokenzier.hasMoreTokens())
			throw makeException(index, "Was expecting '{' token.");
		String token = tokenzier.nextToken();
		if (!token.equals("{"))
			throw makeException(index, "Was expecting '{' token.");

		while (tokenzier.hasMoreTokens()) {
			ScriptElement expression = parseExpression(";}", false);
			if (expression != null)
				body.add(expression);
			if (lastendtoken == null || (lastendtoken.equals("}")))
				break;
		}

		if (lastendtoken == null || (!lastendtoken.equals("}"))) {
			throw makeException(index, "Was expecting '}' token.");
		}

		return unitobject;
	}

	private ScriptElement parseExpression(String endtoken, boolean eofok)
			throws ScriptParserException {

		ArrayList objects = new ArrayList();
		ArrayList operators = new ArrayList();

		boolean nextIsOperator = false;
		boolean eofend = true;

		while (tokenzier.hasMoreTokens()) {
			String token = tokenzier.nextToken();
			index = tokenzier.lastIndex();

			if (endtoken.indexOf(token) != -1) {
				lastendtoken = token;
				eofend = false;
				break;
			}

			if (!nextIsOperator) {
				if (token.equals("<-")) {
					objects.add(new ScriptElement(ScriptElement.TYPE_IDENTIFIER, "output"));
					nextIsOperator = true;
				}
			}

			if (nextIsOperator) {

				if (token.equals("<-")) {
					/*
					 * if (endtoken.indexOf(";") == -1) throw new
					 * ScriptParserException(index, "Wasn't expecting '<-'
					 * token.");
					 */

					if (objects.size() == 0) {
						throw makeException(index,
								"Wasn't expecting '<-' token.");
					}

					ScriptElement lastobject = (ScriptElement) objects
							.get(objects.size() - 1);

					if (lastobject.getType() == ScriptElement.TYPE_CALL) {
						ScriptElement param = new ScriptElement(
								ScriptElement.TYPE_PARAMETER, "input");
						param.add(parseExpression(endtoken, false));
						lastobject.add(param);
						eofend = false;
						break;
					} else if (lastobject.getType() == ScriptElement.TYPE_IDENTIFIER) {
						objects.remove(objects.size() - 1);
						ScriptElement assign = new ScriptElement(
								ScriptElement.TYPE_ASSIGNMENT, lastobject
										.getValue());
						int preindex = index;
						ScriptElement expr = parseExpression(endtoken, false);
						if (expr == null)
							throw makeException(preindex,
									"Was expecting ';' token.");

						assign.add(expr);
						objects.add(assign);
						eofend = false;
						break;
					} else
						throw makeException(index,
								"Wasn't expecting '<-' token.");

				}

				if (token.equals("(")) {
					ScriptElement lastobject = (ScriptElement) objects
							.remove(objects.size() - 1);

					if (lastobject.getType() != ScriptElement.TYPE_IDENTIFIER)
						throw makeException(index,
								"Wasn't expecting '(' token.");

					String value = ((String) lastobject.getValue())
							.toLowerCase();
					if (value.equals("function") || value.equals("unit")
							|| value.equals("module")
							|| value.equals("namespace")) {
						ScriptElement unit = parseUnit(value);
						if (unit == null)
							throw makeException(index, "Was expecting unit.");

						objects.add(unit);
						nextIsOperator = true;
						if(endtoken.indexOf(';') != -1)
						{
							lastendtoken = ";";							
							eofend = false;
							break;
						}
					}

					ScriptElement callelement = new ScriptElement(
							ScriptElement.TYPE_CALL, lastobject.getValue());

					parseParameters(callelement);

					objects.add(callelement);

					nextIsOperator = true;

					continue;
				}

				int tokenorder = infixOperators.indexOf(token);
				if (tokenorder == -1)
					throw makeException(index, "Was expecting operator.");

				while (operators.size() != 0
						&& ((Integer) operators.get(operators.size() - 1) <= tokenorder))
					processOperator(objects, operators);

				operators.add(tokenorder);

				nextIsOperator = false;
			} else {

				ScriptElement valueelement = parseValueToken(token);
				if (valueelement == null)
					throw makeException(index, "Was expecting value token.");
				objects.add(valueelement);
				nextIsOperator = true;
			}
		}

		if (objects.size() == 0)
			return null;

		if (eofend)
			if (!eofok) {
				throw makeException(index, "Was expecting '" + endtoken
						+ "' token.");
			}

		if (!nextIsOperator) {
			throw makeException(index, "Was expecting value token.");
		}

		while (operators.size() != 0)
			processOperator(objects, operators);

		return (ScriptElement) objects.get(0);
	}

	private void parseScript(ScriptElement parent) throws ScriptParserException {
		while (tokenzier.hasMoreTokens()) {
			ScriptElement expression = parseExpression(";", true);
			if (expression != null)
				parent.add(expression);
		}
	}
}
