/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import rasmus.interpreter.Executable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

public class ScriptUnitFactory implements UnitFactory, MetaDataProvider {
	private List unit_params;

	private List unit_defaults;

	private Executable unit_body;

	private NameSpace namespace;
	
	private int unit_type;

	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Scripted Unit");
		
		Iterator p_iter = unit_params.iterator();
		Iterator d_iter = unit_defaults.iterator();
		
		int parindex = 1;
		while(p_iter.hasNext() && d_iter.hasNext())
		{
			String param_name = ((String)p_iter.next()).toLowerCase();
			Variable default_value = (Variable)d_iter.next();
			String defaultval = null;
			
			int type = MetaData.TYPE_UNKNOWN;
			if(default_value != null)
			{
				
				if(default_value.has(ObjectsPart.class))
				{
				if(!ObjectsPart.getInstance(default_value).getObjects().isEmpty())
				{
					defaultval = ObjectsPart.toString(default_value);
					type = MetaData.TYPE_STRING;
				}
				}
				
				if(defaultval == null)
				if(default_value.has(DoublePart.class))
				{
					DecimalFormat dformat = new DecimalFormat("0.##########");
					dformat.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
					defaultval = dformat.format(DoublePart.asDouble(default_value));
					type = MetaData.TYPE_NUMBER;
				}
			}
			if(param_name.equals("output"))
			{
				metadata.add(-1, "output",		"Output",			defaultval, null, type, MetaData.DIRECTION_OUT);
			}
			else
			if(param_name.equals("input"))
			{
				metadata.add(parindex, param_name,	param_name,			defaultval, null, type, MetaData.DIRECTION_IN);
				parindex++;				
			}
			else
			{
				metadata.add(parindex, param_name,	param_name,			defaultval, null, type, MetaData.DIRECTION_INOUT);
				parindex++;
			}
			
			
		}
		
		return metadata;
	}
	
	public ScriptUnitFactory(List params, List defaults, Executable body,
			NameSpace namespace, int unit_type) {
		unit_params = params;
		unit_defaults = defaults;
		unit_body = body;
		this.unit_type = unit_type;
		this.namespace = namespace;
	}

	public UnitInstancePart newInstance(Parameters parameters) {
		return new ScriptUnitFactoryInstance(parameters, unit_params,
				unit_defaults, unit_body, namespace, unit_type);
	}

}