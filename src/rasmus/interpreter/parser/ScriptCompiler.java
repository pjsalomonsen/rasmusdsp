/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import rasmus.interpreter.Executable;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.parser.ScriptParserException;

public class ScriptCompiler {

	public static Executable compile(String script)
			throws ScriptParserException {
		return compile(ScriptOptimizer.optimize(ScriptParser.parse(script)));
	}

	public static Executable compile(ScriptElement script) {
		ScriptCompiler compiler = new ScriptCompiler();
		Integer ret = compiler.eval(script);
		CompiledScript co = compiler.getCompiledScript();
		if (ret != null)
			co.returnVar = ret;
		return co;
	}

	private int varcounter = 0;

	private List c_adds = new ArrayList();

	private List c_radds = new ArrayList();

	private List c_calls = new ArrayList();

	private List c_namespaces = new ArrayList();

	private List c_units = new ArrayList();

	private ArrayList privatevariables = new ArrayList();

	private HashMap<String, Integer> namespace = new HashMap<String, Integer>();

	private Integer newVariable() {
		int varid = varcounter;
		varcounter++;
		return varid;
	}

	private Integer getVariable(String name) {

		Integer var = namespace.get(name);
		if (var == null) {
			var = newVariable();
			namespace.put(name, var);
		}
		return var;
	}

	private CompiledScript getCompiledScript() {

		CompiledScript cs = new CompiledScript();

		// 0
		// Private Variables
		{
			cs.private_variables = new String[privatevariables.size()];
			Iterator iterator = privatevariables.iterator();
			int i = 0;
			while (iterator.hasNext()) {
				cs.private_variables[i] = (String) iterator.next();
				i++;
			}
		}

		// 1
		// Names
		{
			cs.vardef_Names = new String[varcounter];
			Arrays.fill(cs.vardef_Names, null);
			Iterator iterator = namespace.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator.next();
				int varid = ((Integer) entry.getValue());
				cs.vardef_Names[varid] = (String) entry.getKey();
			}
		}

		// 2
		// RVariable Adding
		{
			cs.vardef_Constants = new Variable[varcounter];
			Arrays.fill(cs.vardef_Constants, null);

			// cs.rvaradd_to = new int[c_radds.size()];
			// cs.rvaradd_from = new RVariable[c_radds.size()];
			Iterator iterator = c_radds.iterator();
			int ix = 0;
			while (iterator.hasNext()) {
				Object[] entry = (Object[]) iterator.next();
				// cs.rvaradd_to[ix] = (Integer)entry[0];
				// cs.rvaradd_from[ix] = (RVariable)entry[1];

				cs.vardef_Constants[(Integer) entry[0]] = (Variable) entry[1];
				ix++;
			}
		}

		// 3
		// Variable Adding
		{
			cs.varadd_to = new int[c_adds.size()];
			cs.varadd_from = new int[c_adds.size()];
			Iterator iterator = c_adds.iterator();
			int ix = 0;
			while (iterator.hasNext()) {
				int[] entry = (int[]) iterator.next();
				cs.varadd_to[ix] = entry[0];
				cs.varadd_from[ix] = entry[1];
				ix++;
			}
		}

		// 4
		// Units
		{
			cs.varunit = new int[c_units.size()];
			cs.varunit_params = new List[c_units.size()];
			cs.varunit_body = new CompiledScript[c_units.size()];
			cs.varunit_defaults = new List[c_units.size()];
			cs.varunit_types = new int[c_units.size()];
			Iterator iterator = c_units.iterator();
			int ix = 0;
			while (iterator.hasNext()) {
				Object[] entry = (Object[]) iterator.next();
				cs.varunit[ix] = (Integer) entry[0];
				cs.varunit_params[ix] = (List) entry[1];
				cs.varunit_body[ix] = (CompiledScript) entry[2];
				cs.varunit_defaults[ix] = (List<Integer>) entry[3];
				cs.varunit_types[ix] = (Integer)entry[4];
				ix++;
			}
		}

		// 5
		// Calls
		{
			cs.varcall = new int[c_calls.size()];
			cs.varcall_params = new Map[c_calls.size()];
			Iterator iterator = c_calls.iterator();
			int ix = 0;
			while (iterator.hasNext()) {
				Object[] entry = (Object[]) iterator.next();
				cs.varcall[ix] = (Integer) entry[0];

				Map map = (Map) entry[1];
				Iterator miterator = map.entrySet().iterator();
				while (miterator.hasNext()) {
					Map.Entry mentry = (Map.Entry) miterator.next();
					map.put(mentry.getKey(), ((Integer) mentry.getValue()));
				}
				cs.varcall_params[ix] = map;
				ix++;
			}
		}

		// 6
		// Namespaces
		{
			cs.varnamespace_name = new String[c_namespaces.size()];
			cs.varnamespace_body = new CompiledScript[c_namespaces.size()];
			Iterator iterator = c_namespaces.iterator();
			int ix = 0;
			while (iterator.hasNext()) {
				Object[] entry = (Object[]) iterator.next();
				cs.varnamespace_name[ix] = (String) entry[0];
				cs.varnamespace_body[ix] = (CompiledScript) entry[1];
				ix++;
			}
		}

		return cs;
	}

	private Integer evalBody(ScriptElement element) {
		List elements = element.getElements();
		if (elements == null)
			return null;
		Integer ret = null;
		Iterator<ScriptElement> iterator = elements.iterator();
		while (iterator.hasNext()) {
			ScriptElement child = iterator.next();
			Integer r = eval(child);
			if (r != null)
				ret = r;
		}
		return ret;
	}

	private Integer evalNumber(ScriptElement element) {
		Integer varid = newVariable();
		Object[] item = { varid,
				DoublePart.asVariable((Double) element.getValue()) };
		c_radds.add(item);
		return varid;
	}

	private Integer evalString(ScriptElement element) {
		Integer varid = newVariable();
		Object[] item = { varid,
				ObjectsPart.asVariable((String) element.getValue()) };
		c_radds.add(item);
		return varid;
	}

	private Integer evalIdentifier(ScriptElement element) {
		return getVariable((String) element.getValue());
	}

	private Integer evalAssignment(ScriptElement element) {
		if (element.getElements() != null)
			if (element.getElements().size() == 1) {
				int[] item = { getVariable((String) element.getValue()),
						eval(element.getElements().get(0)) };
				c_adds.add(item);
				
				String name = (String)element.getValue();
				name = name.toLowerCase();
				if (!privatevariables.contains(name)) 
					privatevariables.add(name);
				
				return item[1];
			}
		return null;
	}

	private Integer evalCall(ScriptElement element) {
		Map params = new HashMap();
		if (element.getElements() != null) {
			Iterator<ScriptElement> citerator = element.getElements()
					.iterator();
			while (citerator.hasNext()) {
				ScriptElement param_element = citerator.next();
				if (param_element.getType() == ScriptElement.TYPE_PARAMETER) {
					String paramname = (String) param_element.getValue();
					if (param_element.getElements() != null)
						if (param_element.getElements().size() == 1) {
							Integer paramvalue = eval(param_element
									.getElements().get(0));
							if (paramvalue != null)
								params.put(paramname, paramvalue);
						}
				}
			}
		}
		{
			Integer retvar = newVariable();
			
			if (element.getValue().equals("+")) {				
				Iterator iterator = params.values().iterator();
				while (iterator.hasNext()) {
					Integer paramvalue = (Integer)iterator.next();							
					int[] item = { retvar, paramvalue };
					c_adds.add(item);									
				}
			}
			else
			{			
				params.put("output", retvar);
				Object[] item = { getVariable((String) element.getValue()), params };
				c_calls.add(item);			
			}
			
			return retvar;
		}
	}

	private Integer evalUnit(ScriptElement element) {
		ArrayList<ScriptElement> parameters = new ArrayList<ScriptElement>();
		Iterator<ScriptElement> citerator = element.getElements().iterator();
		ScriptElement body = null;
		while (citerator.hasNext()) {
			ScriptElement param_element = citerator.next();
			if (param_element.getType() == ScriptElement.TYPE_PARAMETER)
				parameters.add(param_element);
			else if (param_element.getType() == ScriptElement.TYPE_BODY)
				body = param_element;
		}

		if (body == null)
			return null;

		if (element.getValue().equals("function")
				|| element.getValue().equals("unit")
				|| element.getValue().equals("module")) {
			List<String> unitparamslist = new ArrayList<String>();
			List<Integer> unitdefaults = new ArrayList<Integer>();

			if (element.getValue().equals("unit")
					|| element.getValue().equals("function")) {
				unitparamslist.add("output");
				unitdefaults.add(null);
			}

			Iterator<ScriptElement> piter = parameters.iterator();
			while (piter.hasNext()) {
				ScriptElement parm_element = piter.next();
				unitparamslist.add((String) parm_element.getValue());
				Integer defval = null;
				if (parm_element.getElements() != null)
					if (parm_element.getElements().size() == 1) {
						Integer vret = eval(parm_element.getElements().get(0));
						if (vret != null)
							defval = vret;
					}
				unitdefaults.add(defval);
			}

			Integer var = newVariable();
			int unit_type = 0;
			if(element.getValue().equals("module")) unit_type = 1;
			Object[] item = { var, unitparamslist, compile(body), unitdefaults, new Integer(unit_type) };
			c_units.add(item);

			return var;

		}

		if (element.getValue().equals("namespace"))
			if (parameters.size() == 1) {
				ScriptElement param1 = parameters.get(0);
				if (param1.getType() == ScriptElement.TYPE_PARAMETER) {
					String namespace = (String) param1.getValue();
					Object[] item = { namespace, compile(body) };
					c_namespaces.add(item);
				}
			}

		return null;

	}

	private Integer eval(ScriptElement element) {
		switch (element.getType()) {
		case ScriptElement.TYPE_BODY:
			return evalBody(element);
		case ScriptElement.TYPE_NUMBER:
			return evalNumber(element);
		case ScriptElement.TYPE_STRING:
			return evalString(element);
		case ScriptElement.TYPE_IDENTIFIER:
			return evalIdentifier(element);
		case ScriptElement.TYPE_ASSIGNMENT:
			return evalAssignment(element);
		case ScriptElement.TYPE_CALL:
			return evalCall(element);
		case ScriptElement.TYPE_UNIT:
			return evalUnit(element);
		}
		return null;
	}

}
