/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.parser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.unit.Unit;

public class ScriptTokenParser {

	public static final String hexDigits = "0123456789ABCDEFabcdef";
	public static final int STATUS_NORMAL = 0;
	public static final int STATUS_SINGLE_QUOTE_STRING = 1;
	public static final int STATUS_DOUBLE_QUOTE_STRING = 2;
	public static final int STATUS_PRE_COMMENTS = 3;
	public static final int STATUS_SINGLE_LINE_COMMENTS = 4;
	public static final int STATUS_MULTI_LINE_COMMENTS = 5;
	public static final int STATUS_MULTI_LINE_COMMENTS_PRE_END = 6;
	
	public static final int STATUS_MULTI_LINE_SINGLE_QUOTE_STRING = 7;
	public static final int STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING = 8;
	public static final int STATUS_MULTI_LINE_SINGLE_QUOTE_STRING_PRE_END = 9;
	public static final int STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING_PRE_END = 10;
	
	public static final String symbols = "=+-<>/!*^&(),{};";

	public static String[] extractStatemensResultGenerators(String statement)
	{
		statement = removeComments(statement);
		char[] codec = statement.toCharArray();
		int status = ScriptTokenParser.STATUS_NORMAL;
		char lastc = ' ';
		int li = -1;
		for (int i = 0; i < codec.length; i++) {
			char c = codec[i];
			status = ScriptTokenParser.parseStatus(c, status);
			boolean isstring  = ScriptTokenParser.isStringStatus(status);
			if(!isstring)
			{
				if(lastc == '<' && c == '-')
				{
					li = i - 1;
					break;
				}
				lastc = c;
			}
		}
		if(li == -1) return null;
		
		statement = statement.substring(li+2).trim();
        codec = statement.toCharArray();
		
		li = -1;
		for (int i = 0; i < codec.length; i++) {
			char c = codec[i];
			status = ScriptTokenParser.parseStatus(c, status);
			boolean isstring  = ScriptTokenParser.isStringStatus(status);		
			if(!isstring)
			{
				if(c == '+') return null;
				if(c == '-') return null;
				if(c == '*') return null;
				if(c == '/') return null;
				if(c == '^') return null;
				if(c == '{') return null;
				
				if(c == '(')
				{
					String[] strs = new String[1];
					strs[0] = statement.substring(0, i).trim();				
					return strs;
				}
				if(lastc == '<' && c == '-')
				{
					li = i - 1;
					break;
				}
				lastc = c;
			}
		}
		return null;
	}
	
	public static String extractStatementResultVariable(String statement)
	{
		char[] codec = statement.toCharArray();
		int status = ScriptTokenParser.STATUS_NORMAL;
		char lastc = ' ';
		int li = -1;
		for (int i = 0; i < codec.length; i++) {
			char c = codec[i];
			status = ScriptTokenParser.parseStatus(c, status);
			boolean iscomment = ScriptTokenParser.isCommentStatus(status);
			boolean isstring  = ScriptTokenParser.isStringStatus(status);
			if((!iscomment) && (!isstring))
			{
				if(lastc == '<' && c == '-')
				{
					li = i - 1;
					break;
				}
				lastc = c;
			}
		}
		if(li == -1) return null;
		
		String st = removeComments(statement.substring(0, li)).trim();
		if(st.length() == 0) return "output";
		if(st.contains("(")) return null;		
		return st;
	}
	
	public static String removeComments(String statement)
	{
		StringBuffer buffer = new StringBuffer();
		char[] codec = statement.toCharArray();
		int status = ScriptTokenParser.STATUS_NORMAL;
		for (int i = 0; i < codec.length; i++) {
			char c = codec[i];
			int laststatus = status;
			status = ScriptTokenParser.parseStatus(c, status);			
			if(status == STATUS_PRE_COMMENTS && ((i+1) <  codec.length))
			{
				int nextstatus = ScriptTokenParser.parseStatus(codec[i+1], status);
				if( ScriptTokenParser.isCommentStatus(nextstatus) )
					status = nextstatus;				
			}
			boolean iscomment = ScriptTokenParser.isCommentStatus(status);
			//boolean isstring  = ScriptTokenParser.isStringStatus(status);			
			
			if((!iscomment))
			if(!ScriptTokenParser.isCommentStatus(laststatus))
			{
				buffer.append(c);
			}
		}
		return buffer.toString();	
	}
	
	public static String removeCommentsAndWhiteSpace(String statement)
	{
		StringBuffer buffer = new StringBuffer();
		char[] codec = statement.toCharArray();
		int status = ScriptTokenParser.STATUS_NORMAL;
		for (int i = 0; i < codec.length; i++) {
			char c = codec[i];
			int laststatus = status;
			status = ScriptTokenParser.parseStatus(c, status);			
			if(status == STATUS_PRE_COMMENTS && ((i+1) <  codec.length))
			{
				int nextstatus = ScriptTokenParser.parseStatus(codec[i+1], status);
				if( ScriptTokenParser.isCommentStatus(nextstatus) )
					status = nextstatus;				
			}
			boolean iscomment = ScriptTokenParser.isCommentStatus(status);
			boolean isstring  = ScriptTokenParser.isStringStatus(status);			
			
			if((!iscomment))
			if(!ScriptTokenParser.isCommentStatus(laststatus))
			{
				if(isstring || !Character.isWhitespace(c))
					buffer.append(c);
				
			}
		}
		return buffer.toString();	
	}	
	
	public static String[] seperateStatements(String code)
	{
		ArrayList<String> strings = new ArrayList<String>(); 
		char[] codec =  code.toCharArray();		
		int status = ScriptTokenParser.STATUS_NORMAL;
		int nestlevel = 0;
		int lasti = 0;
		for (int i = 0; i < codec.length; i++) {
			char c = codec[i];
			status = ScriptTokenParser.parseStatus(c, status);
			if(c == '{') 
				nestlevel++;
			else
			if((c == '}') && (nestlevel > 1))				
				nestlevel--;
			else
			if((c == ';' && (nestlevel == 0)) || (c == '}' && (nestlevel == 1)))
			{
				nestlevel = 0;
				boolean iscomment = ScriptTokenParser.isCommentStatus(status);
				boolean isstring  = ScriptTokenParser.isStringStatus(status);
				if((!iscomment) && (!isstring))
				{
					strings.add(code.substring(lasti, i+1));
					lasti = i+1;
				}
			}	
		}
		strings.add(code.substring(lasti, code.length()));
		return strings.toArray(new String[strings.size()]);
	}

	public static boolean isCommentStatus(int status) 
	{
		if(status == STATUS_SINGLE_LINE_COMMENTS) return true;
		if(status == STATUS_MULTI_LINE_COMMENTS) return true;
		if(status == STATUS_MULTI_LINE_COMMENTS_PRE_END) return true;
		return false;
	}

	public static boolean isNumber(String value)
	{
		if(value.startsWith("0x"))
		{
			for (int i = 2; i < value.length(); i++) {
				char c = value.charAt(i);
				if(hexDigits.indexOf(c) == -1) return false;
			}
			return true;
		}
		
		boolean float_symbol = false;
		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			if(c == '.')
			{
				if(float_symbol) return false;
				float_symbol = true;
			}
			else
			{
				if(c == '%')
				{
					for (int j = i+1; j < value.length(); j++) {
						c = value.charAt(j);
						if(c != '%') return false;
					}
					return true;
				}
				if(!Character.isDigit(c)) return false; 
			}
		}			
		return true;
	}

	public static boolean isStringStatus(int status)
	{
		if(status == STATUS_SINGLE_QUOTE_STRING) return true;
		if(status == STATUS_DOUBLE_QUOTE_STRING) return true;

		if(status == STATUS_MULTI_LINE_SINGLE_QUOTE_STRING) return true;
		if(status == STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING) return true;
		if(status == STATUS_MULTI_LINE_SINGLE_QUOTE_STRING_PRE_END) return true;
		if(status == STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING_PRE_END) return true;

		return false;
	}

	public static String makeFunctionCall(String[] values)
	{
		StringBuffer sb = new StringBuffer();
		sb.append(values[0] + "(");
		for (int i = 1; i < values.length; i++) {
			if(i != 1) sb.append(",");
			sb.append(values[i]);	
		}
		sb.append(")");
		return sb.toString();
	}

	public static String[] parseFunctionCall(String value)
	{
		int li = value.indexOf("(");
		if(li == -1) return null;
		ArrayList list = new ArrayList();
		String funcName = value.substring(0, li);
		list.add(funcName);		
		value = value.substring(li);
		// Remove ( at begin and ) at end
		if(value.startsWith("(")) value = value.substring(1);
		if(value.endsWith(")")) value = value.substring(0, value.length() - 1);
		
		StringBuffer paramvalue = new StringBuffer();
		int status = STATUS_NORMAL;
		int clevel = 0;
		for (int i = 0; i < value.length(); i++) {
			char cc = value.charAt(i);
			status = parseStatus(cc, status);
			paramvalue.append(cc);
			if(status == STATUS_NORMAL)
			{
				if(cc == '(')
				{
					clevel++;
				}
				else
				if(cc == ')')
				{
					clevel--;
				}
				if(clevel == 0)
				{
					if(cc == ',')
					{
						list.add(paramvalue.substring(0, paramvalue.length() - 1));
						paramvalue.setLength(0);
					}
				}
			}			
		}
		list.add(paramvalue.toString());
		
		return (String[])list.toArray(new String[list.size()]);
	}

	public static int parseStatus(char c, int status)
	{
			switch (status) {
			case STATUS_NORMAL:
				
				if(c == '\'') return STATUS_SINGLE_QUOTE_STRING;					
				else if(c == '\"') return STATUS_DOUBLE_QUOTE_STRING;
				else if(c == '/') return STATUS_PRE_COMMENTS;	
				break;
	
			case STATUS_SINGLE_QUOTE_STRING:  
				if(c == '\'') return STATUS_NORMAL;
				if(c == '\n') return STATUS_NORMAL;
				if(c == '\r') return STATUS_NORMAL;				
				break;
	
			case STATUS_DOUBLE_QUOTE_STRING:  
				if(c == '\"') return STATUS_NORMAL;
				if(c == '\n') return STATUS_NORMAL;
				if(c == '\r') return STATUS_NORMAL;
				break;
	
			case STATUS_PRE_COMMENTS:
				if(c == '/') return STATUS_SINGLE_LINE_COMMENTS;
				else if(c == '*') return STATUS_MULTI_LINE_COMMENTS;
				else if(c == '\'') return STATUS_MULTI_LINE_SINGLE_QUOTE_STRING;
				else if(c == '\"') return STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING;
				else return STATUS_NORMAL; 				
				
			case STATUS_SINGLE_LINE_COMMENTS:
				if(c == '\n' || c == '\r') return STATUS_NORMAL;
				break;
	
			case STATUS_MULTI_LINE_COMMENTS:
				if(c == '*') return STATUS_MULTI_LINE_COMMENTS_PRE_END;	
				break;
				
			case STATUS_MULTI_LINE_COMMENTS_PRE_END:
				if(c == '/') return STATUS_NORMAL;
				if(c == '*') return STATUS_MULTI_LINE_COMMENTS_PRE_END;	
				else return STATUS_MULTI_LINE_COMMENTS;
				
			
			case STATUS_MULTI_LINE_SINGLE_QUOTE_STRING:
				if(c == '\'') return STATUS_MULTI_LINE_SINGLE_QUOTE_STRING_PRE_END;	
				break;
				
			case STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING:
				if(c == '\"') return STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING_PRE_END;	
				break;							
				
			case STATUS_MULTI_LINE_SINGLE_QUOTE_STRING_PRE_END:
				if(c == '/') return STATUS_NORMAL;
				if(c == '\'') return STATUS_MULTI_LINE_SINGLE_QUOTE_STRING_PRE_END;
				else return STATUS_MULTI_LINE_SINGLE_QUOTE_STRING;

			case STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING_PRE_END:
				if(c == '/') return STATUS_NORMAL;
				if(c == '\"') return STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING_PRE_END;
				else return STATUS_MULTI_LINE_DOUBLE_QUOTE_STRING;
				
			default:
				break;
			} 	
			return status;
	}

	public static int parseStatus(String string, int status)
	{
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			status = parseStatus(c, status);
		}
		return status;
	}

	public static String[] seperateParamNameAndValue(String value)
	{
		String[] pair = new String[2];
		pair[0] = null;
		pair[1] = value;
		int status = STATUS_NORMAL;
		for (int i = 0; i < value.length(); i++) {
			char cc = value.charAt(i);
			status = parseStatus(cc, status);
			if(status == STATUS_NORMAL)
			{
				if(cc == '=')
				{
					pair[0] = value.substring(0, i);
					pair[1] = value.substring(i+1);
					return pair;
				}
			}
		}
		return pair;
	}
	
	public static boolean isVariableOrNumberLetter(char c)
	{
		return Character.isLetter(c) || 
		Character.isDigit(c) ||
		c == '%' || c == '.' || c == '_' || c == '#';
	}	
	
	Set registered_variables = new HashSet();
	Set possible_functions = new HashSet();
	Set functions = new HashSet();
	NameSpace namespace;

	public ScriptTokenParser(NameSpace namespace)
	{
		this.namespace = namespace;
		
		if(namespace != null)
		{
			namespace.getAllNames(registered_variables);
			possible_functions.addAll(registered_variables);
			functions.add("function");
			functions.add("use");
			functions.add("module");
			functions.add("namespace");
		}		
	}
	
	public boolean isRegisteredVariable(String value)
	{
		if(value.startsWith("global.")) value = value.substring(7);
		return registered_variables.contains(value.toLowerCase());
	}
	public boolean isFunction(String value)
	{
		if(value.startsWith("global.")) value = value.substring(7);
		String lvalue = value.toLowerCase();
		if(functions.contains(lvalue)) return true;
		if(!possible_functions.contains(lvalue)) return false;
		possible_functions.remove(lvalue);
		boolean isfunc = !Unit.getInstance(namespace.get(value)).getObjects().isEmpty();
		if(isfunc) functions.add(lvalue);
		return isfunc;		
	}
		
	
}
