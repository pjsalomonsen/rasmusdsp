/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.unit;

import rasmus.interpreter.Closeable;
import rasmus.interpreter.Commitable;


public class UnitInstance implements Closeable {
	
	
	//ArrayList instances = new ArrayList();
	//Map instances_factories = new HashMap();
	
	private Parameters parameters;
	private Unit runit;
	private boolean updateable = true;
	
	private UnitFactory[] instances_factory = {};
	private UnitInstancePart[] instances_object = {};
	
	public void setUpdateable(boolean updateable)
	{
		this.updateable = updateable;
	}
	

	private class InitTask implements Commitable
	{

		Object[] factories;
		public InitTask(Object[] factories)
		{
			this.factories = factories;
		}
		
		public int getRunLevel()
		{
			return RUNLEVEL_DEFAULT;
		}
		
		public void commit() {
			instances_factory = new UnitFactory[factories.length];
			instances_object = new UnitInstancePart[factories.length];
			for (int i = 0; i < instances_factory.length; i++) {
				UnitFactory factory = (UnitFactory)factories[i];
				instances_factory[i] = factory;
				instances_object[i] = factory.newInstance(parameters);
			}			
		}
	}
	
	public UnitInstance(Unit runit, Parameters parameters, Object[] factories)
	{			

		this.parameters = parameters;
		this.runit = runit;
		
		parameters.getNameSpace().addToCommitStack(new InitTask(factories));
		//parameters.compact();
	}
	
	/*
	private List getFactoryInstances(RUnitFactory factory)
	{
		List list = (List)instances_factories.get(factory);
		if(list == null)
		{
			list = new ArrayList();
			instances_factories.put(factory, list);
		}
		return list;
	} */
	
	private class AddFactoryTask implements Commitable
	{
		UnitFactory factory;
		public AddFactoryTask(UnitFactory factory)
		{
			this.factory = factory;
		}
		
		public int getRunLevel()
		{
			return RUNLEVEL_DEFAULT;
		}
		
		public void commit() {
			//RUnitFactory[] instances_factory = null;
			//RUnitFactory[] instances_object = null;

			if(instances_factory == null)
			{
				instances_factory = new UnitFactory[1];
				instances_object = new UnitInstancePart[1];
			}
			else
			{
				UnitFactory[] new_instances_factory = new UnitFactory[instances_factory.length + 1];
				UnitInstancePart[] new_instances_object =  new UnitInstancePart[instances_object.length + 1];
				for (int i = 0; i < instances_object.length; i++) {
					new_instances_factory[i + 1] = instances_factory[i];
					new_instances_object[i + 1] = instances_object[i];
				}
				instances_factory = new_instances_factory;
				instances_object = new_instances_object;
			}
			instances_factory[0] = factory; //new_instances_factory;
			instances_object[0] = factory.newInstance(parameters); // new_instances_object;
	/*			
			List f_instances = getFactoryInstances(factory);
			RUnitInstancePart instance = factory.newInstance(parameters);
			f_instances.add(instance);
			instances.add(instance);*/
						
		}
	}	
	
	public void addFactory(UnitFactory factory)
	{	
		if(!updateable) return;
		
		parameters.getNameSpace().addToCommitStack(new AddFactoryTask(factory));
	
		//parameters.compact();
	}
	


	private class RemoveFactoryTask implements Commitable
	{
		UnitFactory factory;
		public RemoveFactoryTask(UnitFactory factory)
		{
			this.factory = factory;
		}

		public int getRunLevel()
		{
			return RUNLEVEL_DEFAULT;
		}
		
		public void commit() {
			if(instances_factory == null) return;
			
			if(instances_factory.length == 1)			
			{
				if(instances_factory[0] == factory)
				{
					instances_object[0].close();
					instances_factory = null;
					instances_object = null;
				}
				return;
			}
			
			int fcount = 0;
			for (int i = 0; i < instances_factory.length; i++) 
				if(instances_factory[i] == factory)
				{
					instances_object[i].close();
					fcount++;
				}
			
			if(fcount == 0) return;
			
			
			UnitFactory[] new_instances_factory = new UnitFactory[instances_factory.length - fcount];
			UnitInstancePart[] new_instances_object =  new UnitInstancePart[instances_object.length - fcount];
			
			int j = 0;
			for (int i = 0; i < instances_factory.length; i++) {
				if(instances_factory[i] != factory)
				{
					new_instances_factory[j] = instances_factory[i];				
					new_instances_object[j] = instances_object[i];
					j++;
				}
			}

			instances_factory = new_instances_factory;
			instances_object = new_instances_object;
						
		}
	}
	
	public void removeFactory(UnitFactory factory)
	{
		if(!updateable) return;
		parameters.getNameSpace().addToCommitStack(new RemoveFactoryTask(factory));
	}
	
	public void close()
	{
		parameters.getNameSpace().addToCommitStack(new Commitable()
		{
			
		public int getRunLevel()
		{
			return RUNLEVEL_DEFAULT;
		}
			
		public void commit()
		{
			
		//synchronized (this) {

		if(instances_object != null) 
		{
		for (int i = 0; i < instances_object.length; i++) {
			instances_object[i].close();
		}
		}
		/*	
		Iterator iterator = instances.iterator();
		while (iterator.hasNext()) {
			RUnitInstancePart unitpart = (RUnitInstancePart) iterator.next();
			unitpart.close();
		}
		instances.clear();
		
		instances_factories.clear();*/
		
		instances_object = null;
		instances_factory = null;
		
		
		}
		});
		//}
		
		runit.instances.remove(this);
		
	}
	
	
}
