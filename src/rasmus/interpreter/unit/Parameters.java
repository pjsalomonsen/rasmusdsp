/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.unit;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.NameSpaceAdapter;
import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;

public final class Parameters extends NameSpaceAdapter {
	
	private Map<String, Object> map = new HashMap<String, Object>();	
	
	//public void putNameSpace(NameSpace namespace) { this.namespace = namespace; }
	
	public void setParameter(String name, Variable value) {
		getParameters().put(name.toLowerCase(), value);
		if(names != null)
		{
			names = null;
			values = null;
		}
	}	
	public void setParameter(int paramno, Variable value)
	{
		map.put(Integer.toString(paramno), value);
	}
	
	public void setParameter(String name, double value) {
		getParameters().put(name.toLowerCase(), new Double(value));
		if(names != null)
		{
			names = null;
			values = null;
		}
	}	
	public void setParameter(int paramno, double value)
	{
		map.put(Integer.toString(paramno), new Double(value));
	}
		
	
	private String[] names = null;
	private Variable[] values = null;
	
	public Parameters clone()
	{
		Parameters imap = new Parameters(getNameSpace());
		imap.map.putAll(map);
		return imap;
	}
	
	public Map getParameters()
	{
		
		if(map == null)
		{
			map = new HashMap<String, Object>();
		if(names != null)
		{
			for (int i = 0; i < names.length; i++) {
				map.put(names[i], values[i]);
			}
		}
		}
		return map;
	}
	
	
	public void compact()
	{
		getNameSpace().addToCommitStack(
				new Commitable()
				{
					
					public int getRunLevel() {
						return RUNLEVEL_INIT;
					}
					
					public void commit()
					{
		if(map == null) return;
		if(names != null)
		{
			map = null;
			return;
		}
		int len = map.entrySet().size();
		names = new String[len];
		values = new Variable[len];
		Iterator iterator = map.entrySet().iterator();
		for (int i = 0; i < names.length; i++) {
			if(iterator.hasNext())
			{
				Map.Entry entry = (Map.Entry)iterator.next();
				names[i] = (String)entry.getKey();
				values[i] = (Variable)entry.getValue();
			}
		}
		map = null;
					}
				});
	}
	
//	static int ccp = 0;
	public Parameters(NameSpace namspace)
	{
		setNameSpace(namspace);
//		ccp++;
//		System.out.println("Create parameters!! " + ccp);
	}
	
	public Variable getParameter(String name)
	{
		Object obj = getParameters().get(name.toLowerCase());
		if(obj instanceof Variable) return (Variable)obj;
		else if(obj instanceof Double) return DoublePart.asVariable((Double)obj);
		else return null;
	}	
	public Variable getParameter(int paramno)
	{
		return (Variable)getParameter(Integer.toString(paramno));
	}	
	public Variable getParameter(int paramno, String name)
	{
		Variable ret = (Variable)getParameter(name);
		if(ret != null) return ret;
		return (Variable)getParameter(Integer.toString(paramno));
	}
	
	
	public Variable getParameterWithDefault(String name)
	{
		Variable ret = getParameter(name);
		if(ret != null) return ret;
		return new Variable();
	}		
	public Variable getParameterWithDefault(int paramno)
	{
		Variable ret = getParameter(paramno);
		if(ret != null) return ret;
		return new Variable();
	}		
	public Variable getParameterWithDefault(int paramno, String name)
	{
		Variable ret = getParameter(paramno, name);
		if(ret != null) return ret;
		return new Variable();
	}
	

	

	
	
	
}
