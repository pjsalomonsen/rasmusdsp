/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.unit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;


public final class Unit extends ListPart {
	
	public List<MetaData> getMetaData()
	{
		List<MetaData> list = null;
		Iterator iter = getObjects().iterator();
		while (iter.hasNext()) {
			UnitFactory element = (UnitFactory) iter.next();
			if(element instanceof MetaDataProvider)
			{
				if(list == null) list =  new ArrayList<MetaData>();
				list.add(((MetaDataProvider)element).getMetaData());
			}
		}
		return list;
	}
	
	public static Variable asVariable(UnitFactory value)
	{
		Variable variable = new Variable();
		Unit runit = ((Unit)variable.get(Unit.class));
		//runit.setImmutable(true);
		runit.addObject(value);
		return variable;
	}
	
	public static UnitInstance newInstance(Variable unit, Parameters parameter)
	{
		UnitInstance ret = getInstance(unit).newInstance(parameter);
		///parameter.compact();
		return ret;
	}
	
	public static UnitInstance newInstance(Variable unit, NameSpace namespace, Variable... arguments)
	{
		Parameters p = new Parameters(namespace);
		for (int i = 0; i < arguments.length; i++) {
			if(arguments[i] != null)
				p.setParameter(i, arguments[i]);
		}		
		return newInstance(unit, p);
	}	
	

	
	public UnitInstance newInstance(Parameters parameters)
	{		
		
		
		UnitInstance unitinstance = new UnitInstance(this, parameters, getObjects().toArray());
		/*
		Object[] objects = getObjects().toArray();
		for (int i = 0; i < objects.length; i++) {			
			RUnitFactory unitpart = (RUnitFactory) objects[i];
			unitinstance.addFactory(unitpart);			
		}				*/
		
		instances.add(unitinstance);		
		return unitinstance;
						
		
	}	
	public static Unit getInstance(Variable variable)
	{
		return (Unit)variable.get(Unit.class);
	}
	
	
	ArrayList instances = new ArrayList();
	
	public void addFactory(UnitFactory factory)
	{
		Iterator iterator = instances.iterator();
		while (iterator.hasNext()) {					
			((UnitInstance) iterator.next()).addFactory(factory);
		}		
	}
	public void removeFactory(UnitFactory factory)
	{
		Iterator iterator = instances.iterator();
		while (iterator.hasNext()) {
			((UnitInstance) iterator.next()).removeFactory(factory);
		}		
	}
	
	public Unit()
	{
		forceAddListener(rlistlistener);
	}
	
	ListPartListener rlistlistener = new ListPartListener()
	{
		public void objectAdded(ListPart source, Object object) {
			if(object instanceof UnitFactory) addFactory((UnitFactory)object);
		}
		
		public void objectRemoved(ListPart source, Object object) {
			if(object instanceof UnitFactory) removeFactory((UnitFactory)object);
		}
		
		public void objectsAdded(ListPart source, List objects) {
			Iterator iterator = objects.iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();	
				if(object instanceof UnitFactory) addFactory((UnitFactory)object);			 
			}		
		}
		
		public void objectsRemoved(ListPart source, List objects) {
			Iterator iterator = objects.iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();	
				if(object instanceof UnitFactory) removeFactory((UnitFactory)object);			 
			}				
		}
		
	};
	
}
