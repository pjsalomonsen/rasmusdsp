/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.struct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.VariablePartAdapter;
import rasmus.interpreter.namespace.CommitSystem;

public class Struct extends VariablePartAdapter implements NameSpace {
	
	
	public static Variable get(Variable variable, String name)
	{
		return getInstance(variable).get(name);
	}
	
	public static Struct getInstance(Variable variable)
	{
		return (Struct)variable.get(Struct.class);
	}			
	
	HashMap<String, Variable> overrides = null;
	public void overrideVariable(String name, Variable var)
	{
		if(overrides == null) overrides = new HashMap<String, Variable>();
		overrides.put(name.toLowerCase(), var);
	}
	
	public Variable get(String name)
	{		
		if(name.equalsIgnoreCase("this")) return getVariable();
		
		if(vars == null) vars = new Hashtable<String, Variable>();
		
		String lname = name.toLowerCase();

		if(overrides != null)
		{
			Variable var = overrides.get(lname);
			if(var != null) return var;
		}
		
		Variable var = vars.get(lname);
		if(var == null)
		{					
			var = new Variable();

			if(!isPrivate(name))
			{
				Iterator<Struct> iterator = childstructs.iterator();
				while (iterator.hasNext()) 
					var.add(iterator.next().get(lname));
			}
			
			vars.put(lname, var);
		}
		return var;
	}
	
	public void getAllNames(Set set)
	{		
		Iterator<Struct> iterator = childstructs.iterator();
		while (iterator.hasNext()) 
			iterator.next().getAllNames(set);								
		set.addAll(vars.keySet());
	}
	
	public void registerAsPrivate(String name) {
		
		if(varPrivateCounter == null) varPrivateCounter = new HashMap<String, Integer>();
		if(privateVars == null) privateVars = new TreeSet<String>();
		
		name = name.toLowerCase();
		Integer value = varPrivateCounter.get(name);
		if (value == null)
			value = 1;
		else
			value++;
		varPrivateCounter.put(name, value);

		if (value == 1) {
			privateVars.add(name);
			
			if(vars != null)
			{
			Variable var = (Variable) vars.get(name.toLowerCase());
			if (var != null) {
				Iterator<Struct> iterator = childstructs.iterator();
				while (iterator.hasNext()) 
					var.remove(iterator.next().get(name));		
			}
			}
		}
	}

	public void unRegisterAsPrivate(String name) {
		
		if(varPrivateCounter == null) return;
		if(privateVars == null) return;
		
		name = name.toLowerCase();
		Integer value = varPrivateCounter.get(name);
		if (value == null)
			value = -1;
		else
			value--;
		varPrivateCounter.put(name, value);

		if (value == 0) {
			privateVars.remove(name);
			
			if(vars != null)
			{
			Variable var = (Variable) vars.get(name);
			if (var != null) {
				Iterator<Struct> iterator = childstructs.iterator();
				while (iterator.hasNext()) 
					var.add(iterator.next().get(name));		
			}
			}
		}
	}	
	
	public void put(String name, Variable var) {
		registerAsPrivate(name);		
		if(vars == null) vars = new Hashtable<String, Variable>();		
		vars.put(name.toLowerCase(), var);
	}	
	
	public boolean isPrivate(String name)
	{
		if(varPrivateCounter == null) return false;
		if(privateVars == null) return false;

		name = name.toLowerCase();
		return privateVars.contains(name);
	}
	
	private Map<String, Integer> varPrivateCounter = null;

	private TreeSet<String> privateVars = null;
	
	private Map<String, Variable> vars = null;
	
	private ArrayList<Struct> childstructs = new ArrayList<Struct>();
	
	public Struct clone()
	{
		// Make sure there are no local null content
		if(varPrivateCounter == null) varPrivateCounter = new HashMap<String, Integer>();
		if(privateVars == null) privateVars = new TreeSet<String>();
		if(vars == null) vars = new Hashtable<String, Variable>();
		if(overrides == null) overrides = new HashMap<String, Variable>();
		
		Struct struct = new Struct();
		struct.cosys = cosys;
		struct.childstructs = childstructs;
		struct.overrides = overrides;
		struct.varPrivateCounter = varPrivateCounter;
		struct.privateVars = privateVars;
		struct.vars = vars;
		struct.childstructs = childstructs;
		return struct;
	}

	public void clear() {
		if(vars != null) vars.clear();
	}

	public void add(Variable variable) {
		Struct struct = Struct.getInstance(variable);
		childstructs.add(struct);
		if(vars != null)
		{
			Iterator<Entry<String, Variable>> iterator = vars.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Variable> entry = iterator.next();
				if(!isPrivate(entry.getKey()))			
					entry.getValue().add(struct.get(entry.getKey())); 
			}
		}
	}

	public void remove(Variable variable) {
		Struct struct = Struct.getInstance(variable);
		childstructs.remove(struct);
		if(vars != null)
		{
			Iterator<Entry<String, Variable>> iterator = vars.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Variable> entry = iterator.next();
				if(!isPrivate(entry.getKey()))
					entry.getValue().remove(struct.get(entry.getKey())); 
			}
		}
	}	
	
	private CommitSystem cosys = null;
	
	public void commit() {
		cosys.commit();
	}
	
	public CommitSystem getCommitSystem()
	{
		return cosys; 
	}	
	public void setCommitSystem(CommitSystem cosys)
	{
		this.cosys = cosys; 
	}

	public void addToCommitStack(Commitable commitable) {
		cosys.add(commitable);
	}	
	
}
