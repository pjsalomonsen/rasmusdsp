/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.struct;

import java.util.ArrayList;
import java.util.Iterator;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class StructUnitInstance extends UnitInstanceAdapter implements Commitable
{
	Variable output;
	Variable answer = null;
	Variable input;
	
	ArrayList<Variable> vars = new ArrayList<Variable>();
	NameSpace namespace;
	public StructUnitInstance(Parameters parameters)
	{
		namespace = parameters.getNameSpace();
		output = parameters.getParameterWithDefault("output");			
		input = parameters.getParameter("input");		
		
		int i = 1;
		Variable name = parameters.getParameter(i);
		i++;
		Variable var = parameters.getParameter(i);
		while(name != null)
		{
			vars.add(name);
			vars.add(var);
			
			if(name != null) ObjectsPart.getInstance(name).addListener(this);
			
			i++;
			name = parameters.getParameter(i);			
			i++;
			var = parameters.getParameter(i);			
		}				
		
		calc();
	}
	public void close() {
		clear();

		Iterator<Variable> iter = vars.iterator();
		while (iter.hasNext()) {
			Variable name = iter.next();
			iter.next();		
			if(name != null) ObjectsPart.getInstance(name).removeListener(this);
		}		
	}
	
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			answer = null;
		}
	}
	
	public void calc() {
		namespace.addToCommitStack(this);
	}
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}	
	
	public void commit() {
		clear();
		
		answer = new Variable();
		Struct struct = Struct.getInstance(answer); 
		
		Iterator<Variable> iter = vars.iterator();
		while (iter.hasNext()) {
			Variable name = iter.next();
			Variable var = iter.next();
			if(var == null) var = input;
			if(var != null)
			struct.get(ObjectsPart.toString(name)).add(var);
		}
		
		output.add(answer);		
	}	
	
}

public class StructUnit implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Struct");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_UNKNOWN, MetaData.DIRECTION_OUT);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_UNKNOWN, MetaData.DIRECTION_IN);
		metadata.setHasVarargs(true);
		return metadata;		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new StructUnitInstance(parameters);
	}
}
