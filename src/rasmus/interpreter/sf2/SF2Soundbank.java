/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package rasmus.interpreter.sf2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.io.Resource;
import rasmus.interpreter.io.ResourceManager;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;


class SF2SoundbankInstance extends UnitInstanceAdapter implements Commitable
{
	
	Variable output;
	Variable filename;
	Variable bank;
	
	Variable answer = null;
	NameSpace namespace;
	Variable wrkdir;
	Variable streammode;
	
	ResourceManager manager;
	
	public SF2SoundbankInstance(Parameters parameters) {
		
		namespace = parameters.getNameSpace();
		manager = ResourceManager.getInstance(namespace);
		
		output = parameters.getParameterWithDefault("output");
		filename = parameters.getParameterWithDefault(1, "filename");
		bank = parameters.getParameterWithDefault(2, "bank");
		streammode = parameters.getParameter(3, "streammode");
		if(streammode == null) streammode = DoublePart.asVariable(1);
		wrkdir = parameters.get("wrkdir");
		
		DoublePart.getInstance(bank).addListener(this);
		ObjectsPart.getInstance(filename).addListener(this);
		ObjectsPart.getInstance(wrkdir).addListener(this);
		
		calc();
	}

	public void calc() {
		namespace.addToCommitStack(this);
	}

	public void close() {
		clear();	
		
		if(resource != null)
		{
			resource.close();
			resource  = null;
		}
		DoublePart.getInstance(bank).removeListener(this);
		ObjectsPart.getInstance(filename).removeListener(this);
		ObjectsPart.getInstance(wrkdir).removeListener(this);
	}
	
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			answer = null;
			if(soundbankinterpreter != null)
			{
				soundbankinterpreter.close();
				//inamespace.close();
				soundbankinterpreter = null;
			}
		}
	}
	
	Interpreter soundbankinterpreter = null;
	
	public String expandPath(String path)
	{		
		path = path.trim();
		if(path.length() == 0) return path;
		if(path.startsWith(File.separator)) return path;		
		if(path.length() > 2 && Character.isLetter(path.charAt(0)) && path.charAt(1) == ':' && path.charAt(2) == File.separatorChar)
				return path;
	
		String wrkdir = ObjectsPart.toString(this.wrkdir);
		if(wrkdir.trim().length() == 0)
			return path;					
		
		if(wrkdir.endsWith(File.separator))
		{
			return wrkdir + path;
		}
		else
		{
			return wrkdir + File.separator + path;
		}
	}
	
	Resource resource = null;
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit()
	{
		clear();
		
		String filename = expandPath(ObjectsPart.toString(this.filename));
		/*
		File file = new File(filename);
		if(!file.exists()) return;
		if(!file.isFile()) return;
		*/
			if(resource != null) resource.close();
			resource = manager.getResource(filename);
			if(resource == null) return;
			File file = resource.getFile();
						
			SF2SoundFont sf2soundfont = SF2SoundFontManager.getSF2SoundFont(file.getPath());
			
			int bankshift = (int)DoublePart.asDouble(bank);
			soundbankinterpreter = new Interpreter();
			soundbankinterpreter.add("streammode", streammode);
			soundbankinterpreter.setAutoCommit(false);
			StringBuffer code = new StringBuffer();
			
			Element root = sf2soundfont.getDocument().getDocumentElement();
			NodeList presetlist = root.getElementsByTagName("preset");			
			for (int i = 0; i < presetlist.getLength(); i++) {
				Element preset = (Element)presetlist.item(i);
				int attr_bank = Integer.parseInt(preset.getAttribute("bank"));
				int attr_preset = Integer.parseInt(preset.getAttribute("preset"));												
				soundbankinterpreter.add("sfdescription" + i , preset.getAttribute("name"));				
				if(attr_bank < 128)
				{
					String ident = attr_preset + "," + (attr_bank + bankshift);
					code.append("<- instrument(" + ident + ",sfdescription" + i + ",ch10=0) <- soundbankpreset(filename, " + attr_preset + "," + attr_bank + ",streammode=streammode);");
				}
				else
				{
					String ident2 = attr_preset + "," + (attr_bank - 128 + bankshift);
					code.append("<- instrument(" + ident2 + ",sfdescription" + i + ",ch10=1) <- soundbankpreset(filename, " + attr_preset + "," + attr_bank + ",streammode=streammode);");
				}
				
			}			
			/*
			Soundbank soundbank = MidiSystem.getSoundbank(file);
			if(!(soundbank instanceof RasmusSoundbank)) return;
			
			Instrument[] instruments = soundbank.getInstruments();
			
			*/
			/*
			for (int i = 0; i < instruments.length; i++) {
				Patch patch = instruments[i].getPatch();
				String ident = patch.getProgram() + "," + (patch.getBank() + bankshift);
				
				soundbankinterpreter.add("sfdescription" + i , instruments[i].getName());
				code.append("<- instrument(" + ident + ",sfdescription" + i + ") <- soundbankpreset(filename, " + patch.getProgram() + "," + patch.getBank() + ");");
			}
						*/
			
			try {
				soundbankinterpreter.add("filename", file.getCanonicalPath());
			} catch (IOException e1) {
				e1.printStackTrace();
			}			
			try {
				soundbankinterpreter.eval(code.toString());
			} catch (ScriptParserException e) {
				e.printStackTrace();
			}
			
			answer = soundbankinterpreter.get("output");
			output.add(answer);
			
			
		
	}
	
}

public class SF2Soundbank implements UnitFactory, MetaDataProvider {

	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Soundbank");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_UNKNOWN, MetaData.DIRECTION_OUT);
		metadata.add(1, "filename",		"Filename",		null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		metadata.add(2, "bank",			"Target Bank",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		
		List options = new ArrayList();
		options.add("0");
		options.add("1");				
		metadata.add(3, "streammode",	"Stream mode (0=disk,1=memory)",   "1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN).put("options", options);
		
		return metadata;		
	}	
		
	public UnitInstancePart newInstance(Parameters parameters) {
		return new SF2SoundbankInstance(parameters);
	}
	
}
