/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sf2;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.WeakHashMap;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.io.Resource;
import rasmus.interpreter.io.ResourceManager;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.namespace.GlobalNameSpace;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.sampled.io.AudioFile;
import rasmus.interpreter.sampled.midi.AudioRegisterVoice;
import rasmus.interpreter.sampled.midi.AudioVoiceFactory;
import rasmus.interpreter.status.GlobalStatus;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class SF2PresetInstance extends UnitInstanceAdapter implements Commitable
{
	
	static double ATTEN_EMU8K_DBFIX = 0.376287; //Reference: FluidSynth fluid_conv.h 
	
	Variable preset;
	Variable bank;
	Variable filename;
	Variable output;
	Variable streammode;
	
	PresetEntry answer = null;
	//RInterpreter interpreter = null;
	
	public void clear()
	{
		if(answer != null)
		{
			answer.deuse();
			output.remove(answer.variable);
			answer = null;
		}
		/*
		 if(interpreter != null)
		 {
		 interpreter.close();
		 interpreter = null;
		 } */
		
		
	}
	
	// SoundFont are cached in WeakHashMap, so java can garbage collect them
	
	static private class SampleEntry
	{
		int usages = 0;
		Variable variable;
		String ident;
		UnitInstancePart instance;
		
		public SampleEntry(String ident)
		{
			this.ident = ident;
			sample_garbage.add(ident);
		}
		
		public void use()
		{
			if(usages == 0) sample_garbage.remove(ident);
			usages++;
		}
		public void deuse()
		{
			usages--;
			if(usages == 0) sample_garbage.add(ident);
		}
		
		public void close()
		{
			sampletable.remove(ident);
			instance.close();
		}
	}
	
	private void initPresetGarbageCollector()
	{
		if(garbagerunned)
		{
			garbagerunned = false;
			namespace.addToCommitStack(garbagerunner);
		}
	}
	
	static boolean garbagerunned = true;
	static Commitable garbagerunner = new Commitable()
	{
		
		public int getRunLevel() {
			return RUNLEVEL_INIT;
		}		
		
		public void commit()
		{
			synchronized(garbagerunner)
			{
			garbagerunned = true;
			
			Iterator<String> iter = preset_garbage.iterator();
			while (iter.hasNext()) {
				 String ident = iter.next();
				 PresetEntry entry = presettable.get(ident);
				 entry.close();
			}
			preset_garbage.clear();
			
			iter = sample_garbage.iterator();
			while (iter.hasNext()) {
				 String ident = iter.next();
				 SampleEntry entry = sampletable.get(ident);
				 entry.close();
			}
			sample_garbage.clear();
			}
		}
	};
	
	static private HashSet sample_garbage = new HashSet();
	static private Map<String, SampleEntry> sampletable = new WeakHashMap<String, SampleEntry>(); // WeakHashMap
	static private SampleEntry loadSample(String filename, long offset, long length, long loopstart, long loopend, int streammode)
	{
		NameSpace namespace = GlobalNameSpace.getNameSpace();
		
		String ident = filename + ":" + offset + ":" + length + ":" + loopstart + ":" + loopend + ":stream=" + streammode;
		SampleEntry samplevar = sampletable.get(ident);
		if(samplevar != null) return samplevar;
		
		samplevar = new SampleEntry(ident);
		samplevar.variable = new Variable();
		
		Parameters uparams = new Parameters(namespace);
		uparams.setParameter(1,ObjectsPart.asVariable(filename));
		uparams.setParameter(2,DoublePart.asVariable(offset));
		uparams.setParameter(3,DoublePart.asVariable(length));
		if(loopstart != 0 || loopend != 0 )
		{
			uparams.setParameter("loopstart", DoublePart.asVariable(loopstart) );
			uparams.setParameter("loopend", DoublePart.asVariable(loopend) );
		}
		uparams.setParameter("output", samplevar.variable );
		uparams.setParameter("streammode", streammode);
		samplevar.instance = new AudioFile(true).newInstance(uparams);

		sampletable.put(ident, samplevar);
		
		return samplevar;
	}
	
	static private class PresetEntry
	{
		int usages = 0;
		Variable variable;
		String ident;
		SampleEntry[] samples;
		
		public PresetEntry(String ident, SampleEntry[] samples)
		{
			this.ident = ident;
			this.samples = samples;
			preset_garbage.add(ident);
		}
		
		public void use()
		{
			if(usages == 0) preset_garbage.remove(ident);
			usages++;
			for (int i = 0; i < samples.length; i++) {
				samples[i].use();
			}			
		}
		public void deuse()
		{
			usages--;
			if(usages == 0) preset_garbage.add(ident);
			for (int i = 0; i < samples.length; i++) {
				samples[i].deuse();
			}					
		}
		
		public void close()
		{
			presettable.remove(ident);
			samples = null;
		}
	}
	
	static private HashSet preset_garbage = new HashSet();
	static private Map<String, PresetEntry> presettable = new WeakHashMap<String, PresetEntry>(); // WeakHashMap	
	static private PresetEntry loadPreset(String filename, int presetid, int bankid, Map addparameters, NameSpace namespace, int streammode)
	{
		
		synchronized(garbagerunner)
		{
		String presetcacheid = filename +" : " + presetid+"."+bankid + ":stream=" + streammode;
		
		if(addparameters.size() != 0)
		{
			TreeSet treeset = new TreeSet();
			Iterator iterator = addparameters.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator.next();
				treeset.add(entry.getKey() + "=" + DoublePart.asDouble((Variable)entry.getValue()) + ";"); 
			}
			StringBuilder builder = new StringBuilder();
			iterator = treeset.iterator();
			while (iterator.hasNext()) {
				String entry = (String) iterator.next();
				builder.append(entry);
			}
			presetcacheid += ":" + builder.toString();
		}
		
		//synchronized(presettable)
		{
			PresetEntry answer = presettable.get(presetcacheid);
			if(answer != null) return answer;
		}
		
		
		GlobalStatus.setStatus("loading " + filename + " : " + presetid+"."+bankid);	
		
		//RInterpreter interpreter = new RInterpreter();
		AudioVoiceFactory AudioVoiceFactory = new AudioVoiceFactory(); //   interpreter.get("AudioVoiceFactory");
		AudioRegisterVoice RegisterVoice = new AudioRegisterVoice(); //interpreter.get("RegisterVoice");
		Variable voices = new Variable(); //interpreter.get("voices");
		
		int gen_no = 0;
		
		GlobalStatus.setStatus("parsing " + filename + " : " + presetid+"."+bankid);
		
		Interpreter interpreter = null;
		
		SF2SoundFont sf2 = SF2SoundFontManager.getSF2SoundFont(filename);
		
		// sf2infotable
		GlobalStatus.setStatus("processing " + filename + " : " + presetid+"."+bankid);
		
		Element preset_element = sf2.getPreset(presetid,bankid);
		
		if(preset_element != null)
		{
			ArrayList<SampleEntry> sampleslist = new ArrayList<SampleEntry>();
			
			NodeList instruments = preset_element.getElementsByTagName("generator");
			Element global_preset_generator = null;
			for (int i = 0; i < instruments.getLength(); i++) {
				Element preset_generator = (Element) instruments.item(i);
				
				String instrumentid = preset_generator.getAttribute("instrument");
				if(instrumentid.length() == 0)
				{
					global_preset_generator = preset_generator;
				}
				else
				{
					Element global_instrument_generator = null;
					Element instrument_element = sf2.getInstrument(parseInt(instrumentid));
					NodeList samples = instrument_element.getElementsByTagName("generator");
					for (int j = 0; j < samples.getLength(); j++) {
						Element instrument_generator = ((Element) samples.item(j));
						String sampleid = instrument_generator.getAttribute("sampleID");
						if(sampleid.length() == 0)
						{
							global_instrument_generator = instrument_generator;
						}
						else
						{
							Element sample_element = sf2.getSample(parseInt(sampleid));
							

							int startAddrsOffset = 0;
							int endAddrsOffset = 0;					
							int startloopAddrsOffset = 0;
							int endloopAddrsOffset = 0;					
							int startAddrsCoarseOffset = 0;     // *32768 --> startAddrsOffset
							int endAddrsCoarseOffset = 0;       // *32768 --> endAddrsOffset					
							int startloopAddrsCoarseOffset = 0; // *32768 --> startloopAddrsOffset
							int endloopAddrsCoarseOffset = 0;   // *32768 --> endloopAddrsOffset
							
							int keyRange_from = 0;
							int keyRange_to = 127;
							int velRange_from = 0;
							int velRange_to = 127;
							int overridingRootKey = 60;
							
							int delayVolEnv = -12000;
							int attackVolEnv = -12000;
							int holdVolEnv = -12000;
							int decayVolEnv = -12000;
							int sustainVolEnv = 0;
							int releaseVolEnv = -12000;
							
							int delayModEnv = -12000;
							int attackModEnv = -12000;
							int holdModEnv = -12000;
							int decayModEnv = -12000;
							int sustainModEnv = 0;
							int releaseModEnv = -12000;					
							
							int scaleTuning = 100;
							int coarseTune = 0;
							int fineTune = 0;	
							int samplemode = 0;
							int initialAttenuation = 0;
							
							int velocity = -1;
							int keynum = -1;
							
							int modEnvToPitch = 0;
							int modEnvToFilterFc = 0;
							int initialFilterFc = 13500;
							int initialFilterQ = 0;									
							
							int pan = 0;
							int chorusEffectsSend = 0;
							int reverbEffectsSend = 0;
							
							int delayModLFO= -12000;
							int freqModLFO = 0;
							int modLfoToPitch = 0;
							int modLfoToFilterFc = 0;
							int modLfoToVolume = 0;
							
							int delayVibLFO = -12000;
							int freqVibLFO = 0;
							int vibLfoToPitch = 0;
							
							int keynumToModEnvHold = 0;
							int keynumToModEnvDecay = 0;
							int keynumToVolEnvHold = 0;
							int keynumToVolEnvDecay = 0;
							
							int exclusiveClass = -1;
							
							
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"initialAttenuation").length() != 0)
								initialAttenuation = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"initialAttenuation"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"delayVolEnv").length() != 0)
								delayVolEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"delayVolEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"attackVolEnv").length() != 0)
								attackVolEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"attackVolEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"holdVolEnv").length() != 0)
								holdVolEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"holdVolEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"decayVolEnv").length() != 0)
								decayVolEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"decayVolEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"sustainVolEnv").length() != 0)
								sustainVolEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"sustainVolEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"releaseVolEnv").length() != 0)
								releaseVolEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"releaseVolEnv"));					
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"attackModEnv").length() != 0)
								attackModEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"attackModEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"holdModEnv").length() != 0)
								holdModEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"holdModEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"decayModEnv").length() != 0)
								decayModEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"decayModEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"sustainModEnv").length() != 0)
								sustainModEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"sustainModEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"releaseModEnv").length() != 0)
								releaseModEnv = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"releaseModEnv"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"modEnvToPitch").length() != 0)
								modEnvToPitch = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"modEnvToPitch"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"modEnvToFilterFc").length() != 0)
								modEnvToFilterFc = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"modEnvToFilterFc"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"initialFilterFc").length() != 0)
								initialFilterFc = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"initialFilterFc"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"initialFilterQ").length() != 0)
								initialFilterQ = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"initialFilterQ"));														
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"pan").length() != 0)
								pan = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"pan"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"chorusEffectsSend").length() != 0)
								chorusEffectsSend = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"chorusEffectsSend"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"reverbEffectsSend").length() != 0)
								reverbEffectsSend = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"reverbEffectsSend"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"scaleTuning").length() != 0)
								scaleTuning = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"scaleTuning"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"coarseTune").length() != 0)
								coarseTune = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"coarseTune"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"fineTune").length() != 0)
								fineTune = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"fineTune"));					
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"sampleModes").length() != 0)
								samplemode = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"sampleModes"));							
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"velocity").length() != 0)
								velocity = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"velocity"));							
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynum").length() != 0)
								keynum = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynum"));							
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"startAddrsOffset").length() != 0)
								startAddrsOffset = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"startAddrsOffset"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"endAddrsOffset").length() != 0)
								endAddrsOffset = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"endAddrsOffset"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"startloopAddrsOffset").length() != 0)
								startloopAddrsOffset = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"startloopAddrsOffset"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"endloopAddrsOffset").length() != 0)
								endloopAddrsOffset = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"endloopAddrsOffset"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"startAddrsCoarseOffset").length() != 0)
								startAddrsCoarseOffset = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"startAddrsCoarseOffset"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"endAddrsCoarseOffset").length() != 0)
								endAddrsCoarseOffset = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"endAddrsCoarseOffset"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"startloopAddrsCoarseOffset").length() != 0)
								startloopAddrsCoarseOffset = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"startloopAddrsCoarseOffset"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"endloopAddrsCoarseOffset").length() != 0)
								endloopAddrsCoarseOffset = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"endloopAddrsCoarseOffset"));					
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"delayModLFO").length() != 0)
								delayModLFO = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"delayModLFO"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"freqModLFO").length() != 0)
								freqModLFO = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"freqModLFO"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"modLfoToPitch").length() != 0)
								modLfoToPitch = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"modLfoToPitch"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"modLfoToFilterFc").length() != 0)
								modLfoToFilterFc = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"modLfoToFilterFc"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"modLfoToVolume").length() != 0)
								modLfoToVolume = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"modLfoToVolume"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynumToModEnvHold").length() != 0)
								keynumToModEnvHold = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynumToModEnvHold"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynumToModEnvDecay").length() != 0)
								keynumToModEnvDecay = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynumToModEnvDecay"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynumToVolEnvHold").length() != 0)
								keynumToVolEnvHold = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynumToVolEnvHold"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynumToVolEnvDecay").length() != 0)
								keynumToVolEnvDecay = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"keynumToVolEnvDecay"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"exclusiveClass").length() != 0)
								exclusiveClass = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"exclusiveClass"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"delayVibLFO").length() != 0)
								delayVibLFO = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"delayVibLFO"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"freqVibLFO").length() != 0)
								freqVibLFO = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"freqVibLFO"));
							if(globalSecondAttribute(global_instrument_generator, instrument_generator,"vibLfoToPitch").length() != 0)
								vibLfoToPitch = parseInt(globalSecondAttribute(global_instrument_generator, instrument_generator,"vibLfoToPitch"));
							
							
							
							if(globalSecondAttribute(global_preset_generator, preset_generator, "initialAttenuation").length() != 0)
								initialAttenuation += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "initialAttenuation"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "delayVolEnv").length() != 0)
								delayVolEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "delayVolEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "attackVolEnv").length() != 0)
								attackVolEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "attackVolEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "holdVolEnv").length() != 0)
								holdVolEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "holdVolEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "decayVolEnv").length() != 0)
								decayVolEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "decayVolEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "sustainVolEnv").length() != 0)
								sustainVolEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "sustainVolEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "releaseVolEnv").length() != 0)
								releaseVolEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "releaseVolEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "attackModEnv").length() != 0)
								attackModEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "attackModEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "holdModEnv").length() != 0)
								holdModEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "holdModEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "decayModEnv").length() != 0)
								decayModEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "decayModEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "sustainModEnv").length() != 0)
								sustainModEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "sustainModEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "releaseModEnv").length() != 0)
								releaseModEnv += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "releaseModEnv"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "modEnvToPitch").length() != 0)
								modEnvToPitch += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "modEnvToPitch"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "modEnvToFilterFc").length() != 0)
								modEnvToFilterFc += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "modEnvToFilterFc"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "initialFilterFc").length() != 0)
								initialFilterFc += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "initialFilterFc"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "initialFilterQ").length() != 0)
								initialFilterQ += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "initialFilterQ"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "pan").length() != 0)
								pan += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "pan"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "chorusEffectsSend").length() != 0)
								chorusEffectsSend += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "chorusEffectsSend"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "reverbEffectsSend").length() != 0)
								reverbEffectsSend += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "reverbEffectsSend"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "scaleTuning").length() != 0)
								scaleTuning += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "scaleTuning"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "coarseTune").length() != 0)
								coarseTune += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "coarseTune"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "fineTune").length() != 0)
								fineTune += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "fineTune"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "sampleModes").length() != 0)
								samplemode = parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "sampleModes"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "velocity").length() != 0)
								velocity = parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "velocity"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "keynum").length() != 0)
								keynum = parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "keynum"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "startAddrsOffset").length() != 0)
								startAddrsOffset += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "startAddrsOffset"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "endAddrsOffset").length() != 0)
								endAddrsOffset += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "endAddrsOffset"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "startloopAddrsOffset").length() != 0)
								startloopAddrsOffset += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "startloopAddrsOffset"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "endloopAddrsOffset").length() != 0)
								endloopAddrsOffset += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "endloopAddrsOffset"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "startAddrsCoarseOffset").length() != 0)
								startAddrsCoarseOffset += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "startAddrsCoarseOffset"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "endAddrsCoarseOffset").length() != 0)
								endAddrsCoarseOffset += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "endAddrsCoarseOffset"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "startloopAddrsCoarseOffset").length() != 0)
								startloopAddrsCoarseOffset += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "startloopAddrsCoarseOffset"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "endloopAddrsCoarseOffset").length() != 0)
								endloopAddrsCoarseOffset += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "endloopAddrsCoarseOffset"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "delayModLFO").length() != 0)
								delayModLFO += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "delayModLFO"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "freqModLFO").length() != 0)
								freqModLFO += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "freqModLFO"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "modLfoToPitch").length() != 0)
								modLfoToPitch += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "modLfoToPitch"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "modLfoToFilterFc").length() != 0)
								modLfoToFilterFc += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "modLfoToFilterFc"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "modLfoToVolume").length() != 0)
								modLfoToVolume += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "modLfoToVolume"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "keynumToModEnvHold").length() != 0)
								keynumToModEnvHold += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "keynumToModEnvHold"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "keynumToModEnvDecay").length() != 0)
								keynumToModEnvDecay += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "keynumToModEnvDecay"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "keynumToVolEnvHold").length() != 0)
								keynumToVolEnvHold += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "keynumToVolEnvHold"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "keynumToVolEnvDecay").length() != 0)
								keynumToVolEnvDecay += Integer.parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "keynumToVolEnvDecay"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "exclusiveClass").length() != 0)
								exclusiveClass += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "exclusiveClass"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "delayVibLFO").length() != 0)
								delayVibLFO += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "delayVibLFO"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "freqVibLFO").length() != 0)
								freqVibLFO += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "freqVibLFO"));
							if(globalSecondAttribute(global_preset_generator, preset_generator, "vibLfoToPitch").length() != 0)
								vibLfoToPitch += parseInt(globalSecondAttribute(global_preset_generator, preset_generator, "vibLfoToPitch"));
							
							startAddrsOffset += startAddrsCoarseOffset*32768;
							endAddrsOffset += endAddrsCoarseOffset*32768;					
							startloopAddrsOffset += startloopAddrsCoarseOffset*32768;
							endloopAddrsOffset += endloopAddrsCoarseOffset*32768;					
							
							
							if(sample_element.getAttribute("pitchcorrection").length() != 0)
								fineTune += parseInt(sample_element.getAttribute("pitchcorrection"));					
							if(sample_element.getAttribute("originalkey").length() != 0)
								overridingRootKey = parseInt(sample_element.getAttribute("originalkey"));
							
							if(instrument_generator.getAttribute("overridingRootKey").length() != 0)
								overridingRootKey = parseInt(instrument_generator.getAttribute("overridingRootKey"));
							if(global_instrument_generator != null)
								if(global_instrument_generator.getAttribute("overridingRootKey").length() != 0)
									overridingRootKey = parseInt(global_instrument_generator.getAttribute("overridingRootKey"));
							if(preset_generator.getAttribute("overridingRootKey").length() != 0)
								overridingRootKey = parseInt(preset_generator.getAttribute("overridingRootKey"));
							if(global_preset_generator != null)
								if(global_preset_generator.getAttribute("overridingRootKey").length() != 0)
									overridingRootKey = parseInt(global_preset_generator.getAttribute("overridingRootKey"));
							
							if(instrument_generator.getAttribute("keyRange").length() != 0)
							{
								StringTokenizer st = new StringTokenizer(instrument_generator.getAttribute("keyRange"), "-");
								keyRange_from = parseInt(st.nextToken().trim());
								keyRange_to = parseInt(st.nextToken().trim());
							}
							if(instrument_generator.getAttribute("velRange").length() != 0)
							{
								StringTokenizer st = new StringTokenizer(instrument_generator.getAttribute("velRange"), "-");
								velRange_from = parseInt(st.nextToken().trim());
								velRange_to = parseInt(st.nextToken().trim());
							}				
							if(preset_generator.getAttribute("keyRange").length() != 0)
							{
								StringTokenizer st = new StringTokenizer(preset_generator.getAttribute("keyRange"), "-");
								int a = parseInt(st.nextToken().trim());
								int b = parseInt(st.nextToken().trim());
								if(a > keyRange_from) keyRange_from = a;
								if(b < keyRange_to) keyRange_to = b;
							}
							if(preset_generator.getAttribute("velRange").length() != 0)
							{
								StringTokenizer st = new StringTokenizer(preset_generator.getAttribute("velRange"), "-");
								int a = parseInt(st.nextToken().trim());
								int b = parseInt(st.nextToken().trim());
								if(a > velRange_from) velRange_from = a;
								if(b < velRange_to) velRange_to = b;
							}							
							
							String samplerate = sample_element.getAttribute("samplerate");
							
//							source.append("SAMPLE ID=" + sampleid + " : K:" + keyRange_from + "-" + keyRange_to + " : V:" + velRange_from + "-" + velRange_to +  + CRLF);
							
							double d_delayVolEnv = Math.pow(2, delayVolEnv/1200.0);
							if(delayVolEnv <= -12000) d_delayVolEnv = 0;
							double d_attackVolEnv = Math.pow(2, attackVolEnv/1200.0);
							if(attackVolEnv <= -12000) d_attackVolEnv = 0;
							double d_holdVolEnv = Math.pow(2, holdVolEnv/1200.0);
							double d_decayVolEnv = Math.pow(2, decayVolEnv/1200.0);
							double d_sustainVolEnv = Math.pow(10, -(sustainVolEnv/10)/20.0);
							double d_releaseVolEnv = Math.pow(2, releaseVolEnv/1200.0);					
							if(d_releaseVolEnv < 0.05) d_releaseVolEnv = 0.05;
							
							
							double d_delayModEnv = Math.pow(2, delayModEnv/1200.0);
							double d_attackModEnv = Math.pow(2, attackModEnv/1200.0);
							double d_holdModEnv = Math.pow(2, holdModEnv/1200.0);
							double d_decayModEnv = Math.pow(2, decayModEnv/1200.0);
							double d_sustainModEnv = 1 - (sustainModEnv/1000.0);
							if(d_sustainModEnv < 0) d_sustainModEnv = 0;
							if(d_sustainModEnv > 1) d_sustainModEnv = 1;
							double d_releaseModEnv = Math.pow(2, releaseModEnv/1200.0);					
							if(d_releaseModEnv < 0.05) d_releaseModEnv = 0.05;		
							
							DecimalFormat d = new DecimalFormat("0.########", new java.text.DecimalFormatSymbols(java.util.Locale.ENGLISH));
							
							/* source.append("rem_sf2preset_" + gen_no + "  <- function(note, velocity, active) " + CRLF);
							 source.append("{ " + CRLF);*/										
							
							
							// Ath. hvort nau�synlegt s� a� vera me� modulation envelope
							/*
							 if(modEnvToFilterFc > 0 || modEnvToPitch > 0)
							 {
							 source.append("mod_env <- ahdsr(active," + d.format(d_attackModEnv) + ", " + d.format(d_holdModEnv) + ", " + d.format(d_decayModEnv) + ", " + d.format(d_sustainModEnv) + "," + d.format(d_releaseModEnv) + ");" + CRLF);
							 }					*/
							/*
							 String generator_exp = "generator";
							 String noteexp = "note";*/
							
							// Ath. hvort nau�synlegt s� a� nota LowPass filter
							/*
							 if((initialFilterFc < 13500 && initialFilterQ == 0) || (modEnvToFilterFc > 0))						
							 {
							 double freq = Math.pow(2, initialFilterFc / 1200.0) * 8.21; // 20hz = 1500, 20khz = 13500
							 double q_db = Math.pow(10, -(initialFilterQ/10)/20.0); 
							 
							 String freqexp = Double.toString(freq);
							 if(modEnvToFilterFc > 0)
							 {
							 double d_modEnvToFilterFc = modEnvToFilterFc / 1000.0;
							 freqexp = freqexp + "*pow(2, mod_env*" +d_modEnvToFilterFc + ")";
							 }
							 
							 source.append("wgenerator <- sf2_lowpass(generator, " + freqexp + ", " + q_db + ");" + CRLF);
							 generator_exp = "wgenerator";
							 }*/
							
							
							// Ath. hvort vi� s�um me� modulation envlope � pitch
							/*
							 if(modEnvToPitch > 0)
							 {
							 noteexp = noteexp + "*pow(2, mod_env*" + (modEnvToPitch/1000.0) + ")";
							 }					*/
							
							
							
							/*resamplei((" + samplerate + "/48000)*pow(2, (note-" + overridingRootKey + ") / 12)) <- */
							
							//System.out.println("fineTune = " + fineTune + ", pan = " + pan);
							double notetune = - (double)overridingRootKey + (double)coarseTune + ((double)fineTune / 100.0);
							double scaletune = (double)scaleTuning / 50.0;
							
							//String initantexp = "";
							if(initialAttenuation < 0)
							{
								initialAttenuation = 0;
							}
							double d_initialAttenuation = Math.pow(10, -ATTEN_EMU8K_DBFIX*(initialAttenuation/10)/20.0);
							/*
							 if(initialAttenuation != 0)
							 {
							 initantexp = "*" + d_initialAttenuation;
							 
							 }*/
							
							
							boolean looped = false;
							// 0 indicates a sound reproduced with no loop
							// 1 indicates a sound which loops continuously
							// 2 is unused but should be interpreted as indicating no loop
							// 3 indicates a sound which loops for the duration of key depression then proceeds to play the remainder of the sample.					
							if((samplemode == 1) || (samplemode == 3))
							{
								looped = true;
							}				
							
							//String samplevar = null;
							
							Variable samplevar = null;
							/*
							 if(looped)
							 {
							 samplevar = "looped_sample_" + sampleid;
							 if(!sample_used_looped.contains(sampleid)) sample_used_looped.add(sampleid);
							 }
							 else
							 {
							 samplevar = "sample_" + sampleid;
							 if(!sample_used.contains(sampleid)) sample_used.add(sampleid);
							 } */
							
							Element sampleinfo = sf2.getSample(parseInt(sampleid));
							if(sampleinfo != null) 
							{
								long offset = Long.parseLong(sampleinfo.getAttribute("offset"));
								long start = Long.parseLong(sampleinfo.getAttribute("start"));
								long length = Long.parseLong(sampleinfo.getAttribute("datalength"));
								
								start += startAddrsOffset;
								offset += startAddrsOffset*2;
								length += endAddrsOffset*2;
								
								long loopstart = Long.parseLong(sampleinfo.getAttribute("loopstart"));
								long loopend = Long.parseLong(sampleinfo.getAttribute("loopend"));
								if(loopstart < 0) loopstart = 0;
								if(loopend < 0) loopend = 0;
								
								if(loopstart != 0) loopstart = (loopstart - start)*2;
								if(loopend != 0) loopend = (loopend - start)*2;		
								
								loopstart += startloopAddrsOffset*2;
								loopend += endloopAddrsOffset*2;					
								
								if(loopend - loopstart < 0)
								{
									System.out.println("loop error");
									looped = false;
								}		
								/*
								Map uparams = new InCaseSensativeMap();
								uparams.put("1",RObjects.asVariable(filename));
								uparams.put("2",RDouble.asVariable(offset));
								uparams.put("3",RDouble.asVariable(length));
								
								if(looped)
								{
									
									
									
									uparams.put("loopstart", RDouble.asVariable(loopstart) );
									uparams.put("loopend", RDouble.asVariable(loopend) );
									//sampleexpr = "<- Sample(\"" + filename + "\", " + offset + ", " + length  + ", loopstart=" + loopstart  + ", loopend=" + loopend + ");";
								}
								
								samplevar = new RVariable();
								uparams.put("output", samplevar ); // interpreter.get("sf2preset_" + gen_no)
								
								
								RUnitInstance unitinstance =  RUnit.newInstance(Sample, uparams);
								interpreter.addUnit(unitinstance);
								*/
								
								if(!looped)
								{
									loopstart = 0;
									loopend = 0;
								}
								
//								//TODO
								SampleEntry sampleentry = loadSample(filename, offset, length, loopstart, loopend, streammode);;
								samplevar = sampleentry.variable;
								sampleslist.add(sampleentry);
								
								//samplevar = new RVariable();
								
							}
							
							
							if(samplevar != null)
							{
								Parameters uparams = new Parameters(namespace);
								Map<String, String> audiofactoryparams = new HashMap<String, String>();
								
								if(pan > 500) pan = 500;
								if(pan < -500) pan = -500;
								
								audiofactoryparams.put("velocity", Integer.toString(velocity)); 
								audiofactoryparams.put("keynum", Integer.toString(keynum ));
								audiofactoryparams.put("pan", d.format((pan)/1000.0));
								audiofactoryparams.put("ch1Vol", d.format(Math.cos((0.5 + (pan)/500.0 ) * Math.PI * 0.5 ))); // LEFT
								audiofactoryparams.put("ch2Vol", d.format(Math.sin((0.5 + (pan)/500.0 ) * Math.PI * 0.5))); // RIGHT
								audiofactoryparams.put("ch3Vol", d.format((reverbEffectsSend)/1000.0)); 
								audiofactoryparams.put("ch4Vol", d.format((chorusEffectsSend)/1000.0));					
								audiofactoryparams.put("initialAttenuation", d.format(d_initialAttenuation));
								
								audiofactoryparams.put("delayVolEnv", d.format(d_delayVolEnv));
								audiofactoryparams.put("attackVolEnv", d.format(d_attackVolEnv));
								audiofactoryparams.put("holdVolEnv", d.format(d_holdVolEnv));
								audiofactoryparams.put("decayVolEnv", d.format(d_decayVolEnv));
								audiofactoryparams.put("sustainVolEnv", d.format(d_sustainVolEnv));
								audiofactoryparams.put("releaseVolEnv", d.format(d_releaseVolEnv));
								
								audiofactoryparams.put("delayModEnv", d.format(d_delayModEnv));
								audiofactoryparams.put("attackModEnv", d.format(d_attackModEnv));
								audiofactoryparams.put("holdModEnv", d.format(d_holdModEnv));
								audiofactoryparams.put("decayModEnv", d.format(d_decayModEnv));
								audiofactoryparams.put("sustainModEnv", d.format(d_sustainModEnv));
								audiofactoryparams.put("releaseModEnv", d.format(d_releaseModEnv));			
								audiofactoryparams.put("modEnvToPitch", d.format(modEnvToPitch/1200.0));
								audiofactoryparams.put("modEnvToFilterFc", d.format(modEnvToFilterFc/1200.0)); 
								
								uparams.setParameter("sample", samplevar );
								audiofactoryparams.put("sampleRate", samplerate );
								audiofactoryparams.put("scaleTune", d.format(scaletune));
								audiofactoryparams.put("coarseTune", d.format(notetune));
								
								audiofactoryparams.put("initialFilterFc", d.format(8.177 * Math.pow(2,initialFilterFc/1200.0) ));
								
								// ATTEN_EMU8K_DBFIX
								audiofactoryparams.put("initialFilterQ", d.format(initialFilterQ/10.0));					
								
								audiofactoryparams.put("delayModLFO", "" + d.format(((delayModLFO==-32768)?0:(Math.pow(2, delayModLFO/1200.0)))));
								audiofactoryparams.put("freqModLFO", d.format(8.177 * Math.pow(2,freqModLFO/1200.0) ));
								audiofactoryparams.put("modLfoToPitch", d.format(modLfoToPitch/1200.0));
								audiofactoryparams.put("modLfoToFilterFc", d.format(modLfoToFilterFc/1200.0));
								audiofactoryparams.put("modLfoToVolume", d.format(Math.pow(10, -(modLfoToVolume/10)/20.0)));
								
								audiofactoryparams.put("delayVibLFO", "" + d.format(((delayModLFO==-32768)?0:(Math.pow(2, delayVibLFO/1200.0))))); 
								audiofactoryparams.put("freqVibLFO", d.format(8.177 * Math.pow(2,freqVibLFO/1200) ));
								audiofactoryparams.put("vibLfoToPitch", d.format(vibLfoToPitch/1200.0));
								
								audiofactoryparams.put("keynumToModEnvHold", d.format(keynumToModEnvHold / 100.0));
								audiofactoryparams.put("keynumToModEnvDecay", d.format(keynumToModEnvDecay / 100.0));
								audiofactoryparams.put("keynumToVolEnvHold", d.format(keynumToVolEnvHold / 100.0));
								audiofactoryparams.put("keynumToVolEnvDecay", d.format(keynumToVolEnvDecay / 100.0));
																
								if(addparameters.size() != 0)
								{
									Iterator entry_iterator = addparameters.entrySet().iterator();
									while (entry_iterator.hasNext()) {
										Map.Entry entry = (Map.Entry) entry_iterator.next();
										String extvar = "external_" + (String)entry.getKey();
										if(((String)entry.getKey()).startsWith("add_"))
										{
											String keyname = ((String)entry.getKey()).substring(4);
											String value = audiofactoryparams.get(keyname);
											if(value == null) value = extvar; else value = "(" + value + ") + " + extvar;									
											audiofactoryparams.put(keyname, value);
										}
										else
											if(((String)entry.getKey()).startsWith("mul_"))
											{
												String keyname = ((String)entry.getKey()).substring(4);
												String value = audiofactoryparams.get(keyname);
												if(value != null)
												{
													value = "(" + value + ") * " + extvar;									
													audiofactoryparams.put(keyname, value);
												}
												
											}
											else
												audiofactoryparams.put((String)entry.getKey(), extvar);								
									}
								}
								
								
								
								Iterator entry_iterator = audiofactoryparams.entrySet().iterator();
								while (entry_iterator.hasNext()) {
									Map.Entry entry = (Map.Entry) entry_iterator.next();
									
									String val = (String)entry.getValue();
									String name = (String)entry.getKey();
									if(val.length() != 0)
									{
										
									    
										if(val.startsWith("("))
										{
											if(interpreter == null)
											{
												interpreter = new Interpreter();
												interpreter.setAutoCommit(false);
											}
											try {
												uparams.setParameter(name, interpreter.eval(val));
											} catch (ScriptParserException e) {
												e.printStackTrace();
											}
										}
										else
										{
											try
											{ 
										//RInterpreter interpreter = new RInterpreter();
										//uparams.put(name, interpreter.eval(val));
												uparams.setParameter(name, new Double(Double.parseDouble(val)));
												
											}
											catch(Exception e)
											{
												if(interpreter == null)
												{
													interpreter = new Interpreter();
													interpreter.setAutoCommit(false);
												}
												try {
													uparams.setParameter(name, interpreter.eval(val));
												} catch (ScriptParserException e1) {
													e1.printStackTrace();
												}
											}
										}
									}
									
								}			
								
								Variable sf2preset = new Variable();
								uparams.setParameter("output", sf2preset ); // interpreter.get("sf2preset_" + gen_no)
								
								//TODO
								/*RUnitInstance unitinstance = */ AudioVoiceFactory.newInstance(uparams);
								//interpreter.addUnit(unitinstance);
								
								uparams = new Parameters(namespace);						
								if(exclusiveClass != -1)
								{
									uparams.setParameter("exclusiveClass", new Double(exclusiveClass));
								}						
								
								uparams.setParameter("output", voices);
								uparams.setParameter("input", sf2preset);
								uparams.setParameter("keyfrom", new Double(keyRange_from));
								uparams.setParameter("keyto", new Double(keyRange_to));
								uparams.setParameter("velfrom", new Double(velRange_from));
								uparams.setParameter("velto", new Double(velRange_to));
								uparams.setParameter("name", ObjectsPart.asVariable(sampleinfo.getAttribute("name")));
								//TODO
								/*unitinstance =*/ RegisterVoice.newInstance(uparams);
								//interpreter.addUnit(unitinstance);
								
								
								gen_no++;
								
							}
							
							
						}
					}
				}
			}
			
			if(interpreter != null)
			if(addparameters.size() != 0)
			{
				Iterator entry_iterator = addparameters.entrySet().iterator();
				while (entry_iterator.hasNext()) {
					Map.Entry entry = (Map.Entry) entry_iterator.next();				
					interpreter.add("external_" + (String)entry.getKey(), DoublePart.asVariable( DoublePart.asDouble( (Variable)entry.getValue() )));				
				}
			}
			
			SampleEntry[] samples_entries = new SampleEntry[sampleslist.size()];
			sampleslist.toArray(samples_entries); 
			
			PresetEntry answer = new PresetEntry(presetcacheid, samples_entries);
			answer.variable = voices;
			
			presettable.put(presetcacheid, answer);
			GlobalStatus.setStatus("");
			
			return answer;
		}
		
		return null;
		}
	}
	
	public static int parseInt(String value)
	{
		try
		{
			return Integer.parseInt(value);
		}
		catch(Throwable e)
		{
			return 0;
		}
	}
	
	public static String globalSecondAttribute(Element global, Element primary, String attribute)
	{
		if(primary != null) if(primary.getAttribute(attribute).length() != 0) return primary.getAttribute(attribute);
		if(global != null) if(global.getAttribute(attribute).length() != 0) return global.getAttribute(attribute);
		return "";
	}
	
	public String expandPath(String path)
	{		
		path = path.trim();
		if(path.length() == 0) return path;
		if(path.startsWith(File.separator)) return path;		
		if(path.length() > 2 && Character.isLetter(path.charAt(0)) && path.charAt(1) == ':' && path.charAt(2) == File.separatorChar)
				return path;
	
		String wrkdir = ObjectsPart.toString(this.wrkdir);
		if(wrkdir.trim().length() == 0)
			return path;					
		
		if(wrkdir.endsWith(File.separator))
		{
			return wrkdir + path;
		}
		else
		{
			return wrkdir + File.separator + path;
		}
	}
	
	Resource resource = null;

    public void calc() {

    	namespace.addToCommitStack(this);
    }
	    	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
    
	public void commit()
	{
		
		clear();
		
		initPresetGarbageCollector();
		
		String filename = expandPath(ObjectsPart.toString(this.filename));
		int presetid = (int)DoublePart.asDouble(preset);
		int bankid = (int)DoublePart.asDouble(bank);
							
		if(resource != null) resource.close();
		resource = manager.getResource(filename);
		if(resource == null) return;
		File file = resource.getFile();		
		filename = file.getPath();
		
		int i_streammode = (int)DoublePart.asDouble(streammode);
		
		answer = loadPreset(filename, presetid, bankid, addparameters, namespace, i_streammode);
		if(answer != null)
		{
			answer.use();
			output.add(answer.variable);
			return;
		}
		
		
	}
	
	Map addparameters = new HashMap();
	NameSpace namespace;
	Variable wrkdir;
	
	ResourceManager manager;	
	
	public SF2PresetInstance(Parameters parameters)
	{
		
		namespace = parameters.getNameSpace();
		manager = ResourceManager.getInstance(namespace);
		
		output = parameters.getParameterWithDefault("output");
		filename = parameters.getParameterWithDefault(1, "filename");
		preset = parameters.getParameterWithDefault(2, "preset");
		bank = parameters.getParameterWithDefault(3, "bank");
		streammode = parameters.getParameterWithDefault(4, "streammode");
		wrkdir = parameters.get("wrkdir");
		
		addparameters.putAll(parameters.getParameters());
		addparameters.remove("output");
		addparameters.remove("filename");
		addparameters.remove("preset");
		addparameters.remove("bank");
		addparameters.remove("0");
		addparameters.remove("1");
		addparameters.remove("2");
		addparameters.remove("3");
		addparameters.remove("4");		
		addparameters.remove("5");		
		
		Iterator iterator = addparameters.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();
			DoublePart.getInstance((Variable)entry.getValue()).addListener(this);			
		}
		
		DoublePart.getInstance(preset).addListener(this);
		DoublePart.getInstance(bank).addListener(this);
		DoublePart.getInstance(streammode).addListener(this);
		
		ObjectsPart.getInstance(filename).addListener(this);
		ObjectsPart.getInstance(wrkdir).addListener(this);
		
		calc();
	}
	public void close() {
		
		
		clear();
		
		if(resource != null)
		{
			resource.close();
			resource  = null;
		}		
		Iterator iterator = addparameters.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();
			DoublePart.getInstance((Variable)entry.getValue()).removeListener(this);			
		}		
		DoublePart.getInstance(preset).removeListener(this);
		DoublePart.getInstance(bank).removeListener(this);
		DoublePart.getInstance(streammode).removeListener(this);
		ObjectsPart.getInstance(filename).removeListener(this);
		ObjectsPart.getInstance(wrkdir).removeListener(this);
	}
	
	
}

public class SF2Preset implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("SoundFont 2 Preset");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_UNKNOWN, MetaData.DIRECTION_OUT);
		metadata.add(1, "filename",		"Filename",		null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		metadata.add(2, "preset",		"Preset number",null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "bank",			"Bank number",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);

		List options = new ArrayList();
		options.add("0");
		options.add("1");				
		metadata.add(4, "streammode",	"Stream mode (0=disk,1=memory)",   "0", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN).put("options", options);
				
		return metadata;		
	}	
		
	public UnitInstancePart newInstance(Parameters parameters) {
		return new SF2PresetInstance(parameters);
	}
}
