/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sf2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import rasmus.util.riff.sf2.SoundFont;

public class SF2SoundFont {
	
	private SoundFont soundfont;
	private Document sfdoc ;
	private Hashtable sampleinfotable = new Hashtable();
	private Hashtable presetinfotable = new Hashtable();
	private Hashtable instrumentinfotable = new Hashtable();
	
	public Document getDocument()
	{
		return sfdoc;
	}
	public SoundFont getSoundFont()
	{
		return soundfont;
	}
	public Element getSample(int id)
	{
		return (Element)sampleinfotable.get(Integer.toString(id));
	}
	public Element getPreset(int id)
	{
		return getPreset(id, 0);
	}
	public Element getPreset(int id, int bank)
	{
		return (Element)presetinfotable.get(Integer.toString(id)+"."+Integer.toString(bank));
	}
	public Element getInstrument(int id)
	{
		return (Element)instrumentinfotable.get(Integer.toString(id));
	}		
	
	public static void main(String[] args)
	{
		
		String filename = "C:\\Documents and Settings\\kalli\\My Documents\\broken_bassdrum.sf2"; 
		//"C:\\realmaker2005\\SoundFonts\\Misc\\synth.sf2";
		SF2SoundFont sf2 = SF2SoundFontManager.getSF2SoundFont(filename);
		Document document = sf2.getDocument();
		
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = tFactory.newTransformer();
			FileOutputStream fo = new FileOutputStream("C:\\broken_bassdrum.xml");
			transformer.transform(
					new DOMSource(document),
					new StreamResult(fo));
			fo.close();
		} catch (Exception e) {
		}
		
		System.out.println("sf2preset  <- function(note, velocity, active) ");
		System.out.println("{ ");
		
		/*
		 drumkit_voice <- function(note, velocity, active)
		 {
		 out <- if((note = 60) , drumkit_sample0);
		 out <- if((note = 61) , drumkit_sample1);
		 out <- if((note = 62) , drumkit_sample2);
		 out <- if((note = 63) , drumkit_sample3);
		 out <- if((note = 64) , drumkit_sample4);
		 out <- if((note = 65) , drumkit_sample5);
		 out <- if((note = 66) , drumkit_sample6);
		 out <- if((note = 67) , drumkit_sample7);
		 out <- if((note = 68) , drumkit_sample8);
		 out <- if((note = 69) , drumkit_sample9);
		 out <- if((note = 70) , drumkit_sample10);
		 out <- if((note = 71) , drumkit_sample11);
		 out <- if((note = 72) , drumkit_sample12);
		 
		 output <- channelmux(out, out)*velocity;
		 };		 */
		
		int gen_no = 0;
		
		ArrayList sample_used = new ArrayList();
		Element preset_element = sf2.getPreset(0);
		NodeList instruments = preset_element.getElementsByTagName("generator");
		for (int i = 0; i < instruments.getLength(); i++) {
			Element preset_generator = (Element) instruments.item(i);
			String instrumentid = preset_generator.getAttribute("instrument");
			if(instrumentid.length() != 0)
			{
				Element instrument_element = sf2.getInstrument(Integer.parseInt(instrumentid));
				NodeList samples = instrument_element.getElementsByTagName("generator");
				for (int j = 0; j < samples.getLength(); j++) {
					Element instrument_generator = ((Element) samples.item(j));
					String sampleid = instrument_generator.getAttribute("sampleID");
					if(sampleid.length() != 0)
					{
						Element sample_element = sf2.getSample(Integer.parseInt(sampleid));
						
						if(!sample_used.contains(sampleid)) sample_used.add(sampleid);
						
						
						
						
						int keyRange_from = 0;
						int keyRange_to = 127;
						int velRange_from = 0;
						int velRange_to = 127;
						int overridingRootKey = 60;
						if(sample_element.getAttribute("originalkey").length() != 0)
							overridingRootKey = Integer.parseInt(sample_element.getAttribute("originalkey"));
						if(instrument_generator.getAttribute("overridingRootKey").length() != 0)
							overridingRootKey = Integer.parseInt(instrument_generator.getAttribute("overridingRootKey"));
						if(preset_generator.getAttribute("overridingRootKey").length() != 0)
							overridingRootKey = Integer.parseInt(preset_generator.getAttribute("overridingRootKey"));
						
						if(instrument_generator.getAttribute("keyRange").length() != 0)
						{
							StringTokenizer st = new StringTokenizer(instrument_generator.getAttribute("keyRange"), "-");
							keyRange_from = Integer.parseInt(st.nextToken().trim());
							keyRange_to = Integer.parseInt(st.nextToken().trim());
						}
						if(instrument_generator.getAttribute("velRange").length() != 0)
						{
							StringTokenizer st = new StringTokenizer(instrument_generator.getAttribute("velRange"), "-");
							velRange_from = Integer.parseInt(st.nextToken().trim());
							velRange_to = Integer.parseInt(st.nextToken().trim());
						}				
						if(preset_generator.getAttribute("keyRange").length() != 0)
						{
							StringTokenizer st = new StringTokenizer(preset_generator.getAttribute("keyRange"), "-");
							int a = Integer.parseInt(st.nextToken().trim());
							int b = Integer.parseInt(st.nextToken().trim());
							if(a > keyRange_from) keyRange_from = a;
							if(b > keyRange_to) keyRange_to = b;
						}
						if(preset_generator.getAttribute("velRange").length() != 0)
						{
							StringTokenizer st = new StringTokenizer(preset_generator.getAttribute("velRange"), "-");
							int a = Integer.parseInt(st.nextToken().trim());
							int b = Integer.parseInt(st.nextToken().trim());
							if(a > velRange_from) velRange_from = a;
							if(b > velRange_to) velRange_to = b;
						}							
						
						String samplerate = sample_element.getAttribute("samplerate");
						
//						System.out.println("SAMPLE ID=" + sampleid + " : K:" + keyRange_from + "-" + keyRange_to + " : V:" + velRange_from + "-" + velRange_to);
						
						System.out.println("  // " + sample_element.getAttribute("name"));
						System.out.println("  generator_" + gen_no + " <- resamplei((" + samplerate + "/48000)*pow(2, (note-" + overridingRootKey + ") / 12)) <- sample_" + sampleid + ";");					
						System.out.println("  vgenerator_" + gen_no + " <- xadsr(active,0.01, 1, 1, 0.1,0.01) * generator_" + gen_no);
						System.out.println("  out <- if(((note >= " + keyRange_from + ") * (note <= " + keyRange_to + ") * (velocity >= " + velRange_from + ") * (velocity <= " + velRange_to + ")), vgenerator_" + gen_no + ");");
						System.out.println("  ");
						gen_no++;
						
					}
				}
			}
		}
		System.out.println("  output <- channelmux(out, out)*velocity;");
		System.out.println("}; ");
		
		for (int j = 0; j < sample_used.size(); j++) {
			int sampleid = Integer.parseInt((String)sample_used.get(j));
			System.out.println("sample_" + sampleid + " <- SF2Sample(\"" + filename + "\", " + sampleid + ");");	
		}
		
		
		
	}
	protected SF2SoundFont(String filename)
	{
		
		InputStream fi;
		try {
			fi = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		SoundFont soundfont = new SoundFont();		
		
		try
		{
			try
			{
				
				soundfont.read(fi);
				sfdoc = soundfont.toDocument();
				
				NodeList samplelist = sfdoc.getElementsByTagName("sample");
				for (int i = 0; i < samplelist.getLength(); i++) {
					Element sample = (Element)samplelist.item(i);
					sampleinfotable.put(sample.getAttribute("id"), sample);
				}
				
				NodeList presetlist = sfdoc.getElementsByTagName("preset");
				for (int i = 0; i < presetlist.getLength(); i++) {
					Element preset = (Element)presetlist.item(i);
					presetinfotable.put(preset.getAttribute("preset")+"."+preset.getAttribute("bank"), preset);
				}
				
				NodeList instrumentlist = sfdoc.getElementsByTagName("instrument");
				for (int i = 0; i < instrumentlist.getLength(); i++) {
					Element instrument = (Element)instrumentlist.item(i);
					instrumentinfotable.put(instrument.getAttribute("id"), instrument);
				}				
				
				
			}
			finally
			{
				fi.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
