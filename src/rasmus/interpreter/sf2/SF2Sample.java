/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sf2;

import java.io.File;

import org.w3c.dom.Element;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.io.Resource;
import rasmus.interpreter.io.ResourceManager;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class SF2SampleInstance extends UnitInstanceAdapter
{
	
	Variable sample;
	Variable filename;
	Variable output;
	
	Variable answer = null;
	Interpreter interpreter = null;
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			answer = null;
		}
		if(interpreter != null)
		{
			interpreter.close();
			interpreter = null;
		}
		
		
	}
	
	Resource resource = null;
	public void calc() {
		clear();
		
		String filename = ObjectsPart.toString(this.filename);
		int sampleid = (int)DoublePart.asDouble(sample);
		
		if(resource != null) resource.close();
		resource = manager.getResource(filename);
		if(resource == null) return;
		File file = resource.getFile();		
		filename = file.getPath();		
		
		SF2SoundFont sf2soundfontinfo = SF2SoundFontManager.getSF2SoundFont(filename);
		Element sampleinfo = sf2soundfontinfo.getSample(sampleid);
		if(sampleinfo == null) return;
		
		long offset = Long.parseLong(sampleinfo.getAttribute("offset"));
		long length = Long.parseLong(sampleinfo.getAttribute("datalength"));
		
		interpreter = new Interpreter();
		interpreter.setAutoCommit(false);
		interpreter.add("filename", ObjectsPart.asVariable(filename));		
		interpreter.add("offset", DoublePart.asVariable(offset));
		interpreter.add("length", DoublePart.asVariable(length));
		try {
			answer = interpreter.eval("Sample(filename, offset, length)");
		} catch (ScriptParserException e) {
			e.printStackTrace();
		}
		
		output.add(answer);				
	}	
	
	NameSpace namespace;	
	
	ResourceManager manager;	
	
	public SF2SampleInstance(Parameters parameters)
	{
		
		namespace = parameters.getNameSpace();
		manager = ResourceManager.getInstance(namespace);
		
		output = parameters.getParameterWithDefault("output");
		filename = parameters.getParameterWithDefault(1, "filename");
		sample = parameters.getParameterWithDefault(2, "sample");
		
		DoublePart.getInstance(sample).addListener(this);
		ObjectsPart.getInstance(filename).addListener(this);
		
		calc();
	}
	public void close() {
		
		clear();
		
		if(resource != null)
		{
			resource.close();
			resource  = null;
		}
		
		DoublePart.getInstance(sample).removeListener(this);
		ObjectsPart.getInstance(filename).removeListener(this);		
	}
	
	
}

public class SF2Sample implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("SoundFont 2 Sample");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(1, "filename",		"Filename",		null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		metadata.add(2, "sample",		"Sample number",null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		return metadata;		
	}		
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new SF2SampleInstance(parameters);
	}
}
