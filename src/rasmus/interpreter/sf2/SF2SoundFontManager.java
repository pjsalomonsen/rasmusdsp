/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.sf2;

import java.io.File;
import java.util.WeakHashMap;

public class SF2SoundFontManager {
	
	private static WeakHashMap<String, MapEntry> cachetable = new WeakHashMap<String, MapEntry>();
	
	private static class MapEntry
	{
		SF2SoundFont soundfont;
		long lastmod;
	}
	
	public static SF2SoundFont getSF2SoundFont(String filename)
	{
		synchronized (cachetable) {
		
		MapEntry entry = cachetable.get(filename);
		long file_lastmod = new File(filename).lastModified();		
		if(entry == null)
		{
			entry = new MapEntry();
			entry.soundfont = new SF2SoundFont(filename);
			entry.lastmod = file_lastmod;
			cachetable.put(filename, entry);
		}
		else
		{
			if(entry.lastmod != file_lastmod)
				entry.soundfont = new SF2SoundFont(filename);
		}
		return entry.soundfont;
		
		}
	}
}
