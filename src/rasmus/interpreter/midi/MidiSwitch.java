/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiSwitchInstance implements UnitInstancePart, Receiver
{
	Variable output;
	Variable input;	
	Variable control;
	Variable autostart;
		
	boolean active = false;
	public void start()
	{
		synchronized (this) {
			if(active) return;
			active = true;
			output.add(input);
			namespace.commit();

		}
	}
	public void stop()
	{
		synchronized (this) {
			if(!active) return;
			active = false;
			output.remove(input);
			namespace.commit();			
			
		}
	}		

	NameSpace namespace;
	public MidiSwitchInstance(Parameters parameters)
	{
		namespace = parameters.getNameSpace();
		
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		control = parameters.getParameterWithDefault(1, "control");
		autostart = parameters.getParameter(2, "autostart");
		MidiSequence.getInstance(control).addReceiver(this);
		
		namespace.addToCommitStack(new Commitable()
				{	
			
					public int getRunLevel()
					{
						return RUNLEVEL_DEFAULT;
					}
			
					public void commit()
					{
						if(autostart != null)
							if((int)DoublePart.asDouble(autostart) == 1) 
							{
								if(!active)
								{
									active = true;
									output.add(input);
								}
							}
					}
				});
	}
	public void close() {
		stop();
		MidiSequence.getInstance(control).removeReceiver(this);		
	}
	public void send(MidiMessage arg0, long arg1) {
		if(arg0 instanceof ShortMessage)
		{
			ShortMessage sms = (ShortMessage)arg0;
			if(sms.getStatus() == ShortMessage.START) start();
			if(sms.getStatus() == ShortMessage.STOP) stop();
		}		
	}
	
}

public class MidiSwitch implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Midi Variable Switch Control");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_UNKNOWN, MetaData.DIRECTION_OUT);
		metadata.add(1, "control",		"Control",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(2, "autostart",	"Autostart",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"input",		null, null, MetaData.TYPE_UNKNOWN, MetaData.DIRECTION_IN);
		return metadata;		
	}	

	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiSwitchInstance(parameters);
	}
}
