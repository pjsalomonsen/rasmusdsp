/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Track;
import javax.sound.midi.Transmitter;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;

public class MidiSequence extends ListPart implements Receiver, Transmitter {
	
	public static int DEFAULT_RES = 480;
	
	private Sequence sequence;
	private Track track;
	
	public static MidiSequence getInstance(Variable variable)
	{
		return (MidiSequence)variable.get(MidiSequence.class);
	}		
	
	public static MetaMessage getTempoMessage(float tempo) throws Exception
	{			
		long tempoInMPQ = (long)(60000000.0 / tempo);
		byte[] data = new byte[3];
		data[0] = (byte)((tempoInMPQ >> 16) & 0xFF);
		data[1] = (byte)((tempoInMPQ >> 8) & 0xFF);
		data[2] = (byte)(tempoInMPQ & 0xFF);
		MetaMessage  msg = new MetaMessage ();	
		msg.setMessage(0x51, data, data.length);
		return msg;
	}
	
	
	public static void addEvent(Variable variable, MidiEvent event)
	{
		((MidiSequence)variable.get(MidiSequence.class)).addObject(event);
	}
	public static Sequence asSequence(Variable variable)
	{
		return ((MidiSequence)variable.get(MidiSequence.class)).asSequence();
	}
	public static Variable asVariable(Sequence seq)
	{
		Variable answer = new Variable();
				
		List events = new LinkedList();
		int sourceres = seq.getResolution();
		int targetres = DEFAULT_RES; 
		double resconv = ((double)targetres) / ((double)sourceres);
		Track[] tracks = seq.getTracks();
		for (int i = 0; i < tracks.length; i++) {
			Track track = tracks[i];
			int len = track.size();
			for (int j = 0; j < len; j++) {
				MidiEvent event = track.get(j);
				events.add(new MidiEvent(event.getMessage(), (long)(event.getTick()*resconv)));
			}
		}
		
		MidiSequence.getInstance(answer).addObjects(events);
		MidiSequence.getInstance(answer).setImmutable(true);
		return answer;
	}
	
	ListPartListener listlistener = new ListPartListener()
	{
		
		public void objectAdded(ListPart source, Object object) {
			if(resetMode) return;
			if(track != null)
				if(object instanceof MidiEvent) track.add((MidiEvent)object);
		}
		
		public void objectRemoved(ListPart source, Object object) {
			if(resetMode) return;
			if(track != null)
				if(object instanceof MidiEvent) track.remove((MidiEvent)object);
		}
		
		public void objectsAdded(ListPart source, List list) {
			if(resetMode) return;
			if(track != null)
			{
				Iterator iterator = list.iterator();
				while (iterator.hasNext()) {
					Object object = iterator.next();				 
					if(object instanceof MidiEvent) track.add((MidiEvent)object);			 
				}
			}
		}
		
		public void objectsRemoved(ListPart source, List list) {
			if(resetMode) return;
			if(track != null)
			{
				Iterator iterator = list.iterator();
				while (iterator.hasNext()) {
					Object object = iterator.next();	
					if(object instanceof MidiEvent) track.remove((MidiEvent)object);			 
				}		
			}	
		}
		
	};
	
	public MidiSequence()
	{
		//sequence = new Sequence(Sequence.PPQ, DEFAULT_RES);
		//track = sequence.createTrack();
		
		
		forceAddListener(listlistener);
		
		
	}
	
	public Sequence asSequence()
	{	
		if(!isImmutable())
			if(childvars.size() == 1)
			   return MidiSequence.asSequence((Variable)(childvars.get(0)));
		
		if(sequence != null) return sequence;
		//System.out.println("createSequence");
		try
		{
			sequence = new Sequence(Sequence.PPQ, DEFAULT_RES);
			track = sequence.createTrack();
			
			Iterator iterator = getObjects().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();	
				if(object instanceof MidiEvent) track.add((MidiEvent)object);			 
			}		
			return sequence;
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return null;
	}
	
	
	/*
	 public void addObject(Object object)
	 {			
	 if(RInterpreter.resetMode) return;
	 if(track != null)
	 if(object instanceof MidiEvent) track.add((MidiEvent)object);
	 super.addObject(object);
	 }
	 public void removeObject(Object object)
	 {
	 if(RInterpreter.resetMode) return;
	 if(track != null)
	 if(object instanceof MidiEvent) track.remove((MidiEvent)object);
	 super.removeObject(object);
	 }	
	 public void addObjects(List list)
	 {
	 if(RInterpreter.resetMode) return;
	 if(track != null)
	 {
	 Iterator iterator = list.iterator();
	 while (iterator.hasNext()) {
	 Object object = iterator.next();				 
	 if(object instanceof MidiEvent) track.add((MidiEvent)object);			 
	 }
	 }
	 super.addObjects(list);
	 }
	 public void removeObjects(List list)
	 {
	 if(RInterpreter.resetMode) return;
	 if(track != null)
	 {
	 Iterator iterator = list.iterator();
	 while (iterator.hasNext()) {
	 Object object = iterator.next();	
	 if(object instanceof MidiEvent) track.remove((MidiEvent)object);			 
	 }		
	 }
	 super.removeObjects(list);
	 } */
	
	private ArrayList childvars = new ArrayList();
	public void add(Variable variable)
	{		
		childvars.add(variable);
		MidiSequence.getInstance(variable).addReceiver(this);
		super.add(variable);
	}
	public void remove(Variable variable)	
	{
		childvars.add(variable);
		MidiSequence.getInstance(variable).removeReceiver(this);
		super.remove(variable);
	}
	
	public void addListener(ListPartListener listener)
	{
		if(listener instanceof Receiver) addReceiver((Receiver)listener);
		super.addListener(listener);
	}
	public void removeListener(ListPartListener listener)
	{
		if(listener instanceof Receiver) removeReceiver((Receiver)listener);
		super.removeListener(listener);
	}		        
	
	
	public void send(MidiMessage message, long timeStamp) {
		Receiver[] receiverlist = this.receiverlist;
		if(receiverlist != null)
			for (int i = 0; i < receiverlist.length; i++) 
				receiverlist[i].send(message, timeStamp);
	}
	
	public ArrayList receivers = new ArrayList();
	public volatile Receiver[] receiverlist = null;
	
	public void addReceiver(Receiver receiver)
	{
		synchronized(receivers)
		{
			receivers.add(receiver);
			Receiver[] receiverlist = new Receiver[receivers.size()];
			receivers.toArray(receiverlist); 
			this.receiverlist = receiverlist;
		}
	}
	public void removeReceiver(Receiver receiver)
	{
		synchronized(receivers)
		{
			receivers.remove(receiver);
			Receiver[] receiverlist = new Receiver[receivers.size()];
			receivers.toArray(receiverlist); 
			this.receiverlist = receiverlist;			
		}
	}    
	
	public void close() {
	}

	
	Receiver singlereceiver = null;
	public void setReceiver(Receiver receiver) {
		if(singlereceiver != null)
			removeReceiver(singlereceiver);
		singlereceiver = receiver;
		if(singlereceiver != null)
			addReceiver(singlereceiver);
	}

	public Receiver getReceiver() {
		return singlereceiver;
	}			
	
}
