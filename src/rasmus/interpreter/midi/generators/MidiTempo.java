/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.generators;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiTempoInstance extends UnitInstanceAdapter
{
	Variable output;
	Variable answer = null;
	
	Variable bpm;
	Variable pos;
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			answer = null;
		}
	}
	public void calc()
	{
		clear();
		
		double bpm_val = DoublePart.asDouble(bpm);
		if(bpm_val < 0.0000001) return;

		long tick = (long)(MidiSequence.DEFAULT_RES*DoublePart.asDouble(pos));

		answer = new Variable();
		
		MidiSequence midiseq = MidiSequence.getInstance(answer);
		midiseq.setImmutable(true);
				
		try {
			MetaMessage mmsg = new MetaMessage();
			
			int tempoInMPQ = (int)(60000000.0f / bpm_val);
			
			byte[] data = new byte[3];
			data[0] = (byte)((tempoInMPQ >> 16) & 0xFF);
			data[1] = (byte)((tempoInMPQ >> 8) & 0xFF);
			data[2] = (byte)(tempoInMPQ & 0xFF);
			
			mmsg.setMessage(0x51, data, data.length);
			
			MidiEvent midievent = new MidiEvent(mmsg, tick);
			midiseq.addObject(midievent);		
			output.add(answer);
			
		} catch (InvalidMidiDataException e) {
			System.out.println(e);
		}
		
		
	}
	public MidiTempoInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		bpm = parameters.getParameterWithDefault(1, "bpm");
		pos = parameters.getParameterWithDefault(2, "pos");
		DoublePart.getInstance(bpm).addListener(this);
		DoublePart.getInstance(pos).addListener(this);
		
		calc();
	}
	public void close() {
		
		DoublePart.getInstance(bpm).removeListener(this);
		DoublePart.getInstance(pos).removeListener(this);
		clear();
	}
	
}

public class MidiTempo implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Tempo Message");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "bpm",			"Tempo",	null, "bpm", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "pos",			"Position",	null, "beats", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiTempoInstance(parameters);
	}
}
