/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.generators;

import java.util.ArrayList;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.SysexMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiLongMessageInstance extends UnitInstanceAdapter
{
	Variable output;
	Variable answer = null;
	
	Variable[] data;
	Variable pos;
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			answer = null;
		}
	}
	public void calc()
	{
		clear();
		answer = new Variable();
		
		MidiSequence midiseq = MidiSequence.getInstance(answer);
		midiseq.setImmutable(true);
		
		byte[] b_data = new byte[data.length];
		for (int i = 0; i < b_data.length; i++) {
			int i_data = (int)(DoublePart.asDouble(data[i])); 
			b_data[i] = (byte)i_data;
		}
		
		SysexMessage smsg = new SysexMessage();
		
		try {
			smsg.setMessage(b_data, b_data.length);
		} catch (InvalidMidiDataException e) {
			System.out.println(e);
		}
		long tick = (long)(MidiSequence.DEFAULT_RES*DoublePart.asDouble(pos)); 
		MidiEvent midievent = new MidiEvent(smsg, tick);
		midiseq.addObject(midievent);
		
		
		output.add(answer);
	}
	public MidiLongMessageInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		
		ArrayList datalist = new ArrayList();
		Object dataitem = parameters.getParameter(1);
		int pi = 2;
		while(dataitem != null)
		{
			datalist.add(dataitem);
			dataitem = parameters.getParameter(pi);
			pi++;
		}
		
		data = new Variable[datalist.size()];
		for (int j = 0; j < data.length; j++) {
			data[j] = (Variable)datalist.get(j);
		}
		
		pos = parameters.getParameterWithDefault("pos");
		
		for (int i = 0; i < data.length; i++) {
			DoublePart.getInstance(data[i]).addListener(this);			
		}
		DoublePart.getInstance(pos).addListener(this);
		
		
		calc();
	}
	public void close() {
		
		for (int i = 0; i < data.length; i++) {
			DoublePart.getInstance(data[i]).removeListener(this);
		}
		DoublePart.getInstance(pos).removeListener(this);
		
		clear();
	}
	
}

public class MidiLongMessage implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Long Message");
		metadata.add(-1, "output",		"output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.setHasVarargs(true);
		return metadata;		
	}	

	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiLongMessageInstance(parameters);
	}
}
