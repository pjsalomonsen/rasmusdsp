/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.generators;

import java.util.Iterator;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;
import rasmus.util.ABCTokenizer;

class MidiABCNotationInstance extends UnitInstanceAdapter implements Commitable
{
	Variable output;
	Variable answer = null;
	
	Variable input;
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			answer = null;
		}
	}
	
	public void calc()
	{
		namespace.addToCommitStack(this);
	}
	
	String midiNoteSymbols = "C D EF G A Bc d ef g a b";
	public void parse(MidiSequence midiseq, ABCTokenizer.ABCToken token)
	{
		  
		int midinote = midiNoteSymbols.indexOf(token.symbol);
		if(midinote != -1)
		{
			midinote += token.octave*12;
			midinote += token.accidentals;
			
			long tick = (long)(MidiSequence.DEFAULT_RES*token.pos);
			long endtick = (long)(MidiSequence.DEFAULT_RES*(token.pos+token.len));
						
			int i_ch = token.channel;
			if(i_ch < 1) i_ch = 1;
			if(i_ch > 16) i_ch = 16;
			int i_note = midinote;
			int i_vel = token.velocity;
			
			try {
				ShortMessage smsg = new ShortMessage();
				smsg.setMessage(ShortMessage.NOTE_ON, i_ch-1, i_note, i_vel);
				MidiEvent midievent = new MidiEvent(smsg, tick);
				midiseq.addObject(midievent);				
			} catch (InvalidMidiDataException e) {
				System.out.println(e);
			}
			
			try {
				ShortMessage smsg = new ShortMessage();
				smsg.setMessage(ShortMessage.NOTE_OFF, i_ch-1, i_note, 0);
				MidiEvent midievent = new MidiEvent(smsg, endtick);
				midiseq.addObject(midievent);				
			} catch (InvalidMidiDataException e) {
				System.out.println(e);
			}			
		}
	}
	
	int parse_function_type = 0;
	int parse_function_subtype = 0;
	
	public void parse(MidiSequence midiseq, ABCTokenizer.CommandToken token) 
	{
		int v;
		ShortMessage smsg;
		MidiEvent midievent;
		
		long tick = (long)(MidiSequence.DEFAULT_RES*token.pos);
		int i_ch = token.channel;
		
		if(token.fieldname.equals("f"))
		{
			if(token.fieldvalue.equalsIgnoreCase("p"))
				parse_function_type = 1;
			else
			if(token.fieldvalue.length() > 0 && Character.isDigit(token.fieldvalue.charAt(0)))
			{
				try
				{
					parse_function_type = 2;
					parse_function_subtype = Integer.parseInt(token.fieldvalue);
				}
				catch(Throwable t) {}
			}
		}
		else
		if(token.fieldname.equals("d"))
		{
			try
			{	
			switch (parse_function_type) {
			case 1: // PitchBend
				double d_v = Double.parseDouble(token.fieldvalue);
				v = (int) ((d_v + 100.0) * 8192.0);
				if(v > 128*127) v = 128*127;
				if(v < 0) v = 0;
				smsg = new ShortMessage();
				smsg.setMessage(ShortMessage.PITCH_BEND, i_ch-1, v % 0x80, v / 0x80);
				midievent = new MidiEvent(smsg, tick);
				midiseq.addObject(midievent);				
				
				break;

			case 2: // Midi Control
				v = Integer.parseInt(token.fieldvalue);
				if(v > 127) v = 127;
				if(v < 0) v = 0;
				smsg = new ShortMessage();
				smsg.setMessage(ShortMessage.CONTROL_CHANGE, i_ch-1, parse_function_subtype, v);
				midievent = new MidiEvent(smsg, tick);
				midiseq.addObject(midievent);				
				
				break;				
			default:					    
				break;
			}
			}
			catch(Throwable t) { t.printStackTrace(); }			
		}
		else
		if(token.fieldname.equals("i"))
		{
			try
			{	
							
		    String program = token.fieldvalue;
		    String bank = null;
		    int li = token.fieldvalue.indexOf(",");
		    if(li != -1)
		    {
		    	bank = program.substring(li+1);
		    	program = program.substring(0, li);
		    }
		    
		    if(bank != null)
		    {
		    	int i_bank = Integer.parseInt(bank);
		    	if(i_bank < 0) i_bank = 0;
		    	if(i_bank > 127*128) i_bank = 128*127;
		    	
		    	int m_bank = i_bank / 128;
		    	//if(m_bank != 0)
		    	{
					smsg = new ShortMessage();
					smsg.setMessage(ShortMessage.CONTROL_CHANGE, i_ch-1, 0, m_bank);
					midievent = new MidiEvent(smsg, tick);
					midiseq.addObject(midievent);						    		
		    	}

		    	int l_bank = i_bank % 128;
		    	//if(l_bank != 0)
		    	{
					smsg = new ShortMessage();
					smsg.setMessage(ShortMessage.CONTROL_CHANGE, i_ch-1, 32, l_bank);
					midievent = new MidiEvent(smsg, tick);
					midiseq.addObject(midievent);						    		
		    	}
		    	
		    }
		    int i_program = Integer.parseInt(program);
			smsg = new ShortMessage();
			smsg.setMessage(ShortMessage.PROGRAM_CHANGE, i_ch-1, i_program, 0);
			midievent = new MidiEvent(smsg, tick);
			midiseq.addObject(midievent);		
			
			}
			catch(Throwable t) { t.printStackTrace(); }				
		}		
	}
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit()
	{
		clear();
		answer = new Variable();
		
		ABCTokenizer abcparser = new ABCTokenizer();
		List tokens = abcparser.parse(ObjectsPart.toString(input));
		
		MidiSequence midiseq = MidiSequence.getInstance(answer);
		midiseq.setImmutable(true);
		
		Iterator iterator = tokens.iterator();
		while (iterator.hasNext()) {
			Object token = iterator.next();
			if(token instanceof ABCTokenizer.ABCToken)
				parse(midiseq, (ABCTokenizer.ABCToken)token);
			else if(token instanceof ABCTokenizer.CommandToken)
				parse(midiseq, (ABCTokenizer.CommandToken)token);
		}
		
		output.add(answer);
	}
	
	NameSpace namespace;
	
	public MidiABCNotationInstance(Parameters parameters)
	{		
		namespace = parameters.getNameSpace();
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault(1, "input");
		ObjectsPart.getInstance(input).addListener(this);
		
		calc();
		
	}
	public void close() {
		
		ObjectsPart.getInstance(input).removeListener(this);
		clear();
	}
	
}

public class MidiABCNotation implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("ABC Notation");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "input",		"input",	null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		return metadata;		
	}	

	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiABCNotationInstance(parameters);
	}
}
