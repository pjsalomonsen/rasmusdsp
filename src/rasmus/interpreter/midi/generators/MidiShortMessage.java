/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.generators;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiShortMessageInstance extends UnitInstanceAdapter
{
	Variable output;
	Variable answer = null;
	
	Variable command;
	Variable data1;
	Variable data2;
	Variable ch;
	Variable pos;
	
	Variable todata1;
	Variable todata2;
	Variable len;	
	Variable res;
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			answer = null;
		}
	}
	public void calc()
	{
		clear();
		answer = new Variable();
		
		MidiSequence midiseq = MidiSequence.getInstance(answer);
		midiseq.setImmutable(true);
		
		int i_ch = (int)(DoublePart.asDouble(ch)); 
		int i_command = (int)(DoublePart.asDouble(command)); 
		double f_data1 = (DoublePart.asDouble(data1)); 
		double f_data2 = (DoublePart.asDouble(data2)); 
		int i_data1 = (int)(DoublePart.asDouble(data1)); 
		int i_data2 = (int)(DoublePart.asDouble(data2)); 
		
		if(i_ch != 0) i_ch--;
		
		int i_len = (int)(MidiSequence.DEFAULT_RES*DoublePart.asDouble(len));
		if(i_len != 0)
		{
			int i_res = (int)(MidiSequence.DEFAULT_RES*DoublePart.asDouble(res)); 
			if(i_res == 0) i_res = MidiSequence.DEFAULT_RES/10;
			long tick = (long)(MidiSequence.DEFAULT_RES*DoublePart.asDouble(pos));
			
			int count = i_len / i_res;
			double fcount = count;
			
			double i_to_data1 = ((DoublePart.asDouble(todata1)) - f_data1)/fcount; 
			double i_to_data2 = ((DoublePart.asDouble(todata2)) - f_data2)/fcount; 		
			
			for (int i = 0; i <= count; i++) {
				
				
				f_data1 += i_to_data1;
				f_data2 += i_to_data2;
				i_data1 = (int)f_data1; 
				i_data2 = (int)f_data2; 						  	  	
				
				ShortMessage smsg = new ShortMessage();
				try {
					if(i_command >= 0xF0)
						smsg.setMessage(i_command);
					else
						smsg.setMessage(i_command, i_ch, i_data1, i_data2);
				} catch (InvalidMidiDataException e) {
					System.out.println(e);
				}
				MidiEvent midievent = new MidiEvent(smsg, tick);
				midiseq.addObject(midievent);
				
				tick += i_res;
			}
		}
		else
		{
			
			ShortMessage smsg = new ShortMessage();
			try {
				if(i_command >= 0xF0)
					smsg.setMessage(i_command);
				else				
					smsg.setMessage(i_command, i_ch, i_data1, i_data2);
			} catch (InvalidMidiDataException e) {
				System.out.println(e);
			}
			long tick = (long)(MidiSequence.DEFAULT_RES*DoublePart.asDouble(pos)); 
			MidiEvent midievent = new MidiEvent(smsg, tick);
			midiseq.addObject(midievent);
		}
		
		output.add(answer);
	}
	public MidiShortMessageInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		command = parameters.getParameterWithDefault(1, "command");
		ch = parameters.getParameterWithDefault(2, "channel");
		data1 = parameters.getParameterWithDefault(3, "data1");
		data2 = parameters.getParameterWithDefault(4, "data2");
		todata1 = parameters.getParameterWithDefault(5, "todata1");
		todata2 = parameters.getParameterWithDefault(6, "todata2");
		len = parameters.getParameterWithDefault(7, "len");
		res = parameters.getParameterWithDefault(8, "res");
		pos = parameters.getParameterWithDefault(9, "pos");
		DoublePart.getInstance(command).addListener(this);
		DoublePart.getInstance(data1).addListener(this);
		DoublePart.getInstance(data2).addListener(this);
		DoublePart.getInstance(ch).addListener(this);
		DoublePart.getInstance(pos).addListener(this);
		DoublePart.getInstance(todata1).addListener(this);
		DoublePart.getInstance(todata2).addListener(this);
		DoublePart.getInstance(len).addListener(this);
		DoublePart.getInstance(res).addListener(this);
		
		calc();
	}
	public void close() {
		
		DoublePart.getInstance(command).removeListener(this);
		DoublePart.getInstance(data1).removeListener(this);
		DoublePart.getInstance(data2).removeListener(this);
		DoublePart.getInstance(ch).removeListener(this);
		DoublePart.getInstance(pos).removeListener(this);
		DoublePart.getInstance(todata1).removeListener(this);
		DoublePart.getInstance(todata2).removeListener(this);
		DoublePart.getInstance(len).removeListener(this);
		DoublePart.getInstance(res).removeListener(this);
		
		clear();
	}
	
}

public class MidiShortMessage implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Short Message");
		metadata.add(-1, "output",		"Output",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "command",		"Command",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "channel",		"Channel",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "data1",		"Data1",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "data2",		"Data2",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(5, "todata1",		"Fade to data1",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(6, "todata2",		"Fade to data2",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(7, "len",			"Fade length",	    null, "beats", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(8, "res",			"Fade resolution",	"0.1", "beats", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(9, "pos",			"Positions",	null, "beats", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		return metadata;		
	}	

	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiShortMessageInstance(parameters);
	}
}
