/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

public class MidiKeyListener implements KeyListener {
	
	private List<Integer> keyinside = new LinkedList<Integer>();
	private Receiver recv;
	
	private int currentvelocity = 100;
	private int currentoctave = 3;
	public int getVelocity()
	{
		return currentvelocity;
	}
	public int getOctave()
	{
		return currentoctave;
	}
	public void setVelocity(int value)
	{
		currentvelocity = value;
	}
	public void setOctave(int value)
	{
		currentoctave = value;
	}
	
	public boolean isConsumed(KeyEvent e)
	{
		return !(keyCodeToNote(e) == 0);		
	}
	
	private  int keyCodeToNote(KeyEvent e) {
		
		return keyCodeToNote(currentoctave , e);
	}	
	
	private int keyCodeToNote(int currentoctave, KeyEvent e) {
		int keycode = e.getKeyCode();
		int note;		
		if(keycode == KeyEvent.VK_UNDEFINED)
		{
			char keychar = e.getKeyChar();
			switch (keychar) {
			case (char)230: // Ae 
				note = 15;
				break;
			case (char)198: // Ae (CAP)
				note = 15;
				break;				
			case (char)254: // Thorn 
				note = 16;
				break;
			case (char)222: // Thron (CAP)
				note = 16;
				break;
			case (char)240:  // Eth
				note = 29;
				break;				
			case (char)208:  // Eth (CAP)
				note = 29;
				break;				
			default :
				return 0;
			}			
		}
		else		
			switch (keycode) {
			/*
			 49 = 81 (-1)
			 52 = 69 (+1)
			 56 = 85 (+1)
			 65 = 90 (-1)
			 
			 70 = 67 (+1)
			 75 = 77 (+1)*/
			
			case 49 : //EXT
				note = 11;
				break;		
				
			case 81 :
				note = 12;
				break;
			case 50 :
				note = 13;
				break;
			case 87 :
				note = 14;
				break;
			case 51 :
				note = 15;
				break;
				
			case 52 : //EXT
				note = 17;
				break;
				
			case 69 :
				note = 16;
				break;
			case 82 :
				note = 17;
				break;
			case 53 :
				note = 18;
				break;
			case 84 :
				note = 19;
				break;
			case 54 :
				note = 20;
				break;
			case 89 :
				note = 21;
				break;
			case 55 :
				note = 22;
				break;
				
			case 56 : //EXT
				note = 24;
				break;
				
			case 85 :
				note = 23;
				break;
			case 73 :
				note = 24;
				break;
			case 57 :
				note = 25;
				break;
			case 79 :
				note = 26;
				break;
			case 48 :
				note = 27;
				break;
			case 80 :
				note = 28;
				break;
				
			case 45 :
				note = 30;
				break;
			case 222 :
				note = 31;
				break;
				
				
			case 70 : //EXT
				note = -1;
				break;				
				
			case 153 :
				note = -1;
				break;
			case 90 :
				note = 0;
				break;
			case 83 :
				note = 1;
				break;
			case 88 :
				note = 2;
				break;
			case 68 :
				note = 3;
				break;
				
			case 65 : //EXT
				note = 5;
				break;						
				
			case 67 :
				note = 4;
				break;
			case 86 :
				note = 5;
				break;
			case 71 :
				note = 6;
				break;
			case 66 :
				note = 7;
				break;
			case 72 :
				note = 8;
				break;
			case 78 :
				note = 9;
				break;
			case 74 :
				note = 10;
				break;
				
			case 75 : //EXT
				note = 12;
				break;				
				
			case 77 :
				note = 11;
				break;
			case 44 :
				note = 12;
				break;
			case 76 :
				note = 13;
				break;
			case 46 :
				note = 14;
				break;
				
			default :
				return 0;
			}
		
		
		note += currentoctave * 12;
		return note;
		
	}
	
	
	private LinkedList<Integer> noteinside = new LinkedList<Integer>();
	
	public void allNotesOff()
	{ 
		Iterator iteratr = noteinside.iterator();
		while (iteratr.hasNext()) {
			Integer pnote = (Integer) iteratr.next();
			
			ShortMessage ssmg = new ShortMessage();
			try {
				ssmg.setMessage(ShortMessage.NOTE_ON, 0, pnote.intValue(), 0);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}		
			if(recv != null)
				recv.send(ssmg,0 );			
			
		}
		
		noteinside.clear();
		keyinside.clear();
	}
	
	public MidiKeyListener() {
	}
	
	public void setReceiver(Receiver recv)
	{
		this.recv = recv;
	}
	
	public void keyPressed(KeyEvent e) {
		
		if(e.getKeyCode() == KeyEvent.VK_F10) // Garbage Collect
		{
			System.gc();
			System.runFinalization();
		}	
		if(e.getKeyCode() == KeyEvent.VK_F11) // Reset All Controllers
		{
			ShortMessage ssmg = new ShortMessage();
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x79, 0);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}		
			if(recv != null)
				recv.send(ssmg,0 );
			
		}		
		if(e.getKeyCode() == KeyEvent.VK_F12) // All Sound Off
		{
			ShortMessage ssmg = new ShortMessage();
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x78, 0);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}		
			if(recv != null)
				recv.send(ssmg,0 );
			
		}
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) // All Notes Off
		{
			ShortMessage ssmg = new ShortMessage();
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x7B, 0);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}		
			if(recv != null)
				recv.send(ssmg,0 );

		}
		
		if(e.getKeyChar() == '+')
		{
			ShortMessage ssmg = new ShortMessage();
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x40, 64);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}		
			if(recv != null)
				recv.send(ssmg,0 );
		}
		if(e.getKeyChar() == '-')
		{
			ShortMessage ssmg = new ShortMessage();
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x40, 63);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}		
			if(recv != null)
				recv.send(ssmg,0 );
		}		
		if(e.getKeyChar() == '*')
		{
			ShortMessage ssmg = new ShortMessage();
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x42, 64);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}		
			if(recv != null)
				recv.send(ssmg,0 );
		}
		if(e.getKeyChar() == '/')
		{
			ShortMessage ssmg = new ShortMessage();
			try {
				ssmg.setMessage(ShortMessage.CONTROL_CHANGE, 0, 0x42, 63);
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}		
			if(recv != null)
				recv.send(ssmg,0 );
		}				
		
		int note = keyCodeToNote(e);
		if(note == 0) return;
		e.consume();
		Integer inote = new Integer(note);
		
		if (!keyinside.contains(inote)) {
			keyinside.add(inote);
			ShortMessage ssmg = new ShortMessage();
			try {
				noteinside.add(inote);
				ssmg.setMessage(ShortMessage.NOTE_ON, 0, note, getVelocity());
			} catch (InvalidMidiDataException e1) {
				e1.printStackTrace();
			}		
			if(recv != null)
				recv.send(ssmg,0 );
		}
		
	}
	
	public void keyReleased(KeyEvent e) {
		
		int note = keyCodeToNote(e);
		if(note == 0) return;
		e.consume();
		
		keyinside.remove(new Integer(note));
		
		ShortMessage ssmg = new ShortMessage();
		try {
			noteinside.remove(new Integer(note));
			ssmg.setMessage(ShortMessage.NOTE_ON, 0, note, 0);
		} catch (InvalidMidiDataException e1) {
			e1.printStackTrace();
		}							
		if(recv != null)
			recv.send(ssmg,0 );
		
	}
	public void keyTyped(KeyEvent e) {
		int note = keyCodeToNote(e);
		if(note == 0) return;
		e.consume();
		
	}
	
}

