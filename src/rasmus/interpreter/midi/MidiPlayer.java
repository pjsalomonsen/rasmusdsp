/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiPlayerInstance implements UnitInstancePart
{
	
	static ShortMessage tickmessage = new ShortMessage();
	static {
		try {
			tickmessage.setMessage(0xF9);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
	}
	static ShortMessage stopmessage = new ShortMessage();
	static {
		try {
			stopmessage.setMessage(ShortMessage.STOP);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
	}
	static ShortMessage startmessage = new ShortMessage();
	static {
		try {
			startmessage.setMessage(ShortMessage.START);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
	}
	
	Variable output;
	Variable input;	
	Variable controlin;
	Variable controlout;
	Variable sync;
	
	Receiver syncout;
	
	Receiver recv = new Receiver()
	{
		public void send(MidiMessage arg0, long arg1) {
			if(arg0 instanceof ShortMessage)
			{
				ShortMessage sms = (ShortMessage)arg0;
				if(sms.getStatus() == ShortMessage.START) start();
				if(sms.getStatus() == ShortMessage.STOP) stop();
				if(sms.getStatus() == ShortMessage.TIMING_CLOCK) clock();
			}
		}
		public void close() {
		}
	};
	
	boolean playing = false;
	
	Sequencer sequencer = null;
	
	public Sequencer getSequencer() throws Exception
	{
		
		String midiseqname = "Real Time Sequencer";
		
		MidiDevice.Info devinfo[] = MidiSystem.getMidiDeviceInfo();
		for (int i = 0; i < devinfo.length; i++) {
			if(devinfo[i].getName().equals(midiseqname))
			{
				try {
					MidiDevice mdev = MidiSystem.getMidiDevice(devinfo[i]);
					if(mdev instanceof Sequencer) return (Sequencer)mdev;
				} catch (MidiUnavailableException e) {
					e.printStackTrace();
				}  		
			}
		}
		
		return MidiSystem.getSequencer(false);
	}	

	
	boolean start_on_clock = false;
	public void start()
	{
		synchronized (this) {
			if(!playing)
			{
				int i_sync = (int)DoublePart.asDouble(sync);
				if(i_sync == 0) startPlayBack();
				if(i_sync == 1) start_on_clock  = true;
			}
		}
	}
	
	public void startPlayBack()
	{
		stop();
		synchronized (this) {
			if(!playing)
			{				
				start_on_clock  = false;
				
				playing = true;
				
				try {
					sequencer = getSequencer();
					sequencer.getTransmitter().setReceiver(MidiSequence.getInstance(output));
					sequencer.setSequence(MidiSequence.asSequence(input));
					sequencer.open();
					
				} catch (Exception e) {
					if(sequencer != null)
					{
						sequencer.close();
						sequencer = null;
					}
					e.printStackTrace();
				}
				
				if(sequencer != null)
				{
					sequencer.start();
					if(syncout != null)
					{
						syncout.send(startmessage, -1);
					}					
					new Thread(new SeqWatcher(sequencer)).start();
				}				
			}
		}
	}
	
	private class SeqWatcher implements Runnable
	{
		Sequencer sequencer;
		public SeqWatcher(Sequencer sequencer)
		{			
			this.sequencer = sequencer;
		}
		public void run() {
			
			if(syncout != null)
			{
				syncout.send(tickmessage, -1);
			}						
			
			long lastms = 0;
			while(sequencer.isRunning())
			{
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {					
					e.printStackTrace();
					break;
				}
				long ms = sequencer.getMicrosecondPosition();				
				while(ms - lastms >= 10000)
				{
					lastms += 10000;
					syncout.send(tickmessage, -1);
					
				}
			}
			
			
			if(syncout != null)
			{
				syncout.send(stopmessage, -1);
			}								
		}
	}
	
	public void stop()
	{
		synchronized (this) {
			start_on_clock  = false;
			if(playing)
			{				
				if(sequencer != null)
				{
					sequencer.stop();
					sequencer.close();
					sequencer = null;		
				}
				
				playing = false;
			}			
		}		
	}	
	
	public void clock()
	{
		boolean start_on_clock;
		synchronized (this) {
			start_on_clock = this.start_on_clock;
		}
		if(start_on_clock) startPlayBack();
	}
	
	public MidiPlayerInstance(Parameters parameters)
	{
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		controlin = parameters.getParameterWithDefault(1, "controlin");
		controlout = parameters.getParameterWithDefault(2, "controlout");
		sync = parameters.getParameterWithDefault(3, "sync");
		Variable syncout = parameters.getParameter("syncout");
		if(syncout != null)
		{
			this.syncout = MidiSequence.getInstance(syncout);
		}
		
		MidiSequence.getInstance(controlin).addReceiver(recv);		
	}
	public void close() {
		stop();
		MidiSequence.getInstance(controlin).removeReceiver(recv);		
	}
	
}

public class MidiPlayer implements UnitFactory {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("MIDI Player");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_OUT);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_AUDIO, MetaData.DIRECTION_IN);
		
		metadata.add(1, "controlin",	"controlin",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(2, "controlout",	"controlout",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(3, "sync",			"sync",			"0",  null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		// Sync Mode
		//  0 = no synching
		//  1 = sync on first clock message
		//  2 = slave on clock messages (currently not supported)
		
		return metadata;		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiPlayerInstance(parameters);
	}
}
