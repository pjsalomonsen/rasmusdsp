/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.io;

import java.io.File;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.io.Resource;
import rasmus.interpreter.io.ResourceManager;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiFileInstance extends UnitInstanceAdapter
{
	Variable returnvar;
	Variable filename;
	Variable wrkdir;
	Variable answer = null;
	
	Resource resource = null;	
	
	public void clear()
	{
		if(answer != null)
		{
			returnvar.remove(answer);
			answer = null;
		}		
		if(resource != null) resource.close();
		resource = null;
		
	}
	
	public String expandPath(String path)
	{		
		path = path.trim();
		if(path.length() == 0) return path;
		if(path.startsWith(File.separator)) return path;		
		if(path.length() > 2 && Character.isLetter(path.charAt(0)) && path.charAt(1) == ':' && path.charAt(2) == File.separatorChar)
				return path;
	
		String wrkdir = ObjectsPart.toString(this.wrkdir);
		if(wrkdir.trim().length() == 0)
			return path;					
		
		if(wrkdir.endsWith(File.separator))
		{
			return wrkdir + path;
		}
		else
		{
			return wrkdir + File.separator + path;
		}
	}
	
	public void calc()
	{
		clear();
		
		String filename = ObjectsPart.toString(this.filename);
		if(!filename.toLowerCase().endsWith(".mid")) return;
		resource = manager.getResource(filename);
		if(resource == null) return;
		File file = resource.getFile();		
		
		try
		{
			Sequence seq = MidiSystem.getSequence(file);
			answer = MidiSequence.asVariable(seq);
			returnvar.add(answer);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}				
		
		
	}
	
	NameSpace namespace;	
	ResourceManager manager;	
	
	public MidiFileInstance(Parameters parameters)
	{
		
		namespace = parameters.getNameSpace();
		manager = ResourceManager.getInstance(namespace);
		
		returnvar = parameters.getParameterWithDefault(0, "output");
		filename = parameters.getParameterWithDefault(1, "filename");
		wrkdir = parameters.get("wrkdir");
		ObjectsPart.getInstance(filename).addListener(this);
		ObjectsPart.getInstance(wrkdir).addListener(this);
		
		calc();
	}
	public void close() {
		ObjectsPart.getInstance(filename).removeListener(this);
		ObjectsPart.getInstance(wrkdir).removeListener(this);
		clear();
	}
	
}

public class MidiFile implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Midi File");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "filename",		"Filename",		null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiFileInstance(parameters);
	}
}
