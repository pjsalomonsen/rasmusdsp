/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.io;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Receiver;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiInputInstance extends UnitInstanceAdapter 
{
	Receiver output;
	Variable devname;
	MidiDevice midiindev = null;
	
	public void clear()
	{
		if(midiindev != null)
		{
			midiindev.close();
			midiindev = null;
		}
	}
	
	public void calc()
	{
		clear();                             		
		String s_devname = ObjectsPart.toString(devname);
		if(s_devname.length() == 0) return;
		
		
		MidiDevice.Info devinfo[] = MidiSystem.getMidiDeviceInfo();
		MidiDevice.Info midimapdev = null;
		for (int i = 0; i < devinfo.length; i++) {
			if(devinfo[i].getName().equals(s_devname))
			{
				midimapdev = devinfo[i];		 		  		
				try {
					midiindev = MidiSystem.getMidiDevice(midimapdev);
					midiindev.open();
					midiindev.getTransmitter().setReceiver(output);
					return;
				} catch (Exception e) {
					try
					{
						midiindev.close();
					}
					catch(Exception e2)
					{					
					}
				}		  		
			}
		}
			
	}
	public MidiInputInstance(Parameters parameters)
	{
		Variable output = parameters.getParameterWithDefault(0, "output");
		devname = parameters.getParameterWithDefault(1, "devname");
		
		this.output = MidiSequence.getInstance(output);
		
		ObjectsPart.getInstance(devname).addListener(this);
		
		calc();
	}
	public void close() {
		ObjectsPart.getInstance(devname).removeListener(this);
		clear();
	}
	
}

public class MidiInput implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Midi Input");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		Map map = metadata.add(1, "devname",		"Device name",	null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		
		List options = new ArrayList();
		map.put("options", options);
		MidiDevice.Info devinfo[] = MidiSystem.getMidiDeviceInfo();
		for (int i = 0; i < devinfo.length; i++) {
			try
			{
				MidiDevice mididev = MidiSystem.getMidiDevice(devinfo[i]);
				if(mididev.getTransmitter() != null)
					options.add("\"" + devinfo[i].getName() + "\"");
			}			
			catch(Exception e)
			{
			}				  
		}				
		return metadata;		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiInputInstance(parameters);
	}
}
