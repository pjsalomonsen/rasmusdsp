/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.io;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiOutputInstance extends UnitInstanceAdapter implements Receiver
{
	Variable input;
	Variable devname;
	//List answers = new ArrayList();
	
	MidiDevice mididev = null;
	Receiver recv = null;
	
	public void clear()
	{
		if(recv != null)
		{
			recv.close();
			recv = null;
		}
		if(mididev != null)
		{
			mididev.close();
			mididev = null;
		}
	}
	
	public void calc()
	{
		clear();                             		
		String s_devname = ObjectsPart.toString(devname);
		//if(s_devname.length() == 0) s_devname = "Microsoft MIDI Mapper";
		
		if(s_devname.length() != 0)
		{
		MidiDevice.Info devinfo[] = MidiSystem.getMidiDeviceInfo();
		MidiDevice.Info midimapdev = null;
		for (int i = 0; i < devinfo.length; i++) {
			if(devinfo[i].getName().equals(s_devname))
			{
				midimapdev = devinfo[i];
				
				try {
					mididev = MidiSystem.getMidiDevice(midimapdev);
					mididev.open();			  	
					recv = mididev.getReceiver();
					return;
				} catch (MidiUnavailableException e) {
					e.printStackTrace();
					try
					{
						mididev.close();
					}
					catch(Exception e2)
					{									
					}
				}
				
				
			}
		}
		}
		
	}
	
	public MidiOutputInstance(Parameters parameters)
	{
		input = parameters.getParameterWithDefault("input");
		devname = parameters.getParameterWithDefault(1, "devname");
		
		MidiSequence.getInstance(input).addReceiver(this);
		
		ObjectsPart.getInstance(devname).addListener(this);
		
		calc();
	}
	
	public void close() {
		ObjectsPart.getInstance(devname).removeListener(this);
		MidiSequence.getInstance(input).removeReceiver(this);
		clear();
	}
	
	public void send(MidiMessage arg0, long arg1) {
		if(recv != null)
		{
			recv.send(arg0, arg1);
		}	
	}
	
}

public class MidiOutput implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Midi Output");
		Map map = metadata.add(1, "devname",		"Device name",	null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		
		List options = new ArrayList();
		map.put("options", options);
		MidiDevice.Info devinfo[] = MidiSystem.getMidiDeviceInfo();
		for (int i = 0; i < devinfo.length; i++) {
			try
			{
				MidiDevice mididev = MidiSystem.getMidiDevice(devinfo[i]);
				if(mididev.getReceiver() != null)
					options.add("\"" + devinfo[i].getName() + "\"");
			}			
			catch(Exception e)
			{
			}				  
		}			
		
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}	
			
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiOutputInstance(parameters);
	}
}
