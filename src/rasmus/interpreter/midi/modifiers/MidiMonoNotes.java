/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.modifiers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TreeMap;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiMonoNotesInstance extends UnitInstanceAdapter implements Commitable
{
	public Variable output;
	public Variable input;
	public Receiver recv = null;
	public Variable answer = null;
	MidiSequence midiseq;
	public boolean answer_added = true;
	
	public void clear()
	{
		events.clear();
		//noteevents.clear();
		stop_events.clear();
		if(answer != null)
		{
			output.remove(answer);
			answer_added = false;
		}	
	}
	
	//ArrayList<MidiEvent> noteevents = new ArrayList<MidiEvent>();
	
	Comparator<MidiEvent> midieventcomparator = new Comparator<MidiEvent>()
	{
		public int compare(MidiEvent arg0, MidiEvent arg1) {
			long comp = arg0.getTick() - arg1.getTick();
			if(comp > 0) return 1;
			if(comp < 0) return -1;
			return 0;
		}					
	};	
	
	TreeMap<MidiEvent,MidiEvent> stop_events = new TreeMap<MidiEvent,MidiEvent>(midieventcomparator);
	
	class CommitEvent
	{
		boolean removeEvent = false;
		MidiEvent midievent;
	}
	
	ArrayList<CommitEvent> events = new ArrayList<CommitEvent>();
	boolean in_commit_need = false;
	boolean is_committing = false;
	int addeventcount = 0;
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}	
	public void commit()
	{	
		
		if(!in_commit_need) return;
		in_commit_need = false;
		
		is_committing = true;
		int insize = ((MidiSequence)MidiSequence.getInstance(input)).getObjects().size();
		if( insize == 0)
		{
			stop_events.clear();
			if(answer_added)
			{
				output.remove(answer);
				answer = new Variable();
				midiseq = MidiSequence.getInstance(answer);			    
				answer_added = false;
			}				
			events.clear();
			stop_events.clear();
			is_committing = false;
			addeventcount = 0;
			return;
		}
		
		if(addeventcount == insize)
		{
			events.clear();
			stop_events.clear();
			is_committing = false;
			addeventcount = 0;
			calc();
			return;
		}
		
		Iterator<CommitEvent> iterator = events.iterator();
		while (iterator.hasNext()) {
			CommitEvent event = iterator.next();
			if(event.removeEvent)
				c_removeEvent(event.midievent);
			else
				c_addEvent(event.midievent);
		}
		events.clear();
		
		
		if(!answer_added)
		{
			output.add(answer);
		}
		is_committing = false;
		addeventcount = 0;
		
	}
	
	public void addEvent(MidiEvent midievent)
	{
		addeventcount++;
		if(is_committing)
		{
			throw new Error("Circual Reference Error !!!!");
		}
		if(!in_commit_need)
		{
			in_commit_need = true;
			namespace.addToCommitStack(this);
		}
		
		CommitEvent event = new CommitEvent();
		event.midievent = midievent;
		event.removeEvent = false;
		events.add(event);
	}
	public void removeEvent(MidiEvent midievent)
	{
		if(!in_commit_need)
		{
			in_commit_need = true;
			namespace.addToCommitStack(this);
		}		
		CommitEvent event = new CommitEvent();
		event.midievent = midievent;
		event.removeEvent = true;
		events.add(event);		
	}
	
	public void c_addEvent(MidiEvent midievent)
	{
		MidiMessage midimessage = midievent.getMessage();
		if(midimessage instanceof ShortMessage)
		{
			ShortMessage smidimessage = (ShortMessage)midimessage;
			if((smidimessage.getCommand() == ShortMessage.NOTE_ON && smidimessage.getData2() > 0) || (smidimessage.getCommand() == ShortMessage.NOTE_OFF && smidimessage.getData1() == 0)) 
			{	
				
				
				try
				{
					MidiEvent nextmidievent = stop_events.tailMap(midievent).firstKey();
					if(nextmidievent != null)
					{
						long nexttick = nextmidievent.getTick();
						
						try {
							ShortMessage smsg = new ShortMessage();
							smsg.setMessage(ShortMessage.NOTE_OFF, ((ShortMessage)(midievent.getMessage())).getChannel(), ((ShortMessage)(midievent.getMessage())).getData1(), 0);
							MidiEvent stopevent = new MidiEvent(smsg, nexttick-1);
							stop_events.put(midievent,  stopevent);
							midiseq.addObject(stopevent);
							
						} catch (InvalidMidiDataException e) {
							e.printStackTrace();
						}						
					}
				} catch(NoSuchElementException e) {
					
					stop_events.put(midievent, midievent);
				}
				
				
				
				try
				{
					MidiEvent prevmidievent = stop_events.headMap(midievent).lastKey();
					if(prevmidievent != null)
					{
						long curtick = midievent.getTick();
						MidiEvent stopevent = stop_events.get(prevmidievent);
						if(stopevent == prevmidievent)
						{
							try {
								ShortMessage smsg = new ShortMessage();
								smsg.setMessage(ShortMessage.NOTE_OFF, ((ShortMessage)(midievent.getMessage())).getChannel(), ((ShortMessage)(prevmidievent.getMessage())).getData1(), 0);
								stopevent = new MidiEvent(smsg, curtick-1);
								stop_events.put(prevmidievent,  stopevent);
								midiseq.addObject(stopevent);
								
							} catch (InvalidMidiDataException e) {
								e.printStackTrace();
							}
						}
						else
						{
							midiseq.removeObject(stopevent);
							stopevent.setTick(curtick-1);
							midiseq.addObject(stopevent);
						}					
					}	
				} catch(NoSuchElementException e) {}
			}
		}
		midiseq.addObject(midievent);
	}
	public void c_removeEvent(MidiEvent midievent)
	{
		MidiMessage midimessage = midievent.getMessage();
		if(midimessage instanceof ShortMessage)
		{		
			ShortMessage smidimessage = (ShortMessage)midimessage;
			if((smidimessage.getCommand() == ShortMessage.NOTE_ON && smidimessage.getData2() > 0) || (smidimessage.getCommand() == ShortMessage.NOTE_OFF && smidimessage.getData1() == 0)) 
			{
				
				MidiEvent nextmidievent = null;
				MidiEvent prevmidievent = null;
				
				MidiEvent stopevent = stop_events.get(midievent);
				if(stopevent != null)
				{
					midiseq.removeObject(stopevent);
					stop_events.remove(midievent);
				}			
				
				try
				{
					prevmidievent = stop_events.headMap(midievent).lastKey();
				} catch(NoSuchElementException e) {}
				
				try
				{
					nextmidievent = stop_events.tailMap(midievent).firstKey();
				} catch(NoSuchElementException e) {}
				
				if(prevmidievent != null)
				{
					stopevent = stop_events.get(prevmidievent);
					if(stopevent != prevmidievent)
						if(nextmidievent == null)						
						{
							midiseq.removeObject(stopevent);
							stop_events.remove(prevmidievent);
						}
						else
						{
							midiseq.removeObject(stopevent);
							stopevent.setTick(nextmidievent.getTick()-1);
							midiseq.addObject(stopevent);
						}					
				}
				
				
				
			}			
		}
		midiseq.removeObject(midievent);
	}	
	
	
	public void calc()
	{
		clear();
		
		if(answer != null)
		{
			if(answer_added)
				output.remove(answer);
		}
		answer_added = true;
		answer = new Variable();
		midiseq = MidiSequence.getInstance(answer);               
		
		List events = MidiSequence.getInstance(input).getObjects();
		Iterator iterator = events.iterator();
		while (iterator.hasNext()) {
			MidiEvent element = (MidiEvent) iterator.next();
			c_addEvent(element);
		}
		
		output.add(answer);
		
		
	}
	
	class RListListenerAndRecv implements ListPartListener, Receiver
	{
		public void objectAdded(ListPart source, Object object) {			
			MidiEvent element = (MidiEvent) object;
			addEvent(element);
		}
		public void objectRemoved(ListPart source, Object object) {
			//MidiSequence midiseq = MidiSequence.getInstance(answer);
			removeEvent((MidiEvent)object);
		}
		public void objectsAdded(ListPart source, List objects) {
			Iterator iterator = objects.iterator();
			while(iterator.hasNext())
			{
				objectAdded(source, iterator.next());
			}
		}
		public void objectsRemoved(ListPart source, List objects) {
			Iterator iterator = objects.iterator();
			while(iterator.hasNext())
			{
				objectRemoved(source, iterator.next());
			}
		}
		
		public void send(MidiMessage message, long timeStamp) {
			/*
			 if(recv == null) recv = MidiSequence.getInstance(output);		
			 MidiEvent event = new MidiEvent(message, 0);
			 message = processEvent(event).getMessage();
			 if(message != null)*/
			
			if(recv == null) recv = MidiSequence.getInstance(output);
			if(message != null)
				recv.send(message, timeStamp);
		}
		public void close() {
			
		}		
		
	}
	
	RListListenerAndRecv midilistener = new RListListenerAndRecv();	
	public ListPartListener getMidiListener()
	{
		return midilistener;
	}
	
	NameSpace namespace;
	
	public MidiMonoNotesInstance(Parameters parameters)
	{
		namespace = parameters.getNameSpace();
		
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault(1, "input");
		
		MidiSequence.getInstance(input).addListener(getMidiListener());
		
		calc();
	}
	public void close() {
		
		in_commit_need = false;
		MidiSequence.getInstance(input).removeListener(this);
		clear();
	}
	
	
}


public class MidiMonoNotes implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Monophony filtering");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiMonoNotesInstance(parameters);
	}
}