/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.modifiers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;


class MidiExtractInstance extends MidiFilterAdapter
{
	Variable from;
	Variable to;
	Variable answer;
	long midifrom = 0;
	long midito = 0;
	boolean filtermidito = true;
	
	public MidiEvent processEvent(MidiEvent midievent) {
		long tick = midievent.getTick();
		
		if(filtermidito)
		{
			if((tick < midifrom) || (tick > midito)) return null;
		}
		else
		{
			if((tick < midifrom)) return null;
		}
		
		return new MidiEvent(midievent.getMessage(), tick - midifrom);
	}	
	
	public void calc()
	{
		
		midifrom = (long)(MidiSequence.DEFAULT_RES * DoublePart.asDouble(from));
		if(to == null)
		{
			midito = 0;
			filtermidito = false;
		}
		else
		{
			filtermidito = true;
			midito = (long)(MidiSequence.DEFAULT_RES * DoublePart.asDouble(to));
			if(midito == 0) filtermidito = false;
		}
		super.calc();
		
		if(!filtermidito) return;
		
		if(answer != null) output.remove(answer);
		answer = null;
		
		
		// Check for Notes still active
		Sequence sq = MidiSequence.asSequence(input);
		Track[] tracks = sq.getTracks();
		MidiSequence manswer = null;
		List[] ch = new ArrayList[16];
		for (int i = 0; i < ch.length; i++) {
			ch[i] = new ArrayList();
		}
		
		for (int i = 0; i < tracks.length; i++) {
			Track track = tracks[i];
			int len = track.size();
			for (int j = 0; j < len; j++) {
				MidiEvent event = track.get(j);
				MidiMessage msg = event.getMessage();
				long tick = event.getTick();
				if(!((tick < midifrom) || (tick > midito))) 
					if(msg instanceof ShortMessage)
					{
						ShortMessage smsg = (ShortMessage)msg;
						int cmd = smsg.getCommand();
						int channel = smsg.getChannel();
						int data1 = smsg.getData1(); 
						if(cmd == ShortMessage.NOTE_ON)
						{
							if(smsg.getData2() != 0)
							{
								ch[channel].add(new Integer(data1));
							}
							else
							{
								ch[channel].remove(new Integer(data1));
							}
						}
						else
							if(cmd == ShortMessage.NOTE_OFF)
							{
								ch[channel].remove(new Integer(data1));
							}
					}				
			}
			for (int j = 0; j < ch.length; j++) {
				if(ch[j].size() != 0)
				{
					if(answer == null)
					{
						answer = new Variable();
						manswer = MidiSequence.getInstance(answer);
					}
					Iterator iter = ch[j].iterator();
					while (iter.hasNext()) {
						Integer note = (Integer) iter.next();
						ShortMessage msg = new ShortMessage();
						try {
							msg.setMessage(ShortMessage.NOTE_ON, j, note.intValue(), 0);
							MidiEvent midievent = new MidiEvent(msg, midito - midifrom);
							manswer.addObject(midievent);						
						} catch (InvalidMidiDataException e) {
							e.printStackTrace();
						}
					}					
					
					ch[j].clear();
				}
			}
			
		}
		
		if(answer != null) output.add(answer);
	}
	public MidiExtractInstance(Parameters parameters)
	{
		super(parameters.getNameSpace());
		output = parameters.getParameterWithDefault("output");
		from = parameters.getParameterWithDefault(1, "from");
		to = parameters.getParameterWithDefault(2, "to");
		input = parameters.getParameterWithDefault(3, "input");
		
		DoublePart.getInstance(from).addListener(this);
		if(to != null)
			DoublePart.getInstance(to).addListener(this);
		
		registerInput(input);
		
		calc();
	}
	public void close() {
		super.close();
		DoublePart.getInstance(from).removeListener(this);
		if(to != null)
			DoublePart.getInstance(to).removeListener(this);		
		if(answer != null) output.remove(answer);
		answer = null;
		
		
		clear();
	}
	
}

public class MidiExtract implements UnitFactory  , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Extract");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "from",			"From",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "to",			"To",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiExtractInstance(parameters);
	}
}
