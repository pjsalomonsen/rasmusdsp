/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.modifiers;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;


class MidiModifyMessagesInstance extends MidiFilterAdapter
{
	long mididpos = 0;
	
	Variable command     = null;
	Variable commandadd = null;
	Variable commandmul   = null;
	Variable channel     = null;
	Variable channeladd = null;
	Variable channelmul   = null;
	Variable data1       = null;
	Variable data1add   = null;
	Variable data1mul     = null;
	Variable data2       = null;
	Variable data2add   = null;
	Variable data2mul     = null;
	Variable pos2        = null;
	Variable pos2add    = null;
	Variable pos2mul      = null;
	
	double f_command     = 0;
	double f_commandadd = 0;
	double f_commandmul   = 1;
	double f_channel     = 0;
	double f_channeladd = 0;
	double f_channelmul   = 1;
	double f_data1       = 0;
	double f_data1add   = 0;
	double f_data1mul     = 1;
	double f_data2       = 0;
	double f_data2add   = 0;
	double f_data2mul     = 1;
	double f_pos2        = 0;
	double f_pos2add    = 0;
	double f_pos2mul      = 1;
	
	
	public MidiEvent processEvent(MidiEvent midievent) {
		MidiMessage msg = midievent.getMessage();    	
		if(msg instanceof ShortMessage)
		{
			ShortMessage ssmg = (ShortMessage)msg;
			ShortMessage ssmg2 = new ShortMessage();
			
			int mcmd = ssmg.getCommand();
			int mchannel = ssmg.getChannel();
			int mdata1 = ssmg.getData1();
			int mdata2 = ssmg.getData2();
			
			if(command    != null) mcmd = (int)f_command;
			if(channel    != null) mchannel = (int)f_channel;
			if(data1      != null) mdata1 = (int)f_data1;
			if(data2      != null) mdata2 = (int)f_data2;
			
			mcmd     = (int)((mcmd    *f_commandmul) + f_commandadd);
			mchannel = (int)((mchannel*f_channelmul) + f_channeladd);
			mdata1   = (int)((mdata1  *f_data1mul)   + f_data1add);
			mdata2   = (int)((mdata2  *f_data2mul)   + f_data2add);
			
			try {
				ssmg2.setMessage(mcmd, mchannel, mdata1, mdata2);
			} catch (InvalidMidiDataException e) {
				System.out.println(e);
			}
			msg = ssmg2;
			
		}    	
		
		long tick = midievent.getTick();
		
		if(pos2    != null) tick = (int)f_pos2;
		tick = (int)((tick*f_pos2mul) + f_pos2add);
		
		return new MidiEvent(msg, tick);
		
		//return midievent;
	}	
	
	public void calc()
	{
		
		if(command     != null) f_command     = DoublePart.asDouble(command);
		if(commandadd != null) f_commandadd = DoublePart.asDouble(commandadd);
		if(commandmul   != null) f_commandmul   = DoublePart.asDouble(commandmul);
		if(channel     != null)
		{
			f_channel     = DoublePart.asDouble(channel);
			if(f_channel != 0) f_channel--;
		}
		if(channeladd != null) f_channeladd = DoublePart.asDouble(channeladd);
		if(channelmul   != null) f_channelmul   = DoublePart.asDouble(channelmul);
		if(data1       != null) f_data1       = DoublePart.asDouble(data1);
		if(data1add   != null) f_data1add   = DoublePart.asDouble(data1add);
		if(data1mul     != null) f_data1mul    = DoublePart.asDouble(data1mul);
		if(data2       != null) f_data2       = DoublePart.asDouble(data2);
		if(data2add   != null) f_data2add   = DoublePart.asDouble(data2add);
		if(data2mul     != null) f_data2mul     = DoublePart.asDouble(data2mul);
		if(pos2        != null) f_pos2        = DoublePart.asDouble(pos2) * MidiSequence.DEFAULT_RES;
		if(pos2add    != null) f_pos2add    = DoublePart.asDouble(pos2add)* MidiSequence.DEFAULT_RES;
		if(pos2mul      != null) f_pos2mul      = DoublePart.asDouble(pos2mul)* MidiSequence.DEFAULT_RES;
		super.calc();
	}
	public MidiModifyMessagesInstance(Parameters parameters)
	{
		super(parameters.getNameSpace());
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		
		command     = parameters.getParameter("command");
		commandadd = parameters.getParameter("commandadd");
		commandmul   = parameters.getParameter("commandmul");
		
		channel     = parameters.getParameter("channel");
		channeladd = parameters.getParameter("channeladd");
		channelmul   = parameters.getParameter("channelnot");
		
		data1       = parameters.getParameter("data1");
		data1add   = parameters.getParameter("data1add");
		data1mul     = parameters.getParameter("data1mul");
		
		data2       = parameters.getParameter("data2");
		data2add   = parameters.getParameter("data2add");
		data2mul     = parameters.getParameter("data2mul");
		
		pos2        = parameters.getParameter("pos");
		pos2add    = parameters.getParameter("posadd");
		pos2mul      = parameters.getParameter("posmul");
		
		if(command     != null) DoublePart.getInstance(command).addListener(this);
		if(commandadd != null) DoublePart.getInstance(commandadd).addListener(this);
		if(commandmul   != null) DoublePart.getInstance(commandmul).addListener(this);
		if(channel     != null) DoublePart.getInstance(channel).addListener(this);
		if(channeladd != null) DoublePart.getInstance(channeladd).addListener(this);
		if(channelmul   != null) DoublePart.getInstance(channelmul).addListener(this);
		if(data1       != null) DoublePart.getInstance(data1).addListener(this);
		if(data1add   != null) DoublePart.getInstance(data1add).addListener(this);
		if(data1mul     != null) DoublePart.getInstance(data1mul).addListener(this);
		if(data2       != null) DoublePart.getInstance(data2).addListener(this);
		if(data2add   != null) DoublePart.getInstance(data2add).addListener(this);
		if(data2mul     != null) DoublePart.getInstance(data2mul).addListener(this);
		if(pos2        != null) DoublePart.getInstance(pos2).addListener(this);
		if(pos2add    != null) DoublePart.getInstance(pos2add).addListener(this);
		if(pos2mul      != null) DoublePart.getInstance(pos2mul).addListener(this);
		
		registerInput(input);
		calc();
	}
	public void close() {
		super.close();
		if(command     != null) DoublePart.getInstance(command).removeListener(this);
		if(commandadd != null) DoublePart.getInstance(commandadd).removeListener(this);
		if(commandmul   != null) DoublePart.getInstance(commandmul).removeListener(this);
		if(channel     != null) DoublePart.getInstance(channel).removeListener(this);
		if(channeladd != null) DoublePart.getInstance(channeladd).removeListener(this);
		if(channelmul   != null) DoublePart.getInstance(channelmul).removeListener(this);
		if(data1       != null) DoublePart.getInstance(data1).removeListener(this);
		if(data1add   != null) DoublePart.getInstance(data1add).removeListener(this);
		if(data1mul     != null) DoublePart.getInstance(data1mul).removeListener(this);
		if(data2       != null) DoublePart.getInstance(data2).removeListener(this);
		if(data2add   != null) DoublePart.getInstance(data2add).removeListener(this);
		if(data2mul     != null) DoublePart.getInstance(data2mul).removeListener(this);
		if(pos2        != null) DoublePart.getInstance(pos2).removeListener(this);
		if(pos2add    != null) DoublePart.getInstance(pos2add).removeListener(this);
		if(pos2mul      != null) DoublePart.getInstance(pos2mul).removeListener(this);
		
		clear();
	}
	
}

public class MidiModifyMessages implements UnitFactory  , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Short Messages Modifier");
		metadata.add(-1, "output",		"Output",			null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(-1, "command",		"Command set",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "commandadd",	"Command add",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "commandmul",	"Command mul",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "channel",		"Channel set",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "channeladd",	"Channel add",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "channelmul",	"Channel mul",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data1",		"Data1 set",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data1add",	"Data1 add",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data1mul",	"Data1 mul",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data2",		"Data2 set",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data2add",	"Data2 add",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data2mul",	"Data2 mul",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "pos",			"Position set",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "posadd",		"Position add",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "posmul",		"Position mul",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",			null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiModifyMessagesInstance(parameters);
	}
}