/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.modifiers;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;


class MidiStretchInstance extends MidiFilterAdapter
{
	Variable amount;	
	double f_amount = 1;	
	
	public MidiEvent processEvent(MidiEvent midievent) {
		MidiMessage msg = midievent.getMessage();
		long tick = (long)(midievent.getTick() * f_amount);
		MidiEvent rmidievent = new MidiEvent(msg, tick);
		return rmidievent;
	}	
	
	public void calc()
	{
		f_amount = DoublePart.asDouble(amount);
		super.calc();
	}
	public MidiStretchInstance(Parameters parameters)
	{
		super(parameters.getNameSpace());
		output = parameters.getParameterWithDefault("output");
		amount = parameters.getParameterWithDefault(1, "amount");
		input = parameters.getParameterWithDefault("input");
		
		DoublePart.getInstance(amount).addListener(this);
		registerInput(input);
		calc();
	}
	public void close() {
		super.close();
		DoublePart.getInstance(amount).removeListener(this);
		clear();
	}
	
}

public class MidiStretch  implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Stretch");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "amount",		"Amount",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);		return metadata;		
	}
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiStretchInstance(parameters);
	}
}
