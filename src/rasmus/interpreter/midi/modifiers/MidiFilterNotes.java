/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.modifiers;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiFilterNotesInstance extends MidiFilterAdapter
{
	
	Variable keyFrom     = null;
	Variable keyTo     = null;
	Variable velFrom     = null;
	Variable velTo     = null;
	
	int f_keyFrom     = -1;
	int f_keyTo     = -1;
	int f_velFrom     = -1;
	int f_velTo     = -1;
	
	public MidiEvent processEvent(MidiEvent midievent) {		
		MidiMessage msg = midievent.getMessage();
		if(msg instanceof ShortMessage)
		{
			ShortMessage smsg = (ShortMessage)msg;
			int cmd = smsg.getCommand();
			if(cmd == ShortMessage.NOTE_ON || cmd == ShortMessage.NOTE_OFF)
			{
				if(cmd == ShortMessage.NOTE_ON)
					if(smsg.getData2() > 0)
					{
						if(f_keyFrom != -1) if(smsg.getData1() < f_keyFrom) return null;
						if(f_keyTo != -1) if(smsg.getData1() > f_keyTo) return null;
						if(f_velFrom != -1) if(smsg.getData2() < f_velFrom) return null;
						if(f_velTo != -1) if(smsg.getData2() > f_velTo) return null;
					}
				return midievent;    			
			}
		}
		return null;
	}	
	
	public void calc()
	{
		
		f_keyFrom     = -1;
		f_keyTo     = -1;
		f_velFrom     = -1;
		f_velTo     = -1;		
		
		if(keyFrom     != null) f_keyFrom     = (int)DoublePart.asDouble(keyFrom);
		if(keyTo != null) f_keyTo = (int)DoublePart.asDouble(keyTo);
		if(velFrom   != null) f_velFrom   = (int)DoublePart.asDouble(velFrom);
		if(velTo  != null) f_velTo  = (int)DoublePart.asDouble(velTo);
	}
	
	public MidiFilterNotesInstance(Parameters parameters)
	{
		super(parameters.getNameSpace());
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		
		keyFrom     = parameters.getParameter(1, "keyFrom");
		keyTo     = parameters.getParameter(2, "keyTo");
		velFrom     = parameters.getParameter(3, "velFrom");
		velTo     = parameters.getParameter(4, "velTo");
		
		if(keyFrom != null) DoublePart.getInstance(keyFrom).addListener(this);
		if(keyTo != null) DoublePart.getInstance(keyTo).addListener(this);
		if(velFrom != null) DoublePart.getInstance(velFrom).addListener(this);
		if(velTo != null) DoublePart.getInstance(velTo).addListener(this);
		
		registerInput(input);
		calc();
	}
	public void close() {
		super.close();
		
		if(keyFrom != null) DoublePart.getInstance(keyFrom).removeListener(this);
		if(keyTo != null) DoublePart.getInstance(keyTo).removeListener(this);
		if(velFrom != null) DoublePart.getInstance(velFrom).removeListener(this);
		if(velTo != null) DoublePart.getInstance(velTo).removeListener(this);
		
		clear();
	}
	
}

public class MidiFilterNotes implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Filter for Note Messages");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "keyfrom",		"Key from",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "keyto",		"Key to",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "velfrom",		"Velcotiy from",null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "velto",		"Velocity to",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiFilterNotesInstance(parameters);
	}
}