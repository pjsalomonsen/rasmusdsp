/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.modifiers;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class MidiRemapNotesInstance extends MidiFilterAdapter
{
	Variable transpose[] = new Variable[12];
	int[] miditranspose = new int[12];
	
	
	
	public MidiEvent processEvent(MidiEvent midievent) {
		MidiMessage msg = midievent.getMessage();
		if(msg instanceof ShortMessage)
		{
			ShortMessage omsg = (ShortMessage)msg;
			int cmd = omsg.getCommand();
			if(cmd == ShortMessage.NOTE_ON || cmd == ShortMessage.NOTE_OFF)
			{    		
				ShortMessage smsg = new ShortMessage();
				try {
					int data1 = omsg.getData1();
					int t = miditranspose[data1 % 12];
					data1 = t;
					if(data1 > 128) return null;
					smsg.setMessage(cmd, omsg.getChannel(), data1, omsg.getData2());
				} catch (InvalidMidiDataException e) {
					System.out.println(e.toString());
				}
				msg = smsg;
			}
		}
		long tick = midievent.getTick();
		MidiEvent rmidievent = new MidiEvent(msg, tick);
		return rmidievent;
	}	
	
	public void calc()
	{
		for (int i = 0; i < 12; i++) 
			miditranspose[i] = (int)DoublePart.asDouble(transpose[i]);
		
		super.calc();
	}
	public MidiRemapNotesInstance(Parameters parameters)
	{
		super(parameters.getNameSpace());
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		
		transpose[0] = parameters.getParameterWithDefault(1, "c");
		transpose[1] = parameters.getParameterWithDefault(2, "c#");
		transpose[2] = parameters.getParameterWithDefault(3, "d");
		transpose[3] = parameters.getParameterWithDefault(4, "d#");
		transpose[4] = parameters.getParameterWithDefault(5, "e");
		transpose[5] = parameters.getParameterWithDefault(6, "f");
		transpose[6] = parameters.getParameterWithDefault(7, "f#");
		transpose[7] = parameters.getParameterWithDefault(8, "g");
		transpose[8] = parameters.getParameterWithDefault(9, "g#");
		transpose[9] = parameters.getParameterWithDefault(10, "a");
		transpose[10] = parameters.getParameterWithDefault(11, "a#");
		transpose[11] = parameters.getParameterWithDefault(12, "b");
		
		for (int i = 0; i < transpose.length; i++) {
			if(transpose[i] != null)
				DoublePart.getInstance(transpose[i]).addListener(this);
		}
		
		registerInput(input);
		
		calc();
	}
	public void close() {
		super.close();
		for (int i = 0; i < 12; i++) {
			if(transpose[i] != null)
				DoublePart.getInstance(transpose[i]).removeListener(this);
		}
		
		
		clear();
	}
	
}

public class MidiRemapNotes implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Remap Notes");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1,  "c",		    "C",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2,  "c#",		    "C#",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3,  "d",		    "D",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4,  "d#",		    "D#",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(5,  "e",		    "E",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(6,  "f",		    "F",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(7,  "f#",		    "F#",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(8,  "g",		    "G",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(9,  "g#",		    "G#",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(10,  "a",		    "A",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(11,  "a#",		    "A#",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(12,  "b",		    "B",	    	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiRemapNotesInstance(parameters);
	}
}

