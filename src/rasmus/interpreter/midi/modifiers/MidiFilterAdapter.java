/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.modifiers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.UnitInstanceAdapter;

public abstract class MidiFilterAdapter extends UnitInstanceAdapter implements Commitable
{
	public NameSpace namespace;
	public Variable output;
	public Variable input;
	public Receiver recv = null;
	public Variable answer = null;
	public Map pevents = new HashMap();
	MidiSequence midiseq;
	
	
	public MidiFilterAdapter(NameSpace namespace)
	{
		this.namespace = namespace;
	}
	
	public void clear()
	{
		in_commit_need = false;
		events.clear();
		
		if(answer != null)
		{
			if(answer_added)
				output.remove(answer);
			pevents.clear();
		}	
	}
	
	public abstract MidiEvent processEvent(MidiEvent midievent);
	
	public void calc()
	{
		clear();
		answer = new Variable();
		midiseq = MidiSequence.getInstance(answer);               
		
		List events = MidiSequence.getInstance(input).getObjects();
		Iterator iterator = events.iterator();
		while (iterator.hasNext()) {
			MidiEvent element = (MidiEvent) iterator.next();        	
			MidiEvent pelement = processEvent(element);
			if(pelement != null)
			{
				pevents.put(element, pelement);
				midiseq.addObject(pelement);
			}
		}
		
		output.add(answer);
		answer_added = true;
		
	}
	
	class CommitEvent
	{
		static final int type_objectAdded = 0;
		static final int type_objectRemoved = 1;
		static final int type_objectsAdded = 2;
		static final int type_objectsRemoved = 3;
		
		int eventtype;
		ListPart source;
		Object object;
	}
	ArrayList<CommitEvent> events = new ArrayList<CommitEvent>();
	boolean in_commit_need = false;
	boolean answer_added = true;
	
	ArrayList addobjects = new ArrayList();
	ArrayList removeobjects = new ArrayList();
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit()
	{	
		if(!in_commit_need) return;
		in_commit_need = false;
		
		if(input_midiseq.getObjects().size() == 0)
		{
			if(answer_added)
			{
				output.remove(answer);
				answer = new Variable();
				midiseq = MidiSequence.getInstance(answer);			    
				answer_added = false;
			}				
			events.clear();
			return;
		}
		
		if(answer == null)
		{
			answer = new Variable();
			midiseq = MidiSequence.getInstance(answer);	
			answer_added = false;
		}

		addobjects.clear();
		removeobjects.clear();		

		Iterator<CommitEvent> iterator = events.iterator();
		while (iterator.hasNext()) {
			CommitEvent event = iterator.next();
			Object object = event.object;			
			if(event.eventtype == CommitEvent.type_objectAdded)
			{
				MidiEvent element = (MidiEvent) object;
				MidiEvent pelement = processEvent(element);
				if(pelement != null)
				{
					pevents.put(element, pelement);
					addobjects.add(pelement);
				}				
			}
			if(event.eventtype == CommitEvent.type_objectsAdded)
			{				
				Iterator iter = ((List)object).iterator();
				while (iter.hasNext()) {
					MidiEvent element = (MidiEvent) iter.next();
					MidiEvent pelement = processEvent(element);
					if(pelement != null)
					{
						pevents.put(element, pelement);
						addobjects.add(pelement);
					}
				}
			}			
			else
				if(event.eventtype == CommitEvent.type_objectRemoved)
				{
					Object pelement = pevents.get(object);
					if(pelement != null) removeobjects.add(pelement);					
				}
				else
					if(event.eventtype == CommitEvent.type_objectsRemoved)
					{
						Iterator iter = ((List)object).iterator();
						while (iter.hasNext()) {							
							Object pelement = pevents.get(iter.next());
							if(pelement != null) removeobjects.add(pelement);
						}
					}					
			
		}
		events.clear();
		
		MidiSequence midiseq = MidiSequence.getInstance(answer);
		midiseq.removeObjects(removeobjects);
		midiseq.addObjects(addobjects);
		addobjects.clear();
		removeobjects.clear();
		
		if(!answer_added)
		{
			output.add(answer);
			answer_added = true;
		}		
		
		
		
		
	}
	public void addCommitEvent(int type, ListPart source, Object object)
	{
		if(!in_commit_need)
		{
			in_commit_need = true;
			namespace.addToCommitStack(this);
		}
		
		CommitEvent event = new CommitEvent();
		event.eventtype = type;
		event.source = source;
		event.object = object;
		events.add(event);
		
	}
	
	class RListListenerAndRecv implements ListPartListener, Receiver
	{
		public void objectAdded(ListPart source, Object object) {			
			addCommitEvent(CommitEvent.type_objectAdded, source, object);	
		}
		public void objectRemoved(ListPart source, Object object) {
			addCommitEvent(CommitEvent.type_objectRemoved, source, object);	
		}
		public void objectsAdded(ListPart source, List objects) {
			addCommitEvent(CommitEvent.type_objectsAdded, source, objects);
			Iterator iterator = objects.iterator();
			while(iterator.hasNext())
			{
				objectAdded(source, iterator.next());
			}
			
		}
		public void objectsRemoved(ListPart source, List objects) {
			addCommitEvent(CommitEvent.type_objectsRemoved, source, objects);
			
			Iterator iterator = objects.iterator();
			while(iterator.hasNext())
			{
				objectRemoved(source, iterator.next());
			}
			
		}
		
		public void send(MidiMessage message, long timeStamp) {
			
			if(recv == null) recv = MidiSequence.getInstance(output);		
			MidiEvent event = new MidiEvent(message, 0);
			MidiEvent pevent = processEvent(event);
			if(pevent != null)
			{
				message = pevent.getMessage();
				if(message != null)
					recv.send(message, timeStamp);
			}
		}
		public void close() {
			
		}		
		
	}
	
	RListListenerAndRecv midilistener = new RListListenerAndRecv();
	
	private MidiSequence input_midiseq;
	public void registerInput(Variable input)
	{
		input_midiseq = MidiSequence.getInstance(input);
		input_midiseq.addListener(midilistener);
		
	}
	
	public void close()
	{
		if(input_midiseq != null) input_midiseq.removeListener(midilistener);
	}
	
	
}
