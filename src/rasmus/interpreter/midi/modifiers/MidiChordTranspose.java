/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.modifiers;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;
import rasmus.util.Chord;

class MidiChordTransposeInstance extends MidiFilterAdapter
{
	Variable from;
	Variable to;
	int miditranspose = 0;
	
	int[] revchordpattern;
	int[] chordpattern;
	
	int source;
	int target;
	
	int type;
	
	int transpose;
	
	public MidiEvent processEvent(MidiEvent midievent) {
		MidiMessage msg = midievent.getMessage();
		if(msg instanceof ShortMessage)
		{
			ShortMessage omsg = (ShortMessage)msg;
			int cmd = omsg.getCommand();
			if(omsg.getChannel() != type)
				if(cmd == ShortMessage.NOTE_ON || cmd == ShortMessage.NOTE_OFF)
				{    		
					ShortMessage smsg = new ShortMessage();
					try {
						int data1 = omsg.getData1() - source;
						
						int note = data1 % 12;
						while(note < 0) note += 12;
						data1 -= note;    		 
						note = chordpattern[revchordpattern[note]];
						data1 += note;    		  	    		  
						data1 = data1 + source + transpose;    		  	    		      		  	
						if(data1 < 0) return null;
						if(data1 > 128) return null;
						smsg.setMessage(cmd, omsg.getChannel(), data1, omsg.getData2());
					} catch (InvalidMidiDataException e) {
						System.out.println(e.toString());
					}
					msg = smsg;
				}
		}
		long tick = midievent.getTick();
		MidiEvent rmidievent = new MidiEvent(msg, tick);
		return rmidievent;
	}	
	
	public void calc()
	{
		Chord schord = new Chord(ObjectsPart.toString(from));
		Chord tchord = new Chord(ObjectsPart.toString(to));
		
		source = schord.root;
		target = tchord.root;
		
		transpose = (target - source) % 12;
		if(transpose > 6) transpose -= 12;
		
		revchordpattern = schord.getTransform(true);
		chordpattern = tchord.getTransform(false);
		
		super.calc();
	}
	public MidiChordTransposeInstance(int type, Parameters parameters)
	{
		super(parameters.getNameSpace());
		this.type = type;
		output = parameters.getParameterWithDefault("output");
		from = parameters.getParameterWithDefault(1, "from");
		to = parameters.getParameterWithDefault(2, "to");
		input = parameters.getParameterWithDefault(3, "input");
		
		ObjectsPart.getInstance(from).addListener(this);
		ObjectsPart.getInstance(to).addListener(this);
		registerInput(input);
		
		calc();
	}
	public void close() {
		super.close();
		ObjectsPart.getInstance(from).removeListener(this);
		ObjectsPart.getInstance(to).removeListener(this);
		clear();
	}
	
}

public class MidiChordTranspose implements UnitFactory, MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Chord Transposer");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(1, "from",			"from",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "to",			"to",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "input",		"Input",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}	
	
	
	int type;
	public MidiChordTranspose(int type)
	{
		this.type = type;
	}
	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiChordTransposeInstance(type, parameters);
	}
}
