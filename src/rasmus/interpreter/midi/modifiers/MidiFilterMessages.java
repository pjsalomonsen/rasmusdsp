/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi.modifiers;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;


class MidiFilterMessagesInstance extends MidiFilterAdapter
{
	long mididpos = 0;
	
	Variable command     = null;
	Variable commandfrom = null;
	Variable commandto   = null;
	Variable commandnot  = null;
	Variable channel     = null;
	Variable channelfrom = null;
	Variable channelto   = null;
	Variable channelnot  = null;
	Variable data1       = null;
	Variable data1from   = null;
	Variable data1to     = null;
	Variable data1not    = null;
	Variable data2       = null;
	Variable data2from   = null;
	Variable data2to     = null;
	Variable data2not    = null;
	Variable pos2        = null;
	Variable pos2from    = null;
	Variable pos2to      = null;
	Variable pos2not     = null;
	
	double f_command     = 0;
	double f_commandfrom = 0;
	double f_commandto   = 0;
	double f_commandnot  = 0;
	double f_channel     = 0;
	double f_channelfrom = 0;
	double f_channelto   = 0;
	double f_channelnot  = 0;
	double f_data1       = 0;
	double f_data1from   = 0;
	double f_data1to     = 0;
	double f_data1not    = 0;
	double f_data2       = 0;
	double f_data2from   = 0;
	double f_data2to     = 0;
	double f_data2not    = 0;    
	double f_pos2        = 0;
	double f_pos2from    = 0;
	double f_pos2to      = 0;
	double f_pos2not     = 0;	
	
	
	public MidiEvent processEvent(MidiEvent midievent) {
		MidiMessage msg = midievent.getMessage();    	
		if(msg instanceof ShortMessage)
		{
			ShortMessage ssmg = (ShortMessage)msg;
			
			int val = ssmg.getCommand();
			if(command    != null) if(!(Math.abs(val - f_command)  < 0.2)) return null; 
			if(commandnot != null) if( (Math.abs(val - f_commandnot)  < 0.2)) return null; 
			if(commandfrom!= null)         if(!(val >= f_commandfrom)) return null; 
			if(commandto!= null)           if(!(val <= f_commandto)) return null;     		
			
			val = ssmg.getChannel();
			if(channel    != null) if(!(Math.abs(val - f_channel)  < 0.2)) return null; 
			if(channelnot != null) if( (Math.abs(val - f_channelnot)  < 0.2)) return null; 
			if(channelfrom!= null)         if(!(val >= f_channelfrom)) return null; 
			if(channelto!= null)           if(!(val <= f_channelto)) return null;     		
			
			val = ssmg.getData1();
			if(data1    != null) if(!(Math.abs(val - f_data1)  < 0.2)) return null; 
			if(data1not != null) if( (Math.abs(val - f_data1not)  < 0.2)) return null; 
			if(data1from!= null)         if(!(val >= f_data1from)) return null; 
			if(data1to!= null)           if(!(val <= f_data1to)) return null;     		
			
			val = ssmg.getData2();
			if(data2    != null) if(!(Math.abs(val - f_data2)  < 0.2)) return null; 
			if(data2not != null) if( (Math.abs(val - f_data2not)  < 0.2)) return null; 
			if(data2from!= null)         if(!(val >= f_data2from)) return null; 
			if(data2to!= null)           if(!(val <= f_data2to)) return null;     		        	
			
		}    	
		long tick = midievent.getTick();
		
		if(pos2    != null) if(!(Math.abs(tick - f_pos2)  < 0.2)) return null; 
		if(pos2not != null) if( (Math.abs(tick - f_pos2not)  < 0.2)) return null; 
		if(pos2from!= null)          if(!(tick >= f_pos2from)) return null; 
		if(pos2to!= null)            if(!(tick <= f_pos2to)) return null; 
		
		return midievent;
	}	
	
	public double ToMidiChannel(double in)
	{
		if(in == 0) return 0;
		return in - 1;
	}
	
	public void calc()
	{
		
		if(command     != null) f_command     = DoublePart.asDouble(command);
		if(commandfrom != null) f_commandfrom = DoublePart.asDouble(commandfrom);
		if(commandto   != null) f_commandto   = DoublePart.asDouble(commandto);
		if(commandnot  != null) f_commandnot  = DoublePart.asDouble(commandnot);
		if(channel     != null) f_channel     = ToMidiChannel(DoublePart.asDouble(channel));
		if(channelfrom != null) f_channelfrom = ToMidiChannel(DoublePart.asDouble(channelfrom));
		if(channelto   != null) f_channelto   = ToMidiChannel(DoublePart.asDouble(channelto));
		if(channelnot  != null) f_channelnot  = ToMidiChannel(DoublePart.asDouble(channelnot));
		if(data1       != null) f_data1       = DoublePart.asDouble(data1);
		if(data1from   != null) f_data1from   = DoublePart.asDouble(data1from);
		if(data1to     != null) f_data1to     = DoublePart.asDouble(data1to);
		if(data1not    != null) f_data1not    = DoublePart.asDouble(data1not);
		if(data2       != null) f_data2       = DoublePart.asDouble(data2);
		if(data2from   != null) f_data2from   = DoublePart.asDouble(data2from);
		if(data2to     != null) f_data2to     = DoublePart.asDouble(data2to);
		if(data2not    != null) f_data2not    = DoublePart.asDouble(data2not);
		if(pos2        != null) f_pos2        = DoublePart.asDouble(pos2) * MidiSequence.DEFAULT_RES;
		if(pos2from    != null) f_pos2from    = DoublePart.asDouble(pos2from)* MidiSequence.DEFAULT_RES;
		if(pos2to      != null) f_pos2to      = DoublePart.asDouble(pos2to)* MidiSequence.DEFAULT_RES;
		if(pos2not     != null) f_pos2not     = DoublePart.asDouble(pos2not)* MidiSequence.DEFAULT_RES;			
		super.calc();
	}
	public MidiFilterMessagesInstance(Parameters parameters)
	{
		super(parameters.getNameSpace());
		output = parameters.getParameterWithDefault("output");
		input = parameters.getParameterWithDefault("input");
		
		command     = parameters.getParameter("command");
		commandfrom = parameters.getParameter("commandfrom");
		commandto   = parameters.getParameter("commandto");
		commandnot  = parameters.getParameter("commandnot");
		
		channel     = parameters.getParameter("channel");
		channelfrom = parameters.getParameter("channelfrom");
		channelto   = parameters.getParameter("channelto");
		channelnot  = parameters.getParameter("channelnot");		
		
		data1       = parameters.getParameter("data1");
		data1from   = parameters.getParameter("data1from");
		data1to     = parameters.getParameter("data1to");
		data1not    = parameters.getParameter("data1not");	
		
		data2       = parameters.getParameter("data2");
		data2from   = parameters.getParameter("data2from");
		data2to     = parameters.getParameter("data2to");
		data2not    = parameters.getParameter("data2not");
		
		pos2        = parameters.getParameter("pos");
		pos2from    = parameters.getParameter("posfrom");
		pos2to      = parameters.getParameter("posto");
		pos2not     = parameters.getParameter("posnot");			
		
		if(command     != null) DoublePart.getInstance(command).addListener(this);
		if(commandfrom != null) DoublePart.getInstance(commandfrom).addListener(this);
		if(commandto   != null) DoublePart.getInstance(commandto).addListener(this);
		if(commandnot  != null) DoublePart.getInstance(commandnot).addListener(this);
		if(channel     != null) DoublePart.getInstance(channel).addListener(this);
		if(channelfrom != null) DoublePart.getInstance(channelfrom).addListener(this);
		if(channelto   != null) DoublePart.getInstance(channelto).addListener(this);
		if(channelnot  != null) DoublePart.getInstance(channelnot).addListener(this);
		if(data1       != null) DoublePart.getInstance(data1).addListener(this);
		if(data1from   != null) DoublePart.getInstance(data1from).addListener(this);
		if(data1to     != null) DoublePart.getInstance(data1to).addListener(this);
		if(data1not    != null) DoublePart.getInstance(data1not).addListener(this);
		if(data2       != null) DoublePart.getInstance(data2).addListener(this);
		if(data2from   != null) DoublePart.getInstance(data2from).addListener(this);
		if(data2to     != null) DoublePart.getInstance(data2to).addListener(this);
		if(data2not    != null) DoublePart.getInstance(data2not).addListener(this);
		if(pos2        != null) DoublePart.getInstance(pos2).addListener(this);
		if(pos2from    != null) DoublePart.getInstance(pos2from).addListener(this);
		if(pos2to      != null) DoublePart.getInstance(pos2to).addListener(this);
		if(pos2not     != null) DoublePart.getInstance(pos2not).addListener(this);
		
		registerInput(input);
		
		calc();
	}
	public void close() {
		super.close();
		if(command     != null) DoublePart.getInstance(command).removeListener(this);
		if(commandfrom != null) DoublePart.getInstance(commandfrom).removeListener(this);
		if(commandto   != null) DoublePart.getInstance(commandto).removeListener(this);
		if(commandnot  != null) DoublePart.getInstance(commandnot).removeListener(this);
		if(channel     != null) DoublePart.getInstance(channel).removeListener(this);
		if(channelfrom != null) DoublePart.getInstance(channelfrom).removeListener(this);
		if(channelto   != null) DoublePart.getInstance(channelto).removeListener(this);
		if(channelnot  != null) DoublePart.getInstance(channelnot).removeListener(this);
		if(data1       != null) DoublePart.getInstance(data1).removeListener(this);
		if(data1from   != null) DoublePart.getInstance(data1from).removeListener(this);
		if(data1to     != null) DoublePart.getInstance(data1to).removeListener(this);
		if(data1not    != null) DoublePart.getInstance(data1not).removeListener(this);
		if(data2       != null) DoublePart.getInstance(data2).removeListener(this);
		if(data2from   != null) DoublePart.getInstance(data2from).removeListener(this);
		if(data2to     != null) DoublePart.getInstance(data2to).removeListener(this);
		if(data2not    != null) DoublePart.getInstance(data2not).removeListener(this);
		if(pos2        != null) DoublePart.getInstance(pos2).removeListener(this);
		if(pos2from    != null) DoublePart.getInstance(pos2from).removeListener(this);
		if(pos2to      != null) DoublePart.getInstance(pos2to).removeListener(this);
		if(pos2not     != null) DoublePart.getInstance(pos2not).removeListener(this);
		clear();
	}
	
}

public class MidiFilterMessages implements UnitFactory  , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Short Messages Filter");
		metadata.add(-1, "output",		"Output",			null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(-1, "command",		"Command is",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "commandfrom",	"Command from",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "commandto",	"Command to",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "commandnot",	"Command is not",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "channel",		"Channel is",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "channelfrom",	"Channel from",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "channelto",	"Channel to",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "channelnot",	"Channel is not",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data1",		"Data1 is",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data1from",	"Data1 from",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data1to",		"Data1 to",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data1not",	"Data1 is not",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data2",		"Data2 is",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data2from",	"Data2 from",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data2to",		"Data2 to",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "data2not",	"Data2 is not",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "pos",			"Position is",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "posfrom",		"Position from",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "posto",		"Position to",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "posnot",		"Position is not",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"Input",			null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;		
	}	

	public UnitInstancePart newInstance(Parameters parameters) {
		return new MidiFilterMessagesInstance(parameters);
	}
}
