/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.midi;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.ext.Module;
import rasmus.interpreter.ext.ModuleFactory;
import rasmus.interpreter.midi.generators.MidiABCNotation;
import rasmus.interpreter.midi.generators.MidiLongMessage;
import rasmus.interpreter.midi.generators.MidiNote;
import rasmus.interpreter.midi.generators.MidiShortMessage;
import rasmus.interpreter.midi.generators.MidiTempo;
import rasmus.interpreter.midi.io.MidiFile;
import rasmus.interpreter.midi.io.MidiInput;
import rasmus.interpreter.midi.io.MidiOutput;
import rasmus.interpreter.midi.modifiers.MidiChannel;
import rasmus.interpreter.midi.modifiers.MidiChordTranspose;
import rasmus.interpreter.midi.modifiers.MidiDelay;
import rasmus.interpreter.midi.modifiers.MidiExtract;
import rasmus.interpreter.midi.modifiers.MidiFilterMessages;
import rasmus.interpreter.midi.modifiers.MidiFilterMetaMessages;
import rasmus.interpreter.midi.modifiers.MidiFilterNotes;
import rasmus.interpreter.midi.modifiers.MidiModifyMessages;
import rasmus.interpreter.midi.modifiers.MidiMonoNotes;
import rasmus.interpreter.midi.modifiers.MidiRemapNotes;
import rasmus.interpreter.midi.modifiers.MidiStretch;
import rasmus.interpreter.midi.modifiers.MidiTranspose;
import rasmus.interpreter.midi.modifiers.MidiTranspose2;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.unit.UnitInstancePart;

public class MidiModule extends ModuleFactory {
	
	class ModuleInstance extends Module
	{		
		public ModuleInstance(NameSpace namespace)
		{
			setNameSpace(namespace);
			
			add(":", new MidiNote());			
			//add("|", new MidiDelay());			

			add("ABC", new MidiABCNotation());			

			add("Switch", new MidiSwitch());			
			add("PlayMidi", new MidiPlayer());			
			add("MidiInput", new MidiInput());
			add("MidiOutput", new MidiOutput());
			add("MidiFile", new MidiFile());
			add("MidiPlayer", new MidiPlayer());			
			add("File", new MidiFile());
			add("Channel", new MidiChannel());
			add("FilterNotes", new MidiFilterNotes());
			add("Transpose", new MidiTranspose());
			add("Transpose2", new MidiTranspose2());
			add("ChordTranspose", new MidiChordTranspose(-1));
			add("ChordTranspose2", new MidiChordTranspose(9));
			add("RemapNotes", new MidiRemapNotes());
			add("Delay", new MidiDelay());
			add("Stretch", new MidiStretch());
			add("Extract", new MidiExtract());
			add("ShortMessage", new MidiShortMessage());
			add("LongMessage", new MidiLongMessage());
			add("Note", new MidiNote());
			add("Tempo", new MidiTempo());
			add("FilterMessages", new MidiFilterMessages());
			add("ModifyMessages", new MidiModifyMessages());
			add("FilterMetaMessages", new MidiFilterMetaMessages());			
			add("MonoNotes", new MidiMonoNotes());
			
			for (int i = 0; i < 9; i++) {
				int oct = i * 12;
				add("c" + i, oct + 0);
				add("c#" + i, oct + 1);
				add("d" + i, oct + 2);
				add("d#" + i, oct + 3);
				add("e" + i, oct + 4);
				add("f" + i, oct + 5);
				add("f#" + i, oct + 6);
				add("g" + i, oct + 7);
				add("g#" + i, oct + 8);
				add("a" + i, oct + 9);
				add("a#" + i, oct + 10);
				add("b" + i, oct + 11);
			}	
			
			interpreter = new Interpreter(namespace);
			interpreter.setAutoCommit(false);
			try {
				interpreter.eval("group_midi_dev <- RegisterGroup(\"Input\") <- group_midi_dev_input;");
			} catch (ScriptParserException e1) {
				e1.printStackTrace();
			}
			try {
				interpreter.eval("group_midi_dev <- RegisterGroup(\"Output\") <- group_midi_dev_output;");
			} catch (ScriptParserException e1) {
				e1.printStackTrace();
			}
			MidiDevice.Info devinfo[] = MidiSystem.getMidiDeviceInfo();
			for (int i = 0; i < devinfo.length; i++) {
				try
				{
					MidiDevice mididev = MidiSystem.getMidiDevice(devinfo[i]);
					try
					{
						if(mididev.getReceiver() != null)
							interpreter.eval("group_midi_dev_output <- RegisterConstant(\"" + devinfo[i].getName().replaceAll("\\\"", "\"+'\"'+\"") + "\", \"" + devinfo[i].getDescription() + "\");");
					}
					catch(Exception e)
					{				    	
					}
					
					try
					{
						if(mididev.getTransmitter() != null)
							interpreter.eval("group_midi_dev_input <- RegisterConstant(\"" + devinfo[i].getName().replaceAll("\\\"", "\"+'\"'+\"") + "\", \"" + devinfo[i].getDescription() + "\");");
					}
					catch(Exception e)
					{				    	
					}
				}
				
				catch(Exception e)
				{
					e.printStackTrace();
				}				  
			}			
		}
		
		Interpreter interpreter;
		public void close()
		{
			interpreter.close();
			super.close();
		}
	}
	
	public UnitInstancePart newInstance(NameSpace namespace) {
		return new ModuleInstance(namespace);
	}
	
}
