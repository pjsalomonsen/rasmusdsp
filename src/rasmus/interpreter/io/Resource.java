/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.io;

import java.io.File;

public class Resource {
	
	private File file;
	private String path;
	private ResourceManager manager;
	private boolean stored = false;
	
	public Resource clone(ResourceManager manager)
	{
		return new Resource(manager, path, file, stored);
	}
	
	public Resource(ResourceManager manager, String path, File file)	
	{
		this.path = path;
		this.file = file;
		this.manager = manager;
	}
	
	public Resource(ResourceManager manager, String path, File file, boolean stored)	
	{
		this.path = path;
		this.file = file;
		this.manager = manager;
		this.stored = stored;
	}
		
	
	public ResourceManager getManager()
	{
		return manager;
	}
	
	public String getPath()
	{
		return path;
	}
	
	public File getFile()
	{
		return file;
	}
	
	private int usecount = 0;
	
	protected void checkout()
	{
		synchronized (this) {
			usecount++;
		}
	}

	public void close()
	{
		synchronized (this) {
			usecount--;
			if(!stored)
				if(usecount == 0)
				{
					synchronized (manager.resources) {	
						manager.resources.remove(path);
					}
				}
		}
	}
}
