/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;

public class ResourceManager {
	
	public static ResourceManager getInstance(NameSpace namespace)
	{
		Variable managervar = namespace.get("_resource_manager");
		Object[] objects = ObjectsPart.getInstance(managervar).getObjects().toArray();
		for (int i = 0; i < objects.length; i++) 
			if(objects[i] instanceof ResourceManager) return (ResourceManager)objects[i];		
		ResourceManager manager = new ResourceManager();
		managervar.add(ObjectsPart.asVariable(manager));
		return manager;
	}
	
	protected Hashtable<String, Resource> resources = new Hashtable<String, Resource>();
	
	public Collection<Resource> getResources()
	{
		return resources.values();
	}
	
	public Resource getResource(String path) 
	{
		if( path.length() == 0) return null;
		synchronized (resources) 
		{		
			Resource resource = resources.get(path);
			if(resource == null)
			{
				if(isURL(path))
				{
					resource = getResourceFromURL(path);
					if(resource == null) return null;
					resources.put(path, resource);									
				}
				else
				{				
					File file = new File(path);
					if(!file.exists())
					{
						System.err.println(file.getPath() + " not found.");
						return null;
					}
					try {
						file = file.getCanonicalFile();
					} catch (IOException e) {
						e.printStackTrace();
						return null;
					}
					if(!file.isFile()) return null;
					// path = file.getPath(); We shouldn't do this!!! 			
					resource = new Resource(this, path, file);
					resources.put(path, resource);				
				}
			}
			resource.checkout();
			return resource;
		}
	}
	
	public static boolean isURL(String url)
	{
		char[] chars = url.toCharArray();		
		int i = 0;
		
		if(i == chars.length) return false;
		if(!Character.isLetter(chars[i])) return false;
		i++;
		
		while(i < chars.length)
		{
			if(!Character.isLetter(chars[i])) break;
			i++;
		}
		
		if(i == chars.length) return false;
		if(chars[i] != ':') return false;
		i++;
		
		if(i == chars.length) return false;
		if(chars[i] != '/') return false;
		i++;

		if(i == chars.length) return false;
		if(chars[i] != '/') return false;
		i++;		
		
		return true;
		
	}
		
	private Map<String, Resource> downloaded_resources = new HashMap<String, Resource>();
	
	private Resource getResourceFromURL(String path)
	{		
		synchronized (downloaded_resources) {							
			Resource resource = downloaded_resources.get(path);
			if(resource != null) return resource;		
		}
			
		URL url;
		try {
			url = new URL(path);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
		
		File file;
		try {
			file = downloadFromURL(url);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		if(file == null) return null;
		file.deleteOnExit();
		
		Resource resource = new Resource(this, path, file, true);		
		synchronized (downloaded_resources) {							
			downloaded_resources.put(path, resource);
		}
		return resource;			
	}
			
	private File downloadFromURL(URL url) throws IOException
	{
		InputStream is;
		File tempfile;
		
		if(url.getProtocol().equalsIgnoreCase("http") || url.getProtocol().equalsIgnoreCase("https") )
		{
			URLConnection conn = url.openConnection();
			
			String mimetype = conn.getContentType();
			String ext = null;
			if(mimetype != null)
			{
				ext = getMimeTypeExtension(mimetype);
			}
			if(ext == null)
			{
				String path = url.getPath();
				String suffix = path;
				int li = suffix.lastIndexOf("/");
				if(li == -1) li = suffix.lastIndexOf("\\");
				if(li != -1) suffix = suffix.substring(li + 1);				
				li = path.lastIndexOf(".");				
				if(li != -1) 
					suffix = path.substring(li);
				else
					suffix = "";								
				ext = suffix;
			}
			
			tempfile = File.createTempFile("rasmusdsp_url","."+ext);
			is = conn.getInputStream();						
		}
		else
		{
			String path = url.getPath();
			String suffix = path;
			int li = suffix.lastIndexOf("/");
			if(li == -1) li = suffix.lastIndexOf("\\");
			if(li != -1) suffix = suffix.substring(li + 1);				
			li = path.lastIndexOf(".");				
			if(li != -1) 
				suffix = path.substring(li);
			else
				suffix = "";								
			String ext = suffix;

			tempfile = File.createTempFile("rasmusdsp_url",ext);			
			is = url.openStream();
					
		}
		
		System.out.println("Downloading " + url.toExternalForm());
				
		FileOutputStream fos = new FileOutputStream(tempfile);
		try
		{
			byte[] buffer = new byte[1024];								
			int ret;
			while((ret = is.read(buffer, 0, 1024)) != -1)
				fos.write(buffer, 0, ret);
		}
		finally
		{
			fos.close();
		}						
					
		tempfile.deleteOnExit();
		is.close();
		
		return tempfile;
	}
	
	private static Properties mimetypes = new Properties();
	private static boolean mimetypes_loaded = false;
	
	public static String getMimeTypeExtension(String mimetype) throws IOException
	{
		synchronized (mimetypes) {
			if(!mimetypes_loaded)
			{
				InputStream is = ResourceManager.class.getClass().getResourceAsStream("/rasmus/interpreter/io/mimetypes.properties");
				mimetypes.load(is);
				is.close();
				mimetypes_loaded = true;
			}
		}
		return mimetypes.getProperty(mimetype);
	}

	public void importResource(String path, File file) {
		resources.put(path, new Resource(this, path, file, true));
	}
	
	public void importResources(ResourceManager manager)
	{
		Iterator iterator = manager.resources.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();
			String path = (String)entry.getKey();			
			Resource resource = resources.get(path);
			if(resource == null)
			{
				Resource import_resource = (Resource)entry.getValue();
				resources.put(path, import_resource.clone(this));
			}
		}
	}
}
