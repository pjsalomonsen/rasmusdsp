/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import rasmus.interpreter.Variable;
import rasmus.interpreter.VariablePartAdapter;

public abstract class ListPart extends VariablePartAdapter {

	public static boolean resetMode = false;
	
	private List objects = new LinkedList();
	
	private boolean immutable = false;	
	public boolean isImmutable()
	{
		return immutable;
	}
	public void setImmutable(boolean value)
	{
		immutable = value;
	}
	
	RListList listlist = new RListList();
	
	class RListList implements List
	{
        public ListPart getSource()
        {
        	return ListPart.this;
        }
		public int size() {
			int sizecount = objects.size();
			Iterator iterator = childlists.iterator();
			while (iterator.hasNext()) {
				ListPart element = (ListPart) iterator.next();
				sizecount += element.getObjects().size();
			}
			return sizecount;
		}

		public boolean isEmpty() {
			if(!objects.isEmpty()) return false;
			Iterator iterator = childlists.iterator();
			while (iterator.hasNext()) {
				ListPart element = (ListPart) iterator.next();
				if(!element.getObjects().isEmpty()) return false;
			}				
			return true;
		}

		public Iterator iterator() {
			return new Iterator()
			{
				Iterator iterator = objects.iterator();
				Iterator citerator = childlists.iterator();
				public boolean hasNext() {
					
					if(iterator.hasNext())
					{
						return true;
					}
					
					while(citerator.hasNext())
					{
						iterator = ((ListPart)citerator.next()).getObjects().iterator();
						if(iterator.hasNext()) return true;
					}
					return false;
				}

				public Object next() {
					return iterator.next();
				}

				public void remove() {
				}					
			};
		}


		public boolean contains(Object arg0) {
			
			if(objects.contains(arg0)) return true;
			Iterator iterator = childlists.iterator();
			while (iterator.hasNext()) {
				ListPart element = (ListPart) iterator.next();
				if(element.getObjects().contains(arg0)) return true;
			}				
			return false;
			
		}
		
		public Object[] toArray() {
			
			int size = size();
			Object[] array = new Object[size];
			Iterator iter = iterator();
			int i = 0;
			while(iter.hasNext())
			{
				array[i] = iter.next();
				i++;
			}
			return array;
		}

		public Object[] toArray(Object[] array) {
			Iterator iter = iterator();
			int i = 0;
			while(iter.hasNext())
			{
				array[i] = iter.next();
				i++;
			}
			return array;
		}

		public boolean add(Object arg0) {
			return false;
		}

		public boolean remove(Object arg0) {
			return false;
		}

		public boolean containsAll(Collection arg0) {
			return false;
		}

		public boolean addAll(Collection arg0) {
			return false;
		}

		public boolean addAll(int arg0, Collection arg1) {
			return false;
		}

		public boolean removeAll(Collection arg0) {
			return false;
		}

		public boolean retainAll(Collection arg0) {
			return false;
		}

		public void clear() {
			
		}

		public Object get(int arg0) {
			return null;
		}

		public Object set(int arg0, Object arg1) {
			return null;
		}

		public void add(int arg0, Object arg1) {
			
		}

		public Object remove(int arg0) {
			return null;
		}

		public int indexOf(Object arg0) {
			return 0;
		}

		public int lastIndexOf(Object arg0) {
			return 0;
		}

		public ListIterator listIterator() {
			return null;
		}

		public ListIterator listIterator(int arg0) {
			return null;
		}

		public List subList(int arg0, int arg1) {
			return null;
		}
		
	};
	
	
	// If the audiosystem tries to read from
	// this object with iterator a Concurrent error can occurr
	// TODO INVESTIGATIOn IS NEEDED
	//   has already happen in AudioRenderNotes, line: 655
	
	public List getObjects()
	{
		return listlist;		
	}
	
	public SynchronizedList synchronizedList()
	{
		return new SynchronizedList(this);
	}
	
	boolean hasobjects = false;
	boolean hassingleeobjects = false;
	public void addObject(Object object)
	{
		
		if(resetMode) return;
        objects.add(object);
        sendObjectAdded(object);
	}
	public void removeObject(Object object)
	{
		
		if(resetMode) return;
		objects.remove(object);
        sendObjectRemoved(object);
	}	
	public void addObjects(List list)
	{
		if(resetMode) return;
		/*
		if(list instanceof RListList)
		{
			if(childlists.contains(((RListList)list).getSource()))
			{
				sendObjectsAdded(list);
 				return;
			}
		}*/
		objects.addAll(list);        
		sendObjectsAdded(list);
	}
	public void removeObjects(List list)
	{
		
		if(resetMode) return;
		/*
		if(list instanceof RListList)
		{
			if(childlists.contains(((RListList)list).getSource()))
			{
				sendObjectsRemoved(list);
 				return;
			}
		}*/
		/*
		if(objects.size() == list.size())
			objects.clear();
		else*/
		objects.removeAll(list);
		sendObjectsRemoved(list);
	}		

	static long adding = 0;
	
	ArrayList childlists = new ArrayList();
	public void add(Variable variable)
	{		
		ListPart source = ((ListPart)(variable.get(getClass())));
		//List list = source.objects;
		//addObjects(list);	
		childlists.add(source);
		//addObjects(source.getObjects());	
		sendObjectsAdded(source.getObjects());
		if(!source.isImmutable()) 
			source.addListener(listener);
		dependvar.add(source);		
	}
    public void remove(Variable variable)	
	{
		ListPart source = ((ListPart)(variable.get(getClass())));
		//List list = source.objects;
		//removeObjects(list);		
		childlists.remove(source);
		//removeObjects(source.getObjects());
		sendObjectsRemoved(source.getObjects());
		if(!source.isImmutable()) 
			source.removeListener(listener);
		dependvar.remove(source);
	}	
    public List dependvar = new ArrayList();	
    public void clear()
    {
    	Iterator di = dependvar.iterator();
     	while (di.hasNext()) {
    		((ListPart) di.next()).removeListener(listener);			
    	}
     	ArrayList oder = new ArrayList();
     	Iterator iterator = getObjects().iterator();
     	while (iterator.hasNext()) 
     		oder.add(iterator.next());
     	List bef_objects = oder;//objects;
     	objects = new ArrayList();
     	//removeObjects(bef_objects);
     	sendObjectsRemoved(bef_objects);
     	childlists.clear();
     	dependvar.clear();
    	
    }
    
    // EVENT / LISTENER CODE
    ListPartListener listener = new ListPartListener()
	{
	public void objectAdded(ListPart source, Object object)
	{
		if(childlists.contains(source))
		{
			sendObjectAdded(object);
			return;
		}
		addObject(object);
	}
	public void objectRemoved(ListPart source, Object object)
	{		
		if(childlists.contains(source))
		{
			sendObjectRemoved(object);
			return;
		}		
		removeObject(object);
	}
	public void objectsAdded(ListPart source, List objects)
	{
		if(childlists.contains(source))
		{
			sendObjectsAdded(objects);
			return;
		}		
		addObjects(objects);
	}
	public void objectsRemoved(ListPart source, List objects)	
	{
		if(childlists.contains(source))
		{
			sendObjectsRemoved(objects);
			return;
		}		
		removeObjects(objects);
	}
	};
	
	private ListPartListener[] getListeners()
	{
    	ListPartListener[] listenerslist = null;
    	synchronized (listeners) {
    		if(this.listenerslist == null)
    		{
    			this.listenerslist = new ListPartListener[listeners.size()];
    			listeners.toArray(this.listenerslist);
    		}    			
    		listenerslist = this.listenerslist;    		
    	}
    	return listenerslist;
	}
	
    public void sendObjectAdded(Object object)
    {
    	ListPartListener[] listenerslist = getListeners();
    	for (int i = 0; i < listenerslist.length; i++) 
    		listenerslist[i].objectAdded(this, object);
    }	
    public void sendObjectRemoved(Object object)
    {
    	ListPartListener[] listenerslist = getListeners();
    	for (int i = 0; i < listenerslist.length; i++) 
    		listenerslist[i].objectRemoved(this, object);
    }	    
    public void sendObjectsAdded(List list)
    {
    	ListPartListener[] listenerslist = getListeners();
    	for (int i = 0; i < listenerslist.length; i++) 
    		listenerslist[i].objectsAdded(this, list);
    }	
    public void sendObjectsRemoved(List list)
    {
    	ListPartListener[] listenerslist = getListeners();
    	for (int i = 0; i < listenerslist.length; i++) 
    		listenerslist[i].objectsRemoved(this, list);
    }	            
    public List listeners = new ArrayList();
    public ListPartListener[] listenerslist = null;
	public void addListener(ListPartListener listener)
	{
		if(!isImmutable())
	    	synchronized (listeners) {
	    		listeners.add(listener);
	    		listenerslist = null;
	    	}
	}
	public void removeListener(ListPartListener listener)
	{
		if(!isImmutable())
	    	synchronized (listeners) {
	    		listeners.remove(listener);
	    		listenerslist = null;
	    	}
	}		    
	public void forceAddListener(ListPartListener listener)
	{
    	synchronized (listeners) {
    		listeners.add(listener);
    		listenerslist = null;
    	}
	}
	public void forceRemoveListener(ListPartListener listener)
	{
    	synchronized (listeners) {
    		listeners.remove(listener);
    		listenerslist = null;
    	}	
	}		    	

}