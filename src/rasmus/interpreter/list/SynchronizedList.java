/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


public class SynchronizedList implements List {

	private List list;
	private ListPartListener listener;
	private ListPart rlist;
	
	public SynchronizedList(ListPart rlist)
	{
		this.rlist = rlist;
		list = Collections.synchronizedList(new ArrayList());		
		list.addAll(rlist.getObjects());
		listener = new ListPartListener()
		{

			public void objectAdded(ListPart source, Object object) {
				list.add(object);
			}

			public void objectRemoved(ListPart source, Object object) {
				list.remove(object);				
			}

			public void objectsAdded(ListPart source, List objects) {
				list.addAll(objects);
				
			}

			public void objectsRemoved(ListPart source, List objects) {
				list.removeAll(objects);				
			}
			
		};
		rlist.addListener(listener);
	}
	
	public void close()
	{
		if(rlist == null) return;
		rlist.removeListener(listener);
		rlist = null;
		list.clear();
	}
	
	public int size() {
		return list.size();
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

	public boolean contains(Object arg0) {
		return list.contains(arg0);
	}

	public Iterator iterator() {
		return list.iterator();
	}

	public Object[] toArray() {
		return list.toArray();
	}

	public Object[] toArray(Object[] arg0) {
		return list.toArray(arg0);
	}

	public boolean add(Object arg0) {
		return false;
	}

	public boolean remove(Object arg0) {
		return false;
	}

	public boolean containsAll(Collection arg0) {
		return list.containsAll(arg0);
	}

	public boolean addAll(Collection arg0) {
		return false;
	}

	public boolean addAll(int arg0, Collection arg1) {
		return false;
	}

	public boolean removeAll(Collection arg0) {
		return false;
	}

	public boolean retainAll(Collection arg0) {
		return false;
	}

	public void clear() {
		close();
	}

	public Object get(int arg0) {
		return list.get(arg0);
	}

	public Object set(int arg0, Object arg1) {
		return null;
	}

	public void add(int arg0, Object arg1) {
	}

	public Object remove(int arg0) {
		return null;
	}

	public int indexOf(Object arg0) {
		return list.indexOf(arg0);
	}

	public int lastIndexOf(Object arg0) {
		return list.lastIndexOf(arg0);
	}

	public ListIterator listIterator() {
		return list.listIterator();
	}

	public ListIterator listIterator(int arg0) {
		return list.listIterator(arg0);
	}

	public List subList(int arg0, int arg1) {
		return list.subList(arg0, arg1);
	}

}
