/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.list;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import rasmus.interpreter.Variable;

public class ObjectsPart extends ListPart {
	
	public static Variable asVariable(Object value)
	{
		Variable variable = new Variable();		
		ObjectsPart robjects = ((ObjectsPart)variable.get(ObjectsPart.class));
		robjects.setImmutable(true);
		robjects.addObject(value);
		return variable;
	}
	
	public static List asList(Variable variable)
	{
		return ((ObjectsPart)variable.get(ObjectsPart.class)).getObjects();
	}
	
	public static SynchronizedList synchronizedList(Variable variable)
	{
		return ((ObjectsPart)variable.get(ObjectsPart.class)).synchronizedList();
	}
	
	public static Object[] asObjects(Variable variable)
	{
		return ((ObjectsPart)variable.get(ObjectsPart.class)).getObjects().toArray();
	}		
	public static String toString(Variable variable)
	{
		Object[] objects = asObjects(variable);
		Arrays.sort(objects);
		
		if(objects.length == 0) return "";		
		String curstring = objects[0].toString();			
		for (int i = 1; i < objects.length; i++) {
			String s = objects[i].toString();
			if(s.compareTo(curstring) > 0) curstring = s;
		}
		return curstring;
	}
	public static Object getProperty(Variable variable, String property)
	{
		Object[] objects = asObjects(variable);
		for (int i = 0; i < objects.length; i++) {
			if(objects[i] instanceof Map)
			{
				return ((Map)objects[i]).get(property);
			}			
		}		
		return null;
	}
	
	public static ObjectsPart getInstance(Variable variable)
	{
		return (ObjectsPart)variable.get(ObjectsPart.class);
	}	
	
}
