/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.math;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rasmus.interpreter.Variable;
import rasmus.interpreter.VariablePartAdapter;

public final class DoublePart extends VariablePartAdapter implements DoublePartListener {

	private double value = 0;
	
	private boolean immutable = false;	
	
	public boolean isImmutable()
	{
		return immutable;
	}/*
	public void setImmutable(boolean value)
	{
		immutable = value;
		/*
		if(value == true) 
		{
			if(listeners.size() == 0) listeners = null;
		}
		else
		{
			if(listeners == null) listeners = new RListenerListImpl();
		}*//*
	}	*/
	
	public static Variable asVariable(double value)
	{
		//RVariable variable = new RVariable(rfloat);		
		DoublePart rfloat = new DoublePart(true); // ((RDouble)variable.get(RDouble.class));
		//rfloat.setImmutable(true);
		//rfloat.immutable = true;
		//rfloat.listeners = null;
		//rfloat.dependvar = null;
		rfloat.value = value;
		//rfloat.addFloat(value);
		return new Variable(rfloat);
	}
	
	public DoublePart()
	{
	    dependvar = new ArrayList();
	    listeners = new ArrayList();
	}
	public DoublePart(boolean immutable)
	{
		if(immutable)
		{
			this.immutable = true;
		}
		else
		{
		    dependvar = new ArrayList();
		    listeners = new ArrayList();
			
		}
		
	}
	
	public static double asDouble(Variable variable)
	{
		if(variable == null) return 0;
		return ((DoublePart)variable.get(DoublePart.class)).getDouble();
	}
	
	public static DoublePart getInstance(Variable variable)
	{
		return (DoublePart)variable.get(DoublePart.class);
	}	
	private void addFloat(double value)
	{
		double beforevalue = this.value;
		this.value += value;
		sendValueChanged(beforevalue, this.value);
	}
	/*
	private void removeFloat(double value)
	{
		double beforevalue = value;
		this.value -= value;
		sendValueChanged(beforevalue, value);
	}	*/
	public double getDouble()
	{
        return value;		
	}
	
	public void add(Variable variable)
	{		
		if(dependvar == null) return;
		double beforevalue = value;    	
    	DoublePart source = ((DoublePart)(variable.get(DoublePart.class)));
		value += source.getDouble();
		sendValueChanged(beforevalue, value);
		if(!immutable) source.addListener(this);
		dependvar.add(source);
	}
    public void remove(Variable variable)	
	{
    	if(dependvar == null) return;
    	double beforevalue = value;
    	DoublePart source = ((DoublePart)(variable.get(DoublePart.class)));
		value -= source.getDouble();
		sendValueChanged(beforevalue, value);
		if(!immutable) source.removeListener(this);
		dependvar.remove(source);
	}	
    ArrayList dependvar;
    public void clear()
    {
    	if(dependvar == null) return;
    	Iterator di = dependvar.iterator();
     	while (di.hasNext()) {
    		((DoublePart) di.next()).removeListener(this);			
    	}	 	     	
     	
     	double beforevalue = value;
    	value = 0;
		sendValueChanged(beforevalue, value);
    }

    
    //RDoubleListener listener = null;
    /*
    public RDoubleListener getListener()
    {
    	synchronized (this) {
    		if(listener == null)
    		{
    			listener = new RDoubleListener()
    			{
    			public void valueChanged(RDouble source, double before, double after) {
    				addFloat(after - before);
    			}
    			};
    		}
    		return listener;
		}
    }*/
    // EVENT / LISTENER CODE
  /*  
    RDoubleListener listener = new RDoubleListener()
	{
	public void valueChanged(RDouble source, double before, double after) {
		addFloat(after - before);
	}
	};
*/	
	public void valueChanged(DoublePart source, double before, double after) {
		addFloat(after - before);
	}
	
    public void sendValueChanged(double before, double after)
    {
    	if(!immutable) 
    	synchronized (listeners) {
    	Iterator iterator = listeners.iterator();
    	while (iterator.hasNext()) 
			((DoublePartListener) iterator.next()).valueChanged(this, before, after);
		}
    }	
	public List listeners;
	public void addListener(DoublePartListener listener)
	{		
		if(!immutable)
	    	synchronized (listeners) {			
		listeners.add(listener);
	    	}
	}
	public void removeListener(DoublePartListener listener)
	{
		if(!immutable)
	    	synchronized (listeners) {
			listeners.remove(listener);
	    	}
	}		

}
