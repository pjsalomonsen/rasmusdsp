/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.math;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class FloatMulitplierInstance implements UnitInstancePart, DoublePartListener, Commitable 
{
	int operator;
	Variable answer = null;
	ArrayList elements = new ArrayList();
	ArrayList variables = new ArrayList();
	Variable returnvar;
	public final double toDouble(boolean val)
	{
		if(val) return 1; else return 0;
	}
	boolean calcok = false;
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	
	public void calc()
	{
		if(!calcok) return;
		//commit();
		namespace.addToCommitStack(this);
	}
	
	public void commit() {
		if(returnvar == null) return;
		if(answer != null) returnvar.remove(answer);		
		Iterator itr = elements.iterator();
		double calc;
		
		//System.out.println("Calculate: " + operator);
		
		/*
		 System.out.print(operator + ": ");
		 while (itr.hasNext())
		 {
		 System.out.print(((RDouble) itr.next()).getDouble() + " ");
		 }
		 itr = elements.iterator(); */
		
		if(operator == 50) 
			calc = Math.random();
		else
		{
			if(itr.hasNext())
			{
				
				calc = ((DoublePart) itr.next()).getDouble();
				
				
				
				if(operator < 50)
				{
					
					if(itr.hasNext())
					{
						
						if(operator == 0) { while (itr.hasNext()) { calc *= ((DoublePart) itr.next()).getDouble(); }}
						else if(operator == 1) { while (itr.hasNext()) {
							double  dval = ((DoublePart) itr.next()).getDouble();
							if(dval != 0) calc /= dval; 
						}}
						else if(operator == 2) { while (itr.hasNext()) { calc -= ((DoublePart) itr.next()).getDouble(); }}
						else if(operator == 3) { while (itr.hasNext()) { calc += ((DoublePart) itr.next()).getDouble(); }}
						else if(operator == 4) { while (itr.hasNext()) { calc %= ((DoublePart) itr.next()).getDouble(); }}
						else if(operator == 5) { while (itr.hasNext()) { calc = toDouble(calc > ((DoublePart) itr.next()).getDouble()); }}
						else if(operator == 6) { while (itr.hasNext()) { calc = toDouble(calc < ((DoublePart) itr.next()).getDouble()); }}
						else if(operator == 7) { while (itr.hasNext()) { calc = toDouble(calc >= ((DoublePart) itr.next()).getDouble()); }}
						else if(operator == 8) { while (itr.hasNext()) { calc = toDouble(calc <= ((DoublePart) itr.next()).getDouble()); }}
						else if(operator == 9) { while (itr.hasNext()) { calc = toDouble(calc != ((DoublePart) itr.next()).getDouble()); }}
						else if(operator == 10) { while (itr.hasNext()) {
							double diff = calc - ((DoublePart) itr.next()).getDouble();
							if(diff < 0) diff = -diff;
							calc = 
								toDouble(diff < 0.0001); }}
						
					}
					else
					{
						if(operator == 2) calc = -calc;
					}
					
				}
				else
					if(operator < 200)
					{
						
						if(operator == 100) calc = Math.abs(calc);
						else if(operator == 101) calc = Math.acos(calc);
						else if(operator == 102) calc = Math.asin(calc);
						else if(operator == 103) calc = Math.atan(calc);
						else if(operator == 104) calc = Math.ceil(calc);
						else if(operator == 105) calc = Math.cos(calc);
						else if(operator == 106) calc = Math.exp(calc);
						else if(operator == 107) calc = Math.floor(calc);
						else if(operator == 108) calc = Math.log(calc);
						else if(operator == 109) calc = Math.rint(calc);
						else if(operator == 110) calc = Math.round(calc);
						else if(operator == 111) calc = Math.sin(calc);
						else if(operator == 112) calc = Math.sqrt(calc);
						else if(operator == 113) calc = Math.tan(calc);
						else if(operator == 114) calc = Math.toDegrees(calc);
						else if(operator == 115) calc = Math.toRadians(calc);
						else if(operator == 116)
						{
							if(calc > 0) calc = 1; else if(calc < 0) calc = -1; else calc = 0;
						}
						
					}
					else			
					{
						if(itr.hasNext())
						{
							double calc2 = ((DoublePart) itr.next()).getDouble();
							
							if(operator == 200) calc = Math.IEEEremainder(calc, calc2);
							if(operator == 201) calc = Math.atan2(calc, calc2);
							if(operator == 202) calc = Math.max(calc, calc2);
							if(operator == 203) calc = Math.min(calc, calc2);
							if(operator == 204) calc = Math.pow(calc, calc2);
						}
					}
				
				
			}
			else calc = 0;
		}
		
		//System.out.println(" --> " + calc);
		//containsaudioevents = false;
		
		if(!containsaudioevents)
		{
			answer = DoublePart.asVariable(calc);		
			returnvar.add(answer);
			// System.out.println("adding " + operator + " " + calc);
		}
		else
		{
			answer = null;
			//System.out.println("skiping " + operator + " " + calc);
		}
	}
	
	boolean containsaudioevents = false;
	LinkedList audiosources = new LinkedList();
	public void listCalc(ListPart source)
	{
		if(source.getObjects().size() == 0)
		{
			if(audiosources.contains(source))
			{
				audiosources.remove(source);
				if(containsaudioevents)
					if(audiosources.size() == 0)
					{					
						containsaudioevents = false;
						calc();
					}
				
			}
		}
		else
		{
			if(!audiosources.contains(source))
			{
				audiosources.add(source);
				if(!containsaudioevents)
				{
					containsaudioevents = true;
					calc();
				}
			}
		}
	}
	
	ListPartListener listener = new ListPartListener()
	{
		public void objectAdded(ListPart source, Object object) {
			listCalc(source);
		}
		public void objectRemoved(ListPart source, Object object) {
			listCalc(source);
		}
		public void objectsAdded(ListPart source, List objects) {
			listCalc(source);
		}
		public void objectsRemoved(ListPart source, List objects) {
			listCalc(source);
		}
	};
	
	
	NameSpace namespace;
	public FloatMulitplierInstance(Parameters parameters, int operator)
	{
		
		namespace = parameters.getNameSpace();
		this.operator = operator;
		returnvar = parameters.getParameter("output");
		
		/*
		 Iterator keys = parameters.keySet().iterator();
		 while (keys.hasNext()) {
		 String keyname = (String) keys.next();
		 if(!keyname.equals("0"))
		 if(!keyname.equals("output"))
		 {
		 RDouble var = RDouble.getInstance((RVariable)parameters.get(keyname));
		 var.addListener(this);
		 elements.add(var);				
		 }
		 }*/
		
		int ii = 0;
		Variable var = parameters.getParameter("input");
		if(var == null)
		{
			ii = 1;
			var = parameters.getParameter(ii);
		}
		
		while(var != null)
		{
			AudioEvents events = AudioEvents.getInstance((Variable)var);
			events.addListener(listener);
			listCalc(events);
			
			DoublePart dvar = DoublePart.getInstance((Variable)var);
			variables.add(var);
			elements.add(dvar);			
			dvar.addListener(this);
			ii++;
			var = parameters.getParameter(ii);				
		}		
		
		calcok = true;
		calc();
	}
	
	
	
	public void close() {
		Iterator itr = variables.iterator();
		while (itr.hasNext()) {
			
			Variable var = ((Variable) itr.next());
			AudioEvents events = AudioEvents.getInstance((Variable)var);
			events.removeListener(listener);		
		}
		
		itr = elements.iterator();
		while (itr.hasNext()) {
			((DoublePart) itr.next()).removeListener(this);		
		}
		elements.clear();
		variables.clear();
		if(returnvar == null) return;
		if(answer != null) returnvar.remove(answer);
		answer = null;
	}
	
	public void valueChanged(DoublePart source, double before, double after) {		
		calc();		
	}
	
	
	
	
}

public class DoubleOperator implements UnitFactory {
	
	int operator;
	public DoubleOperator(int operator)
	{
		this.operator = operator;
	}
	public UnitInstancePart newInstance(Parameters parameters) {
		return new FloatMulitplierInstance(parameters, operator);
	}
}
