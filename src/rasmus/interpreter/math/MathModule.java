/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.math;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.ext.Module;
import rasmus.interpreter.ext.ModuleFactory;
import rasmus.interpreter.unit.UnitInstancePart;

public class MathModule extends ModuleFactory {
	
	class ModuleInstance extends Module
	{		
		public ModuleInstance(NameSpace namespace)
		{
			setNameSpace(namespace);
			
			// Boolean in numbers
			//    true  = (1, or number higher than 0)		
			//    false = 0
			// 
			// Boolean Operators:
			//    x && y <=> x and y  <=> x*y
			//    x || y <=> x or y   <=> x+y  <=> (x+y!=0)		
			//    !x     <=> not x    <=> x=0
			//               x xor y  <=> (x=0)!=(y=0) 			
			//    normalized x        <0> x!=0   // convert true => 1, and false => 0
			
			add("+", new Sum());            // General SUM operator
			add("*", new DoubleOperator(0));
			add("/", new DoubleOperator(1));
			add("-", new DoubleOperator(2));
			//	add("+", new FloatOperator(3)); // Build in Feature in RFloat
			add("mod", new DoubleOperator(4)); 
			add(">", new DoubleOperator(5));
			add("<", new DoubleOperator(6));
			add(">=", new DoubleOperator(7));
			add("<=", new DoubleOperator(8));
			add("!=", new DoubleOperator(9));
			add("=", new DoubleOperator(10));
			add("?", new CondOperator());
			add("if", new CondOperator());
			
			add("random", new DoubleOperator(50));
			add("abs", new DoubleOperator(100));
			add("acos", new DoubleOperator(101));
			add("asin", new DoubleOperator(102));
			add("atan", new DoubleOperator(103));
			add("ceil", new DoubleOperator(104));
			
			add("nativecos", new DoubleOperator(105));
			add("fastcos", new DoubleOperator(105));
			add("cos", new DoubleOperator(105));
			
			add("exp", new DoubleOperator(106));
			add("floor", new DoubleOperator(107));
			add("log", new DoubleOperator(108));
			add("rint", new DoubleOperator(109));
			add("round", new DoubleOperator(110));
			
			add("nativesin", new DoubleOperator(111));
			add("fastsin", new DoubleOperator(111));
			add("sin", new DoubleOperator(111));
			
			add("sqrt", new DoubleOperator(112));
			
			add("nativetan", new DoubleOperator(113));
			add("fasttan", new DoubleOperator(113));			
			add("tan", new DoubleOperator(113));
			
			add("toDegrees", new DoubleOperator(114));
			add("toRadians", new DoubleOperator(115));			
			add("sgn", new DoubleOperator(116));
			add("IEEEremainder", new DoubleOperator(200));
			add("atan2", new DoubleOperator(201));
			add("max", new DoubleOperator(202));
			add("min", new DoubleOperator(203));
			add("pow", new DoubleOperator(204));
			add("^", new DoubleOperator(204));
			
			add("PI", Math.PI);
			add("E", Math.E);
			
			add("true", 1);
			add("false", 0);
			
			// Musical Functions
			add("scale", new Scale());
			
		}
		
	}
	
	public UnitInstancePart newInstance(NameSpace namespace) {
		return new ModuleInstance(namespace);
	}
	
}
