/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.math;

import java.util.ArrayList;
import java.util.Iterator;

import rasmus.interpreter.Variable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class SumInstance implements UnitInstancePart
{
	ArrayList elements = new ArrayList();
	Variable returnvar;
	
	public SumInstance(Parameters parameters)
	{
		returnvar = parameters.getParameterWithDefault("output");
		
		Iterator keys = parameters.getParameters().keySet().iterator();
		while (keys.hasNext()) {
			String keyname = (String) keys.next();
			if(!keyname.equals("0"))
				if(!keyname.equals("output"))
				{
					Variable var = parameters.getParameter(keyname);
					elements.add(var);		
					returnvar.add(var);
				}
		}		
	}
	public void close() {
		Iterator itr = elements.iterator();
		while (itr.hasNext()) {
			returnvar.remove(((Variable) itr.next()));		
		}
		elements.clear();        
		if(returnvar == null) return;
	}
}

public class Sum implements UnitFactory {
	public UnitInstancePart newInstance(Parameters parameters) {
		return new SumInstance(parameters);
	}
}
