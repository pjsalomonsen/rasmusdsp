/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.math;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;

import rasmus.interpreter.Variable;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class ScaleInstance implements UnitInstancePart
{
	ArrayList elements = new ArrayList();
	
	double[] scale_elements = null;
	Variable returnvar;
	Variable inputvar;
	
	Variable answer = null;
	
	public void calcInput()
	{		
		if(answer != null)
		{
			returnvar.remove(answer);
			answer = null;
		}		
		
		if(scale_elements == null) return;
		if(scale_elements.length == 0) return;
		
		int note = (int)DoublePart.asDouble(inputvar);		
		int octavlen = scale_elements.length;
		
		int oct  = note / octavlen;
		int base = note - (oct*octavlen);
		if(base < 0)
		{
			oct = oct - 1;
			base = base + octavlen;
		}
		
		double ratio = Math.pow(scale_elements[scale_elements.length - 1], oct);		
		if(base > 0)
		{
			ratio *= scale_elements[base - 1];
		}
		
		answer = DoublePart.asVariable(ratio);
		returnvar.add(answer);
	}
	public void calcScale()
	{
		
		scale_elements = new double[elements.size()];
		int i = 0;
		Iterator itr = elements.iterator();
		while (itr.hasNext()) {       	
			scale_elements[i] = DoublePart.asDouble(  ((Variable) itr.next()));
			i++;
		}
		
		calcInput();
	}
	
	DoublePartListener scale_listener = new DoublePartListener()
	{
		public void valueChanged(DoublePart source, double before, double after) {
			calcScale();			
		}
	};
	
	DoublePartListener input_listener = new DoublePartListener()
	{
		public void valueChanged(DoublePart source, double before, double after) {
			calcInput();			
		}
	};	
	
	public ScaleInstance(Parameters parameters)
	{
		returnvar = parameters.getParameterWithDefault("output");		
		inputvar = parameters.getParameter(1, "input"); 
		if(inputvar == null)
		{
			inputvar = DoublePart.asVariable(0);
		}
		DoublePart.getInstance(inputvar).addListener(input_listener);
		
		TreeMap sortedmap = new TreeMap(new Comparator()
				{
			public int compare(Object arg0, Object arg1) {
				if(arg0 == null) return 0;
				if(arg1 == null) return 0;
				String s1 = (String)arg0;
				String s2 = (String)arg1;
				try
				{							
					return Integer.parseInt(s1) - Integer.parseInt(s2);
				}
				catch(Throwable t)
				{
					return s1.compareTo(s2);
				}
			}
			
				});
		sortedmap.putAll(parameters.getParameters());
		
		Iterator keys = sortedmap.keySet().iterator();				
		while (keys.hasNext()) {
			String keyname = (String) keys.next();
			if(!keyname.equals("0"))
				if(!keyname.equals("1"))
					if(!keyname.equals("input"))
						if(!keyname.equals("output"))
						{
							Variable var = parameters.getParameter(keyname);
							DoublePart.getInstance(var).addListener(scale_listener);
							elements.add(var);						
						}
		}		
		
		calcScale();
	}
	public void close() {
		
		DoublePart.getInstance(inputvar).removeListener(input_listener);
		
		Iterator itr = elements.iterator();
		while (itr.hasNext()) {       	
			DoublePart.getInstance(  ((Variable) itr.next()) ).removeListener(scale_listener);        	
		}
		elements.clear(); 
		
		if(answer != null)
		{
			returnvar.remove(answer);
			answer = null;
		}		
		returnvar = null; 
		inputvar = null;
		
	}
}

public class Scale implements UnitFactory {
	public UnitInstancePart newInstance(Parameters parameters) {
		return new ScaleInstance(parameters);
	}
}
