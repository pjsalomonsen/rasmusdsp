/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.math;

import java.util.List;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class CondMulitplierInstance implements UnitInstancePart, DoublePartListener
{
	
	Variable answer = null;
	
	Variable testvar;
	Variable truevar;
	Variable falsevar;
	
	Variable returnvar;
	public final double toDouble(boolean val)
	{
		if(val) return 1; else return 0;
	}
	public void calc()
	{
		if(returnvar == null) return;
		if(answer != null) returnvar.remove(answer);	
		answer = null;
		
		if(DoublePart.asDouble(testvar) != 0)
			answer = truevar;
		else
			answer = falsevar;
		
		
		//if(AudioEvents.getInstance(testvar).getObjects().size() !=0 )
		if(answer != null)
			returnvar.add(answer);
		
	}
	
	ListPartListener listener = new ListPartListener()
	{
		public void objectAdded(ListPart source, Object object) {
			calc();
		}
		public void objectRemoved(ListPart source, Object object) {
			calc();
		}
		public void objectsAdded(ListPart source, List objects) {
			calc();
		}
		public void objectsRemoved(ListPart source, List objects) {
			calc();
		}
	};
	
	public CondMulitplierInstance(Parameters parameters)
	{
		
		returnvar = parameters.getParameter(0, "output");
		testvar = parameters.getParameterWithDefault(1, "input");
		truevar = parameters.getParameter(2, "true");
		falsevar = parameters.getParameter(3, "false");
		
		//if(truevar == null) truevar = new RVariable();
		//if(falsevar == null) falsevar = new RVariable();
		
		DoublePart.getInstance(testvar).addListener(this);
		AudioEvents.getInstance(testvar).addListener(listener);
		
		calc();
	}
	
	
	
	public void close() {
		
		AudioEvents.getInstance(testvar).removeListener(listener);
		DoublePart.getInstance(testvar).removeListener(this);		
		if(returnvar == null) return;
		if(answer != null) returnvar.remove(answer);
		answer = null;
	}
	
	public void valueChanged(DoublePart source, double before, double after) {
		calc();		
	}
}

public class CondOperator implements UnitFactory {
	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new CondMulitplierInstance(parameters);
	}
}