/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.ui.RegisterConstant;
import rasmus.interpreter.ui.RegisterFunction;
import rasmus.interpreter.ui.RegisterGroup;
import rasmus.interpreter.ui.RegisterUnit;

public class GroupMenus extends JMenu implements ListPartListener, Commitable
{
	private static final long serialVersionUID = 1L;
	ObjectsPart group;
	GroupMenuListener listener;
	NameSpace namespace;
	public GroupMenus(NameSpace namespace, GroupMenuListener listener)
	{
		super("Insert");

		this.namespace = namespace;

		this.listener = listener;
		group = ObjectsPart.getInstance(namespace.get("groups"));
		commit();
		group.addListener(this);
	}
	
	public GroupMenus(NameSpace namespace, RegisterGroup.Record record, GroupMenuListener listener)
	{
		super(record.description);		
		this.namespace = namespace;
		this.listener = listener;
		group = ObjectsPart.getInstance(record.childs);
		commit();
		group.addListener(this);
	}	
	
	public void close()
	{
		group.removeListener(this);
		
		Component[] comps = getComponents();
		for (int i = 0; i < comps.length; i++) {
			if(comps[i] instanceof GroupMenus)
			{
				((GroupMenus)comps[i]).close();
			}
		}
		removeAll();		
	}

	public void objectAdded(ListPart source, Object object) {
		namespace.addToCommitStack(this);
	}
	public void objectRemoved(ListPart source, Object object) {
		namespace.addToCommitStack(this);
	}
	public void objectsAdded(ListPart source, List objects) {
		namespace.addToCommitStack(this);
	}
	public void objectsRemoved(ListPart source, List objects) {
		namespace.addToCommitStack(this);
	}

	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit() {
		Component[] comps = getComponents();
		for (int i = 0; i < comps.length; i++) {
			if(comps[i] instanceof GroupMenus)
			{
				((GroupMenus)comps[i]).close();
			}
		}
		removeAll();
		
		objects = group.getObjects().toArray();
		
		Arrays.sort(objects,new Comparator()
				{
					public int compare(Object o1, Object o2) {
						
						String s1 = "";
						String s2 = "";
						if(o1 instanceof RegisterGroup.Record) s1 = "1" + ((RegisterGroup.Record)o1).description;
						if(o2 instanceof RegisterGroup.Record) s2 = "1" + ((RegisterGroup.Record)o2).description;							
						if(o1 instanceof RegisterUnit.Record)
						{
							if(((RegisterUnit.Record)o1).unit instanceof RegisterConstant)
								s1 = "2" + ((RegisterUnit.Record)o1).name;
							else
								s1 = "3" + ((RegisterUnit.Record)o1).name;
						}
						if(o2 instanceof RegisterUnit.Record)
						{
							if(((RegisterUnit.Record)o2).unit instanceof RegisterConstant)
								s2 = "2" + ((RegisterUnit.Record)o2).name;
							else
								s2 = "3" + ((RegisterUnit.Record)o2).name;
						}
						s1 = s1.toLowerCase();
						s2 = s2.toLowerCase();
						return s1.compareTo(s2);
					}
				});

		for (int i = 0; i < objects.length; i++) {
			Object object = objects[i];
			if(object instanceof RegisterGroup.Record)
			{				
				add(new GroupMenus(namespace, (RegisterGroup.Record)object, listener));				
			}
			else
			if(object instanceof RegisterUnit.Record)
			{
				RegisterUnit.Record record = (RegisterUnit.Record)object;
				add(new InterpreterGroupMenuitem(this, record, listener));
			}
		}
		
	}
	
	Object[] objects ;
	double longestname = 0;
	String longestnametxt = null;
	

	static BufferedImage dummy_image = new BufferedImage(32,32, BufferedImage.TYPE_INT_RGB);
	
	public String getLongestName()
	{	
		if(longestnametxt == null)
		{
			
			Graphics g = dummy_image.getGraphics();
			Font ofont = getFont().deriveFont(Font.BOLD);
			g.setFont(ofont.deriveFont(Font.BOLD));			
			FontMetrics fm = g.getFontMetrics();
			
			for (int i = 0; i < objects.length; i++) {
				Object object = objects[i];
				if(object instanceof RegisterUnit.Record)
				{
					RegisterUnit.Record record = (RegisterUnit.Record)object;
					String namecaption = record.name;
					if(record.unit instanceof RegisterConstant)
					{
						namecaption = namecaption.toUpperCase();
					}
					if(record.unit instanceof RegisterFunction)
					{
						namecaption = namecaption.toLowerCase() + "()";
					}					
					double w = fm.getStringBounds(namecaption, g).getWidth();					
					if(w > longestname)
					{
						longestname = w;
						longestnametxt = namecaption;
					}
				}
			}		
		}
		return longestnametxt;
	}	
	
	public double getLongestName(Graphics g)
	{	
		if(longestname == 0)
		{
			getLongestName();
			/*
			FontMetrics fm = g.getFontMetrics();
			for (int i = 0; i < objects.length; i++) {
				Object object = objects[i];
				if(object instanceof RegisterUnit.Record)
				{
					RegisterUnit.Record record = (RegisterUnit.Record)object;
					String namecaption = record.name;
					if(record.unit instanceof RegisterConstant)
					{
						namecaption = namecaption.toUpperCase();
					}
					if(record.unit instanceof RegisterFunction)
					{
						namecaption = namecaption.toLowerCase() + "()";
					}					
					double w = fm.getStringBounds(namecaption, g).getWidth();
					if(w > longestname) longestname = w;
				}
			}*/
		}
		return longestname;
	}
}

class InterpreterGroupMenuitem extends JMenuItem 
{
	private static final long serialVersionUID = 1L;
	String namecaption;
	String description;
	RegisterUnit.Record record;
	GroupMenus parentgroup;
	GroupMenuListener listener;
	
	public InterpreterGroupMenuitem(GroupMenus parentgroup, RegisterUnit.Record record, GroupMenuListener listener)
	{
		super();
		
		this.listener = listener;
		
		this.parentgroup = parentgroup;
		
		addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						InterpreterGroupMenuitem.this.listener.actionPerformed(InterpreterGroupMenuitem.this.record); 
					}
				});
		
		namecaption = record.name;
		description = record.description;
		this.record = record;
		
		if(record.unit instanceof RegisterConstant)
		{
			namecaption = namecaption.toUpperCase();
		}
		if(record.unit instanceof RegisterFunction)
		{
			namecaption = namecaption.toLowerCase() + "()";
		}
		setText(parentgroup.getLongestName() + " - " + record.description+ "  ");
	}
	
	Color textcolor = ((ColorUIResource)UIManager.getColor("MenuItem.foreground"));
	Color textselcolor = ((ColorUIResource)UIManager.getColor("MenuItem.selectionForeground"));
	
	Color textfieldcolor = ((ColorUIResource)UIManager.getColor("TextField.background"));
	Color panelcolor = ((ColorUIResource)UIManager.getColor("Panel.background"));
	static Font boldfont = null;
	static Font plainfont = null;
	
	static Rectangle2D armed_empty_space = null;
	Rectangle2D bounds_namecaption = null;
	double longaestnamewidth = 0;
	
	public void paint(Graphics g) {
				
		String text = getText();
		setText("");															
		super.paint(g);
		Font ofont = g.getFont();
		if(boldfont == null) boldfont = ofont.deriveFont(Font.BOLD);
		g.setFont(boldfont);

		if(longaestnamewidth == 0) longaestnamewidth = parentgroup.getLongestName(g);
		//double longaestnamewidth = parentgroup.getLongestName(g);

		
		
		
		if(!isArmed())
		{
			if(armed_empty_space == null) armed_empty_space = g.getFontMetrics().getStringBounds(" ", g);;
			Rectangle2D sr = armed_empty_space; //g.getFontMetrics().getStringBounds(" ", g);
			Color color = g.getColor();
			g.setColor(panelcolor);
			g.fillRect(0,0, 5 + (int)(longaestnamewidth + sr.getWidth()), getHeight());
			g.setColor(textfieldcolor);
			g.fillRect(5 + (int)(longaestnamewidth + sr.getWidth()), 0, getWidth(), getHeight());
			g.setColor(color);
		}
		
		if(isArmed())		
			g.setColor(textselcolor);
		else
			g.setColor(textcolor);

		if(bounds_namecaption == null) bounds_namecaption = g.getFontMetrics().getStringBounds(namecaption, g);
		Rectangle2D r = bounds_namecaption; 
		g.drawString(namecaption,5,(int)r.getHeight());
		if(plainfont == null) plainfont = ofont.deriveFont(Font.PLAIN);
		g.setFont(plainfont);
		
		g.drawString("   " + description ,5 + (int)longaestnamewidth,(int)r.getHeight());		
		g.setFont(ofont);
		setText(text);
		
	}	
}
