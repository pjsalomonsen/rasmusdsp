/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.ui;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

public class RegisterUnit implements UnitFactory {
	
	public class Record
	{
		public RegisterUnit unit = RegisterUnit.this;
		public String description;
		public String name;
	}
	public class UnitInstance extends UnitInstanceAdapter
	{
		Variable output;
		Variable answer = null;   
		
		Variable name;
		Variable description;

		public UnitInstance(Parameters parameters)
		{
			output = parameters.getParameterWithDefault("output");
			name = parameters.getParameterWithDefault(1, "name");
			description = parameters.getParameterWithDefault(2, "description");			
			calc();
			ObjectsPart.getInstance(description).addListener(this);
			ObjectsPart.getInstance(name).addListener(this);
		}
		public void close() {		
			if(answer != null)
			{
				output.remove(answer);
				answer = null;
			}		
			ObjectsPart.getInstance(description).removeListener(this);
			ObjectsPart.getInstance(name).removeListener(this);
		}
		public void calc() {
			if(answer != null)
			{
				output.remove(answer);
				answer = null;
			}
			Record record = new Record();
			record.name = ObjectsPart.toString(name);
			record.description = ObjectsPart.toString(description);
			answer = ObjectsPart.asVariable(record);		
			output.add(answer);		
		}

	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new UnitInstance(parameters);
	}
}
