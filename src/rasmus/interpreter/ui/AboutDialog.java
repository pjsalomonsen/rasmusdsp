/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

public class AboutDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	String credit_text;
	public AboutDialog(Frame frame)
	{
		super(frame);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = getClass().getResourceAsStream("/rasmus/interpreter/ui/AboutDialog.txt");
		try
		{
			try
			{
				byte[] buffer = new byte[1024];
				int len = 0;
				while((len = is.read(buffer)) != -1)
				{
					baos.write(buffer,0, len);
				}
			}
			finally
			{
				is.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try {
			credit_text = new String(baos.toByteArray(), "LATIN1");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
		
		
		setModal(true);
		
		setTitle("About RasmusDSP");
		
		JPanel panel = new JPanel();
		
		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		panel.registerKeyboardAction(new ActionListener()
				{
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				}
				, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		
		
		panel.setLayout(new BorderLayout());
		setContentPane(panel);

		JPanel leftpanel = new JPanel();
		JPanel rightpanel = new JPanel();
		panel.add(leftpanel, BorderLayout.WEST);
		panel.add(rightpanel, BorderLayout.EAST);
		
		leftpanel.setBackground(new Color(46,147,73));		
		leftpanel.add(new JLabel(new javax.swing.ImageIcon(getClass().getResource("/rasmus/rasmusdsp_logo.PNG"))));
		
		String about_text =
		"<html><body>"+
		"<h2>RasmusDSP version 0.2</h2>" +
		"<b>http://rasmusdsp.sourceforge.net</b><br>" +
		"<br><br><br>" +
		"Copyright � 2006-2007<br>" +
		"The RasmusDSP developers.<br>"+
		"</html></body>";
					
		JLabel textarea = new JLabel (about_text);
		textarea.setFont(textarea.getFont().deriveFont(11f).deriveFont(Font.PLAIN));
		//textarea.setEditable(false);
		textarea.setBorder(BorderFactory.createEmptyBorder(0,0,0,60));
		rightpanel.setLayout(new BorderLayout());
		rightpanel.setBackground(Color.WHITE);		
		rightpanel.add(textarea);
		
		JPanel buttonpanel = new JPanel();
		buttonpanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		JPanel creditpanel = new JPanel();
		creditpanel.setOpaque(false);
		buttonpanel.setOpaque(false);
		rightpanel.add(creditpanel,BorderLayout.NORTH);
		rightpanel.add(buttonpanel,BorderLayout.SOUTH);
		
		creditpanel.add(textarea);

		JButton creditbutton = new JButton("Credits");
		creditbutton.addActionListener(new ActionListener()
				{
					JDialog dialog;
					public void actionPerformed(ActionEvent e) {
						dialog = new JDialog(AboutDialog.this);
						dialog.setTitle("Credits");
						dialog.setModal(true);
						dialog.setSize(640, 480);
						dialog.setResizable(false);
						
						Dimension size = dialog.getSize();
						Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();		
						dialog.setLocation(screenSize.width/2 - (size.width/2),
									screenSize.height/2 - (size.height/2));
						
						JPanel panel = new JPanel();
						
						KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
						panel.registerKeyboardAction(new ActionListener()
								{
									public void actionPerformed(ActionEvent e) {
										dialog.setVisible(false);
									}
								}
								, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
						
						
						panel.setLayout(new BorderLayout());
						dialog.setContentPane(panel);
												
						JTextArea credittext = new JTextArea(credit_text);
						credittext.setEditable(false);
						JPanel buttonpanel = new JPanel();
						buttonpanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
						
						panel.add(new JScrollPane(credittext), BorderLayout.CENTER);
						panel.add(buttonpanel, BorderLayout.SOUTH);
						
						JButton okbutton = new JButton("OK");
						okbutton.addActionListener(new ActionListener()
								{
									public void actionPerformed(ActionEvent e) {
										dialog.setVisible(false);
									}
								});
						buttonpanel.add(okbutton); 
						
						dialog.setVisible(true);
					}
				});		
		buttonpanel.add(creditbutton);
		JButton okbutton = new JButton("OK");
		okbutton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
		buttonpanel.add(okbutton); 
			   
		getRootPane().setDefaultButton(okbutton);
		
		setResizable(false);
		pack();
		
		Dimension size = getSize();		
		Dimension screenSize = frame.getSize();
		Point parent_loc = frame.getLocation();			
		setLocation(parent_loc.x + screenSize.width/2 - (size.width/2),
				    parent_loc.y + screenSize.height/2 - (size.height/2));
		
		
		
	}

}
