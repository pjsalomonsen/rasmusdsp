/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.ui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.event.DocumentEvent;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Element;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.undo.UndoableEdit;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.parser.ScriptTokenParser;

public class RSyntaxDocument extends DefaultStyledDocument {

	private static final long serialVersionUID = 1L;
	
	Element root;	
	
	// Styles
	SimpleAttributeSet style_normal = new SimpleAttributeSet();
	SimpleAttributeSet style_parameter = new SimpleAttributeSet();
	SimpleAttributeSet style_string = new SimpleAttributeSet();
	SimpleAttributeSet style_number = new SimpleAttributeSet();
	SimpleAttributeSet style_comment = new SimpleAttributeSet();
	SimpleAttributeSet style_symbol = new SimpleAttributeSet();
	SimpleAttributeSet style_keyword = new SimpleAttributeSet();
	SimpleAttributeSet style_keyword_variable = new SimpleAttributeSet();
	
	boolean doupdate = true;
	public void setText(String text)
	{
		try {
			//doupdate = false;
			remove(0, getLength());
			insertString(0, text, style_normal );
			/*
			int pos = 0;
			
			while(st.hasMoreTokens())
			{
				String token = st.nextToken();
				SimpleAttributeSet style;
				
				if(token.startsWith("/*") || token.startsWith("//"))
					style = style_comment;
				else
					style = style_normal;
				
				insertString(pos, text, style );
				pos += token.length();
			}			
			*/
			//doupdate = true;
		} catch (BadLocationException e) {
			e.printStackTrace();
		}		
	}
	
	public int[] extractFunctionCall(int offs) throws BadLocationException
	{
		int doclen = getLength();
		int[] pair = extractNormalToken(offs);
		if(pair == null) return null;
		
		int i = pair[0] + pair[1];
		
		// It is given that we are in normal one
		// because pair != null
		int status = ScriptTokenParser.STATUS_NORMAL;
		
		// find location of "(" if one exists
		while(true)
		{
			if(i >= doclen) break;			
			char cc = getText(i, 1).charAt(0);
			
			status = ScriptTokenParser.parseStatus(cc, status);
						
			if(status == ScriptTokenParser.STATUS_NORMAL)
			{				
				if(cc == '(')
				{
					break;
				}			
				if(cc != ' ')
				{
					// Other char was found
					// we are not working with function call
					return null;
				}
			}
			else
			{
				if(ScriptTokenParser.isStringStatus(status))
				{
					// String status is illegal !!! 
					// it is not function call
					return null;				
				}				
			}
			i++;					
		}
		
		// i is now location of "("
		
		// find the location of ")" if one exists
		int clevel = 0;
		while(true)
		{
			if(i >= doclen) break;			
			char cc = getText(i, 1).charAt(0);
			status = ScriptTokenParser.parseStatus(cc, status);
			if(status == ScriptTokenParser.STATUS_NORMAL)
			{
				if(cc == '(')
				{
					clevel++;
				}
				else
				if(cc == ')')
				{
					clevel--;
					if(clevel == 0)
					{
						break;
					}
				}
				
			}
			i++;
		}
		
		// i is now the location of ")"		
		pair[1] = i - pair[0] + 1;
		
		return pair;
		
	}
	
	public int[] extractNormalToken(int offs) throws BadLocationException
	{
		int status = parseStatus(offs);
		if(status != ScriptTokenParser.STATUS_NORMAL) return null;
		
		char c = getText(offs, 1).charAt(0);
		
		if(ScriptTokenParser.symbols.indexOf(c) != -1) return null;
				
		int doclen = getLength();
		
		{
			if(ScriptTokenParser.isVariableOrNumberLetter(c))
			{
				int begin = offs;
				int end = offs;
				
				int o = begin;
				while(true)
				{
					o--;
					if(o < 0) break;
				    char cc = getText(o, 1).charAt(0);
				    if(ScriptTokenParser.isVariableOrNumberLetter(cc))
				    	begin = o;
				    else
				    	break;
				}
										
				o = end;
				while(true)
				{
					o++;
					if(o >= doclen) break;
					char cc = getText(o, 1).charAt(0);
				    if(ScriptTokenParser.isVariableOrNumberLetter(cc))
				    {
				    	end = o;
				    }
				    else
				    	break;							
				}
				
				int slen = end + 1 - begin;
				int[] pair = new int[2];
				pair[0] = begin;
				pair[1] = slen;
				return pair;
				
			}
		}		
		
		return null;
	}
	
	
	ArrayList undoablelisteners = new ArrayList();
	public void addUndoableEditListener(UndoableEditListener listener) {
		undoablelisteners.add(listener);
	}

	public void removeUndoableEditListener(UndoableEditListener listener) {
		undoablelisteners.remove(listener);
	}
	
	private UndoableEditListener uel = new UndoableEditListener()
	{

		public void undoableEditHappened(UndoableEditEvent e) {
			
			UndoableEdit edit = e.getEdit();
			if(edit instanceof AbstractDocument.DefaultDocumentEvent)
			if(((AbstractDocument.DefaultDocumentEvent)edit).getType() 
					== AbstractDocument.DefaultDocumentEvent.EventType.CHANGE)
			{
				return;
			}
			Iterator iterator = undoablelisteners.iterator();
			while (iterator.hasNext()) {
				UndoableEditListener listener = (UndoableEditListener) iterator.next();
				listener.undoableEditHappened(e);
			}
		}
		
	};

	ScriptTokenParser parser;
	
	public RSyntaxDocument(NameSpace namespace)
	{
		root = getDefaultRootElement();
		parser = new ScriptTokenParser(namespace);
		
		super.addUndoableEditListener(uel);
		
		StyleConstants.setForeground(style_normal, Color.BLACK);
		StyleConstants.setForeground(style_parameter, Color.GRAY);
		StyleConstants.setForeground(style_string, Color.BLUE);
		StyleConstants.setForeground(style_number, Color.RED);
		StyleConstants.setForeground(style_symbol, Color.BLUE.darker());
		StyleConstants.setForeground(style_comment, Color.GREEN.darker());
		StyleConstants.setItalic(style_comment, true);
		StyleConstants.setForeground(style_keyword, Color.MAGENTA.darker());
		StyleConstants.setBold(style_keyword, true);
		StyleConstants.setForeground(style_keyword_variable, Color.MAGENTA.darker());
		

		
	}

	protected void fireInsertUpdate(DocumentEvent evt)
	{
		super.fireInsertUpdate(evt);
		if(!doupdate) return;
		try
		{		
		int offs = evt.getOffset();
		int len = evt.getLength();		
		String str = getText(offs, len);
		int status = parseStatus(offs);
						
		applyHighlighting(offs, str.length());
		
		int newstatus = ScriptTokenParser.parseStatus(str, status);
		if(newstatus != status)
		{
			offs += str.length();
			int nextoffs = findNextSameStatus(offs, newstatus, status);
			applyHighlighting(offs, nextoffs - offs + 1);
		}			
		}
		catch(BadLocationException e)
		{
			e.printStackTrace();
		}
	}

	boolean ishandlingremove = false;
	protected void fireRemoveUpdate(DocumentEvent evt)
	{
		super.fireRemoveUpdate(evt);
		if(!doupdate) return;
		if(!ishandlingremove)
		{
			try {
				applyHighlighting(0, getLength());
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}
	}
	public void remove(int offs, int len) throws BadLocationException 
	{	
		
		String removedcontent = getText(offs, len);
		ishandlingremove = true;
		super.remove(offs, len);
		ishandlingremove = false;
		int status = parseStatus(offs);
		applyHighlighting(offs, 0);
		int oldstatus = ScriptTokenParser.parseStatus(removedcontent, status);
		if(oldstatus != status)
		{
			int nextoffs = findNextSameStatus(offs, oldstatus, status);
			applyHighlighting(offs, nextoffs - offs + 1);
		}		

		
	}
	
	public int applyHighlighting(int offs, int len) throws BadLocationException
	{
		int doclen = getLength();
		
		offs -= 10;
		if(offs < 0) offs = 0;
		len += 20;
		if(len >= (doclen-offs)) len = doclen-offs;
				
		setCharacterAttributes(offs, len, style_normal, true);
		
		String text = getText(offs, len);
		
		int status = parseStatus(offs);
		
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			int beforestatus = status;
			status = ScriptTokenParser.parseStatus(c, status);
			if(ScriptTokenParser.isStringStatus(status)
			  ||ScriptTokenParser.isStringStatus(beforestatus))
			{
				
				int starti = i;
				i++;
				for (; i < text.length(); i++) {
					beforestatus = status;
					c = text.charAt(i);
					status = ScriptTokenParser.parseStatus(c, status);					
					if(!ScriptTokenParser.isStringStatus(status))
					{
						status = beforestatus;
						i--;
						break;
					}
				}				
				
				setCharacterAttributes(offs+starti, i - starti + 1, style_string, true);
				if(beforestatus == ScriptTokenParser.STATUS_PRE_COMMENTS)
				{
					setCharacterAttributes(offs+starti-1, i - starti + 2, style_string, true);
				}
			}
			else
			if(ScriptTokenParser.isCommentStatus(status)
			 ||ScriptTokenParser.isCommentStatus(beforestatus))
			 {		
				int starti = i - 1;
				i++;
				for (; i < text.length(); i++) {
					beforestatus = status;
					c = text.charAt(i);
					status = ScriptTokenParser.parseStatus(c, status);					
					if(!ScriptTokenParser.isCommentStatus(status))
					{
						status = beforestatus;
						i--;
						break;
					}
				}
				setCharacterAttributes(offs+starti, i - starti + 1, style_comment, true);
				
				//setCharacterAttributes(offs+i, 1, style_comment, true);
				//setCharacterAttributes(offs+i-1, 1, style_comment, true);
			 }
			else
			{
				if(ScriptTokenParser.symbols.indexOf(c) != -1) setCharacterAttributes(offs+i, 1, style_symbol, true);
				else				
				{
					if(ScriptTokenParser.isVariableOrNumberLetter(c))
					{
						int begin = offs+i;
						int end = offs+i;
						
						int o = begin;
						while(true)
						{
							o--;
							if(o < 0) break;
						    char cc = getText(o, 1).charAt(0);
						    if(ScriptTokenParser.isVariableOrNumberLetter(cc))
						    	begin = o;
						    else
						    	break;
						}
												
						o = end;
						while(true)
						{
							o++;
							if(o >= doclen) break;
							char cc = getText(o, 1).charAt(0);
						    if(ScriptTokenParser.isVariableOrNumberLetter(cc))
						    {
						    	end = o;
						    	i++;
						    }
						    else
						    	break;							
						}
						
						int slen = end + 1 - begin;
						processToken(begin, slen);
					}
				}

			}
			
			
				
		}		
		return status;
	}
	
	public char getCharAfterString(int offs) throws BadLocationException
	{
		int doclen = getLength();
		int o = offs;
		while(true)
		{
			o++;
			if(o >= doclen) break;
		    char cc = getText(o, 1).charAt(0);
		    if(cc != ' ')		    	
		    {
		    	return cc;
		    }
		}		
		return ' ';		
		
	}
	public void processToken(int offs, int len) throws BadLocationException
	{
		String stext = getText(offs, len);
		if(ScriptTokenParser.isNumber(stext))
		{
			setCharacterAttributes(offs, len, style_number, true);
			return;
		}

		char cs = getCharAfterString(offs+len-1);
		if(cs == '(')
		{
			// Function Call
			if(parser.isFunction(stext))
			{
				setCharacterAttributes(offs, len, style_keyword, true);
				return;
			}			
		}
		else
		if(cs == '=')
		{
			setCharacterAttributes(offs, len, style_parameter, true);
			return;
		}
		else
		{
			// Variable
			if(parser.isRegisteredVariable(stext.toLowerCase()))
			{
				setCharacterAttributes(offs, len, style_keyword_variable, true);
				return;
			}			
		}
		
		setCharacterAttributes(offs, len, style_normal, true);
		
	}
	

	public int findNextSameStatus(int offs, int statusA, int statusB) throws BadLocationException
	{
		int len = getLength() - offs;		
		while(statusA != statusB)
		{
			if(len < 20)
			{
				return getLength();
			}
			else
			{
				statusA = ScriptTokenParser.parseStatus(getText(offs, 20), statusA);
				statusB = ScriptTokenParser.parseStatus(getText(offs, 20), statusB);
				len -= 20;
				offs += 20;
			}			
		}
		return offs;
		
	}
	
	public int parseStatus(int offs) throws BadLocationException
	{
		return ScriptTokenParser.parseStatus(getText(0, offs), ScriptTokenParser.STATUS_NORMAL);
	}	
}
