/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.ext;

import java.util.List;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.Unit;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

class RExternalJavaInstance implements UnitInstancePart, Commitable
{
	
	Variable returnvar;
	Variable inputvar;	
	Variable answer = null;
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void calcInput()
	{		
		namespace.addToCommitStack(this);
	}

	public void commit() {
		if(answer != null)
		{
			returnvar.remove(answer);
			answer = null;
		}		
		
		try
		{
		
		String classname = ObjectsPart.toString(inputvar);		
		Class varclass = Class.forName(classname);
		Object obj = varclass.newInstance();
		if(obj instanceof VariableFactory)
		{
			answer = ((VariableFactory)obj).asVariable();						
		}
		else
		{
			if(obj instanceof UnitFactory)
			{
				answer = Unit.asVariable((UnitFactory)obj);
			}
		}		
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			answer = null;
		}
		
		if(answer != null) returnvar.add(answer);
	}	
	
	ListPartListener input_listener = new ListPartListener()
	{
		public void objectAdded(ListPart source, Object object) {
			calcInput();			
		}
		public void objectRemoved(ListPart source, Object object) {
			calcInput();			
		}
		public void objectsAdded(ListPart source, List objects) {
			calcInput();			
		}
		public void objectsRemoved(ListPart source, List objects) {
			calcInput();			
		}
	};	

	NameSpace namespace;
	public RExternalJavaInstance(Parameters parameters)
	{
		namespace = parameters.getNameSpace();
		returnvar = (Variable)parameters.getParameterWithDefault(0, "output");

		inputvar = (Variable)parameters.getParameterWithDefault(1, "input");
		ObjectsPart.getInstance(inputvar).addListener(input_listener);
				
		calcInput();
	}
	public void close() {
		
		ObjectsPart.getInstance(inputvar).removeListener(input_listener);		
		
		if(answer != null)
		{
			returnvar.remove(answer);
			answer = null;
		}		
		returnvar = null; 
		inputvar = null;
		
	}

}

public class ExternalJava implements UnitFactory {
	public UnitInstancePart newInstance(Parameters parameters) {
		return new RExternalJavaInstance(parameters);
	}
}
