/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.ext;

import java.util.List;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstance;
import rasmus.interpreter.unit.UnitInstancePart;


class EvalExpressionInstance implements UnitInstancePart, ListPartListener, Commitable
{	
	UnitInstance unitinstance;
	Variable inputvar;
	Variable outputvar = null;
	Variable answer = null;
	NameSpace namespace;
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}

	public EvalExpressionInstance(Parameters parameters)
	{
		namespace = parameters.getNameSpace();
		outputvar = (Variable)parameters.getParameter(0, "output");
		inputvar = (Variable)parameters.getParameterWithDefault(1, "input");
		
		ObjectsPart.getInstance(inputvar).addListener(this);
		
		namespace.addToCommitStack(this);
	}
	public void close() {
		
		clear();
		ObjectsPart.getInstance(inputvar).removeListener(this);
		
	}
	
	Interpreter interpreter = null;
	
	public void clear()
	{
		if(interpreter != null)
		{
			interpreter.close();
		}
		if(answer != null)
		{
			outputvar.remove(answer);
			answer = null;
		}		
	}
	
	public void commit()
	{
		clear();
		
		String code = ObjectsPart.toString(inputvar);
		
		Variable ret;
		interpreter = new Interpreter(namespace);
		interpreter.setAutoCommit(false);
		try {
			ret = interpreter.eval(code);
		} catch (ScriptParserException e) {
			e.printStackTrace();
			interpreter.close();
			interpreter = null;
			return;
		}
		
		if(outputvar != null)
		{
			answer = ret;
			if(answer != null)
			outputvar.add(answer);
		}
	}
	
	public void objectAdded(ListPart source, Object object) {
		namespace.addToCommitStack(this);
	}
	public void objectRemoved(ListPart source, Object object) {
		namespace.addToCommitStack(this);
	}
	public void objectsAdded(ListPart source, List objects) {
		namespace.addToCommitStack(this);
	}
	public void objectsRemoved(ListPart source, List objects) {
		namespace.addToCommitStack(this);
	}
}

public class EvalExpression implements UnitFactory {
	public UnitInstancePart newInstance(Parameters parameters) {
		return new EvalExpressionInstance(parameters);
	}
}
