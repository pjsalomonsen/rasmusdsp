/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.ext;

import java.util.ArrayList;
import java.util.Iterator;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.unit.Unit;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstancePart;

public class Module implements UnitInstancePart {

	NameSpace namespace;
	public void setNameSpace(NameSpace namespace)
	{
		this.namespace = namespace;
	}
	
    ArrayList added = new ArrayList();
	public void add(String name, Variable var)
	{
		namespace.get(name).add(var);
		Object[] pair = new Object[2];
        pair[0] = name;
		pair[1] = var;
		added.add(pair);
	}
	public void add(String name, UnitFactory unitfactory)
	{		
		add(name, Unit.asVariable(unitfactory));
	}
	
	public void add(String name, ModuleFactory modulefactory)
	{		
		add(name, modulefactory.asVariable());
	}	

	public void add(String name, double number)
	{
		add(name, DoublePart.asVariable(number));
	}
	
	public void add(String name, float number)
	{
		add(name, DoublePart.asVariable(number));
	}
	
	public void add(String name, String value)
	{
		add(name, ObjectsPart.asVariable(value));
	}	
	
	public void add(String name, Object object)
	{
		add(name, ObjectsPart.asVariable(object));
	}		
	
	public void close() {
		Iterator iterator = added.iterator();
		while (iterator.hasNext()) {
			Object[] pair = (Object[]) iterator.next();
			namespace.get((String)pair[0]).remove((Variable)pair[1]);
		}		
	}
}
