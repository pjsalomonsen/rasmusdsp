/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.namespace.GlobalNameSpace;
import rasmus.interpreter.namespace.InheritNameSpace;
import rasmus.interpreter.parser.ScriptCompiler;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.Unit;
import rasmus.interpreter.unit.UnitFactory;


public class Interpreter extends NameSpaceAdapter implements Closeable {

	private ArrayList<Closeable> instances = new ArrayList<Closeable>();
	
	private boolean autocommit = true;

	public Interpreter() {
		super(new InheritNameSpace(GlobalNameSpace.getNameSpace()));
	}

	public Interpreter(NameSpace namespace) {
		super(namespace);
	}
	
	public void setNameSpace(NameSpace namespace)
	{
		super.setNameSpace(namespace);
	}
	
	public boolean getAutoCommit()
	{ 
		return autocommit;
	}
	
	public void setAutoCommit(boolean value)
	{
		autocommit = value;
	}
	
	public Variable source(String filename) throws IOException, ScriptParserException {
		return source(new File(filename));
	}

	public Variable source(File file) throws IOException, ScriptParserException {

		FileInputStream istream = new FileInputStream(file);
		BufferedInputStream bistream = new BufferedInputStream(istream);
		Variable ret;
		try {
			ret = source(bistream);
		} finally {
			istream.close();
		}
		return ret;
	}

	public Variable source(InputStream instream) throws IOException, ScriptParserException {
		StringBuffer sb = new StringBuffer();
		InputStreamReader reader = new InputStreamReader(instream);
		BufferedReader breader = new BufferedReader(reader);
		String line = breader.readLine();
		while (line != null) {
			sb.append(line + "\r\n");
			line = breader.readLine();
		}
		reader.close();
		return eval(sb.toString());
	}
	

	
	public Variable execute(Executable executable) {
		ExecutableInstance instance = executable.execute(getNameSpace());			
		instances.add(instance);
		if(autocommit) commit();
		return instance.getReturnVariable();
	}

	public Variable eval(String script) throws ScriptParserException {
		return execute(ScriptCompiler.compile(script));
	}


	public void close() {
		
		addToCommitStack(new Commitable() {
			public int getRunLevel()
			{
				return RUNLEVEL_DEFAULT;
			}			
			public void commit()
			{
			Iterator<Closeable> iter = instances.iterator();
			while (iter.hasNext()) 
				iter.next().close();			
			instances.clear();
			}
		});
		if(autocommit) commit();
	}

	private class AddInstance implements Closeable, Commitable
	{
		private Variable var;
		private Variable data;
		
		public int getRunLevel()
		{
			return RUNLEVEL_DEFAULT;
		}

		public AddInstance(Variable var, Variable data)
		{
			this.var = var;
			this.data = data;
			//var.add(data);
		}

		public void close() {
			if(var == null) return;
			var.remove(data);
			var = null;
			data = null;
		}

		public void commit() {
			var.add(data);
			instances.add(this);
		}		 
	}
	
	public void add(Variable var, Variable data) {
		addToCommitStack(new AddInstance(var, data));
		if(autocommit) commit();
	}

	public void add(String varname, Variable data) {
		add(get(varname), data);
	}

	public void add(String varname, Object object) {
		add(varname, asVariable(object));
	}

	public void add(Variable var, Object object) {
		add(var, asVariable(object));
	}

	public Variable asVariable(Object value) {
		if (value == null)
			return new Variable();
		if (value instanceof Variable)
			return (Variable) value;
		if (value instanceof Number)
			return DoublePart.asVariable(((Number) value).doubleValue());
		if (value instanceof String)
			return ObjectsPart.asVariable(value);
		if (value instanceof File) {
			Variable output = new Variable();
			call("file", output, asVariable(((File) value).getPath()));
			return output;
		}
		return new Variable();
	}

	public void call(String unit, Object... arguments) {
		callUnit(get(unit), arguments);
	}

	public void call(UnitFactory unit, Object... arguments) {
		callUnit(Unit.asVariable(unit), arguments);
	}

	public void call(Variable unit, Object... arguments) {
		callUnit(unit, arguments);
	}
	
	private class CallTask implements Commitable
	{

		Variable unit;
		Object[] arguments;
		public CallTask(Variable unit, Object[] arguments)
		{
			this.unit = unit;
			this.arguments = arguments;
		}
		public int getRunLevel()
		{
			return RUNLEVEL_DEFAULT;
		}		
		public void commit() {
			int ix = 0;
			Parameters uparams = new Parameters(Interpreter.this);

			for (int i = 0; i < arguments.length; i++) {
				Object arg = arguments[i];
				if (arg instanceof Map) {
					Map map = (Map) arg;
					Iterator iterator = map.entrySet().iterator();
					while (iterator.hasNext()) {
						Map.Entry entry = (Map.Entry) iterator.next();
						uparams.setParameter((String) entry.getKey(), asVariable(entry
								.getValue()));
					}
				} else {
					uparams.setParameter(ix, asVariable(arg));
					ix++;
				}
			}
			instances.add(Unit.newInstance(unit, uparams));					
		}
	}	

	private void callUnit(Variable unit, Object[] arguments) {
		addToCommitStack(new CallTask(unit, arguments));
		if(autocommit) commit();
	}

}
