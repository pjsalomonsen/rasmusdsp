/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package rasmus.interpreter.controls.midi;

import java.util.Map;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.swing.JComponent;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.controls.ControlAdapter;
import rasmus.interpreter.controls.ControlUnitFactory;
import rasmus.interpreter.controls.ControlInstance;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.midi.MidiSequence;

class ControlMidiControlSliderInstance extends ControlAdapter implements Receiver 
{
	JSlider slider;

	Variable midioutput;
	Variable midiinput;
	Variable control;
	Variable defaultval;
    Variable min;
    Variable max;
    Variable orientation;
    
    Receiver recv;

	public ControlMidiControlSliderInstance(NameSpace namespace, Map parameters)
	{		
		slider = new JSlider();
		slider.setMinimum(0);
		slider.setMaximum(127);
		slider.setValue(0);	
		//slider.setSnapToTicks(true);
		slider.setPaintTicks(true);		
		slider.setMajorTickSpacing(0);
		slider.addChangeListener(new ChangeListener()
				{

					public void stateChanged(ChangeEvent arg0) {
						ShortMessage smessage = new ShortMessage();
						try
						{
							smessage.setMessage(ShortMessage.CONTROL_CHANGE, 0, i_control, slider.getValue()); 
							if(recv != null) recv.send(smessage, -1);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
			
				});
		
		midioutput = (Variable)parameters.get("midioutput");
		midiinput  = (Variable)parameters.get("midiinput");
		control = (Variable)parameters.get("control");
	    defaultval =(Variable)parameters.get("default");
	    min = (Variable)parameters.get("min");
	    max = (Variable)parameters.get("max");
	    orientation = (Variable)parameters.get("orientation");
		
	    if(midiinput != null)
	    {
	    	MidiSequence.getInstance(midiinput).addReceiver(this);
	    }
	    if(control != null) DoublePart.getInstance(control).addListener(this);
	    if(defaultval != null) DoublePart.getInstance(defaultval).addListener(this);
	    if(min != null) DoublePart.getInstance(min).addListener(this);
	    if(max != null) DoublePart.getInstance(max).addListener(this);
	    if(orientation != null) DoublePart.getInstance(orientation).addListener(this);
		if(midioutput != null)
		{
			recv = MidiSequence.getInstance(midioutput);
		}
		calc();
	}
	
	public JComponent getJComponent() {
		return slider;
	}
	
	public void close() {
		
	    if(midiinput != null)
	    {
	    	MidiSequence.getInstance(midiinput).removeReceiver(this);
	    }		
	    if(control != null) DoublePart.getInstance(control).removeListener(this);
	    if(defaultval != null) DoublePart.getInstance(defaultval).removeListener(this);
	    if(min != null) DoublePart.getInstance(min).removeListener(this);
	    if(max != null) DoublePart.getInstance(max).removeListener(this);
	    if(orientation != null) DoublePart.getInstance(orientation).removeListener(this);

	}
	
	int i_control = 0;
	public void calc()
	{
		if(control != null) i_control = (int)DoublePart.asDouble(control);		
		if(min != null) slider.setMinimum((int)DoublePart.asDouble(min));
		if(max != null) slider.setMaximum((int)DoublePart.asDouble(max));
		if(defaultval != null) slider.setValue((int)DoublePart.asDouble(defaultval));
		if(orientation != null) 
		{
			int i_orientation = (int)DoublePart.asDouble(orientation);
			if(i_orientation == 0) slider.setOrientation(JSlider.HORIZONTAL);
			if(i_orientation == 1) slider.setOrientation(JSlider.VERTICAL);			
		}
		
		if(slider.getMaximum() - slider.getMinimum() < 16)
		{
			slider.setSnapToTicks(true);
			slider.setMajorTickSpacing(1);
		}
		else
		if(slider.getMaximum() - slider.getMinimum() == 127)
		{
			slider.setSnapToTicks(false);
			slider.setMajorTickSpacing(16);
		}
		else
		{
			slider.setSnapToTicks(false);
			slider.setMajorTickSpacing(0);
		}
	}

	int set_value = 0; 
	Runnable set_slider_value = new Runnable()
	{
		public void run() {
			if(slider.getMinimum() <= set_value)
			if(slider.getMaximum() >= set_value)
				slider.setValue(set_value);
		}
	};
	
	public void send(MidiMessage arg0, long arg1) {
		if(arg0 instanceof ShortMessage)
		{
			ShortMessage sm = (ShortMessage)arg0;
			if(sm.getStatus() == ShortMessage.CONTROL_CHANGE)
			{
				if(sm.getData1() == i_control)
				{
					set_value = sm.getData2();
					SwingUtilities.invokeLater(set_slider_value);
				}
			}
		}
		
	}

	
}

public class MidiControlSlider extends ControlUnitFactory {

	public MidiControlSlider()
	{
		registerParameter(1, "midioutput");
		registerParameter(2, "control");
		registerParameter(3, "default");
		registerParameter(4, "min");
		registerParameter(5, "max");
		registerParameter(6, "orientation");
		registerParameter(7, "midiinput");
		registerParameter("input", "midiinput");
	}
	

	public MetaData getMetaData()
	{
		MetaData metadata = super.getMetaData();
		metadata.setDescription("MIDI Control Slider");
		metadata.add(1, "midioutput",	"MIDI output",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(2, "control",		"MIDI control",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(3, "default",		"Default value",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "min",			"Min value",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(5, "max",			"Min value",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(6, "orientation",	"Orientation",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(7, "midiinput",	"MIDI Input",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(-1, "input",		"MIDI Input",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;
	}
	
	
	public ControlInstance newControlInstance(NameSpace namespace, Map parameters)
	{
		return new ControlMidiControlSliderInstance(namespace, parameters);
	}
}