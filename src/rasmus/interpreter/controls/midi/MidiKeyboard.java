/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package rasmus.interpreter.controls.midi;

import java.util.Map;

import javax.swing.JComponent;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.controls.ControlAdapter;
import rasmus.interpreter.controls.ControlUnitFactory;
import rasmus.interpreter.controls.ControlInstance;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.midi.MidiSequence;
import uk.co.drpj.midi.VirtualPianoPanel;

class ControlMidiKeyboardInstance extends ControlAdapter
{
	VirtualPianoPanel piano;
	
	public ControlMidiKeyboardInstance(NameSpace namespace, Map parameters)
	{		
		piano = new VirtualPianoPanel();		
		Variable midioutput = (Variable)parameters.get("midioutput");
		if(midioutput != null)
			piano.setReceiver(MidiSequence.getInstance(midioutput));
		
		calc();
	}
	
	public JComponent getJComponent() {
		return piano;
	}
	
	public void close() {
		piano.setReceiver(null);
	}
	
	public void calc()
	{
	}
}

public class MidiKeyboard extends ControlUnitFactory {

	public MidiKeyboard()
	{
		registerParameter(1, "midioutput");
	}
	

	public MetaData getMetaData()
	{
		MetaData metadata = super.getMetaData();
		metadata.setDescription("MIDI Keyboard Control");
		metadata.add(1, "midioutput",	"MIDI Output",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		return metadata;
	}
	
	
	public ControlInstance newControlInstance(NameSpace namespace, Map parameters)
	{
		return new ControlMidiKeyboardInstance(namespace, parameters);
	}
}