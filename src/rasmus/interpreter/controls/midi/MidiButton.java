/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package rasmus.interpreter.controls.midi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.swing.JButton;
import javax.swing.JComponent;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.controls.ControlAdapter;
import rasmus.interpreter.controls.ControlUnitFactory;
import rasmus.interpreter.controls.ControlInstance;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.midi.MidiSequence;

class ControlMidiButtonInstance extends ControlAdapter implements ActionListener
{
	JButton button;
	Variable caption;
	
	Variable midioutput;
	Variable input;
	
	public ControlMidiButtonInstance(NameSpace namespace, Map parameters)
	{		
		button = new JButton();
		button.addActionListener(this);
		
		caption = (Variable)parameters.get("caption");
		if(caption != null)
			ObjectsPart.getInstance(caption).addListener(this);
		
		
		midioutput = (Variable)parameters.get("midioutput");
		input = (Variable)parameters.get("input");
		
		calc();
	}
	
	public JComponent getJComponent() {
		return button;
	}
	
	public void close() {
		button.removeActionListener(this);
		if(caption != null)
			ObjectsPart.getInstance(caption).removeListener(this);			
		caption = null;
	}
	
	public void calc()
	{
		if(caption != null)
		button.setText(ObjectsPart.toString(caption));
	}

	public void actionPerformed(ActionEvent event) {
		
		if(midioutput == null) return;
		if(input == null) return;
		
		Receiver recv = MidiSequence.getInstance(midioutput);
		Sequence seq = MidiSequence.asSequence(input);
		
		try
		{
			Sequencer seqr = getSequencer();
			seqr.open();
			seqr.getTransmitter().setReceiver(recv);			
			seqr.setSequence(seq);			
			seqr.start();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public Sequencer getSequencer() throws Exception
	{
		String midiseqname = "Real Time Sequencer";		
		MidiDevice.Info devinfo[] = MidiSystem.getMidiDeviceInfo();
		for (int i = 0; i < devinfo.length; i++) {
			if(devinfo[i].getName().equals(midiseqname))
			{
				try {
					MidiDevice mdev = MidiSystem.getMidiDevice(devinfo[i]);
					if(mdev instanceof Sequencer) return (Sequencer)mdev;
				} catch (MidiUnavailableException e) {
					e.printStackTrace();
				}  		
			}
		}
		
		return MidiSystem.getSequencer(false);
	}		
}

public class MidiButton extends ControlUnitFactory {

	public MidiButton()
	{
		registerParameter(1, "caption");
		registerParameter(2, "midioutput");
		registerParameter(3, "input");
	}
	

	public MetaData getMetaData()
	{
		MetaData metadata = super.getMetaData();
		metadata.setDescription("MIDI Button Control");
		metadata.add(1, "caption",		"Caption",		null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		metadata.add(2, "midioutput",	"MIDI Output",	null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_OUT);
		metadata.add(3, "input",		"MIDI Messages",null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		return metadata;
	}
	
	
	public ControlInstance newControlInstance(NameSpace namespace, Map parameters)
	{
		return new ControlMidiButtonInstance(namespace, parameters);
	}
}