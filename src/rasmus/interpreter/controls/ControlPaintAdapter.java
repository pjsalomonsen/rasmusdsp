/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package rasmus.interpreter.controls;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

public abstract class ControlPaintAdapter extends ControlAdapter {

	JComponent panel = null;

	BufferedImage offImg;
	BufferedImage onImg;
	
	Graphics2D offGraphic;
	Graphics2D onGraphic;

	Dimension currentsize = null;
	
	boolean paintIntStatus = false;
	long paintIntStatus_time = 0;
	
	boolean doublebuffering = true;
	
	int x_offset = 0;
	public void setXOffset(int value)
	{
		x_offset = value;
	}
	
	public void setDoubleBuffering(boolean value)
	{
		this.doublebuffering = value;
	}
	
	public boolean isDoubleBuffering()
	{
		return doublebuffering;
	}

	public Dimension getSize()
	{
		return currentsize;
	}
	
	public void repaint() {
		try {
			
			if(!doublebuffering)
			{
				panel.repaint();
				return;
			}
			
			synchronized (panel)
			{
				// Check if last picture has been painted
				// if not, dont't send repaint
				if(paintIntStatus)
				{
					// But don't wait more than 250 msec
					if((System.currentTimeMillis() - paintIntStatus_time) < 250) return;
					paintIntStatus = false;
				}
			}
						
			synchronized (ControlPaintAdapter.this) 
			{

				
				BufferedImage bak = onImg;
				onImg = offImg;
				offImg = bak;
				
				Graphics2D bakGraphic = onGraphic;
				onGraphic = offGraphic;
				offGraphic = bakGraphic;		
				
				paintIntStatus = true;
				paintIntStatus_time = System.currentTimeMillis();
			}
			panel.repaint();
		} catch (Exception e) {
		}
	}

	public Graphics2D getGraphics() {
		
		Dimension size = getJComponent().getSize();
		if(size.width == 0)
		if(size.height == 0)
			size = getJComponent().getPreferredSize();
		
		if (currentsize != null) {			
			if ((currentsize.width != size.width)
					|| (currentsize.height != size.height)) {
				synchronized (ControlPaintAdapter.this)
				{
					offImg = null;
					onImg = null;
				}
			}
		}
		if (offImg == null) {
			GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			GraphicsConfiguration graphicsConfiguration = graphicsEnvironment
					.getDefaultScreenDevice().getDefaultConfiguration();
			synchronized (ControlPaintAdapter.this)
			{
			onImg = graphicsConfiguration.createCompatibleImage(size.width,
					size.height, Transparency.BITMASK);
			onGraphic = onImg.createGraphics();
			if(doublebuffering)
			{
				offImg = graphicsConfiguration.createCompatibleImage(size.width,
						size.height, Transparency.BITMASK);
				offGraphic = offImg.createGraphics();			
			}
			else
			{
				offImg = onImg;
				offGraphic = onGraphic;
			}
			
			
			}
			currentsize = size;
		}
		return offGraphic;
		
	}
	
	public BufferedImage getBufferedImage() {
		getGraphics();
		return offImg;
	}

	public JComponent getJComponent() {
		if (panel != null)
			return panel;

		panel = new JComponent() {

			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics graphics) {
				
				if(!doublebuffering)
				{
					int x_offset = ControlPaintAdapter.this.x_offset;
					if(x_offset == 0)
					{
						graphics.drawImage(onImg, 0, 0, this);
					}
					else
					{
						Dimension size = getSize();
						int w = size.width;
						int h = size.height;
						// ef y_offset = 1
						// �� teiknu� vi� fr� y_offset til enda og s��an 0 til y_offset
						graphics.drawImage(onImg, 0, 0, w - x_offset, h, x_offset, 0, w, h, this);
						
						graphics.drawImage(onImg, w - x_offset, 0, w, h, 0, 0, x_offset, h, this);
						
						//graphics.drawImage(onImg, 0, 0, w - x_offset, h, x_offset, 0, w, h, this);
						
						/*
    dx1 - the x coordinate of the first corner of the destination rectangle.
    dy1 - the y coordinate of the first corner of the destination rectangle.
    dx2 - the x coordinate of the second corner of the destination rectangle.
    dy2 - the y coordinate of the second corner of the destination rectangle.
    sx1 - the x coordinate of the first corner of the source rectangle.
    sy1 - the y coordinate of the first corner of the source rectangle.
    sx2 - the x coordinate of the second corner of the source rectangle.
    sy2 - the y coordinate of the second corner of the source rectangle. 
						 */
					}
					return;
				}
				
				synchronized (ControlPaintAdapter.this)
				{
					if (onImg == null) return;					
					graphics.drawImage(onImg, 0, 0, this);					
				}
				
				synchronized (panel) {
					paintIntStatus = false;
				}
			}

		};
		
		return panel;
	}

}
