/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.controls;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ListPart;
import rasmus.interpreter.list.ListPartListener;
import rasmus.interpreter.math.DoublePart;

public class ControlViewer extends JPanel {

	private class RControlVector {
		ControlInstance instance;

		JComponent comp;
	}

	private static final long serialVersionUID = 1L;

	private Controls controls;
	NameSpace namespace;
	
	public ControlViewer(NameSpace namespace, Variable variable) {
		setOpaque(false);
		setLayout(new GridBagLayout());
		controls = Controls.getInstance(variable);
		controls.addListener(listlistener);

		this.namespace = namespace;
		namespace.addToCommitStack(init_runnable);

	}

	public void close() {

		Runnable runnable = new Runnable() {
			public void run() {

				Object[] controls = ControlViewer.this.controls_map.keySet()
						.toArray();
				for (int i = 0; i < controls.length; i++) {
					removeControl((Control) controls[i]);
				}
			}

		};

		if (SwingUtilities.isEventDispatchThread())
			runnable.run();
		else
			SwingUtilities.invokeLater(runnable);

		controls.removeListener(listlistener);
		controls = null;
	}

	private ListPartListener listlistener = new ListPartListener() {
		public void objectAdded(ListPart source, Object object) {
			namespace.addToCommitStack(init_runnable);
		}

		public void objectRemoved(ListPart source, Object object) {
			namespace.addToCommitStack(init_runnable);
		}

		public void objectsAdded(ListPart source, List objects) {
			namespace.addToCommitStack(init_runnable);
		}

		public void objectsRemoved(ListPart source, List objects) {
			namespace.addToCommitStack(init_runnable);
		}
	};

	private HashMap controls_map = new HashMap();

	private void addControl(Control control) {
		RControlVector vector = new RControlVector();
		vector.instance = control.newInstance();
		vector.comp = vector.instance.getJComponent();
		controls_map.put(control, vector);

		GridBagConstraints c = new GridBagConstraints();
		Variable row = (Variable) control.parameters.get("row");
		if (row != null)
			c.gridy = (int) DoublePart.asDouble(row);
		Variable col = (Variable) control.parameters.get("col");
		if (col != null)
			c.gridx = (int) DoublePart.asDouble(col);

		Variable rowspan = (Variable) control.parameters.get("rowspan");
		if (rowspan != null)
			c.gridheight = (int) DoublePart.asDouble(rowspan);
		Variable colspan = (Variable) control.parameters.get("colspan");
		if (colspan != null)
			c.gridwidth = (int) DoublePart.asDouble(colspan);

		Variable rowweight = (Variable) control.parameters.get("rowweight");
		if (rowweight != null)
			c.weighty = DoublePart.asDouble(rowweight);
		Variable colweight = (Variable) control.parameters.get("colweight");
		if (colweight != null)
			c.weightx = DoublePart.asDouble(colweight);
		Variable weight = (Variable) control.parameters.get("weight");
		if (weight != null) {
			c.weightx = DoublePart.asDouble(weight);
			c.weighty = DoublePart.asDouble(weight);
		}

		Variable margin = (Variable) control.parameters.get("margin");
		if (margin != null) {
			int m = (int) DoublePart.asDouble(margin);
			c.insets = new Insets(m, m, m, m);
		}

		Variable fill = (Variable) control.parameters.get("fill");
		if (fill != null) {
			int f = (int) DoublePart.asDouble(fill);
			if (f == 1)
				c.fill = GridBagConstraints.BOTH;
			if (f == 2)
				c.fill = GridBagConstraints.HORIZONTAL;
			if (f == 3)
				c.fill = GridBagConstraints.VERTICAL;
		}

		Variable width = (Variable) control.parameters.get("width");
		Variable height = (Variable) control.parameters.get("height");
		if (width != null) {
			double w = DoublePart.asDouble(width);
			Dimension d = vector.comp.getMaximumSize();
			d.width = (int) w;
			vector.comp.setMaximumSize(d);
			d = vector.comp.getPreferredSize();
			d.width = (int) w;
			vector.comp.setPreferredSize(d);
		}
		if (height != null) {
			double h = DoublePart.asDouble(height);
			Dimension d = vector.comp.getMaximumSize();
			d.height = (int) h;
			vector.comp.setMaximumSize(d);
			d = vector.comp.getPreferredSize();
			d.height = (int) h;
			vector.comp.setPreferredSize(d);
		}

		add(vector.comp, c);
	}

	private void removeControl(Control control) {
		RControlVector vector = (RControlVector) controls_map.get(control);
		if (vector == null)
			return;
		controls_map.remove(control);
		vector.instance.close();

		remove(vector.comp);
	}

	private Commitable init_runnable = new Commitable() {
		
		public int getRunLevel() {
			return RUNLEVEL_INIT;
		}		
		public void commit() {
			Runnable runnable = new Runnable() {
				public void run() {
					if (controls != null) {

						List list = controls.getObjects();
						Iterator iterator = list.iterator();
						while (iterator.hasNext()) {
							Control control = (Control) iterator.next();
							if (!controls_map.containsKey(control)) {
								addControl(control);
							}
						}
						Object[] controls = ControlViewer.this.controls_map
								.keySet().toArray();
						for (int i = 0; i < controls.length; i++) {
							Control control = (Control) controls[i];
							if (!list.contains(control))
								removeControl(control);
						}

						repaint();
						invalidate();
						validate();
					}
				}
			};
			if (SwingUtilities.isEventDispatchThread())
				runnable.run();
			else
				SwingUtilities.invokeLater(runnable);
		}
	};
}
