/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package rasmus.interpreter.controls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.metadata.MetaDataProvider;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

public abstract class ControlUnitFactory implements UnitFactory , MetaDataProvider {
	
	public MetaData getMetaData()
	{
		MetaData metadata = new MetaData("Visual Control");
		metadata.add(-1, "output",		"Output",		null, null, MetaData.TYPE_CONTROL, MetaData.DIRECTION_OUT);
		metadata.add(-1, "row",			"Row",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "rowspan",		"Rowspan",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "col",			"Col",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "colspan",		"Colspan",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "weight",		"Weight",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "width",		"Width",		null, "px", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "height",		"Height",		null, "px", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "fill",		"Fill",			null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "margin",		"Margin",		null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		return metadata;
	}
	
	Map registeredparameters = new HashMap();
	public void registerParameter(int number, String name)
	{
		registeredparameters.put(Integer.toString(number), name.toLowerCase());
	}
	public void registerParameter(String alias, String name)
	{
		registeredparameters.put(alias, name.toLowerCase());
	}
		
	
	public abstract ControlInstance newControlInstance(NameSpace namespace, Map parameters);
	
	private class ControlAdapterUnitInstance extends UnitInstanceAdapter
	{
		Variable output;
		Variable answer = null;   
		
		public ControlAdapterUnitInstance(Parameters parameters)
		{
			Map map = parameters.getParameters();
			Iterator eiterator = registeredparameters.entrySet().iterator();
			while (eiterator.hasNext()) {
				Map.Entry element = (Map.Entry) eiterator.next();
				String key = (String)element.getKey();
				String value = (String)element.getValue();
				if(map.get(key) != null)
				{
					map.put(value, map.get(key));
					map.remove(key);
				}
			}
			output = parameters.getParameterWithDefault("output");		
			answer = Controls.asVariable(new Control( 
					new ControlFactory()
					{
						public ControlInstance newInstance(NameSpace namespace, Map parameters) {
							return newControlInstance(namespace, parameters);
						}
						
					}, map, parameters.getNameSpace()));
			output.add(answer);		
		}
		public void close() {		
			output.remove(answer);
		}
		public void calc() {
		}
		
	}	
	
	public UnitInstancePart newInstance(Parameters parameters) {
		return new ControlAdapterUnitInstance(parameters);
	}
}
