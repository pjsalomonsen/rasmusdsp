/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.controls.sampled;

import java.util.Map;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

import rasmus.interpreter.Variable;
import rasmus.interpreter.controls.ControlPaintAdapter;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.midi.MidiSequence;

public abstract class AudioAbstractAnalyzer extends ControlPaintAdapter implements Receiver, Runnable {

	Variable control = null;
	Variable autostart = null;
	
	public AudioAbstractAnalyzer(Map parameters)
	{		
		control = (Variable)parameters.get("control");
		autostart = (Variable)parameters.get("autostart");
		if(control != null)
		{
			MidiSequence.getInstance(control).addReceiver(this);
		}
	}
	
	public void init()
	{
		if(control != null)
		{
			if(autostart != null)
			if((int)DoublePart.asDouble(autostart) == 1)
			{
				start();
			}
		}
		else
		{
			start();
		}		
	}
	
	public void close()
	{
		stop();
		if(control != null)
		{
			MidiSequence.getInstance(control).removeReceiver(this);
		}
		
	}
	
	Thread analyzerthread = null;
	
	boolean active = false;
	public boolean isActive()
	{
		synchronized (this) {
			return active;
		}
	}
	public void start()
	{
		if(analyzerthread != null) return;
		analyzerthread = new Thread(new Runnable()
				{

					public void run() {
						AudioAbstractAnalyzer.this.run();
						active = false;
						analyzerthread = null;
					}
			
				});
		active = true;		
		analyzerthread.start();
	}
	public void stop()
	{		
		synchronized (this) {
			active = false;
		}		
	}

	public void send(MidiMessage arg0, long arg1) {
		int status = arg0.getStatus();
		
		if(status == ShortMessage.START || status == ShortMessage.CONTINUE)
		{
			start();
		}
		if(status == ShortMessage.STOP)
		{
			stop();
		}
		
	}

	public void calc() {

	}	
}
