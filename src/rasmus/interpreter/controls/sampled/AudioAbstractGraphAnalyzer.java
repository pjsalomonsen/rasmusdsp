/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.controls.sampled;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Map;

import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;

public abstract class AudioAbstractGraphAnalyzer extends AudioAbstractAnalyzer {

	int mode = 0;
	Variable refreshrate;
	
	public AudioAbstractGraphAnalyzer(Map parameters) {
		super(parameters);
		
		Variable mode = (Variable)parameters.get("mode");
		if(mode != null) this.mode = (int)DoublePart.asDouble(mode);
		if(this.mode != 0) setDoubleBuffering(false);
		
		refreshrate = (Variable)parameters.get("refreshrate");
	}
	
	long waittimens = 1000 / 20;
	long lastpainttime;
	
	public void run() {

		double refreshrate = 20; // 20 per second
		
		if(this.refreshrate != null) refreshrate = (int)DoublePart.asDouble(this.refreshrate);
		
		waittimens = (int)(1000 / refreshrate);
		
		lastpainttime = System.currentTimeMillis();
	}

	public void paint(double[][] buffer)
	{		
		switch (mode) {
		case 0:
			paintGraph(buffer);
			break;
		case 1:
			paintGraph2(buffer);
			break;
		case 2:
			paintGraph2(buffer);
			break;			
			
		default:
			break;
		}				
	}
	
	int s_index = 0;
	int[] rgbarray = null;
	public void paintGraph2(double[][] buffer)
	{
		
		BufferedImage buffimg = getBufferedImage();
		Dimension size = getSize();
		int w = size.width;
		int h = size.height;		
		
		int len = buffer[0].length;
				
		double y_step = -(double)h / (double)len;
		double y = h-1;
		int lasty = h-1;		
		int x = s_index; 
		
		if(rgbarray == null || rgbarray.length < h)
		{
			rgbarray = new int[h];
		}
		
		
		if(buffer.length == 1)
		{
			for (int i = 0; i < len; i++, y += y_step) {
				int value = (int)(buffer[0][i]*255.0);
				if(value > 255) value = 255;
				if(value < 0) value = 0;

				int color = (value) +
	            (value << 8) +
	            (0 << 16) + 
	            (0xFF << 24);
	
				int y_i = (int)y;
				if(y_i < 0) y_i = 0;
				Arrays.fill(rgbarray, y_i, lasty, color);						
				lasty = y_i;	
	
			}
		}
		else
		if(buffer.length == 2)
		{
			for (int i = 0; i < len; i++, y += y_step) {
				
				int c_g = (int)(buffer[0][i]*255.0);
				if(c_g > 255) c_g = 255;
				if(c_g < 0) c_g = 0;

				int c_r = (int)(buffer[1][i]*255.0);
				if(c_r > 255) c_r = 255;
				if(c_r < 0) c_r = 0;

				int c_b = c_g + c_r;			
				if(c_b > 255) c_b = 255;
				
				int color = (c_b) +
	            (c_g << 8) +
	            (c_r << 16) + 
	            (0xFF << 24);
	
				int y_i = (int)y;
				if(y_i < 0) y_i = 0;
				Arrays.fill(rgbarray, y_i, lasty, color);						
				lasty = y_i;	
	
			}
		}	
		else
		if(buffer.length > 2)
		{
			for (int i = 0; i < len; i++, y += y_step) {
				
				int c_g = (int)(buffer[0][i]*255.0);
				int c_r = (int)(buffer[1][i]*255.0);
				int c_b = (int)(buffer[2][i]*255.0);
				
				if(c_r > 255) c_r = 255;
				if(c_g > 255) c_g = 255;
				if(c_b > 255) c_b = 255;
				if(c_r < 0) c_r = 0;
				if(c_g < 0) c_g = 0;
				if(c_b < 0) c_b = 0;	
				
				int color = (c_b) +
	            (c_g << 8) +
	            (c_r << 16) + 
	            (0xFF << 24);
	
				int y_i = (int)y;
				if(y_i < 0) y_i = 0;
				Arrays.fill(rgbarray, y_i, lasty, color);						
				lasty = y_i;	
	
			}
		}				
		
		buffimg.setRGB(x, 0, 1, h, rgbarray, 0, 1);
		
		s_index++;
				
		if(s_index >= w)
		{
			s_index = 0;
			if(mode == 1)
			{
			Graphics2D g = getGraphics();
			g.setColor(Color.BLACK);
			g.fillRect(0,0,w,h);
			}
			
		}
		
		if(mode == 2)
		{
			setXOffset(s_index);
		}
		
		if(System.currentTimeMillis() - lastpainttime < waittimens) return;
		lastpainttime = System.currentTimeMillis();
		
		repaint();
	}
	
	public void paintGraph(double[][] buffer)
	{
		if(System.currentTimeMillis() - lastpainttime < waittimens) return;
		lastpainttime = System.currentTimeMillis();
		
		Graphics2D g = getGraphics();
		Dimension size = getSize();
		int w = size.width;
		int h = size.height;		

		g.setColor(Color.BLACK);
		g.fillRect(0,0,w,h);					
	
		for (int c = 0; c < buffer.length; c++) {
			int lastx = 0;
			int lasty = 1;
			
			double[] fftbuff = buffer[c];
			
			double x_step = (double)w / (double)fftbuff.length; 
			double x = 0;
			if(c == 0) 
				g.setColor(Color.CYAN);
			else if(c == 1)
				g.setColor(Color.MAGENTA);
			else 
				g.setColor(Color.YELLOW);
			
			for (int i = 0; i < fftbuff.length; i++, x += x_step) {
				int i_x = (int)x;					
				int y = (int)((1 - fftbuff[i]) * h);
				g.drawLine(lastx, lasty, i_x, y);
				lasty = y;
				lastx = i_x;
			}		
		}		
		
		repaint();		
	}
}
