/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.controls.sampled;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.controls.ControlUnitFactory;
import rasmus.interpreter.controls.ControlInstance;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.util.FFT;


class AudioSpectrumGraphInstance extends AudioAbstractGraphAnalyzer 
{

	Variable input;
	Variable timebase;
	Variable samplerate;
	Variable channels;
	Variable fftframesize;

	Variable mindB;
	Variable maxdB;
	Variable minHz;
	Variable maxHz;
	
	public AudioSpectrumGraphInstance(Map parameters) {
		super(parameters);
		
		timebase = (Variable)parameters.get("timebase");
		input = (Variable)parameters.get("input");
		samplerate = (Variable)parameters.get("samplerate");
		channels = (Variable)parameters.get("channels");
		fftframesize = (Variable)parameters.get("fftframesize");
		mindB = (Variable)parameters.get("mindb");
		maxdB = (Variable)parameters.get("maxdb");
		minHz = (Variable)parameters.get("minhz");
		maxHz = (Variable)parameters.get("maxhz");
		
		Dimension size = new Dimension(300, 300);
		getJComponent().setPreferredSize(size);
		
		init();
	}
	
	public void run() {
		
		if(input == null) return;

		super.run();

		double samplerate = 44100;
		int channels = 1;
		double timebase = 0.1; // 100 msec / 4410 bytes
		int fftframesize = 1024;

		double mindB = -180;
		double maxdB = 0;
		double minHz = 0;
		double maxHz = samplerate / 2;
		
		if(this.samplerate != null) samplerate = DoublePart.asDouble(this.samplerate);
		if(this.channels != null) channels = (int)DoublePart.asDouble(this.channels);
		if(this.timebase != null) timebase = DoublePart.asDouble(this.timebase);
		if(this.fftframesize != null) fftframesize = (int)DoublePart.asDouble(this.fftframesize);
		if(this.mindB != null) mindB = (int)DoublePart.asDouble(this.mindB);
		if(this.maxdB != null) maxdB = (int)DoublePart.asDouble(this.maxdB);
		if(this.minHz != null) minHz = (int)DoublePart.asDouble(this.minHz);
		if(this.maxHz != null) maxHz = (int)DoublePart.asDouble(this.maxHz);
				
		double rangedB = maxdB - mindB;
		
		fftframesize = (int)Math.pow(2, Math.ceil(Math.log(fftframesize)/Math.log(2)));		
		int fftframesizec = fftframesize * channels;
		int fftframesize2 = fftframesize / 2;
		
		int binfrom2 = (int)((minHz * fftframesize2 * 2.0) / (samplerate));
		int binto2   = (int)((maxHz * fftframesize2 * 2.0) / (samplerate));
		if(binfrom2 > fftframesize2) binfrom2 = fftframesize2;
		if(binto2   > fftframesize2) binto2 = fftframesize2;
		if(binfrom2 < 0) binfrom2 = 0;
		if(binto2   < 0) binto2 = 0;
		if(binfrom2 >= binto2)
		{
			binfrom2 = 0;
			binto2 = fftframesize2;
		}
		int binfrom  = binfrom2 * 2;
		int binto    = binto2 * 2;
		int binlen2  = binto2 - binfrom2;
		
		int monitorsamples = (int)(samplerate * timebase);
		int monitorsamplesc = monitorsamples * channels;
		
		AudioSession audiosession = new AudioSession(samplerate, channels);												
		AudioStream audiostream = AudioEvents.openStream(input, audiosession);
	
		FFT fft = new FFT(fftframesize);
		double[][] fftbuffer = new double[channels][];
		double[][] graphbuffer = new double[channels][];
		
		for (int c = 0; c < channels; c++) {
			fftbuffer[c] = new double[fftframesize];
			graphbuffer[c] = new double[binlen2]; // fftframesize2
		}		
		double[] window = fft.wHanning();		
		int bufferlen = monitorsamplesc;
		if(bufferlen < fftframesizec) bufferlen = fftframesizec;
		double[] buffer = new double[bufferlen];

		while(isActive())
		{
			// Read audio buffer
			for (int i = 0; i < bufferlen - monitorsamplesc; i++) 
				buffer[i] = buffer[i + monitorsamplesc];					
			int len = audiostream.replace(buffer, bufferlen - monitorsamplesc, bufferlen);			
			if(len == -1) break;
			Arrays.fill(buffer, bufferlen - monitorsamplesc + len, bufferlen, 0);
			
			// Do FFT transform
			for (int c = 0; c < channels; c++) {
				double[] fftbuff = fftbuffer[c];
				int ci = c;
				for (int i = 0; i < fftframesize; i++) {
					fftbuff[i] = buffer[ci] * window[i];
					ci += channels;
				}
				fft.calcReal(fftbuff, -1);
			}			

			for (int c = 0; c < channels; c++) {
				double[] fftbuff = fftbuffer[c];
				double[] gbuffer = graphbuffer[c];				
				int x = 0;
				for (int i = binfrom; i < binto; x++ ) {
					double real = fftbuff[i++];
					double imag = fftbuff[i++];
					double magn = 4.0 * Math.sqrt(real*real + imag*imag)/fftframesize;					
					double logman = ((Math.log10(magn)*20.0) - mindB) / (rangedB);
					gbuffer[x] = logman;
				}		

				
			}			
			
			paint(graphbuffer);
			
		}
		
		
		audiostream.close();
		audiosession.close();		
	}
}

public class AudioSpectrumGraph extends ControlUnitFactory {

	public AudioSpectrumGraph()
	{
		registerParameter(1, "timebase");
		registerParameter(2, "control");
		registerParameter(3, "samplerate");
		registerParameter(4, "channels");
	}
	
	public MetaData getMetaData()
	{
		MetaData metadata = super.getMetaData();
		metadata.setDescription("Spectrum Graph");
		metadata.add(1, "timebase",		 "Timebase",		"0.1", "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "control",		 "Control",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(3, "samplerate",	 "Sample rate",	"44100", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "channels",		 "Channels",		"1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "mindb",	     "Minimum energy", "-180", "dB", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "maxdb",	     "Maximum energy", "0", "dB", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "minhz",	     "Minimum frequency", "0", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "maxhz",	     "Maximum frequency", null, "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "fftframesize", "FFT Frame Size", "1024", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "refreshrate",	 "Refresh rate",	"15", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "autostart",	 "Auto Start",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		
		List options = new ArrayList();
		options.add("0");
		options.add("1");
		options.add("2");
		metadata.add(-1, "mode",	 	 "Mode",	"0", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN).put("options", options);
		
		
		return metadata;
	}	
	
	public ControlInstance newControlInstance(NameSpace namespace, Map parameters)
	{
		return new AudioSpectrumGraphInstance(parameters);
	}	
	
}
