/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.controls.sampled;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.controls.ControlUnitFactory;
import rasmus.interpreter.controls.ControlInstance;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;
import rasmus.interpreter.sampled.util.FFT;


class AudioCepstrumGraphInstance extends AudioAbstractGraphAnalyzer 
{

	Variable input;
	Variable timebase;
	Variable samplerate;
	Variable channels;
	Variable fftframesize;

	Variable mindB;
	Variable maxdB;
	Variable minHz;
	Variable maxHz;
	
	public AudioCepstrumGraphInstance(NameSpace namespace, Map parameters) {
		super(parameters);
		
		timebase = (Variable)parameters.get("timebase");
		input = (Variable)parameters.get("input");
		samplerate = (Variable)parameters.get("samplerate");
		channels = (Variable)parameters.get("channels");
		fftframesize = (Variable)parameters.get("fftframesize");
		
		Dimension size = new Dimension(300, 300);
		getJComponent().setPreferredSize(size);
		
		init();
	}
	
	public void run() {
	
		if(input == null) return;
		
		super.run();
		
		double samplerate = 44100;
		int channels = 1;
		double timebase = 0.1; // 100 msec / 4410 bytes
		int fftframesize = 1024;

		if(this.samplerate != null) samplerate = DoublePart.asDouble(this.samplerate);
		if(this.channels != null) channels = (int)DoublePart.asDouble(this.channels);
		if(this.timebase != null) timebase = DoublePart.asDouble(this.timebase);
		if(this.fftframesize != null) fftframesize = (int)DoublePart.asDouble(this.fftframesize);
				
		
		fftframesize = (int)Math.pow(2, Math.ceil(Math.log(fftframesize)/Math.log(2)));		
		int fftframesizec = fftframesize * channels;
		int fftframesize2 = fftframesize / 2;
		int fftframesize4 = fftframesize / 4;
		
		int monitorsamples = (int)(samplerate * timebase);
		int monitorsamplesc = monitorsamples * channels;
		
		AudioSession audiosession = new AudioSession(samplerate, channels);												
		AudioStream audiostream = AudioEvents.openStream(input, audiosession);
	
		FFT fft  = new FFT(fftframesize);
		FFT fft2 = new FFT(fftframesize/2);
		
		double[][] fftbuffer = new double[channels][];
		double[][] fftbuffer2 = new double[channels][];
		double[][] graphbuffer = new double[channels][];
		
		for (int c = 0; c < channels; c++) {
			fftbuffer[c] = new double[fftframesize];
			fftbuffer2[c] = new double[fftframesize2];
			graphbuffer[c] = new double[fftframesize4]; // fftframesize2
		}		
		double[] window = fft.wHanning();		
		int bufferlen = monitorsamplesc;
		if(bufferlen < fftframesizec) bufferlen = fftframesizec;
		double[] buffer = new double[bufferlen];

		while(isActive())
		{
			// Read audio buffer
			for (int i = 0; i < bufferlen - monitorsamplesc; i++) 
				buffer[i] = buffer[i + monitorsamplesc];					
			int len = audiostream.replace(buffer, bufferlen - monitorsamplesc, bufferlen);			
			if(len == -1) break;
			Arrays.fill(buffer, bufferlen - monitorsamplesc + len, bufferlen, 0);
			
			// Do FFT transform
			for (int c = 0; c < channels; c++) {
				double[] fftbuff = fftbuffer[c];
				int ci = c;
				for (int i = 0; i < fftframesize; i++) {
					fftbuff[i] = buffer[ci] * window[i];
					ci += channels;
				}
				fft.calcReal(fftbuff, -1);
				
				double[] fftbuff2 = fftbuffer2[c];
				for (int i = 0, j = 0; j < fftframesize2; j++) {
					double real = fftbuff[i++];
					double imag = fftbuff[i++];		
					fftbuff2[j] = Math.log( Math.sqrt(real*real + imag*imag) ); 
				}
				
				fft2.calcReal(fftbuff2, -1);
			}			

			for (int c = 0; c < channels; c++) {
				double[] fftbuff2 = fftbuffer2[c];
				double[] gbuffer = graphbuffer[c];				
				int x = 0;
				for (int i = 0; i < fftframesize2; x++ ) {
					double real = fftbuff2[i++];
					double imag = fftbuff2[i++];
					double magn = Math.sqrt(real*real + imag*imag) / fftframesize2;					
					//double logman = 1 + (Math.log10(magn/((double)fftframesize2))*2) / 2; //((Math.log10(magn)*20.0) - mindB) / (rangedB);
					gbuffer[x] = 1 + Math.log10(magn)/2.0; //logman;
				}		

				
			}			
			
			paint(graphbuffer);
			
		}
		
		
		audiostream.close();
		audiosession.close();		
	}
}

public class AudioCepstrumGraph extends ControlUnitFactory {

	public AudioCepstrumGraph()
	{
		registerParameter(1, "timebase");
		registerParameter(2, "control");
		registerParameter(3, "samplerate");
		registerParameter(4, "channels");
	}
	
	public MetaData getMetaData()
	{
		MetaData metadata = super.getMetaData();
		metadata.setDescription("Cepstrum Graph");
		metadata.add(1, "timebase",		 "Timebase",		"0.1", "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "control",		 "Control",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(3, "samplerate",	 "Sample rate",	"44100", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "channels",		 "Channels",		"1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "fftframesize", "FFT Frame Size", "1024", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "refreshrate",	"Refresh rate",	"15", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "autostart",	 "Auto Start",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		
		List options = new ArrayList();
		options.add("0");
		options.add("1");
		options.add("2");
		metadata.add(-1, "mode",	 	 "Mode",	"0", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN).put("options", options);
		
		
		return metadata;
	}	
	
	public ControlInstance newControlInstance(NameSpace namespace, Map parameters)
	{
		return new AudioCepstrumGraphInstance(namespace, parameters);
	}	
	
}
