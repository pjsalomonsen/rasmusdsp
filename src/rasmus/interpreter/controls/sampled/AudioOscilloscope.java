/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package rasmus.interpreter.controls.sampled;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.util.Arrays;
import java.util.Map;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.controls.ControlUnitFactory;
import rasmus.interpreter.controls.ControlInstance;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.metadata.MetaData;
import rasmus.interpreter.sampled.AudioEvents;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.AudioStream;

class AudioOscilloscopeInstance extends AudioAbstractAnalyzer 
{

	Variable input;
	Variable timebase;
	Variable samplerate;
	Variable channels;
	Variable refreshrate;
	
	public AudioOscilloscopeInstance(Map parameters) {
		super(parameters);
		
		timebase = (Variable)parameters.get("timebase");
		input = (Variable)parameters.get("input");
		samplerate = (Variable)parameters.get("samplerate");
		channels = (Variable)parameters.get("channels");
		refreshrate = (Variable)parameters.get("refreshrate");
		
		Dimension size = new Dimension(300, 300);
		getJComponent().setPreferredSize(size);
		
		init();
	}

	public void run() {

		if(input == null) return;
		
		double samplerate = 44100;
		double refreshrate = 15; // 15 per second
		int channels = 1;
		double timebase = 0.01; // 10 msec
		if(this.samplerate != null) samplerate = DoublePart.asDouble(this.samplerate);
		if(this.channels != null) channels = (int)DoublePart.asDouble(this.channels);
		if(this.timebase != null) timebase = (int)DoublePart.asDouble(this.timebase);
		if(this.refreshrate != null) refreshrate = (int)DoublePart.asDouble(this.refreshrate);
		
		int monitorsamples = (int)(samplerate * timebase);
		int monitorsamplesc = monitorsamples * channels;
		
		AudioSession audiosession = new AudioSession(samplerate, channels);												
		AudioStream audiostream = AudioEvents.openStream(input, audiosession);
		
		int bufferlen = monitorsamplesc;
		if(bufferlen < 500)
		{
			bufferlen = 500 - (500 % channels);			
		}
		double[] buffer = new double[bufferlen];
		
		long waittimens = (int)(1000 / refreshrate);
		long lastms = System.currentTimeMillis();
		endofrun:
		while(isActive())
		{
			while(System.currentTimeMillis() - lastms < waittimens)
			{
			int len = audiostream.replace(buffer, 0, bufferlen);
			if(len == -1) break endofrun;
			if(len != bufferlen)
			{
				Arrays.fill(buffer, len, bufferlen, 0);
			}
			}
			lastms = System.currentTimeMillis();

			Graphics2D g = getGraphics();
			Dimension size = getSize();
			int w = size.width;
			int h = size.height;		
			g.setColor(Color.BLACK);
			g.fillRect(0,0,w,h);			
			g.setColor(Color.GRAY);
			g.drawLine(0, h/2, w, h/2);
			g.drawLine(w/2, 0, w/2, h);
			
			for (int c = 0; c < channels; c++) {
				int lastx = 0;
				int lasty = h/2;
				double x_step = (double)w / (double)monitorsamples; 
				double x = 0;
				if(c == 0) 
					g.setColor(Color.CYAN);
				else if(c == 1)
					g.setColor(Color.MAGENTA);
				else 
					g.setColor(Color.YELLOW);
				
				for (int i = c; i < monitorsamplesc; i+=channels, x += x_step) {
					double sample = buffer[i];
					int i_x = (int)x;					
					int y = (int)((sample * 0.5 + 0.5) * h);
					
					g.drawLine(lastx, lasty, i_x, y);
					
					lasty = y;
					lastx = i_x;
				}			
			}
			
			repaint();
		}
			
		audiostream.close();
		audiosession.close();
	}

}

public class AudioOscilloscope extends ControlUnitFactory {

	public AudioOscilloscope()
	{
		registerParameter(1, "timebase");
		registerParameter(2, "control");
		registerParameter(3, "samplerate");
		registerParameter(4, "channels");
	}
	

	public MetaData getMetaData()
	{
		MetaData metadata = super.getMetaData();
		metadata.setDescription("Oscilloscope");
		metadata.add(1, "timebase",		"Timebase",		"0.01", "sec", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(2, "control",		"Control",		null, null, MetaData.TYPE_MIDI, MetaData.DIRECTION_IN);
		metadata.add(3, "samplerate",	"Sample rate",	"44100", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(4, "channels",		"Channels",		"1", null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "refreshrate",	"Refresh rate",	"15", "Hz", MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);
		metadata.add(-1, "autostart",	"Auto Start",	null, null, MetaData.TYPE_NUMBER, MetaData.DIRECTION_IN);

		
		return metadata;
	}
		
	public ControlInstance newControlInstance(NameSpace namespace, Map parameters)
	{
		return new AudioOscilloscopeInstance(parameters);
	}
}