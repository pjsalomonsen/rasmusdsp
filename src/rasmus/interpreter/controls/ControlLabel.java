/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package rasmus.interpreter.controls;

import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.metadata.MetaData;

class ControlLabelInstance extends ControlAdapter implements Commitable
{
	JLabel label;
	Variable text;
	NameSpace namespace;
	public ControlLabelInstance(NameSpace namespace, Map parameters)
	{		
		this.namespace = namespace;
		label = new JLabel();
		label.setText("test123");
		
		text = (Variable)parameters.get("text");
		if(text != null)
			ObjectsPart.getInstance(text).addListener(this);
		
		commit();
	}
	
	public JComponent getJComponent() {
		return label;
	}
	
	public void close() {
		if(text != null)
			ObjectsPart.getInstance(text).removeListener(this);			
		text = null;
	}
	
	String s_text;
	public void calc()
	{				
		namespace.addToCommitStack(this);
	}
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit()
	{
		if(text == null) return;
				
		s_text = ObjectsPart.toString(text);
		Runnable runnable = new Runnable()
		{
		public void run()
		{
			label.setText(s_text);
		}};
		
		SwingUtilities.invokeLater(runnable);
		
	}
}

public class ControlLabel extends ControlUnitFactory {

	public ControlLabel()
	{
		registerParameter(1, "text");
		registerParameter("input", "text");
	}
	
	public MetaData getMetaData()
	{
		MetaData metadata = super.getMetaData();
		metadata.setDescription("Label");
		metadata.add(1, "text",		"Text",		null, null, MetaData.TYPE_STRING, MetaData.DIRECTION_IN);
		return metadata;
	}
	
	public ControlInstance newControlInstance(NameSpace namespace, Map parameters)
	{
		return new ControlLabelInstance(namespace, parameters);
	}
}