/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.metadata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MetaData {
	
	public static final int TYPE_UNKNOWN = 0;
	public static final int TYPE_NUMBER  = 1;
	public static final int TYPE_STRING  = 2;
	public static final int TYPE_MIDI    = 4;
	public static final int TYPE_AUDIO   = 8;
	public static final int TYPE_AUDIO_AND_NUMBER = 9;
	public static final int TYPE_CONTROL = 16; // e.g. visual control
	
	public static final int DIRECTION_UNKNOWN = 0;
	public static final int DIRECTION_IN = 1;
	public static final int DIRECTION_OUT = 2;
	public static final int DIRECTION_INOUT = 3;
	
	ArrayList parameters = new ArrayList();
	
	String description;
	
	boolean has_varargs = false;
	
	public void setHasVarargs(boolean value)
	{
		has_varargs = value;
	}
	
	public boolean hasVarargs()
	{
		return has_varargs; 
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public MetaData(String description)
	{
		this.description = description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public List getParameters()
	{
		return parameters;
	}

	public Map add(int number, String name, String description, String defaultvalue, String unit, int paramtype, int direction)
	{
		HashMap map = new HashMap();
		if(number > 0)
		map.put("number", new Integer(number));
		if(name != null) map.put("name", name.toLowerCase());
		map.put("type", new Integer(paramtype));
		map.put("direction", new Integer(direction));
		if(description != null) map.put("description", description);
		if(unit != null) map.put("unit", unit);
		if(defaultvalue != null) map.put("default", defaultvalue);
		parameters.add(map);
		return map;
	}

}
