/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.metadata;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.parser.ScriptTokenParser;
import rasmus.interpreter.unit.Unit;

public class FunctionCallEditor extends JDialog {
	
	private GridBagConstraints constrains = new GridBagConstraints();
	{
		constrains.gridy = 0;
		constrains.anchor = GridBagConstraints.WEST;
		constrains.insets = new Insets(3,3,3,3);
	}
	
	private class ParameterEditor 
	{
		private static final long serialVersionUID = 1L;

		JCheckBox checkbox = new JCheckBox();
		JTextField textfield = null;
		JComboBox combobox = null;
		
		public void setValue(String value)
		{
			checkbox.setSelected(true);
			if(textfield != null)
			{
				textfield.setEnabled(true);
				textfield.setText(value);
			}
			if(combobox != null)
			{
				combobox.setEnabled(true);
				combobox.setSelectedItem(value);
			}
		}
		
		public String getValue()
		{
			if(!checkbox.isSelected()) return null;
			if(textfield != null)
				return textfield.getText();
			if(combobox != null)
				return combobox.getSelectedItem().toString();
			return null;
		}
		
		String name = "";
		int number = -1;
		
		public ParameterEditor(Map param_metadata)
		{
			
			
			Integer number = (Integer)param_metadata.get("number");
			String name = (String)param_metadata.get("name");
			//Integer type = (Integer)param_metadata.get("type");
			String description = (String)param_metadata.get("description");
		    String defaultval = (String)param_metadata.get("default");
		    String unit = (String)param_metadata.get("unit");
		    List options = (List)param_metadata.get("options");
		    		    
		    
		    if(options != null)
		    {
		    	combobox = new JComboBox(options.toArray());
		    	combobox.setEditable(true);
		    }
		    else
		    {
		    	textfield = new JTextField();		    	
		    	textfield.setPreferredSize(new Dimension(30, textfield.getPreferredSize().height));
		    }
		    		    
		    this.name = name;
		    if(number != null)
		    	this.number = number.intValue();
		    
		    if(description == null) description = name;
		    
		    String text = name;
		    if(number != null)
		    	text = number + ". " + text;
			
			checkbox.setText(description);
			
			checkbox.addActionListener(new ActionListener()
					{

						public void actionPerformed(ActionEvent e) {
							if(textfield != null)
							   textfield.setEnabled(checkbox.isSelected());
							if(combobox != null)
								combobox.setEnabled(checkbox.isSelected());
						}
				
					});
			constrains.gridx = 0;
			
			paramspanel.add(checkbox, constrains);
			
			constrains.gridx = 1;
			constrains.weightx = 1;
			constrains.fill = GridBagConstraints.HORIZONTAL;
			if(defaultval == null) defaultval = "";
			
			if(textfield != null)
			{
				textfield.setText(defaultval);
				textfield.setEnabled(false);
				paramspanel.add(textfield, constrains);
			}
			if(combobox != null)
			{
				combobox.setSelectedItem(defaultval);
				combobox.setEnabled(false);
				paramspanel.add(combobox, constrains);
			}
			
			
			constrains.fill = GridBagConstraints.NONE;
			constrains.weightx = 0;

			constrains.gridx = 2;
			if(unit != null)
			paramspanel.add(new JLabel(unit), constrains);

			
			constrains.gridx = 3;
			paramspanel.add(new JLabel("   "+text+"   "), constrains);
			
			
			constrains.gridy++;			
		}
	}

	private static final long serialVersionUID = 1L;
	
	//private RNameSpace namespace;
	//private String funcName;
	private JPanel paramspanel = new JPanel();
	private GridBagLayout paramspanel_layout = new GridBagLayout();
	{
		paramspanel.setLayout(paramspanel_layout);
	}
	
	public static boolean isEditableFunction(String funcName, NameSpace namespace)
	{
		if(funcName.contains("("))
		{
			String[] tokens = ScriptTokenParser.parseFunctionCall(funcName);
			if(tokens == null) return false;
			funcName = tokens[0];
			
		}
		List<MetaData> metadata = Unit.getInstance(namespace.get(funcName)).getMetaData();
		if(metadata != null)
		{
			return true;
				
		}
		return false;
	}
	
	public static String editFunctionCall(Object owner, String expression, NameSpace namespace)
	{
		String[] tokens = ScriptTokenParser.parseFunctionCall(expression);
		
		if(!FunctionCallEditor.isEditableFunction(tokens[0], namespace))
		{
			return null;
		}
			
		FunctionCallEditor fce;
		if(owner instanceof Frame)
			fce = new FunctionCallEditor((Frame)owner, namespace, tokens[0]);
		else
		if(owner instanceof Dialog)
			fce = new FunctionCallEditor((Dialog)owner, namespace, tokens[0]);
		else
			return null;
						
		ArrayList outsideNamedParams = new ArrayList();
		ArrayList outsideParams = new ArrayList();
		
		if(!(tokens.length == 2 && tokens[1].trim().length() == 0))
		{
		
		int param_no = 1;
		for (int i = 1; i < tokens.length; i++) {
			String token = tokens[i];
			String[] pair = ScriptTokenParser.seperateParamNameAndValue(token);
			pair[1] = pair[1].trim();
			if(pair[0] != null)
			{
				if(!fce.setParameter(pair[0].trim(), pair[1]))
				{
					pair[0] = pair[0].trim();
					outsideNamedParams.add(pair);
				}
			}
			else
			{				
				if(!fce.setParameter(param_no, pair[1]))
				{
					pair[0] = Integer.toString(param_no);
					outsideParams.add(pair);
				}
				param_no++;				
			}			
		}
		
		}
		
		fce.setVisible(true);

		if(fce.isOK())
		{
			
			ArrayList editors = new ArrayList(fce.params_editor);

			StringBuffer sb = new StringBuffer();
			sb.append(tokens[0] + "(");
			// 1. process numbered params
			int param_no = 1;
			boolean first_param = true;
			while(true)
			{			
				String value = null;
				
				for (int i = 0; i < outsideParams.size(); i++) {
					String[] pair = (String[])outsideParams.get(i);
					if(pair[0].equals(Integer.toString(param_no)))
					{
						value = pair[1];
						outsideParams.remove(i);
						break;
					}
				}
				
				if(value == null)
				for (int i = 0; i < editors.size(); i++) {
					if(((ParameterEditor)editors.get(i)).number == param_no)
					{
						value = ((ParameterEditor)editors.get(i)).getValue();
						editors.remove(i);
						if(value != null) break;
					}
				}
				
				if(value == null) break;
				
				if(!first_param) sb.append(","); else first_param = false;
				sb.append(value);
				
				param_no++;
			}
				
				
			// 2. process named params								
		
			for (int i = 0; i < outsideParams.size(); i++) {
				String[] pair = (String[])outsideParams.get(i);
				if(!first_param) sb.append(","); else first_param = false;
				sb.append(pair[0].trim() + "=" + pair[1]); 
			}
			
			for (int i = 0; i < outsideNamedParams.size(); i++) {
				String[] pair = (String[])outsideNamedParams.get(i);
				if(!first_param) sb.append(","); else first_param = false;
				sb.append(pair[0].trim() + "=" + pair[1]); 
			}			
			
			for (int i = 0; i < editors.size(); i++) {
				String value = ((ParameterEditor)editors.get(i)).getValue();				
				if(value != null)
				{
					String name = ((ParameterEditor)editors.get(i)).name;
					if(!first_param) sb.append(","); else first_param = false;
					sb.append(name + "=" + value);
				}
			}			
			
			sb.append(")");
			return sb.toString();	
		}
		return null;
		
	}
	
	private ArrayList params_added = new ArrayList();	
	private ArrayList params_metadata = new ArrayList();
	
	private ArrayList params_editor = new ArrayList();
	
	private boolean setParameter(int param, String value)
	{
		Iterator iterator = params_editor.iterator();
		while (iterator.hasNext()) {
			ParameterEditor param_editor = (ParameterEditor) iterator.next();
			if(param_editor.number == param)
			{
				param_editor.setValue(value.trim());
				return true;
			}
		}
		return false;
	}
	
	private boolean setParameter(String name, String value)
	{
		Iterator iterator = params_editor.iterator();
		while (iterator.hasNext()) {
			ParameterEditor param_editor = (ParameterEditor) iterator.next();
			if(name.equals(param_editor.name))
			{
				param_editor.setValue(value.trim());
				return true;
			}
		}
		return false;
	}
	
	private void addParameter(Map param_metadata)
	{
		Integer number = (Integer)param_metadata.get("number");
		String name = (String)param_metadata.get("name");
		if(name != null)
		{
			if(params_added.contains(name)) return;
			params_added.add(name);
		}
		if(number != null)
		{
			if(params_added.contains(number)) return;
			params_added.add(number);
		}

		params_metadata.add(param_metadata);
		
		
		//System.out.println(map.get("number")+ ": " + map.get("name"));
		
	}
	
	private void addParamSeperator()
	{
		
		constrains.gridx = GridBagConstraints.REMAINDER;
		constrains.gridwidth = GridBagConstraints.REMAINDER;
		constrains.weightx = 1;
		constrains.fill = GridBagConstraints.HORIZONTAL;		
		paramspanel.add(new JSeparator(JSeparator.HORIZONTAL), constrains);
		constrains.fill = GridBagConstraints.NONE;
		constrains.gridwidth = 1;
		constrains.weightx = 0;		
		constrains.gridx = 0;
		constrains.gridy++;		
	}
	
	private boolean isok = false;
	private boolean isOK()
	{
		return isok;
	}
	private void ok()
	{
		isok = true;
	}
	
	private FunctionCallEditor(Frame owner, NameSpace namespace, String funcName)
	{
		super(owner);
		init(namespace, funcName);
	}
	
	private FunctionCallEditor(Dialog owner, NameSpace namespace, String funcName)
	{
		super(owner);
		init(namespace, funcName);
	}

	private void init(NameSpace namespace, String funcName)
	{
		
		//this.namespace = namespace;
		//this.funcName = funcName;
		

		
		
		setSize(640, 400);	
		Dimension size = getSize();		
		Dimension screenSize = getParent().getSize();
		Point parent_loc = getParent().getLocation();			
		setLocation(parent_loc.x + screenSize.width/2 - (size.width/2),
				    parent_loc.y + screenSize.height/2 - (size.height/2));
				
		
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		panel.setLayout(new BorderLayout());
		setContentPane(panel);
		
		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		panel.registerKeyboardAction(new ActionListener()
				{
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				}
				, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		
		
		
		JPanel buttonpanel = new JPanel();
		panel.add(buttonpanel, BorderLayout.SOUTH);
		buttonpanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		JButton ok_button = new JButton("OK");
		JButton cancel_button = new JButton("Cancel");
		ok_button.setDefaultCapable(true);		
		ok_button.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e) {
						ok();
						setVisible(false);
					}
				});		
		cancel_button.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
		
		buttonpanel.add(ok_button);
		buttonpanel.add(cancel_button);		
		

		panel.add(new JScrollPane(paramspanel), BorderLayout.CENTER);
		
		
		String title = funcName.toLowerCase();
		Variable variable = namespace.get(funcName);
		List<MetaData> metadata = Unit.getInstance(variable).getMetaData();
		if(metadata != null)
		{
			boolean firstone = true;
			Iterator iterator = metadata.iterator();
			while (iterator.hasNext()) {
				MetaData metadatai = (MetaData) iterator.next();
				if(metadatai.getDescription() != null)
					if(firstone)
					{
						title += " - " + metadatai.getDescription();
						firstone = false;
					}
					else
					title += " , " + metadatai.getDescription();
				
				Iterator piterator = metadatai.getParameters().iterator();
				while (piterator.hasNext()) {
					Map param_metadata = (Map) piterator.next();
					addParameter(param_metadata);
				}
			}
		}	
		
		Object[] params = params_metadata.toArray();
		
		for (int i = 0; i < params.length; i++) {
			Map param_metadata = (Map)params[i];
			if(param_metadata.get("number") != null)
			{
				ParameterEditor param_editor = new ParameterEditor(param_metadata);
				params_editor.add(param_editor);				
			}
		}
		
		boolean numbseperator_added = params_editor.size() == 0;
		
		for (int i = 0; i < params.length; i++) {
			Map param_metadata = (Map)params[i];
			if(param_metadata.get("number") == null)
			{
				String name = (String)param_metadata.get("name");
				if(!"output".equals(name))
				if(!"input".equals(name))
				{
					if(!numbseperator_added)
					{
						addParamSeperator();
						numbseperator_added = true;
					}									
					ParameterEditor param_editor = new ParameterEditor(param_metadata);
					params_editor.add(param_editor);
				}
			}
		}		
		
		boolean inout_added = params_editor.size() == 0;
		for (int i = 0; i < params.length; i++) {
			Map param_metadata = (Map)params[i];
			if(param_metadata.get("number") == null)
			if("input".equals(param_metadata.get("name")))
			{
				if(!inout_added)
				{
					addParamSeperator();
					inout_added = true;
				}				
				ParameterEditor param_editor = new ParameterEditor(param_metadata);
				params_editor.add(param_editor);				
			}
		}
		for (int i = 0; i < params.length; i++) {
			Map param_metadata = (Map)params[i];
			if(param_metadata.get("number") == null)
			if("output".equals(param_metadata.get("name")))
			{
				if(!inout_added)
				{
					addParamSeperator();
					inout_added = true;
				}				
				ParameterEditor param_editor = new ParameterEditor(param_metadata);
				params_editor.add(param_editor);				
			}
		}
		
		
		constrains.weighty = 1;
		paramspanel.add(new JPanel(), constrains);
		
        getRootPane().setDefaultButton(ok_button);

		
		setTitle(title);
		
		setModal(true);
		
		
	}
}
