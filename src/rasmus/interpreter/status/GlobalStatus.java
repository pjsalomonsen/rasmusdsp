package rasmus.interpreter.status;

public class GlobalStatus {

	private static StatusListener statuslistener = null;

	public static StatusListener getStatusListener() {
		return statuslistener;
	}	
	public static void setStatusListener(StatusListener fstatuslistener) {
		statuslistener = fstatuslistener;
	}

	public static void setStatus(String string) {
		if (statuslistener != null)
			statuslistener.setStatus(string);
	}

}
