/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.namespace;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Enumeration;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.ext.ExternalJava;
import rasmus.interpreter.io.ResourceManager;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.spi.InterpreterExtension;
import rasmus.interpreter.struct.Struct;
import rasmus.interpreter.unit.Unit;

public class GlobalNameSpace {
	
	private static Struct global_namespace;

	private static Interpreter global_interpreter;
	
	public static Struct newInstance() {
		
		Struct global_namespace = Struct.getInstance(new Variable());
		global_namespace.setCommitSystem(new CommitSystem());
		global_namespace.put("global", global_namespace.get("this")); 

		global_interpreter = new Interpreter(global_namespace);
		global_interpreter.setAutoCommit(false);
		try {
			global_interpreter.add("sysdir", ObjectsPart.asVariable(new File(".")
					.getCanonicalPath()
					+ File.separator));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ResourceManager.getInstance(global_interpreter);
		
		global_interpreter.add("ExternalJava", Unit
				.asVariable(new ExternalJava()));
		global_interpreter.commit();
		
		try {
			Enumeration extenum = Interpreter.class
					.getClassLoader()
					.getResources(
							"META-INF/services/rasmus.interpreter.spi.InterpreterExtension");
			while (extenum.hasMoreElements()) {
				InputStream extstream = ((URL) extenum.nextElement())
						.openStream();
				BufferedReader r = new BufferedReader(new InputStreamReader(
						extstream));
				String line = r.readLine();
				while (line != null) {
					if (!line.startsWith("#")) {
						try {
							Class c = Class.forName(line.trim());
							Object o = c.newInstance();
							if (o instanceof InterpreterExtension) {
								InterpreterExtension oie = (InterpreterExtension) o;
								oie.execute(global_interpreter);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					line = r.readLine();
				}
				extstream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		//global_interpreter.execute(prepare_script);	  
		try {
			File[] files = new File("ext").listFiles();
			if (files != null)
				for (int i = 0; i < files.length; i++) {
					File file = files[i];
					if (file.isFile())
						if (file.getName().toLowerCase().endsWith(".r")) {
							try {
								global_interpreter.source(file.getPath());
							} catch (ScriptParserException e) {
								e.printStackTrace();
							} 
						}
				}

		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		global_interpreter.commit();		

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {

				global_interpreter.close();
				global_interpreter = null;
			}
		});
		
		return global_namespace;
		
	}	

	public static NameSpace getNameSpace() {
		if(global_namespace == null)
		{
			global_namespace = GlobalNameSpace.newInstance();
		}
		return global_namespace;
	}
	
}
