/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.namespace;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.Map.Entry;

import rasmus.interpreter.Commitable;

public class CommitSystem {

	private TreeMap<Integer, LinkedList<Commitable>> runlevels = new TreeMap<Integer, LinkedList<Commitable>>();		
	private Commitable[] commitables;
	private int commitables_len = 0;
	private int commit_count = 0;
	private boolean iscommiting = false;
	
	private LinkedList<Commitable> getRunLevelList(int runlevel)
	{
		LinkedList<Commitable> list = runlevels.get(runlevel);
		if(list == null)
		{
			list = new LinkedList<Commitable>();
			runlevels.put(runlevel, list);
		}
		return list;
	}	
		
	public void commit() {
				
		synchronized (this) {			
			if(commit_count == 0) return;
			if(iscommiting) return;
			iscommiting = true;
		}
		
		commit_loop:
		while(true)
		{
			synchronized (this) {
				Iterator<Entry<Integer, LinkedList<Commitable>>> iter = runlevels.entrySet().iterator();
				commitables_len = 0;
				while (iter.hasNext()) {
					Entry<Integer, LinkedList<Commitable>> entry = iter.next();
					if(!entry.getValue().isEmpty())
					{
						LinkedList<Commitable> commitstack = entry.getValue();
						if(commitables == null || commitables.length < commitstack.size())
						{
							commitables = new Commitable[commitstack.size()];
						}
						commitables_len = commitstack.size();
						commitstack.toArray(commitables);
						commit_count -= commitables_len;
						commitstack.clear();
												
						break;
					}
				}
				if(commitables_len == 0) break commit_loop;
			}			
			for (int i = 0; i < commitables_len; i++) 
				commitables[i].commit();			
		}
		
		synchronized (this) {
			iscommiting = false;
		}
		
	}

	public void add(Commitable commitable) {
		synchronized (this) {
			LinkedList<Commitable> commitstack = getRunLevelList(commitable.getRunLevel());			
			if(commitstack.remove(commitable)) commit_count--;
			commitstack.offer(commitable);
			commit_count++;
			
		}
	}

}
