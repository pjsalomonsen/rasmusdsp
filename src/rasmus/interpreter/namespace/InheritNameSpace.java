/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.namespace;

import rasmus.interpreter.Closeable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.NameSpaceAdapter;
import rasmus.interpreter.Variable;
import rasmus.interpreter.struct.Struct;

public class InheritNameSpace extends NameSpaceAdapter implements Closeable {
	
	private NameSpace parent;
	private Variable parentvar;
	private Variable var;
	private Struct struct;
	private String namespacename = null;
	
	public InheritNameSpace(NameSpace parent) {
		
		this.parent = parent;
		
		parentvar = parent.get("this");
		var = new Variable();
		struct = Struct.getInstance(var);
		struct.put("parent", parentvar);
		struct.put("global", parent.get("global"));
		struct.add(parentvar);
		struct.setCommitSystem(Struct.getInstance(parentvar).getCommitSystem());
		setNameSpace(struct);		
		
	}
	
	public InheritNameSpace(NameSpace parent, String namespacename) {
		
		this.namespacename = namespacename;
		this.parent = parent;
		
		parentvar = parent.get("this");
		var = new Variable();
		struct = Struct.getInstance(var);
		struct.put("parent", parentvar);
		struct.put("global", parent.get("global"));
		struct.add(parentvar);		
		struct.setCommitSystem(Struct.getInstance(parentvar).getCommitSystem());
		setNameSpace(struct);
		
		if(namespacename != null)
			parent.get(namespacename).add(var);
		
	}

	public void put(String name, Variable var) {
		struct.put(name.toLowerCase(), var);
	}
	
	public void close()
	{
		if(namespacename != null)
			parent.get(namespacename).remove(var);
		
		struct.remove(parentvar);					
	}
	
}
