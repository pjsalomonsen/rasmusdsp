/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.strings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import rasmus.interpreter.Commitable;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.io.Resource;
import rasmus.interpreter.io.ResourceManager;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;


class TextFileInstance extends UnitInstanceAdapter implements Commitable
{	
	Variable output;
	Variable filename;
	
	Variable answer = null;
	NameSpace namespace;
	Variable wrkdir;
	Variable encoding;
	
	ResourceManager manager;
	
	public TextFileInstance(Parameters parameters)
	{
		namespace = parameters.getNameSpace();
		manager = ResourceManager.getInstance(namespace);
		
		output = parameters.getParameterWithDefault("output");
		filename = parameters.getParameterWithDefault(1, "filename");
		encoding = parameters.getParameter(2, "encoding");
		wrkdir = parameters.get("wrkdir");
		
		ObjectsPart.getInstance(filename).addListener(this);
		ObjectsPart.getInstance(wrkdir).addListener(this);
		if(encoding != null) ObjectsPart.getInstance(encoding).addListener(this);
		
		calc();
		
		
	}
	
	public void calc() {
		namespace.addToCommitStack(this);
	}

	public void close() {
		clear();	
		
		if(resource != null)
		{
			resource.close();
			resource  = null;
		}
		ObjectsPart.getInstance(filename).removeListener(this);
		ObjectsPart.getInstance(wrkdir).removeListener(this);
		if(encoding != null) ObjectsPart.getInstance(encoding).removeListener(this);
	}
		
	
	public String expandPath(String path)
	{		
		path = path.trim();
		if(path.length() == 0) return path;
		if(path.startsWith(File.separator)) return path;		
		if(path.length() > 2 && Character.isLetter(path.charAt(0)) && path.charAt(1) == ':' && path.charAt(2) == File.separatorChar)
				return path;
	
		String wrkdir = ObjectsPart.toString(this.wrkdir);
		if(wrkdir.trim().length() == 0)
			return path;					
		
		if(wrkdir.endsWith(File.separator))
		{
			return wrkdir + path;
		}
		else
		{
			return wrkdir + File.separator + path;
		}
	}
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			answer = null;
		}
	}	
	
	Resource resource = null;
	
	public int getRunLevel()
	{
		return RUNLEVEL_DEFAULT;
	}
	
	public void commit()
	{
		clear();
		
		String filename = expandPath(ObjectsPart.toString(this.filename));

		if(resource != null) resource.close();
		resource = manager.getResource(filename);
		if(resource == null) return;
		File file = resource.getFile();
		
		String encoding = null;
		if(this.encoding != null)
			encoding = expandPath(ObjectsPart.toString(this.encoding));
		
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;			
		}
		
		StringBuffer sb = null;
		try {
			try
			{
				Reader reader;
				if(encoding != null)
					reader = new InputStreamReader(fis, encoding);
				else
					reader = new InputStreamReader(fis);
				
				sb = new StringBuffer();				
				BufferedReader breader = new BufferedReader(reader);
				String line = breader.readLine();
				while (line != null) {
					sb.append(line + "\n");
					line = breader.readLine();
				}
				
				reader.close();				
				
			}
			finally
			{
				fis.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		if(sb != null)
		{
			answer = ObjectsPart.asVariable(sb.toString());
			output.add(answer);
		}		
		
	}
	
}

public class TextFile implements UnitFactory {
	public UnitInstancePart newInstance(Parameters parameters) {
		return new TextFileInstance(parameters);
	}
}
