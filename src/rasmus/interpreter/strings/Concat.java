/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.strings;


import java.util.ArrayList;
import java.util.Iterator;

import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.unit.Parameters;
import rasmus.interpreter.unit.UnitFactory;
import rasmus.interpreter.unit.UnitInstanceAdapter;
import rasmus.interpreter.unit.UnitInstancePart;

class ConcatInstance extends UnitInstanceAdapter
{
	ArrayList elements = new ArrayList();
	Variable output;
	Variable answer = null;
	
	public ConcatInstance(Parameters parameters)
	{
		
		output = parameters.getParameterWithDefault("output");
		
		int i = 1;
		Variable val = parameters.getParameter(i);
		while(val != null)
		{
			elements.add(val);
			i++;
			val = parameters.getParameter(i);;
		}
		
		Iterator iterator = elements.iterator();
		while (iterator.hasNext()) {
			Variable var = (Variable) iterator.next();
			ObjectsPart.getInstance(var).addListener(this);
			DoublePart.getInstance(var).addListener(this);
		}
		calc();
	}
	public void close() {
		clear();
		
		Iterator iterator = elements.iterator();
		while (iterator.hasNext()) {
			Variable var = (Variable) iterator.next();
			ObjectsPart.getInstance(var).removeListener(this);
			DoublePart.getInstance(var).removeListener(this);
		}		
		elements.clear();
	}
	
	public void clear()
	{
		if(answer != null)
		{
			output.remove(answer);
			answer = null;
		}
	}
	
	public void calc() {
		clear();
		
		StringBuffer catter = new StringBuffer();
		Iterator iterator = elements.iterator();
		while (iterator.hasNext()) {
			Variable var = (Variable) iterator.next();
			String str; 
			if(ObjectsPart.getInstance(var).getObjects().size() == 0)
			{
				double num = DoublePart.asDouble(var);
				if(Math.abs(Math.round(num) - num) < 0.0000000001)
				{
					str = Long.toString((long)num);	
				}
				else
					str = Double.toString(num);
			}
			else
			{
				str = ObjectsPart.toString(var);
			}
			catter.append(str);
		}			
		
		answer = ObjectsPart.asVariable(catter.toString());
		output.add(answer);
	}
}

public class Concat implements UnitFactory {
	public UnitInstancePart newInstance(Parameters parameters) {
		return new ConcatInstance(parameters);
	}
}
