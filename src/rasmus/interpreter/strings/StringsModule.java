/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.interpreter.strings;

import rasmus.interpreter.NameSpace;
import rasmus.interpreter.ext.Module;
import rasmus.interpreter.ext.ModuleFactory;
import rasmus.interpreter.unit.UnitInstancePart;

public class StringsModule extends ModuleFactory {
	
	class ModuleInstance extends Module
	{		
		public ModuleInstance(NameSpace namespace)
		{
			setNameSpace(namespace);
			add("&", new Concat()); 
			add("concat", new Concat()); 
			add("textfile", new TextFile()); 
			
			// Functions that return: String
			// =============================
			// substring
			// upper
			// lower
			// replaceAll
			// replaceFirst
			
			// Functions that return: Number
			// =============================
			// len
			// indexOf
			// lastIndexOf
			
			// Functions that return: boolean(true = 1, false = 0)
			// ===================================================
			// contains
			// startsWith
			
		}		
	}
	
	public UnitInstancePart newInstance(NameSpace namespace) {
		return new ModuleInstance(namespace);
	}
	
}
