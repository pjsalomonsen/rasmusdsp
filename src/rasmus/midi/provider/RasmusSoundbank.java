/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.midi.provider;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import javax.sound.midi.Instrument;
import javax.sound.midi.Patch;
import javax.sound.midi.Soundbank;
import javax.sound.midi.SoundbankResource;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import rasmus.interpreter.sf2.SF2SoundFont;

public class RasmusSoundbank implements Soundbank {
	
	SF2SoundFont sf2soundfont;
	String filename;
	Element root;
	
	Instrument[] instruments;
	Map<Patch, Instrument> instrumentsmap = new TreeMap<Patch, Instrument>(new Comparator<Patch>()
			{
		public int compare(Patch p1, Patch p2) {
			int i1 = p1.getBank() * 256 + p1.getProgram();
			int i2 = p2.getBank() * 256 + p2.getProgram();
			return i1 - i2;
		}

	});
	
	public RasmusSoundbank(String filename, SF2SoundFont sf2soundfont)
	{
		this.filename = filename;
		this.sf2soundfont = sf2soundfont;
		
		root = sf2soundfont.getDocument().getDocumentElement();
		
		
		NodeList presetlist = root.getElementsByTagName("preset");
		instruments = new Instrument[presetlist.getLength()];
		for (int i = 0; i < presetlist.getLength(); i++) {
			Element preset = (Element)presetlist.item(i);

			
			Patch patch = new Patch( Integer.parseInt(preset.getAttribute("bank")),
			           Integer.parseInt(preset.getAttribute("preset")));
			Instrument ins = new RasmusInstrument(this, patch, preset.getAttribute("name"));
			instruments[i] = ins;
			instrumentsmap.put(patch, ins);
		}
		Arrays.sort(instruments, new Comparator<Instrument>()
				{
			public int compare(Instrument o1, Instrument o2) {
				Patch p1 = o1.getPatch();
				Patch p2 = o2.getPatch();
				int i1 = p1.getBank() * 256 + p1.getProgram();
				int i2 = p2.getBank() * 256 + p2.getProgram();
				return i1 - i2;
			}
	
		} );
		
		
	}
	
	public String getFileName()
	{
		return filename;
	}

	public String getName() {
		return root.getAttribute("bankname");
	}

	public String getVersion() {
		return root.getAttribute("version");
	}

	public String getVendor() {
		return root.getAttribute("engineers");
	}

	public String getDescription() {
		return root.getAttribute("comments");
	}

	public SoundbankResource[] getResources() {
		return new SoundbankResource[0];
	}

	public Instrument[] getInstruments() {
		return instruments;
	}

	public Instrument getInstrument(Patch arg0) {
		return instrumentsmap.get(arg0);
	}

}
