/*
 * Created on 7.3.2007
 *
 * Copyright (c) 2006-2007 Karl Helgaosn
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.midi.provider;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;
import javax.sound.midi.Transmitter;
import javax.swing.Icon;

/*
 *  MultiMidiCast implementation for Java Sound API
 * 
 *  Designed to be compatible with: 
 *  MultiMidiCast (http://llg.cubic.org/tools/multimidicast/) LINUX
 *  and ipMIDI (http://nerds.de/en/ipmidi.html) WINDOWS
 *  
 */
class MultiMidiCastTransmitter implements Transmitter
{
	
	class MultiMidiCastServerThread extends Thread	
	{
		MulticastSocket ms;
		boolean active = true;
		public void stopServer()
		{
			active = false;
			interrupt();
			try
			{
				ms.leaveGroup(ia);		
				ms.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		public void run()
		{
			try
			{
				ms = new MulticastSocket(21928 + port);
				ms.setInterface(dev.localhost);
				ms.joinGroup(ia);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				ms = null;
			}
			
			DatagramPacket dp = new DatagramPacket(new byte[1280], 1280);					
			while(active)
			{
				try
				{
					ms.receive(dp);
					if(dp.getLength() != 0 )
						encodeMidiBuffer(dp.getData(), dp.getLength());
				}
				catch(Exception e)
				{				
				}
			}
			ms = null;
		}
	}
	
	MultiMidiCastDevice dev;
	Receiver recv = null;
	int port;
	boolean isOpen = true;
	InetAddress ia;	
	MultiMidiCastServerThread mthread = null;
	boolean started = false;
	
	public MidiMessage encodeMidiBuffer(byte[] data, int offset, int len)
	{
		try
		{	
			
			int statusbyte = data[0];
			if(statusbyte == ((byte)0xF7))
			{
				SysexMessage sysex = new SysexMessage();
				byte[] mydata = new byte[len];
				for (int i = 0; i < len; i++) {
					mydata[i] = data[i + offset];
				}
				try
				{
					sysex.setMessage(statusbyte, mydata, mydata.length);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				return sysex;
			}
			else
			{
				ShortMessage shortmsg = new ShortMessage();
				if(len == 1)			
					shortmsg.setMessage(data[offset] & 0xFF);
				else if(len == 2)
					shortmsg.setMessage(data[offset] & 0xFF, data[offset+1] & 0xFF, 0);	
				else
					shortmsg.setMessage(data[offset] & 0xFF, data[offset+1] & 0xFF, data[offset+2] & 0xFF);
				return shortmsg;
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void send(MidiMessage midimessage)
	{
		if(midimessage == null) return;
		if(recv != null) recv.send(midimessage, -1);
	}
	
	public void encodeMidiBuffer(byte[] data, int len)
	{
		int lasti = 0;
		boolean sysex = false;
		for (int i = 0; i < len; i++) {
			byte d = data[i];
			if(sysex)
			{
				if(d == ((byte)0xF7)) // Sysex Status Byte
				{
					if(i - lasti > 0) send(encodeMidiBuffer(data, lasti, i - lasti));
					sysex = false;
					lasti = i;					
				}
			}
			else
			{
				if(d < 0) // Status Byte
				{
					if(i - lasti > 0) send(encodeMidiBuffer(data, lasti, i - lasti));
					if(d == 0xF0) sysex = true; // Sysex Status Byte
					lasti = i;				
				}
			}
		}
		if(len - lasti > 0) send(encodeMidiBuffer(data, lasti, len - lasti));							
	}
	
	public MultiMidiCastTransmitter(MultiMidiCastDevice dev)
	{
		this.dev = dev;		
		port = dev.info.port;
		dev.transmitters.add(this);
	}	
	public void start()
	{
		if(started) return;
		try
		{
			ia = dev.multicasthost;
			mthread = new MultiMidiCastServerThread();
			mthread.start();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}				
		started = true;
	}
	public void setReceiver(Receiver recv) {
		start();
		this.recv = recv;
	}
	public Receiver getReceiver() {
		return recv;
	}
	public void close() {
		if(!isOpen) return;
		isOpen = false;
		dev.transmitters.remove(this);
		if(mthread != null)
		{
			mthread.stopServer();
			mthread = null;
		}
	}
}

class MultiMidiCastReceiver implements Receiver
{
	MultiMidiCastDevice dev;
	int port;
	boolean isOpen = true;
	MulticastSocket ms = null;
	InetAddress ia;
	public MultiMidiCastReceiver(MultiMidiCastDevice dev)
	{
		this.dev = dev;
		port = dev.info.port;
		dev.receivers.add(this);
		
		try
		{
			ia = dev.multicasthost;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}		
	public void send(MidiMessage arg0, long arg1) {
		if(ms == null) 
		{
			try
			{
				ms = new MulticastSocket();
				ms.setTimeToLive(57);
				ms.setInterface(dev.localhost);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}				
		}
		
		byte[] data = arg0.getMessage();
		DatagramPacket dp = new DatagramPacket(data, data.length, ia, port + 21928);
		try
		{
			ms.send(dp);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	public void close() {
		if(!isOpen) return;
		isOpen = false;
		dev.receivers.remove(this);
		
		try
		{		
			if(ms != null)
			{
				ms.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		ms = null;
	}
}

public class MultiMidiCastDevice implements MidiDevice
{
	
	static InetAddress localhost;
	static {	    
	    new Thread(new Runnable() {
	    @Override
	    public void run() {
		    try {
			localhost = InetAddress.getLocalHost();
			Logger.getLogger(MultiMidiCastDevice.class.getName()).log(Level.INFO, "Multi Midi Cast device interface is {0}", localhost);
		    } catch (UnknownHostException ex) {
			Logger.getLogger(MultiMidiCastDevice.class.getName()).log(Level.SEVERE, null, ex);
		    }
		}
	    }).start();	    
	}
	InetAddress multicasthost;
	
	Vector<Receiver> receivers = new Vector<Receiver>();
	Vector<Transmitter> transmitters = new Vector<Transmitter>();
	
	MultiMidiCastInfo info;
	boolean vIsOpen = false;
		
	private static Icon icon = new javax.swing.ImageIcon(MultiMidiCastDevice.class.getResource("/rasmus/midi/provider/ip_network.png"));
	
	public Icon getIcon()
	{
		return icon;
	}	
	
	public MultiMidiCastDevice(MultiMidiCastInfo info)
	{
		this.info = info;
		try
		{
			
			multicasthost = InetAddress.getByName("225.0.0.37");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public void close() {
		if(!vIsOpen) return;
		vIsOpen = false;
		Object[] recvs = receivers.toArray();
		for (int i = 0; i < recvs.length; i++) {
			((Receiver)recvs[i]).close();
		}
		Object[] trans = transmitters.toArray();
		for (int i = 0; i < trans.length; i++) {
			((Transmitter)trans[i]).close();
		}		
	}
	public Info getDeviceInfo() {
		return info;
	}
	public int getMaxReceivers() {
		return -1;
	}
	public int getMaxTransmitters() {
		return -1;
	}
	public long getMicrosecondPosition() {
		return -1;
	}
	public Receiver getReceiver() throws MidiUnavailableException {
		return new MultiMidiCastReceiver(this);
	}
	public List<Receiver> getReceivers() {
		return receivers;
	}
	public Transmitter getTransmitter() throws MidiUnavailableException {
		return new MultiMidiCastTransmitter(this);
	}
	public List<Transmitter> getTransmitters() {
		return transmitters;
	}
	public boolean isOpen() {
		return vIsOpen;
	}
	public void open() throws MidiUnavailableException {
		if(vIsOpen) return;
		vIsOpen = true;
	}	
	
}
