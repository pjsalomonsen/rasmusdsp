/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.midi.provider;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.spi.MidiDeviceProvider;

class MultiMidiCastInfo extends Info
{
    int port;
    protected MultiMidiCastInfo(int port, String name, String vendor, String description, String version) {
	    super(name, vendor, description, version);
	    this.port = port;
    }
	
}

public class MultiMidiCastProvider extends MidiDeviceProvider {
	
    Info[] infos;
    public MultiMidiCastProvider()
    {
	    infos = new Info[5];
	    for (int i = 0; i < 5; i++) {
		    infos[i] = new MultiMidiCastInfo(0, "MultiMidiCast: " + (i + 1), "Rasmus", "MIDI over UDP Multicast", "");			
	    }

    }
    @Override
    public Info[] getDeviceInfo() {
	    return infos;
    }

    @Override
    public MidiDevice getDevice(Info arg0) {
	    if(!(arg0 instanceof MultiMidiCastInfo)) return null;
	    return new MultiMidiCastDevice((MultiMidiCastInfo)arg0);
    }

    @Override
    public boolean isDeviceSupported(Info info) {
	if(MultiMidiCastInfo.class.isInstance(info)) {
	    return true;
	} else {
	    return false;
	}
    }
	
	 
}
