/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.midi.provider;

import java.io.Externalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.sound.midi.Instrument;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Patch;
import javax.sound.midi.Receiver;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Transmitter;
import javax.sound.midi.VoiceStatus;
import javax.sound.midi.spi.SoundbankReader;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Control;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.Control.Type;
import javax.swing.Icon;

import rasmus.editor.RasmusEditor;
import rasmus.editor.RasmusSession;
import rasmus.interpreter.Interpreter;
import rasmus.interpreter.Variable;
import rasmus.interpreter.io.Resource;
import rasmus.interpreter.io.ResourceManager;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.sampled.midi.InstrumentRecord;
import rasmus.interpreter.sampled.midi.VoiceRecord;
import rasmus.interpreter.sf2.SF2SoundFont;
import rasmus.interpreter.sf2.SF2SoundFontManager;

public class RasmusSynthesizer extends SoundbankReader implements Synthesizer, Mixer, Externalizable, Cloneable {
			
	private static final long serialVersionUID = 7554146056523862933L;
	
	RasmusEditor editor;
	RasmusSession session;
	Receiver recv;
	
	private static Icon icon = new javax.swing.ImageIcon(RasmusSynthesizer.class.getResource("/rasmus/rasmusdsp.PNG"));
	
	public Icon getIcon()
	{
		return icon;
	}
	
	public RasmusSynthesizer clone() {
		
		editor.commit();
		
		RasmusSynthesizer synth = new RasmusSynthesizer();
		synth.saveReferencedData = saveReferencedData;
		synth.name = name;
		synth.session = new RasmusSession();
		
		ResourceManager.getInstance(synth.session.getNameSpace()).importResources(ResourceManager.getInstance(session.getNameSpace()));		
		
		synth.session.getScriptDocument().setString(session.getScriptDocument().toString());
		synth.editor = new RasmusEditor(synth.session);
		synth.editor.setSynthName(name);
		synth.recv = MidiSequence.getInstance(synth.editor.getRealTimeInput());									
		synth.editor.commit();
		
		return synth;
	}

	public RasmusSynthesizer()
	{
	}

	public int getMaxPolyphony() {
		return -1;
	}

	public long getLatency() {
		return -1;
	}

	public MidiChannel[] getChannels() {
		return null;
	}

	public VoiceStatus[] getVoiceStatus() {
		return null;
	}

	public boolean isSoundbankSupported(Soundbank soundbank) {
		return false;
	}

	public boolean loadInstrument(Instrument instrument) {
		return false;
	}

	public void unloadInstrument(Instrument instrument) {	
	}

	public boolean remapInstrument(Instrument from, Instrument to) {
		return false;
	}

	public Soundbank getDefaultSoundbank() {
		return null;
	}

	public Instrument[] getAvailableInstruments() {
		return new Instrument[0];
	}
	
	public class LoadedInstrument extends Instrument
	{
		InstrumentRecord element;
		protected LoadedInstrument(InstrumentRecord element, Soundbank soundbank, Patch patch, String name, Class<?> dataClass) {
			super(soundbank, patch, name, dataClass);
			this.element = element;
		}
		String[] keys = null;
		public String[] getKeys()
		{
			if(keys != null) return keys;
			keys = new String[127];
			Iterator<VoiceRecord> iter = element.getVoices().iterator();
			while (iter.hasNext()) {
				VoiceRecord voice = iter.next();
				for(int k = voice.getKeyFrom(); k <= voice.getKeyTo(); k++)
				{
					if(keys[k] == null)
					{
						String name = voice.getName();
						if(name == null) name =  element.getDescription();
						if(name == null) name = "untitled";
						keys[k] = name;
					}
				}
			}
			return keys;			
		}
		public boolean[] getChannels()
		{
			return element.channels;
		}
		public Object getData() {
			return null;
		}		
	}
	
	private class InstrumentComparator implements Comparator<InstrumentRecord>
	{
		public int compare(InstrumentRecord arg0, InstrumentRecord arg1) {								
			int a = arg0.bank * 128 + arg0.program;
			int b = arg1.bank * 128 + arg1.program;
			return a - b;
		}		
	}

	public Instrument[] getLoadedInstruments() {
		
		Variable instruments_var = session.getNameSpace().get("instruments");
		List list = ObjectsPart.asList(instruments_var);
		
		ArrayList ins_list = new ArrayList();
		
		Iterator iter = list.iterator();
		while (iter.hasNext()) {
			Object object = iter.next();
			if(object instanceof InstrumentRecord)
			//if(((InstrumentRecord)object).channels[0])
			{
				ins_list.add((InstrumentRecord)object); 
			}				
		}
		
		InstrumentRecord[] inslist = new InstrumentRecord[ins_list.size()]; 		
		ins_list.toArray(inslist); 
		Arrays.sort(inslist, new InstrumentComparator());
		
		Instrument[] instruments = new Instrument[inslist.length];
								
		for (int i = 0; i < inslist.length; i++) {				
			InstrumentRecord element = inslist[i];			
			Patch patch = new Patch(element.bank, element.program);			
			Instrument instrument = new LoadedInstrument(element, null, patch, element.description, null);
			instruments[i] = instrument;								
		}
				
		
		return instruments;
	}

	public boolean loadAllInstruments(Soundbank soundbank) {
		if(!(soundbank instanceof RasmusSoundbank)) return false;
		
		String filename = ((RasmusSoundbank)soundbank).getFileName();		
		String code = "\ninstruments <- soundbank(" + "\"" + filename + "\"" + ");";
		
		editor.commit();
		session.getScriptDocument().getObject("instruments").setNormalizedString(code);		
		editor.commit();		
		editor.update();
		
		return true;
	}

	public void unloadAllInstruments(Soundbank soundbank) {
		
		editor.commit();
		session.getScriptDocument().getObject("instruments").remove();
		editor.commit();
		editor.update();
		
	}

	public boolean loadInstruments(Soundbank soundbank, Patch[] patchList) {
		return false;
	}

	public void unloadInstruments(Soundbank soundbank, Patch[] patchList) {
	}

	public MidiDevice.Info getDeviceInfo() {
		return RasmusSynthesizerProvider.infos[0];
	}
	
	boolean isOpen = false;
	
	public void show()
	{
		editor.setVisible(true);
		editor.toFront();
	}
	
	boolean isplaying = false;
			
	public void open() {

		if(session == null)
		{
			session = new RasmusSession();		
			editor = new RasmusEditor(session);
			editor.setSynthName(name);
			recv = MidiSequence.getInstance(editor.getRealTimeInput());
			
			if(realtimeout != null) session.routeAudioOutput(realtimeout);
		}
						
		isOpen = true;
	}

	public void close()
	{
		isOpen = false;
		editor.playStop();
		editor.setVisible(false);
		editor.dispose();
		session.close();		
	}

	public boolean isOpen() {
		return isOpen;
	}

	public long getMicrosecondPosition() {
		return -1;
	}

	public int getMaxReceivers() {
		return 1;
	}

	public int getMaxTransmitters() {
		return 0;
	}

	public Receiver getReceiver() throws MidiUnavailableException {
		
		if(!isplaying)
		{		
			if(!isOpen) return null;
			editor.playRealTime();
			isplaying = true;
		}
		
		return recv;
	}

	public List<Receiver> getReceivers() {
		List list = new ArrayList();
		try
		{
		list.add(getReceiver());
		}
		catch(MidiUnavailableException e) {}
		return list;
	}

	public Transmitter getTransmitter() throws MidiUnavailableException {
		throw new MidiUnavailableException();
	}

	public List<Transmitter> getTransmitters() {
		return new ArrayList();
	}
	
	String name = "Rasmus Synthesizer";
	public void setName(String name)
	{
		this.name = name;
		if(editor != null) editor.setSynthName(name);
	}
	public String getName()
	{
		return name;
	}
	
	public String getScript()
	{
		return session.getScriptDocument().toString();
	}
	
	public void setScript(String script)
	{
		editor.commit();
		session.getScriptDocument().setString(script);
		editor.update();
		editor.commit();
	}
	
	private boolean saveReferencedData = false;
	public void setSaveReferencedData(boolean saveReferencedData)
	{
		this.saveReferencedData = saveReferencedData;
	}
	public boolean getSaveReferencedData()
	{
		return saveReferencedData;
	}

	public void writeExternal(ObjectOutput out) throws IOException {
		
		editor.commit();
		out.writeInt(2);
		out.writeObject(name);
		out.writeObject(session.getScriptDocument().toString());		
		
		ResourceManager manager = ResourceManager.getInstance(session.getNameSpace());
		Collection<Resource> resources = manager.getResources();
		int count = resources.size();
		if(!saveReferencedData) count = 0;
		out.writeInt(count);
		if(count != 0)
		{
		Iterator<Resource> iter = resources.iterator();
		while (iter.hasNext()) {
			Resource resource = iter.next();
			String path = resource.getPath();
			out.writeObject(path);			
			File file = resource.getFile();
			out.writeLong(file.length()); 
			FileInputStream fis = new FileInputStream(file);
			try
			{				
				byte[] buffer = new byte[1024];
				int len;
				while( (len = fis.read(buffer)) != -1)
				{
					out.write(buffer, 0, len);
				}
			}
			finally
			{
				fis.close();
			}
		}
		}
		
	}

	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

		session = new RasmusSession();		
		int version = in.readInt(); // For future upgradeability
		if(version > 2) throw new IOException("Uncompatible version " + version);
		
		name = (String)in.readObject();
		String code = (String)in.readObject();
		
		if(version > 1) // Version with support for saveing reference data
		{
			ResourceManager manager = ResourceManager.getInstance(session.getNameSpace());
			int count = in.readInt();
			for (int i = 0; i < count; i++) {
				String path = (String)in.readObject();
				String suffix = path;
				long size = in.readLong();				
				int li = suffix.lastIndexOf("/");
				if(li == -1) li = suffix.lastIndexOf("\\");
				if(li != -1) suffix = suffix.substring(li + 1);				
				li = path.lastIndexOf(".");				
				if(li != -1) 
					suffix = path.substring(li);
				else
					suffix = "";
				
				File tempfile = File.createTempFile("rasmusdsp",suffix);
				tempfile.deleteOnExit();
				
				FileOutputStream fos = new FileOutputStream(tempfile);
				try
				{
					byte[] buffer = new byte[1024];
					while(size > 0)
					{
						if(size > 1024)
						{
							int ret = in.read(buffer, 0, 1024);
							fos.write(buffer, 0, ret);
							size -= ret;
						}
						else
						{
							in.read(buffer, 0, (int)size);
							fos.write(buffer, 0, (int)size);
							size = 0;
						}
					}
				}
				finally
				{
					fos.close();
				}				
				
				manager.importResource(path, tempfile);				
			}
		}
		
		session.getScriptDocument().setString(code);
		editor = new RasmusEditor(session);
		editor.setSynthName(name);
		recv = MidiSequence.getInstance(editor.getRealTimeInput());			
		if(realtimeout != null) session.routeAudioOutput(realtimeout);					
	}
	
	static javax.sound.sampled.Mixer.Info mixerinfo = new javax.sound.sampled.Mixer.Info("RasmusDSP Synthesizer", "RasmusDSP", "RasmusDSP Synthesizer", "") {};
	static Line.Info targetLineInfo = new Line.Info(TargetDataLine.class);
	
	public javax.sound.sampled.Mixer.Info getMixerInfo() {
		return mixerinfo;
	}
	public javax.sound.sampled.Line.Info[] getTargetLineInfo() {
		Line.Info[] infos = new Line.Info[1];
		infos[0] = targetLineInfo;
		return infos;
	}
	public javax.sound.sampled.Line.Info[] getTargetLineInfo(javax.sound.sampled.Line.Info line) {
		if(isLineSupported(line))
			return getTargetLineInfo();
		return new Line.Info[0];
	}
	public boolean isLineSupported(javax.sound.sampled.Line.Info line) {
		return line.getLineClass() == TargetDataLine.class;
	}	

	Vector activeTargetLines = new Vector();
	
	Variable realtimeout = null;

	public Line getLine(javax.sound.sampled.Line.Info lineinfo) throws LineUnavailableException {
		
		if(!isLineSupported(lineinfo))
			throw new IllegalArgumentException();
		
		realtimeout = new Variable();
				
		try {
			Interpreter interpreter = new Interpreter();
			realtimeout.add(interpreter.eval("signal(0)"));
		} catch (ScriptParserException e1) {
			e1.printStackTrace();
		}				

		TargetDataLine tdataline = new TargetDataLine()
		{			
			AudioFormat format = new AudioFormat(44100, 16, 2, true, false);
			AudioSession audiosession;
			InputStream audiostream;
			public void open(AudioFormat format, int arg1) throws LineUnavailableException {
				this.format = format;
				open();
			}
			public void open(AudioFormat format) throws LineUnavailableException {
				this.format = format;
				open();
			}
			boolean isOpen = false;
			public void open() throws LineUnavailableException {
				if(isOpen) return;
				
				activeTargetLines.add(this);
				isOpen = true;								
				
				audiosession = new AudioSession(format.getSampleRate(), format.getChannels());
				audiosession.setRealTime(true);
				//if(maxpoly > 0)
				//	audiosession.setMaxPolyphony(maxpoly);
				audiostream = audiosession.asByteStream(realtimeout, format); //format.getSampleSizeInBits(), format.getEncoding().equals(AudioFormat.Encoding.PCM_SIGNED) , format.isBigEndian());
			}
			public void close() {
				if(!isOpen) return;
				try
				{
				audiostream.close();
				}
				catch(Exception e)
				{					
				}
				activeTargetLines.remove(this);
				isOpen = false;
			}
			public boolean isOpen() {
				return isOpen;
			}
			public int read(byte[] arg0, int arg1, int arg2) {
				try
				{
					audiosession.commit();
					return audiostream.read(arg0, arg1, arg2);
				}
				catch(Exception e)
				{
					e.printStackTrace();
					return -1;
				}
			}
			public AudioFormat getFormat() {
				return format;
			}
			
			public void drain() {
			}
			public void flush() {
			}
			public void start() {
			}
			public void stop() {
			}
			public boolean isRunning() {
				return isOpen;
			}
			public boolean isActive() {
				return isOpen;
			}
			public int getBufferSize() {
				return -1;
			}
			public int available() {
				return -1;
			}
			public int getFramePosition() {
				return -1;
			}
			public long getLongFramePosition() {
				return -1;
			}
			public long getMicrosecondPosition() {
				return -1;
			}
			public float getLevel() {
				return 1.0f;
			}
			public javax.sound.sampled.Line.Info getLineInfo() {
				return targetLineInfo;
			}
			public Control[] getControls() {
				return new Control[0];
			}
			public boolean isControlSupported(Type arg0) {
				return false;
			}
			public Control getControl(Type arg0) {
				throw new IllegalArgumentException();
			}
			public void addLineListener(LineListener arg0) {
			}
			public void removeLineListener(LineListener arg0) {
			}
			
		};

		if(isOpen)
		{
			editor.playStop();
		}

		if(session != null) session.routeAudioOutput(realtimeout);
		
		if(isOpen)
		{
			editor.playRealTime();
			isplaying = true;
		}
		
		return tdataline;		
		
	}

	
	public Line[] getTargetLines() {
		
		Line[] lines = new Line[activeTargetLines.size()];
		activeTargetLines.toArray(lines);
		return lines;
	}
	static Line.Info mixerLineInfo = new Line.Info(Mixer.class);
	public javax.sound.sampled.Line.Info getLineInfo() {
		return mixerLineInfo;
	}
		

	
	/* ------------------------------------------------------ */
	/*  NOT SUPPORTED FOR MIXERS NOR NEEDED                   */
	/* ------------------------------------------------------ */

	public javax.sound.sampled.Line.Info[] getSourceLineInfo() {
		return new javax.sound.sampled.Line.Info[0];
	}
	public javax.sound.sampled.Line.Info[] getSourceLineInfo(javax.sound.sampled.Line.Info arg0) {
		return new javax.sound.sampled.Line.Info[0];
	}

	public int getMaxLines(javax.sound.sampled.Line.Info arg0) {
		return AudioSystem.NOT_SPECIFIED;
	}

	public Line[] getSourceLines() {
		return new Line[0];
	}
	public void synchronize(Line[] arg0, boolean arg1) {
		throw new IllegalArgumentException();
		
	}

	public void unsynchronize(Line[] arg0) {
		throw new IllegalArgumentException();
	}

	public boolean isSynchronizationSupported(Line[] arg0, boolean arg1) {
		return false;
	}


	public Control[] getControls() {
		return new Control[0];
	}

	public boolean isControlSupported(Type arg0) {
		return false;
	}

	public Control getControl(Type arg0) {
		throw new IllegalArgumentException();
	}

	public void addLineListener(LineListener arg0) {
	}

	public void removeLineListener(LineListener arg0) {
	}

	public Soundbank getSoundbank(URL url) throws InvalidMidiDataException, IOException {
		return null;
	}

	public Soundbank getSoundbank(InputStream stream) throws InvalidMidiDataException, IOException {
		return null;
	}

	public Soundbank getSoundbank(File arg0) throws InvalidMidiDataException, IOException {
		if(!arg0.exists()) throw new InvalidMidiDataException();
		SF2SoundFont sf2soundfont = SF2SoundFontManager.getSF2SoundFont(arg0.getPath());
		return new RasmusSoundbank(arg0.getPath(), sf2soundfont);
	}	

}
