/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util;

import java.util.ArrayList;
import java.util.List;

public 	class Chord
{
	public static int MAXCHORDSIZE = 15;
	public int root = 0;          // note root
	public int level = 5;         // 3/5/7 < defualt 5 >
	public String chord = "";     // m/sus/maj/dim/dim7
	public String suspense = "";  // sus2/sus4
	public String extension = ""; // (3bb 7#)
	public int bass = -1;        // c/d/e/f/g/a/b
	
	private void pattSeat(boolean rev, int[] pattern, int seat, int addv)
	{
		
		if(rev)
		{
			pattern[(seat+addv) % 12] = seat % 12;
		}
		else
		{
			pattern[seat % 12] = (seat+addv) % 12;
		}
	}
	
	private int extractNums(String str)
	{
		String sstr = "";
		for (int i = 0; i < str.length(); i++) {
			char n = str.charAt(i);
			if(Character.isDigit(n))
			{
				sstr += n;
			}
		}
		if(sstr.length() == 0) return 0;
		return Integer.parseInt(sstr);
	}
	
	private String extractChars(String str)
	{
		String sstr = "";
		for (int i = 0; i < str.length(); i++) {
			char n = str.charAt(i);
			if(!Character.isDigit(n))
			{
				sstr += n;
			}
		}
		return sstr;
	}    
	
	private String compactChordForm(String chordform)
	{
		
		List usedseats = new ArrayList();
		List seats = new ArrayList();
		
		String grp = "";
		for (int i = chordform.length()-1; i >= 0; i--) {
			char n = chordform.charAt(i);
			if(n == ' ')
			{
				if(grp.length() != 0)
				{
					int ii = extractNums(grp);
					if(ii == 2)
					{
						ii = 3;
						grp += "BB";
					}
					if(ii == 4)
					{
						ii = 3;
						grp += "#";
					}			
					if(ii == 6)
					{
						ii = 7;
						grp += "BB";
					}			
					
					if(usedseats.indexOf(new Integer(ii)) == -1)
					{
						usedseats.add(new Integer(ii));
						seats.add(ii + extractChars(grp));					
					}
					grp = "";				
				}
			}
			else
			{
				grp = n + grp;
			}
		}
		
		String result = "";
		for (int i = 1; i <= MAXCHORDSIZE; i++) {
			int ii = usedseats.indexOf(new Integer(i));
			if(ii != -1)
			{
				result += seats.get(ii) + " ";
			}
		}
		return result;
	}
	
	
	public String toString()
	{
		return toForm();
	}
	public String toForm()
	{
		
		String suspense = this.suspense;
		String chord = this.chord;
		
		if(suspense.equals("S9")) suspense = "SUS9";
		if(suspense.equals("S4")) suspense = "SUS4";
		if(suspense.equals("S2")) suspense = "SUS2";
		if(suspense.equals("SUS")) suspense = "SUS4";
		if(suspense.equals("SUS9")) suspense = "SUS2";
		if(suspense.equals("S")) suspense = "SUS4";
		
		if(chord.equals("-MAJ")) chord = "MIN";
		if(chord.equals("MMAJ")) chord = "MIN";
		if(chord.equals("O")) chord = "DIM";
		if(chord.equals("+")) chord = "AUG";
		if(chord.equals("-")) chord = "M";    	
		
		String result = "";
		
		if(level >= 1)  result = result + "1 ";
		if(level >= 3) result = result + "3 ";
		if(level >= 5) result = result + "5 ";
		if(level >= 7) result = result + "7B ";
		if(level >= 9) result = result + "9 ";   // 2
		if(level >= 11) result = result + "11 "; // 4
		if(level >= 13) result = result + "13 "; // 6
		
		if(level == 2) result = result + "3BB ";
		if(level == 4) result = result + "3# ";
		if(level == 6) result = result + "7BB ";
		
		if(chord.equals("M")) if(level >= 3) result += "3B ";
		if(chord.equals("MIN"))
		{
			if(level >= 3) result += "3B ";
			if(level >= 7) result += "7 ";
		}
		if(chord.equals("MAJ"))
		{
			if(level >= 7) result += "7 ";
		}        
		if(chord.equals("AUG"))
		{
			if(level >= 5) result += "5# ";
		}        
		if(chord.equals("DIM"))
		{
			if(level >= 3) result += "3B ";
			if(level >= 5) result += "5B ";
			if(level >= 7) result += "5BB ";
		}        
		if(suspense.equals("SUS4"))
		{
			if(level >= 3) result += "3# ";
		}        
		if(suspense.equals("SUS2"))
		{
			if(level >= 3) result += "3BB ";
		}        
		
		result = compactChordForm(result + extension);
		
		return result;
	}
	
	
	
	public int[] getTransform(boolean rev)
	{
		
		int[] result = new int[12];
		for (int i = 0; i < result.length; i++) {
			result[i] = i;
		}
		String fext = toForm();
		
		String grp = "";
		int gaddv = 0;
		char[] chars = fext.toCharArray();
		int seat = 0;
		int tseat = 0;
		for (int i = 0; i < chars.length; i++) {
			char n = chars[i];
			
			if(n == ' ')
			{
				if(grp.length() != 0)
				{					
					try
					{
						seat = Integer.parseInt(grp);
					}
					catch(Throwable e)
					{
						seat = 0;
					}
					tseat = 0;
					while(seat > 7) seat = seat -7;
					
					if(seat == 1) tseat = 0;
					if(seat == 2) tseat = 2;
					if(seat == 3) tseat = 4;
					if(seat == 4) tseat = 5;
					if(seat == 5) tseat = 7;
					if(seat == 6) tseat = 9;
					if(seat == 7) tseat = 11;
					pattSeat(rev,result,tseat,gaddv);
					gaddv = 0;
					grp = "";
					
				}
			}
			else if(n == 'B') gaddv--;
			else if(n == '#') gaddv++;
			else
			{
				if(Character.isDigit(n))
				{
					grp += n;
				}
			}
		}
		
		
		
		return result;
	}
	
	
	
	/*
	 fext := chord.extension + ' '; *)
	 
	 fext := Chord2Form(kchord);
	 grp := '';
	 gaddv := 0;
	 for i := 1 to length(fext) do begin
	 n := copy(fext,i,1);
	 m := copy(' '+fext,i,1);
	 
	 if n = ' ' then begin
	 if grp <> '' then begin
	 seat := strtointdef(grp,0);
	 tseat := 0;
	 while seat > 7 do seat := seat - 7;
	 if seat = 1 then tseat := 0;
	 if seat = 2 then tseat := 2;
	 if seat = 3 then tseat := 4;
	 if seat = 4 then tseat := 5;
	 if seat = 5 then tseat := 7;
	 if seat = 6 then tseat := 9;
	 if seat = 7 then tseat := 11;
	 
	 {if seat = 8 then tseat := 0+12;
	 if seat = 9 then tseat := 2+12;
	 if seat = 10 then tseat := 4+12;
	 if seat = 11 then tseat := 5+12;
	 if seat = 12 then tseat := 7+12;
	 if seat = 13 then tseat := 9+12;
	 if seat = 14 then tseat := 11+12;}
	 
	 PattSeat(rev,result,tseat,gaddv);
	 gaddv := 0;
	 grp := '';
	 end;
	 end else
	 if n = 'B' then
	 gaddv := gaddv - 1
	 else
	 if n = '#' then
	 gaddv := gaddv + 1
	 else
	 if strtointdef(n,-1) <> -1 then
	 grp := grp + n;
	 end;
	 
	 end;    	
	 */
	
	private String workOutExtPart(String data)
	{
		int stat = 0;
		String result = "";
		for (int i = 0; i < data.length(); i++) {
			char n = data.charAt(i);
			if(n == '-') n = 'B';
			if(n == '+') n = '#';
			if(stat > 0)
			{
				if(n == ' ')
				{
					stat = 0;
					result += ' ';
				}
			}
			if(Character.isDigit(n))
			{
				if(stat == 0) stat = 1;
				else if(stat == 2) stat = 101;
				else if(stat == 102)
				{
					stat = 1;
					result += ' ';
				}
				result += n;
				
			}
			else
				if(n == 'B' || n == '#')
				{
					if(stat == 0) stat = 2;
					else if(stat == 1) stat = 102;
					else if(stat == 102)
					{
						stat = 2;
						result += ' ';
					}
					result += n;				
				}
		}
		return result;
	}
	
	
	private int parseNote(String note)
	{
		int basevalue = 0;
		int octave = 0;
		for (int i = 0; i < note.length(); i++) {
			char ch = note.charAt(i);
			if(ch == 'C' || ch == 'c') basevalue += 0;
			if(ch == 'D' || ch == 'd') basevalue += 2;
			if(ch == 'E' || ch == 'e') basevalue += 4;
			if(ch == 'F' || ch == 'f') basevalue += 5;
			if(ch == 'G' || ch == 'g') basevalue += 7;
			if(ch == 'A' || ch == 'a') basevalue += 9;		
			if(ch == 'B' || ch == 'b') basevalue += 11;
			if(ch == '#')  basevalue += 1;			
			if(ch >= '0' && ch <= '9')
			{
				octave *= 10;				
				if(ch == '0') octave += 0; 
				if(ch == '1') octave += 1; 
				if(ch == '2') octave += 2; 
				if(ch == '3') octave += 3; 
				if(ch == '4') octave += 4; 
				if(ch == '5') octave += 5; 
				if(ch == '6') octave += 6; 
				if(ch == '7') octave += 7; 
				if(ch == '8') octave += 8; 
				if(ch == '9') octave += 9; 
			}
		}		
		return basevalue + octave * 12;    	
	}
	
	String chordstr;
	public Chord(String chordstr)
	{
		this.chordstr = chordstr;
		chordstr = chordstr.toUpperCase();	
		char[] chars = chordstr.toCharArray();
		int lasti = chars.length - 1;
		String rootnote = "";
		String bassstr = "";
		String strlevel = "";
		int level = 0;
		for (int i = 0; i < chars.length; i++) {
			char n = chars[i];
			char m;
			if(i == lasti) m = ' '; else m = chars[i+1];
			if(n != ')')
			{
				switch (level) {
				case 0: // Root part
					rootnote += n;					
					if((m != '#') && (m != 'B')) 
					{
						if((n == 'C') || (n == 'D') || (n == 'E') || (n == 'F') ||
								(n == 'G') || (n == 'A') || (n == 'B') || (n == '#')) 
						{
							root = parseNote(rootnote);
							level = 1;
						}
					}					
					break;
				case 1: 
					
					if(n == '\\' || n == '/')
					{
						level = 5; // goto bass part
					}
					else
						if(n == '(')
						{
							level = 4; // goto custom part
						}
						else
							if(n == 'B' || n == '#')
							{
								extension += n;
								level = 4;  // goto custom part
							}
							else
								if(n == 'S')
								{
									level = 3;
									suspense += n;
									level = 3; // goto sustained part
								}
								else
									if(Character.isDigit(n))
									{
										level = 2; // goto chord level part
										strlevel += strlevel + n;
									}
									else
									{
										chord += n;
									}
					
					break;
				case 2:  // Level part
					if(n == '\\' || n == '/')
					{
						level = 5; // goto bass part
					}
					else
						if(n == '(')
						{
							level = 4; // goto custom part
						}
						else
							if(n == 'B' || n == '#' || n == '+' || n == '-')
							{
								extension += n;
								level = 4;  // goto custom part
							}		
							else
								if(!Character.isDigit(n))
								{
									level = 3; // goto sustained part
									suspense += suspense + n;
								}				
								else
								{
									try
									{						  
										if(Integer.parseInt(strlevel + n) > MAXCHORDSIZE)
										{
											extension += n;
											level = 4;
										}
										else
										{
											strlevel += n;
										}
									}
									catch(Throwable e) {
										System.out.println(e.toString());
										strlevel += n;
									};
									
								}
					
					break;
				case 3: // Sustained part
					
					if(n == '\\' || n == '/')
					{
						level = 5; // goto bass part
					}
					else					
						if(n == '(')
						{
							level = 4; // goto custom part
						}		
						else
							if(n == 'B' || n == '#' || n == '+' || n == '-')
							{
								extension += n;
								level = 4;  // goto custom part
							}		
							else
							{
								suspense += n;
							}
					
					break;
				case 4: // Custom part part
					
					if(n == 'S')
					{
						if(suspense.length() == 0)
						{
							suspense += n;
							level = 3;
						}
					}
					else
						if(!(Character.isDigit(n) || n == 'B' || n == '#' || n == '+' || n == '-' || n == ' '))					
						{
							if(chord.length() == 0)
							{
								chord += n;
								level = 1;
							}
						}
						else
							if(n == '\\' || n == '/')
							{
								level = 5; // goto bass part
							}
							else
							{
								extension += n;
							}					
					break;
				case 5: // Bass part
					if(n == '(')
					{
						level = 4; // goto custom part
					}
					else
					{
						bassstr += n;
					}
					break;
					
				default:
					break;
				}
			}
		}
		
		extension = workOutExtPart(extension);
		try
		{
			this.level = Integer.parseInt(strlevel);
		}
		catch(Throwable e)
		{
			this.level = 5;
		}
		
		if(bassstr.length() != 0)
		{
			bass = parseNote(bassstr);
		}
		if(bass == -1)
		{
			bass = root % 12; // make bass be the same as root
		}
		
	}
}
