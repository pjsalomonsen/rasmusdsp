/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util.riff.dls;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import rasmus.util.ByteConversion;
import rasmus.util.riff.RiffReader;

public class Collection {

	class InputStreamCounter extends InputStream
	{
		InputStream inputstream;
		long pos = 0;
		public long getPosition()
		{
			return pos;
		}
		public InputStreamCounter(InputStream inputstream)
		{
			this.inputstream = inputstream;
		}
		public int read() throws IOException {
			int len = inputstream.read();
			if(len != -1) pos += 1;
			return len;
		}	
		public int available() throws IOException {
			return inputstream.available();
		}
		public int read(byte[] b, int off, int len) throws IOException {
			int readed = inputstream.read(b, off, len);
			if(readed > 0) pos += (long)readed;
			return readed;			
		}		
		public int read(byte[] b) throws IOException {		
			int readed = inputstream.read(b);
			if(readed > 0) pos += (long)readed;
			return readed;		
		}
		public long skip(long n) throws IOException {
			long readed = inputstream.skip(n);
			if(readed > 0) pos += readed;
			return readed;		
		}
	}
		
	Document doc;
	InputStreamCounter isc;
	int sampleid = 0;
	public void read(InputStream inputstream) throws Exception {
		
		doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		Element collection_element = doc.createElement("Collection");
		doc.appendChild(collection_element);
		
		
		isc = new InputStreamCounter(inputstream);
		RiffReader riffiterator = new RiffReader(isc);
		if (!riffiterator.getFormat().equals("RIFF"))
			throw new Exception("Incorrect format");
	
		while (riffiterator.hasNextChunk()) {
			RiffReader subchunk = riffiterator.nextChunk();
			
			System.out.println(subchunk.getFormat() + " : " + subchunk.getType());
			
			if (subchunk.getFormat().equals("LIST")) {
				

				// Instruments
				if (subchunk.getType().equals("lins")) {										
					while(subchunk.hasNextChunk())
					{						
						// Instrument
						RiffReader subsubchunk = subchunk.nextChunk();
						if(subsubchunk.getFormat().equals("LIST"))
						if(subsubchunk.getType().equals("ins "))
						{
							Element instrument_element = doc.createElement("preset");
							collection_element.appendChild(instrument_element);
							readInstrument(subsubchunk, instrument_element);							
						}
						
						
					}
				}
				
				// Wave Pool
				if (subchunk.getType().equals("wvpl")) {										
					while(subchunk.hasNextChunk())
					{						
						// Wave
						RiffReader subsubchunk = subchunk.nextChunk();
						if(subsubchunk.getFormat().equals("LIST"))
						if(subsubchunk.getType().equals("wave"))
						{
							Element sample_element = doc.createElement("sample");
							sample_element.setAttribute("id", "" + sampleid);
							sampleid++;
							collection_element.appendChild(sample_element);
							readWave(subsubchunk, sample_element);							
						}
						
						
					}
				}				
												
				// Supplemental Information
				if (subchunk.getType().equals("INFO")) {
					readINFO(subchunk, collection_element);
				}
				
			}
			else
			{
				// Collection Header : colh 
				if(subchunk.getFormat().equals("colh"))
				{
					byte[] bword = new byte[4];
					subchunk.read(bword);		
					collection_element.setAttribute("instrument_count", Long.toString(ByteConversion.dwordToLong(bword)));
				}
				
				// Pool Table : ptbl 
				if(subchunk.getFormat().equals("colh"))
				{
					/*
					<ptbl-ck> ? ptbl(
							<cbSize:ULONG>
							<cCues:ULONG>
							<poolcues(s)>...
							)
							<poolcue> ? struct
							{
							ULONG ulOffset;
							}
							The <ptbl-ck> chunk: 			*/								
				}
				
				// Version
				if(subchunk.getFormat().equals("vers"))
				{
					byte[] bword = new byte[4];
					subchunk.read(bword);		
					collection_element.setAttribute("version_major", Long.toString(ByteConversion.dwordToLong(bword)));
					subchunk.read(bword);
					collection_element.setAttribute("version_minor", Long.toString(ByteConversion.dwordToLong(bword)));
				}
			}
		}
		
		
	}
	
	HashMap mapinfo = new HashMap();
	{
		mapinfo.put("IARL","archival_location");
		mapinfo.put("IART","artist");
		mapinfo.put("ICMS","commissioned");
		mapinfo.put("ICMT","comments");
		mapinfo.put("ICOP","copyright");
		mapinfo.put("ICRD","creationdate");
		mapinfo.put("IENG","engineers");
		mapinfo.put("IGNR","genre");
		mapinfo.put("IKEY","keywords");
		mapinfo.put("IMED","medium");
		mapinfo.put("INAM","name");
		mapinfo.put("IPRD","product");
		mapinfo.put("ISBJ","subject");
		mapinfo.put("ISFT","software");
		mapinfo.put("ISRC","source");
		mapinfo.put("ISRF","source_form");
		mapinfo.put("ITCH","technician");

	}	
	public void readINFO(RiffReader chunk, Element element) throws Exception
	{
		while(chunk.hasNextChunk())
		{
			RiffReader subchunk = chunk.nextChunk();
			String format = subchunk.getFormat();
			
			String attrinfo = (String)mapinfo.get(format);
			
			if(attrinfo != null)
			{
				byte[] buffer = new byte[subchunk.available()];
				subchunk.read(buffer);
				element.setAttribute(attrinfo, ByteConversion.charToString(buffer));				
			}
		}
	}
	
	public void readInstrument(RiffReader chunk, Element element) throws Exception
	{
		while(chunk.hasNextChunk())
		{
			RiffReader subchunk = chunk.nextChunk();			

			if (subchunk.getFormat().equals("LIST")) {
				
				// Articulators
				if (subchunk.getType().equals("lart")) {
					readArticulators(subchunk, element);
				}
				
				
				// Regions
				if (subchunk.getType().equals("lrgn")) {										
					while(subchunk.hasNextChunk())
					{						
						// Region
						RiffReader subsubchunk = subchunk.nextChunk();
						if(subsubchunk.getFormat().equals("LIST"))
						if(subsubchunk.getType().equals("rgn "))
						{
							Element region_element = doc.createElement("generator");
							element.appendChild(region_element);
							readRegion(subsubchunk, region_element);							
						}
						
						
					}
				}				
				
				// Supplemental Information
				if (subchunk.getType().equals("INFO")) {
					readINFO(subchunk, element);
				}				
			}
			else
			{
				// Version
				if(subchunk.getFormat().equals("insh"))
				{
					byte[] bword = new byte[4];
					subchunk.read(bword);		
					//element.setAttribute("region_count", Long.toString(ByteConversion.dwordToLong(bword)));

					byte[] bbyte = new byte[1];
					subchunk.read(bbyte);
					int bank = ByteConversion.byteToInt(bbyte) * 128;
					subchunk.read(bbyte);
					bank += ByteConversion.byteToInt(bbyte);
					subchunk.read(bbyte);		// Reserved
					subchunk.read(bbyte);
					
					element.setAttribute("bank", Integer.toString(bank));
					
					if(bbyte[0] < 0)
						element.setAttribute("drumkit", "yes");

					
					subchunk.read(bword);
					element.setAttribute("id", Long.toString(ByteConversion.dwordToLong(bword)));
				}				
				
				// GUID Identification: dlid
				if(subchunk.getFormat().equals("dlid"))
				{
					// Long Short Short Byte
					
					byte[] buffer = new byte[16];		
					subchunk.read(buffer);		
					element.setAttribute("dlsid", ByteConversion.guidToString(buffer));
				}				
			}
		}
	}
	
	
	public void readArticulators(RiffReader chunk, Element element) throws Exception
	{

		while(chunk.hasNextChunk())
		{						
			// Region
			RiffReader subchunk = chunk.nextChunk();
			if(subchunk.getFormat().equals("art1"))
			{

				byte[] sdata = new byte[2];
				byte[] ldata = new byte[4];
				int ret = 0;					
				ret += subchunk.read(ldata);		
				long size = ByteConversion.dwordToLong(ldata);
				ret += subchunk.read(ldata);
				long count = ByteConversion.dwordToLong(ldata);
				if(size - ret != 0)
				{
					subchunk.skip(size - ret);
				}
				for (int i = 0; i < count; i++) {
					Element art = doc.createElement("Articulators");
					element.appendChild(art);
					
					
					chunk.read(sdata);
					art.setAttribute("source", "" + ByteConversion.shortToInt(sdata));
					chunk.read(sdata);
					art.setAttribute("control", "" + ByteConversion.shortToInt(sdata));
					chunk.read(sdata);
					art.setAttribute("destination", "" + ByteConversion.shortToInt(sdata));
					chunk.read(sdata);
					art.setAttribute("transform", "" + ByteConversion.shortToInt(sdata));
					chunk.read(ldata);
					art.setAttribute("scale", "" + (int)ByteConversion.dwordToLong(ldata));
					
				}
				
			}
			
			
		}		
		

		
		
	}
	
	public void readRegion(RiffReader chunk, Element element) throws Exception
	{
		while(chunk.hasNextChunk())
		{
			RiffReader subchunk = chunk.nextChunk();			

			if (subchunk.getFormat().equals("LIST")) {
				
				// Articulators
				if (subchunk.getType().equals("lart")) {
					readArticulators(subchunk, element);
				}
				
			}
			else
			{
								
				// Wave Sample
				if(subchunk.getFormat().equals("wsmp"))
				{

					byte[] sdata = new byte[2];
					byte[] ldata = new byte[4];
					int ret = 0;					
					ret += subchunk.read(ldata);		
					long size = ByteConversion.dwordToLong(ldata);
					ret += subchunk.read(sdata);				
					element.setAttribute("originalkey", "" + ByteConversion.shortToInt(sdata));
					ret += subchunk.read(sdata);				
					element.setAttribute("finetune", "" + ByteConversion.shortToInt(sdata));
					ret += subchunk.read(ldata);				
					element.setAttribute("attenuation", "" + ((int)ByteConversion.dwordToLong(ldata) / 4294967296.0));
					ret += subchunk.read(ldata);				
					element.setAttribute("fuloptions", "" + ByteConversion.dwordToLong(ldata));
					ret += subchunk.read(ldata);	
					
					long sampleloops = ByteConversion.dwordToLong(ldata);
					
					if(size > ret)
					{
						byte[] buffer = new byte[(int)(size - ret)];
						subchunk.read(buffer);
					}
					
					if(sampleloops != 0)
					{
						// Loop is used
						ret = 0;
						ret += subchunk.read(ldata);		
						size = ByteConversion.dwordToLong(ldata);

						ret += subchunk.read(ldata);				
						element.setAttribute("looptype", "" + ByteConversion.dwordToLong(ldata));
						ret += subchunk.read(ldata);				
						element.setAttribute("loopstart", "" + ByteConversion.dwordToLong(ldata));
						ret += subchunk.read(ldata);				
						element.setAttribute("looplen", "" + ByteConversion.dwordToLong(ldata));
					
					}					
				}
				
				// Wave Link
				if(subchunk.getFormat().equals("wlnk"))
				{
					
					byte[] sdata = new byte[2];
					subchunk.read(sdata);
					element.setAttribute("fusOptions", "" + ByteConversion.shortToInt(sdata));

					subchunk.read(sdata);
					element.setAttribute("phasegroup", "" + ByteConversion.shortToInt(sdata));

					byte[] ldata = new byte[4];

					subchunk.read(ldata);
					element.setAttribute("channel", "" + ByteConversion.dwordToLong(ldata));
					subchunk.read(ldata);
					element.setAttribute("sampleID", "" + ByteConversion.dwordToLong(ldata));
					
				}
				
				
				// Region Header
				if(subchunk.getFormat().equals("rgnh"))
				{
					// Long Short Short Byte
					
					byte[] sdata = new byte[2];
					subchunk.read(sdata);
					int keyfrom = ByteConversion.shortToInt(sdata);
					subchunk.read(sdata);
					int keyto = ByteConversion.shortToInt(sdata);
					element.setAttribute("keyRange", keyfrom + " - " + keyto);
					int velfrom = ByteConversion.shortToInt(sdata);
					subchunk.read(sdata);
					int velto = ByteConversion.shortToInt(sdata);
					element.setAttribute("velRange", velfrom + " - " + velto);

					subchunk.read(sdata);					
					element.setAttribute("options", "" + ByteConversion.shortToInt(sdata));
					
					subchunk.read(sdata);					
					int keygroup = ByteConversion.shortToInt(sdata);
					if(keygroup != 0)
						element.setAttribute("exclusiveClass" , "" + keygroup);
				}				
				
			}
		}
	}
	
	public void readWave(RiffReader chunk, Element element) throws Exception
	{
		while(chunk.hasNextChunk())
		{
			RiffReader subchunk = chunk.nextChunk();			

			if (subchunk.getFormat().equals("LIST")) {
				
				// Supplemental Information
				if (subchunk.getType().equals("INFO")) {
					readINFO(subchunk, element);
				}				
				
			}
			else
			{
				
				// Format
				if(subchunk.getFormat().equals("fmt "))
				{
/*				
				  short          wFormatTag; ( 1 = no compression )
				  unsigned short wChannels;         = channels
				  unsigned long  dwSamplesPerSec;   = samplerate
				  unsigned long  dwAvgBytesPerSec;  = dwSamplesPerSec * wBlockAlign
				  unsigned short wBlockAlign;       = framesize
				  unsigned short wBitsPerSample;    = bits ( 8, 16 e.t.c)
*/				
					
					// wFormatTag = 1 is no compression
					byte[] sdata = new byte[2];
					byte[] ldata = new byte[4];

					subchunk.read(sdata);
					int format = ByteConversion.shortToInt(sdata);
					if(format != 1)
					{
						element.setAttribute("format", "" + format);
					}

					subchunk.read(sdata);
					element.setAttribute("channels", "" + ByteConversion.shortToInt(sdata));					
					subchunk.read(ldata);
					element.setAttribute("samplerate", "" + ByteConversion.dwordToLong(ldata));
					subchunk.read(ldata);
					element.setAttribute("framrate", "" + ByteConversion.dwordToLong(ldata));
					subchunk.read(sdata);
					element.setAttribute("framesize", "" + ByteConversion.shortToInt(sdata));
					subchunk.read(sdata);
					element.setAttribute("bits", "" + ByteConversion.shortToInt(sdata));

					
				}
				
				// Data (wave data)
				if(subchunk.getFormat().equals("data"))
				{
					element.setAttribute("start", "" + isc.pos);
					element.setAttribute("length", "" + subchunk.available());
				}
				
				// GUID Identification: dlid
				if(subchunk.getFormat().equals("dlid"))
				{
					// Long Short Short Byte
					
					byte[] buffer = new byte[16];		
					subchunk.read(buffer);		
					element.setAttribute("dlsid", ByteConversion.guidToString(buffer));
				}				
				
			}
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		Collection coll = new Collection();
		FileInputStream fis = new FileInputStream("C:\\realmaker2005\\SoundFonts\\PersonalCopy\\gm.dls");
		coll.read(fis);
		fis.close();
		
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = tFactory.newTransformer();
			FileOutputStream fo = new FileOutputStream("C:\\gm.xml");
			transformer.transform(
					new DOMSource(coll.doc),
					new StreamResult(fo));
			fo.close();
		} catch (Exception e) {
		}		
	}
	
}
