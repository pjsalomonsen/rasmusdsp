/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util.riff.sf2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import rasmus.util.ByteConversion;

/*
 
 struct sfGenList
 {
 SFGenerator sfGenOper;
 genAmountType genAmount;
 };
 
 */

public class PresetGenerator extends SoundFontChunk {
	public int sfGenOper;
	public byte[] genAmount = new byte[2];
	
	public void read(InputStream stream) throws IOException {
		byte[] word_buffer = new byte[2];
		
		stream.read(word_buffer);
		sfGenOper = ByteConversion.wordToInt_LE(word_buffer);
		stream.read(genAmount);
		
	}
	
	public byte[] getBytes() throws IOException
	{
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		bo.write(ByteConversion.intToWord(sfGenOper));
		bo.write(genAmount);
		return bo.toByteArray();
	}
	
	public static String[] operators =
	{
		"startAddrsOffset",
		"endAddrsOffset",
		"startloopAddrsOffset",
		"endloopAddrsOffset",
		"startAddrsCoarseOffset",
		"modLfoToPitch",
		"vibLfoToPitch",
		"modEnvToPitch",
		"initialFilterFc",
		"initialFilterQ",
		"modLfoToFilterFc",
		"modEnvToFilterFc",
		"endAddrsCoarseOffset",
		"modLfoToVolume",
		"unused1",
		"chorusEffectsSend",
		"reverbEffectsSend",
		"pan",
		"unused2",
		"unused3",
		"unused4",
		"delayModLFO",
		"freqModLFO",
		"delayVibLFO",
		"freqVibLFO",
		"delayModEnv",
		"attackModEnv",
		"holdModEnv",
		"decayModEnv",
		"sustainModEnv",
		"releaseModEnv",
		"keynumToModEnvHold",
		"keynumToModEnvDecay",
		"delayVolEnv",
		"attackVolEnv",
		"holdVolEnv",
		"decayVolEnv",
		"sustainVolEnv",
		"releaseVolEnv",
		"keynumToVolEnvHold",
		"keynumToVolEnvDecay",
		"instrument",
		"reserved1",
		"keyRange",
		"velRange",
		"startloopAddrsCoarseOffset",
		"keynum",
		"velocity",
		"initialAttenuation",
		"reserved2",
		"endloopAddrsCoarseOffset",
		"coarseTune",
		"fineTune",
		"sampleID",
		"sampleModes",
		"reserved3",
		"scaleTuning",
		"exclusiveClass",
		"overridingRootKey",
		"unused5",
	"endOper" };
	
}
