/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util.riff.sf2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import rasmus.util.ByteConversion;
import rasmus.util.riff.RiffReader;

public class Info extends SoundFontChunk  {
	// version of the Sound Font RIFF file
	public int major = 2;
	public int minor = 1;
	
	// target Sound Engine
	public String targetEngine = "E-mu 10K1";
	
	// Sound Font Bank Name	
	public String bankName = "untitled";
	
	// Sound ROM Name
	public String romName = null;
	
	// Sound ROM Version
	public int romVersionMajor = -1;
	public int romVersionMinor = -1;
	
	// Date of Creation of the Bank
	public String creationDate = null;
	
	// Sound Designers and Engineers for the Bank
	public String engineers = null;
	
	// Product for which the Bank was intended
	public String product = null;
	
	// Copyright message
	public String copyright = null;
	
	// Comments
	public String comments = null;
	
	// The SoundFont tools used to create and alter the bank
	public String tools = null;		
	
	public void read(RiffReader subchunk) throws UnsupportedEncodingException, IOException
	{
		while(subchunk.hasNextChunk())
		{
			RiffReader subsubchunk = subchunk.nextChunk();
			String format = subsubchunk.getFormat();
			if(format.equals("ifil"))
			{
				byte[] bword = new byte[2];
				subsubchunk.read(bword);						
				major = ByteConversion.wordToInt_LE(bword);
				subsubchunk.read(bword);						
				minor = ByteConversion.wordToInt_LE(bword);
			}
			if(format.equals("isng"))
			{
				byte[] buffer = new byte[subsubchunk.available()];
				subsubchunk.read(buffer);
				this.targetEngine = ByteConversion.charToString(buffer);
			}
			if(format.equals("INAM"))
			{
				byte[] buffer = new byte[subsubchunk.available()];
				subsubchunk.read(buffer);
				this.bankName = ByteConversion.charToString(buffer);
			}					
			if(format.equals("irom"))
			{
				byte[] buffer = new byte[subsubchunk.available()];
				subsubchunk.read(buffer);
				this.romName = ByteConversion.charToString(buffer);
			}								
			if(format.equals("iver"))
			{
				byte[] bword = new byte[2];
				subsubchunk.read(bword);						
				romVersionMajor = ByteConversion.wordToInt_LE(bword);
				subsubchunk.read(bword);						
				romVersionMinor = ByteConversion.wordToInt_LE(bword);
			}									
			if(format.equals("ICRD"))
			{
				byte[] buffer = new byte[subsubchunk.available()];
				subsubchunk.read(buffer);
				this.creationDate = ByteConversion.charToString(buffer);
			}		
			if(format.equals("IENG"))
			{
				byte[] buffer = new byte[subsubchunk.available()];
				subsubchunk.read(buffer);
				this.engineers = ByteConversion.charToString(buffer);
			}		
			if(format.equals("IPRD"))
			{
				byte[] buffer = new byte[subsubchunk.available()];
				subsubchunk.read(buffer);
				this.product = ByteConversion.charToString(buffer);
			}		
			if(format.equals("ICOP"))
			{
				byte[] buffer = new byte[subsubchunk.available()];
				subsubchunk.read(buffer);
				this.copyright = ByteConversion.charToString(buffer);
			}		
			if(format.equals("ICMT"))
			{
				byte[] buffer = new byte[subsubchunk.available()];
				subsubchunk.read(buffer);
				this.comments = ByteConversion.charToString(buffer);
			}												
			if(format.equals("ISFT"))
			{
				byte[] buffer = new byte[subsubchunk.available()];
				subsubchunk.read(buffer);
				this.tools = ByteConversion.charToString(buffer);
			}												
			
			
		}					
		
	}
	
	
	public byte[] getBytes() throws IOException {
		
		byte[] major_bytes = ByteConversion.intToWord(major);
		byte[] minor_bytes = ByteConversion.intToWord(minor);
		byte[] targetEngine_bytes = targetEngine.getBytes("Latin1");
		byte[] bankName_bytes = bankName.getBytes("Latin1");
		
		
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		
		bo.write(ByteConversion.stringToChar("ifil", 4));
		bo.write(ByteConversion.longToDWord(major_bytes.length + minor_bytes.length));
		bo.write(major_bytes);
		bo.write(minor_bytes);
		
		
		bo.write(ByteConversion.stringToChar("INAM", 4));
		bo.write(ByteConversion.longToDWord(bankName_bytes.length+2));
		bo.write(bankName_bytes);
		bo.write(0);
		bo.write(0);	
		
		bo.write(ByteConversion.stringToChar("isng", 4));
		bo.write(ByteConversion.longToDWord(targetEngine_bytes.length+1));
		bo.write(targetEngine_bytes);
		bo.write(0);
		
		
		byte[] buffer;
		
		if(romName != null) {
			buffer = romName.getBytes("latin1");
			bo.write(ByteConversion.stringToChar("irom", 4));
			bo.write(ByteConversion.longToDWord(buffer.length+2));
			bo.write(buffer);
			bo.write(0);
			bo.write(0);			
		}	
		
		if(creationDate != null) {
			buffer = creationDate.getBytes("latin1");
			bo.write(ByteConversion.stringToChar("ICRD", 4));
			bo.write(ByteConversion.longToDWord(buffer.length+2));
			bo.write(buffer);
			bo.write(0);	
			bo.write(0);			
		}
		
		if(engineers != null) {
			buffer = engineers.getBytes("latin1");
			bo.write(ByteConversion.stringToChar("IENG", 4));
			bo.write(ByteConversion.longToDWord(buffer.length+2));
			bo.write(buffer);
			bo.write(0);	
			bo.write(0);			
		}			
		
		if(product != null) {
			buffer = product.getBytes("latin1");
			bo.write(ByteConversion.stringToChar("IPRD", 4));
			bo.write(ByteConversion.longToDWord(buffer.length+1));
			bo.write(buffer);
			bo.write(0);			
		}
		
		if(copyright != null) {
			buffer = copyright.getBytes("latin1");
			bo.write(ByteConversion.stringToChar("ICOP", 4));
			bo.write(ByteConversion.longToDWord(buffer.length+2));
			bo.write(buffer);	
			bo.write(0);	
			bo.write(0);	
		}
		
		if(comments != null) {				
			buffer = comments.getBytes("latin1");
			bo.write(ByteConversion.stringToChar("ICMT", 4));
			bo.write(ByteConversion.longToDWord(buffer.length+1));
			bo.write(buffer);
			bo.write(0);	
		}
		
		if(tools != null) {		
			buffer = tools.getBytes("latin1");
			bo.write(ByteConversion.stringToChar("ISFT", 4));
			bo.write(ByteConversion.longToDWord(buffer.length+2));
			bo.write(buffer);
			bo.write(0);	
			bo.write(0);	
		}	
		
		return bo.toByteArray();
	}	
}
