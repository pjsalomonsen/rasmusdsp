/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util.riff.sf2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import rasmus.util.ByteConversion;

/*

struct sfSample
	{
	CHAR achSampleName[20];
	DWORD dwStart;
	DWORD dwEnd;
	DWORD dwStartloop;
	DWORD dwEndloop;
	DWORD dwSampleRate;
	BYTE byOriginalKey;
	CHAR chPitchCorrection;
	WORD wSampleLink;
	SFSampleLink sfSampleType;
	};

*/

public class SampleHeader extends SoundFontChunk  {

	public String achSampleName;
	public long dwStart;
	public long dwEnd;
	public long dwStartloop;
	public long dwEndloop;
	public long dwSampleRate;
	public int byOriginalKey;
	public int chPitchCorrection;
	public int wSampleLink;	
	public int sfSampleType;

	public void read(InputStream stream) throws IOException
	{
		byte[] string_buffer = new byte[20];
		byte[] byte_buffer = new byte[1];
		byte[] word_buffer = new byte[2];
		byte[] dword_buffer = new byte[4];

		stream.read(string_buffer);
		achSampleName = ByteConversion.charToString(string_buffer);
		stream.read(dword_buffer);
		dwStart = ByteConversion.dwordToLong(dword_buffer);
		stream.read(dword_buffer);
		dwEnd = ByteConversion.dwordToLong(dword_buffer);
		stream.read(dword_buffer);
		dwStartloop = (int)ByteConversion.dwordToLong(dword_buffer);
		stream.read(dword_buffer);
		dwEndloop = (int)ByteConversion.dwordToLong(dword_buffer);
		stream.read(dword_buffer);
		dwSampleRate = ByteConversion.dwordToLong(dword_buffer);
		stream.read(byte_buffer);
		byOriginalKey = ByteConversion.byteToInt(byte_buffer);

		stream.read(byte_buffer);
		int ch = ByteConversion.byteToInt(byte_buffer);
		if(ch >= 128)
		   chPitchCorrection = ch - 256;
		else
		   chPitchCorrection = ch;
	
		stream.read(word_buffer);
		wSampleLink = ByteConversion.wordToInt_LE(word_buffer);

		stream.read(word_buffer);
		sfSampleType = ByteConversion.wordToInt_LE(word_buffer);	

	}
	
	public byte[] getBytes() throws IOException
	{
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		bo.write(ByteConversion.stringToChar(achSampleName, 20));
		bo.write(ByteConversion.longToDWord(dwStart));
		bo.write(ByteConversion.longToDWord(dwEnd));
		bo.write(ByteConversion.longToDWord(dwStartloop));
		bo.write(ByteConversion.longToDWord(dwEndloop));
		bo.write(ByteConversion.longToDWord(dwSampleRate));		
		bo.write(ByteConversion.intToByte(byOriginalKey));
		bo.write(ByteConversion.intToByte((chPitchCorrection + 256) % 256));
		bo.write(ByteConversion.intToWord(wSampleLink));		
		bo.write(ByteConversion.intToWord(sfSampleType));		
		return bo.toByteArray();
	}			

}
