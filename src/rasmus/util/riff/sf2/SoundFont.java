/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util.riff.sf2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import rasmus.util.ByteConversion;
import rasmus.util.riff.RiffReader;

public class SoundFont {
	
	long sampledata_size = 0;
	long sampledata_offset = 0;
	
	ArrayList presetheader_list = new ArrayList();	
	ArrayList presetindex_list = new ArrayList();
	ArrayList presetmodulator_list = new ArrayList();
	ArrayList presetgenerator_list = new ArrayList();
	ArrayList instrumentnameandindices_list = new ArrayList();
	ArrayList instrumentindex_list = new ArrayList();
	ArrayList instrumentmodulator_list = new ArrayList();
	ArrayList instrumentgenerator_list = new ArrayList();
	ArrayList sampleheader_list = new ArrayList();
	
	
	Info info = new Info();
	
	public Info getInfo() {
		return info;
	}
	
	public ArrayList getPresetHeaderList() { return presetheader_list; }	
	public ArrayList getPresetIndexList() { return presetindex_list; }
	public ArrayList getPresetModulatorList() { return presetmodulator_list; }
	public ArrayList getPresetGeneratorList() { return presetgenerator_list; }
	public ArrayList getInstrumentNamesAndIndicesList() { return instrumentnameandindices_list; }
	public ArrayList getInstrumentIndexList() { return instrumentindex_list; }
	public ArrayList getInsturmentModulatorList() { return instrumentmodulator_list; }
	public ArrayList getInstrumentGeneratorList() { return instrumentgenerator_list; }
	public ArrayList getSampleHeaderList() { return sampleheader_list; }
	
	
	public long getSampleDataSize() {
		return sampledata_size;
	}
	
	public InputStream getSampleDataStream(InputStream inputstream) throws Exception
	{
		RiffReader riffiterator = new RiffReader(inputstream);
		if (!riffiterator.getFormat().equals("RIFF"))
			throw new Exception("Incorrect format");
		
		while (riffiterator.hasNextChunk()) {
			RiffReader subchunk = riffiterator.nextChunk();
			if (subchunk.getFormat().equals("LIST")) {
				
				// Supplemental Information
				if (subchunk.getType().equals("sdta")) 
					if (subchunk.getFormat().equals("LIST"))
					{
						while (subchunk.hasNextChunk()) {
							RiffReader subsubchunk = subchunk.nextChunk();
							String format = subsubchunk.getFormat();
							
							// The Preset Headers
							if (format.equals("smpl")) {							
								return subsubchunk;
							}
						}											
					}
			}
		}
		return new ByteArrayInputStream(new byte[0]);
	}
	
	private byte[] arrayToBytes(ArrayList list) throws IOException
	{
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			SoundFontChunk chunk = (SoundFontChunk) iterator.next();
			bo.write(chunk.getBytes());
		}
		return bo.toByteArray();
	}
	
	private byte[] getPdtaBytes() throws IOException
	{
		byte[] phdr = arrayToBytes(presetheader_list); 
		byte[] pbag = arrayToBytes(presetindex_list); 
		byte[] pmod = arrayToBytes(presetmodulator_list); 
		byte[] pgen = arrayToBytes(presetgenerator_list); 
		byte[] inst = arrayToBytes(instrumentnameandindices_list); 
		byte[] ibag = arrayToBytes(instrumentindex_list); 
		byte[] imod = arrayToBytes(instrumentmodulator_list); 
		byte[] igen = arrayToBytes(instrumentgenerator_list); 
		byte[] shdr = arrayToBytes(sampleheader_list);
		
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		bo.write(ByteConversion.stringToChar("phdr", 4));
		bo.write(ByteConversion.longToDWord(phdr.length));
		bo.write(phdr);
		bo.write(ByteConversion.stringToChar("pbag", 4));
		bo.write(ByteConversion.longToDWord(pbag.length));
		bo.write(pbag);
		bo.write(ByteConversion.stringToChar("pmod", 4));
		bo.write(ByteConversion.longToDWord(pmod.length));
		bo.write(pmod);
		bo.write(ByteConversion.stringToChar("pgen", 4));
		bo.write(ByteConversion.longToDWord(pgen.length));
		bo.write(pgen);
		bo.write(ByteConversion.stringToChar("inst", 4));
		bo.write(ByteConversion.longToDWord(inst.length));
		bo.write(inst);
		bo.write(ByteConversion.stringToChar("ibag", 4));
		bo.write(ByteConversion.longToDWord(ibag.length));
		bo.write(ibag);
		bo.write(ByteConversion.stringToChar("imod", 4));
		bo.write(ByteConversion.longToDWord(imod.length));
		bo.write(imod);
		bo.write(ByteConversion.stringToChar("igen", 4));
		bo.write(ByteConversion.longToDWord(igen.length));
		bo.write(igen);
		bo.write(ByteConversion.stringToChar("shdr", 4));
		bo.write(ByteConversion.longToDWord(shdr.length));
		bo.write(shdr);
		
		return bo.toByteArray();
		
		
	}
	
	/*
	 *         int size = 4 + 
	 8 + major_bytes.length + minor_bytes.length +
	 8 + targetEngine_bytes.length +
	 8 + bankName_bytes.length;
	 bo.write(ChunkIterator.stringToChar("LIST", 4));
	 bo.write(ChunkIterator.longToDWord(size));
	 bo.write(ChunkIterator.stringToChar("INFO", 4));
	 
	 */	
	
	public void write(OutputStream outputstream, InputStream samplestream, long samplesize) throws IOException
	{				
		
		byte[] info_bytes = getInfo().getBytes();
		byte[] pdta_bytes = getPdtaBytes();
		
		long sdta_size = 12 + samplesize;
		
		long size = 4 + 8 +
		12 + info_bytes.length +
		12 + pdta_bytes.length +
		12 + samplesize;
		
		outputstream.write(ByteConversion.stringToChar("RIFF", 4));
		outputstream.write(ByteConversion.longToDWord(size));
		outputstream.write(ByteConversion.stringToChar("sfbk", 4));
		
		
		outputstream.write(ByteConversion.stringToChar("LIST", 4));
		outputstream.write(ByteConversion.longToDWord(info_bytes.length+4));
		outputstream.write(ByteConversion.stringToChar("INFO", 4));
		outputstream.write(info_bytes);
		
		outputstream.write(ByteConversion.stringToChar("LIST", 4));
		outputstream.write(ByteConversion.longToDWord(sdta_size));
		outputstream.write(ByteConversion.stringToChar("sdta", 4));
		outputstream.write(ByteConversion.stringToChar("smpl", 4));
		outputstream.write(ByteConversion.longToDWord(samplesize));
		
		byte[] buffer = new byte[4096];
		long sleft = samplesize;
		while(sleft != 0)
		{
			if(buffer.length > sleft) buffer = new byte[(int)sleft];
			samplestream.read(buffer);
			sleft -= buffer.length;
			outputstream.write(buffer);
		}
		
		outputstream.write(ByteConversion.stringToChar("LIST", 4));
		outputstream.write(ByteConversion.longToDWord(pdta_bytes.length+4));
		outputstream.write(ByteConversion.stringToChar("pdta", 4));
		outputstream.write(pdta_bytes);				
		
		
	}
	
	class InputStreamCounter extends InputStream
	{
		InputStream inputstream;
		long pos = 0;
		public long getPosition()
		{
			return pos;
		}
		public InputStreamCounter(InputStream inputstream)
		{
			this.inputstream = inputstream;
		}
		public int read() throws IOException {
			int len = inputstream.read();
			if(len != -1) pos += 1;
			return len;
		}	
		public int available() throws IOException {
			return inputstream.available();
		}
		public int read(byte[] b, int off, int len) throws IOException {
			int readed = inputstream.read(b, off, len);
			if(readed > 0) pos += (long)readed;
			return readed;			
		}		
		public int read(byte[] b) throws IOException {		
			int readed = inputstream.read(b);
			if(readed > 0) pos += (long)readed;
			return readed;		
		}
		public long skip(long n) throws IOException {
			long readed = inputstream.skip(n);
			if(readed > 0) pos += readed;
			return readed;		
		}
	}
	
	public void read(InputStream inputstream) throws Exception {
		
		InputStreamCounter isc = new InputStreamCounter(inputstream);
		RiffReader riffiterator = new RiffReader(isc);
		if (!riffiterator.getFormat().equals("RIFF"))
			throw new Exception("Incorrect format");
		
		while (riffiterator.hasNextChunk()) {
			RiffReader subchunk = riffiterator.nextChunk();
			if (subchunk.getFormat().equals("LIST")) {
				
				// Supplemental Information
				if (subchunk.getType().equals("INFO")) {
					if (subchunk.getFormat().equals("LIST"))
						info.read(subchunk);
				}
				
				// The Sample Binary Data
				if (subchunk.getType().equals("sdta")) {
					
					while (subchunk.hasNextChunk()) {
						RiffReader subsubchunk = subchunk.nextChunk();
						String format = subsubchunk.getFormat();
						
						// The Preset Headers
						if (format.equals("smpl")) {
							sampledata_size = subsubchunk.available();								
							sampledata_offset = isc.getPosition(); 
						}
						
					}											
				}
				
				// The Preset, Instrument, and Sample Header data
				if (subchunk.getType().equals("pdta")) {
					if (subchunk.getFormat().equals("LIST"))
						while (subchunk.hasNextChunk()) {
							RiffReader subsubchunk = subchunk.nextChunk();
							String format = subsubchunk.getFormat();
							
							// The Preset Headers
							if (format.equals("phdr")) {
								while(subsubchunk.available() != 0)
								{
									PresetHeader presetheader = new PresetHeader();
									presetheader.read(subsubchunk);
									presetheader_list.add(presetheader);
								}
							}
							
							// The Preset Index list
							if (format.equals("pbag")) {
								while(subsubchunk.available() != 0)
								{
									PresetIndex presetindex = new PresetIndex();
									presetindex.read(subsubchunk);
									presetindex_list.add(presetindex);
								}
							}
							
							// The Preset Modulator list
							if (format.equals("pmod")) {
								while(subsubchunk.available() != 0)
								{
									PresetModulator presetmodulator = new PresetModulator();
									presetmodulator.read(subsubchunk);
									presetmodulator_list.add(presetmodulator);
								}
							}
							
							// The Preset Generator list
							if (format.equals("pgen")) {
								while(subsubchunk.available() != 0)
								{
									PresetGenerator presetgenerator = new PresetGenerator();
									presetgenerator.read(subsubchunk);
									presetgenerator_list.add(presetgenerator);
								}
							}
							
							// The Instrument Names and Indices
							if (format.equals("inst")) {
								while(subsubchunk.available() != 0)
								{
									InstrumentNamesAndIndices instrumentnameandindices = new InstrumentNamesAndIndices();
									instrumentnameandindices.read(subsubchunk);
									instrumentnameandindices_list.add(instrumentnameandindices);
								}
							}
							
							// The Instrument Index list
							if (format.equals("ibag")) {
								while(subsubchunk.available() != 0)
								{
									InstrumentIndex instrumentindex = new InstrumentIndex();
									instrumentindex.read(subsubchunk);
									instrumentindex_list.add(instrumentindex);
								}
							}																																			
							
							// The Instrument Modulator list
							if (format.equals("imod")) {
								while(subsubchunk.available() != 0)
								{
									InstrumentModulator instrumentmodulator = new InstrumentModulator();
									instrumentmodulator.read(subsubchunk);
									instrumentmodulator_list.add(instrumentmodulator);
								}
							}
							
							// The Instrument Generator list
							if (format.equals("igen")) {
								while(subsubchunk.available() != 0)
								{
									InstrumentGenerator instrumentgenerator = new InstrumentGenerator();
									instrumentgenerator.read(subsubchunk);
									instrumentgenerator_list.add(instrumentgenerator);
								}							
							}
							
							// The Sample Headers
							if (format.equals("shdr")) {
								while(subsubchunk.available() != 0)
								{
									SampleHeader sampleheader = new SampleHeader();
									sampleheader.read(subsubchunk);
									sampleheader_list.add(sampleheader);
								}
							}							
							
						}
				}
				
			}
			
		}
	}
	
	private void setAttribute(Element element, String attrname, String value)
	{
		if(value != null) element.setAttribute(attrname, value);
	}
	
	public Document toDocument() throws Exception
	{
		Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		
		Element soundfont_element = doc.createElement("SoundFont");
		doc.appendChild(soundfont_element);
		
		setAttribute(soundfont_element, "version", getInfo().major + "." + getInfo().minor);
		setAttribute(soundfont_element, "bankname", getInfo().bankName);
		setAttribute(soundfont_element, "engineers", getInfo().engineers);
		setAttribute(soundfont_element, "creationdate", getInfo().creationDate);
		setAttribute(soundfont_element, "comments", getInfo().comments);
		
		setAttribute(soundfont_element, "product", getInfo().product);
		setAttribute(soundfont_element, "romname", getInfo().romName);
		if(getInfo().romName != null)
			setAttribute(soundfont_element, "romversion", getInfo().romVersionMajor + "." + getInfo().romVersionMinor);
		setAttribute(soundfont_element, "targetengine", getInfo().targetEngine);
		setAttribute(soundfont_element, "tools", getInfo().tools);
		
		setAttribute(soundfont_element, "sampledata_offset", ""+ sampledata_offset);
		setAttribute(soundfont_element, "sampledata_length", ""+ sampledata_size);
		
		
		ArrayList preset_list = getPresetHeaderList();
		ArrayList presetindex_list = getPresetIndexList();
		ArrayList presetgen_list = getPresetGeneratorList();
		for (int j = 0; j < preset_list.size(); j++) {
			
			PresetHeader preset = (PresetHeader) preset_list.get(j);
			
			Element preset_element = doc.createElement("preset");
			if(!preset.achPresetName.trim().equals("EOP"))
				soundfont_element.appendChild(preset_element);	
			
			
			preset_element.setAttribute("id", ""+ j);
			preset_element.setAttribute("name", ""+preset.achPresetName.trim());
			preset_element.setAttribute("bank", ""+preset.wBank);
			preset_element.setAttribute("preset", ""+preset.wPreset);
			preset_element.setAttribute("library", ""+preset.dwLibrary);
			preset_element.setAttribute("genre", ""+preset.dwGenre);
			preset_element.setAttribute("morphology", ""+preset.dwMorphology);
			
			int offset = preset.wPresetBagNdx;
			int nextoffset;
			if((j + 1) < preset_list.size())
			{
				PresetHeader nextpreset = (PresetHeader) preset_list.get(j + 1);
				nextoffset = nextpreset.wPresetBagNdx;
			}
			else
				nextoffset = presetindex_list.size();
			
			for (int i = offset; i < nextoffset; i++) {
				
				PresetIndex presetindex = (PresetIndex)presetindex_list.get(i);
				
				int b_offset = presetindex.wGenNdx;
				int b_nextoffset;
				if((i + 1) < presetindex_list.size())
				{
					PresetIndex b_presetindex = (PresetIndex) presetindex_list.get(i + 1);
					b_nextoffset = b_presetindex.wGenNdx;
				}
				else
					b_nextoffset = presetgen_list.size();
				
				Element generator_element = doc.createElement("generator");
				preset_element.appendChild(generator_element);				
				
				for (int k = b_offset; k < b_nextoffset; k++) {
					PresetGenerator generator = (PresetGenerator)presetgen_list.get(k);
					
					String value;
					if(generator.sfGenOper == 43 || generator.sfGenOper == 44)
						value = "" + ByteConversion.byteToInt(generator.genAmount[0]) + " - " + ByteConversion.byteToInt(generator.genAmount[1]);
					else
						if(generator.sfGenOper == 41 || generator.sfGenOper == 53)
							value = "" + ByteConversion.wordToInt_LE(generator.genAmount);
						else
							value = "" + ByteConversion.shortToInt(generator.genAmount);
					
					generator_element.setAttribute(InstrumentGenerator.operators[generator.sfGenOper], value );
				}		
				
			}
			
		}
		
		
		ArrayList instrument_list = getInstrumentNamesAndIndicesList();
		ArrayList instrumentindex_list = getInstrumentIndexList();
		ArrayList instrumentgen_list = getInstrumentGeneratorList();
		for (int j = 0; j < instrument_list.size(); j++) {
			
			InstrumentNamesAndIndices ins = (InstrumentNamesAndIndices) instrument_list.get(j);
			
			Element instrument = doc.createElement("instrument");
			if(!ins.achInstName.trim().equals("EOI"))
				soundfont_element.appendChild(instrument);	
			
			
			instrument.setAttribute("id", ""+ j);
			instrument.setAttribute("name", ins.achInstName.trim());
			
			int offset = ins.wInstBagNdx;
			int nextoffset;
			if((j + 1) < instrument_list.size())
			{
				InstrumentNamesAndIndices nextins = (InstrumentNamesAndIndices) instrument_list.get(j + 1);
				nextoffset = nextins.wInstBagNdx;
			}
			else
				nextoffset = instrumentindex_list.size();
			
			for (int i = offset; i < nextoffset; i++) {
				
				InstrumentIndex insindex = (InstrumentIndex)instrumentindex_list.get(i);
				
				int b_offset = insindex.wGenNdx;
				int b_nextoffset;
				if((i + 1) < instrumentindex_list.size())
				{
					InstrumentIndex b_insindex = (InstrumentIndex) instrumentindex_list.get(i + 1);
					b_nextoffset = b_insindex.wGenNdx;
				}
				else
					b_nextoffset = instrumentgen_list.size();
				
				Element generator_element = doc.createElement("generator");
				instrument.appendChild(generator_element);				
				
				
				for (int k = b_offset; k < b_nextoffset; k++) {
					InstrumentGenerator generator = (InstrumentGenerator)instrumentgen_list.get(k);
					
					String value;
					if(generator.sfGenOper == 43 || generator.sfGenOper == 44)
						value = "" + ByteConversion.byteToInt(generator.genAmount[0]) + " - " + ByteConversion.byteToInt(generator.genAmount[1]);
					else
						if(generator.sfGenOper == 41 || generator.sfGenOper == 53)
							value = "" + ByteConversion.wordToInt_LE(generator.genAmount);
						else
							value = "" + ByteConversion.shortToInt(generator.genAmount);
					
					generator_element.setAttribute(InstrumentGenerator.operators[generator.sfGenOper], value );
				}		
				
			}
			
		}		
		
		ArrayList sample_list = getSampleHeaderList();
		for (int i = 0; i < sample_list.size(); i++) {
			SampleHeader sampleheader = (SampleHeader)sample_list.get(i);
			
			Element sample = doc.createElement("sample");
			
			if(!sampleheader.achSampleName.trim().equals("EOS"))
				soundfont_element.appendChild(sample);
			
			sample.setAttribute("id", ""+ i);
			sample.setAttribute("name", sampleheader.achSampleName.trim());
			sample.setAttribute("samplerate", ""+ sampleheader.dwSampleRate);
			sample.setAttribute("start", ""+ sampleheader.dwStart);
			sample.setAttribute("end", ""+ sampleheader.dwEnd);
			sample.setAttribute("offset", ""+ ((sampleheader.dwStart*2) + sampledata_offset));
			sample.setAttribute("datalength", ""+ 2*(sampleheader.dwEnd - sampleheader.dwStart));
			sample.setAttribute("length", ""+ (sampleheader.dwEnd - sampleheader.dwStart));
			sample.setAttribute("loopstart", ""+ sampleheader.dwStartloop);
			sample.setAttribute("loopend", ""+ sampleheader.dwEndloop);			
			sample.setAttribute("originalkey", ""+ sampleheader.byOriginalKey);
			sample.setAttribute("pitchcorrection", ""+ sampleheader.chPitchCorrection);
			sample.setAttribute("sampletype", ""+ sampleheader.sfSampleType);
			sample.setAttribute("samplelink", ""+ sampleheader.wSampleLink);
			
		}
		
		
		
		return doc;
	}
	
	public String toString()
	{
		SoundFont sf = this;
		
		StringBuffer stringbuffer = new StringBuffer();
		
		
		stringbuffer.append("Version = " + sf.getInfo().major + "." + sf.getInfo().minor);
		stringbuffer.append("\nBank Name = " + sf.getInfo().bankName);
		stringbuffer.append("\nEngineers = " + sf.getInfo().engineers);
		stringbuffer.append("\nCreated = " + sf.getInfo().creationDate);
		stringbuffer.append("\nComments = " + sf.getInfo().comments);
		stringbuffer.append("\nCopyright = " + sf.getInfo().copyright);
		stringbuffer.append("\nProduct = " + sf.getInfo().product);
		stringbuffer.append("\nRom Name = " + sf.getInfo().romName);
		stringbuffer.append("\nRom Version = " + sf.getInfo().romVersionMajor + "." + sf.getInfo().romVersionMinor);
		stringbuffer.append("\nTarget Engine = " + sf.getInfo().targetEngine);
		stringbuffer.append("\nTarget Tools = " + sf.getInfo().tools);
		stringbuffer.append("\n\n");
		
		ArrayList preset_list = sf.getPresetHeaderList();
		ArrayList presetindex_list = sf.getPresetIndexList();
		ArrayList presetgen_list = sf.getPresetGeneratorList();
		for (int j = 0; j < preset_list.size(); j++) {
			
			PresetHeader preset = (PresetHeader) preset_list.get(j);
			
			stringbuffer.append("\n<Preset id = "+ j + ">");
			stringbuffer.append("\n  Name = " + preset.achPresetName);
			stringbuffer.append("\n  Bank = " + preset.wBank);
			stringbuffer.append("\n  Preset = " + preset.wPreset);
			stringbuffer.append("\n  Library = " + preset.dwLibrary);
			stringbuffer.append("\n  Genre = " + preset.dwGenre);
			stringbuffer.append("\n  Morphology = " + preset.dwMorphology);			
			
			int offset = preset.wPresetBagNdx;
			int nextoffset;
			if((j + 1) < preset_list.size())
			{
				PresetHeader nextpreset = (PresetHeader) preset_list.get(j + 1);
				nextoffset = nextpreset.wPresetBagNdx;
			}
			else
				nextoffset = presetindex_list.size();
			
			for (int i = offset; i < nextoffset; i++) {
				
				PresetIndex presetindex = (PresetIndex)presetindex_list.get(i);
				
				int b_offset = presetindex.wGenNdx;
				int b_nextoffset;
				if((i + 1) < presetindex_list.size())
				{
					PresetIndex b_presetindex = (PresetIndex) presetindex_list.get(i + 1);
					b_nextoffset = b_presetindex.wGenNdx;
				}
				else
					b_nextoffset = presetgen_list.size();
				
				stringbuffer.append("\n  <Generator>"); 
				for (int k = b_offset; k < b_nextoffset; k++) {
					PresetGenerator generator = (PresetGenerator)presetgen_list.get(k);
					
					String value;
					if(generator.sfGenOper == 43 || generator.sfGenOper == 44)
						value = "" + ByteConversion.byteToInt(generator.genAmount[0]) + " - " + ByteConversion.byteToInt(generator.genAmount[1]);
					else
						if(generator.sfGenOper == 41 || generator.sfGenOper == 53)
							value = "" + ByteConversion.wordToInt_LE(generator.genAmount);
						else
							value = "" + ByteConversion.shortToInt(generator.genAmount);
					
					stringbuffer.append("\n     " + PresetGenerator.operators[generator.sfGenOper]  + " = " + value );
				}		
				
			}
			
		}
		
		
		ArrayList instrument_list = sf.getInstrumentNamesAndIndicesList();
		ArrayList instrumentindex_list = sf.getInstrumentIndexList();
		ArrayList instrumentgen_list = sf.getInstrumentGeneratorList();
		for (int j = 0; j < instrument_list.size(); j++) {
			
			InstrumentNamesAndIndices ins = (InstrumentNamesAndIndices) instrument_list.get(j);
			
			stringbuffer.append("\n<Instrument id = "+ j + ">");
			stringbuffer.append("\n  Name = " + ins.achInstName);
			
			int offset = ins.wInstBagNdx;
			int nextoffset;
			if((j + 1) < instrument_list.size())
			{
				InstrumentNamesAndIndices nextins = (InstrumentNamesAndIndices) instrument_list.get(j + 1);
				nextoffset = nextins.wInstBagNdx;
			}
			else
				nextoffset = instrumentindex_list.size();
			
			for (int i = offset; i < nextoffset; i++) {
				
				InstrumentIndex insindex = (InstrumentIndex)instrumentindex_list.get(i);
				
				int b_offset = insindex.wGenNdx;
				int b_nextoffset;
				if((i + 1) < instrumentindex_list.size())
				{
					InstrumentIndex b_insindex = (InstrumentIndex) instrumentindex_list.get(i + 1);
					b_nextoffset = b_insindex.wGenNdx;
				}
				else
					b_nextoffset = instrumentgen_list.size();
				
				stringbuffer.append("\n  <Generator>"); 
				for (int k = b_offset; k < b_nextoffset; k++) {
					InstrumentGenerator generator = (InstrumentGenerator)instrumentgen_list.get(k);
					
					String value;
					if(generator.sfGenOper == 43 || generator.sfGenOper == 44)
						value = "" + ByteConversion.byteToInt(generator.genAmount[0]) + " - " + ByteConversion.byteToInt(generator.genAmount[1]);
					else
						if(generator.sfGenOper == 41 || generator.sfGenOper == 53)
							value = "" + ByteConversion.wordToInt_LE(generator.genAmount);
						else
							value = "" + ByteConversion.shortToInt(generator.genAmount);
					
					stringbuffer.append("\n     " + InstrumentGenerator.operators[generator.sfGenOper]  + " = " + value );
				}		
				
			}
			
		}		
		
		ArrayList sample_list = sf.getSampleHeaderList();
		for (int i = 0; i < sample_list.size(); i++) {
			SampleHeader sampleheader = (SampleHeader)sample_list.get(i);
			stringbuffer.append("\n<Sample id = "+ i + ">");
			stringbuffer.append("\n  Name = " + sampleheader.achSampleName);
			stringbuffer.append("\n  SampleRate = " + sampleheader.dwSampleRate);
			stringbuffer.append("\n  Start = " + sampleheader.dwStart);
			stringbuffer.append("\n  End = " + sampleheader.dwEnd);			
			stringbuffer.append("\n  Length = " + (sampleheader.dwEnd - sampleheader.dwStart));					
			stringbuffer.append("\n  Loop Start = " + sampleheader.dwStartloop);
			stringbuffer.append("\n  Loop End = " + sampleheader.dwEndloop);
			stringbuffer.append("\n  Original Key = " + sampleheader.byOriginalKey);
			stringbuffer.append("\n  Pitch Correction = " + sampleheader.chPitchCorrection);						
			stringbuffer.append("\n  Sample Type = " + sampleheader.sfSampleType);
			stringbuffer.append("\n  Sample Link = " + sampleheader.wSampleLink);
			
		}
		
		stringbuffer.append("\n<SampleData offset="+ sampledata_offset +" length=" + sampledata_size + ">");
		
		
		return stringbuffer.toString();
		
	}
}
