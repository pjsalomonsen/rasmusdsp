/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util.riff.sf2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import rasmus.util.ByteConversion;

/*
 
 struct sfPresetHeader
 {
 CHAR achPresetName[20];
 WORD wPreset;
 WORD wBank;
 WORD wPresetBagNdx;
 DWORD dwLibrary;
 DWORD dwGenre;
 DWORD dwMorphology;
 };
 
 */

public class PresetHeader  extends SoundFontChunk {
	public String achPresetName;
	public int wPreset;
	public int wBank;
	public int wPresetBagNdx;
	public long dwLibrary;
	public long dwGenre;
	public long dwMorphology;
	
	public void read(InputStream stream) throws IOException
	{
		byte[] string_buffer = new byte[20];
		byte[] word_buffer = new byte[2];
		byte[] dword_buffer = new byte[4];
		
		stream.read(string_buffer);
		achPresetName = ByteConversion.charToString(string_buffer);
		
		stream.read(word_buffer);
		wPreset = ByteConversion.wordToInt_LE(word_buffer);
		stream.read(word_buffer);
		wBank = ByteConversion.wordToInt_LE(word_buffer);
		stream.read(word_buffer);
		wPresetBagNdx = ByteConversion.wordToInt_LE(word_buffer);
		
		stream.read(dword_buffer);
		dwLibrary = ByteConversion.dwordToLong(dword_buffer);
		stream.read(dword_buffer);
		dwGenre = ByteConversion.dwordToLong(dword_buffer);
		stream.read(dword_buffer);
		dwMorphology = ByteConversion.dwordToLong(dword_buffer);
		
	}
	
	public byte[] getBytes() throws IOException
	{
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		bo.write(ByteConversion.stringToChar(achPresetName, 20));
		bo.write(ByteConversion.intToWord(wPreset));
		bo.write(ByteConversion.intToWord(wBank));
		bo.write(ByteConversion.intToWord(wPresetBagNdx));
		bo.write(ByteConversion.longToDWord(dwLibrary));
		bo.write(ByteConversion.longToDWord(dwGenre));
		bo.write(ByteConversion.longToDWord(dwMorphology));
		
		return bo.toByteArray();
	}	
}
