/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util.riff.sf2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import rasmus.util.ByteConversion;

/*
 
 struct sfModList
 {
 SFModulator sfModSrcOper;
 SFGenerator sfModDestOper;
 SHORT modAmount;
 SFModulator sfModAmtSrcOper;
 SFTransform sfModTransOper;
 };
 
 */

public class PresetModulator  extends SoundFontChunk {
	
	public int sfModSrcOper;
	public int sfModDestOper;
	public int modAmount;
	public int sfModAmtSrcOper;
	public int sfModTransOper;
	
	public void read(InputStream stream) throws IOException
	{
		byte[] word_buffer = new byte[2];
		byte[] short_buffer = new byte[2];
		
		stream.read(word_buffer);
		sfModSrcOper = ByteConversion.wordToInt_LE(word_buffer);
		stream.read(word_buffer);
		sfModDestOper = ByteConversion.wordToInt_LE(word_buffer);
		
		stream.read(short_buffer);
		modAmount = ByteConversion.shortToInt(short_buffer);
		
		stream.read(word_buffer);
		sfModAmtSrcOper = ByteConversion.wordToInt_LE(word_buffer);
		stream.read(word_buffer);
		sfModTransOper = ByteConversion.wordToInt_LE(word_buffer);
		
	}
	public byte[] getBytes() throws IOException
	{
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		bo.write(ByteConversion.intToWord(sfModSrcOper));
		bo.write(ByteConversion.intToWord(sfModDestOper));
		bo.write(ByteConversion.intToShort(modAmount));
		bo.write(ByteConversion.intToWord(sfModAmtSrcOper));
		bo.write(ByteConversion.intToWord(sfModTransOper));
		return bo.toByteArray();
	}		
	
}
