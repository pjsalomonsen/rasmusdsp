/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util.riff;

import java.io.IOException;
import java.io.InputStream;

public class ChunkReader extends InputStream {
	
	protected long ckSize = 0;
	
	public long getSize()
	{
		return ckSize;
	}
	
	private InputStream stream;
	protected long avail;
	
	public ChunkReader(InputStream stream) throws IOException
	{
		this.stream = stream;
		avail = stream.available();
		ckSize = stream.available();
	}
	
	RiffReader lastiterator = null;
	
	public boolean hasNextChunk() throws IOException
	{
		if(lastiterator != null) lastiterator.finish();
		return avail != 0;
	}
	
	public RiffReader nextChunk() throws IOException
	{
		if(lastiterator != null) lastiterator.finish();
		if(avail == 0) return null;
		lastiterator = new RiffReader(this);
		return lastiterator;
	}
	
	public int read() throws IOException {
		if(avail == 0) return -1;		
		avail--;
		return stream.read();
	}
	
	public int read(byte[] b) throws IOException {
		if(avail == 0) 
			return -1;
		if(b.length > avail)
		{
			int len = stream.read(b,0,(int)avail);			
			avail = 0;
			return len;
		}
		else
		{
			avail -= b.length;
			return stream.read(b);
		}
	}	
	
	public long skip(long n) throws IOException {
		if(avail == 0) 
			return -1;
		if(n > avail)
		{
			long len = stream.skip(avail);			
			avail = 0;
			return len;
		}
		else
		{
			avail -= n;
			return stream.skip(n);
		}
	}	
	
	public int available()
	{
		return (int)avail;
	}
	
	public void finish() throws IOException
	{
		if(avail != 0)
		{
			stream.skip(avail);
			avail = 0;
		}
	}
	
	public void close() throws IOException
	{
		stream.close();
	}
	
}
