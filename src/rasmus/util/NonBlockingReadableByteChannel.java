package rasmus.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;


class ThreadQueueWorker extends Thread
{
	public static ThreadQueueWorker sharedworker = null; 
	public static ThreadQueueWorker getInstance()
	{
		if(sharedworker == null)
		{
			ThreadQueueWorker sharedworker = new ThreadQueueWorker();
			sharedworker.setDaemon(true);
			sharedworker.start();
			sharedworker.setPriority(Thread.MAX_PRIORITY);
			ThreadQueueWorker.sharedworker = sharedworker;
		}
		return sharedworker;
	}	
	ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(300);
	boolean active = true;
	public void run()
	{
		try
		{
			while(active)
			{
				Runnable item = queue.poll(50, TimeUnit.SECONDS);
				if(item != null) item.run();
			}
		}
		catch (InterruptedException e) {			
		}
	}	
	public void stopThreadQueue() throws InterruptedException
	{
		queue.offer(new Runnable()
				{
					public void run()
					{
						active = false;
					}
				}, 10, TimeUnit.SECONDS);
	}
	public void invokeLater(Runnable runnable)
	{
		if(!queue.offer(runnable))
		{
		try {
			queue.offer(runnable, 10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		}
	}
}

public class NonBlockingReadableByteChannel implements Runnable, ReadableByteChannel
{
	private int buffersize = 2048;
	
	private ByteBuffer currentbuffer = null;
	private ByteBuffer nextbuffer = null;
	private FileChannel filechannel = null;
	private RandomAccessFile raf = null;
	private File file = null;
	
	private volatile boolean reading = false;
	private boolean notinited = true;
	
	private ThreadQueueWorker tworker;
	
	private long filepos;	
	
	public NonBlockingReadableByteChannel(NonBlockingReadableByteChannel channel) throws InterruptedException
	{
		if(channel.notinited)
		{
			System.out.println("File was not inited, wait for init !");
			while(channel.notinited)
			{
				Thread.sleep(10);
			}
		}
		
		
		//channel.nextbuffer_shared = true;
		//channel.currentbuffer_shared = true;
		buffersize = channel.buffersize;
		file = channel.file;
		//nextbuffer_shared = true;
		//currentbuffer_shared = true;
		filepos = channel.filepos;
		tworker = channel.tworker;
		reading = false;
		notinited = false;
		currentbuffer = channel.currentbuffer.asReadOnlyBuffer();
		nextbuffer = channel.nextbuffer.asReadOnlyBuffer();		
		
	}
	
	public NonBlockingReadableByteChannel(File file, long pos, int buffersize) throws Exception
	{
		this.buffersize = buffersize;
		currentbuffer = ByteBuffer.allocate(buffersize);
		nextbuffer    = ByteBuffer.allocate(buffersize);
		
		this.file = file;
		filepos = pos;
		tworker = ThreadQueueWorker.getInstance();
		
		Runnable r = new Runnable()
		{
			public void run()
			{
				currentbuffer.rewind();
				nextbuffer.rewind();
				
				try {									
					
					NonBlockingReadableByteChannel.this.raf  = new RandomAccessFile(NonBlockingReadableByteChannel.this.file, "r");
					NonBlockingReadableByteChannel.this.filechannel = raf.getChannel();

					NonBlockingReadableByteChannel.this.filechannel.position(filepos);
					long ret = NonBlockingReadableByteChannel.this.filechannel.read(currentbuffer);
					filepos += ret;
					ret = NonBlockingReadableByteChannel.this.filechannel.read(nextbuffer);
					filepos += ret;
					
					NonBlockingReadableByteChannel.this.raf.close();
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				NonBlockingReadableByteChannel.this.filechannel = null;
				NonBlockingReadableByteChannel.this.raf = null;
				
				currentbuffer.rewind();
				nextbuffer.rewind();
				notinited = false;				
			}
		};
		
		tworker.invokeLater(r);
		
		
		
	}
	
	public void run()
	{
		try
		{
			if(filechannel == null)
			{
				raf = new RandomAccessFile(file, "r");
				filechannel = raf.getChannel();
				filechannel.position(filepos);
				
				Thread.yield();
			}
			
			if(nextbuffer.isReadOnly())
			{
				nextbuffer = ByteBuffer.allocate(buffersize);
			}
			
			nextbuffer.rewind();			
			long ret =  filechannel.read(nextbuffer);
			if(ret == -1)
			{
				nextbuffer = null;
				reading = false;	
				return;
			}
			nextbuffer.limit((int)ret);			
			filepos += ret;
			nextbuffer.rewind();	
			
			reading = false;				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private boolean nextBuffer()
	{
		if(reading)
		{
			System.out.println("File I/O UnderFlow !");
			while(reading)
			{
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		
		ByteBuffer nextbuffer2 = currentbuffer;
		currentbuffer = nextbuffer;
		nextbuffer = nextbuffer2;
		
		reading = true;
		tworker.invokeLater(this);
		
		if(currentbuffer == null) return false;
		
		return true;
	}
	
	public int read(ByteBuffer buffer)
	{			
		if(notinited)
		{
			System.out.println("File was not inited, wait for init !");
			while(notinited)
			{
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
					return -1;
				}
			}
		}
		
		int ret = 0;
		if(currentbuffer == null) return -1;
		while(buffer.hasRemaining())				
		{
			if(buffer.remaining() <= currentbuffer.remaining())
			{				
				ret += buffer.remaining();
				currentbuffer.get(buffer.array(),  buffer.position() + buffer.arrayOffset(), buffer.remaining());
				return ret;
			}
			else
			{
				ret += currentbuffer.remaining();
				buffer.put(currentbuffer);
				if(!nextBuffer()) return ret;
			}
		}
		return ret;
	}

	public boolean isOpen() {
		return true;
	}

	public void close() throws IOException {
		Runnable r = new Runnable()
		{
			public void run()
			{
				if(filechannel != null)
				{
					try {			
						Thread.yield();
						raf.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					filechannel = null;
					raf = null;
				}
			}
		};
		tworker.invokeLater(r);
	}
}