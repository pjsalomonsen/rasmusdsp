/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util;

import java.io.UnsupportedEncodingException;

public final class ByteConversion {
	
	// TODO to finish
	public final static String guidToString(byte[] buffer)
	{
		String str = "";
	    for (int i = 0; i < buffer.length; i++) {
	    	String hex = Integer.toHexString( ByteConversion.byteToInt(buffer[i]) );
	    	if(hex.length() == 1)
	    		str += "0" + hex;
	    	else
	    		str += hex;
		}
	    return str;
	}
	
	public final static int byteToInt(byte buffer)
	{	
		return
		((256 + (int)buffer) % 256);
	}
	
	public final static int byteToInt(byte[] buffer)
	{	
		return ByteConversion.byteToInt(buffer[0]);
	}
	
	public final static String charToString(byte[] buffer) throws UnsupportedEncodingException
	{
		String string = new String(buffer, "latin1");
		int li = string.indexOf((char)0);
		if(li != -1) string = string.substring(0, li);
		return string;
	}
	
	public final static long dwordToLong(byte[] buffer)		
	{
		return
		(((256 + (long)buffer[3]) % 256) << 24)+
		(((256 + (long)buffer[2]) % 256) << 16)+
		(((256 + (long)buffer[1]) % 256) << 8)+
		((256 + (long)buffer[0]) % 256);
	}
	
	public final static byte[] intToByte(int buffer)
	{
		byte[] data = new byte[1];
		data[0] = (byte)buffer;
		return data;	
	}
	
	public final static byte[] intToShort(int buffer)
	{
		if(buffer < 0)
			buffer += (256*256);
		return ByteConversion.intToWord(buffer);
	}
	
	public final static byte[] intToWord(int buffer)
	{
		byte[] data = new byte[2];
		data[0] = (byte)(buffer % 256);
		buffer /= 256;
		data[1] = (byte)(buffer % 256);
		buffer /= 256;
		return data;
	}
	
	public final static byte[] longToDWord(long buffer)
	{
		byte[] data = new byte[4];
		data[0] = (byte)(buffer % 256);
		buffer /= 256;
		data[1] = (byte)(buffer % 256);
		buffer /= 256;
		data[2] = (byte)(buffer % 256);
		buffer /= 256;
		data[3] = (byte)(buffer % 256);
		buffer /= 256;
		return data;
	}
	
	public final static int shortToInt(byte[] buffer)		
	{
		int result =
			(((256 + (int)buffer[1]) % 256) << 8)+
			((256 + (int)buffer[0]) % 256);
		
		if(result >= 256*127)
		{
			return result - (256*256);
		}
		
		return result;
		
	}
	
	public final static byte[] stringToChar(String buffer, int size)
	{	
		byte[] data = buffer.getBytes();
		byte[] outbuffer = new byte[size];
		for (int i = 0; i < data.length; i++) {
			outbuffer[i] = data[i];
		}
		for (int i = data.length; i < outbuffer.length; i++) {
			outbuffer[i] = 0;
		}
		
		// to be sure we have a null terminated string
		// if(size > 0) outbuffer[size-1] = 0;
		
		return outbuffer;
	}
	
	public final static int wordToInt_LE(byte[] buffer)		
	{
		return
		(((256 + (int)buffer[1]) % 256) << 8)+
		((256 + (int)buffer[0]) % 256);
	}	
	
	
	public final static int wordToInt_BE(byte[] buffer)		
	{
		return
		(((256 + (int)buffer[0]) % 256) << 8)+
		((256 + (int)buffer[1]) % 256);
	}	
}
