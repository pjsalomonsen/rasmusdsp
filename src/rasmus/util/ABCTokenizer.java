/* 
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class ABCTokenizer {

	static public class ABCToken {
		public char symbol;

		public int accidentals = 0;

		public int octave = 0;

		public int channel = 1;

		public int velocity;

		public double len;

		public double pos;

		public String code;

		public String toString() {
			return code;
		}
	}

	static public class CommandToken {
		public int channel = 1;

		public String fieldname;

		public String fieldvalue;

		public String code;

		public double pos;

		public String toString() {
			return code;
		}
	}

	private ArrayList tokens;

	public void addLegalSymbols(String symbols) {
		legalSymbols += symbols;
	}

	public List parse(String code) {
		char[] charcode = code.toCharArray();
		tokens = new ArrayList();
		parse(charcode, 0, charcode.length);
		return tokens;
	}

	private void parse(char[] code, int start, int end) {
		int lasti = 0;
		for (int i = start; i < end; i++) {
			char c = code[i];
			if (c == '\n' || c == '\r') {
				if (i - lasti > 0)
					parseLine(code, lasti, i);
				lasti = i + 1;
			}
		}
		if (end - lasti > 0)
			parseLine(code, lasti, end);
	}

	private void parseLine(char[] code, int start, int end) {
		for (int i = start; i < end; i++) {
			char c = code[i];
			if (c == '[')
				break;
			if (c == ':') {
				char c_pre = ' ';
				char c_post = ' ';
				if (i - 1 >= 0)
					c_pre = code[i - 1];
				if (i + 1 < end)
					c_post = code[i + 1];
				if (Character.isLetter(c_pre)) {
					if (c_post != ':' && c_post != '|') {
						parseCommandLine(code, start, end);
						return;
					}
				}
				break;
			}
		}
		parseNotationLine(code, start, end);
	}

	int[] keysig = new int[7]; // fcgdaeb
	{
		Arrays.fill(keysig, 0);
	}

	String notekeys = "fcgdaeb";

	private void parseCommandLine(char[] code, int start, int end) {
		for (int i = start; i < end; i++) {
			char c = code[i];
			if (c == ':') {
				String fieldname = new String(code, start, i - start);
				String fieldvalue = new String(code, i + 1, end - (i + 1));

				CommandToken ctoken = new CommandToken();
				ctoken.channel = channel;
				ctoken.pos = beatpos * 4;
				ctoken.fieldname = fieldname;
				ctoken.fieldvalue = fieldvalue;
				ctoken.code = fieldname + ":" + fieldvalue;
				tokens.add(ctoken);

				if (fieldname.equals("v"))
					velocity = Integer.parseInt(fieldvalue.trim());
				else if (fieldname.equals("c"))
					channel = Integer.parseInt(fieldvalue.trim());
				else if (fieldname.equals("o"))
					octave = Integer.parseInt(fieldvalue.trim());
				else if (fieldname.equals("L"))
					notelen = parseNoteLen(fieldvalue.trim());
				else if (fieldname.equals("M"))
					meter = fieldvalue.trim();
				else if (fieldname.equals("K")) {
					StringTokenizer st = new StringTokenizer(fieldvalue.trim(),
							" ");
					boolean firsttoken = true;

					// 5 = B -2 = Bb 12 = B#
					// 4 = E -3 = Eb 11 = E#
					// 3 = A -4 = Ab 10 = A#
					// 2 = D -5 = Db 9 = D#
					// 1 = G -6 = Gb 8 = G#
					// 0 = C -7 = Cb 7 = C#
					// -1 = F -8 = Fb 6 = F#

					int key = 0;

					// -1 = lyd
					// 0 = maj/ion
					// 1 = mix
					// 2 = dor
					// 3 = min/aeo
					// 4 = phr
					// 5 = loc
					int keytype = 0;
					boolean keytypeset = false;
					boolean hasprocessedkey = false;

					while (st.hasMoreTokens()) {
						String token = st.nextToken().trim();
						String ltoken = token.toLowerCase();
						if (ltoken.startsWith("clef="))
							break;
						if (firsttoken) {
							firsttoken = false;

							if (ltoken.length() != 0) {
								key = notekeys.indexOf(ltoken.charAt(0)) - 1;
								if (key != -2) {
									ltoken = ltoken.substring(1);
									if (ltoken.length() != 0) {
										char cc = ltoken.charAt(0);
										if (cc == 'b') {
											key -= 7;
											ltoken = ltoken.substring(1);
										} else if (cc == '#') {
											key += 7;
											ltoken = ltoken.substring(1);
										}
									}
								} else
									key = 0;
							}

						}

						if (!keytypeset) {
							if (ltoken.startsWith("none")
									|| ltoken.startsWith("exp")) {
								keytype = 0;
								key = 0;
								keytypeset = true;
							}

							if (ltoken.startsWith("lyd")) {
								keytype = -1;
								keytypeset = true;
							}
							if (ltoken.startsWith("maj")
									|| ltoken.startsWith("ion")) {
								keytype = 0;
								keytypeset = true;
							}
							if (ltoken.startsWith("mix")) {
								keytype = 1;
								keytypeset = true;
							}
							if (ltoken.startsWith("dor")) {
								keytype = 2;
								keytypeset = true;
							}
							if (ltoken.startsWith("min")
									|| ltoken.startsWith("aeo")) {
								keytype = 3;
								keytypeset = true;
							}
							if (ltoken.startsWith("phr")) {
								keytype = 4;
								keytypeset = true;
							}
							if (ltoken.startsWith("loc")) {
								keytype = 5;
								keytypeset = true;
							}
							if (keytypeset)
								key -= keytype;
						}

						if (!hasprocessedkey) {
							if (ltoken.startsWith("="))
								keytypeset = true;
							if (ltoken.startsWith("^"))
								keytypeset = true;
							if (ltoken.startsWith("_"))
								keytypeset = true;
							if (!st.hasMoreTokens())
								keytypeset = true;

							if (keytypeset) {
								hasprocessedkey = true;
								if (key < -7)
									key += 12;
								if (key > 7)
									key -= 12;

								Arrays.fill(keysig, 0);

								if (key > 0)
									keysig[0]++;
								if (key > 1)
									keysig[1]++;
								if (key > 2)
									keysig[2]++;
								if (key > 3)
									keysig[3]++;
								if (key > 4)
									keysig[4]++;
								if (key > 5)
									keysig[5]++;
								if (key > 6)
									keysig[6]++;

								if (key < 0)
									keysig[6]--;
								if (key < -1)
									keysig[5]--;
								if (key < -2)
									keysig[4]--;
								if (key < -3)
									keysig[3]--;
								if (key < -4)
									keysig[2]--;
								if (key < -5)
									keysig[1]--;
								if (key < -6)
									keysig[0]--;

							}
						}

						int acc = 0;
						boolean acc_set = false;
						while (ltoken.length() != 0) {
							char cc = ltoken.charAt(0);
							if (cc == '_') {
								acc--;
								acc_set = true;
								ltoken = ltoken.substring(1);
							} else if (cc == '^') {
								acc++;
								acc_set = true;
								ltoken = ltoken.substring(1);
							} else if (cc == '=') {
								acc_set = true;
								ltoken = ltoken.substring(1);
							} else
								break;
						}

						if (ltoken.length() == 1) {
							if (acc_set) {
								int li = notekeys.indexOf(ltoken.charAt(0));
								if (li != -1) {
									keysig[li] = acc;
								}
							}
						}

					}

				} else if (fieldname.equals("Q")) {
					// Tempo
					// legal values can be 120 or 1/4=120 or 1/4 3/8=40 .
					// 120 means 120 notelens per min
				}
			}
		}

	}

	// z and x are rest symbols
	// x is not visible in staff prints
	// y creates space in staff prints
	private String legalSymbols = "zxabcdefgABCDEFG";

	private int channel = 1; // Field:c

	private int octave = 4; // Field:o

	private int velocity = 127; // Field:v

	private double notelen = 1.0 / 8.0; // Field:L

	private double beatpos = 0; // Field:b

	private boolean isgrouping = false;

	private double groupmul = 1.0;

	private String meter = "4/4";

	// Binary GCD algorithm
	// Reference: http://en.wikipedia.org/wiki/Binary_GCD_algorithm
	public static int gcd(int a, int b) {
		int shift = 0;
		int diff;
		while ((a & 1) == 0 && (b & 1) == 0) {
			a >>= 1;
			b >>= 1;
			shift++;
		}
		while ((a & 1) == 0)
			a >>= 1;
		do {
			while ((b & 1) == 0)
				b >>= 1;
			diff = a - b;
			if (diff < 0)
				b = -diff;
			else {
				a = b;
				b = diff;
			}
			b >>= 1;
		} while (diff != 0);
		return a << shift;
	}

	public static int[] rationalize(double val) {
		double input_val = val;
		int[] list = new int[100];
		int i = 0;
		double tol = 0.0000000001;
		double delta;
		boolean neg = val < 0;
		if (neg)
			val = -val;
		do {
			double f = Math.floor(val);
			list[i] = (int) f;
			delta = val - f;
			val = 1.0 / delta;
			i++;
			if (i >= list.length) {
				// Ration not found
				int[] ration = new int[2];
				ration[0] = (int) (input_val * 39916800);
				ration[1] = 39916800;
				if(ration[0] != 0)
				{
					int g = gcd(ration[0], ration[1]);
					ration[0] = ration[0]/g;
					ration[1] = ration[1]/g;
				}
				return ration;
			}
		} while (Math.abs(delta) > tol);

		i--;
		int a = list[i];
		int b = 1;
		i--;
		for (; i >= 0; i--) {
			int baka = a;
			a = b + a * list[i];
			b = baka;
		}
		if (neg)
			a = -a;
		int[] ration = new int[2];
		ration[0] = a;
		ration[1] = b;
		if(ration[0] != 0)
		if(ration[1] != 1)
		{
			int g = gcd(ration[0], ration[1]);
			ration[0] = ration[0]/g;
			ration[1] = ration[1]/g;
		}
		return ration;
	}

	public static double parseNoteLen(String notelen) {
		StringTokenizer st = new StringTokenizer(notelen, "/", true);
		double num = 1.0;
		boolean divmode = false;
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.equals("/")) {
				if (divmode)
					num /= 2.0;
				divmode = true;
			} else {
				if (divmode)
					num /= Double.parseDouble(token);
				else
					num *= Double.parseDouble(token);
				divmode = false;
			}
		}
		return num;
	}

	public static String formatNoteLen(double notelen) {
		
		int[] rat = rationalize(notelen);
		if(rat[1] == 1) return Integer.toString(rat[0]);	
		if(rat[1] == 2) return Integer.toString(rat[0]) + "/";
		if(rat[1] == 4) return Integer.toString(rat[0]) + "//";
		return rat[0] + "/" + rat[1];
	}

	private void parseNotationLine(char[] code, int start, int end) {

		double barbeatpos = beatpos;
		double groupmaxlen = 0;

		int tuplet_count = 0;
		double tuplet_factor = 1.0;

		double next_brythmfactor = 1.0;
		int accidentals = 0;
		boolean accidentals_inuse = false;
		StringBuffer tokenbuffer = new StringBuffer();

		for (int i = start; i < end; i++) {
			char c = code[i];
			if (c == '[' && (((i + 1 < end) && (code[i + 1] != '|')))) {
				int starti = i + 1;
				for (; i < end; i++)
					if (code[i] == ']')
						break;

				boolean isCommand = false;
				for (int j = starti; j < i; j++)
					if (code[j] == ':') {
						isCommand = true;
						break;
					}

				tokenbuffer.setLength(0);
				if (isCommand)
					parseCommandLine(code, starti, i);
				else {
					isgrouping = true;
					int from = starti;
					int to = i;

					starti = i;
					while ((i + 1 < end)
							&& (code[i + 1] == '/' || Character
									.isDigit(code[i + 1]))) {
						i++;
					}

					String notelen = "1";
					if (starti != i) {
						notelen = new String(code, starti + 1, i - starti);
					}
					groupmul = parseNoteLen(notelen);
					parseNotationLine(code, from, to);
					groupmul = 1;
					isgrouping = false;
				}
			} else if (c == '{') {
				i++;
				for (; i < end; i++)
					if (code[i] == '}')
						break;
				i++;
			} else if (c == '+') {
				i++;
				for (; i < end; i++)
					if (code[i] == '+')
						break;
				i++;
			} else if (c == '\"') {
				i++;
				for (; i < end; i++)
					if (code[i] == '\"')
						break;
			} else if (c == '=') {
				accidentals_inuse = true;
				tokenbuffer.append(c);
			} else if (c == '^') {
				accidentals++;
				accidentals_inuse = true;
				tokenbuffer.append(c);
			} else if (c == '_') {
				accidentals--;
				accidentals_inuse = true;
				tokenbuffer.append(c);
			} else if (legalSymbols.indexOf(c) != -1) {

				tokenbuffer.append(c);

				int octmodifier = 0;
				while ((i + 1 < end) && (code[i + 1] == '\'')) {
					octmodifier += 1;
					tokenbuffer.append(code[i + 1]);
					i++;
				}
				while ((i + 1 < end) && (code[i + 1] == ',')) {
					octmodifier -= 1;
					tokenbuffer.append(code[i + 1]);
					i++;
				}

				int starti = i;
				while ((i + 1 < end)
						&& (code[i + 1] == '/' || Character
								.isDigit(code[i + 1]))) {
					// tokenbuffer.append(code[i + 1]);
					i++;
				}

				String notelen = "1";
				if (starti != i) {
					notelen = new String(code, starti + 1, i - starti);
				}
				double d_notelen = parseNoteLen(notelen) * this.notelen;

				if (tuplet_count != 0) {
					d_notelen *= tuplet_factor;
					tuplet_count--;
				}

				d_notelen *= next_brythmfactor;
				next_brythmfactor = 1.0;

				int dotted = 0;
				while ((i + 1 < end) && (code[i + 1] == '>')) {
					i++;
					dotted++;
				}
				if (dotted != 0) {
					next_brythmfactor = (1.0) / ((double) dotted);
					d_notelen *= 1.0 + (((double) dotted) - 1)
							/ ((double) dotted);
				}

				if (dotted == 0) {
					while ((i + 1 < end) && (code[i + 1] == '<')) {
						i++;
						dotted++;
					}
					if (dotted != 0) {
						d_notelen *= (1.0) / ((double) dotted);
						next_brythmfactor = 1.0 + (((double) dotted) - 1)
								/ ((double) dotted);
					}

				}

				if (isgrouping)
					d_notelen *= groupmul;

				if (isgrouping)
					if (d_notelen > groupmaxlen)
						groupmaxlen = d_notelen;

				if (!accidentals_inuse) {
					int li = notekeys.indexOf(Character.toLowerCase(c));
					if (li != -1) {
						accidentals = keysig[li];
					}
				}

				ABCToken abctoken = new ABCToken();
				abctoken.symbol = c;
				abctoken.accidentals = accidentals;
				abctoken.octave = octave + octmodifier;
				abctoken.len = d_notelen * 4;
				abctoken.pos = beatpos * 4;
				abctoken.channel = channel;
				abctoken.velocity = velocity;
				abctoken.code = tokenbuffer.toString();
				tokens.add(abctoken);

				tokenbuffer.setLength(0);

				if (!isgrouping)
					beatpos += d_notelen;

				accidentals = 0;
				accidentals_inuse = false;
			} else if (c == '|' || c == ':') {
				// new bar, reset accidentals if any
				accidentals = 0;
				accidentals_inuse = false;
				next_brythmfactor = 1.0;

				barbeatpos = beatpos;
			} else if (c == '&') {
				beatpos = barbeatpos;
				// Reset Beat Position
			} else if (c == '(')
				if ((i + 1 < end)
						&& (code[i + 1] == ':' || Character
								.isDigit(code[i + 1]))) {

					int starti = i;
					while ((i + 1 < end)
							&& (code[i + 1] == ':' || Character
									.isDigit(code[i + 1]))) {
						i++;
					}

					String tupletstr = new String(code, starti + 1, i - starti);

					StringTokenizer st = new StringTokenizer(tupletstr, ":",
							true);
					int fieldid = 0;
					String p = "";
					String q = "";
					String r = "";
					while (st.hasMoreTokens()) {
						String token = st.nextToken();
						if (token.equals(":")) {
							fieldid++;
						}
						if (fieldid == 0)
							p = token;
						if (fieldid == 1)
							q = token;
						if (fieldid == 2)
							r = token;
					}
					if (p.length() == 0) {
						if (q.length() == 0)
							q = null;
						if (r.length() == 0)
							r = null;

						if (q == null || r == null) {

							tuplet_count = Integer.parseInt(p);
							double n = 2.0;
							if (meter.startsWith("6/8"))
								n = 3.0;
							else if (meter.startsWith("9/8"))
								n = 3.0;
							else if (meter.startsWith("12/8"))
								n = 3.0;
							switch (tuplet_count) {
							case 2:
								tuplet_factor = 3.0 / 2.0;
								break;
							case 3:
								tuplet_factor = 2.0 / 3.0;
								break;
							case 4:
								tuplet_factor = 3.0 / 4.0;
								break;
							case 5:
								tuplet_factor = n / 5.0;
								break;
							case 6:
								tuplet_factor = 2.0 / 6.0;
								break;
							case 7:
								tuplet_factor = n / 7.0;
								break;
							case 8:
								tuplet_factor = 3.0 / 8.0;
								break;
							case 9:
								tuplet_factor = n / 9.0;
								break;
							default:
								tuplet_factor = 1.0;
								break;
							}

						} else {
							if (r == null)
								r = p;
							if (q == null)
								q = r;

							if (r != null && q != null) {
								tuplet_count = Integer.parseInt(r);
								tuplet_factor = ((double) Integer.parseInt(q))
										/ ((double) Integer.parseInt(p));
							}
						}
					}
				}
		}

		if (isgrouping)
			beatpos += groupmaxlen;

	}

}
