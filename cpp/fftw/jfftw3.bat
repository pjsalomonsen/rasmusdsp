rem
rem
rem  This make file is used to create jfftw.dll for Windows
rem
rem  This file assume that FFTW is located in the directory : C:\java\fftw-3.1.2-dll
rem  And that the file "libfftw3-3.lib" has been created with impllib
rem  using the command line:
rem
rem      implib -a fftw3-3.lib libfftw3-3.dll
rem
rem  And this file assume java is locataed in : c:\program files\java\jdk1.5.0_05
rem
rem  This scripts uses Borland C++ Compiler version 5.5
rem
rem

set path=%path%;c:\borland\bcc55\bin

del *.dll
del *.obj
del *.tds

bcc32 -tWD -I"c:\borland\bcc55\include;c:\progra~1\java\jdk1.5.0_05\include;C:\progra~1\java\jdk1.5.0_05\include\win32;C:\java\fftw-3.1.2-dll;." -L"c:\borland\bcc55\lib;C:\java\fftw-3.1.2-dll" jfftw3.cpp

del *.obj
del *.tds

pause