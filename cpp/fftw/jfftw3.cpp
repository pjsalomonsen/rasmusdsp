/*
 * Copyright (c) 2006, Karl Helgason
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma comment(lib, "libfftw3-3.lib" )

#include "jfftw3.h"
#include <fftw3.h>

JNIEXPORT jlong JNICALL Java_rasmus_fft_fftw_FFTW_createPlanDft1D
  (JNIEnv * env, jobject obj, jint n, jint sign, jint flags)
{
	jclass clazz;
    fftw_complex *in, *out;
    fftw_plan p;

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);

    p = (fftw_plan)fftw_plan_dft_1d(n, in, out, sign, flags);

	jobject in_buffer = env->NewDirectByteBuffer(in, sizeof(fftw_complex) * n);
	jobject out_buffer = env->NewDirectByteBuffer(out,  sizeof(fftw_complex) * n);

	clazz = env->GetObjectClass( obj );
	env->SetObjectField(obj, env->GetFieldID( clazz, "invar", "Ljava/nio/ByteBuffer;" ) , in_buffer  );
	env->SetObjectField(obj, env->GetFieldID( clazz, "outvar", "Ljava/nio/ByteBuffer;" ) , out_buffer );

    return (jlong)p;
}

JNIEXPORT jlong JNICALL Java_rasmus_fft_fftw_FFTW_createPlanDft
  (JNIEnv * env, jobject obj, jintArray dims, jint sign, jint flags)
{
    // NOT IMPLEMENTED YET

    return 0;
}

JNIEXPORT void JNICALL Java_rasmus_fft_fftw_FFTW_destroyPlan
  (JNIEnv * env, jobject obj, jlong plan)
{

    fftw_plan p = (fftw_plan)plan;
	fftw_destroy_plan(p);

	jclass clazz = env->GetObjectClass( obj );
	jobject in_buffer = env->GetObjectField( obj, env->GetFieldID( clazz, "invar", "Ljava/nio/ByteBuffer;" )  );
	jobject out_buffer = env->GetObjectField( obj, env->GetFieldID( clazz, "outvar", "Ljava/nio/ByteBuffer;" )  );

	fftw_free(env->GetDirectBufferAddress(in_buffer));
    fftw_free(env->GetDirectBufferAddress(out_buffer));

	env->SetObjectField(obj, env->GetFieldID( clazz, "invar", "Ljava/nio/ByteBuffer;" ) , NULL  );
	env->SetObjectField(obj, env->GetFieldID( clazz, "outvar", "Ljava/nio/ByteBuffer;" ) , NULL );

}

JNIEXPORT void JNICALL Java_rasmus_fft_fftw_FFTW_execute
  (JNIEnv * env, jobject obj, jlong plan)
{

    fftw_plan p = (fftw_plan)plan;
	fftw_execute(p);
}
