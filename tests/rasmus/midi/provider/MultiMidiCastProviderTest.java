package rasmus.midi.provider;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import org.junit.Test;

/**
 *
 * @author Peter Salomonsen
 */
public class MultiMidiCastProviderTest {
    
    
    @Test
    public void testGetMultiCastMidiDevices() {
	Arrays.asList(MidiSystem.getMidiDeviceInfo())
	    .stream()
	    .filter((Info info) -> MultiMidiCastInfo.class.isInstance(info))
	    .forEach((Info info) -> {
	    try {
		System.out.println(MidiSystem.getMidiDevice(info));
	    } catch (MidiUnavailableException ex) {
		Logger.getLogger(MultiMidiCastProviderTest.class.getName()).log(Level.SEVERE, null, ex);
	    }
	});
    }
    
}
