import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import junit.framework.TestCase;
import rasmus.interpreter.parser.ScriptOptimizer;
import rasmus.interpreter.parser.ScriptParser;
import rasmus.interpreter.parser.ScriptTokenizer;
import rasmus.interpreter.parser.ScriptParserException;

public class TestParser extends TestCase {

	public void testParser() throws UnsupportedEncodingException, ScriptParserException
	{

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = getClass().getResourceAsStream("/testparser.r");
	
		try {
			try {
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = is.read(buffer)) != -1) {
					baos.write(buffer, 0, len);
				}
			} finally {
				is.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		String script = new String(baos.toByteArray(), "LATIN1");

	    ScriptTokenizer st = new ScriptTokenizer(script);		  
		while(st.hasMoreTokens()) { String token = st.nextToken();
		System.out.println(st.lastIndex() + ": " + token); }

		System.out.println("");
		System.out.println("==============================================");
		System.out.println("");
		
		System.out.println(ScriptParser.parse(script).toString());
		
		System.out.println("");
		System.out.println("==============================================");
		System.out.println("");

		System.out.println(ScriptOptimizer.optimize(ScriptParser.parse(script)).toString());		
	}
	
}
