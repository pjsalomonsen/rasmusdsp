import rasmus.interpreter.Interpreter;
import rasmus.interpreter.Variable;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.parser.ScriptParserException;
import junit.framework.TestCase;

public class TestMath extends TestCase {

	double x = -1.5;
	double y = 0.7;
	private double eval(String eval) throws ScriptParserException
	{
		Interpreter interpreter = new Interpreter();
		interpreter.add("x", x);
		interpreter.add("y", y);
		Variable ret = interpreter.eval(eval);
	    return DoublePart.asDouble(ret);
	}
	private boolean beval(String eval) throws ScriptParserException
	{
		return eval(eval) > 0.5;
	}

	public void testLogicalOperators() throws ScriptParserException
	{
		assertFalse(beval("12 > 13"));
		assertTrue(beval("13 > 12"));
		assertFalse(beval("13 < 12"));
		assertTrue(beval("12 < 13"));
		assertTrue(beval("12 >= 12"));
		assertFalse(beval("12 >= 13"));
		assertTrue(beval("12 < 13"));
		assertTrue(beval("12 <= 13"));
		assertFalse(beval("2 != 2"));
		assertFalse(beval("3 = 2"));
	}
	public void testLogicalOperators2() throws ScriptParserException
	{	
		assertTrue(beval("2 != 7"));
		
	}
	
	public void testNumberFormats() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("2%") - (0.02)) < 0.0000001);
		assertTrue(Math.abs(eval("2%%") - (0.0002)) < 0.0000001);
		assertTrue(Math.abs(eval("0x37") - (0x37)) < 0.0000001);
		assertTrue(Math.abs(eval("0xA7") - (0xA7)) < 0.0000001);
	}
	
	public void testSimpleOperators() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("2+5") - (2.0+5.0)) < 0.0000001);
		assertTrue(Math.abs(eval("2-5") - (2.0-5.0)) < 0.0000001);
		assertTrue(Math.abs(eval("2*5") - (2.0*5.0)) < 0.0000001);
		assertTrue(Math.abs(eval("mod(12,5)") - (12.0%5.0)) < 0.0000001);
	}
	public void testNegativeNumber() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("-2") - (-2.0)) < 0.0000001);
	}
	public void testNegativeNumbers() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("(-2)+5") - ((-2.0)+5.0)) < 0.0000001);
		assertTrue(Math.abs(eval("-2+5") - (-2.0+5.0)) < 0.0000001);
		assertTrue(Math.abs(eval("-2-5") - (-2.0-5.0)) < 0.0000001);
	}
	public void testNegativeNumbers2() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("-2*5") - (-2.0*5.0)) < 0.0000001);
		assertTrue(Math.abs(eval("-2/5") - (-2.0/5.0)) < 0.0000001);		
	}
	public void testNegativeNumbers3() throws ScriptParserException
	{		
		assertTrue(Math.abs(eval("-2*(-5)") - (-2.0*(-5.0))) < 0.0000001);
		assertTrue(Math.abs(eval("-2/(-5)") - (-2.0/(-5.0))) < 0.0000001);
		assertTrue(Math.abs(eval("-2*-5") - (-2.0*-5.0)) < 0.0000001);
		assertTrue(Math.abs(eval("-2/-5") - (-2.0/-5.0)) < 0.0000001);
	}
	public void testNegativeNumbers4() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("2*(-5)") - (2.0*(-5.0))) < 0.0000001);
		assertTrue(Math.abs(eval("2/(-5)") - (2.0/(-5.0))) < 0.0000001);
		assertTrue(Math.abs(eval("2*-5") - (2.0*-5.0)) < 0.0000001);
		assertTrue(Math.abs(eval("2/-5") - (2.0/-5.0)) < 0.0000001);
	}
	public void testFractions() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("2.1+5.1") - (2.1+5.1)) < 0.0000001);
		assertTrue(Math.abs(eval("2.2-5.2") - (2.2-5.2)) < 0.0000001);
		assertTrue(Math.abs(eval("2.3*5.3") - (2.3*5.3)) < 0.0000001);
	}
	public void testMultiplication() throws ScriptParserException
	{		
		assertTrue(Math.abs(eval("3+(5*7)") - (3+(5*7))) < 0.0000001);
		assertTrue(Math.abs(eval("3*(5+7)") - (3*(5+7))) < 0.0000001);
		assertTrue(Math.abs(eval("3*5+7") - (3*5+7)) < 0.0000001);
		assertTrue(Math.abs(eval("3+5*7") - (3+5*7)) < 0.0000001);
	}
	public void testDivAndMinusTriples() throws ScriptParserException
	{		
		assertTrue(Math.abs(eval("1-1-1") - (1-1-1)) < 0.0000001);
		assertTrue(Math.abs(eval("2/2/2") - (2.0/2.0/2.0)) < 0.0000001);
		
	}	
	public void testDivAndMinusTriples2() throws ScriptParserException
	{		
		assertTrue(Math.abs(eval("1-(1-1)") - (1-(1-1))) < 0.0000001);
		assertTrue(Math.abs(eval("2/(2/2)") - (2.0/(2.0/2.0))) < 0.0000001);
		assertTrue(Math.abs(eval("(1-1)-1") - ((1-1)-1)) < 0.0000001);
		assertTrue(Math.abs(eval("(2/2)/2") - ((2.0/2.0)/2.0)) < 0.0000001);
		
	}		
	
	public void testDivAndMinusComplex() throws ScriptParserException
	{		
		assertTrue(Math.abs(eval("1-1-1+1-1-1") - (1-1-1+1-1-1)) < 0.0000001);
		assertTrue(Math.abs(eval("1-1-1*1-1-1") - (1-1-1*1-1-1)) < 0.0000001);
		assertTrue(Math.abs(eval("2/2/2+2/2/2") - (2.0/2.0/2.0+2.0/2.0/2.0)) < 0.0000001);
		assertTrue(Math.abs(eval("2/2/2*2/2/2") - (2.0/2.0/2.0*2.0/2.0/2.0)) < 0.0000001);		
		assertTrue(Math.abs(eval("1+3-2+5-3*2+7-5*3-3+5") - (1+3-2+5-3*2+7-5*3-3+5)) < 0.0000001);
		assertTrue(Math.abs(eval("1+3-(2+5)-3*2+7-5*3-3+5") - (1+3-(2+5)-3*2+7-5*3-3+5)) < 0.0000001);		
	}		
	public void testFunctions() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("sin(1.5)") - Math.sin(1.5)) < 0.0000001);
		assertTrue(Math.abs(eval("cos(1.5)") - Math.cos(1.5)) < 0.0000001);
		assertTrue(Math.abs(eval("exp(1.5)") - Math.exp(1.5)) < 0.0000001);
		assertTrue(Math.abs(eval("abs(-1.5)") - Math.abs(-1.5)) < 0.0000001);
		assertTrue(Math.abs(eval("abs(1.5)") - Math.abs(1.5)) < 0.0000001);
		assertTrue(Math.abs(eval("acos(0.7)") - Math.acos(0.7)) < 0.0000001);
		assertTrue(Math.abs(eval("asin(0.7)") - Math.asin(0.7)) < 0.0000001);
		assertTrue(Math.abs(eval("atan(0.7)") - Math.atan(0.7)) < 0.0000001);
		assertTrue(Math.abs(eval("log(0.7)") - Math.log(0.7)) < 0.0000001);
		assertTrue(Math.abs(eval("ceil(8.7)") - Math.ceil(8.7)) < 0.0000001);
		assertTrue(Math.abs(eval("floor(12.7)") - Math.floor(12.7)) < 0.0000001);
		assertTrue(Math.abs(eval("1.5^2") - Math.pow(1.5, 2.0)) < 0.0000001);
	}
	public void testConstants() throws ScriptParserException
	{	
		assertTrue(Math.abs(eval("true") - 1) < 0.0000001);
		assertTrue(Math.abs(eval("false") - 0) < 0.0000001);
		assertTrue(Math.abs(eval("pi") - Math.PI) < 0.0000001);
		assertTrue(Math.abs(eval("e") - Math.E) < 0.0000001);
		
	}
	
	
	public void testVarSimpleOperators() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("x+Y") - (x+y)) < 0.0000001);
		assertTrue(Math.abs(eval("x-y") - (x-y)) < 0.0000001);
		assertTrue(Math.abs(eval("x*y") - (x*y)) < 0.0000001);
		assertTrue(Math.abs(eval("mod(x*13,y)") - ((x*13)%y)) < 0.0000001);
	}
	public void testVarNegativeNumber() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("-x") - (-x)) < 0.0000001);
	}
	public void testVarNegativeNumbers() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("(-x)+y") - ((-x)+y)) < 0.0000001);
		assertTrue(Math.abs(eval("-x+y") - (-x+y)) < 0.0000001);
		assertTrue(Math.abs(eval("-x-y") - (-x-y)) < 0.0000001);
	}
	public void testVarNegativeNumbers2() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("-x*y") - (-x*y)) < 0.0000001);
		assertTrue(Math.abs(eval("-x/y") - (-x/y)) < 0.0000001);		
	}
	public void testVarNegativeNumbers3() throws ScriptParserException
	{		
		assertTrue(Math.abs(eval("-x*(-y)") - (-x*(-y))) < 0.0000001);
		assertTrue(Math.abs(eval("-x/(-y)") - (-x/(-y))) < 0.0000001);
		assertTrue(Math.abs(eval("-x*-y") - (-x*-y)) < 0.0000001);
		assertTrue(Math.abs(eval("-x/-y") - (-x/-y)) < 0.0000001);
	}
	public void testVarNegativeNumbers4() throws ScriptParserException
	{
		assertTrue(Math.abs(eval("x*(-y)") - (x*(-y))) < 0.0000001);
		assertTrue(Math.abs(eval("x/(-y)") - (x/(-y))) < 0.0000001);
		assertTrue(Math.abs(eval("x*-y") - (x*-y)) < 0.0000001);
		assertTrue(Math.abs(eval("x/-y") - (x/-y)) < 0.0000001);
	}
	public void testVarMultiplication() throws ScriptParserException
	{		
		assertTrue(Math.abs(eval("x+(y*x)") - (x+(y*x))) < 0.0000001);
		assertTrue(Math.abs(eval("x*(y+x)") - (x*(y+x))) < 0.0000001);
		assertTrue(Math.abs(eval("x*x+y") - (x*x+y)) < 0.0000001);
		assertTrue(Math.abs(eval("x+x*y") - (x+x*y)) < 0.0000001);
	}
	
}
