/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.scriptengine;

import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.script.Bindings;
import javax.script.ScriptContext;

import rasmus.interpreter.namespace.GlobalNameSpace;
import rasmus.interpreter.namespace.InheritNameSpace;

public class RasmusScriptContext implements ScriptContext {
	
	RasmusBindings bindings;
	RasmusBindings global;
	
	public RasmusScriptContext()
	{
		global =  new RasmusBindings(GlobalNameSpace.getNameSpace());
		bindings = new RasmusBindings(new InheritNameSpace(global.getNameSpace()));		
	}

	public void setBindings(Bindings bindings, int scope) {
		if(scope == ScriptContext.ENGINE_SCOPE)
		{
			this.bindings = (RasmusBindings)bindings;
		}
	}

	public Bindings getBindings(int scope) {
		if(scope == ScriptContext.ENGINE_SCOPE)
		{
			return bindings;
		}
		if(scope == ScriptContext.GLOBAL_SCOPE)
		{
			return global;
		}		
		return null;
	}

	public void setAttribute(String name, Object value, int scope) {
	}

	public Object getAttribute(String name, int scope) {
		return null;
	}

	public Object removeAttribute(String name, int scope) {
		return null;
	}

	public Object getAttribute(String name) {
		return null;
	}

	public int getAttributesScope(String name) {
		return 0;
	}

	public Writer getWriter() {
		return null;
	}

	public Writer getErrorWriter() {
		return null;
	}

	public void setWriter(Writer writer) {
	}

	public void setErrorWriter(Writer writer) {
	}

	public Reader getReader() {
		return null;
	}

	public void setReader(Reader reader) {
	}

	public List<Integer> getScopes() {
		List<Integer> list = new ArrayList<Integer>();
		list.add(ScriptContext.ENGINE_SCOPE);
		list.add(ScriptContext.GLOBAL_SCOPE);
		return list;
	}


}
