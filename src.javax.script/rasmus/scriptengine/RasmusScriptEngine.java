/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.scriptengine;

import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.nio.ByteOrder;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptException;
import javax.sound.midi.Sequence;
import javax.sound.sampled.AudioFormat;

import rasmus.interpreter.Interpreter;
import rasmus.interpreter.NameSpace;
import rasmus.interpreter.Variable;
import rasmus.interpreter.list.ObjectsPart;
import rasmus.interpreter.math.DoublePart;
import rasmus.interpreter.midi.MidiSequence;
import rasmus.interpreter.namespace.InheritNameSpace;
import rasmus.interpreter.parser.ScriptParserException;
import rasmus.interpreter.sampled.AudioSession;
import rasmus.interpreter.struct.Struct;

public class RasmusScriptEngine implements ScriptEngine, Closeable, rasmus.interpreter.Closeable, Invocable, Compilable {
	
	private RasmusScriptEngineFactory factory;
	
	private Interpreter interpreter = null;
	
	private RasmusScriptContext context;
	
	public NameSpace getNameSpace()
	{
		return ((RasmusBindings)getBindings(ScriptContext.ENGINE_SCOPE)).getNameSpace();
	}
			
	public Interpreter getInterpreter()
	{
		if(interpreter == null)
			interpreter = new Interpreter(getNameSpace());
		else
			interpreter.setNameSpace(getNameSpace());
		return interpreter;
	}
	
	public RasmusScriptEngine(RasmusScriptEngineFactory factory)
	{
		this.factory = factory;				
	}
	
	public Object eval(String script) throws ScriptException {
						
		if(script.equals("close()")) 
		{
			close();
			return null;
		}
		if(script.equals("close();")) 
		{
			close();
			return null;
		}
		try {
			return getInterpreter().eval(script);
		} catch (ScriptParserException e) {
			throw new ScriptException(e.getMessage());
		}
	}

	public Object eval(Reader reader) throws ScriptException {

		StringBuffer sbuffer = new StringBuffer();
		char[] buffer = new char[1024];
		int len;
		try {
			while((len = reader.read(buffer)) != -1)
			{
				sbuffer.append(buffer, 0, len);
			}
		} catch (IOException e) {
			throw new ScriptException(e.getMessage());
		}		
		return eval(sbuffer.toString());
	}
	

	public Object eval(String script, ScriptContext context) throws ScriptException {
		ScriptContext oldcontext = getContext();		
		Object ret;
		try
		{			
			setContext(context);
			ret = eval(script);			
		}
		finally
		{
			setContext(oldcontext);
		}
		return ret;
	}

	public Object eval(Reader reader, ScriptContext context) throws ScriptException {
		ScriptContext oldcontext = getContext();		
		Object ret;
		try
		{			
			setContext(context);
			ret = eval(reader);			
		}
		finally
		{
			setContext(oldcontext);
		}
		return ret;
	}

	public Object eval(String script, Bindings n) throws ScriptException {
		Bindings oldbindings = getBindings(ScriptContext.ENGINE_SCOPE);		
		Object ret;
		try
		{			
			setBindings(n, ScriptContext.ENGINE_SCOPE);
			ret = eval(script);			
		}
		finally
		{
			setBindings(oldbindings, ScriptContext.ENGINE_SCOPE);
		}
		return ret;
	}

	public Object eval(Reader reader, Bindings n) throws ScriptException {
		Bindings oldbindings = getBindings(ScriptContext.ENGINE_SCOPE);		
		Object ret;
		try
		{			
			setBindings(n, ScriptContext.ENGINE_SCOPE);
			ret = eval(reader);			
		}
		finally
		{
			setBindings(oldbindings, ScriptContext.ENGINE_SCOPE);
		}
		return ret;
	}

	public void put(String key, Object value) {		
		if(value instanceof Sequence)
		{
			value = MidiSequence.asVariable((Sequence)value);
		}
		getInterpreter().add(key, value);
	}

	public Object get(String key) {
		return getBindings(ScriptContext.ENGINE_SCOPE).get(key);
	}

	public Bindings getBindings(int scope) {
		return getContext().getBindings(scope);
	}
	public void setBindings(Bindings bindings, int scope) {
		getContext().setBindings(bindings, scope);
	}

	public Bindings createBindings() {
		return  new RasmusBindings(new InheritNameSpace(getNameSpace()));
	}
	public ScriptContext getContext() {
		if(context == null) context = new RasmusScriptContext();
		return context;
	}
	public void setContext(ScriptContext context) {
		this.context = (RasmusScriptContext)context; 
	}

	public ScriptEngineFactory getFactory() {
		return factory;
	}

	public void close() {
		if(interpreter != null)
		{
			interpreter.close();
			interpreter = null;
		}
	}


	public Object invokeMethod(Object thiz, String name, Object... args) throws ScriptException, NoSuchMethodException {
		return null;
	}
	public Object invokeFunction(String name, Object... args) throws ScriptException, NoSuchMethodException {
		getInterpreter().call(name, args);
		return null;
	}

	public <T> T getInterface(Class<T> clasz) {
		return getInterface("output", clasz);
	}

	public <T> T getInterface(Object thiz, Class<T> clasz) {
		
		if(!(thiz instanceof Variable)) return null;
		
		Variable var = (Variable)thiz;
		String outtype = clasz.getName();	
		if(outtype.equals("java.lang.Double"))
			return clasz.cast(new Double(DoublePart.asDouble(var)));
		if(outtype.equals("java.lang.Integer"))
			return clasz.cast(new Integer((int)DoublePart.asDouble(var)));
		if(outtype.equals("java.lang.Long"))
			return clasz.cast(new Long((long)DoublePart.asDouble(var)));
		if(outtype.equals("java.lang.String"))				
			return clasz.cast(ObjectsPart.toString(var));
		if(outtype.equals("java.lang.Object[]"))
			return clasz.cast(ObjectsPart.asObjects(var));
		if(outtype.equals("javax.script.Bindings"))
		    return clasz.cast(new RasmusBindings(Struct.getInstance(var)));
		if(outtype.equals("javax.sound.midi.Sequence"))
			return clasz.cast(MidiSequence.asSequence(var));
		if(outtype.equals("javax.sound.midi.Receiver"))
			return clasz.cast(MidiSequence.getInstance(var));
		if(outtype.equals("javax.sound.midi.Transmitter"))
			return clasz.cast(MidiSequence.getInstance(var));
		if(outtype.equals("javax.sound.sampled.AudioInputStream"))
		{
			AudioFormat format = new AudioFormat(44100, 16, 2, false, ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);
			AudioSession session = new AudioSession(44100, 2);
			return clasz.cast(session.asByteStream(var, format));
		}
		
		return null;
	}

	public CompiledScript compile(String script) throws ScriptException {
		return new RasmusCompiledScript(this, script);
	}
	
	public CompiledScript compile(Reader reader) throws ScriptException {
		StringBuffer sbuffer = new StringBuffer();
		char[] buffer = new char[1024];
		int len;
		try {
			while((len = reader.read(buffer)) != -1)
			{
				sbuffer.append(buffer, 0, len);
			}
		} catch (IOException e) {
			throw new ScriptException(e.getMessage());
		}		
		return compile(sbuffer.toString());
	}	

}
