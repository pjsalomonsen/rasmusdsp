/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.scriptengine;

import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;

public class RasmusScriptEngineFactory implements ScriptEngineFactory {

	public String getEngineName() {
		return "RasmusDSP";
	}

	public String getEngineVersion() {
		return "0.2";
	}

	public List<String> getExtensions() {
		List<String> extensions = new ArrayList<String>();
		extensions.add("rasmusdsp");
		return extensions;
	}

	public List<String> getMimeTypes() {
		List<String> list = new ArrayList<String>();
		list.add("application/x-rasmusdsp");
		return list;
	}

	public List<String> getNames() {
		List<String> list = new ArrayList<String>();
		list.add("RasmusDSP");
		return list;
	}

	public String getLanguageName() {
		return "RasmusDSP";
	}

	public String getLanguageVersion() {
		return "0.2";
	}

	public Object getParameter(String key) {
	    if(key.equals(ScriptEngine.ENGINE)) return getEngineName();
	    if(key.equals(ScriptEngine.ENGINE_VERSION)) return getEngineVersion();
	    if(key.equals(ScriptEngine.NAME)) return getEngineName();
	    if(key.equals(ScriptEngine.LANGUAGE)) return getLanguageName();
	    if(key.equals(ScriptEngine.LANGUAGE_VERSION)) return getLanguageVersion();
		return null;
	}

	public String getMethodCallSyntax(String obj, String m, String... args) {

	      String ret =m + "(";
	      for (int i = 0; i < args.length; i++) {
	          ret += args[i];
	          if (i == args.length - 1) {
	              ret += ")";
	          } else {
	              ret += ",";
	          }
	      }
	      return ret;
	}

	public String getOutputStatement(String toDisplay) {
		return null;
	}

	public String getProgram(String... statements) {
	      String ret = "";
	      int len = statements.length;
	      for (int i = 0; i < len; i++) {
	          ret += statements[i] + ";\n";
	      }
	      return ret;
	}

	public ScriptEngine getScriptEngine() {
		return new RasmusScriptEngine(this);
	}



}
