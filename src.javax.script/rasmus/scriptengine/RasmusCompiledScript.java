/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package rasmus.scriptengine;

import javax.script.Bindings;
import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import rasmus.interpreter.Executable;
import rasmus.interpreter.parser.ScriptCompiler;
import rasmus.interpreter.parser.ScriptParserException;

public class RasmusCompiledScript extends CompiledScript {

	Executable exec;
	RasmusScriptEngine engine = null;
	
	public RasmusCompiledScript(RasmusScriptEngine engine, String script) throws ScriptException
	{
		this.engine = engine;
		try {
			exec = ScriptCompiler.compile(script);
		} catch (ScriptParserException e) {
			throw new ScriptException(e.getMessage());
		}			
	}

	public RasmusCompiledScript(RasmusScriptEngine engine, Executable exec) throws ScriptException
	{
		this.engine = engine;
		this.exec = exec;	
	}	

	public Object eval(ScriptContext context) throws ScriptException {
		ScriptContext bakcontext = engine.getContext();
		Object ret;
		try		
		{
			engine.setContext(context);
			ret = engine.getInterpreter().execute(exec);
		}
		finally
		{
			engine.setContext(bakcontext);
		}
		return ret;
	}
	
	public Object eval() throws ScriptException {
		return engine.getInterpreter().execute(exec);
	}

	public Object eval(Bindings bindings) throws ScriptException {
		Bindings bakbindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);
		Object ret;
		try		
		{
			engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
			ret = engine.getInterpreter().execute(exec);
		}
		finally
		{
			engine.setBindings(bakbindings, ScriptContext.ENGINE_SCOPE);
		}
		return ret;
	}

	public ScriptEngine getEngine() {
		return engine;
	}
}
