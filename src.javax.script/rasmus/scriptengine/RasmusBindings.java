/* 
 * Copyright (c) 2007, Karl Helgason
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package rasmus.scriptengine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.Bindings;

import rasmus.interpreter.NameSpace;

public class RasmusBindings implements Bindings {
	
	private NameSpace namespace;
	
	public RasmusBindings(NameSpace namespace)
	{
		this.namespace = namespace;
	}
	
	public NameSpace getNameSpace()
	{
		return namespace;
	}

	public boolean containsKey(Object key) {		
		return keySet().contains(key);
	}

	public Object get(Object key) {
		return namespace.get((String)key);
	}

	public int size() {
		return keySet().size();
	}

	public boolean isEmpty() {
		return size() != 0;
	}

	public boolean containsValue(Object value) {				
		Iterator iter = keySet().iterator();
		while (iter.hasNext()) {
			if(get(iter.next()) == value) return true;
		}		
		return false;
	}

	public Set<String> keySet() {
		Set allnames = new HashSet();
		namespace.getAllNames(allnames);
		return allnames;
	}
	public Collection<Object> values() {
		List list = new ArrayList();
		Iterator iter = keySet().iterator();
		while (iter.hasNext()) {
			list.add(get(iter.next()));			
		}
		return list;
	}	
	
	public void clear() {
		throw new UnsupportedOperationException();
	}	
	public Object put(String name, Object value) {
		throw new UnsupportedOperationException();		
	}
	public void putAll(Map<? extends String, ? extends Object> arg0) {
		throw new UnsupportedOperationException();
	}		
	public Set<Entry<String, Object>> entrySet() {
		throw new UnsupportedOperationException();
	}
	public Object remove(Object key) {	
		throw new UnsupportedOperationException();
	}

	
}
